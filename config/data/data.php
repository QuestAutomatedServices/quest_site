<?php 
include('../functions/functions.php');

function getData($n){
	
	$regdates = date_range(date('Y-m-d', strtotime("-20 days")), date("Y-m-d"));	

	$array = '';

	foreach ($regdates as $pdate) {  

		$array[] = array(strtotime($pdate)*1000,intval($n));
		$n=$n+1;
	}

	return json_encode($array);
}


?>
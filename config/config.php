<?php 

include("functions/functions.php");
session_start();

if (!isset($_SESSION['auth'])) {

	session_destroy();
	header('location: ../quest/login.php');
	exit();		
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Configurations</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">
    
    <link href="datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet">
    
    <script src="https://use.fontawesome.com/96c3b1dce4.js"></script>

    <link rel="stylesheet" href="jquery/jquery-ui.min.css"/>
    
    <style>
		.hrAlt {
			border-color:#428bca; !important
		}
	</style>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand">Dashboard Configuration</a>
       
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a href="support.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span>Home</a>
              <ul class="dropdown-menu">
					<li>Home</li>

              </ul>
            </li>
 		

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          
         <ul class="nav nav-sidebar">
                  
            <?php 
			 		
			 // TOOK RECENT ALARMS OFF USER 1, BECAUSE THEY ARE IN EACH ACCOUNTS DB
		
			$array = array('Configuration','Layout'); 
			 
			 
			 foreach($array as $fun) {
				 if($_GET['f'] == 'get'.str_replace(" ","",$fun)){
					 ?><li class="active"><a href="config.php?f=get<?php echo str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php
				 }elseif (!isset($_GET['f'])){
					 
					 if($fun == 'Alarms Overview'){
						 ?><li class="active"><a href="config.php?f=get<?php str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php
					 }else{
						?><li><a href="config.php?f=get<?php echo str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php 
					 }
				 
				 }else{
				 	?><li><a href="config.php?f=get<?php echo str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php
				 }
			 }
			 ?>
         		<?php if($_SESSION['auth'] == 1){?>
					<li><a href="../quest/index.php">Back to Dashboard</a></li>
				<?php } ?>
         
         		<li><a href="home.php">Home</a></li>
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
            <div class="row">
            
            <?php if(!isset($_GET['f'])){
					echo getConfiguration();
						
					}else{
						echo $_GET['f']();
					}
			?>
            
            	<!--
                <div class="col-md-4">
                	
                     <h2 class="sub-header"></h2>

                </div>
                -->
                
            
            </div>







        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
    
    
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script src="jquery/jquery-ui.min.js"></script>
    
	<script type="text/javascript" language="javascript" src="datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="datatables/media/js/dataTables.bootstrap.js"></script>
    
	
	<script>
		
		$('.configtable').DataTable( {
			paging: false,
			searching: false,
			ordering: false,
			info: false,
        	columnDefs: [
				{ width: 35, targets: 0 }
        	]
		} );
		
		$('.currenttable').DataTable( {
			paging: false,
			searching: false,
			ordering: false,
			info: false,
		} );
		
		var collapseNum = $('#cNum').attr('data-num');
		$('#collapse' + collapseNum).collapse("show");
		
		$('.options').change(function() {
			var targetID = $(this).attr('data-id');
			var targetType = $(this).attr('data-type');
					
			if(targetType == 2){
				if(targetID == "newAlarm"){
					$('#newID').prop("disabled", false);
				}else{
					$('#' + targetID).prop("disabled", false);
				}
			}else{
				if(targetID == "newAlarm"){
					$('#newID').prop("disabled", true);
				}else{
					$('#' + targetID).prop("disabled", true);
				}
			}
		});
		
		$('.globalOption').change(function() {
			if($(this).val() == 0){
				$('#globalInput').prop("disabled", true);
			}else{
				$('#globalInput').prop("disabled", false);
			}
		});
		
		$('#regionsNum').change(function() {
			
			$('.regionInputs').remove();
			
			for(i = 0; i < $(this).val();i++){
				$('#regionWell').append('<div class="form-group regionInputs"><div class="col-sm-2"><label for="email">Region Name</label></div><div class="col-sm-7"><input type="text" class="form-control" name="region_name[]" placeholder="Name"></div><br><br></div>');
			}
		})
		
		if(document.getElementById("regionDIV_site") != null){
			var regionHTML_site = document.getElementById("regionDIV_site").innerHTML;
		}
		if(document.getElementById("leaseDIV_site") != null){
			var leaseHTML_site = document.getElementById("leaseDIV_site").innerHTML;
		}
		
		$('#sitesNum').change(function() {
						
			$('.siteInputs').remove();
			$('.regionSelectInputsSite').remove();
			$('.leaseSelectInputsSite').remove();
			
			for(i = 0; i < $(this).val();i++){
				$('#siteWell').append('<div class="form-group siteInputs"><div class="col-sm-2"><label for="email">Site Name</label></div><div class="col-sm-7"><input type="text" class="form-control" name="site_name[]" placeholder="Name"></div><br><br></div>');
				$('#siteWell').append(leaseHTML_site);
				$('#siteWell').append(regionHTML_site);
			}
		})
		
		if(document.getElementById("regionDIV_lease") != null){
			var regionHTML_lease = document.getElementById("regionDIV_lease").innerHTML;	
		}
		
		$('#leasesNum').change(function() {
						
			$('.leaseInputs').remove();
			$('.regionSelectInputsLease').remove();
			
			for(i = 0; i < $(this).val();i++){
				$('#leaseWell').append('<div class="form-group leaseInputs"><div class="col-sm-2"><label for="email">Lease Name</label></div><div class="col-sm-7"><input type="text" class="form-control" name="lease_name[]" placeholder="Name"></div><br><br></div>');
				$('#leaseWell').append(regionHTML_lease);
			}

		})
		
		if(document.getElementById("moduleDIV_region") != null){
			var moduleHTML_region = document.getElementById("moduleDIV_region").innerHTML;
		}
		
		$('#modFieldsNum').change(function() {
			
			$('.moduleRegionInputs').remove();
			
			for(i = 0; i < $(this).val(); i++){
				$('#modFieldRegionWell').append(moduleHTML_region);
			}
		})


	</script>
  
		<?php include("javascript/support_scripts.php"); ?>
    
  </body>
</html>


<?php 

include("functions/functions.php");
include("data/data.php");
session_start();

if (!isset($_SESSION['auth'])) {

	session_destroy();
	header('location: ../quest/login.php');
	exit();		
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Home</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">
    
    <link href="datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet">
    
    <script src="https://use.fontawesome.com/96c3b1dce4.js"></script>

    <link rel="stylesheet" href="jquery/jquery-ui.min.css"/>
    
    <style>
		.hrAlt {
			border-color:#428bca; !important
		}
	</style>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand">Dashboard</a>
       
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a href="support.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span>Home</a>
              <ul class="dropdown-menu">
					<li>Home</li>

              </ul>
            </li>
 		

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          
         <ul class="nav nav-sidebar">
          <?php
			 	$global = getGlobalConfig($_SESSION['user_accountID']);
			 	$regions = getRegionConfig($_SESSION['user_accountID']);
		  ?>
                <div class="" id="accordion">
                 <ul class="list-group">
                 <?php if($global != ''){ ?>
                  <li class="list-group-item" style="background-color: inherit; border: none;"><a href="home.php?f=getGlobalHome&g=<?php echo $global['globalID'] ?>"><?php echo $global['global_name']; ?></a></li><br>
				 <?php } ?>
				 
				 <?php $i = 1;
					   $j = 1;
				 foreach($regions as $region){  ?>
				  <li class="list-group-item" style="background-color: inherit; border:none;"><a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>"><span class="glyphicon glyphicon-plus-sign" style="font-size:12px; color:#428bca;"></span></a>&nbsp;&nbsp;<a href="home.php?f=getRegionHome&r=<?php echo $region['regionID'] ?>"><?php echo $region['region_name']; ?></a></li>
					
					<div id="collapse<?php echo $i; ?>" class="collapse">
						<?php $leases = getLeaseConfigByRegion($region['regionID']);
						foreach($leases as $lease) { ?>
							<li class="list-group-item" style="border:none;">&nbsp;&nbsp;<a data-toggle="collapse" data-parent="#accordion" href="#collapseL<?php echo $j; ?>"><span class="glyphicon glyphicon-plus-sign" style="font-size:12px; color:#428bca;"></span></a>&nbsp;&nbsp;<a href="home.php?f=getLeaseHome&l=<?php echo $lease['leaseID'] ?>"><?php echo $lease['lease_name']; ?></a></li>
							<div id="collapseL<?php echo $j; ?>" class="collapse">
							<?php $leaseSites = getSiteConfigByLease($lease['leaseID']); 
							foreach($leaseSites as $leaseSite) {?>
								<li class="list-group-item" style="border:none;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="home.php?f=getSiteHome&s=<?php echo $leaseSite['siteID'] ?>"><?php echo $leaseSite['site_name']; ?></a></li>
							<?php } ?>
							</div>
						<?php $j++;
						}?>
					</div>
				 <?php 
				 $i++;
				 } ?> 
					<br>    
					<li class="list-group-item" style="background-color: inherit; border: none;"><a href="config.php">Configuration</a></li>
				 </ul>	
			 	</div>
                  
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
            <div class="row">
        
            
            <?php if(!isset($_GET['f'])){
					//echo getConfiguration();
						
					}else{
						echo $_GET['f']();
					}
			?>
            
            	<!--
                <div class="col-md-4">
                	
                     <h2 class="sub-header"></h2>

                </div>
                -->
                
            
            </div>







        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
    
    
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script src="jquery/jquery-ui.min.js"></script>
    
	<script type="text/javascript" language="javascript" src="datatables/media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="datatables/media/js/dataTables.bootstrap.js"></script>
	  
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    
	
	<script>
		$('.moduletable').DataTable( {
			paging: false,
			searching: false,
			ordering: false,
			info: false
		} );
	</script>
	  
	<script>
		
				   Highcharts.chart('chart', {
                    title: {
                        text: 'Oil/Gas/Water BBL',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d'    //ex- 01 Jan 2016
						}
						
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [
						<?php
							echo getHomeGraph($_GET['s']);
						?>
					]
                });
	
	</script>
  
    
  </body>
</html>


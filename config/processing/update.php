<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");

$a = $_SESSION['user_accountID'];

if($_POST['pt'] == 1){
	updateRegions($_POST,$a);
	$location = "../config.php?f=getConfiguration&c=2";
	
}elseif($_POST['pt'] == 2){
	updateLeases($_POST,$a);
	$location = "../config.php?f=getConfiguration&c=3";
	
}elseif($_POST['pt'] == 3){
	updateSites($_POST,$a);
	$location = "../config.php?f=getConfiguration&c=4";
	
}

header("location: $location");

?>
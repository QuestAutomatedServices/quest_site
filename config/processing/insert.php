<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");
//include("../user_mail.php");

$a = $_SESSION['user_accountID'];

if ($_POST['pt'] == 1) {
	
	deleteCurrentGlobal($a);

	$_POST['global_active'] = $_POST['globalOption'];
	unset($_POST['globalOption']);
	
	$table = 'tbl_configGlobal';
	
	$location = "../config.php?f=getConfiguration&c=1";
	
}elseif($_POST['pt'] == 2){
	
	unset($_POST['regions_number']);
	
	$p = 2;
	
	$location = "../config.php?f=getConfiguration&c=2";
	
}elseif($_POST['pt'] == 3){

	$p = 3;
	
	$location = "../config.php?f=getConfiguration&c=3";
	
}elseif($_POST['pt'] == 4){

	$p = 4;
	
	$location = "../config.php?f=getConfiguration&c=4";
}elseif($_POST['pt'] == 5){
	deleteCurrentRD($_POST['cdID'],$a);
	$s = $_POST['siteID'];
	$d = $_POST['deviceID'];
	
	$p = 5;
	
	$location = "../config.php?f=getConfigRD&s=$s&d=$d";
}elseif($_POST['pt'] == 6){
	
	$parentID = $_POST['module_parentID'];
	$type = $_POST['module_type'];
	
	$_POST['module_labels'] = serialize($_POST['module_labels']);
	$_POST['module_variables'] = serialize($_POST['module_variables']);
	
	$table = 'tbl_configModule';
	
	if($type == "g"){
		$location = "../config.php?f=getConfigModuleGlobal&g=$parentID";
	}elseif($type == "r"){
		$location = "../config.php?f=getConfigModuleRegion&r=$parentID";
	}elseif($type == "l"){
		$location = "../config.php?f=getConfigModuleLease&l=$parentID";
	}elseif($type == "s"){
		$location = "../config.php?f=getConfigModuleSite&s=$parentID";
	}

}elseif($_POST['pt'] == 7){
	
	$table = 'tbl_configVariable';
	
	$parentID = $_POST['variable_parentID'];
	
	$type = $_POST['variable_type'];
	
	$regdev1 = explode("_",$_POST['registerIDs'][0]);
	$regdev2 = explode("_",$_POST['registerIDs'][1]);
	
	$_POST['variable_regOneID'] = $regdev1[0];
	$_POST['variable_regOneDevID'] = $regdev1[1];
	$_POST['variable_cdIDOne'] = $regdev1[2];
	$_POST['variable_regTwoID'] = $regdev2[0];
	$_POST['variable_regTwoDevID'] = $regdev2[1];
	$_POST['variable_cdIDTwo'] = $regdev2[2];
	
	unset($_POST['registerIDs']);
	
	if($type == "g"){
		$location = "../config.php?f=getConfigModuleGlobal&g=$parentID";
	}elseif($type == "r"){
		$location = "../config.php?f=getConfigModuleRegion&r=$parentID";
	}elseif($type == "l"){
		$location = "../config.php?f=getConfigModuleLease&l=$parentID";
	}elseif($type == "s"){
		$location = "../config.php?f=getConfigModuleSite&s=$parentID";
	}

}elseif($_POST['pt'] == 8){
	
	$table = 'tbl_configGraphValues';
	
	$parentID = $_POST['gv_parentID'];
	
	$type = $_POST['gv_type'];
	
	if($type == "g"){
		$location = "../config.php?f=getConfigModuleGlobal&g=$parentID";
	}elseif($type == "r"){
		$location = "../config.php?f=getConfigModuleRegion&r=$parentID";
	}elseif($type == "l"){
		$location = "../config.php?f=getConfigModuleLease&l=$parentID";
	}elseif($type == "s"){
		$location = "../config.php?f=getConfigModuleSite&s=$parentID";
	}
}


unset($_POST['pt']);

if($p == 2){
	insertRegionConfigs($_POST,$a);
}elseif($p == 3){
	insertLeaseConfigs($_POST,$a);
}elseif($p == 4){
	insertSiteConfigs($_POST,$a);
}elseif($p == 5){
	insertDRConfigsHelper($_POST,$a);
}else{
	insertRecord($_POST,$table,$a);
}




header("location: $location ");




?>
<?php
session_start();
include('dfunctions.php');
ini_set('max_execution_time', "-1"); 
ini_set('memory_limit',"-1");

if (!isset($_SESSION['authB'])) {
	header('location: ../login.php');
	exit();		
}

$log = $_GET['log'];

//$database = "account_".$_SESSION['user_accountID']."";
$database = "account_300";
$db = connectTWO($database);

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */


/** PHPExcel */
require_once '../phpExcel/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);

$objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Tag Name')
	        ->setCellValue('B1', 'Friendly Name')
            ->setCellValue('C1', 'Date (GMT)')
            ->setCellValue('D1', 'Value')
			->setCellValue('E1', 'Log');


	$line = 2;

    $readings = getReadingsByLog($log,300);

    $counter = 1;

	foreach ($readings as $reading) {
		
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$line, $reading['opcua_convention'])
				->setCellValue('B'.$line, $reading['tag_plc_name'])
				->setCellValue('C'.$line, formatTimestamp($reading['register_date']))
				->setCellValue('D'.$line, $reading['register_reading'])
				->setCellValue('E'.$line, $reading['sql_table']);

            $counter++;
			$line++;

	}
			

$objPHPExcel->getActiveSheet()->setTitle('Readings Export');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="readings_export.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
ob_clean();
$objWriter->save('php://output');
exit;
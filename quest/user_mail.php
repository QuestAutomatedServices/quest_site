<?php

include("PHPMailer_5.2.0/class.phpmailer.php");

function sendUserMail($email,$name,$pass){
	
	$body = '<h3>Welcome to Quest Automated Services!</h3>
			<p>'.$name.', your user account has been setup and activated. Visit the following link to get started!</p>
			
			<a href="https://qautomated.com/">https://qautomated.com/</a><br>
			<p>Login Information<br>
				Username: '.$email.'<br>
				Password: '.$pass.' (This can be changed at anytime)
			</p>
			
			<img src="http://qautomated.com/reporting/images/logo.jpg">';
	
	$mail = new PHPMailer(); // defaults to using php "mail()"

	$mail->SetFrom('do-not-reply@qautomated.com', 'Quest Automated Services');
	$mail->AddReplyTo('do-not-reply@qautomated.com','Quest Automated Services');

	$mail->AddBCC('mmorton@quest-automated.com', 'Mark Morton');
	$mail->AddBCC('nmorton@quest-automated.com', 'Nick Morton');
	$mail->AddBCC('jmartin@quest-automated.com', 'Jacob Martin');
	$mail->AddBCC('acooper@quest-automated.com', 'Adam Cooper');
	$mail->AddAddress($email, $name);


	$mail->Subject = "Quest Automated Systems";

	$mail->MsgHTML($body);

	$mail->Send();
}

function sendUserDemoMail($email,$name,$pass){
	
	$body = '<h3>Welcome to Quest Automated Services!</h3>
			<p>'.$name.', your demo account has been setup and activated. Your account is good for two logins. Visit the following link to get started!</p>
			
			<a href="https://qautomated.com/">https://qautomated.com/</a><br>
			<p>Login Information<br>
				Username: '.$email.'<br>
				Password: '.$pass.' (This can be changed at anytime)
			</p>
			
			<img src="http://qautomated.com/reporting/images/logo.jpg">';
	
	$mail = new PHPMailer(); // defaults to using php "mail()"

	$mail->SetFrom('do-not-reply@qautomated.com', 'Quest Automated Services');
	$mail->AddReplyTo('do-not-reply@qautomated.com','Quest Automated Services');

	$mail->AddBCC('mmorton@quest-automated.com', 'Mark Morton');
	$mail->AddBCC('nmorton@quest-automated.com', 'Nick Morton');
	$mail->AddBCC('jmartin@quest-automated.com', 'Jacob Martin');
	$mail->AddBCC('acooper@quest-automated.com', 'Adam Cooper');
	$mail->AddAddress($email, $name);


	$mail->Subject = "Quest Automated Systems";

	$mail->MsgHTML($body);

	$mail->Send();
}
 

?>
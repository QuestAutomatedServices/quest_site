<?php 
session_start();

error_reporting(E_ALL);

include('connections/mysql.php');
include('functions/functions.php');

if (!isset($_SESSION['auth'])) {
	header('location: login.php');
	exit();		
}


//error_reporting(E_ALL);
//ini_set('display_errors', 1);
//echo "<pre>";
//print_r($_SESSION);

$regions = array('Texas');
	




?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

       <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
     
      <div class="container">
       
        <div class="navbar-header">
         
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#"> Grenadier Energy Partners</a>
       

        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
           
            <li <?php if (!isset($_GET['r']) && !isset($_GET['f'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            
				<li <?php if ($_GET['r'] ==  ($key+1) && !isset($_GET['d']) && !isset($_GET['tb'])) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
             
 		<?php if (isset($_GET['r'])) { ?>
         
            <ul class="nav navbar-nav navbar-right">
				<li class="nav-header">Automated Sites</li>


					<?php 
				
				if ($_GET['d'] == 239) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=239&s=6">Hyden 47-38 WA 1Ha</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=239&s=6">Hyden 47-38 WA 1H</a></li>';
				}
				
				
	
				echo '<ul><li><a href="dashboard.php?r=1&d=239&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=64">Oil Tank #8393</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=70">Oil Tank #8394</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=76">Oil Tank #8395</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=82">Oil Tank #8396</a></li></ul>';
	
				if ($_GET['d'] == 241) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=241&s=6">Broughton-Wise 18-19 WA 1H</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=241&s=6">Broughton-Wise 18-19 WA 1H</a></li>';	
				}
	
	
					echo '<ul><li><a href="dashboard.php?r=1&d=241&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=64">Oil Tank #1</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=70">Oil Tank #2</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=76">Oil Tank #3</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=82">Oil Tank #4</a></li></ul>'; 
	
	
				if ($_GET['d'] == 244) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=244&s=6">Oldham Trust 4051WA</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=244&s=6">Oldham Trust 4051WA</a></li>';	
				}
	
	
					echo '<ul><li><a href="dashboard.php?r=1&d=244&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=64">Oil Tank #1</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=70">Oil Tank #2</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=76">Oil Tank #3</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=82">Oil Tank #4</a></li></ul>'; 
	
	
				if ($_GET['d'] == 250) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=250&s=6">Oldham Trust 3871WA</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=250&s=6">Oldham Trust 3871WA</a></li>';	
				}
	
	
					echo '<ul><li><a href="dashboard.php?r=1&d=250&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=64">9816</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=70">9815</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=76">9814</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=82">9813</a></li></ul>'; 
	
	
				if ($_GET['d'] == 254) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=254&s=6">Oldham Trust 4058LS</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=254&s=6">Oldham Trust 4058LS</a></li>';	
				}
	
	
					echo '<ul><li><a href="dashboard.php?r=1&d=254&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=64">Oil Tank #1</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=70">Oil Tank #2</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=76">Oil Tank #3</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=82">Oil Tank #4</a></li></ul>'; 
	
/*				if ($_GET['w'] == 3) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=3">Old Ham Trust 40-25 WA 1H</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=3">Old Ham Trust 40-25 WA 1H</a></li>';	
				}*/
	
				if ($_GET['w'] == 4) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=4">Whitaker 3905 1LS</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=4">Whitaker 3905 1LS</a></li>';	
				}
	
				if ($_GET['w'] == 5) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=5">Whitaker 3907 WA</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=5">Whitaker 3907 WA</a></li>';	
				}
	
	
				echo '<li class="nav-header">Manual Sites</li>';
	
							  

				if ($_GET['tb'] == 1) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=1">Broughton-Wise #1</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=1">Broughton-Wise #1</a></li>';
				}
					
				if ($_GET['tb'] == 2) {	
					echo '<li class="active"><a href="dashboard.php?r=1&tb=2">Gillihan #3</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=2">Gillihan #3</a></li>';
				}
					
				if ($_GET['tb'] == 6) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=6">Hyden SWD</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=6">Hyden SWD</a></li>';
				}
					
				if ($_GET['tb'] == 7) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=7">M.C. Hyden #7</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=7">M.C. Hyden #7</a></li>';
				}
					
				if ($_GET['tb'] == 3) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=3">Midway 6 #1</a></li>'; 
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=3">Midway 6 #1</a></li>'; 
				}
					
				if ($_GET['tb'] == 5) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=5">Satterwhite #1</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=5">Satterwhite #1</a></li>';
				}
						
				if ($_GET['tb'] == 4) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=4">South Hutto Tank Battery</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=4">South Hutto Tank Battery</a></li>';
				}
	
				//if ($_GET['w'] == 6) {
					//echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=6">Oldham Trust 3871WA</a></li>';
				//} else {
				//	echo '<li><a href="dashboard.php?r=1&d=999&w=6">Oldham Trust 3871WA</a></li>';
				//}
	 
/*				if ($_GET['w'] == 7) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=7">Oldham Trust 4058LS</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=7">Oldham Trust 4058LS</a></li>';	
				}*/
	
				if ($_GET['w'] == 8) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=8">Chevron USA 3-38 WA 1H</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=8">Chevron USA 3-38 WA 1H</a></li>';
				}
				?>
            	
          </ul>
         <?php } ?>


		 
		   <ul class="nav navbar-nav navbar-right">
			 <?php 						  
					if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
						echo '<li><a href="dashboard.php?f=getAccountUsers">Users</a></li>';
					}
					echo '<li><a href="dashboard.php?f=getCurrentUser">Account</a></li>';
				?>
				
			<li><a href="../support/support.php">Support</a></li>
		
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li <?php if (!isset($_GET['r']) && !isset($_GET['f'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            

				<li <?php if ($_GET['r'] ==  ($key+1) && !isset($_GET['d']) && !isset($_GET['tb'])) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
          
          
         <?php if (isset($_GET['r'])) { ?>
          <ul class="nav nav-list">
            <li class="nav-header">Sites</li>
            
            <?php //$sitedevices = getDevicesByRegion($_SESSION['account_id'],$regions[$_GET['r']-1]); 
					

		/*			foreach ($sitedevices as $device) {
						 
						if ($_GET['d'] == $device['device_deviceID']) {
							echo '<li class="leftnavactive"><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">&raquo; '.$device['device_name'].'</a></li>'; 
						} else { 
							echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
						}
					}*/ 
	
				if ($_GET['d'] == 239) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=239&s=6">Hyden 47-38 WA 1Ha</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=239&s=6">Hyden 47-38 WA 1H</a></li>';
				}
				
				
	
				echo '<ul><li><a href="dashboard.php?r=1&d=239&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=64">Oil Tank #8393</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=70">Oil Tank #8394</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=76">Oil Tank #8395</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=82">Oil Tank #8396</a></li></ul>';
	
				if ($_GET['d'] == 241) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=241&s=6">Broughton-Wise 18-19 WA 1H</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=241&s=6">Broughton-Wise 18-19 WA 1H</a></li>';	
				}
	
	
				echo '<ul><li><a href="dashboard.php?r=1&d=241&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=64">Oil Tank #1</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=70">Oil Tank #2</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=76">Oil Tank #3</a></li><li><a href="dashboard.php?r=1&d=241&c=1&g=82">Oil Tank #4</a></li></ul>';
	
	
				if ($_GET['d'] == 244) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=244&s=6">Oldham Trust 4051WA</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=244&s=6">Oldham Trust 4051WA</a></li>';	
				}
	
	
				echo '<ul><li><a href="dashboard.php?r=1&d=244&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=64">Oil Tank #1</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=70">Oil Tank #2</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=76">Oil Tank #3</a></li><li><a href="dashboard.php?r=1&d=244&c=1&g=82">Oil Tank #4</a></li></ul>'; 
	
	
				if ($_GET['d'] == 250) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=250&s=6">Oldham Trust 3871WA</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=250&s=6">Oldham Trust 3871WA</a></li>';	
				}
	
	
					echo '<ul><li><a href="dashboard.php?r=1&d=250&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=64">9816</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=70">9815</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=76">9814</a></li><li><a href="dashboard.php?r=1&d=250&c=1&g=82">9813</a></li></ul>'; 
	
	
				if ($_GET['d'] == 254) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=254&s=6">Oldham Trust 4058LS</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=254&s=6">Oldham Trust 4058LS</a></li>';	
				}
	
	
					echo '<ul><li><a href="dashboard.php?r=1&d=254&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=64">Oil Tank #1</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=70">Oil Tank #2</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=76">Oil Tank #3</a></li><li><a href="dashboard.php?r=1&d=254&c=1&g=82">Oil Tank #4</a></li></ul>'; 
	
	
/*				if ($_GET['w'] == 3) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=3">Old Ham Trust 40-25 WA 1H</a></li>'; 
				} else { 
					echo '<li><a href="dashboard.php?r=1&d=999&w=3">Old Ham Trust 40-25 WA 1H</a></li>';	
				}*/ 
	
				if ($_GET['w'] == 4) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=4">Whitaker 3905 1LS</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=4">Whitaker 3905 1LS</a></li>';	
				}
	
				if ($_GET['w'] == 5) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=5">Whitaker 3907 WA</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=5">Whitaker 3907 WA</a></li>';	
				}
	
				//if ($_GET['w'] == 6) {
					//echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=6">Oldham Trust 3871WA</a></li>';
				//} else {
					//echo '<li><a href="dashboard.php?r=1&d=999&w=6">Oldham Trust 3871WA</a></li>';
				//}
	 
/*				if ($_GET['w'] == 7) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=7">Oldham Trust 4058LS</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=7">Oldham Trust 4058LS</a></li>';	
				}*/
	
				if ($_GET['w'] == 8) {
					echo '<li class="active"><a href="dashboard.php?r=1&d=999&w=8">Chevron USA 3-38 WA 1H</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&d=999&w=8">Chevron USA 3-38 WA 1H</a></li>';
				}
	
	
				
				
				
			
				
				

	
	
				echo '<li class="nav-header">Manual Sites</li>';
	
							  

				if ($_GET['tb'] == 1) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=1">Broughton-Wise #1</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=1">Broughton-Wise #1</a></li>';
				}
					
				if ($_GET['tb'] == 2) {	
					echo '<li class="active"><a href="dashboard.php?r=1&tb=2">Gillihan #3</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=2">Gillihan #3</a></li>';
				}
					
				if ($_GET['tb'] == 6) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=6">Hyden SWD</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=6">Hyden SWD</a></li>';
				}
					
				if ($_GET['tb'] == 7) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=7">M.C. Hyden #7</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=7">M.C. Hyden #7</a></li>';
				}
					
				if ($_GET['tb'] == 3) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=3">Midway 6 #1</a></li>'; 
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=3">Midway 6 #1</a></li>'; 
				}
					
				if ($_GET['tb'] == 5) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=5">Satterwhite #1</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=5">Satterwhite #1</a></li>';
				}
						
				if ($_GET['tb'] == 4) {
					echo '<li class="active"><a href="dashboard.php?r=1&tb=4">South Hutto Tank Battery</a></li>';
				} else {
					echo '<li><a href="dashboard.php?r=1&tb=4">South Hutto Tank Battery</a></li>';
				}

			?>
            	
          </ul>
         <?php } ?>
         
          
         <ul class="nav nav-sidebar">
                  
                 <?php 
						 if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
							  if ($_GET['f'] == 'getAccountUsersTwo'){
								  echo '<li class="active"><a href="dashboard.php?f=getAccountUsersTwo&a=46">Users</a></li>';
							  }else{
								  echo '<li><a href="dashboard.php?f=getAccountUsersTwo&a=46">Users</a></li>';
							  }
						}
			 
			 			if ($_GET['f'] == 'getCurrentUser'){
 								
								echo '<li class="active"><a href="dashboard.php?f=getCurrentUser&a=46">Account</a></li>';			
							  }else{
								   echo '<li><a href="dashboard.php?f=getCurrentUser&a=46">Account</a></li>';
						 }
			 
			 
			 
			 ?>
                   
			<li><a href="../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<?php if(!isset($_GET['f'])){
					
			?>
         
          <h1 class="page-header"><?php if (!isset($_GET['r']) && !isset($_GET['f'])) { echo 'Global Overview'; } else if (!isset($_GET['d'])  && !isset($_GET['f']) && !isset($_GET['tb'])) { echo $regions[$_GET['r']-1].' Overview'; } ?>
          
          			<?php if (isset($_GET['d'])  && $_GET['d'] == 239)  {
				

								echo 'Hyden 47-38 WA 1H'; 
				
						} else  if (isset($_GET['d'])  && $_GET['d'] == 241)  {
				
								echo 'Broughton-Wise 18-19 WA 1H';
				
						} else  if (isset($_GET['d'])  && $_GET['d'] == 244)  {
				
								echo 'Oldham Trust 4051WA';
				
						} else  if (isset($_GET['d'])  && $_GET['d'] == 250)  {
				
								echo 'Oldham Trust 3871WA';
				
						} else  if (isset($_GET['d'])  && $_GET['d'] == 254)  {
				
								echo 'Oldham Trust 4058LS';
				
						} else {
						

							//if ( $_GET['w'] == 2 ) {
								//echo 'Broughton-Wise 18-19 WA 1H'; 
						//	}
				
							//if ( $_GET['w'] == 3 ) {
							//	echo 'Old Ham Trust 40-25 WA 1H'; 
							//}
				
							if ( $_GET['w'] == 4 ) {
								echo 'Whitaker 3905 1LS'; 
							}
							
							if ( $_GET['w'] == 5 ) {
								echo 'Whitaker 3907 WA'; 
							}
				
/* 
							if ( $_GET['w'] == 6 ) {
								echo 'Oldham Trust 3871WA'; 
							}
				
							if ( $_GET['w'] == 7 ) {
								echo 'Oldham Trust 4058LS'; 
							}*/
				
							if ( $_GET['w'] == 8 ) {
								echo 'Chevron USA 3-38 WA 1H'; 
							}
				
							if ($_GET['tb'] == 1) {
								echo 'Broughton-Wise #1';
							}
					
							if ($_GET['tb'] == 2) {	
								echo 'Gillihan #3';
							}

							if ($_GET['tb'] == 6) {
								echo 'Hyden SWD';
							}
					
							if ($_GET['tb'] == 7) {
								echo 'M.C. Hyden #7';
							}
					
							if ($_GET['tb'] == 3) {
								echo 'Midway 6 #1'; 
							}
					
							if ($_GET['tb'] == 5) {
								echo 'Satterwhite #1';
							}
						
							if ($_GET['tb'] == 4) {
								echo 'South Hutto Tank Battery';
							}
				
							
					}
			  ?>
          			
          			
          	</h1>

			<?php 
			}else{
				echo $_GET['f']();
			}?>



        <?php  if (!isset($_GET['d']) && !isset($_GET['f']) && !isset($_GET['tb'])) { 
			
			
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-29 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);
			
			
			
			?>  
        
       	 <div class="btn-group topbuttons">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       
  
           		
            </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
		
        
			    
             <div id="chart"></div>

<?php /*?>    		<div class="btn-group">
            
            	 <?php  if (isset($_GET['r'])) { ?>
                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>" class="btn btn-primary <?php if (!isset($_GET['rng'])) echo 'active' ?>">1 Day</a>
                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&rng=5" class="btn btn-primary <?php if ($_GET['rng'] == 5)  echo 'active' ?>">5 Day</a>         
                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&rng=10" class="btn btn-primary <?php if ($_GET['rng'] == 10)  echo 'active' ?>">10 Day</a> 
            	 <?php  } else { ?> 	
                  <a href="dashboard.php" class="btn btn-primary <?php if (!isset($_GET['rng'])) echo 'active' ?>">1 Day</a>
                  <a href="dashboard.php?rng=5" class="btn btn-primary <?php if ($_GET['rng'] == 5)  echo 'active' ?>">5 Day</a>                  
                  <a href="dashboard.php?rng=10" class="btn btn-primary <?php if ($_GET['rng'] == 10)  echo 'active' ?>">10 Day</a>               
            	 <?php  } ?> 	


            </div><?php */?>
        


			
            <div class="row">
            	
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Oil</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                  <!--<th>Barrels Sold</th>-->
                                </tr>
                              </thead>
                              <tbody> 
                               <?php 
	
										$totals = getTotalsArrayGroup(array(239,241,244,250,254),46); 
	
							   	   		foreach ($totals as $key=>$total) {  
											
											//<td>'.$total['totsales'].'</td>
											
											$globaltotal = $total['totprod'] + getLogbookTotalCalcByDate(2,$total['calculation_date'],46);
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.number_format($globaltotal,2).'</td>
												</tr>';
                                    
										}
	/*
								   		foreach ($regdates as $pdate) {  

											echo '<tr>
													  <td>'.formatDate($pdate).'</td>
													<td>'.getOilProd($pdate,46).'</td>
													<td>'.getOilSale($pdate,46).'</td>

													</tr>';

											}
										/*foreach ($regdates as $pdate) {  

/*											echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>'.getRegReadingByDate(239,150,$pdate,46).'</td> 
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													</tr>';
										
												echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													</tr>';

											}*/
                               
                               ?>
                              </tbody>
                            </table>

                </div>
            

                <div class="col-md-4">

                     <h2 class="sub-header">Water</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                  <!--<th>Disposed</th>-->
                
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php  
							       
						   		$totals_water = getTotalsArrayWaterGroup(array(239,241,244,250,254),46);

                             		foreach ($totals_water as $key=>$total) {  
										
										$globaltotal = $total['totprod'] + getLogbookTotalCalcByDate(3,$total['calculation_date'],46);
											
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  		<td>'.number_format($globaltotal,2).'</td>
												</tr>';
                                    
										}
	
	
						/*				foreach ($regdates as $pdate) {  

/*											echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>'.getRegReadingByDate(239,150,$pdate,46).'</td> 
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													</tr>';
										
												echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													</tr>';

											}*/
                               
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Gas</h2> 
                     
                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
								<th>Production (MCF)</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
	
								$totals_gas = getTotalsArrayGasGroup(array(239,241,244,250,254),46);
	
							

                             		foreach ($totals_gas as $key=>$total) {  
										
											
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  		<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
							       
/*									foreach ($regdates as $pdate) {  
										
										$globaltotal = getRegReadingByDate(239,204,$pdate,46) + getLogbookTotalCalcByDate(1,$pdate,46);

										echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
														<td>'.number_format($globaltotal,2).'</td>
													</tr>';
										
 
											}*/
                               
                               ?>
                              </tbody>
                            </table>
                    
                
                </div>
            
            
            </div>
            
            
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>

<?php /*?>
            <div class="row"> 
            	
                <div class="col-md-6">
                	
                     <h2 class="sub-header">Gas</h2> 
                     
                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Accumulated Volume (MCF)</th>
                                  <th>Spot Rate (MCFD)</th>
                                  <th>Previous Day (MCF)</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
							       
									foreach ($regdates as $pdate) {  

										echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>'.getRegReadingByDate(239,200,$pdate,46).'</td> 
													<td>'.getRegReadingByDate(239,202,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,204,$pdate,46).'</td>
													</tr>';
										
 
											}
                               
                               ?>
                              </tbody>
                            </table>
                    
                
                </div>
            

                <div class="col-md-6">
                
                     <h2 class="sub-header">Pressure</h2>

                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Tubing</th>
                                  <th>Casing</th>
                                  <th>HT</th>
                                  <th>LE-VSP</th>
                                  <th>H-VSP</th>
                                  <th>SW-VSP</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       

									foreach ($regdates as $pdate) {  

											echo '<tr>
													  <td>'.formatDate($pdate).'</td>
													<td>'.getRegReadingByDate(239,150,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,152,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,153,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,154,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,155,$pdate,46).'</td>

													</tr>';

											}

                               
                               ?>
                              </tbody>
                            </table>

                
                </div>
            
            </div><?php */?>
            


        <?php  } ?>  
        
        
        
        
         <?php  if (isset($_GET['tb'])) { 
			
			
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-6 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);
			
			
			
			?>  
        
       	 <div class="btn-group topbuttons">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       				  <a href="logbook.php?tb=<?php echo $_GET['tb']; ?>&date=<?php echo date("m/Y"); ?>" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>" target="_blank">Digital Log Book</a>
  
           		
            </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
		
        
			    
             <div id="chart"></div>

        


			
            <div class="row">
            	
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Oil</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody> 
                               <?php 
	
										$totals = getTotalsArrayLogbook($_GET['tb'],2,46); 
	
							   	   		foreach ($totals as $key=>$total) {  
											
											//<td>'.$total['totsales'].'</td>
												
											echo '<tr>
												    <td>'.formatDate($total['calc_date']).'</td>
												  	<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
	
                               
                               ?>
                              </tbody>
                            </table>

                </div>
            

                <div class="col-md-4">

                     <h2 class="sub-header">Water</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                  <!--<th>Disposed</th>-->
                
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php  
							       
									$totals = getTotalsArrayLogbook($_GET['tb'],3,46); 
	
							   	   		foreach ($totals as $key=>$total) {  
											
											//<td>'.$total['totsales'].'</td>
												
											echo '<tr>
												    <td>'.formatDate($total['calc_date']).'</td>
												  		<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
	
					                               
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Gas</h2> 
                     
                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
								<th>Production (MCF)</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
							       
									$totals = getTotalsArrayLogbook($_GET['tb'],1,46); 
	
							   	   		foreach ($totals as $key=>$total) {  
											
											//<td>'.$total['totsales'].'</td>
												
											echo '<tr>
												    <td>'.formatDate($total['calc_date']).'</td>
												  		<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
                               
                               ?>
                              </tbody>
                            </table>
                    
                
                </div>
            
            
            </div>
            
            
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1&tb=<?php echo $_GET['tb']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2&tb=<?php echo $_GET['tb']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3&tb=<?php echo $_GET['tb']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>


        <?php  } ?>  
        


        <?php  if (isset($_GET['d']) && $_GET['d'] != 999 ) { ?>  

	       	<div class="btn-group topbuttons">
                 
                   <?php  if ($_GET['d'] == 250) { ?>  
	                   <a href="http://166.130.113.209:8383" class="btn btn-primary" target="_blank">HMI</a>
					<?php  } ?> 
                 
                    <?php  if ($_GET['d'] == 254) { ?>  
	                   <a href="http://166.130.14.90:8383" class="btn btn-primary" target="_blank">HMI</a>
					<?php  } ?> 
                  
                 <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=6" class="btn btn-primary <?php if ($_GET['s'] == 6) echo 'active' ?>">Overview</a>
                 
                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Oil</a>
                  
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Water</a>
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=3" class="btn btn-primary <?php if ($_GET['s'] == 3) echo 'active' ?>">Gas</a>
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=4" class="btn btn-primary <?php if ($_GET['s'] == 4) echo 'active' ?>">Pressures</a>

                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=5" class="btn btn-primary <?php if ($_GET['s'] == 5) echo 'active' ?>">ESP</a>
                   
                    <?php // if ($_GET['d'] == 241) { ?>  
	                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=7" class="btn btn-primary <?php if ($_GET['s'] == 7) echo 'active' ?>">Daily Rate</a>
					<?php // } ?> 
              </div>


	       	<div class="btn-group topbuttons">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>
            
                  <a href="logbook.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&b=1" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>" target="_blank">Digital Log Book</a>
                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&fm=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           			<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&m=1" class="btn btn-primary <?php if ($_GET['m'] == 1) echo 'active' ?>">Map</a>
           		
            </div>
            
          <?php  if ( $_GET['s'] or $_GET['g'] ) { ?>  
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
			  <?php  } ?>  
			 
		
 		
        <?php  } ?>  


        <?php  if ($_GET['m'] == 1) { ?>  

          	<h2 class="sub-header">Location & Status</h2>

			<div class="alert-success">Device Properly Working</div>
			<div id="map"></div>

        <?php  } ?>  

        <?php  if ($_GET['fm'] == 1) { ?>  
          	


          	<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

        <?php  } ?>  
        
        

        <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],46); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],46); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
							  <th class="fullview">Register #</th>
							  <th class="fullview">GMT Date</th>
							  <th>TZ Date</th>
							  <th>Reading</th>
							</tr>
							
						<?php $readings = getRegisterReadings($_GET['d'],$_GET['g'],46); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td class="fullview">'.$reading['register_number'].'</td>
											<td class="fullview">'.formatTimestamp($reading['register_date']).'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  </thead>
						  <tbody>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Reporting Configuration</h2>
          	  	
          	 
            
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>Label</th>
							  <th>Config</th>
							  <th>Measurement Factor</th>
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getRegistersByDevice($_GET['d'],46); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {
								
								$noprodsell = '';
								$prodsell = '';
								
								if ($reg['register_prod_sell'] == 1) {
									$noprodsell = ' checked="checked" ';
								}
								
								if ($reg['register_prod_sell'] == 2) {
									$prodsell = ' checked="checked" ';
								}
								
								if ($reg['register_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}
								
								if ($reg['register_label'] == '') {
									
									$reg['register_label'] = $reg['register_name'];
								}
								

								echo '<tr '.$configset.'>
										<td id="'.$reg['register_number'].'">'.$reg['register_number'].'</td>
										<td><input name="register_label" class="reglabel" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="25" type="text" value="'.$reg['register_label'].'"> <i class="fa fa-info-circle fa-lg" aria-hidden="true" title="#'.$reg['register_number'].'<br>'.$reg['register_label'].'<br>'.$reg['register_name'].'"></i></td>
										<td>'.getRegistersConfigSelect($_GET['d'],$reg['register_number'],$reg['register_config']).'</td>
										<td><input name="measurement_factor" class="regfactor" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="8" type="text" value="'.$reg['register_factor'].'"></td>
										<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['register_number'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  

		 <?php  if ($_GET['s'] == 6) { 
							  
							  
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-29 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);	  
							  
							  ?> 
        
        	<div id="chart"></div>
        	
        		
            <div class="row">
            	
                <div class="col-md-3">
                	
                     <h2 class="sub-header">Oil</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Barrels Produced</th>
                                  <!--<th>Barrels Sold</th>-->
                                </tr>
                              </thead>
                              <tbody> 
                               <?php 
	
										$totals = getTotalsArray($_GET['d'],46); 
	
							   	   		foreach ($totals as $key=>$total) {  
											
											//<td>'.$total['totsales'].'</td>
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.$total['totprod'].'</td>
												</tr>';
                                    
										}
	/*
								   		foreach ($regdates as $pdate) {  

											echo '<tr>
													  <td>'.formatDate($pdate).'</td>
													<td>'.getOilProd($pdate,46).'</td>
													<td>'.getOilSale($pdate,46).'</td>

													</tr>';

											}
										/*foreach ($regdates as $pdate) {  

/*											echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>'.getRegReadingByDate(239,150,$pdate,46).'</td> 
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													</tr>';
										
												echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													</tr>';

											}*/
                               
                               ?>
                              </tbody>
                            </table>

                </div>
            

                <div class="col-md-3">

                     <h2 class="sub-header">Water</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production</th>
                                  <!--<th>Disposed</th>-->
                
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php  
							       
						   		$totals_water = getTotalsArrayWater($_GET['d'],46);

                             		foreach ($totals_water as $key=>$total) {  
										
										//<td>'.$total['tottrucked'].'</td>
											
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.$total['totprod'].'</td>
												</tr>';
                                    
										}
	
	
						/*				foreach ($regdates as $pdate) {  

/*											echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>'.getRegReadingByDate(239,150,$pdate,46).'</td> 
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													<td>'.getRegReadingByDate(239,151,$pdate,46).'</td>
													</tr>';
										
												echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													<td>&nbsp;</td> 
													</tr>';

											}*/
                               
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
                <div class="col-md-6">
                	
                     <h2 class="sub-header">Gas</h2> 
                     
                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Accumulated Volume (MCF)</th>
                                  <th>Spot Rate (MCFD)</th>
                                  <th>Previous Day (MCF)</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
							       
									foreach ($regdates as $pdate) {  
										
										$gasarray = getRegReadingByDateMulti($_GET['d'],'200,202,204',$pdate,46);

										echo '<tr>
													 <td>'.formatDate($pdate).'</td> 
													<td>'.$gasarray[0].'</td> 
													<td>'.$gasarray[1].'</td>
													<td>'.$gasarray[2].'</td>
													</tr>';
										
 
											}
	
										unset($gasarray);
                               
                               ?>
                              </tbody>
                            </table>
                    
                
                </div>
            
            
            </div>
        
         <?php  } ?> 

        <?php  if ($_GET['s'] == 1) { 
							  
				//////////oil					
				$totals = getTotalsArray($_GET['d'],46);
				$totalsreg = getTotalsArrayByRegister($_GET['d'],46);
	
				//echo "<pre>";
				//print_r($totalsreg);

							  
		 ?>  
        
        <div id="chart"></div>

          <h2 class="sub-header">Production & Sales</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Oil Production (ft/in)</th>
			      <th>Barrels Produced</th>
				  <th>Oil Sold (ft/in)</th>
				 <th>Barrels Sold</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
					
					if ($_GET['d'] == 239) {
						$tanklevel = 'Hyden 47-38 WA 1H';
					} else if ($_GET['d'] == 241) { 
						$tanklevel = 'Broughton-Wise 18-19 WA 1H';
					} else if ($_GET['d'] == 244) { 
						$tanklevel = 'Oldham Trust 4051WA';
					}
			   
			   		foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#sales'.$key.'" aria-hidden="true" title="Show Detail"></i> '.$tanklevel.'</td>						  
							  <td>'.formatDate($total['calculation_date']).'</td>
							  <td>'.$total['totprodin'].'"</td>
							  <td>'.$total['totprod'].'</td>
							  <td>'.$total['totsalesin'].'"</td>
							  <td>'.$total['totsales'].'</td>
							</tr>';
						
						
						echo '<tr id="sales'.$key.'" class="collapse">
								<td colspan="6">
								           <table class="table table-striped">
										  <thead>
											<tr class="warning">
											  <th>Tank</th>
											  <th>Date</th>
											  <th>Oil Production (ft/in)</th>
											  <th>Barrels Produced</th>
											  <th>Oil Sold (ft/in)</th>
											 <th>Barrels Sold</th>
											</tr>
										  </thead>
										  <tbody>';
						
												foreach ($totalsreg as $rkey=>$rtotal) {  	
													
													if ($total['calculation_date'] == $rtotal['calculation_date']) {
														
														echo '<tr>
														  <td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$rtotal['register_number'].'">'.getRegisterName($_GET['d'],$rtotal['register_number'],46).'</a></td>				
														   <td>'.formatDate($rtotal['calculation_date']).'</td>
														   	  <td>'.$rtotal['totprodin'].'"</td>
															  <td>'.$rtotal['totprod'].'</td>
															  <td>'.$rtotal['totsalesin'].'"</td>
															  <td>'.$rtotal['totsales'].'</td>
														</tr>';
														
													}
													
												
													
												}
										  	
											
												
						echo				 '</tbody>
										 </table>
								
								</td>
							</tr>';
						
						
						
					}
			   
			   ?>
              </tbody>
            </table>
          </div>

        <?php /*?>  <h2 class="sub-header">Sales</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Barrels</th>
                </tr>
              </thead>
              <tbody>
               <?php 
						foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#sales'.$key.'" aria-hidden="true" title="Show Detail"></i> '.getDeviceName($_GET['d']).'</td>	
							  <td>'.formatDate($total['calculation_date']).'</td>
							  <td>'.$total['totsales'].'</td>
							</tr>';
							
							
							echo '<tr id="sales'.$key.'" class="collapse">
								<td colspan="3">
								           <table class="table table-striped">
										  <thead>
											<tr>
											  <th>Register</th>
											  <th>Date</th>
											  <th>Barrels</th>
											</tr>
										  </thead>
										  <tbody>';
						
											foreach ($totalsreg as $regdata) { 
												
												if ($regdata['calculation_date'] == $total['calculation_date']) {
													
													echo '<tr>
													  <td>'.$regdata['register_number'].'</td>				
													  <td>'.formatDate($regdata['calculation_date']).'</td>
													  <td>'.$regdata['sales_calculation'].'</td>
													</tr>';
												}
													

										  	
											}
												
						echo				 '</tbody>
										 </table>
								
								</td>
							</tr>';
						}
			   
			   ?>
              </tbody>
            </table>
          </div><?php */?>
          
          
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1&d=<?php echo $_GET['d']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2&d=<?php echo $_GET['d']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3&d=<?php echo $_GET['d']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>


        <?php  } ?>  

        <?php  if ($_GET['s'] == 2) { 
							 
	
					///////water
					$totals_water = getTotalsArrayWater($_GET['d'],46);
					$totalsreg_water = getTotalsByRegisterWater($_GET['d'],46);
							  
							  
			  ?>  
        
           <div id="chart"></div>


          <h2 class="sub-header">Production</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Barrels</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
	
					if ($_GET['d'] == 239) {
						$tanklevel = 'Hyden 47-38 WA 1H';
					} else if ($_GET['d'] == 241) { 
						$tanklevel = 'Broughton-Wise 18-19 WA 1H';
					} else if ($_GET['d'] == 244) { 
						$tanklevel = 'Oldham Trust 4051WA';
					}
			   
			   		foreach ($totals_water as $key=>$total) {  
			   			echo '<tr>
							  <td>'.$tanklevel.'</td>						  
							  <td>'.formatDate($total['calculation_date']).'</td>
							  <td>'.$total['totprod'].'</td>
							</tr>';
						
						
						echo '<tr id="water'.$key.'" class="collapse">
								<td colspan="3">
								           <table class="table table-striped">
										  <thead>
											<tr class="warning">
											  <th>Tank</th>
											  <th>Date</th>
											  <th>Barrels</th>
											</tr>
										  </thead>
										  <tbody>';
						
											foreach ($totalsreg_water as $regdata) { 
												
												if ($regdata['calculation_date'] == $total['calculation_date']) {
													
													echo '<tr>
													    <td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1#'.$regdata['register_number'].'">'.$regdata['register_number'].' - '.getRegisterName($_GET['d'],$regdata['register_number'],46).'</a></td>					
													  <td>'.formatDate($regdata['calculation_date']).'</td>
													  <td>'.$regdata['water_production_calculation'].'</td>
													</tr>';
												}
													

										  	
											}
												
						echo				 '</tbody>
										 </table>
								
								</td>
							</tr>';
						
					}
			   ?>
              </tbody>
            </table>
          </div> 

         <?php /*?> <h2 class="sub-header">Disposed</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Barrels</th>
                </tr>
              </thead>
              <tbody>
              <?php 
			   
			   		foreach ($totals_water as $key=>$total) {  
			   			echo '<tr>
							  <td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#trucked'.$key.'" aria-hidden="true" title="Show Detail"></i> '.getDeviceName($_GET['d'],46).'</td>						  
							  <td>'.formatDate($total['calculation_date']).'</td>
							  <td>'.$total['tottrucked'].'</td>
							</tr>';
						
						
						echo '<tr id="trucked'.$key.'" class="collapse">
								<td colspan="3">
								           <table class="table table-striped">
										  <thead>
											<tr>
											  <th>Register</th>
											  <th>Date</th>
											  <th>Barrels</th>
											</tr>
										  </thead>
										  <tbody>';
						
											foreach ($totalsreg_water as $regdata) { 
												
												if ($regdata['calculation_date'] == $total['calculation_date']) {
													
													echo '<tr>
													  <td>'.$regdata['register_number'].'</td>				
													  <td>'.formatDate($regdata['calculation_date']).'</td>
													  <td>'.$regdata['water_trucked_calculation'].'</td>
													</tr>';
												}
													

										  	
											}
												
						echo				 '</tbody>
										 </table>
								
								</td>
							</tr>';
						
					}
			   ?>
              </tbody>
            </table>
          </div><?php */?>

          
             <div id="chart_modal" title="Chart Types" style="display: none;">
							   
							   
  </div>

        <?php  } ?>  



        <?php  if ($_GET['s'] == 3) { ?>  

        <div id="chart"></div>


          <h2 class="sub-header">Well Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Mcf</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
			   
/*			   		foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td>North Doyle Penn</td>
							  <td>'.formatDate($total['total_date']).'</td>
							  <td>'.$total['total_BOPD'].'</td>
							</tr>';
					}*/
			   
			   ?>
              </tbody>
            </table>
          </div>

          <h2 class="sub-header">Check Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Mcf</th>
                </tr>
              </thead>
              <tbody>
               <?php 
			   
/*			   		foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td>North Doyle Penn</td>
							  <td>'.formatDate($total['total_date']).'</td>
							  <td>'.$total['total_BOSold'].'</td>
							</tr>';
					}*/
			   
			   ?>
              </tbody>
            </table>
           </div>

          <h2 class="sub-header">Sales Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Mcf</th>
                </tr>
              </thead>
              <tbody>
               <?php 
			   
/*			   		foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td>North Doyle Penn</td>
							  <td>'.formatDate($total['total_date']).'</td>
							  <td>'.$total['total_BOSold'].'</td>
							</tr>';
					}
*/			   
			   ?>
              </tbody>
            </table>
		</div>

          <h2 class="sub-header">Flare Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Mcf</th>
                </tr>
              </thead>
              <tbody>
               <?php 
			   
/*			   		foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td>North Doyle Penn</td>
							  <td>'.formatDate($total['total_date']).'</td>
							  <td>'.$total['total_BOSold'].'</td>
							</tr>';
					}
*/			   
			   ?>
              </tbody>
            </table>
          </div>
          
          
             <div id="chart_modal" title="Chart Types" style="display: none;">
							   
							   
  </div>

        <?php  } ?>  



        <?php  if ($_GET['s'] == 4) { 
							  
							  
				$pressures = getLastReadingsArrayPressure($_GET['d'],46); 
	
				//$gauge = getGaugeSettings($_GET['d']); 
				//echo "<pre>";
				//print_r($pressures);
							  
		 ?>  
	
	
	


          <h2 class="sub-header">Pressure Readings</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
				  <th>Date</th>
				  <th>Register</th>
				  <th>Reading</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 

					foreach ($pressures as $key=>$pressure) {  
						
						
						
			   			echo '<tr>
							<td>'.formatTimestampOffset($pressure['register_date'],5).'</td>
							  <td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$pressure['register_number'].'">'.$pressure['register_number'].' - '.getRegisterName($_GET['d'],$pressure['register_number'],46).'</a></td>	

							  <td>'.$pressure['register_reading'].'</td>

							</tr>';
					
					}
			   ?>
              </tbody>
            </table>
          </div>
          
          
							  <div id="settings_modal" title="Pressure Gauge Settings" style="display: none;">
							  	
							  	
							  	          <div class="table-responsive">
											<table class="table table-striped" style="width:600px;">

											  <tbody>
												  
												<tr>
												<td style="background-color:#55BF3B; ">Good</td>
												<td><input type="text" class="form-control modalfield" name="gauge_start_good" data-id="<?php echo $_GET['d']; ?>" placeholder="Start Value" value="<?php echo $gauge['gauge_start_good']; ?>"></td>
												<td><input type="text" class="form-control modalfield" name="gauge_end_good" data-id="<?php echo $_GET['d']; ?>" placeholder="End Value" value="<?php echo $gauge['gauge_end_good']; ?>"></td>
												</tr>

												<tr>
												<td style="background-color:#DDDF0D; ">Warning</td>
												<td><input type="text" class="form-control modalfield" name="gauge_start_warn" data-id="<?php echo $_GET['d']; ?>" placeholder="Start Value" value="<?php echo $gauge['gauge_start_warn']; ?>"></td>
												<td><input type="text" class="form-control modalfield" name="gauge_end_warn" data-id="<?php echo $_GET['d']; ?>" placeholder="End Value" value="<?php echo $gauge['gauge_end_warn']; ?>"></td>
												</tr>

												<tr>
												<td style="background-color:#DF5353; ">Bad</td>
												<td><input type="text" class="form-control modalfield" name="gauge_start_bad" data-id="<?php echo $_GET['d']; ?>" placeholder="Start Value" value="<?php echo $gauge['gauge_start_bad']; ?>"></td>
												<td><input type="text" class="form-control modalfield" name="gauge_end_bad" data-id="<?php echo $_GET['d']; ?>" placeholder="End Value" value="<?php echo $gauge['gauge_end_bad']; ?>"></td>
												</tr>

											  </tbody>
											</table>
										  </div>
							  								  	
							  </div>
							  
							  
							   <div id="chart_modal" title="Chart Types" style="display: none;">
							   
							   
							   </div>

        <?php  } ?>  
        
        
       



        <?php  if ($_GET['s'] == 5) { ?> 
        
        
        <h2 class="sub-header">ESP Analysis</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Run Status</th>
                  <th>Downhole Intake Pressure</th>
                  <th>Downhole Motor Temperature(F)</th>
                  <th>Downhole Temperature (F)</th>
                  <th>Downhole Current Phases</th>
				  </tr>
              </thead>
              <tbody>
               
               <?php 

					foreach ($pressures as $key=>$pressure) {  
						
			   		echo '<tr>
							  <td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1#'.$pressure['register_number'].'">'.$pressure['register_number'].' - '.getRegisterName($_GET['d'],$pressure['register_number'],46).'</a></td>	
							  <td>'.formatDate($pressure['register_date']).'</td>
							  <td>'.formatTimeOffset($pressure['register_date'],5).'</td>
							  <td>'.$pressure['register_reading'].'</td>
							  <td><a href="#" id="dialsettings">Click here</a></td>
							</tr>';
					
					}
			   ?>
              </tbody>
            </table>
          </div>
	
	
	
	 	<?php  } ?>  
        
        
        
          <?php  if ($_GET['s'] == 7) { 
	
	
					$today = date('Y-m-d', time() - 21600);
	 				$tomorrow = date('Y-m-d H:i:s', strtotime("+1 days") - 21600);
							  
					$avgreading = getDailyProductionEstimate($_GET['d'],46,$today,$tomorrow);
			?> 
          
           <h2 class="sub-header">Daily Oil Production</h2>
          
          <div id="chart"></div>
          
          	<h4>Actual/Projected Daily Production</h4>
          	
          	<div class="alert alert-success" role="alert">
          			<strong>Average Per Hour:</strong> <?php echo $avgreading; ?><br>
          			<strong>Daily Projection:</strong> <?php echo $avgreading*24; ?><br>
          	</div>
          	
          	<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Hour</th>
                  <th>Average Prod Per Hour/Per Reading (BBL)</th>
                  <th>Prod Hour (BBL)</th>
				  </tr>
              </thead>
              <tbody>
          		
          	
          	 <?php 
					
							  
					$array = getDailyProductionVolumes($_GET['d'],46,$today,$tomorrow);

	
					foreach ($array as $calc) {
						
						echo '<tr>
							  <td>'.($calc['prodhour']-6).'</td>
							  <td>'.$calc['prodavg'].'</td>
							  <td>'.$calc['prodsum'].'</td>
							</tr>';
						
						
					}
			?> 
         
         
                 </tbody>
            </table>
          </div>
          
          
          <?php  } ?>  
          
          
          
        
         <?php  if ($_GET['b'] == 1) { 
							  
							  
		 ?>  
		
		
			<h2 class="sub-header">Digital Log Book [<a href="#" id="entryview">entry view</a>]</h2>
			
			<p><strong>Add New Field </strong> - Please choose a type: </p>
			
			 
			
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="1"> This field will compare entered log book values to auto calculated values
				  </label>
				</div>
				
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="2"> This field will be a calculation of up to 3 other fields
				  </label>
				</div>
				
			  <div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="3"> This field is for entry only
				  </label>
				</div>
				

					
				<div id="log_field1" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Comparison">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
						<?php echo  getLogCompareField(); ?> 
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				
				<div id="log_field2" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Calculation">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Calculated Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Calculated Field Name"> = 
						
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name"> 
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
									
						
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				<div id="log_field3" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Entry Only">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	
		
				
				<div id="entry_modal" title="Log Book Entry" style="display: none;">
						
					<h4>Enter Reading</h4>
					
					   <div class="table-responsive">
						<table class="table table-striped" style="width:600px;">

						  <tbody>

						  <tr>
							  <td>Date</td>
							  <td><input type="text" class="form-control datepicker" name="reading_date" placeholder="Enter Date"></td>
						</tr>
						
						<tr>
							  <td>Time</td>
							  <td><input type="time" class="form-control" name="reading_time" placeholder="Enter Time"></td>
						</tr>
						
						
						 <?php 

								$fields = getLogBookFieldsByDevice($_GET['d'],46);

								//print_r($fields);

							foreach ($fields as $key=>$field) {  

							echo '<tr>
									  <td>'.$field['log_book_field'].'</td>
									  <td><input type="text" class="form-control" name="'.$field['logfieldID'].'" placeholder="Enter Value"></td>
								</tr>';

							}
					   ?>

						              </tbody>
            </table>
          </div>
						<p>Comments</p>

				</div>


        
         
          <div class="table-responsive" style="margin-top: 30px;">
           
            <p><strong>Current Fields </strong></p>
           
            <table class="table table-striped" style="width:700px;">

              <tbody>
              
<!--              <tr>
				  <td>Date</td>
				  <td><input type="text" class="form-control datepicker" name="'.$field['logfieldID'].'" placeholder="Enter Date"></td>
			</tr>-->
               
               <?php 
					
						$fields = getLogBookFieldsByDevice($_GET['d'],46);
						
						//print_r($fields);

					foreach ($fields as $key=>$field) {  
						
						if ($field['log_field_type'] == 'Comparison') {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>Compare to: '.$field['log_book_comparison'].'</td>
						   </tr>';
	
						} else if ($field['log_field_type'] == 'Calculation') {
							
							$calcfields = unserialize($field['log_book_calcs']);
							$calcops = unserialize($field['log_book_math']);
							$calcstring = '';
							
							foreach ($calcfields as $key=>$calc) {
								
								$calcstring .= $calc.' '.$calcops[$key].' ';
							}
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>'.$calcstring.'</td>
						   </tr>';

					
						} else  {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>&nbsp;</td>
						   </tr>';
														
						}
						
			   		 
					
					}
			   ?>
              </tbody>
            </table>
          </div>
          
          
          
          	   <div id="log_modal" title="Edit Log Book Field" style="display: none;">
          	   
          	   	<form method="post" action="process/edit_log_book_field.php"  class="form-inline">
          	   	<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
					<input type="hidden" name="logfieldID" id="logfieldID_edit">
						  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" id="log_book_field_edit" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit</button>
					</form>
							   
							   
			</div>

        <?php  } ?>  




        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   


	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    

    <?php  if ($_GET['fm']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if ($_GET['s'] or $_GET['g'] or !isset($_GET['d'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
				  
		
					   		
		
 					   		$( "#newlink" ).click(function() {	

								$( "#add_modal" ).dialog( "open" );	
							});

							$( "#newlinkEXTRA" ).click(function() {	

								$( "#add_modalEXTRA" ).dialog( "open" );	
							});

							$( "#newlink2" ).click(function() {	

								$( "#add_modalWIDE" ).dialog( "open" );	
							});


							$( "#add_modal" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalEXTRA" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalWIDE" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 950,
							  modal: true,

							});
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   <?php } else if ($_GET['s'] == 7 ) { ?>
				   
				   	var start = moment().subtract(1, 'days');
					 var end = moment();
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(29, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s']  && !$_GET['tb']  ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   	<?php } else if ($_GET['tb']  ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&tb=<?php echo $_GET['tb']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	
 	
 	<?php  if (!$_GET['m'] && !$_GET['c'] && !$_GET['b'] && !$_GET['f'] ) { ?>  
   
   	<?php  if ($_GET['s'] != 4 && $_GET['s'] != 7) { ?>  
   	
    	<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					   
					     chart: {
								type: '<?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { 
							 		
												echo 'line';
										
											} else if ($_SESSION['chart_type'] == 2 )  {
	
												echo 'waterfall';

											
											} else if ($_SESSION['chart_type'] == 3 )  {

												echo 'bar';
											}
							 	?>', 

							},
					   
                    title: {
                        text: '<?php 	if (isset($_GET['d']) && $_GET['d'] == 239 ) {
	
											echo 'Hyden 47-38 WA 1H'; 
									
									
										} else if (isset($_GET['d']) && $_GET['d'] == 241 ) {
	
											echo 'Broughton-Wise 18-19 WA 1H'; 
									
										} else if (isset($_GET['d']) && $_GET['d'] == 244 ) {
	
											echo 'Oldham Trust 4051WA'; 
									
										} else  if (isset($_GET['d'])  && $_GET['d'] == 250)  {
				
												echo 'Oldham Trust 3871WA';

										} else  if (isset($_GET['d'])  && $_GET['d'] == 254)  {

												echo 'Oldham Trust 4058LS';
									
									
	
										} else {

											echo getDeviceName($_GET['d'],46);

										}
						
						
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)/PSI'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('process/time_series_production_46.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, <?php /*?>{
                        name: 'Oil Sales',
                        data: <?php include('process/time_series_sales_46.php'); ?>,
						color: '#267326',
						tooltip: {
							valueSuffix: ' BBL'
						 }
					},<?php */?> {
                        name: 'Water Production',
                        data: <?php include('process/time_series_water_46.php'); ?>,
					    color: '#4d79ff',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, <?php /*?>{
                        name: 'Water Disposed',
                        data: <?php include('process/time_series_water_inj_46.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, <?php */?>{
                        name: 'Gas (Prev Day)',
                        data: <?php include('process/time_series_gas_46.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' MCF'
						 }
                   }
							 
					<?php 	if (isset($_GET['d'])  && $_GET['d'] != 999 ) {	?>	 
							 , {
                        name: 'Tubing Pressure',
                        data: <?php include('process/time_series_tubing_46.php'); ?>,
						color: '#ffa31a',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                   }, {
                        name: 'Casing Pressure',
                        data: <?php include('process/time_series_casing_46.php'); ?>,
						color: '#b35900',
						tooltip: {
							valueSuffix: ' PSI'
						}
                   }
							<?php  } ?> 
							
							]
                });
				   
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
		
		
		<?php  if ($_GET['s'] == 7) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x'
				},
					   
                    title: {
                        text: 'Daily Production Trending',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)/PSI'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Hourly Oil Production',
                        data: <?php include('process/time_series_production_all_46.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }		
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
   
    
      <?php  if ($_GET['m'] == 1) { ?>  

				<?php include('includes/google_maps.php'); ?>
                
        <?php  } ?>  

		<?php include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

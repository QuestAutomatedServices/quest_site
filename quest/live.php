<?php 

	session_start();
	error_reporting(0);
	
	if (!isset($_SESSION['auth'])) {
		 session_destroy();
		 header("location: login.php");
		 exit();
	} else if($_SESSION['auth'] != 1){
		 session_destroy();
		 header("location: login.php");
		 exit();
	}

	if($_SESSION['auth'] == 1){
		if(isset($_SESSION['user_accountID'])){
			unset($_SESSION['user_accountID']);
		}
	}

include("functions/functions.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">
    
    <link href="datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet">
    
    <script src="https://use.fontawesome.com/96c3b1dce4.js"></script>

    <link rel="stylesheet" href="jquery/jquery-ui.min.css"/>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand"><?php echo $_SESSION['user_name']; ?>, Welcome to Quest Automated Services Admin Panel!</a>
       
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">

            <li class="dropdown">
              <a href="support.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="caret"></span>Home</a>
              <ul class="dropdown-menu">
					<li>Home</li>

              </ul>
            </li>
 		
			  
            
   
		
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          
         <ul class="nav nav-sidebar">
                  
            <?php 
					
			 $array = array('Usable Accounts','API Status','Alarm Demo','Demo Users','Admin Level Users'); 

			 
			 foreach($array as $fun) {
				 if($_GET['f'] == 'get'.str_replace(" ","",$fun)){
					 ?><li class="active"><a href="index.php?f=get<?php echo str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php
				 }elseif (!isset($_GET['f'])){
					 
					 if($fun == 'Usable Accounts'){
						 ?><li class="active"><a href="index.php"><?php echo $fun ?></a></li><?php
					 }else{
						?><li><a href="index.php?f=get<?php echo str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php 
					 }
				 
				 }else{
				 	?><li><a href="index.php?f=get<?php echo str_replace(" ","",$fun) ?>"><?php echo $fun ?></a></li><?php
				 }
			 }
			 ?>

          				<li><a href="process/logout.php">Logout</a></li>
						<li><a href="../support/support.php">Support</a></li>
						<li><a href="../alarms/alarms.php">Alarms</a></li>
        				 <li><a href="live.php">Live Data Demo</a></li>
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
            <div class="row">
            
           	<h1>Live Data Demo</h1><hr>
            
				<div id="container"></div>
                
            
            </div>







        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script
			  src="https://code.jquery.com/jquery-1.12.4.min.js"
			  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
			  crossorigin="anonymous"></script>
  
	<script>
		
		var chart; // global
		
		/**
		 * Request data from the server, add it to the graph and set a timeout 
		 * to request again
		 */
		function requestData() {
			$.ajax({
				url: 'live-server-data.php',
				success: function(point) {
					var series = chart.series[0],
						shift = series.data.length > 20; // shift if the series is 
														 // longer than 20

					// add the point
					chart.series[0].addPoint(point, true, shift);

					// call it again after one second
					setTimeout(requestData, 1000);    
				},
				cache: false
			});
		}
		
		
		document.addEventListener('DOMContentLoaded', (function() {
			chart = Highcharts.chart('container', {
				chart: {
					type: 'spline',
					events: {
						load: requestData
					}
				},
				title: {
					text: 'Live Oil Volume'
				},
				xAxis: {
					type: 'datetime',
					tickPixelInterval: 150,
					maxZoom: 20 * 1000,
					labels: {
						format: '{value:%b %e %y %I:%M:%S}'
					  },
				},
				yAxis: {
					minPadding: 0.2,
					maxPadding: 0.2,
					title: {
						text: 'Value',
						margin: 80
					}
				},
				series: [{
					name: 'Current Oil Level in BBLs',
					data: []
				}]
			});        
		}));
		
	</script>
    
  </body>
</html>

<?php
include("functions/functions.php");
// Set the JSON header
header("Content-type: text/json");

/*// The x value is the current JavaScript time, which is the Unix time multiplied 
// by 1000.
$x = time() * 1000;
// The y value is a random number
$y = rand(0, 100);*/

$database = "account_300";
$db = connectTWO($database);

$stmt = $db->query("SELECT * FROM `tbl_live_data` ORDER BY readingID DESC LIMIT 1 ");
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);


$value = $results[0]['register_reading'];
$x = (strtotime($results[0]['register_date']) - 18000) * 1000;
// Create a PHP array and echo it as JSON
$ret = array($x, floatval(number_format($value, 2, '.', ''))); 

echo json_encode($ret);
?>
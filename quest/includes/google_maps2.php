<?php //$locations = getAllLocationsGeo(42); 

//print_r($locations);
?>
<script>
function initMap() {
	
	var locations = <?php echo getAllLocationsGeo(42); ?>;
	
 	var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(locations[3][1], locations[3][2]),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent('<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
			'<p><img src="images/logo.jpg"></p>'+
            '<h1 id="firstHeading" class="firstHeading">' + locations[i][0] + '</h1>'+
            '<div id="bodyContent">'+
            '<p>' + locations[i][3] + '</p>'+
            '</div>'+
            '</div>');
          infowindow.open(map, marker);
        }
      })(marker, i));
	}
}	
	
</script>
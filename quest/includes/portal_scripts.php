<script>
$(function() {
	

	$('.collapse').on('hidden.bs.collapse', function () {
		  //var clicked = $(this).attr("id");
   			//alert("collapse " + clicked);
		})

	$("#2").collapse('show');
	$("#6").collapse('show');

	$(".datepicker").datepicker();
	
		
	$(".timepicker").timepicker({ 'step': 60, 'timeFormat': 'H:i' });
	
	$('[title]').qtip();

	$('.configsel').change(function(){

		config = $(this).val();

		reg = $(arguments[0].target).attr("data-id");	
		device = $(arguments[0].target).attr("data-type");	

		$.ajax({

			url: "process/update_config.php",
			type: 'GET',
			data: 'reg='+reg+'&device='+device+'&config='+config,



		});

	
	});
	
	
	$( "#entryview" ).click(function() {	
			$( "#entry_modal" ).dialog( "open" );	
	});


	$( "#entry_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
		
	  	close: function() {
				location.reload();
		  }
	 
	});	
	
	<?php if ($_GET['d'] && $_GET['bt']) { ?>
	
	
	$( ".gauge_diary" ).click(function() {	
		
			var ddate = $(arguments[0].target).attr("data-date");	
		
			$( "#diarymodaldate" ).val(ddate);
		
			$.ajax	 ({
				 type: "GET",
				 url: "process/get_tank_diary_entry.php",
				 data: "date="+ddate+"&d="+<?php echo $_GET['d']; ?>+"&bt="+<?php echo $_GET['bt']; ?>,
				 cache: false,
				 success: function(html) {

					$( "#diary_text_edit" ).val(html);	
				}	
			});
		

			$( "#diary_modal" ).dialog( "open" );
			
	});
 

	$( "#diary_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	
	
	<?php } ?>
	
		$( ".goal_link" ).click(function() {	
			
		
			var goalnum = $(arguments[0].target).attr("data-num");	
		
			$( "#goalnumfield" ).val(goalnum);
		
			$.ajax	 ({
				 type: "GET",
				 url: "process/get_goal_value.php",
				 data: "num="+goalnum,
				 cache: false,
				 success: function(data) {

					$( "#goal_value_edit" ).val(data);	
				}	
			});
		

			$( "#goal_modal" ).dialog( "open" );
			
	});
 

	$( "#goal_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	
	
	
	
	$( ".prodsales" ).click(function() {	
			$( "#mod_modal" ).dialog( "open" );
			
	});
	
	$( "#mod_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	

	
	$( ".goalset" ).click(function() {	
			$( "#goal_modal" ).dialog( "open" );
			
	});
	
	$( "#goal_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	
	
	
	$('.logfield').change(function(){

		num = $(this).val();
		
		if (num == 1) {
			
			$( "#log_field1" ).show();	
			$( "#log_field2" ).hide();	
			$( "#log_field3" ).hide();	
			
		} else if (num == 2) {
			
			$( "#log_field1" ).hide();	
			$( "#log_field2" ).show();	
			$( "#log_field3" ).hide();	
			
		} else if (num == 3) {
			
			$( "#log_field1" ).hide();	
			$( "#log_field2" ).hide();	
			$( "#log_field3" ).show();	
		}


	
	});
	
	$('.regfactor').change(function(){

		config = $(this).val();
		reg = $(arguments[0].target).attr("data-id");	
		device = $(arguments[0].target).attr("data-type");	

		$.ajax({

			url: "process/update_factor.php",
			type: 'GET',
			data: 'reg='+reg+'&device='+device+'&config='+config,



		});

	
	});
	
	
	
		
	$('.reglabel').change(function(){

		config = $(this).val();
		reg = $(arguments[0].target).attr("data-id");	
		device = $(arguments[0].target).attr("data-type");	

		$.ajax({

			url: "process/update_label.php",
			type: 'GET',
			data: 'reg='+reg+'&device='+device+'&config='+config,



		});

	
	});
	
	
	$('.timepicker').change(function(){

		config = $(this).val();
		field = $(arguments[0].target).attr("name");	
		
		$.ajax({

			url: "process/update_time.php",
			type: 'GET',
			data: 'field='+field+'&config='+config,



		});

	
	});
	
	
	$('.regprodsell').click(function(){

		config = $(this).val();
		reg = $(arguments[0].target).attr("data-id");	
		device = $(arguments[0].target).attr("data-type");


		$.ajax({

			url: "process/update_prod_sell.php",
			type: 'GET',
			data: 'reg='+reg+'&device='+device+'&config='+config,



		});

	
	});
	
	
	
	$( "#dialsettings" ).click(function() {	
			$( "#settings_modal" ).dialog( "open" );	
	});


	$( "#settings_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
		
	  	close: function() {
				location.reload();
		  }
	 
	});	
	
	
	$(".modalfield").change(function(){

		config = $(this).val();
		field = $(arguments[0].target).attr("name");	
		d = $(arguments[0].target).attr("data-id");


		$.ajax({

			url: "process/update_gauge.php",
			type: 'GET',
			data: 'field='+field+'&config='+config+'&d='+d,

		});

	
	});
	
	
	$( "#choosechart" ).click(function() {	
			$( "#chart_modal" ).dialog( "open" );	
	});


	$( "#chart_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 1050,
	  modal: true,

	});	
	
	
	
	$( ".log_field_edit" ).click(function() {	
		
			id = $(arguments[0].target).attr("data-id");
		
			$.ajax	 ({
				 type: "GET",
				 url: "process/get_log_book_field.php",
				 data: "id="+id,
				 cache: false,
				 success: function(html) {

					$( "#log_book_field_edit" ).val(html);	
				}	
			});
		
			$( "#logfieldID_edit" ).val(id);
			
		
			$( "#log_modal" ).dialog( "open" );	
	});


	$( "#log_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	

});
</script>
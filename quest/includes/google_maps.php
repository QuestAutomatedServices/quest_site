<?php $location = getLocationsGeo($_GET['d'],42); 

//print_r($location);
?>
<script>
function initMap() {
	
 var myLatLng = {lat: <?php echo $location['latitude']; ?>, lng: <?php echo $location['longitude']; ?>};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: myLatLng
  });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
			'<p><img src="images/logo.jpg"></p>'+
            '<h1 id="firstHeading" class="firstHeading"><?php echo $location['reference_name']; ?></h1>'+
            '<div id="bodyContent">'+
            '<p><?php echo $location['driving_directions']; ?></p>'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
	title: '<?php echo $location['reference_name']; ?>'
  });


 marker.addListener('click', function() {
          infowindow.open(map, marker);
        });
}
</script>
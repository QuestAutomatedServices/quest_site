<?php 

include('../functions/functions.php');

$array = '';

$database = "account_46";
$db = connectTWO($database);

if ($_GET['start']) {
	$stmt = $db->query("SELECT calculation_date, sum(water_trucked_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID = 239 AND calculation_type = 2 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' GROUP BY calculation_date ORDER BY calculation_date");
} else {
	$stmt = $db->query("SELECT calculation_date, sum(water_trucked_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID = 239 AND calculation_type = 2  GROUP BY calculation_date ORDER BY calculation_date DESC LIMIT 7 ");
}
	

$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($results as $row) {
	
	$array[] = array((strtotime($row['calculation_date'])*1000),intval($row['totsales']));	
}

echo json_encode($array);

?>
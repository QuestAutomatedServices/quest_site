<?php 

set_time_limit(2000);
include("/var/www/html/quest/functions/functions.php");

$regdates = date_range('2018-01-20',date('Y-m-d'));
rsort($regdates);
 
//truncateTable('tbl_water_calc',24);
$db = connectTWO("account_24");

$i= 0;

foreach ($regdates as $pdate) {  
	
	$totalbwpd = 0;
	
		$totalbwpd += round((getRegReadingByDate(214,182,$regdates[$i],24) - getRegReadingByDate(214,182,$regdates[$i+1],24)),2);
		$totalbwpd += round((getRegReadingByDate(214,214,$regdates[$i],24) - getRegReadingByDate(214,214,$regdates[$i+1],24)),2);
		$totalbwpd += round((getRegReadingByDate(214,240,$regdates[$i],24) - getRegReadingByDate(214,240,$regdates[$i+1],24)),2);
		$totalbwpd += round((getRegReadingByDate(214,253,$regdates[$i],24) - getRegReadingByDate(214,253,$regdates[$i+1],24)),2);
	
	
	$totalinj = 0;
											
		$totalinj += round((getRegReadingByDate(214,223,$regdates[$i],24) - getRegReadingByDate(214,223,$regdates[$i+1],24)),2);
		$totalinj += round((getRegReadingByDate(214,233,$regdates[$i],24) - getRegReadingByDate(214,233,$regdates[$i+1],24)),2);

		$stmt = $db->query("DELETE FROM tbl_water_calc WHERE calc_date = '$pdate' ");
		$stmt = $db->query("INSERT INTO tbl_water_calc (water_produced, water_injected, calc_date) VALUES ($totalbwpd, $totalinj, '$pdate') ");
		
	$i++;
}



 
function getRegisterValues($reg,$date,$date2) {
	
	$db = connectTWO("account_24");
		
	$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 214 AND date(register_date) = '$date' AND hour(register_date) = '12' ORDER BY register_date DESC LIMIT 1 ");
	
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 214 AND register_date BETWEEN '".$results[0]['register_date']."' AND '".$date2." 11:59:59' ORDER BY register_date DESC ");
	
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$array = '';
	
	foreach ($results as $row) {
		
		$array[] = $row;	
		
	}
	
	//echo "<pre>";
//	print_r($array);
	
	$production = 0;
	$sales = 0;
	$last = count($array) - 1;
	
	foreach ($array as $key=>$reading) {
		
		//If (New – Old) > 0, add to production
		//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

		if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
			$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
			$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
			$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		}
			 	
		
	}
		
		
	$prevday = ($array[$last]['register_reading']);
	$today = number_format(($array[$last]['register_reading']+$production),2);

	return array($array,$prevday,round($production,2),$sales,$today,$array[0]['register_reading']);
	
	
}

function getRegisterValue($reg,$sub,$date) {
	
	$db = connectTWO("account_24");
 
 $stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 214 AND date(register_date) = '$date' AND hour(register_date) = '11' ORDER BY register_date DESC LIMIT 1 ");
 $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
 
 if ($sub) {
  $stmt2 = $db->query("SELECT * FROM tbl_register_history WHERE register_number = $sub AND register_deviceID = 214 AND date(register_date) = '$date' AND hour(register_date) = '11' ORDER BY register_date DESC LIMIT 1 ");
  $results2 = $stmt2->fetchAll(PDO::FETCH_ASSOC);

  return ($results[0]['register_reading'] - $results2[0]['register_reading']);
  
 } else {
  	return $results[0]['register_reading'];
 }
 
}


$current = date('Y-m-d');
$prev = date('Y-m-d', strtotime("-1 day"));

//$current = '2018-02-06';
//$prev = '2018-02-05';

$totalBOSold = 0;

$forty = getRegisterValues(42,$prev,$current);


$fifty = getRegisterValues(52,$prev,$current);
$ninety = getRegisterValues(92,$prev,$current);
$oneten = getRegisterValues(112,$prev,$current);


$onethirty = getRegisterValues(132,$prev,$current);
$oneforty = getRegisterValues(142,$prev,$current);
$onesixty = getRegisterValues(162,$prev,$current);

$totalBOPD20 = (($forty[2] + $fifty[2]  + $ninety[2]  + $oneten[2]  + $onethirty[2]  + $oneforty[2] ) * 20);
$totalBOPD14 = ($onesixty[2] * 14);
$totalBOPD =  round(($totalBOPD14+$totalBOPD20),2);


$totalBOSold20 = (($forty[3] + $fifty[3]  + $ninety[3]  + $oneten[3]  + $onethirty[3]  + $oneforty[3] ) * 20);
$totalBOSold14 = ($onesixty[3] * 14);
$totalBOSold =  round((abs($totalBOSold20+$totalBOSold14)),2);

///// INDIVIDUAL REGISTER TOTALS

$fortyBOPD = round((abs($forty[2]*20)),2);
$fortyBOSold = round((abs($forty[3]*20)),2);

$fiftyBOPD = round((abs($fifty[2]*20)),2);
$fiftyBOSold = round((abs($fifty[3]*20)),2);

$ninetyBOPD = round((abs($ninety[2]*20)),2);
$ninetyBOSold = round((abs($ninety[3]*20)),2);

$onetenBOPD = round((abs($oneten[2]*20)),2);
$onetenBOSold = round((abs($oneten[3]*20)),2);

$onethirtyBOPD = round((abs($onethirty[2]*20)),2);
$onethirtyBOSold = round((abs($onethirty[3]*20)),2);

$onefortyBOPD = round((abs($oneforty[2]*20)),2);
$onefortyBOSold = round((abs($oneforty[3]*20)),2);

$onesixtyBOPD = round((abs($onesixty[2]*14)),2);
$onesixtyBOSold = round((abs($onesixty[3]*14)),2);

// FLOW METER INDIVIDUAL LEVELS

// ONLY INSERTING NUBBS
$nubbsMeterBWPD = getRegisterValue(182,'',$current)-getRegisterValue(182,'',$prev);
//$B7MeterBWPD = getRegisterValue(214,'',$current)-getRegisterValue(214,'',$prev);
//$B1MeterBWPD = getRegisterValue(243,'',$current)-getRegisterValue(243,'',$prev);
//$B2MeterBWPD = getRegisterValue(253,'',$current)-getRegisterValue(253,'',$prev);

//$totalBWPD = $nubbsMeterBWPD + $B7MeterBWPD + $B1MeterBWPD + $B2MeterBWPD;

// WATER INJECTION LEVEL
$northInj = getRegisterValue(223,'',$current)-getRegisterValue(223,'',$prev);
$middleInj = getRegisterValue(230,'',$current)-getRegisterValue(230,'',$prev);

$totalWaterInj = $northInj + $middleInj;

$stmt = $db->query("DELETE FROM tbl_daily_totals WHERE total_date = '$current' ");

$stmt = $db->prepare("INSERT INTO tbl_daily_totals (total_date, total_BOPD, total_BOSold, 42_BOPD, 42_BOSold, 52_BOPD, 52_BOSold, 92_BOPD, 92_BOSold, 112_BOPD, 112_BOSold, 132_BOPD, 132_BOSold, 142_BOPD, 142_BOSold, 162_BOPD, 162_BOSold, 182_BWPD, total_WI) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");

$stmt->execute(array($current,$totalBOPD,$totalBOSold,$fortyBOPD,$fortyBOSold,$fiftyBOPD,$fiftyBOSold,$ninetyBOPD,$ninetyBOSold,$onetenBOPD,$onetenBOSold,$onethirtyBOPD,$onethirtyBOSold,$onefortyBOPD,$onefortyBOSold,$onesixtyBOPD,$onesixtyBOSold,$nubbsMeterBWPD,$totalWaterInj));





?>
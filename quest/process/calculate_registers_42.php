<?php 
include("/var/www/html/quest/functions/functions.php");

$database = "account_42";

$db = connectTWO($database);

$dates = date_range('2017-06-01',date('Y-m-d'));


$oildevregs = array(224=>array(790,792,794,796),226=>array(790,792));

//truncateTable('tbl_register_calculations');


foreach ($oildevregs  as $device=>$regs) {
	
	foreach ($regs as $reg) {

			//echo $reg;
		
		foreach ($dates as $key=>$date) {

				$array = getRegisterReadingsByDate($device,$reg,$date,42);
		
			
				
				
				$production = 0;
				$factoredprod = 0;
			
				$sales = 0;
				$factoredsales = 0;
			
				$last = count($array) - 1;
			
				$startreading = $array[0];
				$endreading = $array[$last];
				
				$calculation = $startreading['register_reading']-$endreading['register_reading'];
			
				if ($reg != 794) {
					
					if ($calculation > 0) {
						$sales = $calculation;
						$factoredsales = $sales * 12 * 1.67;
					} else if ($calculation < 0) {
						$production = abs($calculation);
						$factoredprod = $production * 12 * 1.67;
					}
					
				} else {
					
					if ($calculation < 0) {
						$sales = abs($calculation);
						$factoredsales = $sales * 12 * 1.67;
					} else if ($calculation > 0) {
						$production = $calculation;
						$factoredprod = $production * 12 * 1.67;
					}
					
				}

			
			
				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $reg AND register_deviceID = $device AND calculation_date = '$date' AND calculation_type = 1 ");

				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($reg, $device, $factoredprod, $production, $factoredsales, $sales, '$date', 1) ");
 
/*				foreach ($array as $key=>$reading) {

					if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 5.00) {
						$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -5.00) {
						$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -5.00 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
						$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					}


				}*/

				//$factoredprod = ($production);
			  // $factoredsales = ($sales);
			  
/*			
				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $reg AND register_deviceID = $device AND calculation_date = '$date' AND calculation_type = 1 ");

				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($reg, $device, $factoredprod, $production, abs($factoredsales), abs($sales), '$date', 1) ");*/
			
		}

	}

}



$waterdevregs = array(226=>array(794));

//truncateTable('tbl_register_calculations');


foreach ($waterdevregs  as $device=>$regs) {
	
	foreach ($regs as $reg) {

			//echo $reg;
		
		foreach ($dates as $key=>$date) {

				$array = getRegisterReadingsByDate($device,$reg,$date,42);
		
			
				
				
				$production = 0;
				$factoredprod = 0;
			
				$sales = 0;
				$factoredsales = 0;
			
				$last = count($array) - 1;
			
				$startreading = $array[0];
				$endreading = $array[$last];
				
				$calculation = $startreading['register_reading']-$endreading['register_reading'];
			
					
					if ($calculation > 0) {
						$sales = $calculation;
						$factoredsales = $sales * 12 * 1.67;
					} else if ($calculation < 0) {
						$production = abs($calculation);
						$factoredprod = $production * 12 * 1.67;
					}

			
			
				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $reg AND register_deviceID = $device AND calculation_date = '$date' AND calculation_type = 2 ");


				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, water_production_calculation, water_trucked_calculation, calculation_date, calculation_type) VALUES ($reg, $device, $factoredprod, $factoredsales, '$date', 2) ");
 
/*				foreach ($array as $key=>$reading) {

					if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 5.00) {
						$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -5.00) {
						$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -5.00 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
						$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					}


				}*/

				//$factoredprod = ($production);
			  // $factoredsales = ($sales);
			  
/*			
				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $reg AND register_deviceID = $device AND calculation_date = '$date' AND calculation_type = 1 ");

				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($reg, $device, $factoredprod, $production, abs($factoredsales), abs($sales), '$date', 1) ");*/
			
		}

	}

}

?>
<?php 

//echo getcwd();

include("/var/www/html/quest/functions/functions.php");


$dates = date_range('2018-07-04',date('Y-m-d'));

$devices = getDevices(47);


//truncateTable('tbl_register_calculations',47);

foreach ($devices as $device) {
	
	$devid = $device['device_deviceID'];
	
	
	$db = connectTWO("account_47");
	
	$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_deviceID = $devid AND register_config = 'Oil - Production and Sales'");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	

	foreach($results as $data){
		

		$regnum = $data['register_number'];
		
		foreach ($dates as $key=>$date) {

				$array = getRegisterReadingsByDate($devid,$regnum,$date,47);

				$register = getRegisterConfig($devid,$regnum,47);

				$production = 0;
				$sales = 0;
				$last = count($array) - 1;
			
			


				foreach ($array as $key=>$reading) {

					//If (New – Old) > 0, add to production
					//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

					if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
						$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
						$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
						$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					}


				}

				$factoredprod = ($production*$register[0]['register_factor']);
			   $factoredsales = ($sales*$register[0]['register_factor']);
			 
			
				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $regnum AND register_deviceID = $devid AND calculation_date = '$date' AND calculation_type = 1 ");
			
				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($regnum, $devid, $factoredprod, $production, abs($factoredsales), abs($sales), '$date', 1) ");
			
			
		}

	}

	//////////only use certain registers for sales and production
	$db->query("UPDATE tbl_register_calculations SET production_calculation = NULL, production_calculation_in = NULL WHERE register_number != 81 AND register_number != 88  "); 
	$db->query("UPDATE tbl_register_calculations SET sales_calculation = NULL, sales_calculation_in = NULL WHERE register_number = 81 or register_number = 88 "); 
	

	$db = connectTWO("account_47");
	$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_deviceID = $devid AND register_config = 'Water - Tank Level Feet'");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	 
	foreach($results as $data){
				
		$regnum = $data['register_number'];
		

		
		foreach ($dates as $key=>$date) {

				$array = getRegisterReadingsByDate($devid,$regnum,$date,47);


				$register = getRegisterConfig($devid,$regnum,47);

				$production = 0;
				$sales = 0;
				$last = count($array) - 1;
			
			


				foreach ($array as $key=>$reading) {
					//print_r($array);
					//exit();


					if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
						$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
						$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
						$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					}


				}

			
			   $factoredprod = ($production*$register[0]['register_factor']);
			   $factoredsales = ($sales*$register[0]['register_factor']);
			

				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $regnum AND register_deviceID = $devid AND calculation_date = '$date' AND calculation_type = 2 ");
			

				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, water_production_calculation, water_trucked_calculation, calculation_date, calculation_type) VALUES ($regnum, $devid, $factoredprod, abs($factoredsales), '$date', 2) ");
			
		}
	}
		


	$db = connectTWO("account_47");
	$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_deviceID = $devid AND register_config = 'Gas - Well Meter'");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	 
	foreach($results as $data){
				
		$regnum = $data['register_number'];
		
	
		foreach ($dates as $key=>$date) {
			

		
				$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE register_number = $regnum AND register_deviceID = $devid AND calculation_date = '$date' AND calculation_type = 3 ");
			
				$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_deviceID = $devid AND register_number = $regnum AND register_date BETWEEN '$date 06:00:00' AND '$date 06:59:59' ORDER BY register_date DESC LIMIT 1  ");
				$gas = $stmt->fetchAll(PDO::FETCH_ASSOC);
			

				$stmt = $db->query("INSERT INTO tbl_register_calculations (register_number, register_deviceID, gas_volume, calculation_date, calculation_type) VALUES ($regnum, $devid, '".$gas[0]['register_reading']."', '$date', 3) ");
			
		}
	}

}
?>
<?php 

include('../functions/functions.php');

$array = '';

$database = "account_46";
$db = connectTWO($database);


if (isset($_GET['d'])) {	 

	if ($_GET['start']) {
		$stmt = $db->query("SELECT calculation_date, sum(water_production_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID = ".$_GET['d']." AND calculation_type = 2 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' GROUP BY calculation_date ORDER BY calculation_date");
	} else {
		$stmt = $db->query("SELECT calculation_date, sum(water_production_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID = ".$_GET['d']." AND calculation_type = 2  GROUP BY calculation_date ORDER BY calculation_date DESC LIMIT 30 ");
	}
	
} else {
	
	if ($_GET['start']) {
		$stmt = $db->query("SELECT calculation_date, sum(water_production_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID IN (239,241,244,250,254) AND calculation_type = 2 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' GROUP BY calculation_date ORDER BY calculation_date");
	} else {
		$stmt = $db->query("SELECT calculation_date, sum(water_production_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID IN (239,241,244,250,254) AND calculation_type = 2  GROUP BY calculation_date ORDER BY calculation_date DESC LIMIT 30 ");
	}
	
}
	

	

$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($results as $row) {
	
	if ($_GET['tb']) {
		$array[] = array((strtotime($row['calculation_date'])*1000),intval(getLogbookTotalCalcByDate(3,$row['calculation_date'],46,$_GET['tb'])));		
	} else {
	
		$array[] = array((strtotime($row['calculation_date'])*1000),intval($row['totsales']+getLogbookTotalCalcByDate(3,$row['calculation_date'],46)));	
	}
}

echo json_encode($array);

?>
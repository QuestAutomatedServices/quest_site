<?php 
include("/var/www/html/quest/functions/functions.php");

ini_set('max_execution_time', '-1'); 
ini_set('memory_limit',"-1");

$prev = date('Y-m-d', strtotime("-4 days"));
$dates = date_range($prev,date('Y-m-d'));

$db = connectTWO("account_43");


$stmt = $db->query("SELECT * FROM tbl_modbus_mapping_v2 WHERE map_tag_config = 'Oil - Production and Sales'");
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);



foreach($results as $tag){
	
		

		$multiplier = $tag['map_measurement_factor'];
	    //$conventreaingion = $tag['map_opcua_convention'];
		$d = $tag['map_siteID'];
	 	$mapID = $tag['mapID'];

		foreach ($dates as $dkey=>$date) {

			$dateplus1 = date('Y-m-d', strtotime($date . ' +1 day'));
			
			//$stmt = $db->query("SELECT * FROM tbl_register_history WHERE opcua_convention = '$convention' AND (date(register_date) = '$date' and hour(register_date) >= 07) OR  opcua_convention = '$convention' AND (date(register_date) = '$dateplus1' and hour(register_date) < 05)   ORDER BY register_date DESC ");
			
            
            $stmt = $db->query("SELECT * FROM tbl_modbus_readings_v2 WHERE reading_mapID = $mapID AND reading_date BETWEEN '$date 14:00:00' AND '$dateplus1 13:59:59' ORDER BY reading_date DESC ");
			
			$array = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			

			$production = 0;
			$sales = 0;
			$last = count($array) - 1;




			foreach ($array as $key=>$reading) {
				

				$increment = 0;
				$sincrement = 0;

				///////increase of at least one inch - production
				if ($key != $last && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) >= 1 && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) < 10 ) {
					$production +=	($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					$increment = ($array[$key]['reading_value'] - $array[$key+1]['reading_value']); 
				
				///////decrease of at least one inch - sales
				} else if ($key != $last && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) <= -1 && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) > -10) {
					$sales += ($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					$sincrement = ($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					
				}
					
				//} else if ($key != $last && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) > -2 && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) < 2) {
					//$production += ($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					//$increment = ($array[$key]['reading_value'] - $array[$key+1]['reading_value']); 
			//	}

				if ($increment > 0 && $increment < 10 ) { 
					

						$factoredinc = ($increment*$multiplier);

						$stmt = $db->query("DELETE FROM tbl_incremental_oil_prod WHERE mapID = $mapID AND register_date = '".$array[$key]['reading_date']."' AND production_calculation IS NOT NULL ");

						$stmt = $db->query("INSERT INTO tbl_incremental_oil_prod (mapID, d, production_calculation, production_calculation_in, register_date ) VALUES ($mapID, $d, $factoredinc, $increment, '".$array[$key]['reading_date']."' ) ");

				}


				if ($sincrement < 0 && $sincrement > -10) {

						$factoredsinc = ($sincrement*$multiplier);

						$stmt = $db->query("DELETE FROM tbl_incremental_oil_prod WHERE mapID = $mapID AND register_date = '".$array[$key]['reading_date']."' AND sales_calculation IS NOT NULL ");

						$stmt = $db->query("INSERT INTO tbl_incremental_oil_prod (mapID, d, sales_calculation, sales_calculation_in, register_date ) VALUES ($mapID, $d, abs($factoredsinc), abs($sincrement), '".$array[$key]['reading_date']."' ) ");

				}



			}


		   $factoredprod = ($production*$multiplier);
		   $factoredsales = ($sales*$multiplier);


			$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE mapID = $mapID AND calculation_date = '$date' AND calculation_type = 1 ");
			
			
			
			$stmt = $db->query("INSERT INTO tbl_register_calculations (mapID, d, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($mapID, $d, $factoredprod, $production, abs($factoredsales), abs($sales), '$date', 1) ");

	
		}
}



$stmt = $db->query("SELECT * FROM tbl_modbus_mapping_v2 WHERE map_tag_config = 'Water - Tank Level Feet'");
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

//echo "<pre>";
//print_r($results);
//exit();


foreach($results as $tag){

		$multiplier = $tag['map_measurement_factor'];
	    //$conventreaingion = $tag['map_opcua_convention'];
		$d = $tag['map_siteID'];
	 	$mapID = $tag['mapID'];

		foreach ($dates as $key=>$date) {

			$dateplus1 = date('Y-m-d', strtotime($date . ' +1 day'));
			
			//$stmt = $db->query("SELECT * FROM tbl_register_history WHERE opcua_convention = '$convention' AND (date(register_date) = '$date' and hour(register_date) >= 07) OR  opcua_convention = '$convention' AND (date(register_date) = '$dateplus1' and hour(register_date) < 05)   ORDER BY register_date DESC ");
            
            $stmt = $db->query("SELECT * FROM tbl_modbus_readings_v2 WHERE reading_mapID = $mapID AND reading_date BETWEEN '$date 14:00:00' AND '$dateplus1 13:59:59' ORDER BY reading_date DESC ");
			
			$array = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			//echo "<pre>";
		//	print_r($array);
			//exit();

		

			$production = 0;
			$sales = 0;
			$last = count($array) - 1;




			foreach ($array as $key=>$reading) {

				$increment = 0;
				$sincrement = 0;

				///////increase of at least one inch - production
				if ($key != $last && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) >= 1 && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) < 10 ) {
					$production +=	($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					$increment = ($array[$key]['reading_value'] - $array[$key+1]['reading_value']); 
				
				///////decrease of at least one inch - sales
				} else if ($key != $last && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) <= -1 && ($array[$key]['reading_value'] - $array[$key+1]['reading_value']) > -10) {
					$sales += ($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					$sincrement = ($array[$key]['reading_value'] - $array[$key+1]['reading_value']);
					
				}



			}


		   $factoredprod = ($production*$multiplier);
		   $factoredsales = ($sales*$multiplier);


			$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE mapID = $mapID AND calculation_date = '$date' AND calculation_type = 2 ");

			$stmt = $db->query("INSERT INTO tbl_register_calculations (mapID, d, water_production_calculation, water_trucked_calculation, calculation_date, calculation_type) VALUES ($mapID, $d, $factoredprod, abs($factoredsales), '$date', 2) ");

		}
}


?>
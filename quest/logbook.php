<?php 
session_start();

error_reporting(E_ALL);

include('connections/mysql.php');
include('functions/functions.php');

if (!isset($_SESSION['auth'])) {
	header('location: login.php');
	exit();		
}


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/MonthPicker.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">Grenadier Energy Partners</a>
       
                 
       </div>
        
      </div>
      
      <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right controlmenu">
           
           
           		<?php 
			  			echo '<li><a href="dashboard.php">Dashboard</a></li>';
			  
			  			if (isset($_GET['date'])) { 
							
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'">Pressures</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&s=3">Gas Volumes</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&s=1">Levels</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&s=2">Sales/Hauls</a></li>';
							
						} //else {
							
						 // echo '<li><a href="logbook.php?date=">Pressures</a></li>';
						 // echo '<li><a href="logbook.php?s=1">Levels</a></li>';
						 // echo '<li><a href="logbook.php?s=2">Sales/Hauls</a></li>';
							
						//}

			  		     echo '<li><a href="process/logout.php">Logout</a></li>';
			  	?>
		  </ul>
			  </div>
      
    </nav>

    <div class="container-fluid">
     
       <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           	
           		<?php 
			  		
			  			echo '<li><a href="dashboard.php">Dashboard</a></li>';
			  
			  			if (isset($_GET['date'])) { 
							
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'">Pressures</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&s=3">Gas Volumes</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&s=1">Levels</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&s=2">Sales/Hauls</a></li>';
						  echo '<li><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&st=1">Settings</a></li>';	
							
						} //else {
							
						  //echo '<li><a href="logbook.php">Reporting Home</a></li>';
						  //echo '<li><a href="logbook.php?s=1">Levels</a></li>';
						  //echo '<li><a href="logbook.php?s=2">Sales/Hauls</a></li>';
							
						//}
		  		
			  		    //echo '<li><a href="logbook.php?st=1">Settings</a></li>'; 
			  			echo '<li><a href="process/logout.php">Logout</a></li>';
			  	?>
         

			<ul>
        </div>
        
        	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<h1>Oil/Gas Gauge Report</h1>
       	
        	<!-- <div class="well">
        	
        	
					<h3>Gallihan #3, Howard, TX</h3>
       				<h4>Field: Big Spring (Mississippian)</h4>		
        	
				</div>-->
        	
        
              
              <?php if (!isset($_GET['date']) && !isset($_GET['de'])) { ?>	
              
              
              		<form method="get" action="logbook.php"  class="form-inline">
					  <div class="form-group">
					  
					  <select name="tb" class="form-control">
						  <option value="1">Broughton-Wise #1</option>
						  <option value="2">Gillihan #3</option>
						  <option value="6">Hyden SWD</option>
						  <option value="7">M.C. Hyden #7</option>
						  <option value="3">Midway 6 #1</option>
						  <option value="5">Satterwhite</option>
						  <option value="4">South Hutto Tank Battery</option>
						</select>
					  	
						<label for="log_book_field" class="sr-only">Choose Date</label>
						<input type="text" class="form-control monthpicker" name="date" placeholder="Choose Report Date">
					  </div>
					  <button type="submit" class="btn btn-default">Change Report</button>
					</form>
              
              
               <?php } else { 
				
					$dateparts = explode("/",$_GET['date']);
				
					$datelist=array();
					$month = $dateparts[0];
					$year = $dateparts[1];

					for($d=1; $d<=31; $d++)
					{
						$time=mktime(12, 0, 0, $month, $d, $year);          
						if (date('m', $time)==$month)       
							$datelist[]=date('m/d/Y', $time);
					}
			
					$prevmonth = date('m',strtotime($datelist[0] . "-1 day"));
					$prevyear = date('Y',strtotime($datelist[0] . "-1 day"));
				
				?>	
               
               
                  <?php if (!isset($_GET['de'])) { ?>
                   <form method="get" action="logbook.php"  class="form-inline" style="margin-bottom: 20px;">
                   
                   <?php if ($_GET['s'] == 1) { ?>	
                   	<input type="hidden" name="s" value="1">
                    <?php } ?>
                    
                    <?php if ($_GET['s'] == 2) { ?>	
                   	<input type="hidden" name="s" value="2">
                    <?php } ?>
                    
                    <?php if ($_GET['s'] == 3) { ?>	
                   	<input type="hidden" name="s" value="3">
                    <?php } ?>
                   
					  <div class="form-group">

					  
					  	<select name="tb" class="form-control">
						  <option value="1" <?php if ($_GET['tb'] == 1) echo 'selected'; ?>>Broughton-Wise #1</option>
						  <option value="2" <?php if ($_GET['tb'] == 2) echo 'selected'; ?>>Gillihan #3</option>
						  <option value="6" <?php if ($_GET['tb'] == 6) echo 'selected'; ?>>Hyden SWD</option>
						  <option value="7" <?php if ($_GET['tb'] == 7) echo 'selected'; ?>>M.C. Hyden #7</option>
						  <option value="3" <?php if ($_GET['tb'] == 3) echo 'selected'; ?>>Midway 6 #1</option>
						  <option value="5" <?php if ($_GET['tb'] == 5) echo 'selected'; ?>>Satterwhite</option>
						  <option value="4" <?php if ($_GET['tb'] == 4) echo 'selected'; ?>>South Hutto Tank Battery</option>
						</select>
					  
					  
						<label for="log_book_field" class="sr-only">Choose Date</label>
						<input type="text" class="form-control monthpicker" name="date" placeholder="Choose Report Date" value="<?php echo $_GET['date']; ?>">
					  </div>
					  <button type="submit" class="btn btn-default">Change Report</button>
					</form>
               
                 <div class="btn-group mobilelogbuttons" style="margin-bottom: 25px;">
                  
                  <a href="logbook.php?date=<?php echo $_GET['date']; ?>&tb=<?php echo $_GET['tb']; ?>" class="btn btn-primary <?php if (!isset($_GET['s']) && !isset($_GET['st'])) echo 'active' ?>">Pressures</a>
                  
                  <a href="logbook.php?date=<?php echo $_GET['date']; ?>&tb=<?php echo $_GET['tb']; ?>&s=3" class="btn btn-primary <?php if ($_GET['s'] == 3) echo 'active' ?>">Gas Volumes</a>
                  
                  <a href="logbook.php?date=<?php echo $_GET['date']; ?>&tb=<?php echo $_GET['tb']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Levels</a>
                  
                   <a href="logbook.php?date=<?php echo $_GET['date']; ?>&tb=<?php echo $_GET['tb']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Sales/Hauls</a>
                   
                   <a href="logbook.php?date=<?php echo $_GET['date']; ?>&tb=<?php echo $_GET['tb']; ?>&st=1" class="btn btn-primary <?php if ($_GET['st'] == 1) echo 'active' ?>">Settings</a>

              </div>
              
              <?php } ?>
               
               		<!--<p class="text-danger">Click a Date to Add Comments!</p>-->
               
               <?php if (isset($_GET['ndate'])) { ?>	
               
               			<h3>Enter Notes for <?php echo $_GET['ndate']; ?> </h3>
               			
               			<form method="post" action="process/logbook_notes.php"  class="form-inline">
               				<input type="hidden" name="month" value="<?php echo $month; ?>">
							<input type="hidden" name="year" value="<?php echo $year; ?>">
							<input type="hidden" name="ndate" value="<?php echo $_GET['ndate']; ?>">
								
						  <div class="form-group">
							<label for="logbook_notes" class="sr-only">Notes/Comments</label>
							<textarea name="logbook_notes" class="form-control" cols="80" rows="5"></textarea>
						  </div>
						  <button type="submit" class="btn btn-default" style="display: block;margin-top: 5px;">Submit</button>
						</form>
    			
    			
    						<ul class="list-group" style="width: 500px; margin-top: 30px;">
					<?php 				
								$lnotes = getLogbookNotes($_GET['ndate'],46);
												 
								
								foreach ($lnotes as $lnote) {
									
									echo '<li class="list-group-item">'.$lnote['logbook_notes'].'</li>';
								}
				
						?>
     			
     					</ul>

     			<?php } else { ?>	
     			
     			
     						<?php if (isset($_GET['de'])) { ?>
     						
     							<h2>You Are Entering Data for <?php echo $_GET['date']; ?> </h2>
     						
     						<form>
								  <div class="form-group">
									<label for="exampleInputEmail1">Hours Prod</label>
									<input type="text" class="form-control">
								  </div>
								  <div class="form-group">
									<label for="exampleInputPassword1">Tubing Pressure</label>
									<input type="text" class="form-control">
								  </div>
								  <div class="form-group">
									<label for="exampleInputPassword1">Casing Pressure</label>
									<input type="text" class="form-control">
								  </div>
								  <div class="form-group">
									<label for="exampleInputPassword1">Choke</label>
									<input type="text" class="form-control">
								  </div>
								  
								<div class="form-group">
									<label for="exampleInputPassword1">LP</label>
									<input type="text" class="form-control">
								  </div>

								  <button type="submit" class="btn btn-default">Submit</button>
								</form>
     							
     						<?php } else if (isset($_GET['st'])) { 
				
				
									$settings = getLogbookSettings($_GET['tb'],46);
					
									//print_r($settings);
								?>
     						
     						
     					<form method="post" action="process/logbook_settings.php"  class="form-inline">
               				<input type="hidden" name="month" value="<?php echo $month; ?>">
							<input type="hidden" name="year" value="<?php echo $year; ?>">
							<input type="hidden" name="tb" value="<?php echo $_GET['tb']; ?>">
     						
     							<table class="table table-striped"s style="width: 500px;">
									  <thead>

										<tr>
										 	<th>Tank</th>
										  <th>Volume Factor</th>
										</tr>


									  </thead>
									  <tbody>
									  
									  <tr><td>Oil Tank 1</td><td><input name="settings[ot1]" type="text" class="form-control input-sm" value="<?php echo $settings['ot1']; ?>"></td></tr>
									  <tr><td>Oil Tank 2</td><td><input name="settings[ot2]" type="text" class="form-control input-sm" value="<?php echo $settings['ot2']; ?>"></td></tr>
									  <tr><td>Oil Tank 3</td><td><input name="settings[ot3]" type="text" class="form-control input-sm" value="<?php echo $settings['ot3']; ?>"></td></tr>
									  <tr><td>Oil Tank 4</td><td><input name="settings[ot4]" type="text" class="form-control input-sm" value="<?php echo $settings['ot4']; ?>"></td></tr>
									  <tr><td>Water Tank 1</td><td><input name="settings[wt1]" type="text" class="form-control input-sm" value="<?php echo $settings['wt1']; ?>"></td></tr>
									  <tr><td>Water Tank 2</td><td><input name="settings[wt2]" type="text" class="form-control input-sm" value="<?php echo $settings['wt2']; ?>"></td></tr>
									  
									</tbody>
									</table>
     						
     								<button type="submit" class="btn btn-default">Submit</button>
								</form>
      			
							<?php } else if (!isset($_GET['s'])) { ?>	

								<form method="post" action="process/logbook.php" style="margin-bottom: 200px;">
								<input type="hidden" name="month" value="<?php echo $month; ?>">
								<input type="hidden" name="year" value="<?php echo $year; ?>">
								<input type="hidden" name="tb" value="<?php echo $_GET['tb']; ?>">
								
									<div class="table-responsive">

									<table class="table table-striped">
									  <thead>
										<tr>
										 <th colspan="14" bgcolor="#428bca" style="color: #FFF;">PRESSURES</th>
										</tr>


										<tr>
										 <th rowspan="2">Date</th>
										 <th rowspan="2">Notes</th>
										 <th rowspan="2">Hours Prod</th>
										 <th rowspan="2">Tubing Pressure</th>
										 <th rowspan="2">Casing Pressure</th>
										 <th rowspan="2">Choke</th>
										 <th rowspan="2">LP</th>

										</tr>


									  </thead>
									  <tbody>

										<?php 

											$current = getLogbookArray($month,$year,1,$_GET['tb'],46);
											//print_r($current);

											foreach ($datelist as $targdate) {
												
												
												if ($targdate <= date('m/d/Y')) {
													echo '<tr>
														<td><a href="logbook.php?date='.$targdate.'&tb='.$_GET['tb'].'&de=1">'.$targdate.'</a></td><td><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&ndate='.$targdate.'">View</a></td>';
												} else {
													echo '<tr>
														<td>'.$targdate.'</td><td><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&ndate='.$targdate.'">View</a></td>';
												}
												

												

														for ($t=1;$t<=5;$t++) {
															
															if ($t==4) {
																 echo '<td><input name="pressures['.$t.']['.$targdate.']" type="text" class="form-control input-sm" value="'.$current[$t][$targdate].'" style="width:40%;float:left;">&nbsp;&nbsp;&nbsp;/64 </td>';
															} else {
																 echo '<td><input name="pressures['.$t.']['.$targdate.']" type="text" class="form-control input-sm" value="'.$current[$t][$targdate].'"></td>';
															}
															
														
														}

												echo '</tr>';



											}


										 ?>


									</tbody>

								</table>

										</div>

									 <button type="submit" class="btn btn-default">Submit Values</button>

									</form>

							<?php } ?>	


							<?php if ($_GET['s'] == 1) { ?>	

							   <form method="post" action="process/logbook.php" style="margin-bottom: 200px;">
								<input type="hidden" name="month" value="<?php echo $month; ?>">
								<input type="hidden" name="year" value="<?php echo $year; ?>">
								<input type="hidden" name="tb" value="<?php echo $_GET['tb']; ?>">
								<input type="hidden" name="s" value="1">

									<div class="table-responsive">

									<table class="table table-striped">
									  <thead>
										<tr>
										 <th colspan="14" bgcolor="#428bca" style="color: #FFF;">WATER AND OIL LEVELS</th>
										</tr>


										<tr>
										<th>Date</th>
										<th>Notes</th>
										 <th>Oil Tank 1</th>
										 <th>Oil Tank 2</th>
										 <th>Oil Tank 3</th>
										 <th>Oil Tank 4</th>
										 <th>Water Tank 1</th>
										 <th>Water Tank 2</th>
										 <th>Addl Water</th>
										 <th class="success">Oil Prod (BBLS)</th>
										  <th class="success">Oil Sales (BBLS)</th>
										 <th class="info">Water Prod (BBLS)</th>
										 <th class="info">Water Hauls (BBLS)</th>
										 <th>Oil Stock (BBLS)</th>

										</tr>


									  </thead>
									  <tbody>

										<?php 

											$current = getLogbookArray($month,$year,2,$_GET['tb'],46);
											$prev = getLogbookArray($prevmonth,$prevyear,2,$_GET['tb'],46);
														
											$settings = getLogbookSettings($_GET['tb'],46);
														
											//echo "<pre>";
											//print_r($current);

											$i = 0;
											foreach ($datelist as $targdate) {
												
												$prevday = date('m/d/Y',strtotime($targdate . "-1 day"));


												echo '<tr>
														<td>'.$targdate.'</td><td><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&ndate='.$targdate.'">View</a></td>';

														for ($t=1;$t<=10;$t++) {

															if ($t<=4) {
																
																$oilstock = 0;
																$preoilstock = 0;
																
																if ($t==1) {
																	$tankvar = "ot1";	
																}
																
																if ($t==2) {
																	$tankvar = "ot2";	
																}
																
																if ($t==3) {
																	$tankvar = "ot3";	
																}
																
																if ($t==4) {
																	$tankvar = "ot4";	
																}
																
																if ($i == 0){
	
																	$preoilstock += ($prev['feet'][$t][$prevday]*12);
																	$preoilstock += ($prev['inches'][$t][$prevday]);
																	$preoilbarrels[0] += number_format(round($preoilstock*$settings[$tankvar],2),2);
																}
																	
																$oilstock += ($current['feet'][$t][$targdate]*12);
																$oilstock += ($current['inches'][$t][$targdate]);
																
																$oilbarrels[$i] += number_format(round($oilstock*$settings[$tankvar],2),2);
																
															}
															
															
															
															if ($t==5 || $t==6 || $t==7) {
																
																$waterstock = 0;
																$prewaterstock = 0;
																
																if ($t==5) {
																	$tankvar = "wt1";	
																}
																
																if ($t==6) {
																	$tankvar = "wt2";	
																}
																
																if ($i == 0){
																		
																	$prewaterstock += ($prev['feet'][$t][$prevday]*12);
																	$prewaterstock += ($prev['inches'][$t][$prevday]);
																	
																	$prewaterbarrels[0] += number_format(round($prewaterstock*$settings[$tankvar],2),2);
																}
																
																$waterstock += ($current['feet'][$t][$targdate]*12);
																$waterstock += ($current['inches'][$t][$targdate]);
																
																$waterbarrels[$i] += number_format(round($waterstock*$settings[$tankvar],2),2);
															}

															if ($t == 8) {
																
																if ($targdate <= date("m/d/Y")) {
																	
																	if ($i == 0){
																		
																		if ($prev != ''){
																			
																		echo '<td class="success">'.number_format(round(($oilbarrels[$i]-$preoilbarrels[0]),2),2).'</td>';
																		
																		echo '<td class="success">'.getLogbookTotalCalcByDate(4,formatDateMYSQL($targdate),46,$_GET['tb']).'</td>';
																			
																		$_POST['tank_battery'] = $_GET['tb'];
																		$_POST['calc_date'] = formatDateMYSQL($targdate);
																		$_POST['calc_type'] = 2;
																		$_POST['calc_value'] = $oilbarrels[$i]-$preoilbarrels[0];

																		deleteCalc($_POST,'tbl_logbook_calcs',46);
																		insertRecord($_POST,'tbl_logbook_calcs',46);
																			
																		} else {
																			echo '<td class="success">0.00</td>';
																		}
																		
																		
																	} else if ($oilbarrels[$i] != '') {
																		
																		echo '<td class="success">'.number_format(round(($oilbarrels[$i]-$oilbarrels[$i-1]),2),2).'</td>';
																		
																		echo '<td class="success">'.getLogbookTotalCalcByDate(4,formatDateMYSQL($targdate),46,$_GET['tb']).'</td>';
																		
																		$_POST['tank_battery'] = $_GET['tb'];
																		$_POST['calc_date'] = formatDateMYSQL($targdate);
																		$_POST['calc_type'] = 2;
																		$_POST['calc_value'] = $oilbarrels[$i]-$oilbarrels[$i-1];

																		deleteCalc($_POST,'tbl_logbook_calcs',46);
																		insertRecord($_POST,'tbl_logbook_calcs',46);
																	} else {
																		
																		echo '<td class="success">---</td>';
																		echo '<td class="success">---</td>';
																	}
																	
																} else {
																	
																	echo '<td class="success">---</td>';
																	echo '<td class="success">---</td>';
																	
																}
																	
																
																//echo '<td>&nbsp;</td>';
															} else if ($t == 9) {							
																
																if ($targdate <= date("m/d/Y")) {
																	
																	if ($i == 0){
																		
																			if ($prev != ''){

																			echo '<td class="info">'.number_format(round(($waterbarrels[$i]-$prewaterbarrels[0]),2),2).'</td>';
																																					
																			echo '<td class="info">'.getLogbookTotalCalcByDate(5,formatDateMYSQL($targdate),46,$_GET['tb']).'</td>';

																			$_POST['tank_battery'] = $_GET['tb'];
																			$_POST['calc_date'] = formatDateMYSQL($targdate);
																			$_POST['calc_type'] = 3;
																			$_POST['calc_value'] = $waterbarrels[$i]-$prewaterbarrels[0];

																			deleteCalc($_POST,'tbl_logbook_calcs',46);
																			insertRecord($_POST,'tbl_logbook_calcs',46);

																			} else {
																				echo '<td class="info">0.00</td>';
																			}
																		
																		
																		} else if ($waterbarrels[$i] != '') {
																		
																		echo '<td class="info">'.number_format(round(($waterbarrels[$i]-$waterbarrels[$i-1]),2),2).'</td>';

																		echo '<td class="info">'.getLogbookTotalCalcByDate(5,formatDateMYSQL($targdate),46,$_GET['tb']).'</td>';
																		
																		$_POST['tank_battery'] = $_GET['tb'];
																		$_POST['calc_date'] = formatDateMYSQL($targdate);
																		$_POST['calc_type'] = 3;
																		$_POST['calc_value'] = $waterbarrels[$i]-$waterbarrels[$i-1];

																		deleteCalc($_POST,'tbl_logbook_calcs',46);
																		insertRecord($_POST,'tbl_logbook_calcs',46);
																		
																		} else {
																	
																			echo '<td class="info">---</td>';
																			echo '<td class="info">---</td>';
																		}
																	
																} else {
																	
																	echo '<td class="info">---</td>';
																	echo '<td class="info">---</td>';
																	
																}
																
															} else if ($t == 10) {							
																echo '<td>'.$oilbarrels[$i].'</td>';
																

																
																
															} else {
																echo '<td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" placeholder="feet" name="levels[feet]['.$t.']['.$targdate.']" value="'.$current['feet'][$t][$targdate].'"><input type="text" class="form-control input-sm" style="width:45%;float:left;" placeholder="inches" name="levels[inches]['.$t.']['.$targdate.']"  value="'.$current['inches'][$t][$targdate].'" ></td>';
															}
														}

												echo '</tr>';
												$i++;
												
												


											}
														
											//echo "<pre>";
											//print_r($oilbarrels);	
											//print_r($preoilbarrels);		


										 ?>


									</tbody>

								</table>

										</div>

									 <button type="submit" class="btn btn-default">Submit</button>

									</form>

							<?php } ?>


							<?php if ($_GET['s'] == 2) { ?>	


							   <form method="post" action="process/logbook.php" style="margin-bottom: 200px;">
								<input type="hidden" name="month" value="<?php echo $month; ?>">
								<input type="hidden" name="year" value="<?php echo $year; ?>">
								<input type="hidden" name="tb" value="<?php echo $_GET['tb']; ?>">
								<input type="hidden" name="s" value="2">

									<div class="table-responsive">

									<table class="table table-striped">
									  <thead>
										<tr>
										 <th colspan="14" bgcolor="#428bca" style="color: #FFF;">SALES/HAULS</th>
										</tr>

									<tr>
										<th colspan="7" class="info">Oil Sales</th>
										<th colspan="1" class="success">Water Hauls</th>


										</tr>

									<tr>
										<th>Report Date</th>
										<th>Prod Date</th>
										 <th>Tank No</th>
										 <th>Ticket No</th>
										 <th>From</th>
										 <th>To</th>
										 <th>Barrels</th>
										 <th>Water Hauled</th>


										</tr>


									  </thead>
									  <tbody>

										<?php 

											$current = getLogbookArray($month,$year,3,$_GET['tb'],46);

											//echo "<pre>";
											//print_r($current);

											for ($i=1;$i<=32;$i++) {

												echo '<tr>
														<td><input type="text" class="form-control input-sm datepicker" placeholder="date" name="sales[repdate]['.$i.']"  value="'.$current['repdate'][$i].'"></td>
														<td><input type="text" class="form-control input-sm datepicker" placeholder="date" name="sales[prodate]['.$i.']" value="'.$current['prodate'][$i].'"></td>';

														for ($t=1;$t<=6;$t++) {

															if ($t != 3 && $t != 4) {
																echo '<td><input type="text" class="form-control input-sm" name="sales[inputfield]['.$i.']['.$t.']" value="'.$current['inputfield'][$i][$t].'"></td>'; 
															} else {
																echo '<td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" placeholder="feet" name="sales[feet]['.$i.']['.$t.']" value="'.$current['feet'][$i][$t].'"><input type="text" class="form-control input-sm" style="width:45%;float:left;" placeholder="inches" name="sales[inches]['.$i.']['.$t.']" value="'.$current['inches'][$i][$t].'"></td>';
															}
														}

												echo '</tr>';



											}


										 ?>


									</tbody>

								</table>

										</div>

									 <button type="submit" class="btn btn-default">Submit</button>

									</form>

							<?php } ?>
       	     	
       	     	
							<?php if ($_GET['s'] == 3) { ?>	
							
							
							<form method="post" action="process/logbook.php" style="margin-bottom: 200px;">
								<input type="hidden" name="month" value="<?php echo $month; ?>">
								<input type="hidden" name="year" value="<?php echo $year; ?>">
								<input type="hidden" name="tb" value="<?php echo $_GET['tb']; ?>">
							   <input type="hidden" name="s" value="3">

									<div class="table-responsive">

									<table class="table table-striped">
									  <thead>
										<tr>
										 <th colspan="14" bgcolor="#428bca" style="color: #FFF;">GAS VOLUMES</th>
										</tr>


		<!--								<tr>
											 <th colspan="4">GEP FDP</th> 

</tr>-->


										  <tr>
											<th>Date</th>
											<th>Notes</th>
											<th>Cum Vol (MCF)</th>
											<th>Spot Rate (MCFD)</th>
											<th>VRU (MCF)</th>
											<th>Daily Vol (MCF)</th>


										  </tr>

									  </thead>
									  <tbody>

										<?php 
						
											$current = getLogbookArray($month,$year,4,$_GET['tb'],46);
											$prev = getLogbookArray($prevmonth,$prevyear,4,$_GET['tb'],46);
														
											//echo "<pre>";
											//print_r($prev);
											$i = 0;
														
											foreach ($datelist as $targdate) {

												echo '<tr>
														<td>'.$targdate.'</td><td><a href="logbook.php?date='.$_GET['date'].'&tb='.$_GET['tb'].'&ndate='.$targdate.'">View</a></td>';

														for ($t=1;$t<=4;$t++) {
															
															$prevday = date('m/d/Y',strtotime($targdate . "-1 day"));
															
															if ($t ==4) {
																if ($current[1][$targdate] && $i==0){
																	
																	//if ($prev[1][$prevday] != '' ){
																		
																		echo '<td><input name="gas['.$t.']['.$targdate.']" type="text" class="form-control input-sm" value="'.$current[$t][$targdate].'"></td>';
																		
																		//echo '<td>'.($current[1][$targdate]-$prev[1][$prevday]).'</td>';
																		
																		//$_POST['tank_battery'] = $_GET['tb'];
																		//$_POST['calc_date'] = formatDateMYSQL($targdate);
																		//$_POST['calc_type'] = 1;
																		//$_POST['calc_value'] = ($current[1][$targdate]-$prev[1][$prevday]);

																		//deleteCalc($_POST,'tbl_logbook_calcs',46);
																		//insertRecord($_POST,'tbl_logbook_calcs',46);
																		
																	//} else {
																		//echo '<td>No prev day reading</td>';
																	//}
																	
																} else if ($current[1][$targdate]){
																	
																	//if ($current[1][$prevday] != '' ){
																		
																		echo '<td><input name="gas['.$t.']['.$targdate.']" type="text" class="form-control input-sm" value="'.$current[$t][$targdate].'"></td>';
																		
																		//echo '<td>'.($current[1][$targdate]-$current[1][$prevday]).'</td>';	
																		
																		//$_POST['tank_battery'] = $_GET['tb'];
																		//$_POST['calc_date'] = formatDateMYSQL($targdate);
																		//$_POST['calc_type'] = 1;
																		//$_POST['calc_value'] = ($current[1][$targdate]-$current[1][$prevday]);
																		
																		//deleteCalc($_POST,'tbl_logbook_calcs',46);
																		//insertRecord($_POST,'tbl_logbook_calcs',46);
																		
																	//} else {
																		//echo '<td>No prev day reading</td>';
																	//}
																	
																} else {
																	//echo '<td>&nbsp;</td>';
																	
																	echo '<td><input name="gas['.$t.']['.$targdate.']" type="text" class="form-control input-sm"></td>';
																}
																
															} else {
																echo '<td><input name="gas['.$t.']['.$targdate.']" type="text" class="form-control input-sm" value="'.$current[$t][$targdate].'"></td>';
															}
															
														 
														}

												echo '</tr>';

												$i++;

											}


										 ?>


									</tbody>

								</table>

										</div>

									 <button type="submit" class="btn btn-default">Submit</button>

									</form>

							<?php } ?>
       	     	
       	     	
        	     	
      	            <?php } ?>
       	                  	
       	         <?php } ?>	   
        	                  	
		   </div>
      
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-1.12.1.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script src="https://cdn.rawgit.com/digitalBush/jquery.maskedinput/1.4.1/dist/jquery.maskedinput.min.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    

	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="jquery/MonthPicker.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../beta/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  	
  	<script>
			$(function() {

						$(".datepicker").datepicker();
 					$(".monthpicker").MonthPicker({ Button: false });
				

			});
</script>
 
	<?php //include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

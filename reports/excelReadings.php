<?php
session_start();
include('functions/functions.php');
ini_set('max_execution_time', '-1'); 
ini_set('memory_limit',"-1");

if (!isset($_SESSION['auth'])) {
	header('location: login.php');
	exit();		
}


//$database = "account_".$_SESSION['user_accountID']."";
$database = "account_24";
$db = connectTWO($database);

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */


/** PHPExcel */
require_once 'phpExcel/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(12);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(12);



// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B1', 'Battery 1')
            ->setCellValue('D1', 'Battery 2')
            ->setCellValue('F1', 'Battery 4')
            ->setCellValue('H1', 'Battery 7')
            ->setCellValue('J1', 'Flow Meters')
			->setCellValue('M1', 'Totals')
			->setCellValue('A2', 'Date')
			->setCellValue('B2', 'BOPD')
			->setCellValue('C2', 'Sales')
			->setCellValue('D2', 'BOPD')
			->setCellValue('E2', 'Sales')
			->setCellValue('F2', 'BOPD')
			->setCellValue('G2', 'Sales')
			->setCellValue('H2', 'BOPD')
			->setCellValue('I2', 'Sales')
			->setCellValue('J2', 'NUBs H20')
			->setCellValue('K2', 'H20 TANK (in)')
			->setCellValue('M2', 'Oil')
			->setCellValue('N2', 'Water')
			->setCellValue('O2', 'Sales');


	$line = 3;

	$dates=array();
	$month = $_GET['m'];
	$year = $_GET['y'];

	for($d=1; $d<=31; $d++)
	{
		$time=mktime(12, 0, 0, $month, $d, $year);          
		if (date('m', $time)==$month)       
			$dates[]=date('Y-m-d', $time);
	}

	

	foreach ($dates as $date) {
		
			$totals = getDailyTotalsByDate($date,24);
		
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$line, $date)
				->setCellValue('B'.$line, $totals[0]['112_BOPD'])
				->setCellValue('C'.$line, $totals[0]['112_BOSold'])
				->setCellValue('D'.$line, $totals[0]['132_BOPD']+$totals[0]['142_BOPD'])
				->setCellValue('E'.$line, $totals[0]['132_BOSold']+$totals[0]['142_BOSold'])
				->setCellValue('F'.$line, $totals[0]['162_BOPD'])
				->setCellValue('G'.$line, $totals[0]['162_BOSold'])
				->setCellValue('H'.$line, $totals[0]['42_BOPD']+$totals[0]['52_BOPD']+$totals[0]['92_BOPD'])
				->setCellValue('I'.$line, $totals[0]['42_BOSold']+$totals[0]['52_BOSold']+$totals[0]['92_BOSold'])
				->setCellValue('J'.$line, $totals[0]['182_BWPD'])
				->setCellValue('K'.$line, $totals[0]['total_WI'])
				->setCellValue('M'.$line, $totals[0]['total_BOPD'])
				->setCellValue('N'.$line, $totals[0]['182_BWPD'] + $totals[0]['total_WI'])
				->setCellValue('O'.$line, $totals[0]['total_BOSold']);

			$line++;

	}
			

$objPHPExcel->getActiveSheet()->setTitle('Readings Export');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="readings_export.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
ob_clean();
$objWriter->save('php://output');
exit;
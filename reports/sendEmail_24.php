<?php
include("functions/functions.php");
include("PHPMailer_5.2.0/class.phpmailer.php");

//echo date("H:i:s");

ini_set('max_execution_time', '-1'); 
ini_set('memory_limit',"-1");


//$current = '2016-11-08';
//$prev = '2016-11-07';

$current = date('Y-m-d');
$prev = date('Y-m-d', strtotime("-1 day"));


$totalBOSold = 0;

$forty = getRegisterValues(42,$prev,$current);



$fifty = getRegisterValues(52,$prev,$current);
$ninety = getRegisterValues(92,$prev,$current);
$oneten = getRegisterValues(112,$prev,$current);


$onethirty = getRegisterValues(132,$prev,$current);
$oneforty = getRegisterValues(142,$prev,$current);
$onesixty = getRegisterValues(162,$prev,$current);

$totalBOPD20 = (($forty[2] + $fifty[2]  + $ninety[2]  + $oneten[2]  + $onethirty[2]  + $oneforty[2] ) * 20);
$totalBOPD14 = ($onesixty[2] * 14);
$totalBOPD =  round(($totalBOPD14+$totalBOPD20),2);


$totalBOSold20 = (($forty[3] + $fifty[3]  + $ninety[3]  + $oneten[3]  + $onethirty[3]  + $oneforty[3] ) * 20);
$totalBOSold14 = ($onesixty[3] * 14);
$totalBOSold =  round((abs($totalBOSold20+$totalBOSold14)),2);

$body = '<img src="http://www.questdashboard.com/images/logo.jpg" width="260" height="80" />

<h4 style="font-family:Verdana, Geneva, sans-serif;font-size:18px;">Phoenix (North Doyle Penn) </h4>

<h4 style="font-family:Verdana, Geneva, sans-serif;font-size:14px;">Date: '.formatDate($current).' </h4>

<h4 style="font-family:Verdana, Geneva, sans-serif;font-size:14px;color:#339966;">Daily Total BOP: '.number_format($totalBOPD,2).' </h4>

<h4 style="font-family:Verdana, Geneva, sans-serif;font-size:14px;color:#C00;">Daily Total BO Sold:  '.number_format($totalBOSold,2).' </h4>

<h4 style="font-family:Verdana, Geneva, sans-serif;font-size:14px;color:#339966;">MTD BOP: '.getMTDValue(1).' </h4>

<h4 style="font-family:Verdana, Geneva, sans-serif;font-size:14px;color:#C00;">MTD BO Sold:  '.getMTDValue(2).' </h4>

<table width="800" cellspacing="0" cellpadding="3" style="font-family:Verdana, Geneva, sans-serif;font-size:11px;">
  <tr>
    <td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Battery #7</td>
  </tr>
  <tr>
    <td width="137" bgcolor="#F4F4F4">Oil Prod</td>
    <td width="52" align="right" bgcolor="#F4F4F4">Today</td>
    <td width="58" align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td width="43" align="right" bgcolor="#F4F4F4">Prod</td>
    <td width="8" bgcolor="#F4F4F4"></td>
    <td width="23" bgcolor="#F4F4F4"></td>
    <td width="126" align="right" bgcolor="#F4F4F4">Total BOPD</td>
    <td width="20" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="111" bgcolor="#F4F4F4">BO Sold</td>
    <td width="160" align="right" bgcolor="#F4F4F4">Total BO Sold</td>
  </tr>
  <tr>
    <td>West Oil Tank</td>
    <td align="right">'.$forty[5].'</td>
    <td align="right">'.$forty[1].'</td>
    <td align="right">'.$forty[2].'</td>
    <td></td>
    <td></td>
    <td align="right">'.round((( $forty[2] + $fifty[2] + $ninety[2] ) * 20),2).'</td>
    <td>&nbsp;</td>
    <td>West Oil Tank</td>
    <td align="right">'.round(abs($forty[3]*20),2).'</td>
  </tr>
  <tr>
    <td>Middle Oil Tank</td>
    <td align="right">'.$fifty[5].'</td>
    <td align="right">'.$fifty[1].'</td>
    <td align="right">'.$fifty[2].'</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>Middle Oil Tank</td>
    <td align="right">'.round(abs($fifty[3]*20),2).'</td>
  </tr>
  <tr>
    <td>East Oil Tank</td>
    <td align="right">'.$ninety[5].'</td>
    <td align="right">'.$ninety[1].'</td>
    <td align="right">'.$ninety[2].'</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>East Oil Tank</td>
    <td align="right">'.round(abs($ninety[3]*20),2).'</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
	<td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Battery #1</td>
  </tr>
    <tr>
      <td width="137" bgcolor="#F4F4F4">Oil Prod</td>
    <td width="52" align="right" bgcolor="#F4F4F4">Today</td>
    <td width="58" align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td width="43" align="right" bgcolor="#F4F4F4">Prod</td>
    <td width="8" bgcolor="#F4F4F4"></td>
    <td width="23" bgcolor="#F4F4F4"></td>
    <td width="126" align="right" bgcolor="#F4F4F4">Total BOPD</td>
    <td width="20" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="111" bgcolor="#F4F4F4">BO Sold</td>
    <td width="160" align="right" bgcolor="#F4F4F4">Total BO Sold</td>
  </tr>
  <tr>
    <td>Oil Tank</td>
    <td align="right">'.$oneten[5].'</td>
    <td align="right">'.$oneten[1].'</td>
    <td align="right">'.$oneten[2].'</td>
    <td></td>
    <td></td>
    <td align="right">'.round(($oneten[2]* 20),2).'</td>
    <td>&nbsp;</td>
    <td><p>Oil Tank</p></td>
    <td align="right">'.round(abs($oneten[3]*20),2).'</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
	<td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Battery #2</td>
  </tr>
    <tr>
      <td width="137" bgcolor="#F4F4F4">Oil Prod</td>
    <td width="52" align="right" bgcolor="#F4F4F4">Today</td>
    <td width="58" align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td width="43" align="right" bgcolor="#F4F4F4">Prod</td>
    <td width="8" bgcolor="#F4F4F4"></td>
    <td width="23" bgcolor="#F4F4F4"></td>
    <td width="126" align="right" bgcolor="#F4F4F4">Total BOPD</td>
    <td width="20" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="111" bgcolor="#F4F4F4">BO Sold</td>
    <td width="160" align="right" bgcolor="#F4F4F4">Total BO Sold</td>
  </tr>
  <tr>
    <td>Middle West Oil</td>
    <td align="right">'.$onethirty[5].'</td>
    <td align="right">'.$onethirty[1].'</td>
    <td align="right">'.$onethirty[2].'</td>
    <td></td>
    <td></td>
    <td align="right">'.round((( $onethirty[2] + $oneforty[2]  ) * 20),2).'</td>
    <td>&nbsp;</td>
    <td>Middle West Oil</td>
    <td align="right">'.round(abs($onethirty[3]*20),2).'</td>
  </tr>
  <tr>
    <td>North East Oil</td>
    <td align="right">'.$oneforty[5].'</td>
    <td align="right">'.$oneforty[1].'</td>
    <td align="right">'.$oneforty[2].'</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>North East Oil</td>
    <td align="right">'.round(abs($oneforty[3]*20),2).'</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
	<td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Battery #4</td>
  </tr>
    <tr>
      <td width="137" bgcolor="#F4F4F4">Oil Prod</td>
    <td width="52" align="right" bgcolor="#F4F4F4">Today</td>
    <td width="58" align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td width="43" align="right" bgcolor="#F4F4F4">Prod</td>
    <td width="8" bgcolor="#F4F4F4"></td>
    <td width="23" bgcolor="#F4F4F4"></td>
    <td width="126" align="right" bgcolor="#F4F4F4">Total BOPD</td>
    <td width="20" bgcolor="#F4F4F4">&nbsp;</td>
    <td width="111" bgcolor="#F4F4F4">BO Sold</td>
    <td width="160" align="right" bgcolor="#F4F4F4">Total BO Sold</td>
  </tr>
  <tr>
    <td>Oil Tank</td>
    <td align="right">'.$onesixty[5].'</td>
    <td align="right">'.$onesixty[1].'</td>
    <td align="right">'.$onesixty[2].'</td>
    <td></td>
    <td></td>
    <td align="right">'.round(($onesixty[2]*14),2).'</td>
    <td>&nbsp;</td>
    <td>Oil Tank</td>
    <td align="right">'.round(abs($onesixty[3]*14),2).'</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="right">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
	<td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Flow Meters</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#F4F4F4">Nubbs Meter</td>
    <td align="right" bgcolor="#F4F4F4">Today</td>
    <td align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td align="right" bgcolor="#F4F4F4">+/-</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td align="right" bgcolor="#F4F4F4">Nubbs Total BWPD</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
  </tr>
  <tr>
    <td>Nubbs Water Meter</td>
    <td align="right">'.getRegisterValue(182,'',$current).'</td>
    <td align="right">'.getRegisterValue(182,'',$prev).'</td>
    <td align="right">'.(getRegisterValue(182,'',$current)-getRegisterValue(182,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td align="right">'.(getRegisterValue(182,'',$current)-getRegisterValue(182,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#F4F4F4">Battery #7 Flow Meter</td>
    <td align="right" bgcolor="#F4F4F4">Today</td>
    <td align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td align="right" bgcolor="#F4F4F4">+/-</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td align="right" bgcolor="#F4F4F4">Total BWPD VHT</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
  </tr>
  <tr>
    <td>Vertical Heater Treater</td>
    <td align="right">'.getRegisterValue(214,'',$current).'</td>
    <td align="right">'.getRegisterValue(214,'',$prev).'</td>
    <td align="right">'.(getRegisterValue(214,'',$current)-getRegisterValue(214,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td align="right">'.(getRegisterValue(214,'',$current)-getRegisterValue(214,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#F4F4F4">Battery #1 Flow Meter</td>
    <td align="right" bgcolor="#F4F4F4">Today</td>
    <td align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td align="right" bgcolor="#F4F4F4">+/-</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td align="right" bgcolor="#F4F4F4"> Total BWPD</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
  </tr>
  <tr>
    <td>Water Flow Meter</td>
    <td align="right">'.getRegisterValue(243,'',$current).'</td>
    <td align="right">'.getRegisterValue(243,'',$prev).'</td>
    <td align="right">'.(getRegisterValue(243,'',$current)-getRegisterValue(243,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td align="right">'.(getRegisterValue(243,'',$current)-getRegisterValue(243,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td bgcolor="#F4F4F4">Battery #2 Flow Meter</td>
    <td align="right" bgcolor="#F4F4F4">Today</td>
    <td align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td align="right" bgcolor="#F4F4F4">+/-</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td align="right" bgcolor="#F4F4F4"> Total BWPD</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
  </tr>
  <tr>
    <td>Water Flow Meter</td>
    <td align="right">'.getRegisterValue(253,'',$current).'</td>
    <td align="right">'.getRegisterValue(253,'',$prev).'</td>
    <td align="right">'.(getRegisterValue(253,'',$current)-getRegisterValue(253,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td align="right">'.(getRegisterValue(253,'',$current)-getRegisterValue(253,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
	<td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Injected Water</td>
  </tr>

  <tr>
    <td bgcolor="#F4F4F4">Injected Water</td>
    <td align="right" bgcolor="#F4F4F4">Today</td>
    <td align="right" bgcolor="#F4F4F4">Prev Day</td>
    <td align="right" bgcolor="#F4F4F4">+/-</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td align="right" bgcolor="#F4F4F4">Injected Water Total</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
  </tr>
  <tr>
    <td>North Inj</td>
    <td align="right">'.getRegisterValue(223,'',$current).'</td>
    <td align="right">'.getRegisterValue(223,'',$prev).'</td>
    <td align="right">'.(getRegisterValue(223,'',$current)-getRegisterValue(223,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td align="right">'.((getRegisterValue(223,'',$current)-getRegisterValue(223,'',$prev)) + (getRegisterValue(233,'',$current)-getRegisterValue(233,'',$prev)) ).'</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>Middle Inj</td>
    <td align="right">'.getRegisterValue(233,'',$current).'</td>
    <td align="right">'.getRegisterValue(233,'',$prev).'</td>
    <td align="right">'.(getRegisterValue(233,'',$current)-getRegisterValue(233,'',$prev)).'</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td></td>
    <td>&nbsp;</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
	<td colspan="10" bgcolor="#02AEEE" style="font-weight:bold;color:#FFF;font-size:16px;">Pressures</td>
  </tr>

  <tr>
    <td bgcolor="#F4F4F4">Pressures (PSI)</td>
    <td align="right" bgcolor="#F4F4F4">Last Poll</td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
    <td bgcolor="#F4F4F4"></td>
  </tr>
  <tr>
    <td>Middle Inj (press 1)</td>
    <td align="right">'.getRegisterValue(1,'',$current).'</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>North Inj (press 2)</td>
    <td align="right">'.getRegisterValue(2,'',$current).'</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
</table>';

//echo $body;

$mail = new PHPMailer(); // defaults to using php "mail()"

$mail->isSMTP();  // Set mailer to use SMTP
$mail->Host = 'smtp.mailgun.org';  // Specify mailgun SMTP servers
$mail->SMTPAuth = true; // Enable SMTP authentication
$mail->Username = 'postmaster@qautomated.com'; // SMTP username from https://mailgun.com/cp/domains
$mail->Password = 'b56cc91d54d2139d094538d1f6eab513'; // SMTP password from https://mailgun.com/cp/domains
$mail->SMTPSecure = 'tls';

$mail->SetFrom('do-not-reply@questdashboard.net', 'Quest Automated Services');
$mail->AddReplyTo('do-not-reply@questdashboard.net','Quest Automated Services');

/*
$mail->AddBCC('markmorton@phpprousa.com', 'Mark Morton');
//$mail->AddAddress('mpatton@quest-automated.com', 'Mark Patton');
$mail->AddBCC('acooper@quest-automated.com', 'Adam Cooper');
$mail->AddBCC('jmartin@quest-automated.com', 'Jacob Martin');
//$mail->AddAddress('rray@quest-automated.com', 'Robert Ray');

$mail->AddAddress('phoenixpetro@msn.com', 'Terry Fuller');
$mail->AddAddress('bobbull68@att.net', 'Bob Bullock');
$mail->AddAddress('jwpetro57@gmail.com', 'Jim Williams');
$mail->AddAddress('crwpetro@gmail.com', 'Charlie Williams');
$mail->AddAddress('jwill643@gmail.com', 'Dan Williams');
$mail->AddAddress('c.fuller@phoenixpetrocorp.com', 'Clint Fuller');
$mail->AddAddress('j.curtiss@sbcglobal.net', 'Jim Curtiss');
$mail->AddAddress('calvinlay44@hotmail.com', 'Calvin Lay');
$mail->AddAddress('dean.riggs50@yahoo.com', 'Dean Riggs');
$mail->AddAddress('garyramsey1969@gmail.com', 'Gary Ramsey');
$mail->AddAddress('s.hermen@phoenixpetrocorp.com', 'Steve Herman');
$mail->AddAddress('b.bullock@phoenixpetrocorp.com', 'Bob Bullock');
$mail->AddAddress('d.riggs@phoenixpetrocorp.com', 'Dean Riggs');
 
*/

$mail->AddAddress('nmorton@quest-automated.com');
 
$mail->Subject = "Phoenix Daily Report";

$mail->MsgHTML($body);

$mail->Send();

?>
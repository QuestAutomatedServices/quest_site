<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>
    <style>
        .cylinder-wrapper{
            border: 1.1px solid gray;
            border-radius: 15px;
            background-color: #f2f2f2;
            height: 200px;
            width: 105px;
            position:relative;
        }
        
        .cylinder-value1{
            background-color: #002db3;
            border-radius: 0px 0px 15px 15px;
            width: 100%;
            bottom: 0;
            position: absolute;
        }
        
        .cylinder-value2{
            background-color: #79d279;
            width: 100%;
            position: absolute;
        }
    </style>
</head>

<body>
    <div class="cylinder-wrapper">
        <div class="cylinder-value1" id="1" data-value="40"></div>
        <div class="cylinder-value2" id="2" data-value="40"></div>
    </div>
    
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
    
    
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
    
    <script src="jquery/jquery-ui.min.js"></script>
    
    <script>
        $(function() {
            for(var i=1;i<=2;i++){
                var id1 = '#'+i;
                var value1 = $(id1).data("value");
                value1 = value1 + '%';
                $(id1).css("height",value1);
                
                i++;
                var id2 = '#'+i;
                var value2 = $(id2).data("value");
                value2 = value2 + '%';
                $(id2).css("height",value2);
                $(id2).css("bottom",value1);
            }
        });
    </script>
</body>
</html>

<?php
	include("scriptCalls.php");
	include("wrapper.php");
    /**
    * The parameters of the constructor are as follows
    * chartType   {String} 
    * chartId     {String} 
    * chartWidth  {String} 
    * chartHeight {String} 
    * containerId {String}
    * dataFormat  {String}
    * dataSource  {String} 
    */

function createChart($i,$name,$value,$color,$unit){

	$angularChart = new FusionCharts("cylinder", "ex$i" , "170", "270", "chart-$i", "json", '{
      "chart": {
	  		"manageresize": "1",
        	"caption": "'.$name.'",
        	"subcaption": "..subcaption?..",
            "captionAlignment": "left",
            "lowerLimit": "0",
            "upperLimit": "100",
            "lowerLimitDisplay": "Empty",
            "upperLimitDisplay": "Full",
            "numberSuffix": " '.$unit.'",
            "showValue": "1",
            //Customizing the origin x position
            "cyloriginx": "50",
            //Customizing the origin y position
            "cyloriginy": "215",
            //Setting the radius of the Cylinder
            "cylradius": "40",
            //Setting the height of the Cylinder
            "cylheight": "150",
			"bgColor": "none",
			"showBorder": "0",
			"cylfillcolor": "'.$color.'"
      },
	  "value": "'.$value.'"

    }');
	// Render the chart
	$angularChart->render();
}

createChart(1,'chart1',35,'#428bca','m');
createChart(2,'chart2',65,'#428bca','in');
createChart(3,'chart3',85,'#428bca','ft');
createChart(4,'chart4',10,'#428bca','ltrs');
createChart(5,'chart5',27,'#428bca','cm');

?>
<span id="chart-1" style="display:inline;"></span>
<span id="chart-2" style="display:inline;"></span>
<span id="chart-3" style="display:inline;"></span>
<span id="chart-4" style="display:inline;"></span>
<span id="chart-5" style="display:inline;"></span>

</body>
</html>
<?php
	include("testfunction.php");
?>
<html>
<head>
<script type="text/javascript" src="js/fusioncharts.js"></script>
<script type="text/javascript" src="js/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>
</head>
<body>
	<script>
		FusionCharts.ready(function(){
			var chart = new FusionCharts({
				<?php echo createChart(13,'my test tank',0,20); ?>
			});
			chart.render();
		});
		
	</script>
	<div id="chart-container"></div>
</body>
</html>
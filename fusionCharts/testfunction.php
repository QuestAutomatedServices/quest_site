<?php

function createChart($value,$name,$lowValue,$highValue){
	
	$script = 'type: "cylinder",
	dataFormat: "json",
    id: "fuelMeter-2",
    renderAt: "chart-container",
    width: "250",
    height: "350",
    dataSource: {
        "chart": {
            "theme": "fint",
            "caption": "'.$name.'",
            "subcaption": "Bakersfield Central",
            "captionAlignment": "left",
            "lowerLimit": "'.$lowValue.'",
            "upperLimit": "'.$highValue.'",
            "lowerLimitDisplay": "Empty",
            "upperLimitDisplay": "Full",
            "numberSuffix": " ltrs",
            "showValue": "1",
            //Customizing the origin x position
            "cyloriginx": "50",
            //Customizing the origin y position
            "cyloriginy": "260",
            //Setting the radius of the Cylinder
            "cylradius": "40",
            //Setting the height of the Cylinder
            "cylheight": "150"
        },
        "value1": "'.$value.'",
        "value2": "90"
    }';
	
	return $script;
}

?>
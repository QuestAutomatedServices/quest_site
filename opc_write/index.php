<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Quest Automated Services</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="../reporting/css/dashboard.css"/>
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>

</head>

<body>

	<div id="header">

		<div id="logo"><img src="../reporting/images/logo.jpg" width="260" height="80" /></div>
        
      </div>
		
<!--	
	<h1 style="color: crimson;">DEVELOPMENT MODIFICATIONS ONGOING!</h1>-->
	<div id="login" style="height: auto; padding: 30px;">
   
   					
   					<p>Consecutive Low Fluid Violations Allowed</p>	 
   					<p>Number of consecutive strokes that the Low Fluid Load setpoint has been violated.</p>
					<p>ID: 42239</p>

    				
                	<form action="process/post.php" method="post">
                	<input type="hidden" name="nodeid" value="ns=1;s=Site1.Controller1.Object1.Device1.Operation3.Group1.Tag1" />
                
                	<label>Value</label> <input name="value" type="text" />
					<input name="submit" type="submit" value="Submit" />      
                    

                </form>
    		
    
    </div>
        
        	

</body>
</html>
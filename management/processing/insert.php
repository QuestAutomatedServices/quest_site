<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: ../../quest/login.php");
	exit();
}

include("../functions/functions.php");
include("../user_mail.php");

	/*$info = getUserMailInfo($_POST['userID']);
	sendUserMail($info);*/

if ($_POST['pt'] == 1) {
	$POST['project_dueDate'] = formatDateMYSQL($_POST['project_dueDate']);
	$table = "tbl_projects";
	$location = "../management.php?f=getProjects";
}elseif ($_POST['pt'] == 2){
	$table = "tbl_messages";
	if(isset($_POST['message_projectID'])){
		$pid = $_POST['message_projectID'];
		$location = "../management.php?f=getProject&p=$pid";
	}elseif(isset($_POST['message_taskID'])){
		$tid = $_POST['message_taskID'];
		$location = "../management.php?f=getTask&t=$tid";
	}
}elseif($_POST['pt'] == 3){
	$table = "tbl_tasks";
	$pid = $_POST['task_projectID'];
	$location = "../management.php?f=getProject&p=$pid";	
}elseif($_POST['pt'] == 4){
	$table = "tbl_assignments";
	$tid = $_POST['assignment_taskID'];
	$location = "../management.php?f=getTask&t=$tid";
}

unset($_POST['pt']);
insertRecord($_POST,$table);

header("location: $location ");

?>
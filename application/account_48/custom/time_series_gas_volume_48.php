<?php 
if ($_GET['start']) {
	$regdates = date_range($_GET['start'], $_GET['end']);	
} else {
	$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
}

$array = '';

foreach ($regdates as $pdate) {  
	
	$total = 0;
	$total += getReadingDateHour(253,202,$pdate,12,48);
	$total += getReadingDateHour(253,212,$pdate,12,48);
	$total += getReadingDateHour(253,221,$pdate,12,48);
	

	$array[] = array(strtotime($pdate)*1000,intval($total));	
}

echo json_encode($array);

?>

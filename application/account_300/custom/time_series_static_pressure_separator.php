<?php 
$newarray = '';
$array = '';

if ($_GET['start']) {
	$startdate = formatDateMYSQL($_GET['start']);
	$today = formatDateMYSQL($_GET['end']);
} else {
	$startdate = date('Y-m-d', strtotime("-7 days"));
	$today = date('Y-m-d');	
}

$tags = getOPCTagsByObject($_GET['ob'],300);

$oiltanks = array('Charlotte_TP_Separator_Tags_TS_204_Static_Pressure_2000','Charlotte_TP_Separator_Tags_TS_205_Static_Pressure_2000','Charlotte_TP_Separator_Tags_TS_512_Static_Pressure_2000','Charlotte_TP_Separator_Tags_TS_513_Static_Pressure_2000');

foreach ($tags as $tag) {
	
	if (in_array($tag['map_friendly_name'],$oiltanks)) {
		$readings = getOPCReadings($tag['mapID'],300); 
	}
}


foreach ($readings as $result) {

		$array[] = array(((strtotime(formatTimestampOffset($result['register_date'],5)))*1000),$result['register_reading']);	
	}

echo json_encode($array,JSON_NUMERIC_CHECK);


?>
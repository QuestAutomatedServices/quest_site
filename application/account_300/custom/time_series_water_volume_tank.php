<?php 
$newarray = '';
$array = '';

if ($_GET['start']) {
	$startdate = formatDateMYSQL($_GET['start']);
	$today = formatDateMYSQL($_GET['end']);
} else {
	$startdate = date('Y-m-d', strtotime("-1 days"));
	$today = date('Y-m-d');	
}

$tags = getOPCTagsByObject($_GET['ob'],300);

$oiltanks = array('Charlotte_Oil_Tanks_Oil_Tank_11_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_12_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_13_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_14_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_15_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_16_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_17_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_18_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_19_Water_Volume','Charlotte_Oil_Tanks_Oil_Tank_20_Water_Volume');

foreach ($tags as $tag) {
	
	if (in_array($tag['map_friendly_name'],$oiltanks)) {
		$readings = getOPCReadings($tag['mapID'],300); 
	}
}


foreach ($readings as $result) {

		$array[] = array(((strtotime($result['register_date']))*1000),$result['register_reading']);	
	}

echo json_encode($array,JSON_NUMERIC_CHECK);


?>
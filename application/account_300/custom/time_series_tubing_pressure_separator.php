<?php 
$newarray = '';
$array = '';

if ($_GET['start']) {
	$startdate = formatDateMYSQL($_GET['start']);
	$today = formatDateMYSQL($_GET['end']);
} else {
	$startdate = date('Y-m-d', strtotime("-7 days"));
	$today = date('Y-m-d');	
}

$tags = getOPCTagsByObject($_GET['ob'],300);

$oiltanks = array('Charlotte_TP_Separator_Tags_TS_204_Tubing_Pressure','Charlotte_TP_Separator_Tags_TS_205_Tubing_Pressure','Charlotte_TP_Separator_Tags_TS_512_Tubing_Pressure','Charlotte_TP_Separator_Tags_TS_513_Tubing_Pressure');

foreach ($tags as $tag) {
	
	if (in_array($tag['map_friendly_name'],$oiltanks)) {
		$readings = getOPCReadings($tag['mapID'],300); 
	}
}


foreach ($readings as $result) {

		$array[] = array(((strtotime(formatTimestampOffset($result['register_date'],5)))*1000),$result['register_reading']);	
	}

echo json_encode($array,JSON_NUMERIC_CHECK);


?>
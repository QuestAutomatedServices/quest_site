<?php 
include('/var/www/html/application/includes/site.php');
$regions = array('Texas');

//$devarray = array(1=>'MR 1A',18=>'NWR 204',19=>'NWR 205',20=>'NWR 512',21=>'NWR 513');
$devarray = array(18=>'NWR 204',19=>'NWR 205',20=>'NWR 512',21=>'NWR 513');

if (!isset($_GET['start']) && $_SESSION['chart_date_start'] != '')  { 
		$_GET['start'] = $_SESSION['chart_date_start'];
		$_GET['end'] = $_SESSION['chart_date_end'];			   
				   
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

       <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
     
      <div class="container">
       
        <div class="navbar-header">
         
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#"> PetroEdge Energy IV</a>
       

        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
           
            <li <?php if (!isset($_GET['r']) && !isset($_GET['f'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            
				<li <?php if ($_GET['r'] ==  ($key+1) && !isset($_GET['d']) && !isset($_GET['tb'])) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
             
 		<?php if (isset($_GET['r'])) { ?>
         
            <ul class="nav navbar-nav navbar-right">



					<?php 
						foreach ($devarray as $d=>$device) {

								if ($_GET['d'] == $d) {
									echo '<li class="active"><a href="dashboard.php?r=1&d='.$d.'&s=6">'.$device.'</a></li>';
								} else {
									echo '<li><a href="dashboard.php?r=1&d='.$d.'&s=6">'.$device.'</a></li>';
								}

						}
				?>
            	
          </ul>
         <?php } ?>


		 
		   <ul class="nav navbar-nav navbar-right">
		    <li><a href="../alarms_champion/alarms.php">Alarms</a></li>
			 <?php 						  
					if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
						echo '<li><a href="dashboard.php?f=getAccountUsers">Users</a></li>';
					}
					echo '<li><a href="dashboard.php?f=getCurrentUser">Account</a></li>';
				?>
				
			<li><a href="../support/support.php">Support</a></li>
		
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           
            <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            

				<li <?php if ($_GET['r'] ==  ($key+1)) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
          
          
         <?php if (isset($_GET['r'])) { ?>
          <ul class="nav nav-list">
           
           <li class="nav-header">Remote Access</li>
           <li><a href="http://166.130.155.222:2051" target="_blank">Charlotte HMI</a></li>
           	<!--<li><a href="dashboard.php?r=1&vm=1">Volumes</a></li>-->
           	
           	
           	<li class="nav-header">Sites</li>
           	
           	<li><a href="dashboard.php?r=1&site=1">Charlotte</a></li>
           	
           	<ul>
            
            <?php 
				
					foreach ($devarray as $d=>$device) {
						
							if ($_GET['d'] == $d) {
								echo '<li class="active"><a href="dashboard.php?r=1&d='.$d.'&s=5&ob='.$d.'">'.$device.'</a></li>';
							} else {
								echo '<li><a href="dashboard.php?r=1&d='.$d.'&s=5&ob='.$d.'">'.$device.'</a></li>';
							}
						
					}
	
	
				//echo '<ul><li><a href="dashboard.php?r=1&d=239&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=64">Oil Tank #8393</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=70">Oil Tank #8394</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=76">Oil Tank #8395</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=82">Oil Tank #8396</a></li></ul>';
	
				
			?>
			  
            
             <?php 
	
					$tanknum = 55;
					$object = 7;
				
					for ($i=1;$i<=10;$i++) {
						
							
							if ($_GET['t'] == $i) {
								echo '<li class="active"><a href="dashboard.php?r=1&t='.$i.'&s=5&tn='.$tanknum.'&ob='.$object.'">Tank '.$tanknum.'</a></li>';
							} else {
								echo '<li><a href="dashboard.php?r=1&t='.$i.'&s=5&tn='.$tanknum.'&ob='.$object.'">Tank  '.$tanknum.'</a></li>';
							}
						
							$tanknum++;
							$object++;
					}
	
			
			?>
         
         </ul>
          </ul>
         <?php } ?>
         
          
         <ul class="nav nav-sidebar">
              
                  
                 <?php 
						 if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
							  if ($_GET['f'] == 'getAccountUsersTwo'){
								  echo '<li class="active"><a href="dashboard.php?f=getAccountUsersTwo">Users</a></li>';
							  }else{
								  echo '<li><a href="dashboard.php?f=getAccountUsersTwo">Users</a></li>';
							  }
						}
			 
			 			if ($_GET['f'] == 'getCurrentUser'){
 								
								echo '<li class="active"><a href="dashboard.php?f=getCurrentUser">Account</a></li>';			
							  }else{
								   echo '<li><a href="dashboard.php?f=getCurrentUser">Account</a></li>';
						 }
			 
			 
			 
			 ?>
                   
			<li><a href="../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>
          </ul>
         
         
       
        
        </div>
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<?php if(!isset($_GET['f'])){
					
			?>
         
          <h1 class="page-header">
          
          
          <?php 
	
	
					if (!isset($_GET['r']) && !isset($_GET['vm']) && !isset($_GET['eq'])  && !isset($_GET['site'])) { 
						echo 'Global Overview'; 
					} else if (isset($_GET['site'])) { 
						echo 'Charlotte Overview';
					} else if (isset($_GET['vm'])) { 
						echo 'Volumes Reporting'; 
					} else if (isset($_GET['eq'])) { 
						echo 'Equipment Reporting'; 
					} else if (!isset($_GET['d']) && !isset($_GET['t'])) { 
						echo $regions[$_GET['r']-1].' Overview'; 
					} else if (isset($_GET['t'])) { 
						echo 'Tank '.$_GET['tn']; 
					} else {
						echo $devarray[$_GET['d']].''; 
					}
			  
			  ?>
                    			
          	</h1>

			<?php ///////filemanager
			} else {
				echo $_GET['f']();
			}?>




	  <!--File Manager-->
		<?php  if ($_GET['fm'] == 1) { ?>  

			<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

		<?php  } ?> 
		<!--File Manager--> 
        
        
       <!--Equipment Reporting-->
		<?php  if ($_GET['eq'] == 1) { ?>  
       
     		<table class="table table-bordered">
     		  <tr class="info"><td colspan="9">Compressor</td></tr>
			  <tr>
				<td align="center" colspan="2">Suction</td>
				<td align="center" colspan="2">1st Stage</td>
				<td align="center" colspan="2">2nd Stage</td>
				<td align="center" colspan="2">3rd Stage</td>
				<td>Run Time</td>
			  </tr>
			  <tr>
				<td>Press</td>
				<td>Temp</td>
				<td>Press</td>
				<td>Temp</td>
				<td>Press</td>
				<td>Temp</td>
				<td>Press</td>
				<td>Temp</td>
				<td>Hours</td>
			  </tr>
			  <tr>
				<td align="right">80</td>
				<td align="right">74</td>
				<td align="right">160</td>
				<td align="right">190</td>
				<td align="right">380</td>
				<td align="right">180</td>
				<td align="right">650</td>
				<td align="right">125</td>
				<td align="right">23.75</td>
			  </tr>
			</table>
       
       
       <table class="table table-bordered">
		<tr class="info"><td colspan="6">Pipe Line</td></tr>
		  <tr>
			<td align="center" colspan="2">Pad 2</td>
			<td align="center" colspan="2">Pad 5 PL Pressures</td>
			<td align="center" colspan="2">Facility Pressures</td>
		  </tr>
		  <tr>
			<td>Liquids</td>
			<td>Gas</td>
			<td>Liquids</td>
			<td>Gas</td>
			<td>Liquids</td>
			<td>Gas</td>
		  </tr>
		  <tr>
			<td align="right">150</td>
			<td align="right">140</td>
			<td align="right">150</td>
			<td align="right">165</td>
			<td align="right">120</td>
			<td align="right">120</td>
		  </tr>
		</table>
       
       
       <table class="table table-bordered">
		<tr class="info"><td colspan="3">Facility Equipment</td></tr>
		  <tr>
			<td>&nbsp;</td>
			<td>Press</td>
			<td>Temp </td>
		  </tr>
		  <tr>
			<td>HT</td>
			<td align="right">48</td>
			<td align="right">140</td>
		  </tr>
		  <tr>
			<td>2 Phase</td>
			<td align="right">114</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>3 Phase</td>
			<td align="right">114</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>Gnerator KO</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		  </tr>
		  <tr>
			<td>Gens On Line</td>
			<td align="right">2</td>
			<td>&nbsp;</td>
		  </tr>
		</table>
        
        <?php  } ?> 
        <!--Equipment Reporting-->
        
		<!--Volumes Reporting-->
		<?php  if ($_GET['vm'] == 1) { 
			
				if (!isset($_GET['date'])) {
					$thisdate = date("m/d/Y");
				} else {
					$thisdate = $_GET['date'];
				}
	
	
				$curdate = formatDateMYSQL($thisdate);
				$curdateplusone = date("Y-m-d", strtotime($curdate ." +1 day"));
	
				$oilonhandstart = 0;
				$oilonhandend = 0;
			
				?>  
			 
			 <h3><form action="dashboard.php" class="form-inline"><input type="hidden" name="r" value="1"><input type="hidden" name="vm" value="1"><input class="form-control datepicker" type="text" name="date" style="width:300px;" value="<?php echo $thisdate; ?>">  <button type="submit" class="btn btn-default">Submit</button> </form></h3>

			 <div class="row">
			 
			 	<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="success">
						  
						  <?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object7_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object7_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object7_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object7_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Oil Tank #55</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object12_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object12_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object12_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object12_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #60</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="info">
							<?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object1_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object1_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object1_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object1_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								//$oilonhandstart += $oilbblsstart;
								//$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Water Tank #1</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				
				<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="info">
							<?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object4_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object4_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object4_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object4_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								//$oilonhandstart += $oilbblsstart;
								//$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Water Tank #4</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
			 
			</div>
			
			<div class="row">
			 
			 	<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object8_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object8_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object8_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object8_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #56</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object13_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object13_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object13_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object13_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;tart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #61</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="info">
							<?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object2_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object2_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object2_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object2_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								//$oilonhandstart += $oilbblsstart;
								//$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Water Tank #2</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				
				<div class="col-md-3">
				  
						<table class="table table-bordered">
						  <tr class="info">
							<?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object5_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object5_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object5_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object5_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								//$oilonhandstart += $oilbblsstart;
								//$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Water Tank #5</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
			 
			</div>
			
			<div class="row">
			 
			 	<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object9_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object9_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object9_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object9_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;						  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #57</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object14_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object14_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object14_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object14_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #62</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
				</div>
				
				<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="info">
							<?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object3_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object3_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object3_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object3_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								//$oilonhandstart += $oilbblsstart;
								//$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Water Tank #3</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				
				<div class="col-md-3">
				  
					  <table class="table table-bordered">
						  <tr class="info">
							<?php 
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object6_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object6_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object6_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object6_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								//$oilonhandstart += $oilbblsstart;
								//$oilonhandend += $oilbblsend;
							  ?>
							<td colspan="6" align="center" width="366">Water Tank #6</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
				</div>
			 
			</div>
			
			<div class="row">
			 
			 	<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object10_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object10_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object10_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object10_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #58</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object15_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object15_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object15_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object15_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #63</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-6">
			  
					   <table class="table table-bordered">
								  <tr class="success">
									<td colspan="12" align="center">Oil</td>
								  </tr>
								  <tr>
									<td>Ticket #</td>
									<td>Bbls</td>
									<td>Ticket #</td>
									<td>Bbls</td>
									<td>Ticket #</td>
									<td>Bbls</td>
									<td>Ticket #</td>
									<td>Bbls</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								</table>
				  
					 
				</div>
				
		 
			</div>
			
			
		<div class="row">
			 
			 	<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object11_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object11_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object11_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object11_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #59</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-3">
				  
					 <table class="table table-bordered">
						  <tr class="success">
						  
						  	<?php 			
	
								$starttop = 0;
								$endtop = 0;
							 	$leveltopf = 0;
								$leveltopi = 0;
								$levelbottomf = 0;
								$levelbottomi = 0;
								$oilbblsstart = 0;
								$waterbblsstart = 0;
						  		$leveltopiend = 0;
								$levelbottomfend = 0;
								$levelbottomiend = 0;
								$oilbblsend = 0;
								$waterbblsend = 0;
	
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object16_Device1_Operation1_Group1_Tag1',$curdate,300);
												
								$starttoppieces = explode(".",$starttop);
								$leveltopf = $starttoppieces[0];
								$leveltopi = round(($starttoppieces[1]*12)/100,0);
	
												
								$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object16_Device1_Operation1_Group1_Tag2',$curdate,300);

								$starttoppieces = explode(".",$starttop);
								$levelbottomf = $starttoppieces[0];
								$levelbottomi = round(($starttoppieces[1]*12)/100,0);
								
								$waterbblsstart = (($levelbottomf*12) + $levelbottomi) * 2.78;
								$oilbblsstart = ((($leveltopf*12) + $leveltopi) * 2.78) - $waterbblsstart;
	
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object16_Device1_Operation1_Group1_Tag1',$curdateplusone,300);
												
								$endtoppieces = explode(".",$endtop);
								$leveltopfend = $endtoppieces[0];
								$leveltopiend = round(($endtoppieces[1]*12)/100,0);
	
								$endtop = getOPCReadingByTagDateTime('Site1_Controller2_Object16_Device1_Operation1_Group1_Tag2',$curdateplusone,300);

								$endtoppieces = explode(".",$endtop);
								$levelbottomfend = $endtoppieces[0];
								$levelbottomiend = round(($endtoppieces[1]*12)/100,0);
												
							  	$waterbblsend = (($levelbottomfend*12) + $levelbottomiend) * 2.78;
								$oilbblsend = ((($leveltopfend*12) + $leveltopiend) * 2.78) - $waterbblsend;
	
								$oilonhandstart += $oilbblsstart;
								$oilonhandend += $oilbblsend;
							  ?>
						  
							<td colspan="6" align="center" width="366">Oil Tank #64</td>
						  </tr>
						  <tr>
							<td colspan="2" align="center">Top</td>
							<td colspan="2" align="center">Bottom</td>
							<td>Oil</td>
							<td>Water</td>
						  </tr>
						  <tr>
							<td>Feet</td>
							<td>Inches</td>
							<td>Feet</td>
							<td>Inches</td>
							<td>BBLS</td>
							<td>BBLS</td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopf; ?></td>
							<td align="right"><?php echo $leveltopi; ?></td>
							<td align="right"><?php echo $levelbottomf; ?></td>
							<td align="right"><?php echo $levelbottomi; ?></td>
							<td align="right"><?php echo $oilbblsstart; ?></td>
							<td align="right"><?php echo $waterbblsstart; ?></td>
						  </tr>
						  <tr>
							<td align="right"><?php echo $leveltopfend; ?></td>
							<td align="right"><?php echo $leveltopiend; ?></td>
							<td align="right"><?php echo $levelbottomfend; ?></td>
							<td align="right"><?php echo $levelbottomiend; ?></td>
							<td align="right"><?php echo $oilbblsend; ?></td>
							<td align="right"><?php echo $waterbblsend; ?></td>
						  </tr>
						</table>
						
				</div>
				
				<div class="col-md-6">
			  
					   <table class="table table-bordered">
								  <tr class="info">
									<td colspan="12" align="center">Water</td>
								  </tr>
								  <tr>
									<td>Ticket #</td>
									<td>Bbls</td>
									<td>Ticket #</td>
									<td>Bbls</td>
									<td>Ticket #</td>
									<td>Bbls</td>
									<td>Ticket #</td>
									<td>Bbls</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								  <tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								  </tr>
								</table>
				  
					 
				</div>
				
			 
			</div>
			
			
			<div class="row">
			 
			 	<div class="col-md-6">
			 	
			 		<table class="table table-bordered">
					  <tr>
						<td class="success" colspan="3" align="center">Oil</td>
						<td class="info" colspan="3" align="center">Water</td>
						<td class="danger" colspan="4" align="center">Gas </td>
					  </tr>
					  <tr>
						<td>On Hand</td>
						<td>Daily Prod</td>
						<td>Sold</td>
						<td>On Hand</td>
						<td>Daily Prod</td>
						<td>Hauled</td>
						<td>PE IV Comp</td>
						<td>ETC</td>
						<td>PEIV Net</td>
						<td>Flared Gas</td>
					  </tr>
					  <tr>
						<td align="right"><?php echo $oilonhandstart; ?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					  <tr>
						<td align="right"><?php echo $oilonhandend; ?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					</table>
			 	
				</div>
			
				<div class="col-md-6">
				
					   <table class="table table-bordered">
						  <tr class="info">
							<td colspan="12" align="center">Water Pipeline</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td align="center">psi</td>
							<td align="center">Start</td>
							<td align="center">Stop</td>
							<td align="center">Total/d</td>
							<td align="center">Grand Total</td>
						  </tr>
						  <tr>
							<td>Pump 1</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>Pump 2</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						</table>
			 	
				</div>
				
			</div>
			
			
			<div class="row">
			 
			 	<div class="col-md-12">
			 	
				<table class="table table-bordered">
	
					  <tr>
						<td class="active">&nbsp;</td>
						<td class="warning">Tbg</td>
						<td class="warning">Csg</td>
						<td class="warning">Temp</td>
						<td class="warning">Choke</td>
						<td class="warning">Static psi</td>
						<td class="success">Oil</td>
						<td class="info">Water</td>
						<td class="danger">Gas Sep</td>
						<td class="danger">Net Gas</td>
						<td class="danger">GL PSI</td>
						<td class="danger">GL Rate</td>
						<td class="danger">GL Vol</td>
					  </tr>
					  <tr>
						<td>NWR 204</td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object18_Device7_Operation1_Group1_Tag1',$curdate,300); ?></td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object18_Device7_Operation1_Group1_Tag2',$curdate,300); ?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					  </tr>
					  <tr>
						<td>NWR 205</td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object19_Device7_Operation1_Group1_Tag1',$curdate,300); ?></td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object19_Device7_Operation1_Group1_Tag2',$curdate,300); ?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					  <tr>
						<td>NWR 512</td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object20_Device7_Operation1_Group1_Tag1',$curdate,300); ?></td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object20_Device7_Operation1_Group1_Tag2',$curdate,300); ?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					  <tr>
						<td>NWR 513</td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object21_Device7_Operation1_Group1_Tag1',$curdate,300); ?></td>
						<td align="right"><?php echo getOPCReadingByTagDateTime('Site1_Controller2_Object21_Device7_Operation1_Group1_Tag2',$curdate,300); ?></td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					  <tr>
						<td>Total</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
						<td align="right">&nbsp;</td>
					  </tr>
					</table>
			 	
				</div>
			
			</div>
			


		<?php  } ?> 
		<!--Volumes Reporting--> 
        
        
        
         <!--GLOBAL & REGION SECTION--> 
         <?php  if (!isset($_GET['d']) && !isset($_GET['c'])  && !isset($_GET['eq']) && !isset($_GET['vm']) && !isset($_GET['f']) && !isset($_GET['t'])) { ?> 
         
             <div class="btn-group topbuttons">

                  <a href="../application/account_300/custom/set_chart.php?t=1&r=<?php echo $_GET['r']; ?>" class="btn btn-primary">Line Chart</a>
                  <a href="../application/account_300/custom/set_chart.php?t=2&r=<?php echo $_GET['r']; ?>" class="btn btn-primary">Waterfall Chart</a>
                  <a href="../application/account_300/custom/set_chart.php?t=3&r=<?php echo $_GET['r']; ?>" class="btn btn-primary">Bar Chart</a>
                  <a href="../application/account_300/custom/set_chart.php?t=4&r=<?php echo $_GET['r']; ?>" class="btn btn-primary">Scatter Plot Chart</a>
                  

             </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
			    
             <div id="chart"></div>
             
             <div class="row">
             
             	<?php
					
						if ($_GET['start']) {
							$dates = date_range($_GET['start'], $_GET['end']);	
						} else {
							$dates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
						}
			
						
						rsort($dates);
						
            	?>
            	
            	<div class="table-responsive" style="margin-top: 50px;">
            	 <table class="table table-bordered table-striped">

						  <tr>
							<th>Date</th>
							<th>Oil Production</th>
							<th>Water Production</th>
							<th>Gas Production</th>
						  </tr>
						  
						      <?php
							  
							  		$i = 100;
									
									foreach ($dates as $date) {
										
										echo '<tr>
												<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#'.$i.'" aria-hidden="true" title="Show Detail"></i> '.formatDate($date).'</td>
												<td>'.getOilProductionOPCUA($date,300).'</td>
												<td>'.getWaterProductionOPCUA($date,300).'</td>
												<td>'.getGasProductionOPCUA($date,300).'</td>
											  </tr>
										
										
											<tr id="'.$i.'" class="collapse" style="background-color:#ccc;"> <!--WELL COLLAPSE ROW-->
											
												<td colspan="4">
												
												          <table class="table table-bordered table-striped">
														  	
														 <tr>
															<th colspan="4">'.formatDate($date).'</th>

														  </tr>
														  
														  <tr>
															<th>Well</th>
															<th>Oil Production</th>
															<th>Water Production</th>
															<th>Gas Production</th>
														  </tr>
														  
														  <tr>
															<td><a href="dashboard.php?r=1&d=18&s=5&ob=18">'.$devarray[18].'</a></td>
															<td>'.getOilProductionOPCUATag($date,300,18).'</td>
															<td>'.getWaterProductionOPCUATag($date,300,18).'</td>
															<td>'.getGasProductionOPCUATag($date,300,18).'</td>
														  </tr>
														  
														  <tr>
															<td><a href="dashboard.php?r=1&d=19&s=5&ob=19">'.$devarray[19].'</a></td>
															<td>'.getOilProductionOPCUATag($date,300,19).'</td>
															<td>'.getWaterProductionOPCUATag($date,300,19).'</td>
															<td>'.getGasProductionOPCUATag($date,300,19).'</td>
														  </tr>
														  
														  <tr>
															<td><a href="dashboard.php?r=1&d=20&s=5&ob=20">'.$devarray[20].'</a></td>
															<td>'.getOilProductionOPCUATag($date,300,20).'</td>
															<td>'.getWaterProductionOPCUATag($date,300,20).'</td>
															<td>'.getGasProductionOPCUATag($date,300,20).'</td>
														  </tr>
														  
														  <tr>
															<td><a href="dashboard.php?r=1&d=21&s=5&ob=21">'.$devarray[21].'</a></td>
															<td>'.getOilProductionOPCUATag($date,300,21).'</td>
															<td>'.getWaterProductionOPCUATag($date,300,21).'</td>
															<td>'.getGasProductionOPCUATag($date,300,21).'</td>
														  </tr>

														  </table>
												
												</td>

											
											</tr>';
								
											
										
										$i++;
									}

							?>

				 </table>
				 </div>
            </div>
                  
         <?php  } ?> 
		  <!--GLOBAL & REGION SECTION--> 
       
       
       	<!--DEVICE SECTION--> 
         <?php  if (isset($_GET['d']) && !isset($_GET['c']) && !isset($_GET['f'])) { 
							  
							  
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);	  
							  
							  ?> 
							  
			    <div class="btn-group topbuttons">
                 
                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=5&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 5) echo 'active' ?>">Overview</a>
                 
                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=6&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == base64_encode()) echo 'active' ?>">Oil</a>

                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=7&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == base64_encode()) echo 'active' ?>">Water</a>
                                
                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=8&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == base64_encode()) echo 'active' ?>">Gas</a>
                                                
                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=9&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == base64_encode()) echo 'active' ?>">Pressures</a>
                 
                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Live Readings</a>
                  
	
              </div>
              
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
        
        	<div id="chart"></div>
        	
        	
        	<?php  if ($_GET['s'] == 1) { ?> 
        	
        		<div class="table-responsive" style="margin-top: 50px;">

							<table class="table table-striped">
							  <thead>

							<tr><th colspan="6" class="info">LIVE READINGS</th></tr>

							<tr>
								<th width="35%">Tag Name</th>
								<th width="30%">Friendly Name</th>
								<th width="20%">Last Reading</th>
								<th>View Readings</th>
							</tr>
							  </thead>
							  <tbody>
							  
									<?php 
										
										
								  		$tags = getOPCTagsByObject($_GET['ob'],300);
	
										//echo "<pre>";
										//print_r($tags);

										foreach ($tags as $tag) {
											
												if (isset($_GET['v']) && $_GET['v'] == $tag['mapID'] ) {
													$class = ' class="warning" ';
												} else {
													$class = '';
												}
												

										 		echo '<tr '.$class.'><td>'.$tag['map_opcua_convention'].'</td>
										 			<td>'.$tag['map_friendly_name'].'</td>
													<td>'.getLastOPCReadingByTag($tag['map_opcua_convention'],300).'</td>
													<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&s='.$_GET['s'].'&ob='.$_GET['ob'].'&v='.$tag['mapID'].'">View</td>
													
													</tr>';
										}
								  
								  	
								  ?>

								
							</tbody>

						</table>
			</div>
			    
			    			<?php  if (isset($_GET['v'])) {  ?>
									  
						<table class="table table-striped">
						  <thead>
							<tr>
								<th width="35%">Tag Name</th>
								<th width="30%">Friendly Name</th>
							  <th width="20%">Timestamp</th>
							  <th>Reading</th>
							</tr>
							</thead>
						  <tbody>
							
						<?php
									  
									  
							$readings = getOPCReadings($_GET['v'],300); 
															 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td class="fullview">'.$reading['map_opcua_convention'].'</td>
											<td class="fullview">'.$reading['map_friendly_name'].'</td>
											<td class="fullview">'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
								
							</tbody>

						</table>

										
								
						<?php 	  } ?> 
            
        	
        	 
        	  <?php  } ?>  
        	  
        	  
        	 
        
         <?php  } ?> 
       	<!--DEVICE SECTION--> 
       	
       	<!--WELLS SECTION--> 
       	
       	
       	<!--WELLS SECTION--> 
       	
       	
       	 <!--TANK SECTION--> 
       	  <?php  if (isset($_GET['t']) && !isset($_GET['c']) && !isset($_GET['f'])) { ?> 
       	  
       	  	    <div class="btn-group topbuttons">
                 
                <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&t=<?php echo $_GET['t']; ?>&s=5&tn=<?php echo $_GET['tn']; ?>&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 5) echo 'active' ?>">Chart</a>
                   
<?php /*?>                 <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&t=<?php echo $_GET['t']; ?>&s=1&tn=<?php echo $_GET['tn']; ?>&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Levels</a>
                 
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&t=<?php echo $_GET['t']; ?>&s=2&tn=<?php echo $_GET['tn']; ?>&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Run Tickets</a>
                                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&t=<?php echo $_GET['t']; ?>&s=3&tn=<?php echo $_GET['tn']; ?>&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 3) echo 'active' ?>">Sales Tickets</a>
<?php */?>
                 
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&t=<?php echo $_GET['t']; ?>&s=4&tn=<?php echo $_GET['tn']; ?>&ob=<?php echo $_GET['ob']; ?>" class="btn btn-primary <?php if ($_GET['s'] == 4) echo 'active' ?>">Live Readings</a>
                  
	
              </div>
              
             <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
              
                <?php  if ($_GET['s'] == 1) { //////levels 
						
								$regdates = date_range(date('Y-m-1'), date("Y-m-t"));	

								//print_r($regdates);
						
			
					?> 
				 	 <form method="post" action="/application/account_300/custom/tank_levels.php" style="margin-top: 50px;margin-bottom: 20px;">
				 	 <input type="hidden" name="tank_number" value="<?php echo $_GET['tn']; ?>">
				 	 <input type="hidden" name="start_date" value="<?php echo $regdates[0]; ?>">

							<div class="table-responsive">

							<table class="table table-striped">
							  <thead>

							<tr><th colspan="5" class="info">MANUAL LEVEL REPORTING</th></tr>

							<tr>
								<th>Report Date</th>
								 <th>Top</th>
								 <th>Bottom</th>
								 <th>Barrels</th>
							</tr>
							  </thead>
							  <tbody>

								<?php 

									$levels = getLevels($_GET['tn'],$regdates[0],$_SESSION['user_accountID']);
									$levels = unserialize($levels[0]['level_array']);
									//echo "<pre>";
									//print_r($levels);
									
									$i = 0;
									foreach ($regdates as $curdate) {
										
										if ($levels['top']['feet'][$i] == '') {
											
											//if ($_GET['ob'] = 7) {
												
												$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object'.$_GET['ob']. '_Device1_Operation1_Group1_Tag1',$curdate,300);
												
												$starttoppieces = explode(".",$starttop);
												$levels['top']['feet'][$i] = 	$starttoppieces[0];
												$levels['top']['inches'][$i] = 	$starttoppieces[1];
											//}
											
										}
										
										if ($levels['bottom']['feet'][$i] == '') {
											
											//if ($_GET['ob'] = 7) {
												
												$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object'.$_GET['ob']. '_Device1_Operation1_Group1_Tag2',$curdate,300);
												
												$starttoppieces = explode(".",$starttop);
												$levels['bottom']['feet'][$i] = 	$starttoppieces[0];
												$levels['bottom']['inches'][$i] = 	$starttoppieces[1];
											//}
											
										}

										echo '<tr><td>'.formatDate($curdate).'</td>';

										echo '<td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" placeholder="feet" name="top[feet][]" value="'.$levels['top']['feet'][$i].'"><input type="text" class="form-control input-sm" style="width:45%;float:left;" placeholder="inches" name="top[inches][]" value="'.$levels['top']['inches'][$i].'"></td>';

										echo '<td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" placeholder="feet" name="bottom[feet][]" value="'.$levels['bottom']['feet'][$i].'"><input type="text" class="form-control input-sm" style="width:45%;float:left;" placeholder="inches" name="bottom[inches][]" value="'.$levels['bottom']['inches'][$i].'"></td>';

										echo '<td>--calculated--</td>'; 

										echo '</tr>';
										
										$i++;
										
										$curdateplusone = date("Y-m-d", strtotime($curdate ." +1 day"));
										
										if ($levels['top']['feet'][$i] == '') {
											
											//if ($_GET['ob'] = 7) {
												
												$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object'.$_GET['ob']. '_Device1_Operation1_Group1_Tag1',$curdateplusone.' 08:56:00',300);
												
												$starttoppieces = explode(".",$starttop);
												$levels['top']['feet'][$i] = 	$starttoppieces[0];
												$levels['top']['inches'][$i] = 	$starttoppieces[1];
											//}
											
										}
										
										if ($levels['bottom']['feet'][$i] == '') {
											
											//if ($_GET['ob'] = 7) {
												
												$starttop = getOPCReadingByTagDateTime('Site1_Controller2_Object'.$_GET['ob']. '_Device1_Operation1_Group1_Tag2',$curdateplusone.' 08:56:00',300);
												
												$starttoppieces = explode(".",$starttop);
												$levels['bottom']['feet'][$i] = 	$starttoppieces[0];
												$levels['bottom']['inches'][$i] = 	$starttoppieces[1];
											//}
											
										}
										
										echo '<tr><td>=> '.formatDate($curdate).'</td>';

										echo '<td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" placeholder="feet" name="top[feet][]" value="'.$levels['top']['feet'][$i].'"><input type="text" class="form-control input-sm" style="width:45%;float:left;" placeholder="inches" name="top[inches][]" value="'.$levels['top']['inches'][$i].'"></td>';

										echo '<td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" placeholder="feet" name="bottom[feet][]" value="'.$levels['bottom']['feet'][$i].'"><input type="text" class="form-control input-sm" style="width:45%;float:left;" placeholder="inches" name="bottom[inches][]" value="'.$levels['bottom']['inches'][$i].'"></td>';


										echo '<td>--calculated--</td>'; 

										echo '</tr>';

										$i++;


									}


								 ?>


							</tbody>

						</table>

						</div>

					 <button type="submit" class="btn btn-default">Submit</button>

					</form>
							
				  <?php  } ///////levels ?> 
				  
				  <?php  if ($_GET['s'] == 2) { //////run tickets	?> 
				  
				 	 <form method="post" action="/application/account_300/custom/run_ticket.php" style="margin-top: 50px;margin-bottom: 20px;">
				 	 <input type="hidden" name="tank_number" value="<?php echo $_GET['tn']; ?>">

							<div class="table-responsive">

							<table class="table table-striped">
							  <thead>

							<tr><th colspan="5" class="info">RUN TICKETS</th></tr>

							<tr>
								<th>Date</th>
								 <th>Ticket Number</th>
								 <th>Barrels</th>
							</tr>
							  </thead>
							  <tbody>
							  
							  <tr>
							  <td><input type="text" class="form-control input-sm datepicker" style="width:45%;float:left;margin-right:5px;" placeholder="Date" name="ticket_date" value="<?php echo date("m/d/Y"); ?>"></td>
							  
							   <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="ticket_number" placeholder="Ticket Number"></td>
							  
								 <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="ticket_barrels" placeholder="Barrels"></td>
							  </tr>

								
							</tbody>

						</table>

						</div>

					 <button type="submit" class="btn btn-default">Submit</button>

					</form>
					
					
						 <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
								<th>Ticket Number</th>
                               <th>Barrels</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
	
										$tiks = getRunTickets($_GET['tn'],$_SESSION['user_accountID']); 
										
	
							   	   		foreach ($tiks as $tik) {  
												
												
											echo '<tr>
												    <td>'.formatDate($tik['ticket_date']).'</td>
													<td>'.$tik['ticket_number'].'</td>
												  	<td>'.number_format($tik['ticket_barrels'],2).'</td>
												</tr>';
                                    
										}
							       
                               
                               ?>
                              </tbody>
                            </table>
				  
				  
				   <?php  } ///////run tickets ?> 
				   
				   
				   
				    <?php  if ($_GET['s'] == 3) { //////sales tickets	?> 
				  
				 	 <form method="post" action="/application/account_300/custom/sales_ticket.php" style="margin-top: 50px;margin-bottom: 20px;">
				 	 <input type="hidden" name="tank_number" value="<?php echo $_GET['tn']; ?>">

							<div class="table-responsive">

							<table class="table table-striped">
							  <thead>

							<tr><th colspan="6" class="info">SALES TICKETS</th></tr>

							<tr>
								<th>Date</th>
								<th>Lease Number</th>
								 <th>Ticket Number</th>
								 <th>Net Barrels</th>
								 <th>Corr Gravity</th>
								 <th>SSW Barrels</th>
							</tr>
							  </thead>
							  <tbody>
							  
							  <tr>
							  <td><input type="text" class="form-control input-sm datepicker" style="width:45%;float:left;margin-right:5px;" placeholder="Date" name="ticket_date" value="<?php echo date("m/d/Y"); ?>"></td>
							  
							   <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="lease_number" placeholder="Lease Number"></td>
							   
								 <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="ticket_number" placeholder="Ticket Number"></td>
							  
								 <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="net_barrels" placeholder="Net Barrels"></td>
								 
								 <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="corr_gravity" placeholder="Corr Gravity"></td>
								 
								 <td><input type="text" class="form-control input-sm" style="width:45%;float:left;margin-right:5px;" name="ssw_barrels" placeholder="SSW Barrels"></td>
							  </tr>

								
							</tbody>

						</table>

						</div>

					 <button type="submit" class="btn btn-default">Submit</button>

					</form>
					
					
					
						 <table class="table table-striped">
                              <thead>
                                <tr>
                                <th>Date</th>
                                <th>Lease Number</th>
								<th>Ticket Number</th>
                               	<th>Net Barrels</th>
                               	<th>Corr Gravity</th>
                               	<th>SSW Barrels</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
	
										$tiks = getSalesTickets($_GET['tn'],$_SESSION['user_accountID']); 
										
	
							   	   		foreach ($tiks as $tik) {  
												
												
											echo '<tr>
												    <td>'.formatDate($tik['ticket_date']).'</td>
													<td>'.$tik['lease_number'].'</td>
													<td>'.$tik['ticket_number'].'</td>
												  	<td>'.number_format($tik['net_barrels'],2).'</td>
													<td>'.number_format($tik['corr_gravity'],2).'</td>
													<td>'.number_format($tik['ssw_barrels'],2).'</td>
												</tr>';
                                    
										}
							       
                               
                               ?>
                              </tbody>
                            </table>
				  
				  
				   <?php  } ///////sales tickets ?> 
				   
				   
				   
				    <?php  if ($_GET['s'] == 4) { //////live readings	?> 
				    
				    
				    		<div class="table-responsive" style="margin-top: 50px;">

							<table class="table table-striped">
							  <thead>

							<tr><th colspan="6" class="info">LIVE READINGS</th></tr>

							<tr>
								<th width="35%">Tag Name</th>
								<th width="30%">Friendly Name</th>
								<th width="20%">Last Reading</th>
								<th>View Readings</th>
							</tr>
							  </thead>
							  <tbody>
							  
									<?php 
										
										
								  		$tags = getOPCTagsByObject($_GET['ob'],300);
	
										//echo "<pre>";
										//print_r($tags);

										foreach ($tags as $tag) {
											
												if (isset($_GET['v']) && $_GET['v'] == $tag['mapID'] ) {
													$class = ' class="warning" ';
												} else {
													$class = '';
												}
												

										 		echo '<tr '.$class.'><td>'.$tag['map_opcua_convention'].'</td>
										 			<td>'.$tag['map_friendly_name'].'</td>
													<td>'.getLastOPCReadingByTag($tag['map_opcua_convention'],300).'</td>
													<td><a href="dashboard.php?r='.$_GET['r'].'&t='.$_GET['t'].'&s='.$_GET['s'].'&tn='.$_GET['tn'].'&ob='.$_GET['ob'].'&v='.$tag['mapID'].'">View</td>
													
													</tr>';
										}
								  
								  	
								  ?>

								
							</tbody>

						</table>
			</div>
			    
			    			<?php  if (isset($_GET['v'])) {  ?>
									  
						<table class="table table-striped">
						  <thead>
							<tr>
								<th width="35%">Tag Name</th>
								<th width="30%">Friendly Name</th>
							  <th width="20%">Timestamp</th>
							  <th>Reading</th>
							</tr>
							</thead>
						  <tbody>
							
						<?php
									  
									  
							$readings = getOPCReadings($_GET['v'],300); 
															 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td class="fullview">'.$reading['opcua_convention'].'</td>
											<td class="fullview">'.$reading['map_friendly_name'].'</td>
											<td class="fullview">'.formatTimestamp($reading['register_date']).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
								
							</tbody>

						</table>

										
								
						<?php 	  } ?> 
				    
				    
				    
				     <?php  } ///////live readings ?> 
				     
				     
				      <?php  if ($_GET['s'] == 5) { //////chart	?> 
				     
				     <div id="chart"></div>
				     
				     	<?php 	  } //////chart ?> 
       
          <?php  } ?> 
        <!--TANK SECTION--> 
        
        <!--CONFIG TAG SECTION--> 
         <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getOPCTagName($_GET['g'],$_SESSION['user_accountID']); ?> <a href="/application/account_300/excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getOPCTagName($_GET['g'],$_SESSION['user_accountID']); ?> <a href="/application/account_300/excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
							  <th class="fullview">Tag</th>
							  <th>Timestamp</th>
							  <th>Reading</th>
							</tr>
							
						<?php $readings = getOPCReadings($_GET['g'],$_SESSION['user_accountID']); 
									 
							//print_r($readings);

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td class="fullview">'.$reading['tag_plc_name'].'</td>
											<td class="fullview">'.formatTimestamp($reading['register_date']).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  </thead>
						  <tbody>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Tag Manager</h2>
          	  	
          	 
            
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>OPC Tag</th>
							  <th>Friendly Name</th>
							  <th>Config</th>
							  <th>Measurement Factor</th>
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getOPCTagsByDevice($_GET['d'],$_SESSION['user_accountID']); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {

								
								
								if ($reg['tag_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}

								echo '<tr '.$configset.'>
										<td id="'.$reg['mapID'].'">'.$reg['tag_plc_name'].'</td>
										
										<td><input name="friendly_name" class="reglabel" data-id="'.$reg['mapID'].'" size="25" type="text" value="'.$reg['friendly_name'].'"> <i class="fa fa-info-circle fa-lg" aria-hidden="true" title="#'.$reg['opcua_convention'].'"></i></td>
										
										<td>'.getOPCTagConfigSelect($reg['mapID'],$reg['tag_config']).'</td>
										
										<td><input name="measurement_factor" class="regfactor" data-id="'.$reg['mapID'].'" size="8" type="text" value="'.$reg['measurement_factor'].'"></td>
										
										<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['mapID'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  
          <!--CONFIG TAG SECTION--> 
        

         
          <!--CHART CHOOSE MODAL -->  
            
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="../application/account_300/custom/set_chart.php?t=1&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="../application/account_300/custom/set_chart.php?t=2&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="../application/account_300/custom/set_chart.php?t=3&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>
               <!--CHART CHOOSE MODAL -->  
          
       
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   


	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    

    <?php  if ($_GET['fm']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if (!isset($_GET['fm'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
				   
				   $(".datepicker").datepicker();
				  
						$( "#choosechart" ).click(function() {	
								$( "#chart_modal" ).dialog( "open" );	
						});


						$( "#chart_modal" ).dialog({
						  autoOpen: false,
						  height: 'auto',
						  width: 1050,
						  modal: true,

						});	
					   		
		
 					   		$( "#newlink" ).click(function() {	

								$( "#add_modal" ).dialog( "open" );	
							});

					
							$( "#add_modal" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	


				   
				
			      <?php if ($_GET['start']) { 
				   
				   
				   			$_SESSION['chart_date_start'] = $_GET['start'];
							$_SESSION['chart_date_end'] = $_GET['end'];
				   ?>
				   
				   
				   
				    var start = moment('<?php echo $_SESSION['chart_date_start']; ?>');
					 var end = moment('<?php echo $_SESSION['chart_date_end']; ?>');
				   
				     <?php } else if ($_SESSION['chart_date_start'] != '') { ?>
				   
				   				    var start = moment('<?php echo $_SESSION['chart_date_start']; ?>');
					 var end = moment('<?php echo $_SESSION['chart_date_end']; ?>');
				   

				   <?php } else if ($_GET['s'] == 7 ) { ?>
				   
				   	var start = moment().subtract(1, 'days');
					 var end = moment();
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(7, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] && !$_GET['tn'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&ob=<?php echo $_GET['ob']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['s'] && $_GET['tn'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&t=<?php echo $_GET['t']; ?>&s=<?php echo $_GET['s']; ?>&tn=<?php echo $_GET['tn']; ?>&ob=<?php echo $_GET['ob']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s']  && !$_GET['tb']  ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   	<?php } else if ($_GET['tb']  ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&tb=<?php echo $_GET['tb']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	
 	
   
   	<?php  if (!$_GET['s']) { ?>  
   	
    	<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					   
					     chart: {
								type: '<?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { 
							 		
												echo 'line';
										
											} else if ($_SESSION['chart_type'] == 2 )  {
	
												echo 'waterfall';

											
											} else if ($_SESSION['chart_type'] == 3 )  {

												echo 'bar';
	
											} else if ($_SESSION['chart_type'] == 4 )  {

												echo 'scatter';
											}
							 	?>', 
							 	
							 zoomType: 'x'
							},
					   		
					   
                    title: {
                        text: '<?php 	if (!isset($_GET['r'])) { 
											echo 'Global Overview'; 
										} else if (isset($_GET['site'])) { 
											echo 'Charlotte Overview';
										} else if (!isset($_GET['d'])) { 
											echo $regions[$_GET['r']-1].' Overview'; 
										} 
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('../application/account_300/custom/time_series_production_300_v2.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
					 },{
                        name: 'Water Production',
                        data: <?php include('../application/account_300/custom/time_series_production_300_v2_water.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
					 },{
                        name: 'Gas Production',
                        data: <?php include('../application/account_300/custom/time_series_production_300_v2_gas.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' BBL'
						 }
					 }	]
                });
				   
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
				
		
		<?php  if ($_GET['s'] == 5 && isset($_GET['tn'])) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x'
				},
					   
                    title: {
                        text: 'Volume Trending',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Volume',
                        data: <?php include('../application/account_300/custom/time_series_oil_volume_tank.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    },{
                        name: 'Water Volume',
                        data: <?php include('../application/account_300/custom/time_series_water_volume_tank.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }	
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  
		
		
		
		<?php  if ($_GET['s'] == 5 && isset($_GET['d'])) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x',
									
				},


                    title: {
                        text: 'Readings Trends',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Volume',
                        data: <?php include('../application/account_300/custom/time_series_oil_volume_separator.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    },{
                        name: 'Water Volume',
                        data: <?php include('../application/account_300/custom/time_series_water_volume_separator.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
					},{
                        name: 'Gas Flow',
                        data: <?php include('../application/account_300/custom/time_series_gas_flow_separator.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Casing Pressure',
                        data: <?php include('../application/account_300/custom/time_series_casing_pressure_separator.php'); ?>,
						color: '#DAA520',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Tubing Pressure',
                        data: <?php include('../application/account_300/custom/time_series_tubing_pressure_separator.php'); ?>,
						color: '#D2691E',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Static Pressure',
                        data: <?php include('../application/account_300/custom/time_series_static_pressure_separator.php'); ?>,
						color: '#6495ED',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Differential Pressure',
                        data: <?php include('../application/account_300/custom/time_series_differential_pressure_separator.php'); ?>,
						color: '#E9967A',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    }				
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  
		
		
		<?php  if ($_GET['s'] == 6 && isset($_GET['d'])) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x',
									
				},


                    title: {
                        text: 'Readings Trends',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Volume',
                        data: <?php include('../application/account_300/custom/time_series_oil_volume_separator.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }				
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  
		
		
		
		<?php  if ($_GET['s'] == 7 && isset($_GET['d'])) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x',
									
				},


                    title: {
                        text: 'Readings Trends',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Water Volume',
                        data: <?php include('../application/account_300/custom/time_series_water_volume_separator.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }				
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  
	
		
		<?php  if ($_GET['s'] == 8 && isset($_GET['d'])) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x',
									
				},


                    title: {
                        text: 'Readings Trends',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Gas Flow',
                        data: <?php include('../application/account_300/custom/time_series_gas_flow_separator.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    }				
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  	
		
		
		
	<?php  if ($_GET['s'] == 9 && isset($_GET['d'])) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x',
									
				},


                    title: {
                        text: 'Readings Trends',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Casing Pressure',
                        data: <?php include('../application/account_300/custom/time_series_casing_pressure_separator.php'); ?>,
						color: '#DAA520',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Tubing Pressure',
                        data: <?php include('../application/account_300/custom/time_series_tubing_pressure_separator.php'); ?>,
						color: '#D2691E',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Static Pressure',
                        data: <?php include('../application/account_300/custom/time_series_static_pressure_separator.php'); ?>,
						color: '#6495ED',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    },{
                        name: 'Differential Pressure',
                        data: <?php include('../application/account_300/custom/time_series_differential_pressure_separator.php'); ?>,
						color: '#E9967A',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                    }		
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  	
		
   
		<script>
			
				/////////ajax script for tag manager
				$('.configsel').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	
					

					$.ajax({

						url: "../application/account_300/custom/update_config.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});
			
		
				$('.reglabel').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	

					$.ajax({

						url: "../application/account_300/custom/update_label.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});
			
			
					
				$('.regfactor').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	

					$.ajax({

						url: "../application/account_300/custom/update_factor.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});

		</script>
	
    
  </body>
</html>

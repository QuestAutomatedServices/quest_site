<?php 
include('/var/www/html/application/includes/site.php');

if (!$_GET) {
	header("location: dashboard.php?r=2");
	exit();
}

$targetdate = date("Y-m-d", strtotime("-1 day"));


$regions = array('Global','Texas','Oklahoma');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

       <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">Keystone Hughes - Richland Resources Corporation</a>
       
               </div>
        <div id="navbar" class="navbar-collapse collapse">
       <ul class="nav navbar-nav navbar-right">
<?php /*?>     	<li <?php if (!isset($_GET['r']) && !isset($_GET['tb']) && !isset($_GET['po'])  && !isset($_GET['sm']) && !isset($_GET['pl'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li><?php */?>
                      
          <li <?php if ($_GET['r'] == 2) echo 'class="active"' ?>><a href="dashboard.php?r=2">Oklahoma</a></li>
            <li <?php if ($_GET['r'] == 1) echo 'class="active"' ?>><a href="dashboard.php?r=1">Texas</a></li>
             <li <?php if ($_GET['tb'] == 1) echo 'class="active"' ?>><a href="dashboard.php?tb=1">Tank Batteries</a></li>
             <li <?php if ($_GET['wa'] == 1) echo 'class="active"' ?>><a href="dashboard.php?wa=1">Well Attributes</a></li>
            <li <?php if ($_GET['po'] == 1) echo 'class="active"' ?>><a href="dashboard.php?po=1">Pump Off Controllers</a></li>
            <li <?php if ($_GET['pl'] == 1) echo 'class="active"' ?>><a href="dashboard.php?pl=1">Plunger Lift Mgmt</a></li>
             <li><a href="control.php?r=227">Control Settings</a></li>
                 	<li><a href="../../support/support.php">Support</a></li>
                  	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <?php /*?><li <?php if (!isset($_GET['r']) && !isset($_GET['tb']) && !isset($_GET['sm']) && !isset($_GET['po']) && !isset($_GET['pl'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li><?php */?>
                      
            <li <?php if ($_GET['r'] == 2) echo 'class="active"' ?>><a href="dashboard.php?r=2">Oklahoma</a></li>
             
             
                         
             <?php if (isset($_GET['r'])  && $_GET['r'] == 2 or isset($_GET['ct'])  && $_GET['r'] == 2) { ?>
          <ul>
                   
           		
           		
            
            
            <?php	
	
					echo '<li><a href="dashboard.php?ct=7">Caddo</a>
					
								<ul>
								
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=248&geo=20">Hazel 1-33</a></li>

							
									
								
								</ul>
								
					
					
					</li>';
					
						echo '<li><a href="dashboard.php?ct=5">Comanche</a>
					
								<ul>
								
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=243&geo=20">State A-2</a></li>

							
									
								
								</ul>
								
					
					
					</li>';
	
	
						
						echo '<li><a href="dashboard.php?ct=6">Grady</a>
					
								<ul>
								
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=242&geo=21">Fritz 5-31</a></li>

							
									
								
								</ul>
								
					
					
					</li>';

							
					echo '<li><a href="dashboard.php?ct=4">Roger Mills</a>
					
								<ul>
								
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=240&geo=18">Matthews 1-33A</a></li>

							
									
								
								</ul>
								
					
					
					</li>';
									  
				
					
			?>
           	
           	
           	
            	
          </ul>
         <?php } ?>
             
             
              <li <?php if ($_GET['r'] == 1) echo 'class="active"' ?>><a href="dashboard.php?r=1">Texas</a></li>
             
             <?php if (isset($_GET['r'])  && $_GET['r'] == 1 or isset($_GET['ct'])  && $_GET['r'] == 1) { ?>
          <ul>
                   
           		
           		
            
            
            <?php	
					  
					//echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d=192&ct=1">Crane</a><li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">Ector County POC</a></li>
							
							echo '<li><a href="dashboard.php?ct=1">Crane</a>
					
								<ul>
								
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea A2</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea B3</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea B4</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea B5</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea C1</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea C2</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea C3</a></li>
										<li><a href="dashboard.php?r='.$_GET['r'].'&d=224&geo=5">PJ Lea D1</a></li>
							
									
								
								</ul>
								
					
					
					</li>';
									  
									  
					//echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d=192&ct=1">Ector</a>
	
						echo '<li><a href="dashboard.php?ct=2">Ector</a>
					
								<ul>
								
									
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=226&geo=6">JD Slater 1</a></li>

								
								</ul>
								
					
					
					</li>';
									  
					//echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d=192&sct=1">Gaines</a>
	
							echo '<li><a href="dashboard.php?ct=3">Gaines</a>
					
								<ul>
								
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=227&geo=7">Bayliss #1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=999&geo=28">Bellinger 1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=231&geo=8">Cunningham-Davis ET2</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=229&geo=9">Cunningham-Davis B 5-74</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=999&geo=10">Dora Cunningham DO A1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=223&geo=18">Gaines County Sales</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=223&geo=19">Gaines County Check Meter</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=228&geo=11">Homann #1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=230&geo=13">Homann #3</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=232&geo=14">JB Riley 1A</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=237&geo=15">Northrup-Lindsey 1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=229&geo=27">Riley 2G</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=229&geo=16">RH Cummins 1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=999&geo=29">State 1</a></li>
									<li><a href="dashboard.php?r='.$_GET['r'].'&d=225&geo=17">TS Riley F1</a></li>


								
								</ul>
								
					
					
					</li>';

					
			?>
           	
           	
           	
            	
          </ul>
         <?php } ?>
            
            


           <li <?php if ($_GET['tb'] == 1) echo 'class="active"' ?>><a href="dashboard.php?tb=1">Tank Batteries</a></li>
                 <li <?php if ($_GET['wa'] == 1) echo 'class="active"' ?>><a href="dashboard.php?wa=1">Well Attributes</a></li>
            <li <?php if ($_GET['po'] == 1) echo 'class="active"' ?>><a href="dashboard.php?po=1">Pump Off Controllers</a></li>
            <li <?php if ($_GET['pl'] == 1) echo 'class="active"' ?>><a href="dashboard.php?pl=1">Plunger Lift Mgmt</a></li>
                  	           
			
         
         
         
          
         
           <li><a href="../support/support.php">Support</a></li>
			 <li><a href="process/logout.php">Logout</a></li>
          </ul>
          
          
        
         
        
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        
        		
         
        <?php  if (!isset($_GET['d']) && !isset($_GET['tb'])  && !isset($_GET['ct'])  && !isset($_GET['cto'])  && !isset($_GET['r'])  && !isset($_GET['po'])   && !isset($_GET['gs'])   && !isset($_GET['pl'])   && !isset($_GET['pls'])   && !isset($_GET['ntm'])  ) { ?>  
        
        
       	
        <?php  } ?>  
        
         <?php  if (isset($_GET['sm']) ) { ?>  
        
              <h1 class="page-header">Device Summary</h1>
              
              <div class="alert alert-info" role="alert">Click a reading to see reading history</div>
              
              
               <div class="table-responsive">
              
              
              	<table class="table table-striped">

									<thead>
									<tr>
									  <th>Device</th>
									   <th>Gas Flow</th>
									    <th>Gas Flow Yesterday</th>
									    <th>Oil Top Level 1</th>
									     <th>Oil Top Level 2</th>
									      <th>Water Top Level 1</th>

									</tr>
								    </thead>
								    
								   <tbody>
								   
								        <?php 
	
												
											
			  		
												$devices = getDevicesByAccount(42);

												foreach ($devices as $device) {
													
													if ($device['device_deviceID'] == 223) {
													
																
														echo '<tr>
																<td>'.$device['device_name'].' (1)</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=7">'.getGasReading($device['device_deviceID'],1,'',42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=9">'.getGasReading($device['device_deviceID'],2,'',42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														echo '<tr>
																<td>'.$device['device_name'].' (2)</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=289">'.getGasReading($device['device_deviceID'],3,289,42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=291">'.getGasReading($device['device_deviceID'],4,291,42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														
													} else if ($device['device_deviceID'] == 229) {
													
																
														echo '<tr>
																<td>Cunningham Davis B 574</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=7">'.getGasReading($device['device_deviceID'],1,'',42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=9">'.getGasReading($device['device_deviceID'],2,'',42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														echo '<tr>
																<td>Homann #2</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=107">'.getGasReading($device['device_deviceID'],5,107,42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=109">'.getGasReading($device['device_deviceID'],6,109,42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														echo '<tr>
																<td>RH Cummins</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=207">'.getGasReading($device['device_deviceID'],5,207,42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=209">'.getGasReading($device['device_deviceID'],6,209,42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';	
														
														echo '<tr>
																<td>Homann Check Meter</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=307">'.getGasReading($device['device_deviceID'],5,307,42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=309">'.getGasReading($device['device_deviceID'],6,309,42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';	
														
														
													} else if ($device['device_deviceID'] == 226) {
														
														echo '<tr>
																<td>'.$device['device_name'].'</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=790">'.convert16((15.6-getOilReading($device['device_deviceID'],1,790,42))).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=792">'.convert16((15.6-getOilReading($device['device_deviceID'],2,792,42))).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=794">'.convert16((15.6-getWaterReading($device['device_deviceID'],1,42))).'</a></td>
																<td>&nbsp;</td>
																										
													
															</tr>';
														
													} else if ($device['device_deviceID'] == 224) {
														
															echo '<tr>
																<td>PJ Lea A&B Tanks</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=790">'.convert16((15.6-getOilReading($device['device_deviceID'],1,790,42))).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=792">'.convert16((15.6-getOilReading($device['device_deviceID'],2,792,42))).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																										
													
															</tr>';
														
															echo '<tr>
																<td>PJ Lea C&D Tanks</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=794">'.convert16((getOilReading($device['device_deviceID'],1,794,42))).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=796">'.convert16((15.6-getOilReading($device['device_deviceID'],2,796,42))).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																										
													
															</tr>';
														
														
													
																										
													} else {
														
												
													
														echo '<tr>
																<td>'.$device['device_name'].'</td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=7">'.getGasReading($device['device_deviceID'],1,'',42).'</a></td>
																<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g=9">'.getGasReading($device['device_deviceID'],2,'',42).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																										
													
															</tr>';
												}

												}
			  						?>
								   
								   
									</tbody>
           
			</table>
            
			</div>
            
          </div>
			


          


        <?php  } ?>  
        
        
        
        
        
         <?php  if (isset($_GET['wa']) ) { ?>  
        
              <h1 class="page-header">Well Attributes</h1>
              
               <p><a href="dashboard.php?wa=1"><i class="fa fa-list" aria-hidden="true"></i> List View</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href="dashboard.php?wa=1&mp=1"><i class="fa fa-map-marker" aria-hidden="true"></i> Global Map View</a></p>
               
               
                <?php  if (isset($_GET['mp']) ) { ?> 
                
                
               			 <?php 
									echo '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoWVbLzA7BMW2rkqwj5Ihz-qL_rUE8gKE&callback=initMap" async defer></script>';
	
								   include('includes/google_maps2.php'); ?>

									<div id="map" style="width: 90%; border: 5px solid #E4E4E4; border-radius: 20px;"></div>
                
                   <?php  } else { ?>
              
              			
              				  <?php  if (isset($_GET['wid']) ) { ?> 
              				  
              				  		<?php  echo getWellAttributeForm($_GET['wid'],42); ?>
              				  
              				  <?php  } else { ?>
              
							  	 <div class="table-responsive">


								<table class="table table-striped">

													<thead>
													<tr>
													  <th>Name</th>
													   <th>Property Number</th>
														<th>API Number</th>
														<th>State</th>
														 <th>County</th>
														 <th>Well Status</th>
														 <th>Well Type</th>
														  <th>Artificial Lift</th>
														  <th>Producing Formation</th>
														  <th>DOFS</th>

													</tr>
													</thead>

												   <tbody>

														<?php 




																$wells = getWellAttributes(42);

																//print_r($wells);

																foreach ($wells as $well) {



																		echo '<tr>
																				<td><a href="dashboard.php?wa=1&wid='.$well['attributeID'].'">'.$well['reference_name'].'</a></td>
																				<td>'.$well['property_number'].'</td>
																				<td>'.$well['API_number'].'</td>
																				<td>'.$well['state'].'</td>
																				<td>'.$well['county'].'</td>
																				<td>'.$well['well_status'].'</td>
																				<td>'.$well['well_type'].'</td>
																				<td>'.$well['artificial_lift'].'</td>
																				<td>'.$well['producing_formation'].'</td>
																				<td>'.$well['DOFS'].'</td>




																			</tr>';



																}
													?>


													</tbody>

							</table>

							</div>
							
							 <?php  } ?>
        
				   <?php  } ?>

          


        <?php  } ?>  
        
        
        
        
          <?php  if (isset($_GET['r'])  && $_GET['r'] == 1) { ?>  
        
        
              <?php /*?><h1 class="page-header"><?php if (!isset($_GET['r'])) { echo $regions[0].' Overview'; } else if (!isset($_GET['d'])) { echo $regions[$_GET['r']].' Overview'; } ?><?php if (isset($_GET['d'])) echo getDeviceName($_GET['d']);  ?></h1>
              
              	<div class="btn-group">
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">POCs</a>
                  
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Plunger Lifts</a>


              </div>
              
              	<div class="btn-group">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>
            
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&b=1" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>">Digital Log Book</a>
                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           			<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&m=1" class="btn btn-primary <?php if ($_GET['m'] == 1) echo 'active' ?>">Map</a>
           		
            </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
		
            
             <div id="chart"></div><?php */?>

				
		<h2 class="sub-header"><?php 	
									if (isset($_GET['d']) && $_GET['d'] != 229 && $_GET['d'] != 999  && $_GET['d'] != 223 )	{
										echo getDeviceName($_GET['d'],42) . ' - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 9)	{
										echo 'Cunningham-Davis B 5-74 - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 27)	{
										echo 'Riley 2G - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 16)	{
										echo 'RH Cummins 1 - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 18)	{
										echo 'Gaines County Sales - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 19)	{
										echo 'Gaines County Check Meter - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 10)	{
										echo 'Dora Cunningham DO A1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 28)	{
										echo 'Bellinger 1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 29)	{
										echo 'State 1 - Production & Sales';
									} else {
										echo $regions[$_GET['r']].' - Production & Sales';
									}
								?></h2>
        

              
              	<div class="btn-group">
                  
                  <a href="dashboard.php?po=1" class="btn btn-primary <?php if ($_GET['po'] == 1) echo 'active' ?>">POCs</a>
                  
                  
                  <a href="dashboard.php?pls=1" class="btn btn-primary <?php if ($_GET['pl'] == 1) echo 'active' ?>">Plunger Lifts</a>


              </div>
              
              	<div class="btn-group">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       			 <?php  if (isset($_GET['d']) ) { ?>  
					  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>

					  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&b=1" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>">Digital Log Book</a>
                  <?php  } ?>  
                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           			<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&m=1" class="btn btn-primary <?php if ($_GET['m'] == 1) echo 'active' ?>">Map</a>
           		
            </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
         
         <div id="chart"></div>
         
          <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>
          
          
           <div class="table-responsive">
           
             		 <?php  if (isset($_GET['r']) && !isset($_GET['d']) ) { ?> 
             		 
             		   
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
									  <th>Geo</th>
									  <th>Date</th>
									  <th>Gas Prod</th>
									  <th>Gas Sold</th>
									  <th>Goal</th>
									  <th>Var</th>								  
									  <th>Oil Prod</th>
									  <th>Oil Sold</th>
									  <th>Goal</th>
									  <th>Var</th>							  
									  <th>Water Prod</th>
									  <th>Tubing psi</th>
									  <th>Casing psi</th>
									  <th>Line psi</th>
									  <th>Downtime</th>	
									</tr>
								    </thead>
								    
								   <tbody><!--STATE TBODY-->
               
               						<?php /*?><a href="dashboard.php?ntm=1&geo=1&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i> </a><?php */?>
									<tr> <!--STATE ROW-->
									<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#2" aria-hidden="true" title="Show Detail"></i> <?php echo $regions[$_GET['r']];?></td>	
									<td><?php echo formatDate($targetdate);?></td>
									<td><?php echo getTotalGasReading(2,$targetdate,42);?></td>
									<td><?php echo getTotalGasReading(1,$targetdate,42);?></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td style="color: forestgreen;">No issues</td>
									<td style="color: forestgreen;">No issues</td>
									<td style="color: forestgreen;">No issues</td>
									<td>&nbsp;</td>
									</tr> <!--STATE ROW-->
									
									<tr id="2" class="collapse"> <!--STATE COLLAPSE ROW-->
								
									<td colspan="14">  <!--STATE COLLAPSE TD-->
									
									
										<table class="table table-striped" style="background-color:azure;border: 1px solid #D7D7D7;"><!--COUNTY TABLE-->

										<thead>
										<tr>
										  <th>Geo</th>
										  <th>Date</th>
										  <th>Gas Prod</th>
										  <th>Gas Sold</th>
										  <th>Goal</th>
										  <th>Var</th>								  
										  <th>Oil Prod</th>
										  <th>Oil Sold</th>
										  <th>Goal</th>
										  <th>Var</th>							  
										  <th>Water Prod</th>
										  <th>Tubing psi</th>
										  <th>Casing psi</th>
										  <th>Line psi</th>
										  <th>Downtime</th>	
										</tr>
										</thead>
										
										<?php /*?><a href="dashboard.php?ntm=1&geo=2&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> <?php */?>
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#4" aria-hidden="true" title="Show Detail"></i> Crane</td>
										<td><?php echo formatDate($targetdate);?></td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="4" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												
												<tr> <!--SITE ROW-->
												<td>PJ Lea A2</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getOilSold(224,$targetdate,790,42);?></td>
												<td><?php echo getOilProduced(224,$targetdate,790,42);?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td>PJ Lea B3</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getOilSold(224,$targetdate,792,42);?></td>
												<td><?php echo getOilProduced(224,$targetdate,792,42);?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td>PJ Lea B4</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td>PJ Lea B5</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td>PJ Lea C1</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getOilSold(224,$targetdate,794,42);?></td>
												<td><?php echo getOilProduced(224,$targetdate,794,42);?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td>PJ Lea C2</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td>PJ Lea C3</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td>PJ Lea D1</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getOilSold(224,$targetdate,796,42);?></td>
												<td><?php echo getOilProduced(224,$targetdate,796,42);?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#5" aria-hidden="true" title="Show Detail"></i> Ector</td>	
										<td><?php echo formatDate($targetdate);?></td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="5" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td>JD Slater 1</td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getOilSold(226,$targetdate,'',42);?></td>
												<td><?php echo getOilProduced(226,$targetdate,'',42);?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td><?php echo getLastReading(226,235,42);?></td>
												<td><?php echo getLastReading(226,234,42);?></td>
												<td>&nbsp;</td>
												<td><?php echo getLastReading(226,150,42); ?></td>
												</tr> <!--SITE ROW-->
												
												
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#6" aria-hidden="true" title="Show Detail"></i> Gaines</td>	
										<td><?php echo formatDate($targetdate);?></td>
										<td><?php echo getTotalGasReading(2,$targetdate,42);?></td>
										<td><?php echo getTotalGasReading(1,$targetdate,42);?></td>
										<td><?php //echo getWellGoalTotal();?></td>
										<td>&nbsp;</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="6" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tbody>
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=227&geo=7">Bayliss #1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(7,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(7,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(1,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(7,$targetdate,42)-getWellGoal(1,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(227,11,42,'','',1);?></td>
												<td><?php echo getLastReading(227,10,42,'','',1);?></td>
												<td><?php echo getLastReading(227,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(227,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=999&geo=28">Bellinger 1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(28,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(28,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(23,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(28,$targetdate,42) - getWellGoal(23,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(229,711,42,'','',1);?></td>
												<td><?php echo getLastReading(229,710,42,'','',1);?></td>
												<td><?php echo getLastReading(229,703,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(229,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
												

												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=231&geo=8">Cunningham-Davis ET2</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(8,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(8,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(2,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(8,$targetdate,42) - getWellGoal(2,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(231,11,42,'','',1);?></td>
												<td><?php echo getLastReading(231,10,42,'','',1);?></td>
												<td><?php echo getLastReading(231,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(231,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=229&geo=9">Cunningham-Davis B 5-74</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(9,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(9,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(3,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(9,$targetdate,42) - getWellGoal(3,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(229,11,42,'','',1);?></td>
												<td><?php echo getLastReading(229,10,42,'','',1);?></td>
												<td><?php echo getLastReading(229,3,42,-13.2,'',1);?></td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=999&geo=10">Dora Cunningham DO A1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(10,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(10,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(4,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(10,$targetdate,42) - getWellGoal(4,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(229,411,42,'','',1);?></td>
												<td><?php echo getLastReading(229,410,42,'','',1);?></td>
												<td><?php echo getLastReading(229,403,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(230,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
												
														
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=223&geo=18">Gaines County Sales</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(1,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(1,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(5,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(1,$targetdate,42) - getWellGoal(5,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(223,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(223,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=223&geo=19">Gaines County Check Meter</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(2,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(2,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(6,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(2,$targetdate,42) - getWellGoal(6,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(223,285,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(223,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->		
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=228&geo=11">Homann #1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(11,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(11,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(7,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(11,$targetdate,42) - getWellGoal(7,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(228,11,42,'','',1);?></td>
												<td><?php echo getLastReading(228,10,42,'','',1);?></td>
												<td><?php echo getLastReading(228,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(228,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=230&geo=13">Homann #3</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(13,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(13,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(9,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(13,$targetdate,42) - getWellGoal(9,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(230,11,42,'','',1);?></td>
												<td><?php echo getLastReading(230,10,42,'','',1);?></td>
												<td><?php echo getLastReading(230,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(230,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=232&geo=14">JB Riley 1A</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(14,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(14,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(10,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(14,$targetdate,42) - getWellGoal(10,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(232,11,42,'','',1);?></td>
												<td><?php echo getLastReading(232,10,42,'','',1);?></td>
												<td><?php echo getLastReading(232,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(232,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=237&geo=15">Northrup-Lindsey 1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(18,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(18,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(11,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(18,$targetdate,42) - getWellGoal(11,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(237,11,42,'','',1);?></td>
												<td><?php echo getLastReading(237,10,42,'','',1);?></td>
												<td><?php echo getLastReading(237,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(237,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->	

												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=229&geo=16">RH Cummins 1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(16,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(16,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(12,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(16,$targetdate,42) - getWellGoal(12,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(229,211,42,'','',1);?></td>
												<td><?php echo getLastReading(229,210,42,'','',1);?></td>
												<td><?php echo getLastReading(229,203,42,-13.2,'',1);?></td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=229&geo=27">Riley 2G</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(27,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(27,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(24,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(27,$targetdate,42) - getWellGoal(24,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(229,311,42,'','',1);?></td>
												<td><?php echo getLastReading(229,310,42,'','',1);?></td>
												<td><?php echo getLastReading(229,303,42,-13.2,'',1);?></td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=999&geo=29">State 1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(29,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(29,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(22,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(29,$targetdate,42) - getWellGoal(22,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(229,611,42,'','',1);?></td>
												<td><?php echo getLastReading(229,610,42,'','',1);?></td>
												<td><?php echo getLastReading(229,603,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(229,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=1&d=225&geo=18">TS Riley F1</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(17,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(17,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(13,'',42);?></td>
												<td><?php echo number_format(round(getTotalGasReading(17,$targetdate,42) - getWellGoal(13,1,42),2),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(225,11,42,'','',1);?></td>
												<td><?php echo getLastReading(225,10,42,'','',1);?></td>
												<td><?php echo getLastReading(225,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(225,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->															
										

												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
												
												
												<?php /*?><table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>	  
												  <th>Flow Rate</th>
												  <th>Today</th>
												  <th>BTU Factor</th>
												  <th>Yesterday</th>
												  <th>BTU Factor</th>
												  <th>PM8 Lift Type</th>							  

												</tr>
												</thead>
												
												<tbody>
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?ntm=1&geo=4&dv=227"><i class="fa fa-commenting" aria-hidden="true"></i></a> Bayliss #1</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td><?php echo getLastReading(227,5);?></td>
												<td><?php echo getLastReading(227,7);?></td>
												<td>&nbsp;</td>
												<td><?php echo getLastReading(227,9);?></td>
												<td>&nbsp;</td>
												<td><?php echo getLastReading(227,346);?></td>
												</tr> <!--SITE ROW-->
												
																							
											</table>
											
											
											
											<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>	  
												  <th>Hours Today</th>
												  <th>Hours Yesterday</th>
												  <th>Today Arrivals</th>
												  <th>Yesterday Arrivals</th>
												  <th>Today Non Arrivals</th>
												 <th>Yesterday Non Arrivals</th>
					  
												</tr>
												</thead>
												
												<tbody>
												
												<tr style="background-color: aliceblue;"> <!--SITE ROW-->
												<td><a href="dashboard.php?ntm=1&geo=4&dv=231"><i class="fa fa-commenting" aria-hidden="true"></i></a> Cunningham-Davis ET2</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td><?php echo getLastReading(231,137);?></td>
												<td><?php echo getLastReading(231,138);?></td>
												<td><?php echo getLastReading(231,398);?></td>
												<td><?php echo getLastReading(231,399);?></td>
												<td><?php echo getLastReading(231,400);?></td>
												<td><?php echo getLastReading(231,401);?></td>
												</tr> <!--SITE ROW-->
												
																							
											</table><?php */?>
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->

										</tbody><!--COUNTY TBODY-->
           					
            							</table><!--COUNTY TABLE-->
									
									
									</td><!--STATE COLLAPSE TD-->
					
									</tr> <!--STATE COLLAPSE ROW-->
									
															  
							       </tbody><!--STATE TBODY-->
           					
            					</table><!--STATE TABLE-->
            					
            					
									 <div id="goal_modal" title="Set Well Goal" style="display: none;">

<!--												<form method="post" action="process/battery_diary.php">
												<input type="hidden" name="deviceID" value="238"> 
												<input type="hidden" name="tank_battery" value="1"> 
												<input type="hidden" name="diary_date" id="diarymodaldate">-->
											   
											   
											   <form method="post" action="process/set_goal_value.php">
												   
												   <input type="hidden" name="goal_number" id="goalnumfield">

												   <div class="form-group">

													<label for="log_book_field">Goal </label>
													 <input type="text" name="goal_value" class="form-control" id="goal_value_edit" placeholder="Set Goal">


												  </div>

												  <button type="submit" class="btn btn-default">Submit</button>
												</form>





										</div> 

           
           					 <?php  } ?> 
           					 
           					 
           					  <?php  if ($_GET['d'] && !isset($_GET['c'])  && !isset($_GET['f']) ) { ?> 
           					  
           					  <div id="chart"></div>
           					  
       									<table class="table table-striped" style="border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;background-color:#344D97;">
												  <th style="width: 200px;">Device/Well</th>
												  <th>Date</th>
												  <th>Spot Rate</th>
												  <th>Gas Prod (MCF)</th>
												  <th>Gas Sold (MCF)</th>
												  <th>Goal (MCF)</th>
												  <th>Var (MCF)</th>
												  <th>Gas Prod (MMBTU)</th>							  
												  <th>Oil Prod (BBLS)</th>
												  <th>Oil Sold (BBLS)</th>
												  <th>Goal (BBLS)</th>
												  <th>Var (BBLS)</th>							  
												  <th>Water Prod  (BBLS)</th>
												  <th>Tubing (PSIG)</th>
												  <th>Casing (PSIG)</th>
												  <th>Line (PSIG)</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<?php  if ($_GET['d'] == 226) { ?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=3&dv=226&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> JD Slater 1</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php echo getOilSold(226,$targetdate,'',42);?></td>
													<td><?php echo getOilProduced(226,$targetdate,'',42);?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php echo getLastReading(226,235,42);?></td>
													<td><?php echo getLastReading(226,234,42);?></td>
													<td>&nbsp;</td>
													<td><?php echo getLastReading(226,150,42);?></td>
													</tr> <!--SITE ROW-->
												<?php  } ?> 
												
												<?php  if ($_GET['d'] == 227) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
														
																
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=227&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Bayliss #1</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(227,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(7,$targetdatem1,42,1); } else { echo getLastReading(227,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(7,$targetdatem1,42,1); } else { echo getLastReading(227,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(7,$targetdate,42)*(getLastReading(227,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(227,11,42,'','',1); } else { echo getDailyAverage(227,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(227,10,42,'','',1); } else { echo getDailyAverage(227,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(227,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (getLastReading(227,138,42,-24,'',1)); }?></td>
													</tr> <!--SITE ROW-->
												<?php  } 
											
			 										}	?> 
												
												<?php  if ($_GET['d'] == 231) { 
											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=231&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Cunningham-Davis ET2</td>	
													<td><?php echo formatDate($targetdate); ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(231,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(8,$targetdatem1,42,1); } else { echo getLastReading(231,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(8,$targetdatem1,42,1); } else { echo getLastReading(231,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(8,$targetdate,42)*(getLastReading(231,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(231,11,42,'','',1); } else { echo getDailyAverage(231,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(231,10,42,'','',1); } else { echo getDailyAverage(231,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(231,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (getLastReading(231,138,42,-24,'',1)); }?></td>
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>

												
												<?php  if ($_GET['d'] == 229 && $_GET['geo'] == 9) {		
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?>  
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=229&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Cunningham-Davis B 5-74</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(9,$targetdatem1,42,1); } else { echo getLastReading(229,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(9,$targetdatem1,42,1); } else { echo getLastReading(229,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(9,$targetdate,42)*(getLastReading(229,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,11,42,'','',1); } else { echo getDailyAverage(229,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,10,42,'','',1); } else { echo getDailyAverage(229,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,3,42,-13.2,'',1); }?></td>
													<td>NA</td>
													</tr> <!--SITE ROW-->	
												<?php    } 
											
			 										}	?>											
													
												<?php  if ($_GET['d'] == 999 && $_GET['geo'] == 10) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?>  
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=229&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Dora Cunningham DO A1</td>														
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")){ echo getLastReading(229,405,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(10,$targetdatem1,42,1); } else { echo getLastReading(229,407,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(10,$targetdatem1,42,1); } else { echo getLastReading(229,407,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(10,$targetdate,42)*(getLastReading(229,413,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,411,42,'','',1); } else { echo getDailyAverage(229,411,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,410,42,'','',1); } else { echo getDailyAverage(229,410,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,403,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo 'NA'; }?></td>
												
												</tr> <!--SITE ROW-->		
												<?php    } 
											
			 										}	?>	
			 										
			 																				
			 									<?php  if ($_GET['d'] == 999 && $_GET['geo'] == 28) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?>  
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=28&dv=229&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Bellinger 1</td>														
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")){ echo getLastReading(229,705,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(28,$targetdatem1,42,1); } else { echo getLastReading(229,707,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(28,$targetdatem1,42,1); } else { echo getLastReading(229,707,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(28,$targetdate,42)*(getLastReading(229,713,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,711,42,'','',1); } else { echo getDailyAverage(229,711,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,710,42,'','',1); } else { echo getDailyAverage(229,710,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,703,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo 'NA'; }?></td>
												
												</tr> <!--SITE ROW-->		
												<?php    } 
											
			 										}	?>											
													
													
													
												<?php  if ($_GET['d'] == 999 && $_GET['geo'] == 29) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?>  
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=29&dv=229&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> State 1</td>														
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")){ echo getLastReading(229,605,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(29,$targetdatem1,42,1); } else { echo getLastReading(229,607,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(29,$targetdatem1,42,1); } else { echo getLastReading(229,607,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(29,$targetdate,42)*(getLastReading(229,613,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,611,42,'','',1); } else { echo getDailyAverage(229,611,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,610,42,'','',1); } else { echo getDailyAverage(229,610,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,603,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo 'NA'; }?></td>
												
												</tr> <!--SITE ROW-->		
												<?php    } 
											
			 										}	?>		
													
												
												<?php  if ($_GET['d'] == 228) { 
											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=228&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Homann #1</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")){ echo getLastReading(228,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(11,$targetdatem1,42,1); } else { echo getLastReading(228,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(11,$targetdatem1,42,1); } else { echo getLastReading(228,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(11,$targetdate,42)*(getLastReading(228,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>												
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(228,11,42,'','',1); } else { echo getDailyAverage(228,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(228,10,42,'','',1); } else { echo getDailyAverage(228,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(228,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(228,138,42,-24,'',1); }?></td>
													

													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>		

												<?php  if ($_GET['d'] == 229 && $_GET['geo'] == 27) {
				 											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?>
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=27&dv=229&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Riley 2G</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")){ echo getLastReading(229,305,42,'','',1); }?></td>	
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(27,$targetdatem1,42,1); } else { echo getLastReading(229,307,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(27,$targetdatem1,42,1); } else { echo getLastReading(229,307,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(27,$targetdate,42)*(getLastReading(229,313,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,311,42,'','',1); } else { echo getDailyAverage(229,311,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,310,42,'','',1); } else { echo getDailyAverage(229,310,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,303,42,-13.2,'',1); }?></a></td>
													<td>NA</td>
													
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	

												<?php  if ($_GET['d'] == 230) { 
											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=230&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Homann #3</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(230,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(13,$targetdatem1,42,1); } else { echo getLastReading(230,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(13,$targetdatem1,42,1); } else { echo getLastReading(230,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(13,$targetdate,42)*(getLastReading(230,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(230,11,42,'','',1); } else { echo getDailyAverage(230,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(230,10,42,'','',1); } else { echo getDailyAverage(230,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(230,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(230,138,42,-24,'',1); }?></td>
													
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	 

												<?php  if ($_GET['d'] == 232) { 
											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=232&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> JB Riley 1A</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(232,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(14,$targetdatem1,42,1); } else { echo getLastReading(232,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(14,$targetdatem1,42,1); } else { echo getLastReading(232,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(14,$targetdate,42)*(getLastReading(232,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(232,11,42,'','',1); } else { echo getDailyAverage(232,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(232,10,42,'','',1); } else { echo getDailyAverage(232,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(232,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(232,138,42,-24,'',1); }?></td>
													
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	 	

												<?php  if ($_GET['d'] == 237) { 
											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=237&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Northrup-Lindsey 1</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(237,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(18,$targetdatem1,42,1); } else { echo getLastReading(237,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(18,$targetdatem1,42,1); } else { echo getLastReading(237,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(18,$targetdate,42)*(getLastReading(237,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(237,11,42,'','',1); } else { echo getDailyAverage(237,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(237,10,42,'','',1); } else { echo getDailyAverage(237,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(237,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(237,138,42,-24,'',1); }?></td>
													
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	 		

												<?php  if ($_GET['d'] == 229 && $_GET['geo'] == 16) {
				 											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=229&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> RH Cummins 1</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,205,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(16,$targetdatem1,42,1); } else { echo getLastReading(229,207,42,'','',1); }?></td>											
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(16,$targetdatem1,42,1); } else { echo getLastReading(229,207,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(16,$targetdate,42)*(getLastReading(229,213,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,211,42,'','',1); } else { echo getDailyAverage(229,211,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,210,42,'','',1); } else { echo getDailyAverage(229,210,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(229,203,42,-13.2,'',1); }?></a></td>
													<td>NA</td>
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	

												<?php  if ($_GET['d'] == 225) { 
											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=225&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> TS Riley F1</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(225,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(17,$targetdatem1,42,1); } else { echo getLastReading(225,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(17,$targetdatem1,42,1); } else { echo getLastReading(225,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(17,$targetdate,42)*(getLastReading(225,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(225,11,42,'','',1); } else { echo getDailyAverage(225,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(225,10,42,'','',1); } else { echo getDailyAverage(225,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(225,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(225,138,42,-24,'',1); }?></td>
													</tr> <!--SITE ROW-->		
												<?php    } 
											
			 										}	?>		
			 										
			 									<?php  if ($_GET['d'] == 223 && $_GET['geo'] == 18) {
				 											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=223&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Gaines County Sales</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><a href="dashboard.php?r=1&d=223&c=1&g=5"><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(223,5,42); }?></a></td>
													
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(1,$targetdatem1,42); } else { echo getLastReading(223,7,42); }?></td>
																									
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(1,$targetdatem1,42); } else { echo getLastReading(223,7,42); }?></td>
																										

													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(1,$targetdate,42)*(getLastReading(223,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>NA</td>
													<td>NA</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(223,3,42)-13.2; }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (24-getLastReading(223,138,42)); }?></td>
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	
			 										
			 										
			 										
			 										<?php  if ($_GET['d'] == 223 && $_GET['geo'] == 19) {
				 											
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=223&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Gaines County Check</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><a href="dashboard.php?r=1&d=223&c=1&g=287"><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(223,287,42); }?></a></td>
													
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(2,$targetdatem1,42); } else { echo getLastReading(223,289,42); }?></td>
																									
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(2,$targetdatem1,42); } else { echo getLastReading(223,289,42); }?></td>
																										

													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(2,$targetdate,42)*(getLastReading(223,295,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>NA</td>
													<td>NA</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(223,285,42)-13.2; }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (24-getLastReading(223,138,42)); }?></td>
													</tr> <!--SITE ROW-->
												<?php    } 
											
			 										}	?>	

												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
           					  
           					   <?php  } ?> 
            
          </div>
			

      


        <?php  } ?>  
        
        
        
        
        
        
          <?php  if (isset($_GET['r'])  && $_GET['r'] == 2) { ?>  
        
        
 				
		<h2 class="sub-header"><?php 	
									if (isset($_GET['d']) )	{
										echo getDeviceName($_GET['d'],42) . ' - Production & Sales';
									} else {
										echo $regions[$_GET['r']].' - Production & Sales';
									}
								?></h2>
        

              
              	<div class="btn-group">
                  
                  <a href="dashboard.php?po=1" class="btn btn-primary <?php if ($_GET['po'] == 1) echo 'active' ?>">POCs</a>
                  
                  
                  <a href="dashboard.php?pls=1" class="btn btn-primary <?php if ($_GET['pl'] == 1) echo 'active' ?>">Plunger Lifts</a>


              </div>
              
              	<div class="btn-group">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       			 <?php  if (isset($_GET['d']) ) { ?>  
					  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>

					  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&b=1" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>">Digital Log Book</a>
                  <?php  } ?>  
                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           			<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&m=1" class="btn btn-primary <?php if ($_GET['m'] == 1) echo 'active' ?>">Map</a>
           		
            </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
         
         <div id="chart"></div>
         
         <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>
          
           <div class="table-responsive">
           
             		 <?php  if (isset($_GET['r']) && !isset($_GET['d']) ) { ?> 
             		 
             		   
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
									  <th>Geo</th>
									  <th>Date</th>
									  <th>Gas Prod</th>
									  <th>Gas Sold</th>
									  <th>Goal</th>
									  <th>Var</th>								  
									  <th>Oil Prod</th>
									  <th>Oil Sold</th>
									  <th>Goal</th>
									  <th>Var</th>							  
									  <th>Water Prod</th>
									  <th>Tubing psi</th>
									  <th>Casing psi</th>
									  <th>Line psi</th>
									  <th>Downtime</th>	
									</tr>
								    </thead>
								    
								   <tbody><!--STATE TBODY-->
               
               						<?php /*?><a href="dashboard.php?ntm=1&geo=1&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i> </a><?php */?>
									<tr> <!--STATE ROW-->
									<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#2" aria-hidden="true" title="Show Detail"></i> <?php echo $regions[$_GET['r']];?></td>	
									<td><?php echo formatDate($targetdate);?></td>
									<td><?php echo getTotalGasReading(2,$targetdate,42);?></td>
									<td><?php echo getTotalGasReading(1,$targetdate,42);?></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td style="color: forestgreen;">No issues</td>
									<td style="color: forestgreen;">No issues</td>
									<td style="color: forestgreen;">No issues</td>
									<td>&nbsp;</td>
									</tr> <!--STATE ROW-->
									
									<tr id="2" class="collapse"> <!--STATE COLLAPSE ROW-->
								
									<td colspan="14">  <!--STATE COLLAPSE TD-->
									
									
										<table class="table table-striped" style="background-color:azure;border: 1px solid #D7D7D7;"><!--COUNTY TABLE-->

										<thead>
										<tr>
										  <th>Geo</th>
										  <th>Date</th>
										  <th>Gas Prod</th>
										  <th>Gas Sold</th>
										  <th>Goal</th>
										  <th>Var</th>								  
										  <th>Oil Prod</th>
										  <th>Oil Sold</th>
										  <th>Goal</th>
										  <th>Var</th>							  
										  <th>Water Prod</th>
										  <th>Tubing psi</th>
										  <th>Casing psi</th>
										  <th>Line psi</th>
										  <th>Downtime</th>	
										</tr>
										</thead>
										
										<?php /*?><a href="dashboard.php?ntm=1&geo=2&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> <?php */?>
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#7" aria-hidden="true" title="Show Detail"></i> Caddo</td>
										<td><?php echo formatDate($targetdate);?></td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="7" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=2&d=248&geo=20">Hazel 1-33</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(31,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(31,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(25,'',42);?></td>
												<td><?php echo round(getTotalGasReading(31,$targetdate,42)-getWellGoal(25,1,42),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(248,11,42,'','',1);?></td>
												<td><?php echo getLastReading(248,10,42,'','',1);?></td>
												<td><?php echo getLastReading(248,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(248,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
																								
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										<?php /*?><a href="dashboard.php?ntm=1&geo=2&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> <?php */?>
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#4" aria-hidden="true" title="Show Detail"></i> Comanche</td>
										<td><?php echo formatDate($targetdate);?></td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="4" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=2&d=243&geo=20">State A-2</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(3,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(3,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(19,'',42);?></td>
												<td><?php echo round(getTotalGasReading(3,$targetdate,42)-getWellGoal(19,1,42),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(243,11,42,'','',1);?></td>
												<td><?php echo getLastReading(243,10,42,'','',1);?></td>
												<td><?php echo getLastReading(243,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(243,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
																								
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										
											<?php /*?><a href="dashboard.php?ntm=1&geo=2&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> <?php */?>
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#5" aria-hidden="true" title="Show Detail"></i> Grady</td>
										<td><?php echo formatDate($targetdate);?></td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="5" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=2&d=242&geo=21">Fritz 5-31</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(5,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(5,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(20,'',42);?></td>
												<td><?php echo round(getTotalGasReading(5,$targetdate,42)-getWellGoal(20,1,42),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(242,11,42,'','',1);?></td>
												<td><?php echo getLastReading(242,10,42,'','',1);?></td>
												<td><?php echo getLastReading(242,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(242,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
																								
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										
											<?php /*?><a href="dashboard.php?ntm=1&geo=2&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> <?php */?>
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#6" aria-hidden="true" title="Show Detail"></i> Rogers Mills</td>
										<td><?php echo formatDate($targetdate);?></td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>NA</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td style="color: forestgreen;">No issues</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="6" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:#344D97;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;">
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td><a href="dashboard.php?r=2&d=240&geo=7">Matthews 1-33A</a></td>	
												<td><?php echo formatDate($targetdate);?></td>
												<td><?php echo getTotalGasReading(6,$targetdate,42,1);?></td>
												<td><?php echo getTotalGasReading(6,$targetdate,42,1);?></td>
												<td><?php echo getWellGoal(18,'',42);?></td>
												<td><?php echo round(getTotalGasReading(6,$targetdate,42)-getWellGoal(18,1,42),2);?></td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td>NA</td>
												<td><?php echo getLastReading(240,11,42,'','',1);?></td>
												<td><?php echo getLastReading(240,10,42,'','',1);?></td>
												<td><?php echo getLastReading(240,3,42,-13.2,'',1);?></td>
												<td><?php echo getLastReading(240,138,42,-24,1,1);?></td>
												</tr> <!--SITE ROW-->
																								
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										

										</tbody><!--COUNTY TBODY-->
           					

            							</table><!--COUNTY TABLE-->
									
									
									</td><!--STATE COLLAPSE TD-->
					
									</tr> <!--STATE COLLAPSE ROW-->
									
															  
							       </tbody><!--STATE TBODY-->
           					
            					</table><!--STATE TABLE-->
            					
            					
									 <div id="goal_modal" title="Set Well Goal" style="display: none;">

<!--												<form method="post" action="process/battery_diary.php">
												<input type="hidden" name="deviceID" value="238"> 
												<input type="hidden" name="tank_battery" value="1"> 
												<input type="hidden" name="diary_date" id="diarymodaldate">-->
											   
											   
											   <form method="post" action="process/set_goal_value.php">
												   
												   <input type="hidden" name="goal_number" id="goalnumfield">

												   <div class="form-group">

													<label for="log_book_field">Goal </label>
													 <input type="text" name="goal_value" class="form-control" id="goal_value_edit" placeholder="Set Goal">


												  </div>

												  <button type="submit" class="btn btn-default">Submit</button>
												</form>





										</div> 

           
           					 <?php  } ?> 
           					 
           					 
           					  <?php  if ($_GET['d'] && !isset($_GET['c'])  && !isset($_GET['f']) ) { ?> 
           					  
           					  <div id="chart"></div>
           					  
       									<table class="table table-striped" style="border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr style="color: #FFF;background-color:#344D97;">
												  <th style="width: 200px;">Device/Well</th>
												  <th>Date</th>
												  <th>Spot Rate</th>
												  <th>Gas Prod (MCF)</th>
												  <th>Gas Sold (MCF)</th>
												  <th>Goal (MCF)</th>
												  <th>Var (MCF)</th>
												  <th>Gas Prod (MMBTU)</th>							  
												  <th>Oil Prod (BBLS)</th>
												  <th>Oil Sold (BBLS)</th>
												  <th>Goal (BBLS)</th>
												  <th>Var (BBLS)</th>							  
												  <th>Water Prod  (BBLS)</th>
												  <th>Tubing (PSIG)</th>
												  <th>Casing (PSIG)</th>
												  <th>Line (PSIG)</th>
												  <th>Downtime</th>	
												</tr>
												</thead>

												
												<?php  if ($_GET['d'] == 240) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
														
																$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
															//$targetdate = date("Y-m-d", strtotime("-".$i." day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=4&dv=240&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Matthews 1-33A</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(240,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(6,$targetdatem1,42,1); } else { echo getLastReading(240,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(6,$targetdatem1,42,1); } else { echo getLastReading(240,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(6,$targetdate,42)*(getLastReading(240,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(240,11,42,'','',1); } else { echo getDailyAverage(240,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(240,10,42,'','',1); } else { echo getDailyAverage(240,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(240,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (getLastReading(240,138,42,-24,'',1)); }?></td>
													</tr> <!--SITE ROW-->
												<?php  } 
											
			 										}	?> 
			 										
			 										
			 									<?php  if ($_GET['d'] == 242) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
																
															//$targetdate = date("Y-m-d", strtotime("-".$i." day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=21&dv=242&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Fritz 5-31</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(242,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(5,$targetdatem1,42,1); } else { echo getLastReading(242,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(5,$targetdatem1,42,1); } else { echo getLastReading(242,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(5,$targetdate,42)*(getLastReading(242,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(242,11,42,'','',1); } else { echo getDailyAverage(242,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(242,10,42,'','',1); } else { echo getDailyAverage(242,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(242,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (getLastReading(242,138,42,-24,'',1)); }?></td>
													</tr> <!--SITE ROW-->
												<?php  } 
											
			 										}	?> 
			 										
			 										
			 										
			 									<?php  if ($_GET['d'] == 243) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
																
															//$targetdate = date("Y-m-d", strtotime("-".$i." day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=20&dv=243&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> State A-2</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(243,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(3,$targetdatem1,42,1); } else { echo getLastReading(243,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(3,$targetdatem1,42,1); } else { echo getLastReading(243,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(3,$targetdate,42)*(getLastReading(243,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(243,11,42,'','',1); } else { echo getDailyAverage(243,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(243,10,42,'','',1); } else { echo getDailyAverage(243,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(243,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (getLastReading(243,138,42,-24,'',1)); }?></td>
													</tr> <!--SITE ROW-->
												<?php  } 
											
			 										}	?> 
			 										
			 										
			 										
			 										
			 										<?php  if ($_GET['d'] == 248) { 
				 
				 											if ($_GET['start']) {
																$datesrange = date_range($_GET['start'],$_GET['end']);
															} else {
																$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
															}
															
														rsort($datesrange);
				 										
				 										foreach ($datesrange as $targetdate) {
															
															$targetdatem1 = date("Y-m-d", strtotime($targetdate." +1 day"));
																
															//$targetdate = date("Y-m-d", strtotime("-".$i." day"));
														?> 
													<tr> <!--SITE ROW-->
													<td><a href="dashboard.php?ntm=1&geo=20&dv=248&date=<?php echo $targetdate; ?>"><i class="fa fa-commenting" aria-hidden="true"></i></a> Hazel 1-33</td>	
													<td><?php echo formatDate($targetdate);?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(248,5,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(31,$targetdatem1,42,1); } else { echo getLastReading(248,7,42,'','',1); }?></td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo getTotalGasReading(31,$targetdatem1,42,1); } else { echo getLastReading(248,7,42,'','',1); }?></td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate!=date("Y-m-d")) { echo round((getTotalGasReading(31,$targetdate,42)*(getLastReading(248,13,42)/1000)),2); }?></td>
													<td>NA</td>
													<td>NA</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td>&nbsp;</td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(248,11,42,'','',1); } else { echo getDailyAverage(248,11,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(248,10,42,'','',1); } else { echo getDailyAverage(248,10,$targetdate,42); } ?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo getLastReading(248,3,42,-13.2,'',1); }?></td>
													<td><?php if ($targetdate==date("Y-m-d")) { echo (getLastReading(248,138,42,-24,'',1)); }?></td>
													</tr> <!--SITE ROW-->
												<?php  } 
											
			 										}	?> 
												
												

												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
           					  
           					   <?php  } ?> 
            
          </div>
			

      


        <?php  } ?>  

        
        
        
            <?php  if (isset($_GET['tb'])) { ?>  
        
        
        		 <h1 class="page-header">Tank Battery Summary</h1>
        		 
        		<!-- <div id="chart"></div>-->
        		
        		<!--Current, Yesterday, Production Today, Production Yesterday, Produciton 7day, Production 30day-->
        		
        			<div class="alert alert-info" role="alert">Click a reading to see reading history</div>
        			
        
        
          			<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
										<th colspan="3">&nbsp;</th>
										<th colspan="6" class="warning">Oil Tank 1</th>
										<th colspan="6" class="info">Oil Tank 2</th>
										<th colspan="6" class="danger">Water Tank</th>
									</tr>
									
									<tr>
									  <th>Gauge Sheet</th>
									  <th>Battery Name</th>
									  <th>Status</th>
									  <th class="warning">CRT</th>
									  <th class="warning">YDY</th>
									  <th class="warning">PCRT</th>
									  <th class="warning">PYDY</th>
									  <th class="warning">P7</th>
									  <th class="warning">P30</th>
									  <th class="info">CRT</th>
									  <th class="info">YDY</th>
									  <th class="info">PCRT</th>
									  <th class="info">PYDY</th>
									  <th class="info">P7</th>
									  <th class="info">P30</th>
									  <th class="danger">CRT</th>
									  <th class="danger">YDY</th>
									  <th class="danger">PCRT</th>
									  <th class="danger">PYDY</th>
									  <th class="danger">P7</th>
									  <th class="danger">P30</th>

									  
									  
									</tr>
								    </thead>
								    
								    <tbody>
												
									<tr> <!--SITE ROW-->
									<td><a href="dashboard.php?gs=1&d=226&bt=1&t1=790&t2=792&t3=794"><i class="fa fa-search" aria-hidden="true"></i></a> <i class="fa fa-file-excel-o" aria-hidden="true"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
									
									<td>JD Slator #1 Tanks</td>
									<td class="success">Good</td>
									<td><a href="dashboard.php?c=1&d=226&g=790"><?php echo convert16((15.6-getLastReading(226,790,42))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard.php?c=1&d=226&g=792"><?php echo convert16((15.6-getLastReading(226,792,42))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard.php?c=1&d=226&g=794"><?php echo convert16((15.6-getLastReading(226,794,42))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
																											
	
									</tr> <!--SITE ROW-->

									<tr> <!--SITE ROW-->
									<td><a href="dashboard.php?gs=1&d=224&bt=2&t1=790&t2=792"><i class="fa fa-search" aria-hidden="true"></i></a> <i class="fa fa-file-excel-o" aria-hidden="true"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
									
									<td>PJ Lea A&B Tanks</td>
									<td class="success">Good</td>
									<td><a href="dashboard.php?c=1&d=224&g=790"><?php echo convert16((15.6-getLastReading(224,790,42))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard.php?c=1&d=224&g=792"><?php echo convert16((15.6-getLastReading(224,792,42))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
	
									</tr> <!--SITE ROW-->
									
									<tr> <!--SITE ROW-->
									<td><a href="dashboard.php?gs=1&d=224&bt=3&t1=794&t2=796"><i class="fa fa-search" aria-hidden="true"></i></a> <i class="fa fa-file-excel-o" aria-hidden="true"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>
									<td>PJ Lea C&D Tanks</td>
									<td class="success">Good</td>
									<td><a href="dashboard.php?c=1&d=224&g=794"><?php echo convert16(getLastReading(224,794,42)); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard.php?c=1&d=224&g=796"><?php echo convert16((15.6-getLastReading(224,796,42))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
	
									</tr> <!--SITE ROW-->

									</tbody><!--SITE TBODY-->
								    
								    
								    
								    
			  </table>
        
        
        
            <?php  } ?>  
            
            
            
             <?php  if (isset($_GET['pl'])) { 
			  
			  
			  		$columns = getPMFieldArray();	
					
					
					
						/*echo "<pre>";
						print_r($data);*/
			  
			  	?>  
        
        
        		 <h1 class="page-header">Plunger Lift Management</h1>
    	        
<?php /*?>    	       <div class="btn-group" style="margin-bottom: 20px;">
                  
                  <a href="dashboard.php?pl=1<?php if ($_GET['deviceID']) echo '&deviceID='.$_GET['deviceID'].'' ?>" class="btn btn-primary <?php if ($_GET['pl'] == 1 && !isset($_GET['plc'])) echo 'active' ?>">Cycles</a>
                  
                   <a href="dashboard.php?pl=1&plc=1<?php if ($_GET['deviceID']) echo '&deviceID='.$_GET['deviceID'].'' ?>" class="btn btn-primary <?php if ($_GET['pl'] == 1 && $_GET['plc'] == 1) echo 'active' ?>">Production Chart</a>
                   
                   
 
              </div><?php */?>
               
                <form method="get" class="form-inline" action="dashboard.php" style="margin-bottom: 20px;">
      	        <input type="hidden" name="pl" value="1">
      	         <?php if ($_GET['pl'] == 1 && $_GET['plc'] == 1) echo '<input type="hidden" name="plc" value="1">' ?>
      	        
      	         <?php echo getPLDevicesByAccountSelect($_GET['deviceID'],42); ?>
      	         
      	         	<button type="submit" class="btn btn-default">Submit</button>
       				
        		</form>
     	        
     	        
     	        <?php if ($_GET['pl'] == 1 && isset($_GET['deviceID']) && !isset($_GET['plc'])) { ?>
     	        
     	      
      	         <div class="panel-group">
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" href="#collapse1">Chart</a>
						  </h4>
						</div>
						<div id="collapse1" class="panel-collapse collapse">
						  <div class="panel-body"><div id="plcchart" style="height: 600px;"></div></div>

						</div>
					  </div>
					</div> 
       	        
      	             
      	                  	      
      	         <div class="panel-group">
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" href="#collapse2">Cycles (coming soon)</a>
						  </h4>
						</div>
						<div id="collapseOLD" class="panel-collapse collapse">
						  <div class="panel-body">
      	             
								<div class="alert alert-info" role="alert">Click a reading to see reading history &nbsp;&nbsp; &nbsp; &nbsp;  [<a href="dashboard.php?pls=1">choose columns</a>] &nbsp;&nbsp; &nbsp; &nbsp;  [<a href="dashboard.php?plo=1">order columns</a>]</div>

									<div class="table-responsive">


										<table class="table table-striped"><!--STATE TABLE-->

											<thead>

											<tr><th>Register</th><th>Event Date</th><th>Actual Reading</th><th>Open Event</th>
												<?php  /*foreach ($columns as $col) {

														echo '<th>'.$col['register_name'].'</th>';


														} */
												?> 								  
											</tr>
											</thead>

											<tbody>



													<?php  	

/*														$array = array(222,223,224,225,226); 

														foreach ($array as $id) {

															$reg = getRegisterData($_GET['deviceID'],$id);

															echo '<tr><td colspan="4">'.$reg[0]['register_name'].'</td></tr>'; 


															$qry = mysqli_query($link,"SELECT * FROM `tbl_register_history` WHERE `register_number` = $id AND `register_deviceID` = ".$_GET['deviceID']." GROUP BY register_reading, register_date ORDER BY register_date DESC LIMIT 100");

															while ($row = mysqli_fetch_assoc($qry)) {

																//print_r($row);

																echo '<tr>	<td>&nbsp;</td>
																			<td>'.$row['register_date'].'</td>
																			<td><a href="dashboard.php?r=1&d='.$_GET['deviceID'].'&c=1&g='.$id.'">'.$row['register_reading'].'</a></td>
																			<td>'.getPLLabel(intval($row['register_reading'])).'</td>

																	</tr>'; 
															}


														}*/


											/*		if ($_GET['deviceID']) {

														$device = getDeviceData($_GET['deviceID']);

														for ($i=0;$i<=10;$i++) {



																	echo '<tr><td>'.formatTimestampOffset(getLastReadingTS($_GET['deviceID'],129,$i),5).'</td>'; 

																foreach ($columns as $col) {



																	echo '<td><a href="dashboard.php?c=1&d='.$device['device_deviceID'].'&g='.$col['register_number'].'">'.getLastReading($device['device_deviceID'],$col['register_number'],$i,$col['register_pl_convert']).'</a></td>';


																} 

															echo '</tr>';


														}

													}*/


												?> 			

													</tr>


											</tbody><!--SITE TBODY-->




										</table>

							</div></div>

						</div>
					  </div>
					</div> 
      	             
      	             
      	             
      	            <div class="panel-group">
					  <div class="panel panel-default">
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" href="#collapse3">Control Settings</a>
						  </h4>
						</div>
						<div id="collapse3" class="panel-collapse collapse">
						  <div class="panel-body">
						  			
						  		<div class="btn-group" style="margin-bottom: 20px;">
                  
									  <a href="dashboard.php?pl=1&deviceID=<?php echo $_GET['deviceID']; ?>&cs=1" class="btn btn-primary">Cycle Control</a>



								  </div>
						  			
						  			
						  			<?php 
				  
				  						if (!empty($_SESSION['reg_changes'])) {
											
											echo '<div class="alert alert-info" role="alert">';
											
												foreach ($_SESSION['reg_changes'] as $change) {
													
													echo '<p>'.getRegisterName($_GET['deviceID'],$change[1],42).' was changed to '.$change[2].'</p>';
												}
											
											echo '</div>';
											
										}
				  
				  					 ?>
				  					 
				  					 
				  					 
				  					<?php 
				  
				  						if ($_GET['cs'] == 1) {
											
														  
				  					 ?>	 
				  					 
				  					 
									

											<!--	<form method="post" action="process/set_registers.php">-->
												<input type="hidden" value="<?php echo $_GET['deviceID']; ?>" name="deviceID">
												
												
											<?php
											
											echo getControlRegistersSubCategoryPanels($_GET['deviceID'],$_GET['cs']);
										
				  
				  					 ?>	
												
<!--												<table class="table table-striped">
 
													<thead>   

													<tr><th>Register</th><th>Current Value</th><th>Update</th></tr>  
													</thead>
   
													<tbody>-->
													 
													
														<?php 

															/*	$controls = getCfgControlRegisters($_GET['deviceID'],42);
				  
				  												


																foreach ($controls as $control) { 
																	
																	$lastreading = getLastReading($_GET['deviceID'],$control['register_number'],42);
																	
																	if ($control['register_onoff'] == 2) {
																		
																		if ($lastreading == 0.00) {
																			$lastreading = 'Disabled'; 
																		} else {
																			$lastreading = 'Enabled';
																		}
																		
																	} 


																	echo '<tr>

																			 <td>'.$control['register_name'].'</td>
																			 <td>'.$lastreading.'</td>';
																	
																	
																		if ($control['register_onoff'] == 2) {
																			echo '<td><label class="radio-inline"><input type="radio" name="reg['.$control['register_number'].']" value="0">Disabled</label><label class="radio-inline"><input type="radio" name="reg['.$control['register_number'].']" value="1">Enabled</label></td>';
																		} else {
																			echo '<td><input type="text" name="reg['.$control['register_number'].']" class="form-control" placeholder="Enter Value (no negative numbers)" style="width:250px;"></td>';
																		}
																	
													

																	echo '</tr>';

																}
*/


														?>

								<button type="submit" class="btn btn-default" style="margin-top: 20px;">Submit</button>

								</form>

									
									
									     <?php  } ?> 
	
						  </div>

						</div>
					  </div>
					</div> 
       	        
       	        
       	         <?php  } ?>  
       	         
       	          <?php //if ($_GET['pl'] == 1 && $_GET['plc'] == 1) { ?>
       	         
       	         		
       	         		
       	         		
       	         		
       	         
       	         <?php  //} ?> 
        	        
        				
        
            <?php  } ?>  
            
            
            
            <?php  if (isset($_GET['pls'])) { ?>  
            
            	
            		<h1 class="page-header">Plunger Lift Column Settings</h1>
            		
            		<form method="post" action="process/pm_fields.php">
            		
            		<?php  
							$array = getPMArray();				 
											 
							$fields = getPMColumns();
	
							//echo "<pre>";
											 
							//print_r($fields );
							
							foreach ($fields as $field) {
								
									if ($current_cat == '' or $current_cat != $field['register_category'] && $field['register_category'] != '' or $current_cat == 99 && $field['register_category'] != '') {
								
									if ($field['register_category'] == '') {
										$current_cat = 99;
										echo '<div class="alert alert-info" role="alert">Uncategorized</div>';
									} else {
										$current_cat = $field['register_category'];
										echo '<div class="alert alert-info" role="alert">'.$field['register_category'].'</div>';	
									}

									
									}
								
								if (in_array($field['register_number'],$array['pmfield'])) {
									echo '<p class="bg-warning"><input name="pmfield[]" type="checkbox" value="'.$field['register_number'].'" checked="checked"> '.$field['register_name'].'</p>';
								} else {
									echo '<p><input name="pmfield[]" type="checkbox" value="'.$field['register_number'].'"> '.$field['register_name'].'</p>';
								}
								
								
							}
							
					?>  
           
            			<button type="submit" class="btn btn-default">Submit</button>
       				
        				</form>
        	
            
            
             <?php  } ?> 
             
             
              <?php  if (isset($_GET['plo'])) { ?>  
            
            	
            		<h1 class="page-header">Plunger Lift Column Order</h1>
            		
            		<form method="post" class="form-inline" action="process/pm_field_order.php">
            		
            		       <div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr><th>Order</th><th>Tag</th>
						  
									</tr>
								    </thead>
								    
								    <tbody>
            		
            		<?php  
							$columns = getPMFieldArray();	
		 
						
							foreach ($columns as $col) {
								
								echo '<tr><td><input class="form-control" name="reg['.$col['register_number'].']" type="text" value="'.$col['register_pl_order'].'"></td><td>'.$col['register_name'].'</td></tr>';
								
								
							}
							
					?>  
									</tbody>
							   </table>
          
						</div>
           
            			<button type="submit" class="btn btn-default">Submit</button>
       				
        				</form>
        	
            
            
             <?php  } ?> 
            
            
             <?php  if (isset($_GET['gs'])) { 

						if ($_GET['start']) {
							$dates = date_range(formatDateMYSQL($_GET['start']),formatDateMYSQL($_GET['end']));
						} else {
							$dates = date_range(date('Y-m-01'),date('Y-m-t'));
						}
						
	
						if ($_GET['bt'] == 1) {
							
							$batterylabel = 'JD Slator #1 Tanks';
							
						} else if ($_GET['bt'] == 2) {
							
							$batterylabel = 'PJ Lea A&B Tanks';
							
						} else if ($_GET['bt'] == 3) {
							
							$batterylabel = 'PJ Lea C&D Tanks';
							
						}
						

					?>  
        
        
        		 <h1 class="page-header">Gauge Sheet - <?php echo $batterylabel; ?></h1>
        		 
        		 <p><a href="../application/account_42/custom/excel_gauge.php?gs=<?php echo $_GET['gs']; ?>&d=<?php echo $_GET['d']; ?>&bt=<?php echo $_GET['bt']; ?>&t1=<?php echo $_GET['t1']; ?>&t2=<?php echo $_GET['t2']; ?>&t3=<?php echo $_GET['t3']; ?>&start=<?php echo $_GET['start']; ?>&end=<?php echo $_GET['end']; ?>">Download to Excel</a></p>
        		 
        		<div class="row" style="margin-bottom: 10px;">
        		
        		    <form method="get" class="form-inline pull-left" action="/application/account_42/custom/select_tb.php" style="margin-bottom: 20px;">
     	         
						<select class="form-control" name="tbsel">
							<option value="1" <?php if ($_GET['bt'] == 1) echo "selected"; ?>>JD Slator #1 Tanks</option>
							<option value="2" <?php if ($_GET['bt'] == 2) echo "selected"; ?>>PJ Lea A&B Tanks</option>
							<option value="3" <?php if ($_GET['bt'] == 3) echo "selected"; ?>>PJ Lea C&D Tanks</option>
						</select>
      	         
      	         	<button type="submit" class="btn btn-default">Submit</button>
       				
        		</form>
        		
        		
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				<span></span> <b class="caret"></b></div>
       			</div>
        
          			<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
										<th>&nbsp;</th>
										<th colspan="4" class="warning">Oil Tank 1</th>
										<th colspan="4" class="info">Oil Tank 2</th>
										<th colspan="4" class="danger">Water Tank</th>
										<th>&nbsp;</th>
									</tr>
									
									<tr>
								  	  <th>Day</th>
								  	  
									  <th class="warning">Top Level</th>
									  <th class="warning">Oil Stock (BBLS)</th>
									  <th class="warning">Oil Hauls (BBLS)</th>
									  <th class="warning">BOPD (BBLS)</th>
									  
									  <th class="info">Top Level</th>
									  <th class="info">Oil Stock (BBLS)</th>
									  <th class="info">Oil Hauls (BBLS)</th>
									  <th class="info">BOPD (BBLS)</th>
									  
									  <th class="danger">Top Level</th>
									  <th class="danger">Stock (BBLS)</th>
									  <th class="danger">Hauls (BBLS)</th>
									  <th class="danger">WPD (BBLS)</th>
									  
									  <th>Comments</th>
									  
									  
									</tr>
								    </thead>
								    
								    <tbody>
									

									 <?php  foreach ($dates as $date) { 
										
											if (getTankDiaryCount($_GET['d'],$_GET['bt'],$date,42) > 0) {
												$color = 'cornflowerblue';
											} else {
												$color = 'darkgrey';
											}
											
											
										?>  						
										<tr> <!--SITE ROW-->
										<td><?php echo formatDate($date);?></td>	

										  
										  <td><a href="dashboard.php?r=1&c=1&d=<?php echo $_GET['d'];?>&g=<?php echo $_GET['t1'];?>&start=<?php echo formatDate($date);?>&end=<?php echo formatDate($date);?>"><?php echo convert16((getRegReadingByDate($_GET['d'],$_GET['t1'],$date,15.6,42))); ?></a></td>	
										 	
										<td><?php echo convertBBLS((getRegReadingByDate($_GET['d'],$_GET['t1'],$date,15.6,42))); ?></td>
										  
										
										
										
										<td><?php echo getTotalOilSalesByDate($_GET['d'],$_GET['t1'],$date,'',42); ?></td>
										<td><?php echo getTotalOilProducedByDate($_GET['d'],$_GET['t1'],$date,'',42); ?></td>
										
										<td><a href="dashboard.php?r=1&c=1&d=<?php echo $_GET['d'];?>&g=<?php echo $_GET['t2'];?>&start=<?php echo formatDate($date);?>&end=<?php echo formatDate($date);?>"><?php echo convert16((getRegReadingByDate($_GET['d'],$_GET['t2'],$date,15.6,42))); ?></a></td>
										<td><?php echo convertBBLS((getRegReadingByDate($_GET['d'],$_GET['t2'],$date,15.6,42))); ?></td>	
										<td><?php echo getTotalOilSalesByDate($_GET['d'],$_GET['t2'],$date,'',42); ?></td>
										<td><?php echo getTotalOilProducedByDate($_GET['d'],$_GET['t2'],$date,'',42); ?></td>									
										<td><a href="dashboard.php?r=1&c=1&d=<?php echo $_GET['d'];?>&g=<?php echo $_GET['t3'];?>&start=<?php echo formatDate($date);?>&end=<?php echo formatDate($date);?>"><?php echo convert16((getRegReadingByDate($_GET['d'],$_GET['t3'],$date,15.6,42))); ?></a></td>
										<td><?php echo convertBBLS((getRegReadingByDate($_GET['d'],$_GET['t3'],$date,15.6,42))); ?></td>	
										<td><?php echo getTotalWaterTruckedByDate($_GET['d'],$_GET['t3'],$date,42); ?></td>
										<td><?php echo getTotalWaterProducedByDate($_GET['d'],$_GET['t3'],$date,42); ?></td>

										<td style="max-width: 120px;"><i class="fa fa-book gauge_diary" aria-hidden="true" data-date="<?php echo $date;?>" style="color: <?php echo $color;?>;"></i> <?php echo getTankDiaryText($_GET['d'],$_GET['bt'],$date,42); ?></td>
										</tr> <!--SITE ROW-->
									 <?php  }?>  
									

									</tbody><!--SITE TBODY-->
								    
								    
								</table>
								
								
								    <div id="diary_modal" title="Gauge Diary - <?php echo $batterylabel; ?>" style="display: none;">
								    
								    		<form method="post" action="process/battery_diary.php">
											<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
											<input type="hidden" name="tank_battery" value="<?php echo $_GET['bt']; ?>">
											<input type="hidden" name="diary_date" id="diarymodaldate">
											<input type="hidden" name="t1" value="<?php echo $_GET['t1']; ?>">
											<input type="hidden" name="t2" value="<?php echo $_GET['t2']; ?>">
											<input type="hidden" name="t3" value="<?php echo $_GET['t3']; ?>">
											 
											   <div class="form-group">
											  
												<label for="log_book_field">Entry</label>
												<textarea class="form-control" name="diary_text" id="diary_text_edit" rows="5"></textarea>
												

											  </div>
											  
											  <button type="submit" class="btn btn-default">Submit</button>
											</form>
											
											
											<ul class="list-group" style="margin-top: 20px;">


											
											</ul>
								    
								    	
							   
							   
  									</div>
        
        
        
            <?php  } ?>  
        
        
        
        
        
        
         <?php  if (isset($_GET['po'])) { ?>  
        
        
        
        		 
        		 
        		 <?php  if (isset($_GET['deviceID'])) { 
						
							$poctabs = getPOCGroups();
							$pocfields = getPOCFields($_GET['key'],$_GET['group']);
	
							//print_r($pocfields);
				?>  
        		 
        		 
        		 <h1 class="page-header">POC Management - <?php echo $_GET['key']; ?></h1>
        		 		 
        		 <div>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs" role="tablist">
				  
				  		
				  		<?php  foreach ($poctabs as $key=>$tab) {
				  		
							if ($tab['tag_group'] == $_GET['group']) {
										echo '<li role="presentation" class="active"><a href="dashboard.php?po=1&deviceID='.$_GET['deviceID'].'&key='.$_GET['key'].'&group='.$tab['tag_group'].'" aria-controls="home" role="tab">'.$tab['tag_group'].'</a></li>';
								
							} else {
										echo '<li role="presentation" ><a href="dashboard.php?po=1&deviceID='.$_GET['deviceID'].'&key='.$_GET['key'].'&group='.$tab['tag_group'].'" aria-controls="home" role="tab">'.$tab['tag_group'].'</a></li>';
							}
					
						
						} ?>  
				  </ul>
						
						
						<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr>
								  	  <th>Tag Name</th>
								  	  <th>Description</th>
								  	  <th>Read</th>
								  	   <th>Value</th>
								  	   <th>Unit</th>
								  	   <th>Timestamp (CDT)</th>
								  	     <th>iTimestamp (CDT)</th>

									</tr>
							    
								    </thead>
								    
								    <tbody>
								    
								    
									<?php  foreach ($pocfields as $key=>$field) {

													$opcvalues = getOPCCurrentValue($field['id_read']);
					
													//echo "<pre>";
													//print_r($opcvalues);
					
													if ($opcvalues->value != '') {
														
															echo '<tr>
															<td>'.$field['tag_name'].'</td>
															<td><a class="modallink" title="'.$field['tag_description'].'">View</a></td>
															<td><a href="http://208.64.57.215:8088/beeond/Quest/1.0.0/Readings/CurrentValue?nodeid=ns=1;s='.$field['id_read'].'" target="_blank">'.$field['id_read'].'</a></td>
															<td>'.$opcvalues->value.'</td>
															<td>'.$field['unit'].'</td>
															<td>'.date('m/d/Y g:i A', $opcvalues->timestamp-18000).'</td>
															<td>'.date('m/d/Y g:i A', $opcvalues->itimestamp-18000).'</td>
															</tr>';
														
													} else {
														
																											echo '<tr>
															<td>'.$field['tag_name'].'</td>
															<td><a class="modallink" title="'.$field['tag_description'].'">View</a></td>
															<td><a href="http://208.64.57.215:8088/beeond/Quest/1.0.0/Readings/CurrentValue?nodeid=ns=1;s='.$field['id_read'].'" target="_blank">'.$field['id_read'].'</a></td>
															<td>--</td>
															<td>'.$field['unit'].'</td>
															<td>--</td>
															</tr>';
													}




									} ?>  
								    
								    
								    
								   </tbody>
       			 
						  </table>


				</div>
      			  
        			  
        			  <?php  } else { ?>  
        			  
        			  		 <h1 class="page-header">POC Summary</h1>
        			  		 
        			  <div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr>
								  	  <th>POC</th>
								  	   <th>Comm Status</th>
								  	   <th>Controller Mode</th>
								  	   <th>Operation Mode</th>
								  	   <th>Auto DT Mode</th>
								  	   <th>Inferred Production (BBL)</th>
								  	   <th>Inferred Production Y (BBL)</th>
								  	   <th>Percent Runtime (%)</th>
								  	   <th>Percent Runtime Date</th>
								  	   <th>Percent Runtime Y (%)</th>
								  	   <th>Tot Strokes</th>
								  	   <th>Tot Strokes Y</th>
								  	   
								  	   <th>Timed On (Hours)</th>
								  	   <th>Timed On (Mins)</th>
								  	   <th>Timed Off (Hours)</th>
								  	   <th>Timed Off (Mins)</th>
								  	   <th>Pump Fill Last Stroke (%)</th>
								  	   <th>Avg Gross Pump Eff (%)</th>
								  	   <th>Pump Fill Violation Set (%)</th>
								  	   <th>Gauge Off Time (Hours)</th>
								  	   <th>Peak Load (Lbs)</th>
								  	   <th>Pump Card Min Load (Lbs)</th>
								  	   <th>Stroke Length (Inches)</th>
								  	   <th>Rod Odometer</th>
								  	   <th>Strokes/Min</th>
									</tr>
							    
								    </thead>
								    
								    <tbody>
								    
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=224&key=PJ Lea A2&group=Consecutive Allowed">PJ Lea A2</a></td>
								    		<td><?php echo getLastReading(224,516,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(224,517); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(224,522); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(224,515); ?></td>
								    		<td><?php echo getLastReading(224,500,42); ?></td>
								    		<td><?php echo getLastReading(224,501,42); ?></td>
								    		<td><?php echo getLastReading(224,523,42); ?></td>
								    		<td><?php echo getLastReadingDate(224,523,42); ?></td>
								    		<td><?php echo getLastReading(224,524,42); ?></td>
								    		<td><?php echo getLastReading(224,561,42); ?></td>
								    		<td><?php echo getLastReading(224,564,42); ?></td>
								    		
								    		<td><?php echo getLastReading(224,586,42); ?></td>
								    		<td><?php echo getLastReading(224,587,42); ?></td>
								    		<td><?php echo getLastReading(224,584,42); ?></td>
								    		<td><?php echo getLastReading(224,585,42); ?></td>
								    		<td><?php echo getLastReading(224,525,42); ?></td>
								    		<td><?php echo getLastReading(224,549,42); ?></td>
								    		<td><?php echo getLastReading(224,526,42); ?></td>
								    		<td><?php echo getLastReading(224,504,42); ?></td>
								    		<td><?php echo getLastReading(224,511,42); ?></td>
								    		<td><?php echo getLastReading(224,513,42); ?></td>
								    		<td><?php echo getLastReading(224,559,42); ?></td>
								    		<td><?php echo getLastReading(224,566,42); ?></td>
								    		<td><?php echo getLastReading(224,527,42); ?></td>
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=224&key=PJ Lea B3&group=Consecutive Allowed">PJ Lea B3</a></td>
								    		<td><?php echo getLastReading(224,286,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(224,287,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(224,292,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(224,285,42); ?></td>
								    		<td><?php echo getLastReading(224,270,42); ?></td>
								    		<td><?php echo getLastReading(224,271,42); ?></td>
								    		<td><?php echo getLastReading(224,293,42); ?></td>
								    		<td><?php echo getLastReadingDate(224,293,42); ?></td>
								    		<td><?php echo getLastReading(224,294,42); ?></td>
								    		<td><?php echo getLastReading(224,331,42); ?></td>
								    		<td><?php echo getLastReading(224,334,42); ?></td>
								    		
								    		<td><?php echo getLastReading(224,356,42); ?></td>
								    		<td><?php echo getLastReading(224,357,42); ?></td>
								    		<td><?php echo getLastReading(224,354,42); ?></td>
								    		<td><?php echo getLastReading(224,355,42); ?></td>
								    		<td><?php echo getLastReading(224,295,42); ?></td>
								    		<td><?php echo getLastReading(224,319,42); ?></td>
								    		<td><?php echo getLastReading(224,296,42); ?></td>
								    		<td><?php echo getLastReading(224,274,42); ?></td>
								    		<td><?php echo getLastReading(224,281,42); ?></td>
								    		<td><?php echo getLastReading(224,283,42); ?></td>
								    		<td><?php echo getLastReading(224,329,42); ?></td>
								    		<td><?php echo getLastReading(224,336,42); ?></td>
								    		<td><?php echo getLastReading(224,297,42); ?></td>
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=224&key=PJ Lea B4&group=Consecutive Allowed">PJ Lea B4</a></td>
								    		<td><?php echo getLastReading(224,56,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(224,57,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(224,62,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(224,55,42); ?></td>
								    		<td><?php echo getLastReading(224,40,42); ?></td>
								    		<td><?php echo getLastReading(224,41,42); ?></td>
								    		<td><?php echo getLastReading(224,63,42); ?></td>
								    		<td><?php echo getLastReadingDate(224,63,42); ?></td>								    		
								    		<td><?php echo getLastReading(224,64,42); ?></td>
								    		<td><?php echo getLastReading(224,101,42); ?></td>
								    		<td><?php echo getLastReading(224,104,42); ?></td>
								    		
								    		<td><?php echo getLastReading(224,126,42); ?></td>
								    		<td><?php echo getLastReading(224,127,42); ?></td>
								    		<td><?php echo getLastReading(224,124,42); ?></td>
								    		<td><?php echo getLastReading(224,125,42); ?></td>
								    		<td><?php echo getLastReading(224,65,42); ?></td>
								    		<td><?php echo getLastReading(224,89,42); ?></td>
								    		<td><?php echo getLastReading(224,66,42); ?></td>
								    		<td><?php echo getLastReading(224,44,42); ?></td>
								    		<td><?php echo getLastReading(224,51,42); ?></td>
								    		<td><?php echo getLastReading(224,53,42); ?></td>
								    		<td><?php echo getLastReading(224,99,42); ?></td>
								    		<td><?php echo getLastReading(224,106,42); ?></td>
								    		<td><?php echo getLastReading(224,67,42); ?></td>
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=236&key=PJ Lea B5&group=Consecutive Allowed">PJ Lea B5</a></td>
								    		<td><?php echo getLastReading(236,516,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(236,517,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(236,522,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(236,515,42); ?></td>
								    		<td><?php echo getLastReading(236,500,42); ?></td>
								    		<td><?php echo getLastReading(236,501,42); ?></td>
								    		<td><?php echo getLastReading(236,523,42); ?></td>
								    		<td><?php echo getLastReadingDate(236,523,42); ?></td>
								    		<td><?php echo getLastReading(236,524,42); ?></td>
								    		<td><?php echo getLastReading(236,561,42); ?></td>
								    		<td><?php echo getLastReading(236,564,42); ?></td>
								    		
								    		<td><?php echo getLastReading(236,586,42); ?></td>
								    		<td><?php echo getLastReading(236,587,42); ?></td>
								    		<td><?php echo getLastReading(236,584,42); ?></td>
								    		<td><?php echo getLastReading(236,585,42); ?></td>
								    		<td><?php echo getLastReading(236,525,42); ?></td>
								    		<td><?php echo getLastReading(236,549,42); ?></td>
								    		<td><?php echo getLastReading(236,526,42); ?></td>
								    		<td><?php echo getLastReading(236,504,42); ?></td>
								    		<td><?php echo getLastReading(236,511,42); ?></td>
								    		<td><?php echo getLastReading(236,513,42); ?></td>
								    		<td><?php echo getLastReading(236,559,42); ?></td>
								    		<td><?php echo getLastReading(236,566,42); ?></td>
								    		<td><?php echo getLastReading(236,527,42); ?></td>
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=236&key=PJ Lea C1&group=Consecutive Allowed">PJ Lea C1</a></td>
								    		<td><?php echo getLastReading(236,56,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(236,57,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(236,62,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(236,55,42); ?></td>
								    		<td><?php echo getLastReading(236,40,42); ?></td>
								    		<td><?php echo getLastReading(236,41,42); ?></td>
								    		<td><?php echo getLastReading(236,63,42); ?></td>
								    		<td><?php echo getLastReadingDate(236,63,42); ?></td>
								    		<td><?php echo getLastReading(236,64,42); ?></td>
								    		<td><?php echo getLastReading(236,101,42); ?></td>
								    		<td><?php echo getLastReading(236,104,42); ?></td>
								    		
								    		<td><?php echo getLastReading(236,126,42); ?></td>
								    		<td><?php echo getLastReading(236,127,42); ?></td>
								    		<td><?php echo getLastReading(236,124,42); ?></td>
								    		<td><?php echo getLastReading(236,125,42); ?></td>
								    		<td><?php echo getLastReading(236,65,42); ?></td>
								    		<td><?php echo getLastReading(236,89,42); ?></td>
								    		<td><?php echo getLastReading(236,66,42); ?></td>
								    		<td><?php echo getLastReading(236,44,42); ?></td>
								    		<td><?php echo getLastReading(236,51,42); ?></td>
								    		<td><?php echo getLastReading(236,53,42); ?></td>
								    		<td><?php echo getLastReading(236,99,42); ?></td>
								    		<td><?php echo getLastReading(236,106,42); ?></td>
								    		<td><?php echo getLastReading(236,67,42); ?></td>
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=235&key=PJ Lea C2&group=Consecutive Allowed">PJ Lea C2</a></td>
								    		<td><?php echo getLastReading(235,286,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(235,287,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(235,292,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(235,285,42); ?></td>
								    		<td><?php echo getLastReading(235,270,42); ?></td>
								    		<td><?php echo getLastReading(235,271,42); ?></td>
								    		<td><?php echo getLastReading(235,293,42); ?></td>
								    		<td><?php echo getLastReadingDate(235,293,42); ?></td>
								    		<td><?php echo getLastReading(235,294,42); ?></td>
								    		<td><?php echo getLastReading(235,331,42); ?></td>
								    		<td><?php echo getLastReading(235,334,42); ?></td>
								    		
								    		<td><?php echo getLastReading(235,356,42); ?></td>
								    		<td><?php echo getLastReading(235,357,42); ?></td>
								    		<td><?php echo getLastReading(235,354,42); ?></td>
								    		<td><?php echo getLastReading(235,355,42); ?></td>
								    		<td><?php echo getLastReading(235,295,42); ?></td>
								    		<td><?php echo getLastReading(235,319,42); ?></td>
								    		<td><?php echo getLastReading(235,296,42); ?></td>
								    		<td><?php echo getLastReading(235,274,42); ?></td>
								    		<td><?php echo getLastReading(235,281,42); ?></td>
								    		<td><?php echo getLastReading(235,283,42); ?></td>
								    		<td><?php echo getLastReading(235,329,42); ?></td>
								    		<td><?php echo getLastReading(235,336,42); ?></td>
								    		<td><?php echo getLastReading(235,297,42); ?></td>
								    	</tr>
								    	
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=236&key=PJ Lea C3&group=Consecutive Allowed">PJ Lea C3</a></td>
								    		<td><?php echo getLastReading(236,286,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(236,287,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(236,292,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(236,285,42); ?></td>
								    		<td><?php echo getLastReading(236,270,42); ?></td>
								    		<td><?php echo getLastReading(236,271,42); ?></td>
								    		<td><?php echo getLastReading(236,293,42); ?></td>
								    		<td><?php echo getLastReadingDate(236,293,42); ?></td>
								    		<td><?php echo getLastReading(236,294,42); ?></td>
								    		<td><?php echo getLastReading(236,331,42); ?></td>
								    		<td><?php echo getLastReading(236,334,42); ?></td>
								    		
								    		<td><?php echo getLastReading(236,356,42); ?></td>
								    		<td><?php echo getLastReading(236,357,42); ?></td>
								    		<td><?php echo getLastReading(236,354,42); ?></td>
								    		<td><?php echo getLastReading(236,355,42); ?></td>
								    		<td><?php echo getLastReading(236,295,42); ?></td>
								    		<td><?php echo getLastReading(236,319,42); ?></td>
								    		<td><?php echo getLastReading(236,296,42); ?></td>
								    		<td><?php echo getLastReading(236,274,42); ?></td>
								    		<td><?php echo getLastReading(236,281,42); ?></td>
								    		<td><?php echo getLastReading(236,283,42); ?></td>
								    		<td><?php echo getLastReading(236,329,42); ?></td>
								    		<td><?php echo getLastReading(236,336,42); ?></td>
								    		<td><?php echo getLastReading(236,297,42); ?></td>
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=235&key=PJ Lea D1&group=Consecutive Allowed">PJ Lea D1</a></td>
								    		<td><?php echo getLastReading(235,56,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(235,57,42); ?></td>
								   			<td><?php echo 'Timed'; //getLastReading(235,62,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(235,55,42); ?></td>
								    		<td><?php echo getLastReading(235,40,42); ?></td>
								    		<td><?php echo getLastReading(235,41,42); ?></td>
								    		<td><?php echo getLastReading(235,63,42); ?></td>	
								    		<td><?php echo getLastReadingDate(235,63,42); ?></td>	
								    		<td><?php echo getLastReading(235,64,42); ?></td>
								    		<td><?php echo getLastReading(235,101,42); ?></td>
								    		<td><?php echo getLastReading(235,104,42); ?></td>
								    		
								    		<td><?php echo getLastReading(235,126,42); ?></td>
								    		<td><?php echo getLastReading(235,127,42); ?></td>
								    		<td><?php echo getLastReading(235,124,42); ?></td>
								    		<td><?php echo getLastReading(235,125,42); ?></td>
								    		<td><?php echo getLastReading(235,65,42); ?></td>
								    		<td><?php echo getLastReading(235,89,42); ?></td>
								    		<td><?php echo getLastReading(235,66,42); ?></td>
								    		<td><?php echo getLastReading(235,44,42); ?></td>
								    		<td><?php echo getLastReading(235,51,42); ?></td>
								    		<td><?php echo getLastReading(235,53,42); ?></td>
								    		<td><?php echo getLastReading(235,99,42); ?></td>
								    		<td><?php echo getLastReading(235,106,42); ?></td>
								    		<td><?php echo getLastReading(235,67,42); ?></td>
								    		
								    	</tr>
								    	
								    	<tr>
								    		<td><a href="dashboard.php?po=1&deviceID=226&key=JD Slater 1&group=Consecutive Allowed">JD Slater 1</a></td>
								    		<td><?php echo getLastReading(226,56,42); ?></td>
								    		<td><?php echo 'Downhole'; //getLastReading(224,57,42); ?></td>
								    		<td><?php echo 'Timed'; //getLastReading(224,62,42); ?></td>
								    		<td><?php echo 'Manual'; //getLastReading(224,55,42); ?></td>
								    		<td><?php echo getLastReading(226,40,42); ?></td>
								    		<td><?php echo getLastReading(226,41,42); ?></td>
								    		<td><?php echo getLastReading(226,63,42); ?></td>
								    		<td><?php echo getLastReadingDate(226,63,42); ?></td>								    		
								    		<td><?php echo getLastReading(226,64,42); ?></td>
								    		<td><?php echo getLastReading(226,101,42); ?></td>
								    		<td><?php echo getLastReading(226,104,42); ?></td>
								    		
								    		<td><?php echo getLastReading(226,126,42); ?></td>
								    		<td><?php echo getLastReading(226,127,42); ?></td>
								    		<td><?php echo getLastReading(226,124,42); ?></td>
								    		<td><?php echo getLastReading(226,125,42); ?></td>
								    		<td><?php echo getLastReading(226,65,42); ?></td>
								    		<td><?php echo getLastReading(226,89,42); ?></td>
								    		<td><?php echo getLastReading(226,66,42); ?></td>
								    		<td><?php echo getLastReading(226,44,42); ?></td>
								    		<td><?php echo getLastReading(226,51,42); ?></td>
								    		<td><?php echo getLastReading(226,53,42); ?></td>
								    		<td><?php echo getLastReading(226,99,42); ?></td>
								    		<td><?php echo getLastReading(226,106,42); ?></td>
								    		<td><?php echo getLastReading(226,67,42); ?></td>
								    	</tr>
								    	
								    	
								    	
								    
									</tbody>
       			 
						  </table>
        			 
        			  
        			  <?php  } ?>  
            
            
            <?php  } ?>  
            
            
            
            
              <?php  if (isset($_GET['ct'])) { 
								
								
								
					$columns = getCTOFieldArray();	?>  
        
        
        		 <h1 class="page-header">County/Field Overview</h1>
        		 
        		 
        		       	 <div class="alert alert-info" role="alert">Click a reading to see reading history &nbsp;&nbsp; &nbsp; &nbsp;  [<a href="dashboard.php?cto=1">choose columns</a>]</div>
        		 
        		 
       		      		<h2>Wells</h2>
        		      
          					<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr>
										
									<thead>
									
									<tr>
								  	
								  		<th>Device</th>
									  	<?php  foreach ($columns as $col) {
					
												echo '<th>'.$col['register_name'].'</th>';


												} 
										?> 								  
									</tr>
								    </thead>
									  
									</tr>
								    </thead>
								    
								    <tbody>
												
								

									</tbody><!--SITE TBODY-->
		
								    
									  </table>
									  
			  					</div>
			  					
			  					
			  					
			  					
			  					<h2>Compressors</h2>
        		      
          					<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr>

								
									  
									</tr>
								    </thead>
								    
								    <tbody>
												
								

									</tbody><!--SITE TBODY-->
		
								    
									  </table>
									  
			  					</div>
			  					
			  					
			  					<h2>Field Variance</h2>
			  					
			  					<div class="alert alert-success" role="alert">
										Variance in MCF
								</div>
			  					
			  					

        
        
          <?php  } ?> 
          
          
          
           <?php  if (isset($_GET['cto'])) { ?>  
            
            	
            		<h1 class="page-header">County/Field Column Settings</h1>
            		
            		<form method="post" action="process/cto_fields.php">
            		
            		<?php  
							$array = getCTOArray();				 
											 
							$fields = getCTOColumns();
									
	
							//echo "<pre>";
							//print_r($fields);
							
							foreach ($fields as $field) {
								
								if ($current_cat == '' or $current_cat != $field['register_category'] && $field['register_category'] != '' or $current_cat == 99 && $field['register_category'] != '') {
								
									if ($field['register_category'] == '') {
										$current_cat = 99;
										echo '<div class="alert alert-info" role="alert">Uncategorized</div>';
									} else {
										$current_cat = $field['register_category'];
										echo '<div class="alert alert-info" role="alert">'.$field['register_category'].'</div>';	
									}

									
								}
								
								if (in_array($field['register_number'],$array['ctofield'])) {
									echo '<p class="bg-warning"><input name="ctofield[]" type="checkbox" value="'.$field['register_number'].'" checked="checked"> '.$field['register_name'].'</p>';
								} else {
									echo '<p><input name="ctofield[]" type="checkbox" value="'.$field['register_number'].'"> '.$field['register_name'].'</p>';
								}
								
								
							}
							
					?>  
           
            			<button type="submit" class="btn btn-default">Submit</button>
       				
        				</form>
        	
            
            
             <?php  } ?> 


        
          <?php  if (isset($_GET['ntm'])) { ?>  
            
            	
            		<h1 class="page-header">Notes/Comments </h1>
            		
            		
            			<form method="post" action="process/add_note.php">
						<input type="hidden" name="note_geo" value="<?php echo $_GET['geo']; ?>">
					   
					   		<?php  if (isset($_GET['dv'])) { ?> 
						   <input type="hidden" name="note_device" value="<?php echo $_GET['dv']; ?>">
						   <?php  } ?>  
						   
						  <div class="form-group">

							<label for="note_body">Related Date</label>
							<input type="text" class="form-control datepicker" name="note_date" value="<?php echo formatDate($_GET['date']); ?>">

						  </div>

						   <div class="form-group">

							<label for="note_body">Note/Comment</label>
							<textarea name="note_body" class="form-control" rows="3"></textarea>

						  </div>

						  <button type="submit" class="btn btn-default">Submit</button>
						</form>
            		
            		
            			<h3>Note History</h3>
            			<div class="table-responsive">
           
             
								<table class="table table-striped"><!--NOTES TABLE-->

									<thead>
									
									<tr>

										<th>Timestamp</th>
										<th>Date</th>
										<th>Geo</th>
										<th>Device</th>
										<th>User</th>
										<th>Note</th>
									  
									</tr>
								    </thead>
								    
								    <tbody>
										
										<?php  	
	
											$notes = getNotes(42);
										   
										   	//print_r($notes);

											foreach ($notes as $note) {

											
												echo '<tr><td>'.formatTimestampOffset($note['note_timestamp'],5).'</td><td>'.formatDate($note['note_date']).'</td>
												<td>'.$note['geo_label'].'</td><td>'.$note['device_name'].'</td><td>Keystone</td><td>'.$note['note_body'].'</td>'; 
	
						
												
												echo '</tr>';
												
											}
										?> 			
								

									</tbody><!--NOTES TBODY-->
		
								    
									  </table>
									  
			  					</div>
            		
            		
        	
            
            
             <?php  } ?> 
         


        <?php  if ($_GET['m'] == 1) { ?>  

          	<h2 class="sub-header">Location & Status</h2>

			<div class="alert-success">Device Properly Working</div>
			<div id="map"></div>

        <?php  } ?>  

        <?php  if ($_GET['f'] == 1) { ?>  
          	


          	<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

        <?php  } ?>  
        
        

        <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<h2><?php echo getDeviceName($_GET['d'],42); ?></h2>
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],42); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],42); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							 <!-- <th>GMT Date</th>-->
							  <th>CDT Date (GMT -5)</th>
							  <th>Reading</th>
							</tr>
							</thead>
							
							<tbody>
							
						<?php $readings = getRegisterReadings($_GET['d'],$_GET['g'],42); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {
									
									if ($_GET['readingID'] && $_GET['readingID'] == $reading['readingID'] ) {
										
										echo '<tr class="info">
											<td>'.$reading['register_number'].'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 
										
									} else {
										
										echo '<tr>
											<td>'.$reading['register_number'].'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 
										
									}
									

	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  
						  </tbody>
								</table>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Reporting Configuration</h2>
          	  	
          	  	<p>Reporting Time Range <input name="config_start_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_start_time",42); ?>"> to <input name="config_end_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_end_time",42); ?>"> </p>
            
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>Label</th>
							  <th>Config</th>
							  <th>Measurement Factor</th>
							   <th>Alarm</th>
							  <th>Produce/Sell</th>
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getRegistersByDevice($_GET['d'],42); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {
								
								$noprodsell = '';
								$prodsell = '';
								
								if ($reg['register_prod_sell'] == 1) {
									$noprodsell = ' checked="checked" ';
								}
								
								if ($reg['register_prod_sell'] == 2) {
									$prodsell = ' checked="checked" ';
								}
								
								if ($reg['register_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}
								
								if ($reg['register_label'] == '') {
									
									$reg['register_label'] = $reg['register_name'];
								}
								

								echo '<tr '.$configset.'>
										<td id="'.$reg['register_number'].'">'.$reg['register_number'].'</td>
										<td><input name="register_label" class="reglabel" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="25" type="text" value="'.$reg['register_label'].'"> <i class="fa fa-info-circle fa-lg" aria-hidden="true" title="#'.$reg['register_number'].'<br>'.$reg['register_label'].'<br>'.$reg['register_name'].'"></i></td>
										<td>'.getRegistersConfigSelect($_GET['d'],$reg['register_number'],$reg['register_config']).'</td>
										<td><input name="measurement_factor" class="regfactor" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="8" type="text" value="'.$reg['register_factor'].'"></td>
										<td>&nbsp;</td>
										<td><input name="produce_sell_'.$reg['register_number'].'" class="regprodsell" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  type="radio" value="1" '.$noprodsell.' > No <input name="produce_sell_'.$reg['register_number'].'" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  class="regprodsell" type="radio" value="2"  '.$prodsell.'> Yes</td>
										<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['register_number'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  



     
        
         <?php  if ($_GET['b'] == 1) { 
							  
							  
		 ?>  
		
		
			<h2 class="sub-header">Digital Log Book [<a href="#" id="entryview">entry view</a>]</h2>
			
			<p><strong>Add New Field </strong> - Please choose a type: </p>
			
			 
			
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="1"> This field will compare entered log book values to auto calculated values
				  </label>
				</div>
				
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="2"> This field will be a calculation of up to 3 other fields
				  </label>
				</div>
				
			  <div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="3"> This field is for entry only
				  </label>
				</div>
				

					
				<div id="log_field1" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Comparison">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
						<?php echo  getLogCompareField(); ?> 
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				
				<div id="log_field2" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Calculation">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Calculated Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Calculated Field Name"> = 
						
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name"> 
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
									
						
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				<div id="log_field3" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Entry Only">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	
		
				
				<div id="entry_modal" title="Log Book Entry" style="display: none;">
						
					<h4>Enter Reading</h4>
					
					   <div class="table-responsive">
						<table class="table table-striped" style="width:600px;">

						  <tbody>

						  <tr>
							  <td>Date</td>
							  <td><input type="text" class="form-control datepicker" name="reading_date" placeholder="Enter Date"></td>
						</tr>
						
						<tr>
							  <td>Time</td>
							  <td><input type="time" class="form-control" name="reading_time" placeholder="Enter Time"></td>
						</tr>
						
						
						 <?php 

								$fields = getLogBookFieldsByDevice($_GET['d']);

								//print_r($fields);

							foreach ($fields as $key=>$field) {  

							echo '<tr>
									  <td>'.$field['log_book_field'].'</td>
									  <td><input type="text" class="form-control" name="'.$field['logfieldID'].'" placeholder="Enter Value"></td>
								</tr>';

							}
					   ?>

						              </tbody>
            </table>
          </div>
						<p>Comments</p>

				</div>


        
         
          <div class="table-responsive" style="margin-top: 30px;">
           
            <p><strong>Current Fields </strong></p>
           
            <table class="table table-striped" style="width:700px;">

              <tbody>
              
<!--              <tr>
				  <td>Date</td>
				  <td><input type="text" class="form-control datepicker" name="'.$field['logfieldID'].'" placeholder="Enter Date"></td>
			</tr>-->
               
               <?php 
					
						$fields = getLogBookFieldsByDevice($_GET['d']);
						
						//print_r($fields);

					foreach ($fields as $key=>$field) {  
						
						if ($field['log_field_type'] == 'Comparison') {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>Compare to: '.$field['log_book_comparison'].'</td>
						   </tr>';
	
						} else if ($field['log_field_type'] == 'Calculation') {
							
							$calcfields = unserialize($field['log_book_calcs']);
							$calcops = unserialize($field['log_book_math']);
							$calcstring = '';
							
							foreach ($calcfields as $key=>$calc) {
								
								$calcstring .= $calc.' '.$calcops[$key].' ';
							}
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>'.$calcstring.'</td>
						   </tr>';

					
						} else  {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>&nbsp;</td>
						   </tr>';
														
						}
						
			   		 
					
					}
			   ?>
              </tbody>
            </table>
          </div>
          
          
          
          	   <div id="log_modal" title="Edit Log Book Field" style="display: none;">
          	   
          	   	<form method="post" action="process/edit_log_book_field.php"  class="form-inline">
          	   	<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
					<input type="hidden" name="logfieldID" id="logfieldID_edit">
						  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" id="log_book_field_edit" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit</button>
					</form>
							   
							   
			</div>

        <?php  } ?>  




        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="/application/libraries/jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="/application/libraries/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    
    

    <?php  if ($_GET['f']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    
    <?php if ($_GET['pl'] == 1) { ?>
			<script>
				
				
			<?php  if ($_GET['cs']) { ?>  
					$("#collapse3").collapse('show');
			<?php  } ?> 
				
				
			  $('#plcchart').highcharts({
				chart: {
					type: 'line',
					zoomType: 'x'
				},
				title: {
					text: 'Plunger Lift Analysis'
				},
				subtitle: {
					text: '<?php 	
																								 
									if ($_GET['deviceID'])	{
										echo getDeviceName($_GET['deviceID'],42);
									} 
																							 
									?>',
				},
				xAxis: {
					type: 'datetime',
					
						labels: {
	
								 format: '{value:%b %d}',

							},
		
					title: {
						text: 'Date'
					}
				},
				yAxis: {
					title: {
						text: 'Flow'
					},
					min: 0
				},
				tooltip: {
					//headerFormat: '<b>{series.name}</b><br>',
					
				},

				plotOptions: {
					spline: {
						marker: {
							enabled: true
						}
					}
				},

				series: [{
					name: 'Gas Spot Rate (MCF)',
					data: <?php include('custom/time_series_gas.php'); ?>,
					color: '#C00000'

				},{
					name: 'Pressure Casing (PSI)',
					data: <?php include('custom/time_series_casing.php'); ?>,
					color: '#7a00cc'

				},{
					name: 'Gas Differential (H2O)',
					data: <?php include('custom/time_series_gas_diff.php'); ?>,
					color: '#00B300'

				},{
					name: 'Gas Static (PSI)',
					data: <?php include('custom/time_series_gas_stat.php'); ?>,
					color: '#FF8000'

				},{
					name: 'Pressure Tubing (PSI)',
					data: <?php include('custom/time_series_tubing.php'); ?>,
					color: '#000000'

				}]
			});
		
			</script>
 	<?php  } ?> 
    
    
    <?php  if (!isset($_GET['f']) && ( $_GET['s'] or $_GET['g'] or $_GET['r'] or $_GET['gs'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   
				    <?php } else { ?>
           
				   	 var start = moment('<?php echo date('Y-m-01'); ?>');
					 var end = moment('<?php echo date('Y-m-t'); ?>');
				   
					 //var start = moment().subtract(29, 'days');
					 //var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['d'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=<?php echo $_GET['r']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   	<?php } else if ($_GET['r'] && $_GET['d'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&geo=<?php echo $_GET['geo']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   
					 	<?php } else if ($_GET['gs'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?d=<?php echo $_GET['d']; ?>&gs=1&bt=<?php echo $_GET['bt']; ?>&t1=<?php echo $_GET['t1']; ?>&t2=<?php echo $_GET['t2']; ?>&t3=<?php echo $_GET['t3']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	
 	
 	
 	<?php  if (!$_GET['m'] && !$_GET['c'] && !$_GET['b'] && !$_GET['f']  && !$_GET['gs']  && !$_GET['pl']  && !$_GET['po'] && !$_GET['ntm']) { ?>  
   
   	<?php  if ($_GET['s'] != 4) { ?>  
   	
    	<script>
               $(function () {
				   
				   <?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { ?>  
                
				   Highcharts.chart('chart', {
                    title: {
                        text: '<?php 	
																								 
									if (isset($_GET['d']) && $_GET['d'] != 229 && $_GET['d'] != 999  && $_GET['d'] != 223 )	{
										echo getDeviceName($_GET['d'],42) . ' - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 9)	{
										echo 'Cunningham-Davis B 5-74 - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 27)	{
										echo 'Riley 2G - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 16)	{
										echo 'RH Cummins 1 - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 18)	{
										echo 'Gaines County Sales - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 19)	{
										echo 'Gaines County Check Meter - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 10)	{
										echo 'Dora Cunningham DO A1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 28)	{
										echo 'Bellinger 1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 29)	{
										echo 'State 1 - Production & Sales';
									} else {
										echo $regions[$_GET['r']].' - Production & Sales';
									}
			
																							 
									
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day,
						
							labels: {
	
								 format: '{value:%b %d}',

							}
                    },
                    yAxis: {
                        title: {
                            text: 'Value'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        //valueSuffix: 'bbl'tooltip: {
						xDateFormat: '%m/%d/%Y',
                   },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Gas Prod/Sold',
                        data: <?php include('custom/time_series_gas_total.php'); ?>,
						color: '#DF5353' // red
                    },{
                        name: 'Oil Produced',
                        data: <?php include('custom/time_series.php'); ?>,
						color: '#55BF3B' // green
                    }, {
                        name: 'Oil Sold',
                        data: <?php include('custom/time_series_sales.php'); ?>,
						color: '#2d862d'
                    }, {
                        name: 'Water Production',
                        data: <?php include('custom/time_series_water.php'); ?>,
						color: '#003cb3'
                   }]
                });
				   
				<?php  } ?>  
	
				 
				   <?php  if ($_SESSION['chart_type'] == 2 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'waterfall',

							},
					   
                    title: {
                        text: '<?php 
									
									if (isset($_GET['d']) && $_GET['d'] != 229 && $_GET['d'] != 999  && $_GET['d'] != 223 )	{
										echo getDeviceName($_GET['d'],42) . ' - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 9)	{
										echo 'Cunningham-Davis B 5-74 - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 27)	{
										echo 'Riley 2G - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 16)	{
										echo 'RH Cummins 1 - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 18)	{
										echo 'Gaines County Sales - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 19)	{
										echo 'Gaines County Check Meter - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 10)	{
										echo 'Dora Cunningham DO A1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 28)	{
										echo 'Bellinger 1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 29)	{
										echo 'State 1 - Production & Sales';
									} else {
										echo $regions[$_GET['r']].' - Production & Sales';
									}
															  
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
							tickInterval: 1 * 24 * 3600 * 1000, // one day
							tickWidth: 0,
							gridLineWidth: 1,
							labels: {
								align: 'left',
								 format: '{value:%m-%d}',
								x: 3,
								y: -3
							}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
  					series: [{
                        name: 'Gas Prod/Sold',
                        data: <?php include('custom/time_series_gas_total.php'); ?>,
						color: '#DF5353' // red
                    },{
                        name: 'Oil Produced',
                        data: <?php include('custom/time_series.php'); ?>,
						color: '#55BF3B' // green
                    }, {
                        name: 'Oil Sold',
                        data: <?php include('custom/time_series_sales.php'); ?>,
						color: '#2d862d'
                    }, {
                        name: 'Water Production',
                        data: <?php include('custom/time_series_water.php'); ?>,
						color: '#003cb3'
                   }]
                });
				   
				<?php  } ?>  
			   				   
	
				   
				 <?php  if ($_SESSION['chart_type'] == 3 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'bar',

							},
					   
                    title: {
                        text: '<?php 
															
									if (isset($_GET['d']) && $_GET['d'] != 229 && $_GET['d'] != 999  && $_GET['d'] != 223 )	{
										echo getDeviceName($_GET['d'],42) . ' - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 9)	{
										echo 'Cunningham-Davis B 5-74 - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 27)	{
										echo 'Riley 2G - Production & Sales';
									} else if ($_GET['d'] == 229 && $_GET['geo'] == 16)	{
										echo 'RH Cummins 1 - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 18)	{
										echo 'Gaines County Sales - Production & Sales';
									} else if ($_GET['d'] == 223 && $_GET['geo'] == 19)	{
										echo 'Gaines County Check Meter - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 10)	{
										echo 'Dora Cunningham DO A1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 28)	{
										echo 'Bellinger 1 - Production & Sales';
									} else if ($_GET['d'] == 999  && $_GET['geo'] == 29)	{
										echo 'State 1 - Production & Sales';
									} else {
										echo $regions[$_GET['r']].' - Production & Sales';
									}
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
  					series: [{
                        name: 'Gas Prod/Sold',
                        data: <?php include('custom/time_series_gas_total.php'); ?>,
						color: '#DF5353' // red
                    },{
                        name: 'Oil Produced',
                        data: <?php include('custom/time_series.php'); ?>,
						color: '#55BF3B' // green
                    }, {
                        name: 'Oil Sold',
                        data: <?php include('custom/time_series_sales.php'); ?>,
						color: '#2d862d'
                    }, {
                        name: 'Water Production',
                        data: <?php include('custom/time_series_water.php'); ?>,
						color: '#003cb3'
                   }]
                });
				   
				<?php  } ?>  
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
   
    
      <?php  if ($_GET['m'] == 1) { ?>  

				<?php 
					echo '<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoWVbLzA7BMW2rkqwj5Ihz-qL_rUE8gKE&callback=initMap" async defer></script>';
				   include('includes/google_maps.php'); ?>
                
        <?php  } ?>  

		<?php include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

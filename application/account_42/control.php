<?php 
include('/var/www/html/application/includes/site.php');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

       <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">Keystone Hughes - Richland Resources Corporation</a>
       
                 
       </div>
        
      </div>
      
      <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right controlmenu">
           
           
           		<?php 
			  		
			  
			  		echo '<li><a href="dashboard.php?r=1">&laquo; Return to Dashboard</a></li>';
			  
			  		$devices = getDevicesByAccount(42);
			  					  
			  		foreach ($devices as $device) {
						
						echo '<li><a href="control.php?r='.$device['device_deviceID'].'">'.$device['device_name'].'</a></li>';
					}
			  		
			  		     echo '<li><a href="process/logout.php">Logout</a></li>';
			  	?>
		  </ul>
			  </div>
      
    </nav>

    <div class="container-fluid">
     
       <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           	
           		<?php 
			  		
			  
			  		echo '<li><a href="dashboard.php?r=1">&laquo; Return to Dashboard</a></li>';
			  
			  		$devices = getDevicesByAccount(42);
			  					  
			  		foreach ($devices as $device) {
						
						echo '<li><a href="control.php?r='.$device['device_deviceID'].'">'.$device['device_name'].'</a></li>';
					}
			  		
			  		     echo '<li><a href="process/logout.php">Logout</a></li>';
			  	?>
         

			<ul>
        </div>
        
        	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<h1> Control Registers</h1>
        	
        	    <?php 
			  
						if ($_GET['r'] && !isset($_GET['edit'])) {
							
							$device = getDeviceData($_GET['r'],42);
							
							echo '<div class="alert alert-info" role="alert"><strong>'.$device['device_name'].'</strong></div>';
						
			  
			  
			  	?>
      	
      					<form method="post" action="process/update_register.php" style="margin-bottom: 200px;">
      					<input type="hidden" name="register_deviceID" value="<?php echo $_GET['r'];	?>">
      					
      					<div class="table-responsive">
       	
       					<table class="table table-striped">
						  <thead>
							<tr>
						  	 <th>Category</th>
							  <th>Register #</th>
							  <th>QLabel</th>
						
<!--							  <th>Custom Label</th>
							  <th>Units</th>-->
							  <th>Past</th>
							  <th>Current</th>
							  <th>Edit</th>

							</tr>
						  </thead>
						  <tbody>
						  
						  <?php 
			  					
									$controls = getControlRegisters($_GET['r'],42);
									
														
									foreach ($controls as $control) {
										
										////<td>'.getRegCatSelect($control['categoryID'],$control['register_number']).'</td>
										
										echo '<tr>
													<td>'.$control['register_number'].'</td>
												
												  <td>'.$control['register_name'].'</td>
												  
													<td>'.getLastReading($_GET['r'],$control['register_number'],42).'</td>
													 <td>'.getLastReading($_GET['r'],$control['register_number'],42).'</td>
													
													
										  			<td>'.getWriteRegLink($_GET['r'],$control['register_number'],42).'</td>

												  </tr>';
										
									}
					
									
			  	  /*  <td><input type="text" class="form-control" name="reg_label['.$control['register_number'].']" style="width:150px;" value="'.$control['reg_label'].'"></td>
													<td><input type="text" class="form-control" name="reg_units['.$control['register_number'].']" style="width:150px;" value="'.$control['reg_units'].'"></td> <td><input type="text" class="form-control" name="reg_value" style="width:75px;"></td>
												  */
			  
			  				?>
        	
        				</tbody>
            		</table>
            		
							</div>
						
       					 <button type="submit" class="btn btn-default">Submit</button>
       				
        				</form>
        	
        	        <?php } ?>
        	        
        	           <?php 
			  
						if (isset($_GET['edit'])) {
							
							$device = getDeviceData($_GET['r'],42);
							$register = getRegisterData($_GET['r'],$_GET['n'],42);
							$pocconfig = getPOCConfigData($_GET['r'],$_GET['rd'],42);
							
							echo '<div class="alert alert-info" role="alert"><strong>'.$device['device_name'].'</strong></div>';
						
			  				echo '<h4>Tag: '.$register[0]['register_name'].'</h4>';
							echo '<h4>Description: '.$pocconfig[0]['tag_description'].'</h4>';
							echo '<h4>Current: '.getLastReading($_GET['r'],$_GET['rd'],42).'</h4>';
							
							echo '<form method="post" action="process/set_register.php" style="margin-bottom: 20px;">
      								<input type="hidden" name="deviceid" value="'.$_GET['r'].'">
									<input type="hidden" name="reg" value="'.$_GET['n'].'">
									
										<div class="form-group">

											<label for="log_book_field">Change Setting</label>
											<input type="text" class="form-control" name="value" placeholder="Enter Value" style="width:100px;"> 

										  </div>
									
										<button type="submit" class="btn btn-default">Submit</button>
       				
        								</form>';
			  
			  		?>
        	        
        	        
        	        <table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							 <!-- <th>GMT Date</th>-->
							  <th>CDT Date (GMT -5)</th>
							  <th>Reading</th>
							</tr>
							</thead>
							
							<tbody>
							
						<?php $readings = getRegisterReadings($_GET['r'],$_GET['rd'],42); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td>'.$reading['register_number'].'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  
						  </tbody>
								</table>
        	        
        	        
        	        <?php } ?>
        	
        	
		   </div>
      
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   


	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
  	
 	
 
	<?php include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

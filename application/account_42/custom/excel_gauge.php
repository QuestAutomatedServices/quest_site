<?php
include('/var/www/html/application/includes/site.php');


$database = "account_".$_SESSION['user_accountID']."";
$db = connectTWO($database);

if ($_GET['start']) {
	$dates = date_range(formatDateMYSQL($_GET['start']),formatDateMYSQL($_GET['end']));
} else {
	$dates = date_range(date('Y-m-01'),date('Y-m-t'));
}

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */


/** PHPExcel */
require_once '/var/www/html/quest/phpExcel/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

/*Day 	Top Level 	Oil Stock (BBLS) 	Oil Hauls (BBLS) 	BOPD (BBLS) 	Top Level 	Oil Stock (BBLS) 	Oil Hauls (BBLS) 	BOPD (BBLS) 	Top Level 	Stock (BBLS) 	Hauls (BBLS) 	WPD (BBLS) 	Comments*/

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setWidth(85);

if ($_GET['bt'] == 1) {

	$batterylabel = 'JD Slator #1 Tanks';

} else if ($_GET['bt'] == 2) {

	$batterylabel = 'PJ Lea A&B Tanks';

} else if ($_GET['bt'] == 3) {

	$batterylabel = 'PJ Lea C&D Tanks';

}


$styleArray1 = array(
    'font' => array(
        'bold' => true,
	 	'size' => '14',
    ),
	 'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'fcf8e3')
        )
);

$objPHPExcel->getActiveSheet()->getStyle('B1')->applyFromArray($styleArray1);
$objPHPExcel->getActiveSheet()->mergeCells('B1:E1');
$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Oil Tank 1');

$styleArray2 = array(
    'font' => array(
        'bold' => true,
	 	'size' => '14',
    ),
	 'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'd9edf7')
        )
);

$objPHPExcel->getActiveSheet()->getStyle('F1')->applyFromArray($styleArray2);
$objPHPExcel->getActiveSheet()->mergeCells('F1:I1');
$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Oil Tank 2');

$styleArray3 = array(
    'font' => array(
        'bold' => true,
	 	'size' => '14',
    ),
	 'fill' => array(
            'type' => PHPExcel_Style_Fill::FILL_SOLID,
            'color' => array('rgb' => 'f2dede')
        )
);

$objPHPExcel->getActiveSheet()->getStyle('J1')->applyFromArray($styleArray3);
$objPHPExcel->getActiveSheet()->mergeCells('J1:M1');
$objPHPExcel->getActiveSheet()->setCellValue('J1', 'Water Tank');


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A2', 'Day')
            ->setCellValue('B2', 'Top Level')
            ->setCellValue('C2', 'Oil Stock (BBLS)')
            ->setCellValue('D2', 'Oil Hauls (BBLS)')
            ->setCellValue('E2', 'BOPD (BBLS)')
            ->setCellValue('F2', 'Top Level')
            ->setCellValue('G2', 'Oil Stock (BBLS)')
            ->setCellValue('H2', 'Oil Hauls (BBLS)')
            ->setCellValue('I2', 'BOPD (BBLS)')
            ->setCellValue('J2', 'Top Level')
            ->setCellValue('K2', 'Stock (BBLS)')
            ->setCellValue('L2', 'Hauls (BBLS)')
            ->setCellValue('M2', 'WPD (BBLS)')
            ->setCellValue('N2', 'Comments');

	$line = 3;



	foreach ($dates as $date) { 
										 
			
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$line, formatDate($date))
				->setCellValue('B'.$line, convert16((getRegReadingByDate($_GET['d'],$_GET['t1'],$date,15.6,42))))
				->setCellValue('C'.$line, convertBBLS((getRegReadingByDate($_GET['d'],$_GET['t1'],$date,15.6,42))))
				->setCellValue('D'.$line, getTotalOilSalesByDate($_GET['d'],$_GET['t1'],$date,'',42))
				->setCellValue('E'.$line, getTotalOilProducedByDate($_GET['d'],$_GET['t1'],$date,'',42))
				->setCellValue('F'.$line, convert16((getRegReadingByDate($_GET['d'],$_GET['t2'],$date,15.6,42))))
				->setCellValue('G'.$line, convertBBLS((getRegReadingByDate($_GET['d'],$_GET['t2'],$date,15.6,42))))
				->setCellValue('H'.$line, getTotalOilSalesByDate($_GET['d'],$_GET['t2'],$date,'',42))
				->setCellValue('I'.$line, getTotalOilProducedByDate($_GET['d'],$_GET['t2'],$date,'',42))
				->setCellValue('J'.$line, convert16((getRegReadingByDate($_GET['d'],$_GET['t3'],$date,15.6,42))))
				->setCellValue('K'.$line, convertBBLS((getRegReadingByDate($_GET['d'],$_GET['t3'],$date,15.6,42))))
				->setCellValue('L'.$line, getTotalWaterTruckedByDate($_GET['d'],$_GET['t3'],$date,42))
				->setCellValue('M'.$line, getTotalWaterProducedByDate($_GET['d'],$_GET['t3'],$date,42))
				->setCellValue('N'.$line, getTankDiaryTextFull($_GET['d'],$_GET['bt'],$date,42));
				
			$line++; 

	}
			

$objPHPExcel->getActiveSheet()->setTitle($batterylabel);

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="gauge_sheet_export.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
ob_clean();
$objWriter->save('php://output');
exit;
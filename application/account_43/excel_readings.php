<?php
include('/var/www/html/application/includes/site.php');


$database = "account_".$_SESSION['user_accountID']."";
$db = connectTWO($database);

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */


/** PHPExcel */
require_once '../../quest/phpExcel/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(75);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(15);


// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Tag Name')
            ->setCellValue('B1', 'Reading Date (GMT)')
            ->setCellValue('C1', 'Reading');


	$line = 2;


	if ($_GET['start']) {
		
		
		$stmt = $db->prepare("SELECT * FROM tbl_opcua_mapping as m, tbl_register_history as h WHERE m.opcua_convention=h.opcua_convention AND m.mapID = ".$_GET['g']." AND date(register_date) >= '".formatDateMYSQL($_GET['start'])."' AND date(register_date) <= '".formatDateMYSQL($_GET['end'])."' ORDER BY h.register_date DESC ");


	} else {

		$stmt = $db->prepare("SELECT * FROM tbl_opcua_mapping as m, tbl_register_history as h WHERE m.opcua_convention=h.opcua_convention AND m.mapID = ".$_GET['g']." AND date(h.register_date) >= '".date('Y-m-d', strtotime('-2 days'))."'  ORDER BY h.register_date DESC ");
	}

	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	foreach ($results as $row) {
		
		
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$line, $row['tag_plc_name'])
				->setCellValue('B'.$line, formatTimestamp($row['register_date']))
				->setCellValue('C'.$line, $row['register_reading']);

			$line++; 

	}
			

$objPHPExcel->getActiveSheet()->setTitle('Readings Export');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="readings_export.xls"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
ob_clean();
$objWriter->save('php://output');
exit;
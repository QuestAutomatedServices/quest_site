<?php 
include('/var/www/html/application/includes/site.php');
$regions = array('Texas');
$devarray = array(1=>'CLS INJ',2=>'WALBAT',3=>'14SE19 BATTERY',4=>'SEC 19 TEST FACILITY',5=>'SEC 36 TEST FACILITY',6=>'REMOTE WELLS');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

       <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
     
      <div class="container">
       
        <div class="navbar-header">
         
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#"> Champion Lonestar</a>
       

        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
           
            <li <?php if (!isset($_GET['r']) && !isset($_GET['f'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            
				<li <?php if ($_GET['r'] ==  ($key+1) && !isset($_GET['d']) && !isset($_GET['tb'])) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
             
 		<?php if (isset($_GET['r'])) { ?>
         
            <ul class="nav navbar-nav navbar-right">



					<?php 
						foreach ($devarray as $d=>$device) {

								if ($_GET['d'] == $d) {
									echo '<li class="active"><a href="dashboard.php?r=1&d='.$d.'&s=6">'.$device.'</a></li>';
								} else {
									echo '<li><a href="dashboard.php?r=1&d='.$d.'&s=6">'.$device.'</a></li>';
								}

						}
				?>
            	
          </ul>
         <?php } ?>


		 
		   <ul class="nav navbar-nav navbar-right">
		    <li><a href="../alarms_champion/alarms.php">Alarms</a></li>
			 <?php 						  
					if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
						echo '<li><a href="dashboard.php?f=getAccountUsers">Users</a></li>';
					}
					echo '<li><a href="dashboard.php?f=getCurrentUser">Account</a></li>';
				?>
				
			<li><a href="../support/support.php">Support</a></li>
		
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           
            <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            

				<li <?php if ($_GET['r'] ==  ($key+1)) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
          
          
         <?php if (isset($_GET['r'])) { ?>
          <ul class="nav nav-list">
            <li class="nav-header">Sites</li>
            
            <?php 
				
					foreach ($devarray as $d=>$device) {
						
							if ($_GET['d'] == $d) {
								echo '<li class="active"><a href="dashboard.php?r=1&d='.$d.'&s=6">'.$device.'</a></li>';
							} else {
								echo '<li><a href="dashboard.php?r=1&d='.$d.'&s=6">'.$device.'</a></li>';
							}
						
					}
	
	
				//echo '<ul><li><a href="dashboard.php?r=1&d=239&c=1&g=51">Water Tank #1</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=57">Water Tank #2</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=64">Oil Tank #8393</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=70">Oil Tank #8394</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=76">Oil Tank #8395</a></li><li><a href="dashboard.php?r=1&d=239&c=1&g=82">Oil Tank #8396</a></li></ul>';
	
				
			?>
            	
          </ul>
         <?php } ?>
         
          
         <ul class="nav nav-sidebar">
               <li><a href="../alarms_champion/alarms.php">Alarms</a></li>
                  
                 <?php 
						 if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
							  if ($_GET['f'] == 'getAccountUsersTwo'){
								  echo '<li class="active"><a href="dashboard.php?f=getAccountUsersTwo">Users</a></li>';
							  }else{
								  echo '<li><a href="dashboard.php?f=getAccountUsersTwo">Users</a></li>';
							  }
						}
			 
			 			if ($_GET['f'] == 'getCurrentUser'){
 								
								echo '<li class="active"><a href="dashboard.php?f=getCurrentUser">Account</a></li>';			
							  }else{
								   echo '<li><a href="dashboard.php?f=getCurrentUser">Account</a></li>';
						 }
			 
			 
			 
			 ?>
                   
			<li><a href="../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>
          </ul>
         
         
       
        
        </div>
        
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        	<?php if(!isset($_GET['f'])){
					
			?>
         
          <h1 class="page-header">
          
          
          <?php 
	
	
					if (!isset($_GET['r'])) { 
						echo 'Global Overview'; 
					} else if (!isset($_GET['d'])) { 
						echo $regions[$_GET['r']-1].' Overview'; 
					} else {
						echo $devarray[$_GET['d']].''; 
					}
			  
			  ?>
                    			
          	</h1>

			<?php ///////filemanager
			} else {
				echo $_GET['f']();
			}?>





        <?php  if (isset($_GET['d'])) { ?>  

	       	<div class="btn-group topbuttons">
                  
                 <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=6" class="btn btn-primary <?php if ($_GET['s'] == 6) echo 'active' ?>">Overview</a>
                 
                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Oil</a>
                  
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Water</a>
                   
<?php /*?>                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=3" class="btn btn-primary <?php if ($_GET['s'] == 3) echo 'active' ?>">Gas</a>
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=4" class="btn btn-primary <?php if ($_GET['s'] == 4) echo 'active' ?>">Pressures</a>

                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=5" class="btn btn-primary <?php if ($_GET['s'] == 5) echo 'active' ?>">VFD</a><?php */?>
                  
	                   <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=7" class="btn btn-primary <?php if ($_GET['s'] == 7) echo 'active' ?>">Daily Rate</a>
	                   
	                   <?php if ($_GET['d'] == 2) { ?>
	                 			<a href="http://166.166.14.22:8383" class="btn btn-primary" target="_blank">HMI</a>
	                 <?php } ?>
	
              </div>


	       	<div class="btn-group topbuttons">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Tag Manager</a>
                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&fm=1" class="btn btn-primary <?php if ($_GET['fm'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           		
            </div>
     
         	 <?php if (!isset($_GET['fm'])) { ?>
				 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
				<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				<span></span> <b class="caret"></b></div>
			  <?php  } ?>  
	
			 
		
 		
        <?php  } ?>  


	  <!--File Manager-->
		<?php  if ($_GET['fm'] == 1) { ?>  

			<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

		<?php  } ?> 
		<!--File Manager--> 
        
        
        
        
         <!--GLOBAL & REGION SECTION--> 
         <?php  if (!isset($_GET['d']) && !isset($_GET['c']) && !isset($_GET['f'])) { ?> 
         
             <div class="btn-group topbuttons">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

             </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
			    
             <div id="chart"></div>
             
             <div class="row">
            	
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Oil</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody> 
                               <?php 
	
										$totals = getTotalsArrayOPC(1,$_SESSION['user_accountID']); 
	
							   	   		foreach ($totals as $key=>$total) {  
												
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
                               
                               ?>
                              </tbody>
                            </table>

                </div>
            

                <div class="col-md-4">

                     <h2 class="sub-header">Water</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php  
							       
										$totals = getTotalsArrayOPC(2,$_SESSION['user_accountID']); 
	
							   	   		foreach ($totals as $key=>$total) {  
												
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
	                              
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Gas</h2> 
                     
                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
								<th>Production (MCF)</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php 
	
										$totals = getTotalsArrayOPC(2,$_SESSION['user_accountID']); 
	
							   	   		foreach ($totals as $key=>$total) {  
												
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>0.00</td>
												</tr>';
                                    
										}
							       
                               
                               ?>
                              </tbody>
                            </table>
                    
                
                </div>
            
            
            </div>
                  
         <?php  } ?> 
		  <!--GLOBAL & REGION SECTION--> 
       
       
       	<!--DEVICE SECTION--> 
         <?php  if (isset($_GET['d']) && !isset($_GET['c']) && !isset($_GET['f'])) { 
							  
							  
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);	  
							  
							  ?> 
        
        	<div id="chart"></div>
        	
        	
        	<?php  if ($_GET['s'] == 1) { ?> 
        	
        	<div class="row">
            	
                	<div class="col-md-12">
                	
                     <h2 class="sub-header">Oil</h2>
                     

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                 <th>Tag</th>
                                  <th>Production (BBLS)</th>
                                  <th>Sales (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody> 
                               <?php  
								  
								$results = getOPCTagsByDeviceConfigModbus($_GET['d'],'Oil - Production and Sales',43);

								 //print_r($results);
								//foreach($results as $tag){
									
								//$totals = getTotalsArrayOPC(1,$_SESSION['user_accountID']); 
								  
								  //echo "<pre>";
								 // print_r($totals);
	
							   	foreach ($regdates as $targetdate) {  
									
											echo '<tr>
													<th colspan="2">'.formatDate($targetdate).'</th>
												</tr>';
                                    
												
										foreach($results as $tag){		
											echo '<tr>
													<td>'.$tag['map_friendly_name'].'</td>
												  	<td>'.number_format(getTotalsOPCDateMap(1,$targetdate,$tag['mapID'],$_SESSION['user_accountID']),2).'</td>
												</tr>';
                                    
										}
									
								}
                               
                               ?>
                              </tbody>
                            </table>

               	 </div>
                
			</div>
            
        	
        	 
        	  <?php  } ?>  
        	  
        	  
        	  	<?php  if ($_GET['s'] == 2) { ?> 
        	
        	<div class="row">
            	
                	<div class="col-md-12">
                	
                     <h2 class="sub-header">Water</h2>
                     

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                 <th>Tag</th>
                                  <th>Production (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody> 
                               <?php  
								  
								$results = getOPCTagsByDeviceConfigModbus($_GET['d'],'Water - Tank Level Feet',43);

								 //print_r($results);
								//foreach($results as $tag){
									
								//$totals = getTotalsArrayOPC(1,$_SESSION['user_accountID']); 
								  
								  //echo "<pre>";
								 // print_r($totals);
	
							   	foreach ($regdates as $targetdate) {  
									
											echo '<tr>
													<th colspan="2">'.formatDate($targetdate).'</th>
												</tr>';
                                    
												
										foreach($results as $tag){		
											echo '<tr>
													<td>'.$tag['map_friendly_name'].'</td>
												  	<td>'.number_format(getTotalsOPCDateMap(2,$targetdate,$tag['mapID'],$_SESSION['user_accountID']),2).'</td>
												</tr>';
                                    
										}
									
								}
                               
                               ?>
                              </tbody>
                            </table>

               	 </div>
                
			</div>
            
        	
        	 
        	  <?php  } ?>  
        	
        	
        	<?php  if ($_GET['s'] == 6) { ?>  
            
            	<div class="row">
            	
                	<div class="col-md-3">
                	
                     <h2 class="sub-header">Oil</h2>
                     

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody> 
                               <?php 
	
										$totals = getTotalsArrayOPC(1,$_SESSION['user_accountID']); 
	
							   	   		foreach ($totals as $key=>$total) {  
												
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
                               
                               ?>
                              </tbody>
                            </table>

                </div>
            

                <div class="col-md-3">

                     <h2 class="sub-header">Water</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production (BBLS)</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php  
							       
										$totals = getTotalsArrayOPC(2,$_SESSION['user_accountID']); 
	
							   	   		foreach ($totals as $key=>$total) {  
												
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.number_format($total['totprod'],2).'</td>
												</tr>';
                                    
										}
	                              
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
                <div class="col-md-6">
                	
                     <h2 class="sub-header">Gas</h2> 
                     
                         <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Accumulated Volume (MCF)</th>
                                  <th>Spot Rate (MCFD)</th>
                                  <th>Previous Day (MCF)</th>
                                </tr> 
                              </thead>
                              <tbody>
                               
                               <?php  
							       
									$totals = getTotalsArrayOPC(2,$_SESSION['user_accountID']); 
	
							   	   		foreach ($totals as $key=>$total) {  
												
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>0.00</td><td>0.00</td><td>0.00</td>
												</tr>';
                                    
										}
                               
                               ?>
                              </tbody>
                            </table>
                    
                
                </div>
            
            
            </div>
             <?php  } ?> 
        
         <?php  } ?> 
       	<!--DEVICE SECTION--> 
       
       
        
        <!--CONFIG TAG SECTION--> 
         <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getOPCTagNameModbus($_GET['g'],$_SESSION['user_accountID']); ?> <a href="/application/account_43/excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getOPCTagNameModbus($_GET['g'],$_SESSION['user_accountID']); ?> <a href="/application/account_43/excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
						  	  <th>Modbus</th>
							  <th>Tag Name</th>
							  <th>Timestamp</th>
							  <th>Reading</th>
							</tr>
							
						<?php $readings = getOPCReadings($_GET['g'],$_SESSION['user_accountID']); 
									 
							//print_r($readings);

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td class="fullview">'.$reading['map_modbus_address'].'</td>
											<td class="fullview">'.$reading['map_tag_name'].'</td>
											<td class="fullview">'.formatTimestampOffset($reading['reading_date'],5).'</td>
											<td>'.$reading['reading_value'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  </thead>
						  <tbody>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Tag Manager</h2>
          	  	
          	 
            
						<table class="table table-striped">
						  <thead>
							<tr>
						  	  <th>Modbus</th>
							  <th>Tag Name</th>
							  <th>Friendly Name</th>
							  <th>Config</th>
							  <th>Units</th>
							  <th>Factor</th>
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getOPCTagsByDeviceModbus($_GET['d'],$_SESSION['user_accountID']); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {

								
								
								if ($reg['map_tag_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}

								echo '<tr '.$configset.'>
										<td id="'.$reg['mapID'].'">'.$reg['map_modbus_address'].'</td>
										<td id="'.$reg['mapID'].'">'.$reg['map_tag_name'].'</td>
										
										<td><input name="dash_name" class="reglabel" data-id="'.$reg['mapID'].'" size="25" type="text" value="'.$reg['map_friendly_name'].'"></td>
										
										<td>'.getOPCTagConfigSelect($reg['mapID'],$reg['map_tag_config']).'</td>
										<td>'.getUnitSelect($reg['mapID'],$reg['map_units']).'</td>
										
										<td><input name="map_measurement_factor" class="regfactor" data-id="'.$reg['mapID'].'" size="8" type="text" value="'.$reg['map_measurement_factor'].'"></td>
										
										<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['mapID'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  
          <!--CONFIG TAG SECTION--> 
        

         
          <!--CHART CHOOSE MODAL -->  
            
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="../application/account_43/custom/set_chart.php?t=1&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="../application/account_43/custom/set_chart.php?t=2&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="../application/account_43/custom/set_chart.php?t=3&r=<?php echo $_GET['r']; ?>" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>
               <!--CHART CHOOSE MODAL -->  
          
       
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   


	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    

    <?php  if ($_GET['fm']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if (!isset($_GET['fm'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
				  
						$( "#choosechart" ).click(function() {	
								$( "#chart_modal" ).dialog( "open" );	
						});


						$( "#chart_modal" ).dialog({
						  autoOpen: false,
						  height: 'auto',
						  width: 1050,
						  modal: true,

						});	
					   		
		
 					   		$( "#newlink" ).click(function() {	

								$( "#add_modal" ).dialog( "open" );	
							});

					
							$( "#add_modal" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	


				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   <?php } else if ($_GET['s'] == 7 ) { ?>
				   
				   	var start = moment().subtract(1, 'days');
					 var end = moment();
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(29, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s']  && !$_GET['tb']  ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   	<?php } else if ($_GET['tb']  ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&tb=<?php echo $_GET['tb']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	
 	
 	<?php  if (!$_GET['f'] && !$_GET['fm']) { ?>  
   
   	<?php  if ($_GET['s'] != 4 && $_GET['s'] != 7) { ?>  
   	
    	<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					   
					     chart: {
								type: '<?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { 
							 		
												echo 'line';
										
											} else if ($_SESSION['chart_type'] == 2 )  {
	
												echo 'waterfall';

											
											} else if ($_SESSION['chart_type'] == 3 )  {

												echo 'bar';
											}
							 	?>', 

							},
					   
                    title: {
                        text: '<?php 	if (!isset($_GET['r'])) { 
											echo 'Global Overview'; 
										} else if (!isset($_GET['d'])) { 
											echo $regions[$_GET['r']-1].' Overview'; 
										} else {
											echo $devarray[$_GET['d']].''; 
										}
		
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('../application/account_43/custom/time_series_production_43.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, <?php /*?>{
                        name: 'Oil Sales',
                        data: <?php include('process/time_series_sales_46.php'); ?>,
						color: '#267326',
						tooltip: {
							valueSuffix: ' BBL'
						 }
					},<?php */?> {
                        name: 'Water Production',
                        data: <?php include('../application/account_43/custom/time_series_water_43.php'); ?>,
					    color: '#4d79ff',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, <?php /*?>{
                        name: 'Water Disposed',
                        data: <?php include('process/time_series_water_inj_46.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, <?php */?>{
                        name: 'Gas (Prev Day)',
                        data: <?php include('../application/account_43/custom/time_series_gas_43.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' MCF'
						 }
                   }
							 
					<?php 	if ($_GET['d'] == 999 ) {	?>	 
							 , {
                        name: 'Tubing Pressure',
                        data: <?php //include('process/time_series_tubing_46.php'); ?>,
						color: '#ffa31a',
						tooltip: {
							valueSuffix: ' PSI'
						 }
                   }, {
                        name: 'Casing Pressure',
                        data: <?php //include('process/time_series_casing_46.php'); ?>,
						color: '#b35900',
						tooltip: {
							valueSuffix: ' PSI'
						}
                   }
							<?php  } ?> 
							
							]
                });
				   
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
		
		
		<?php  if ($_GET['s'] == 7) { ?> 
		
		<script>
               $(function () {
				   
				   
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x'
				},
					   
                    title: {
                        text: 'Daily Production Trending',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
/*                    tooltip: {
                        valueSuffix: 'bbl' 
                    },*/
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('../application/account_43/custom/time_series_production_all_43.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }		
					]
                });
				   
				   
				   
			   
			   });
			
		</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
		<script>
			
				/////////ajax script for tag manager
				$('.configsel').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	
					

					$.ajax({

						url: "../application/account_43/custom/update_config.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});
			
			
				$('.unitsel').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	
					

					$.ajax({

						url: "../application/account_43/custom/update_unit.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});
			
		
				$('.reglabel').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	

					$.ajax({

						url: "../application/account_43/custom/update_label.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});
			
			
					
				$('.regfactor').change(function(){

					config = $(this).val();
					reg = $(arguments[0].target).attr("data-id");	

					$.ajax({

						url: "../application/account_43/custom/update_factor.php",
						type: 'GET', 
						data: 'reg='+reg+'&config='+config,

					});

				});

		</script>
	
    
  </body>
</html>

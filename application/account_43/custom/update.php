<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");

if ($_POST['pt'] == 1) {
	
	if($_POST['user_password'] != ''){
		$_POST['user_password'] = password_hash($_POST['user_password'], PASSWORD_DEFAULT);
	}else{
		unset($_POST['user_password']);
	}
	
	$a = $_SESSION['user_accountID'];
	
	$table = 'tbl_users';
	$f = 'getCurrentUser';
	
	$key = 'userID';
	
	$redir = "user";
	
	
} else if ($_POST['pt'] == 2) { 
	
		$table = 'tbl_registers';
		$key = 'regID';
	
		updateSingleRecord($_POST['id'],$_POST['field'],$_POST['val'],$table,$key,42);
	
	
} else if ($_POST['pt'] == 3) { 
	
	$table = 'tbl_well_attributes';
	$key = 'attributeID';
	
	updateRecordAcct($_POST,$table,$key,42);
} 

if ($_POST['pt'] != 2) {
	unset($_POST['pt']);

	updateRecord($_POST,$table,$key);



	if ($redir == "admin") {
		header("location: ../index.php?f=$f");
	}else if($redir == "user"){
		if(isset($a)){
			header("location: ../dashboard.php?f=$f&a=$a");
		}else{
			header("location: ../dashboard.php?f=$f");
		}
	} else {
		header("location: ../dashboard.php?wa=1");
	}
}
?>
<?php 
include('/var/www/html/application/includes/site.php');
$regions = array('Texas');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#"><?php echo $_SESSION['user_name']; ?></a>
       
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
       <?php /*?>     <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global</a></li><?php */?>

           <?php foreach ($regions as $key=>$region) { ?>
            <li class="<?php if ($_GET['r'] == ($key+1)) echo 'active ' ?> dropdown">
              <a href="dashboard.php?r=1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $region; ?> <span class="caret"></span></a>
              <ul class="dropdown-menu">

                    <?php $sitedevices = getDevicesByRegion($_SESSION['account_id'],$region,44); 
                            foreach ($sitedevices as $device) {
                                echo '<li><a href="dashboard.php?r='.($key+1).'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
                            }
                    ?>

              </ul>
            </li>
 		 <?php }
			  
			  ?>

            
   
			<li><a href="../../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           <?php /*?> <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li><?php */?>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            

				<li <?php if ($_GET['r'] ==  ($key+1)) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
          
          
         <?php if (isset($_GET['r'])) { ?>
          <ul class="nav nav-list">
            <li class="nav-header">Sites</li>
            
            <?php $sitedevices = getDevicesByRegion($_SESSION['user_accountID'],$regions[$_GET['r']-1],44); 
					

					foreach ($sitedevices as $device) {
						
						if ($_GET['d'] == $device['device_deviceID']) {
							echo '<li class="leftnavactive"><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">&raquo; '.$device['device_name'].'</a></li>';
						} else {
							echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
						}
					}
			?>
            	
          </ul>
         <?php } ?>
         
          
         <ul class="nav nav-sidebar">
                  
               <?php 
			 			  if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
							  if ($_GET['f'] == 'getAccountUsersTwo'){
								  echo '<li class="active"><a href="dashboard.php?f=getAccountUsersTwo&a=44">Users</a></li>';
							  }else{
								  echo '<li><a href="dashboard.php?f=getAccountUsersTwo&a=44">Users</a></li>';
							  }
						}
			 
			 				if ($_GET['f'] == 'getCurrentUser'){
 								
								echo '<li class="active"><a href="dashboard.php?f=getCurrentUser&a=44">Account</a></li>';			
							  }else{
								   echo '<li><a href="dashboard.php?f=getCurrentUser&a=44">Account</a></li>';
							  }
			 
			
			 
			 
			 ?>
                   
			<li><a href="../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         
         <?php if(!isset($_GET['a'])) {?>
          <h1 class="page-header"><?php if (!isset($_GET['r'])) { echo 'Global Overview'; } else if (!isset($_GET['d'])) { echo $regions[$_GET['r']-1].' Overview'; } ?><?php if (isset($_GET['d'])) echo getDeviceName($_GET['d'],44);  }?></h1>

		<?php 
			if($_GET['f'] == 'getAccountUsersTwo' or $_GET['f'] == 'getCurrentUser'){
				echo $_GET['f']();
			}
			
		?>



        <?php  if (!isset($_GET['d']) && !isset($_GET['a'])) { 
			
			
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);
			
			?>  
        
        
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
		
        


           
             <div id="chart"></div>


			
            <div class="row">
            	
                <div class="col-md-12">
                	
                     <h2 class="sub-header">Oil Tank Levels</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
							   		for ($i=225;$i<=229;$i++) {  

											echo '<tr>
													  <td>'.getRegisterName(234,$i,44).'</td>
													<td>'.getLastReading(234,$i,44,'','',1).'</td>

													</tr>';

											}
                               
                               ?>
                              
                              </tbody>
                            </table>

                </div>
            

           
                
                
            
            </div>


       		


        <?php  } ?>  


        <?php  if (isset($_GET['d'])) { ?>  

	       	<div class="btn-group">
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Oil</a>
                  
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Water</a>
                  
				<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=3" class="btn btn-primary <?php if ($_GET['s'] == 3) echo 'active' ?>">Gas</a>
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=4" class="btn btn-primary <?php if ($_GET['s'] == 4) echo 'active' ?>">Pressures</a>
                  
                 <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=5" class="btn btn-primary <?php if ($_GET['s'] == 5) echo 'active' ?>">H-Pump</a>


              </div>


	       	<div class="btn-group">
                  
                  <!--<a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>-->
			
      			<a href="http://166.130.171.32:81/remote.htm?session=null&info=/assets/json/full-x1.json" class="btn btn-primary" target="_blank">HMI</a>
       
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>
            
<?php /*?>                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&b=1" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>">Digital Log Book</a><?php */?>
                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           		<?php /*?>	<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&m=1" class="btn btn-primary <?php if ($_GET['m'] == 1) echo 'active' ?>">Map</a><?php */?>
           		
            </div>
            
          <?php  if ( $_GET['s'] or $_GET['g'] ) { ?>  
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
			  <?php  } ?>  
			 
		
 		
        <?php  } ?>  


        <?php  if ($_GET['m'] == 1) { ?>  

          	<h2 class="sub-header">Location & Status</h2>

			<div class="alert-success">Device Properly Working</div>
			<div id="map"></div>

        <?php  } ?>  

        <?php  if ($_GET['f'] == 1) { ?>  
          	


          	<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

        <?php  } ?>  
        
        

        <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],44); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],44); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>GMT Date</th>
							  <th>TZ Date</th>
							  <th>Reading</th>
							</tr>
							
						<?php $readings = getRegisterReadings($_GET['d'],$_GET['g'],44); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td>'.$reading['register_number'].'</td>
											<td>'.formatTimestamp($reading['register_date']).'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  </thead>
						  <tbody>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Reporting Configuration</h2>
          	  	
          	  	<p>Reporting Time Range <input name="config_start_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_start_time",24); ?>"> to <input name="config_end_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_end_time",24); ?>"> </p>
            
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>Label</th>
							  <th>Config</th>
							  <th>Measurement Factor</th>
<!--							   <th>Alarm</th>
							  <th>Produce/Sell</th>-->
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getRegistersByDevice($_GET['d'],44); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {
								
								$noprodsell = '';
								$prodsell = '';
								
								if ($reg['register_prod_sell'] == 1) {
									$noprodsell = ' checked="checked" ';
								}
								
								if ($reg['register_prod_sell'] == 2) {
									$prodsell = ' checked="checked" ';
								}
								
								if ($reg['register_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}
								
								if ($reg['register_label'] == '') {
									
									$reg['register_label'] = $reg['register_name'];
								}
								
								
										//<td>&nbsp;</td><td><input name="produce_sell_'.$reg['register_number'].'" class="regprodsell" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  type="radio" value="1" '.$noprodsell.' > No <input name="produce_sell_'.$reg['register_number'].'" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  class="regprodsell" type="radio" value="2"  '.$prodsell.'> Yes</td>

								echo '<tr '.$configset.'>
										<td id="'.$reg['register_number'].'">'.$reg['register_number'].'</td>
										<td><input name="register_label" class="reglabel" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="25" type="text" value="'.$reg['register_label'].'"> <i class="fa fa-info-circle fa-lg" aria-hidden="true" title="#'.$reg['register_number'].'<br>'.$reg['register_label'].'<br>'.$reg['register_name'].'"></i></td>
										<td>'.getRegistersConfigSelect($_GET['d'],$reg['register_number'],$reg['register_config']).'</td>
										<td><input name="measurement_factor" class="regfactor" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="8" type="text" value="'.$reg['register_factor'].'"></td>
										<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['register_number'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  



        <?php  if ($_GET['s'] == 1) { 
							  
				//////////oil					
				//$totals = getTotalsArray($_GET['d'],24);
				//$totalsreg = getTotalsByRegister($_GET['d'],24);
	
				//echo "<pre>";
				//print_r($totalsreg);

							  
		 ?>  
        
        <div id="chart"></div>

    <div class="col-md-12">
                	
                     <h2 class="sub-header">Oil Tank Levels</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
							   		for ($i=225;$i<=229;$i++) {  

											echo '<tr>
													  <td>'.getRegisterName(234,$i,44).'</td>
													<td>'.getLastReading(234,$i,44,'','',1).'</td>

													</tr>';

											}
                               
                               ?>
                              
                              </tbody>
                            </table>

                </div>

        <?php /*?>  <h2 class="sub-header">Sales</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Barrels</th>
                </tr>
              </thead>
              <tbody>
               <?php 
						foreach ($totals as $key=>$total) {  
			   			echo '<tr>
							  <td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#sales'.$key.'" aria-hidden="true" title="Show Detail"></i> '.getDeviceName($_GET['d']).'</td>	
							  <td>'.formatDate($total['calculation_date']).'</td>
							  <td>'.$total['totsales'].'</td>
							</tr>';
							
							
							echo '<tr id="sales'.$key.'" class="collapse">
								<td colspan="3">
								           <table class="table table-striped">
										  <thead>
											<tr>
											  <th>Register</th>
											  <th>Date</th>
											  <th>Barrels</th>
											</tr>
										  </thead>
										  <tbody>';
						
											foreach ($totalsreg as $regdata) { 
												
												if ($regdata['calculation_date'] == $total['calculation_date']) {
													
													echo '<tr>
													  <td>'.$regdata['register_number'].'</td>				
													  <td>'.formatDate($regdata['calculation_date']).'</td>
													  <td>'.$regdata['sales_calculation'].'</td>
													</tr>';
												}
													

										  	
											}
												
						echo				 '</tbody>
										 </table>
								
								</td>
							</tr>';
						}
			   
			   ?>
              </tbody>
            </table>
          </div><?php */?>
          
          
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>


        <?php  } ?>  

        <?php  if ($_GET['s'] == 2) { 
							 
			  ?>  
       
       		  <div id="chart"></div>
        
             <div class="col-md-12">
               	
               	          <h2 class="sub-header">Water Tank Levels</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
							   		for ($i=230;$i<=234;$i++) {  

											echo '<tr>
													  <td>'.getRegisterName(234,$i,44).'</td>
													<td>'.getLastReading(234,$i,44,'','',1).'</td>

													</tr>';

											}
                               
                               ?>
                              
                              </tbody>
                            </table>
               	
                	 
<!--                      <h2 class="sub-header">Flow Transmitters </h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
							   		//for ($i=326;$i<=374;$i+=3) {  

											//echo '<tr>
												///	  <td>'.getRegisterName(234,$i,44).'</td>
												//	<td>'.getLastReading(234,$i,44,'','',1).'</td>

													//</tr>';

											//}
                               
                               ?>
                              
                              </tbody>
                            </table>-->

                </div>
          


        <?php  } ?>  



        <?php  if ($_GET['s'] == 3) { ?>  
  					
              	<div id="chart"></div>
               	
               	<div class="col-md-12">
                	
                      <h2 class="sub-header">Flow Meters</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
							   		for ($i=302;$i<=314;$i+=4) {  

											echo '<tr>
													  <td>'.getRegisterName(234,$i,44).'</td>
													<td>'.getLastReading(234,$i,44,'','',1).'</td>

													</tr>';

											}
                               
                               ?>
                              
                              </tbody>
                            </table>

                </div>
          
        <?php  } ?>  



        <?php  if ($_GET['s'] == 4) { 
							  
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
				}
				
				
	
				rsort($regdates);
							  
				//$pressures = getLastReadingsArrayPressure($_GET['d'],24); 
				//echo "<pre>";
				//print_r($regdates);
							  
		 ?>  
		
		

	
			

         
   				<div class="col-md-12">
                	
                      <h2 class="sub-header">Vessel Pressure Transmitters</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
							   		for ($i=450;$i<=461;$i++) {  

											echo '<tr>
													  <td>'.getRegisterName(234,$i,44).'</td>
													<td>'.getLastReading(234,$i,44,'','',1).'</td>

													</tr>';

											}
                               
                               ?>
                              
                              </tbody>
                            </table>

                </div>
                
                
                
          
          
							  <div id="settings_modal" title="Pressure Gauge Settings" style="display: none;">
							  	
							  	
							  	          <div class="table-responsive">
											<table class="table table-striped" style="width:600px;">

											  <tbody>
												  
												<tr>
												<td style="background-color:#55BF3B; ">Good</td>
												<td><input type="text" class="form-control modalfield" name="gauge_start_good" data-id="<?php echo $_GET['d']; ?>" placeholder="Start Value" value="<?php echo $gauge['gauge_start_good']; ?>"></td>
												<td><input type="text" class="form-control modalfield" name="gauge_end_good" data-id="<?php echo $_GET['d']; ?>" placeholder="End Value" value="<?php echo $gauge['gauge_end_good']; ?>"></td>
												</tr>

												<tr>
												<td style="background-color:#DDDF0D; ">Warning</td>
												<td><input type="text" class="form-control modalfield" name="gauge_start_warn" data-id="<?php echo $_GET['d']; ?>" placeholder="Start Value" value="<?php echo $gauge['gauge_start_warn']; ?>"></td>
												<td><input type="text" class="form-control modalfield" name="gauge_end_warn" data-id="<?php echo $_GET['d']; ?>" placeholder="End Value" value="<?php echo $gauge['gauge_end_warn']; ?>"></td>
												</tr>

												<tr>
												<td style="background-color:#DF5353; ">Bad</td>
												<td><input type="text" class="form-control modalfield" name="gauge_start_bad" data-id="<?php echo $_GET['d']; ?>" placeholder="Start Value" value="<?php echo $gauge['gauge_start_bad']; ?>"></td>
												<td><input type="text" class="form-control modalfield" name="gauge_end_bad" data-id="<?php echo $_GET['d']; ?>" placeholder="End Value" value="<?php echo $gauge['gauge_end_bad']; ?>"></td>
												</tr>

											  </tbody>
											</table>
										  </div>
							  								  	
							  </div>
							  
							  
							   <div id="chart_modal" title="Chart Types" style="display: none;">
							   
							   
							   </div>

        <?php  } ?>  
        
           <?php  if ($_GET['s'] == 5) { 
							 
			  ?>  
       
       		 <div id="chart"></div>
        
             <div class="col-md-12">
               	
               	          <h2 class="sub-header">Flow Transmitters</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Current Rate</th>
                                  <th>Todays Total</th>
                                  <th>Yesterdays Total</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
	
									$r = 1;
							       
							   		for ($i=325;$i<=342;$i++) {  
										
											if ($r == 1) {
												
												echo '<tr><td>'.str_replace("Current Rate","",getRegisterName(234,$i,44)).'</td>';
											}

											echo '<td>'.getLastReading(234,$i,44,'','',1).'</td>';

											if ($r == 3) {
												
												echo '</tr>';
												$r = 1;
											} else {
												$r++;
											}
										
										
									}
                               
                               ?>
                              
                              </tbody>
                            </table>
               	
                	 
                     <h2 class="sub-header">Pressure Transmitters </h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Suction Pressure</th>
                                  <th>Discharge Pressure</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
	
									$r = 1;
							       
							   		for ($i=450;$i<=461;$i++) {  
										
											if ($r == 1) {
												
												echo '<tr><td>'.str_replace("C","",getRegisterName(234,$i,44)).'</td>';
											}

											echo '<td>'.getLastReading(234,$i,44,'','',1).'</td>';

											if ($r == 2) {
												
												echo '</tr>';
												$r = 1;
											} else {
												$r++;
											}
										
										
									}
                               
                               ?>
                              
                              </tbody>
                            </table>
                            
       
                	
                      <h2 class="sub-header">Vibration Sensors</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Tank</th>
                                  <th>Current Level</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       $vsarray = array(115,116,120);
							   		foreach ($vsarray as $key=>$i) {

											echo '<tr>
													  <td>'.getRegisterName(234,$i,44).'</td>
													<td>'.getLastReading(234,$i,44,'','',1).'</td>

													</tr>';

											}
                               
                               ?>
                              
                              </tbody>
                            </table>


                </div>
          


        <?php  } ?>  
        
        
        
        
         <?php  if ($_GET['b'] == 1) { 
							  
							  
		 ?>  
		
		
			<h2 class="sub-header">Digital Log Book [<a href="#" id="entryview">entry view</a>]</h2>
			
			<p><strong>Add New Field </strong> - Please choose a type: </p>
			
			 
			
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="1"> This field will compare entered log book values to auto calculated values
				  </label>
				</div>
				
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="2"> This field will be a calculation of up to 3 other fields
				  </label>
				</div>
				
			  <div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="3"> This field is for entry only
				  </label>
				</div>
				

					
				<div id="log_field1" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Comparison">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
						<?php echo  getLogCompareField(); ?> 
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				
				<div id="log_field2" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Calculation">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Calculated Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Calculated Field Name"> = 
						
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name"> 
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
									
						
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				<div id="log_field3" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Entry Only">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	
		
				
				<div id="entry_modal" title="Log Book Entry" style="display: none;">
						
					<h4>Enter Reading</h4>
					
					   <div class="table-responsive">
						<table class="table table-striped" style="width:600px;">

						  <tbody>

						  <tr>
							  <td>Date</td>
							  <td><input type="text" class="form-control datepicker" name="reading_date" placeholder="Enter Date"></td>
						</tr>
						
						<tr>
							  <td>Time</td>
							  <td><input type="time" class="form-control" name="reading_time" placeholder="Enter Time"></td>
						</tr>
						
						
						 <?php 

								$fields = getLogBookFieldsByDevice($_GET['d'],24);

								//print_r($fields);

							foreach ($fields as $key=>$field) {  

							echo '<tr>
									  <td>'.$field['log_book_field'].'</td>
									  <td><input type="text" class="form-control" name="'.$field['logfieldID'].'" placeholder="Enter Value"></td>
								</tr>';

							}
					   ?>

						              </tbody>
            </table>
          </div>
						<p>Comments</p>

				</div>


        
         
          <div class="table-responsive" style="margin-top: 30px;">
           
            <p><strong>Current Fields </strong></p>
           
            <table class="table table-striped" style="width:700px;">

              <tbody>
              
<!--              <tr>
				  <td>Date</td>
				  <td><input type="text" class="form-control datepicker" name="'.$field['logfieldID'].'" placeholder="Enter Date"></td>
			</tr>-->
               
               <?php 
					
						$fields = getLogBookFieldsByDevice($_GET['d'],24);
						
						//print_r($fields);

					foreach ($fields as $key=>$field) {  
						
						if ($field['log_field_type'] == 'Comparison') {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>Compare to: '.$field['log_book_comparison'].'</td>
						   </tr>';
	
						} else if ($field['log_field_type'] == 'Calculation') {
							
							$calcfields = unserialize($field['log_book_calcs']);
							$calcops = unserialize($field['log_book_math']);
							$calcstring = '';
							
							foreach ($calcfields as $key=>$calc) {
								
								$calcstring .= $calc.' '.$calcops[$key].' ';
							}
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>'.$calcstring.'</td>
						   </tr>';

					
						} else  {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>&nbsp;</td>
						   </tr>';
														
						}
						
			   		 
					
					}
			   ?>
              </tbody>
            </table>
          </div>
          
          
          
          	   <div id="log_modal" title="Edit Log Book Field" style="display: none;">
          	   
          	   	<form method="post" action="process/edit_log_book_field.php"  class="form-inline">
          	   	<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
					<input type="hidden" name="logfieldID" id="logfieldID_edit">
						  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" id="log_book_field_edit" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit</button>
					</form>
							   
							   
			</div>

        <?php  } ?>  




        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="/application/libraries/jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="/application/libraries/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    

    <?php  if ($_GET['f']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if ($_GET['s'] or $_GET['g'] or !isset($_GET['d'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(2, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	<script>
		
					   		
		
 					   		$( "#newlink" ).click(function() {	

								$( "#add_modal" ).dialog( "open" );	
							});

							$( "#newlinkEXTRA" ).click(function() {	

								$( "#add_modalEXTRA" ).dialog( "open" );	
							});

							$( "#newlink2" ).click(function() {	

								$( "#add_modalWIDE" ).dialog( "open" );	
							});


							$( "#add_modal" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalEXTRA" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalWIDE" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 950,
							  modal: true,

							});
			
		</script>
 	
 	
 	<?php  if (!$_GET['m'] && !$_GET['c'] && !$_GET['b'] && !$_GET['f'] ) { ?>  
   
   	<?php  if ($_GET['s'] != 4) { ?>  
   	
    	<script>
               $(function () {
				   	
				   
				   <?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { ?>  
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x'
				},
					   
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
						
                    },
                    yAxis: {
                        title: {
                            text: 'Pressure (psi)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: 'psi'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					   
					<?php  if (!isset($_GET['s']) or $_GET['s'] == 1) { ?>  
					   
                    series: [{
                        name: 'TK210 Level',
                        data: <?php include('custom/time_series_levels.php'); ?>
                    },{
                        name: 'TK211 Level',
                        data: <?php include('custom/time_series_levels2.php'); ?>
                    },{
                        name: 'TK212 Level',
                        data: <?php include('custom/time_series_levels3.php'); ?>
                    },{
                        name: 'TK213 Level',
                        data: <?php include('custom/time_series_levels4.php'); ?>
                    },{
                        name: 'TK214 Level',
                        data: <?php include('custom/time_series_levels5.php'); ?>
                    }]
						
					<?php  } else if ($_GET['s'] == 2) { ?>  
						
					series: [{
                        name: 'TK220 Level',
                        data: <?php include('custom/time_series_levels6.php'); ?>
                    },{
                        name: 'TK221 Level',
                        data: <?php include('custom/time_series_levels7.php'); ?>
                    },{
                        name: 'TK222 Level',
                        data: <?php include('custom/time_series_levels8.php'); ?>
                    },{
                        name: 'TK223 Level',
                        data: <?php include('custom/time_series_levels9.php'); ?>
                    },{
                        name: 'TK224 Level',
                        data: <?php include('custom/time_series_levels10.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 3) { ?>  
						
					series: [{
                        name: 'FQI103 Flow',
                        data: <?php include('custom/time_series_levels11.php'); ?>
                    },{
                        name: 'FQI110 Flow',
                        data: <?php include('custom/time_series_levels12.php'); ?>
                    },{
                        name: 'FQI430B Flow',
                        data: <?php include('custom/time_series_levels13.php'); ?>
                    },{
                        name: 'FQI111C Flow',
                        data: <?php include('custom/time_series_levels14.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 5) { ?>  
						
					series: [{
                        name: 'FT-310',
                        data: <?php include('custom/time_series_levels15.php'); ?>
                    },{
                        name: 'FT-311',
                        data: <?php include('custom/time_series_levels16.php'); ?>
                    },{
                        name: 'FT-312',
                        data: <?php include('custom/time_series_levels17.php'); ?>
                    },{
                        name: 'FT-313',
                        data: <?php include('custom/time_series_levels18.php'); ?>
                    },{
                        name: 'FT-314',
                        data: <?php include('custom/time_series_levels19.php'); ?>
                    },{
                        name: 'FT-315',
                        data: <?php include('custom/time_series_levels20.php'); ?>
                    },{
                        name: 'PT-310',
                        data: <?php include('custom/time_series_levels21.php'); ?>
                    },{
                        name: 'PT-311',
                        data: <?php include('custom/time_series_levels22.php'); ?>
                    },{
                        name: 'PT-312',
                        data: <?php include('custom/time_series_levels23.php'); ?>
                    },{
                        name: 'PT-313',
                        data: <?php include('custom/time_series_levels24.php'); ?>
                    },{
                        name: 'PT-314',
                        data: <?php include('custom/time_series_levels25.php'); ?>
                    },{
                        name: 'PT-315',
                        data: <?php include('custom/time_series_levels26.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels27.php'); ?>
                    },{
                        name: 'VT-314 EU Value',
                        data: <?php include('custom/time_series_levels28.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels29.php'); ?>
                    }]
						
					<?php  } ?>  
						
                });
				   
				<?php  } ?>  
	
				 
				   <?php  if ($_SESSION['chart_type'] == 2 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'waterfall',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Pressure (psi)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'psi'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					   
					<?php  if (!isset($_GET['s']) or $_GET['s'] == 1) { ?>  
					   
                    series: [{
                        name: 'TK210 Level',
                        data: <?php include('custom/time_series_levels.php'); ?>
                    },{
                        name: 'TK211 Level',
                        data: <?php include('custom/time_series_levels2.php'); ?>
                    },{
                        name: 'TK212 Level',
                        data: <?php include('custom/time_series_levels3.php'); ?>
                    },{
                        name: 'TK213 Level',
                        data: <?php include('custom/time_series_levels4.php'); ?>
                    },{
                        name: 'TK214 Level',
                        data: <?php include('custom/time_series_levels5.php'); ?>
                    }]
						
					<?php  } else if ($_GET['s'] == 2) { ?>  
						
					series: [{
                        name: 'TK220 Level',
                        data: <?php include('custom/time_series_levels6.php'); ?>
                    },{
                        name: 'TK221 Level',
                        data: <?php include('custom/time_series_levels7.php'); ?>
                    },{
                        name: 'TK222 Level',
                        data: <?php include('custom/time_series_levels8.php'); ?>
                    },{
                        name: 'TK223 Level',
                        data: <?php include('custom/time_series_levels9.php'); ?>
                    },{
                        name: 'TK224 Level',
                        data: <?php include('custom/time_series_levels10.php'); ?>
                    }]
						
				<?php  } else if ($_GET['s'] == 3) { ?>  
						
					series: [{
                        name: 'FQI103 Flow',
                        data: <?php include('custom/time_series_levels11.php'); ?>
                    },{
                        name: 'FQI110 Flow',
                        data: <?php include('custom/time_series_levels12.php'); ?>
                    },{
                        name: 'FQI430B Flow',
                        data: <?php include('custom/time_series_levels13.php'); ?>
                    },{
                        name: 'FQI111C Flow',
                        data: <?php include('custom/time_series_levels14.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 5) { ?>  
						
					series: [{
                        name: 'FT-310',
                        data: <?php include('custom/time_series_levels15.php'); ?>
                    },{
                        name: 'FT-311',
                        data: <?php include('custom/time_series_levels16.php'); ?>
                    },{
                        name: 'FT-312',
                        data: <?php include('custom/time_series_levels17.php'); ?>
                    },{
                        name: 'FT-313',
                        data: <?php include('custom/time_series_levels18.php'); ?>
                    },{
                        name: 'FT-314',
                        data: <?php include('custom/time_series_levels19.php'); ?>
                    },{
                        name: 'FT-315',
                        data: <?php include('custom/time_series_levels20.php'); ?>
                    },{
                        name: 'PT-310',
                        data: <?php include('custom/time_series_levels21.php'); ?>
                    },{
                        name: 'PT-311',
                        data: <?php include('custom/time_series_levels22.php'); ?>
                    },{
                        name: 'PT-312',
                        data: <?php include('custom/time_series_levels23.php'); ?>
                    },{
                        name: 'PT-313',
                        data: <?php include('custom/time_series_levels24.php'); ?>
                    },{
                        name: 'PT-314',
                        data: <?php include('custom/time_series_levels25.php'); ?>
                    },{
                        name: 'PT-315',
                        data: <?php include('custom/time_series_levels26.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels27.php'); ?>
                    },{
                        name: 'VT-314 EU Value',
                        data: <?php include('custom/time_series_levels28.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels29.php'); ?>
                    }]
						
					<?php  } ?>  
					   
                });
				   
				<?php  } ?>  
			   				   
	
				   
				 <?php  if ($_SESSION['chart_type'] == 3 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'bar',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					   
					<?php  if (!isset($_GET['s']) or $_GET['s'] == 1) { ?>  
					   
                    series: [{
                        name: 'TK210 Level',
                        data: <?php include('custom/time_series_levels.php'); ?>
                    },{
                        name: 'TK211 Level',
                        data: <?php include('custom/time_series_levels2.php'); ?>
                    },{
                        name: 'TK212 Level',
                        data: <?php include('custom/time_series_levels3.php'); ?>
                    },{
                        name: 'TK213 Level',
                        data: <?php include('custom/time_series_levels4.php'); ?>
                    },{
                        name: 'TK214 Level',
                        data: <?php include('custom/time_series_levels5.php'); ?>
                    }]
						
					<?php  } else if ($_GET['s'] == 2) { ?>  
						
					series: [{
                        name: 'TK220 Level',
                        data: <?php include('custom/time_series_levels6.php'); ?>
                    },{
                        name: 'TK221 Level',
                        data: <?php include('custom/time_series_levels7.php'); ?>
                    },{
                        name: 'TK222 Level',
                        data: <?php include('custom/time_series_levels8.php'); ?>
                    },{
                        name: 'TK223 Level',
                        data: <?php include('custom/time_series_levels9.php'); ?>
                    },{
                        name: 'TK224 Level',
                        data: <?php include('custom/time_series_levels10.php'); ?>
                    }]
						
				<?php  } else if ($_GET['s'] == 3) { ?>  
						
					series: [{
                        name: 'FQI103 Flow',
                        data: <?php include('custom/time_series_levels11.php'); ?>
                    },{
                        name: 'FQI110 Flow',
                        data: <?php include('custom/time_series_levels12.php'); ?>
                    },{
                        name: 'FQI430B Flow',
                        data: <?php include('custom/time_series_levels13.php'); ?>
                    },{
                        name: 'FQI111C Flow',
                        data: <?php include('custom/time_series_levels14.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 5) { ?>  
						
					series: [{
                        name: 'FT-310',
                        data: <?php include('custom/time_series_levels15.php'); ?>
                    },{
                        name: 'FT-311',
                        data: <?php include('custom/time_series_levels16.php'); ?>
                    },{
                        name: 'FT-312',
                        data: <?php include('custom/time_series_levels17.php'); ?>
                    },{
                        name: 'FT-313',
                        data: <?php include('custom/time_series_levels18.php'); ?>
                    },{
                        name: 'FT-314',
                        data: <?php include('custom/time_series_levels19.php'); ?>
                    },{
                        name: 'FT-315',
                        data: <?php include('custom/time_series_levels20.php'); ?>
                    },{
                        name: 'PT-310',
                        data: <?php include('custom/time_series_levels21.php'); ?>
                    },{
                        name: 'PT-311',
                        data: <?php include('custom/time_series_levels22.php'); ?>
                    },{
                        name: 'PT-312',
                        data: <?php include('custom/time_series_levels23.php'); ?>
                    },{
                        name: 'PT-313',
                        data: <?php include('custom/time_series_levels24.php'); ?>
                    },{
                        name: 'PT-314',
                        data: <?php include('custom/time_series_levels25.php'); ?>
                    },{
                        name: 'PT-315',
                        data: <?php include('custom/time_series_levels26.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels27.php'); ?>
                    },{
                        name: 'VT-314 EU Value',
                        data: <?php include('custom/time_series_levels28.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels29.php'); ?>
                    }]
						
					<?php  } ?>   
                });
				   
				<?php  } ?>  
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
   
    

		<?php //include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

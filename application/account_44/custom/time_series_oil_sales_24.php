<?php 

if ($_GET['start']) {
	$regdates = date_range($_GET['start'], $_GET['end']);	
} else {
	$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
}

$array = '';

foreach ($regdates as $pdate) {  

	$array[] = array(strtotime($pdate)*1000,intval(getOilSale($pdate,24)));	
}

echo json_encode($array);

?>

<?php 
include('/var/www/html/application/includes/site.php');
//$regions = array('Texas');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">OPC UA Test Site</a>
       
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
       <?php /*?>     <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global</a></li><?php */?>

           <?php foreach ($regions as $key=>$region) { ?>
            <li class="<?php if ($_GET['r'] == ($key+1)) echo 'active ' ?> dropdown">
              <a href="dashboard.php?r=1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $region; ?> <span class="caret"></span></a>
              <ul class="dropdown-menu">

                    <?php $sitedevices = getDevicesByRegion($_SESSION['account_id'],$region,44); 
                            foreach ($sitedevices as $device) {
                                echo '<li><a href="dashboard.php?r='.($key+1).'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
                            }
                    ?>

              </ul>
            </li>
 		 <?php }
			  
			  ?>

			<li><a href="index.php">&laquo; Back to Accounts</a></li>
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           <?php /*?> <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global <span class="sr-only">(current)</span></a></li><?php */?>
            
            
            <?php foreach ($regions as $key=>$region) { ?>
            

				<li <?php if ($_GET['r'] ==  ($key+1)) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
          
          
         <?php if (isset($_GET['r'])) { ?>
          <ul class="nav nav-list">
            <li class="nav-header">Sites</li>
            
            <?php $sitedevices = getDevicesByRegion($_SESSION['user_accountID'],$regions[$_GET['r']-1],44); 
					

					foreach ($sitedevices as $device) {
						
						if ($_GET['d'] == $device['device_deviceID']) {
							echo '<li class="leftnavactive"><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">&raquo; '.$device['device_name'].'</a></li>';
						} else {
							echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
						}
					}
			?>
            	
          </ul>
         <?php } ?>
         
          
         <ul class="nav nav-sidebar">         
			<li><a href="index.php">&laquo; Back to Accounts</a></li>
          	<li><a href="process/logout.php">Logout</a></li>
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

			<h1 class="page-header">Tag Readings</h1>
		
        


           
           <!--  <div id="chart"></div>-->


			
            <div class="row">
            	
                <div class="col-md-6">
                
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th colspan="3">Measurements [<a href="dashboard.php">refresh</a>]</th>
                                </tr>
                                <tr><th>Friendly Name</th><th>Value</th><th>Timestamp</th></tr>
                              </thead>
                              <tbody>

								<?php 

										$database = "account_301";	
										$db = connectTWO($database);

										$stmt = $db->prepare("SELECT * FROM tbl_opcua WHERE node_type = 'Measurement' ");
										$stmt->execute();
										$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

										foreach ($results as $result) {

											echo '<tr><td>'.$result['friendly_name'].'</td>';

											$json = file_get_contents('http://35.170.198.183:8088/beeond/Quest/1.0.0/Readings/CurrentValue?nodeid=ns=1;s='.$result['node_name'].'&customerid=4');
											$obj = json_decode($json);

											if (!empty($obj->value)) {
												echo '<td><a href="http://35.170.198.183:8088/beeond/Quest/1.0.0/Readings/CurrentValue?nodeid=ns=1;s='.$result['node_name'].'&customerid=4" target="_blank">'.$obj->value.'</a></td><td>'.date("m/d/Y H:i:s",$obj->itimestamp).' EST</td>';

											} else {
												echo '<td colspan="2"><span class="text-danger">No data!</span></td>';
											}

											echo '</tr>';
										}



								?>                     
                              </tbody>
                            </table>

                </div>
            
            
            
                <div class="col-md-6">
                
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th colspan="2">Triggers</th>
                                </tr>
                                <tr><th>Friendly Name</th><th>Value</th></tr>
                              </thead>
                              <tbody>
                              
                              
                              
								<?php 

										$database = "account_301";	
										$db = connectTWO($database);

										$stmt = $db->prepare("SELECT * FROM tbl_opcua WHERE node_type = 'Trigger' ");
										$stmt->execute();
										$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

										foreach ($results as $result) {

											echo '<tr><td>'.$result['friendly_name'].'</td>';
												
											echo '<td><form class="form-inline" method="post" action="/application/account_301/post.php" >
												<input type="hidden" name="node_name" value="'.$result['node_name'].'">
												<input type="hidden" name="fr_name" value="'.$result['friendly_name'].'">
												<input class="form-control" name="value" type="text">
												  <button type="submit" class="btn btn-default">Submit</button>
											</form></td>';


											echo '</tr>';
										}
								  
								  
								  											 
										if (isset($_SESSION['setpoint'])) {

											echo '<tr><td colspan="2"><div class="alert alert-info" role="alert" style="margintop:15px;"><h4>'.$_SESSION['fr'].'</h4>Setpoint set to: '.$_SESSION['setpoint'].' for Node  '.$_SESSION['node'].'</div></td></tr>';
											
											
											unset($_SESSION['setpoint']);
											unset($_SESSION['node']);
											unset($_SESSION['fr']);


										}



								?> 

           

							</tbody>
                            </table>

                
                
            
            </div>




        </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="/application/libraries/jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="/application/libraries/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="/application/libraries/jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    

    <?php  if ($_GET['f']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if ($_GET['s'] or $_GET['g'] or !isset($_GET['d'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(2, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	<script>
		
					   		
		
 					   		$( "#newlink" ).click(function() {	

								$( "#add_modal" ).dialog( "open" );	
							});

							$( "#newlinkEXTRA" ).click(function() {	

								$( "#add_modalEXTRA" ).dialog( "open" );	
							});

							$( "#newlink2" ).click(function() {	

								$( "#add_modalWIDE" ).dialog( "open" );	
							});


							$( "#add_modal" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalEXTRA" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalWIDE" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 950,
							  modal: true,

							});
			
		</script>
 	
 	
 	<?php  if (!$_GET['m'] && !$_GET['c'] && !$_GET['b'] && !$_GET['f'] ) { ?>  
   
   	<?php  if ($_GET['s'] != 4) { ?>  
   	
    	<script>
               $(function () {
				   	
				   
				   <?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { ?>  
                
				   Highcharts.chart('chart', {
					  
					   				chart: {
					type: 'line',
					zoomType: 'x'
				},
					   
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
						
                    },
                    yAxis: {
                        title: {
                            text: 'Pressure (psi)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: 'psi'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					   
					<?php  if (!isset($_GET['s']) or $_GET['s'] == 1) { ?>  
					   
                    series: [{
                        name: 'TK210 Level',
                        data: <?php include('custom/time_series_levels.php'); ?>
                    },{
                        name: 'TK211 Level',
                        data: <?php include('custom/time_series_levels2.php'); ?>
                    },{
                        name: 'TK212 Level',
                        data: <?php include('custom/time_series_levels3.php'); ?>
                    },{
                        name: 'TK213 Level',
                        data: <?php include('custom/time_series_levels4.php'); ?>
                    },{
                        name: 'TK214 Level',
                        data: <?php include('custom/time_series_levels5.php'); ?>
                    }]
						
					<?php  } else if ($_GET['s'] == 2) { ?>  
						
					series: [{
                        name: 'TK220 Level',
                        data: <?php include('custom/time_series_levels6.php'); ?>
                    },{
                        name: 'TK221 Level',
                        data: <?php include('custom/time_series_levels7.php'); ?>
                    },{
                        name: 'TK222 Level',
                        data: <?php include('custom/time_series_levels8.php'); ?>
                    },{
                        name: 'TK223 Level',
                        data: <?php include('custom/time_series_levels9.php'); ?>
                    },{
                        name: 'TK224 Level',
                        data: <?php include('custom/time_series_levels10.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 3) { ?>  
						
					series: [{
                        name: 'FQI103 Flow',
                        data: <?php include('custom/time_series_levels11.php'); ?>
                    },{
                        name: 'FQI110 Flow',
                        data: <?php include('custom/time_series_levels12.php'); ?>
                    },{
                        name: 'FQI430B Flow',
                        data: <?php include('custom/time_series_levels13.php'); ?>
                    },{
                        name: 'FQI111C Flow',
                        data: <?php include('custom/time_series_levels14.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 5) { ?>  
						
					series: [{
                        name: 'FT-310',
                        data: <?php include('custom/time_series_levels15.php'); ?>
                    },{
                        name: 'FT-311',
                        data: <?php include('custom/time_series_levels16.php'); ?>
                    },{
                        name: 'FT-312',
                        data: <?php include('custom/time_series_levels17.php'); ?>
                    },{
                        name: 'FT-313',
                        data: <?php include('custom/time_series_levels18.php'); ?>
                    },{
                        name: 'FT-314',
                        data: <?php include('custom/time_series_levels19.php'); ?>
                    },{
                        name: 'FT-315',
                        data: <?php include('custom/time_series_levels20.php'); ?>
                    },{
                        name: 'PT-310',
                        data: <?php include('custom/time_series_levels21.php'); ?>
                    },{
                        name: 'PT-311',
                        data: <?php include('custom/time_series_levels22.php'); ?>
                    },{
                        name: 'PT-312',
                        data: <?php include('custom/time_series_levels23.php'); ?>
                    },{
                        name: 'PT-313',
                        data: <?php include('custom/time_series_levels24.php'); ?>
                    },{
                        name: 'PT-314',
                        data: <?php include('custom/time_series_levels25.php'); ?>
                    },{
                        name: 'PT-315',
                        data: <?php include('custom/time_series_levels26.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels27.php'); ?>
                    },{
                        name: 'VT-314 EU Value',
                        data: <?php include('custom/time_series_levels28.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels29.php'); ?>
                    }]
						
					<?php  } ?>  
						
                });
				   
				<?php  } ?>  
	
				 
				   <?php  if ($_SESSION['chart_type'] == 2 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'waterfall',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Pressure (psi)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'psi'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					   
					<?php  if (!isset($_GET['s']) or $_GET['s'] == 1) { ?>  
					   
                    series: [{
                        name: 'TK210 Level',
                        data: <?php include('custom/time_series_levels.php'); ?>
                    },{
                        name: 'TK211 Level',
                        data: <?php include('custom/time_series_levels2.php'); ?>
                    },{
                        name: 'TK212 Level',
                        data: <?php include('custom/time_series_levels3.php'); ?>
                    },{
                        name: 'TK213 Level',
                        data: <?php include('custom/time_series_levels4.php'); ?>
                    },{
                        name: 'TK214 Level',
                        data: <?php include('custom/time_series_levels5.php'); ?>
                    }]
						
					<?php  } else if ($_GET['s'] == 2) { ?>  
						
					series: [{
                        name: 'TK220 Level',
                        data: <?php include('custom/time_series_levels6.php'); ?>
                    },{
                        name: 'TK221 Level',
                        data: <?php include('custom/time_series_levels7.php'); ?>
                    },{
                        name: 'TK222 Level',
                        data: <?php include('custom/time_series_levels8.php'); ?>
                    },{
                        name: 'TK223 Level',
                        data: <?php include('custom/time_series_levels9.php'); ?>
                    },{
                        name: 'TK224 Level',
                        data: <?php include('custom/time_series_levels10.php'); ?>
                    }]
						
				<?php  } else if ($_GET['s'] == 3) { ?>  
						
					series: [{
                        name: 'FQI103 Flow',
                        data: <?php include('custom/time_series_levels11.php'); ?>
                    },{
                        name: 'FQI110 Flow',
                        data: <?php include('custom/time_series_levels12.php'); ?>
                    },{
                        name: 'FQI430B Flow',
                        data: <?php include('custom/time_series_levels13.php'); ?>
                    },{
                        name: 'FQI111C Flow',
                        data: <?php include('custom/time_series_levels14.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 5) { ?>  
						
					series: [{
                        name: 'FT-310',
                        data: <?php include('custom/time_series_levels15.php'); ?>
                    },{
                        name: 'FT-311',
                        data: <?php include('custom/time_series_levels16.php'); ?>
                    },{
                        name: 'FT-312',
                        data: <?php include('custom/time_series_levels17.php'); ?>
                    },{
                        name: 'FT-313',
                        data: <?php include('custom/time_series_levels18.php'); ?>
                    },{
                        name: 'FT-314',
                        data: <?php include('custom/time_series_levels19.php'); ?>
                    },{
                        name: 'FT-315',
                        data: <?php include('custom/time_series_levels20.php'); ?>
                    },{
                        name: 'PT-310',
                        data: <?php include('custom/time_series_levels21.php'); ?>
                    },{
                        name: 'PT-311',
                        data: <?php include('custom/time_series_levels22.php'); ?>
                    },{
                        name: 'PT-312',
                        data: <?php include('custom/time_series_levels23.php'); ?>
                    },{
                        name: 'PT-313',
                        data: <?php include('custom/time_series_levels24.php'); ?>
                    },{
                        name: 'PT-314',
                        data: <?php include('custom/time_series_levels25.php'); ?>
                    },{
                        name: 'PT-315',
                        data: <?php include('custom/time_series_levels26.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels27.php'); ?>
                    },{
                        name: 'VT-314 EU Value',
                        data: <?php include('custom/time_series_levels28.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels29.php'); ?>
                    }]
						
					<?php  } ?>  
					   
                });
				   
				<?php  } ?>  
			   				   
	
				   
				 <?php  if ($_SESSION['chart_type'] == 3 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'bar',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					   
					<?php  if (!isset($_GET['s']) or $_GET['s'] == 1) { ?>  
					   
                    series: [{
                        name: 'TK210 Level',
                        data: <?php include('custom/time_series_levels.php'); ?>
                    },{
                        name: 'TK211 Level',
                        data: <?php include('custom/time_series_levels2.php'); ?>
                    },{
                        name: 'TK212 Level',
                        data: <?php include('custom/time_series_levels3.php'); ?>
                    },{
                        name: 'TK213 Level',
                        data: <?php include('custom/time_series_levels4.php'); ?>
                    },{
                        name: 'TK214 Level',
                        data: <?php include('custom/time_series_levels5.php'); ?>
                    }]
						
					<?php  } else if ($_GET['s'] == 2) { ?>  
						
					series: [{
                        name: 'TK220 Level',
                        data: <?php include('custom/time_series_levels6.php'); ?>
                    },{
                        name: 'TK221 Level',
                        data: <?php include('custom/time_series_levels7.php'); ?>
                    },{
                        name: 'TK222 Level',
                        data: <?php include('custom/time_series_levels8.php'); ?>
                    },{
                        name: 'TK223 Level',
                        data: <?php include('custom/time_series_levels9.php'); ?>
                    },{
                        name: 'TK224 Level',
                        data: <?php include('custom/time_series_levels10.php'); ?>
                    }]
						
				<?php  } else if ($_GET['s'] == 3) { ?>  
						
					series: [{
                        name: 'FQI103 Flow',
                        data: <?php include('custom/time_series_levels11.php'); ?>
                    },{
                        name: 'FQI110 Flow',
                        data: <?php include('custom/time_series_levels12.php'); ?>
                    },{
                        name: 'FQI430B Flow',
                        data: <?php include('custom/time_series_levels13.php'); ?>
                    },{
                        name: 'FQI111C Flow',
                        data: <?php include('custom/time_series_levels14.php'); ?>
                    }]
					   
					<?php  } else if ($_GET['s'] == 5) { ?>  
						
					series: [{
                        name: 'FT-310',
                        data: <?php include('custom/time_series_levels15.php'); ?>
                    },{
                        name: 'FT-311',
                        data: <?php include('custom/time_series_levels16.php'); ?>
                    },{
                        name: 'FT-312',
                        data: <?php include('custom/time_series_levels17.php'); ?>
                    },{
                        name: 'FT-313',
                        data: <?php include('custom/time_series_levels18.php'); ?>
                    },{
                        name: 'FT-314',
                        data: <?php include('custom/time_series_levels19.php'); ?>
                    },{
                        name: 'FT-315',
                        data: <?php include('custom/time_series_levels20.php'); ?>
                    },{
                        name: 'PT-310',
                        data: <?php include('custom/time_series_levels21.php'); ?>
                    },{
                        name: 'PT-311',
                        data: <?php include('custom/time_series_levels22.php'); ?>
                    },{
                        name: 'PT-312',
                        data: <?php include('custom/time_series_levels23.php'); ?>
                    },{
                        name: 'PT-313',
                        data: <?php include('custom/time_series_levels24.php'); ?>
                    },{
                        name: 'PT-314',
                        data: <?php include('custom/time_series_levels25.php'); ?>
                    },{
                        name: 'PT-315',
                        data: <?php include('custom/time_series_levels26.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels27.php'); ?>
                    },{
                        name: 'VT-314 EU Value',
                        data: <?php include('custom/time_series_levels28.php'); ?>
                    },{
                        name: 'VT-310 EU Value',
                        data: <?php include('custom/time_series_levels29.php'); ?>
                    }]
						
					<?php  } ?>   
                });
				   
				<?php  } ?>  
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
   
    

		<?php //include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

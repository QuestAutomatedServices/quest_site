<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");
include("../user_mail.php");

if ($_POST['pt'] == 1) {

	$string = generate_random_string();
	
	$_POST['user_password'] = password_hash($string, PASSWORD_DEFAULT);
	
	$f = 'getAdminLevelUsers';
	$table = 'tbl_users';
	
	sendUserMail($_POST['user_email'], $_POST['user_name'], $string);
	
	$redir = "admin";
	
}else if ($_POST['pt'] == 2) {
	
	$string = generate_random_string();
	
	$_POST['user_password'] = password_hash($string, PASSWORD_DEFAULT);
	
	$f = 'getAccountUsers';
	$table = 'tbl_users';
	
	sendUserMail($_POST['user_email'], $_POST['user_name'], $string);
	
}else if ($_POST['pt'] == 3) {
	
	$string = generate_random_string();
	
	$_POST['user_password'] = password_hash($string, PASSWORD_DEFAULT);
	
	$f = 'getAccountUsersTwo';
	$a = $_POST['user_accountID'];
	$table = 'tbl_users';
	
	sendUserMail($_POST['user_email'], $_POST['user_name'], $string);
	
}


unset($_POST['pt']);

if ($redir == "admin") {
	$record = insertRecord($_POST,$table,999);
} else {
	$record = insertRecord($_POST,$table,999);
}





if ($redir == "admin"){
	
	header("location: ../index.php?f=$f");
}
else {
	if(isset($a)){
		header("location: ../dashboard.php?f=$f&a=$a");
	}else{
		header("location: ../dashboard.php?f=$f");
	}
}

/*
if ($noID){
	header("location: ../index.php?f=$f");
}else{
	if ($id) {
		if ($needBID) {
			header("location: ../index.php?f=$f&id=$id&bid=$id2");
		}else {
			header("location: ../index.php?f=$f&id=$id");
		}

	} else {
		header("location: ../index.php?f=$f&id=$record");
	}	
}*/


?>
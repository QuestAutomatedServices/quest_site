<?php

include("../../twilio/process/sendAlarmCall.php");
include("../../twilio/process/sendAlarmText.php");
include("../../twilio/process/sendAlarmEmail.php");

if($_POST['pt'] == 1){
	switch($_POST['demo_type']){
		case "Call":
			twilioCallDemo($_POST['demo_number'],$_POST['demo_message']);
			break;
		case "Text":
			twilioTextDemo($_POST['demo_number'],$_POST['demo_message']);
			break;
		case "Email":
			mailgunEmailDemo($_POST['demo_number'],$_POST['demo_message']);
			break;
	}
}

header("location: ../index.php?f=getAlarmDemo");

?>

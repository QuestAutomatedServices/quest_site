<?php 
include("/var/www/html/quest/functions/functions.php");

ini_set('max_execution_time', '-1'); 
ini_set('memory_limit',"-1");

$prev = date('Y-m-d', strtotime("-2 days"));
$dates = date_range($prev,date('Y-m-d'));

$db = connectTWO("account_43");

$stmt = $db->query("SELECT * FROM tbl_opcua_mapping WHERE tag_config = 'Oil - Production and Sales'");
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);



foreach($results as $tag){

		$multiplier = $tag['measurement_factor'];
	    $convention = $tag['opcua_convention'];
		$d = $tag['d'];
	 	$mapID = $tag['mapID'];

		foreach ($dates as $key=>$date) {

			$dateplus1 = date('Y-m-d', strtotime($date . ' +1 day'));
			
			$stmt = $db->query("SELECT * FROM tbl_register_history WHERE opcua_convention = '$convention' AND (date(register_date) = '$date' and hour(register_date) >= 07) OR  opcua_convention = '$convention' AND (date(register_date) = '$dateplus1' and hour(register_date) < 05)   ORDER BY register_date DESC ");
			
			$array = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			//echo "<pre>";
		//	print_r($array);
			//exit();

		

			$production = 0;
			$sales = 0;
			$last = count($array) - 1;




			foreach ($array as $key=>$reading) {

				$increment = 0;
				$sincrement = 0;

				if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
					$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					$increment = ($array[$key]['register_reading'] - $array[$key+1]['register_reading']); 
				} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
					$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					$sincrement = ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
				} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
					$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					$increment = ($array[$key]['register_reading'] - $array[$key+1]['register_reading']); 
				}

				if ($increment > 0) { 

						$factoredinc = ($increment*$multiplier);

						$stmt = $db->query("DELETE FROM tbl_incremental_oil_prod WHERE mapID = $mapID AND register_date = '".$array[$key]['register_date']."' AND production_calculation IS NOT NULL ");

						$stmt = $db->query("INSERT INTO tbl_incremental_oil_prod (mapID, d, production_calculation, production_calculation_in, register_date ) VALUES ($mapID, $d, $factoredinc, $increment, '".$array[$key]['register_date']."' ) ");

				}


				if ($sincrement < 0) {

						$factoredsinc = ($sincrement*$multiplier);

						$stmt = $db->query("DELETE FROM tbl_incremental_oil_prod WHERE mapID = $mapID AND register_date = '".$array[$key]['register_date']."' AND sales_calculation IS NOT NULL ");

						$stmt = $db->query("INSERT INTO tbl_incremental_oil_prod (mapID, d, sales_calculation, sales_calculation_in, register_date ) VALUES ($mapID, $d, abs($factoredsinc), abs($sincrement), '".$array[$key]['register_date']."' ) ");

				}



			}


		   $factoredprod = ($production*$multiplier);
		   $factoredsales = ($sales*$multiplier);


			$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE mapID = $mapID AND calculation_date = '$date' AND calculation_type = 1 ");
			
			$stmt = $db->query("INSERT INTO tbl_register_calculations (mapID, d, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($mapID, $d, $factoredprod, $production, abs($factoredsales), abs($sales), '$date', 1) ");


	
		}
}



$stmt = $db->query("SELECT * FROM tbl_opcua_mapping WHERE tag_config = 'Water - Tank Level Feet'");
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

//echo "<pre>";
//print_r($results);
//exit();


foreach($results as $tag){

		$multiplier = $tag['measurement_factor'];
	    $convention = $tag['opcua_convention'];
		$d = $tag['d'];
	 	$mapID = $tag['mapID'];

		foreach ($dates as $key=>$date) {

			$dateplus1 = date('Y-m-d', strtotime($date . ' +1 day'));
			
			$stmt = $db->query("SELECT * FROM tbl_register_history WHERE opcua_convention = '$convention' AND (date(register_date) = '$date' and hour(register_date) >= 07) OR  opcua_convention = '$convention' AND (date(register_date) = '$dateplus1' and hour(register_date) < 05)   ORDER BY register_date DESC ");
			
			$array = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			//echo "<pre>";
		//	print_r($array);
			//exit();

		

			$production = 0;
			$sales = 0;
			$last = count($array) - 1;




			foreach ($array as $key=>$reading) {

				$increment = 0;
				$sincrement = 0;

				if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
					$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					$increment = ($array[$key]['register_reading'] - $array[$key+1]['register_reading']); 
				} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
					$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					$sincrement = ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
				} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
					$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					$increment = ($array[$key]['register_reading'] - $array[$key+1]['register_reading']); 
				}



			}


		   $factoredprod = ($production*$multiplier);
		   $factoredsales = ($sales*$multiplier);


			$stmt = $db->query("DELETE FROM tbl_register_calculations WHERE mapID = $mapID AND calculation_date = '$date' AND calculation_type = 2 ");

			$stmt = $db->query("INSERT INTO tbl_register_calculations (mapID, d, water_production_calculation, water_trucked_calculation, calculation_date, calculation_type) VALUES ($mapID, $d, $factoredprod, abs($factoredsales), '$date', 2) ");

		}
}


?>
<?php 
include('../functions/functions.php');

$array = '';

	$database = "account_45";
	$db = connectTWO($database);
	
	if ($_GET['start']) {
		$stmt = $db->prepare("SELECT calculation_date, sum(water_production_calculation) as totprod FROM `tbl_register_calculations` WHERE register_deviceID = 238 AND register_number = 61 AND calculation_type = 2 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' GROUP BY calculation_date ORDER BY calculation_date");
	} else {
		$stmt = $db->prepare("SELECT calculation_date, sum(water_production_calculation) as totprod FROM `tbl_register_calculations` WHERE  register_deviceID = 238  AND register_number = 61 AND  calculation_type = 2  GROUP BY calculation_date ORDER BY calculation_date DESC LIMIT 7 ");
	}
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);


foreach($results as $row) {
	
	$array[] = array((strtotime($row['calculation_date'])*1000),intval($row['totprod']));	
}

echo json_encode($array);

?>

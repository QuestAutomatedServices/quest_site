<?php 
include('/var/www/html/application/includes/site.php');
$regions = array('Texas');


?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
      
    <style>
        .cylinder-wrapper{
            border: 10px solid #EFEFEF;
            border-radius: 15px;
            background-color: #FFF;
            height: 200px;
            width: 105px;
            position:relative;
			z-index: 999999;
			margin-left: auto;
			margin-right: auto;
        }
		
		  .cylinder-wrapper:hover {
               border: 10px solid #777792;
        }
        
        .cylinder-value1{
            background-color: #002db3;
            border-radius: 0px 0px 5px 5px;
            width: 100%;
            bottom: 0;
            position: absolute;
        }
        
        .cylinder-value2{
            background-color: #79d279;
            width: 100%;
            position: absolute;
        }
    </style>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">Olifant Energy</a>
       
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
       <?php /*?>     <li <?php if (!isset($_GET['r'])) echo 'class="active"' ?>><a href="dashboard.php">Global</a></li><?php */?>

           <?php foreach ($regions as $key=>$region) { ?>
            <li class="<?php if ($_GET['r'] == ($key+1)) echo 'active ' ?> dropdown">
              <a href="dashboard.php?r=1" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $region; ?> <span class="caret"></span></a>
              <ul class="dropdown-menu">

                    <?php $sitedevices = getDevicesByRegion($_SESSION['user_accountID'],$region,$_SESSION['user_accountID']); 
                            foreach ($sitedevices as $device) {
                                echo '<li><a href="dashboard.php?r='.($key+1).'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
                            }
                    ?>

              </ul>
            </li>
 		 <?php }
			  
			  ?>

            
   
			<li><a href="../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            
            <?php foreach ($regions as $key=>$region) { ?>
            

				<li <?php if ($_GET['r'] ==  ($key+1)) echo 'class="active"' ?>><a href="dashboard.php?r=<?php echo ($key+1) ?>"><?php echo $region; ?></a></li>
          
           <?php } ?>

          </ul>
          
          
         <?php if (isset($_GET['r'])) { ?>
          <ul class="nav nav-list">
            <li class="nav-header">Sites</li>
            
            <?php $sitedevices = getDevicesByRegion($_SESSION['user_accountID'],$regions[$_GET['r']-1],$_SESSION['user_accountID']); 
					

					foreach ($sitedevices as $device) {
						
						if ($_GET['d'] == $device['device_deviceID']) {
							echo '<li class="leftnavactive"><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">&raquo; '.$device['device_name'].'</a></li>';
						} else {
							echo '<li><a href="dashboard.php?r='.$_GET['r'].'&d='.$device['device_deviceID'].'&s=1">'.$device['device_name'].'</a></li>';
						}
					}
			?>
            	
          </ul>
         <?php } ?>
         
          
         <ul class="nav nav-sidebar">
                  
               <?php 
			 			  if($_SESSION['auth'] == 1 or $_SESSION['auth'] == 2){
							  if ($_GET['f'] == 'getAccountUsersTwo'){
								  echo '<li class="active"><a href="dashboard.php?f=getAccountUsersTwo">Users</a></li>';
							  }else{
								  echo '<li><a href="dashboard.php?f=getAccountUsersTwo">Users</a></li>';
							  }
						}
			 
			 				if ($_GET['f'] == 'getCurrentUser'){
 								
								echo '<li class="active"><a href="dashboard.php?f=getCurrentUser">Account</a></li>';			
							  }else{
								   echo '<li><a href="dashboard.php?f=getCurrentUser">Account</a></li>';
							  }
			 
			
			 
			 
			 ?>
                   
			<li><a href="../support/support.php">Support</a></li>
          	<li><a href="process/logout.php">Logout</a></li>
          </ul>
         
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         
         <?php if(!isset($_GET['a'])) {?>
          <h1 class="page-header"><?php if (!isset($_GET['r'])) { echo 'Global Overview'; } else if (!isset($_GET['d'])) { echo $regions[$_GET['r']-1].' Overview'; } ?><?php if (isset($_GET['d'])) echo getDeviceName($_GET['d'],$_SESSION['user_accountID']);  }?></h1>

		<?php 
			if($_GET['f'] == 'getAccountUsersTwo' or $_GET['f'] == 'getCurrentUser'){
				echo $_GET['f']();
			}
			
		?>



        <?php  if (!isset($_GET['d']) && !isset($_GET['a']) && !isset($_GET['f'])) { 
			
			
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
				}
			
				rsort($regdates);
			
			?>  
        
        
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
		
            
             <div id="chart"></div>


			
            <div class="row">
            	
                <div class="col-md-4">
                	
                     <h2 class="sub-header">Oil</h2>
                     
                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Barrels Produced</th>
                                  <th>Barrels Sold</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
										$totals = getTotalsArrayGroup(array(249),47); 
	
	
							   	   		foreach ($totals as $key=>$total) {  
							
												
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.$total['totprod'].'</td>
													<td>'.$total['totsales'].'</td>
												</tr>';
                                    
										}
                               
                               ?>
                              
                              </tbody>
                            </table>

                </div>
            

                <div class="col-md-4">

                     <h2 class="sub-header">Water</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Production</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
	   							$totals_water = getTotalsArrayWaterGroup(array(249),47);

                             		foreach ($totals_water as $key=>$total) {  

											
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  			<td>'.$total['totprod'].'</td>
												</tr>';
                                    
										}
                               
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
                  <div class="col-md-4">

                     <h2 class="sub-header">Gas</h2>

                            <table class="table table-striped">
                              <thead>
                                <tr>
                                  <th>Date</th>
                					<th>Volume</th>
                                </tr>
                              </thead>
                              <tbody>
                               
                               <?php 
							       
 
  	   							$totals_gas = getTotalsArrayGasGroup(array(249),47);
	
                             		foreach ($totals_gas as $key=>$total) {  

											
											echo '<tr>
												    <td>'.formatDate($total['calculation_date']).'</td>
												  	<td>'.$total['totprod'].'</td>
												</tr>';
                                    
										}
                               ?>
                              </tbody>
                            </table>
                
                </div>
                
                
            
            </div>


       


        <?php  } ?>  


        <?php  if (isset($_GET['d'])) { ?>  

	       	<div class="btn-group">
                 
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=5" class="btn btn-primary <?php if ($_GET['s'] == 5) echo 'active' ?>">Tank Levels</a>
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">Oil</a>
                  
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Water</a>
                  
				<a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=3" class="btn btn-primary <?php if ($_GET['s'] == 3) echo 'active' ?>">Gas</a>
                  
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=4" class="btn btn-primary <?php if ($_GET['s'] == 4) echo 'active' ?>">Pressures</a>


              </div>


	       	<div class="btn-group">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       
                  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>

                  
				  <a href="dashboard.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           		
            </div>
            
          <?php  if ( $_GET['s'] or $_GET['g'] ) { ?>  
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
			  <?php  } ?>  
			 
		
 		
        <?php  } ?>  


        <?php  if ($_GET['m'] == 1) { ?>  

          	<h2 class="sub-header">Location & Status</h2>

			<div class="alert-success">Device Properly Working</div>
			<div id="map"></div>

        <?php  } ?>  

        <?php  if ($_GET['f'] == 1) { ?>  
          	


          	<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

        <?php  } ?>  
        
        

        <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],$_SESSION['user_accountID']); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g'],$_SESSION['user_accountID']); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>GMT Date</th>
							  <th>TZ Date</th>
							  <th>Reading</th>
							</tr>
							
						<?php $readings = getRegisterReadings($_GET['d'],$_GET['g'],$_SESSION['user_accountID']); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td>'.$reading['register_number'].'</td>
											<td>'.formatTimestamp($reading['register_date']).'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  </thead>
						  <tbody>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Reporting Configuration</h2>
          	  	
          	  	<p>Reporting Time Range <input name="config_start_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_start_time",$_SESSION['user_accountID']); ?>"> to <input name="config_end_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_end_time",$_SESSION['user_accountID']); ?>"> </p>
            
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>Label</th>
							  <th>Config</th>
							  <th>Measurement Factor</th>
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getRegistersByDevice($_GET['d'],$_SESSION['user_accountID']); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {
								
								$noprodsell = '';
								$prodsell = '';
								
								if ($reg['register_prod_sell'] == 1) {
									$noprodsell = ' checked="checked" ';
								}
								
								if ($reg['register_prod_sell'] == 2) {
									$prodsell = ' checked="checked" ';
								}
								
								if ($reg['register_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}
								
								if ($reg['register_label'] == '') {
									
									$reg['register_label'] = $reg['register_name'];
								}
								
								
										//<td>&nbsp;</td><td><input name="produce_sell_'.$reg['register_number'].'" class="regprodsell" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  type="radio" value="1" '.$noprodsell.' > No <input name="produce_sell_'.$reg['register_number'].'" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  class="regprodsell" type="radio" value="2"  '.$prodsell.'> Yes</td>

								echo '<tr '.$configset.'>
										<td id="'.$reg['register_number'].'">'.$reg['register_number'].'</td>
										<td><input name="register_label" class="reglabel" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="25" type="text" value="'.$reg['register_label'].'"> <i class="fa fa-info-circle fa-lg" aria-hidden="true" title="#'.$reg['register_number'].'<br>'.$reg['register_label'].'<br>'.$reg['register_name'].'"></i></td>
										<td>'.getRegistersConfigSelect($_GET['d'],$reg['register_number'],$reg['register_config']).'</td>
										<td><input name="measurement_factor" class="regfactor" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="8" type="text" value="'.$reg['register_factor'].'"></td>
										<td><a href="dashboard.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['register_number'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  



        <?php  if ($_GET['s'] == 1) { 
							  
				//////////oil					
				//$totals = getTotalsArray($_GET['d'],24);
				//$totalsreg = getTotalsByRegister($_GET['d'],24);
	
				//echo "<pre>";
				//print_r($totalsreg);

							  
		 ?>  
        
        <div id="chart"></div>

          <h2 class="sub-header">Production & Sales</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Oil Production (ft/in)</th>
			      <th>Barrels Produced</th>
				  <th>Oil Sold (ft/in)</th>
				 <th>Barrels Sold</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
					
					if ($_GET['start']) {
						$regdates = date_range($_GET['start'], $_GET['end']);	
					} else {
						$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
					}
					//print_r($regdates);
				$i = 100;
	
				rsort($regdates);
			   
			   		foreach ($regdates as $pdate) {  
						
			   			echo '<tr>
							  <td width="200"><i class="fa fa-plus-square" data-toggle="collapse" data-target="#'.$i.'" aria-hidden="true" title="Show Detail"></i> <a href="dashboard.php?r=1&d=214&s=1">'.getDeviceName($_GET['d'],$_SESSION['user_accountID']).'</a></td>						  
							  <td>'.formatDate($pdate).'</td>
							  <td>'.(getOilProdCalc($pdate,$_SESSION['user_accountID'])/20).'"</td>
							  <td>'.getOilProdCalc($pdate,$_SESSION['user_accountID']).'</td>
							  <td>'.(getOilSoldCalc($pdate,$_SESSION['user_accountID'])/20).'"</td>
							  <td>'.getOilSoldCalc($pdate,$_SESSION['user_accountID']).'</td
							</tr>
							
							
							<tr id="'.$i.'" class="collapse"> <!--WELL COLLAPSE ROW-->
								
								<td colspan="14">  <!--WELL COLLAPSE TD-->
								
										  <table class="table table-striped">
										  <thead>
											<tr class="warning">
											  <th width="200">Tank</th>
											  <th>Date</th>
											  <th>Oil Production (ft/in)</th>
											  <th>Barrels Produced</th>
											  <th>Oil Sold (ft/in)</th>
											 <th>Barrels Sold</th>
											</tr>
										  </thead>
										  <tbody>'; 
										  
											$db = connectTWO("account_47");
											$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Oil - Production and Sales'");
											$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
											foreach ($results as $tank) {
												
												$stmt = $db->query("SELECT * FROM tbl_register_calculations WHERE register_number = ".$tank['register_number']." AND calculation_date = '$pdate' "); 
												
												$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);
												
												if ($tank['register_number'] == 81) {
													
														echo '<tr><td>25-18</td>
															<td>'.formatDate($pdate).'</td>
															<td>'.($readings[0]['production_calculation_in']/20).'</td>
															<td>'.$readings[0]['production_calculation'].'</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															</tr>';	
													
												} else if ($tank['register_number'] == 88) {
													
														echo '<tr><td>45-20</td>
															<td>'.formatDate($pdate).'</td>
															<td>'.($readings[0]['production_calculation_in']/20).'</td>
															<td>'.$readings[0]['production_calculation'].'</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															</tr>';	
												} else {
													
														echo '<tr><td>'.$tank['register_name'].'</td>
															<td>'.formatDate($pdate).'</td>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															<td>'.$readings[0]['sales_calculation_in'].'</td>
															<td>'.$readings[0]['sales_calculation'].'</td>
															</tr>';
													
												}

												
											}

						
									echo	'</tbody>
										 </table>
									
									
								</td>
							</tr>';
						
						$i++;
						
					}
			   
			   ?>
              </tbody>
            </table>
          </div>
          
          
             <div id="chart_modal" title="Chart Types" style="display: none;">
             
             		<!--<img src="images/line_chart.png" class="img-responsive img-rounded"/> -->
             		
             		      <div class="row">
            	
               				 	<div class="col-md-4">
                					
                					<a href="process/set_chart.php?t=1" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Line Chart</div>
										  <div class="panel-body">
											<img src="images/line_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
 
            			        <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=2" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Waterfall Chart</div>
										  <div class="panel-body">
											<img src="images/waterfall_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>
           			  
           			             			  
           			             <div class="col-md-4">
                
                					<a href="process/set_chart.php?t=3" class="nounderline">
										<div class="panel panel-primary">
										  <div class="panel-heading">Bar Chart</div>
										  <div class="panel-body">
											<img src="images/bar_chart.png" class="img-responsive img-rounded"/>
										  </div>
										</div>
           							</a>
            		
							  	</div>           			  
            			  
             			  </div>
             </div>


        <?php  } ?>  

        <?php  if ($_GET['s'] == 2) { 
							 
	
					///////water
					//$totals_water = getTotalsArrayWater($_GET['d'],24);
					//$totalsreg_water = getTotalsByRegisterWater($_GET['d'],24);
	
					if ($_GET['start']) {
						$regdates = date_range($_GET['start'], $_GET['end']);	
					} else {
						$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
					}
				
				
	
				$i = 100;
	
				rsort($regdates);
					
	
	          echo '<h2 class="sub-header">Production & Sales</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Site</th>
                  <th>Date</th>
                  <th>Water Production</th>
                </tr>
              </thead>
              <tbody>';
               
	
				
			   		foreach ($regdates as $pdate) {  
						
			   			echo '<tr>
							  <td width="200"><i class="fa fa-plus-square" data-toggle="collapse" data-target="#'.$i.'" aria-hidden="true" title="Show Detail"></i> <a href="dashboard.php?r=1&d=214&s=1">'.getDeviceName($_GET['d'],$_SESSION['user_accountID']).'</a></td>						  
							  <td>'.formatDate($pdate).'</td>
							  <td>'.getWaterProdCalc($pdate,$_SESSION['user_accountID']).'</td>
							</tr>
							
							
							<tr id="'.$i.'" class="collapse"> <!--WELL COLLAPSE ROW-->
								
								<td colspan="14">  <!--WELL COLLAPSE TD-->
								
										  <table class="table table-striped">
										  <thead>
											<tr class="warning">
											  <th width="200">Tank</th>
											  <th>Date</th>
											  <th>Water Production</th>
											</tr>
										  </thead>
										  <tbody>'; 
										  
											$db = connectTWO("account_47");
											$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Water - Tank Level Feet'");
											$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
						
											foreach ($results as $tank) {
												
												$stmt = $db->query("SELECT * FROM tbl_register_calculations WHERE register_number = ".$tank['register_number']." AND calculation_date = '$pdate' "); 
												
												$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);
												
												echo '<tr><td>'.$tank['register_name'].'</td>
															<td>'.formatDate($pdate).'</td>
															<td>'.$readings[0]['water_production_calculation'].'</td>
															</tr>';
												
											}

						
									echo	'</tbody>
										 </table>
									
									
								</td>
							</tr>';
						
						$i++;
						
					}
			   
							  
			  ?>  
       
       
       
        	             </tbody>
            </table>
          </div>
         
          


        <?php  } ?>  



        <?php  if ($_GET['s'] == 3) { ?>  

       

          <h2 class="sub-header">Well Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                <th>Register</th>
                  <th>Today Volume</th>
                </tr>
              </thead>
              <tbody>
               
               <?php 
					$db = connectTWO("account_47");
								$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Gas - Well Meter'");
								$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

								foreach ($results as $tank) {


									$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = ".$tank['register_number']." ORDER BY register_date DESC LIMIT 1 "); 

									$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);

									echo '<tr><td>'.$tank['register_name'].'</td>
												<td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$tank['register_number'].'">'.$readings[0]['register_reading'].'</a></td>
												</tr>';

								}	
			   
			   ?>
              </tbody>
            </table>
          </div>

          <h2 class="sub-header">Check Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                <th>Register</th>
                  <th>Today Volume</th>
                </tr>
              </thead>
              <tbody>
               <?php 
			   
					$db = connectTWO("account_47");
								$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Gas - Check Meter'");
								$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

								foreach ($results as $tank) {


									$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = ".$tank['register_number']." ORDER BY register_date DESC LIMIT 1 "); 

									$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);

									echo '<tr><td>'.$tank['register_name'].'</td>
												<td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$tank['register_number'].'">'.$readings[0]['register_reading'].'</a></td>
												</tr>';

								}	
			   
			   ?>
              </tbody>
            </table>
           </div>

          <h2 class="sub-header">Sales Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                <th>Register</th>
                  <th>Today Volume</th>
                </tr>
              </thead>
              <tbody>
               <?php 
			   
					$db = connectTWO("account_47");
								$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Gas - Sales Meter'");
								$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

								foreach ($results as $tank) {


									$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = ".$tank['register_number']." ORDER BY register_date DESC LIMIT 1 "); 

									$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);

									echo '<tr><td>'.$tank['register_name'].'</td>
												<td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$tank['register_number'].'">'.$readings[0]['register_reading'].'</a></td>
												</tr>';

								}		   
			   ?>
              </tbody>
            </table>
		</div>

          <h2 class="sub-header">Flare Meter</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Register</th>
                  <th>Today Volume</th>
                </tr>
              </thead>
              <tbody>
               <?php 
			   
								$db = connectTWO("account_47");
								$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Gas - Flare Meter'");
								$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

								foreach ($results as $tank) {


									$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = ".$tank['register_number']." ORDER BY register_date DESC LIMIT 1 "); 

									$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);

									echo '<tr><td>'.$tank['register_name'].'</td>
												<td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$tank['register_number'].'">'.$readings[0]['register_reading'].'</a></td>
												</tr>';

								}			   
			   ?>
              </tbody>
            </table>
          </div>
          
          
             <div id="chart_modal" title="Chart Types" style="display: none;">
							   
							   
  </div>

        <?php  } ?>  



        <?php  if ($_GET['s'] == 4) { 
							  
				if ($_GET['start']) {
					$regdates = date_range($_GET['start'], $_GET['end']);	
				} else {
					$regdates = date_range(date('Y-m-d', strtotime("-7 days")), date("Y-m-d"));	
				}
				
				
	
				rsort($regdates);
							  
				//$pressures = getLastReadingsArrayPressure($_GET['d'],24); 
				//echo "<pre>";
				//print_r($regdates);
							  
		 ?>  
		
		

	


          <h2 class="sub-header">Pressures</h2>
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Register</th>
                  <th>Last PSI</th>

                </tr>
              </thead>
              <tbody>
               
               <?php 
	
	
								$db = connectTWO("account_47");
								$stmt = $db->query("SELECT * FROM tbl_registers WHERE register_config = 'Pressure'");
								$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

								foreach ($results as $tank) {


									$stmt = $db->query("SELECT * FROM tbl_register_history WHERE register_number = ".$tank['register_number']." ORDER BY register_date DESC LIMIT 1 "); 

									$readings = $stmt->fetchAll(PDO::FETCH_ASSOC);

									echo '<tr><td>'.$tank['register_name'].'</td>
												<td><a href="dashboard.php?r=1&d='.$_GET['d'].'&c=1&g='.$tank['register_number'].'">'.$readings[0]['register_reading'].'</a></td>
												</tr>';

								}


 
			   ?>
              </tbody>
            </table>
          </div>
          

        <?php  } ?>  
        
        
        <?php  if ($_GET['s'] == 5) { 
		  
	
						////////OIL TANKS
						//16 foot tanks, readings are in inches, multiply by  0.0833333333 to get feet, then divide by 16 to get % of tank filled
						$oiltank1oillevel = round((getLastReading(249,51,47) * 0.0833333333)/16,2);
						$oiltank1waterlevel = round((getLastReading(249,100,47) * 0.0833333333)/16,2);
						$oiltank1oillevel = $oiltank1oillevel - $oiltank1waterlevel;
						
						$oiltank2oillevel = round((getLastReading(249,53,47) * 0.0833333333)/16,2);
						$oiltank2waterlevel = round((getLastReading(249,101,47) * 0.0833333333)/16,2);
						$oiltank2oillevel = $oiltank2oillevel - $oiltank2waterlevel;
	
						$oiltank3oillevel = round((getLastReading(249,55,47) * 0.0833333333)/16,2);
						$oiltank3waterlevel = round((getLastReading(249,102,47) * 0.0833333333)/16,2);
						$oiltank3oillevel = $oiltank3oillevel - $oiltank3waterlevel;

						$oiltank4oillevel = round((getLastReading(249,57,47) * 0.0833333333)/16,2);
						$oiltank4waterlevel = round((getLastReading(249,103,47) * 0.0833333333)/16,2);
						$oiltank4oillevel = $oiltank4oillevel - $oiltank4waterlevel;
	
	
						////////GUNBARREL TANK
						//30 foot tanks, readings are in inches, multiply by  0.0833333333 to get feet, then divide by 30 to get % of tank filled
						$oiltankGBoillevel = round((getLastReading(249,67,47) * 0.0833333333)/30,2);
						$oiltankGBwaterlevel = round((getLastReading(249,108,47) * 0.0833333333)/30,2);
						$oiltankGBoillevel = $oiltankGBoillevel - $oiltankGBwaterlevel;

	
						////////TEST TANK
						//16 foot tanks, readings are in inches, multiply by  0.0833333333 to get feet, then divide by 16 to get % of tank filled
						$oiltankBWoillevel = round((getLastReading(249,69,47) * 0.0833333333)/16,2);
						$oiltankBWwaterlevel = round((getLastReading(249,109,47) * 0.0833333333)/16,2);
						$oiltankBWoillevel = $oiltankBWoillevel - $oiltankBWwaterlevel;
						
	
						////////WATER TANKS
						//24 foot tanks, readings are in inches, multiply by  0.0833333333 to get feet, then divide by 24 to get % of tank filled
						$watertank1waterlevel = round((getLastReading(249,59,47) * 0.0833333333)/24,2);
						$watertank2waterlevel = round((getLastReading(249,61,47) * 0.0833333333)/24,2);
						$watertank3waterlevel = round((getLastReading(249,63,47) * 0.0833333333)/24,2);
						$watertank4waterlevel = round((getLastReading(249,65,47) * 0.0833333333)/24,2);

		  
		  ?>
	
					<div class="row">
					    <br>
						<h2 class="sub-header">Oil Tanks</h2>


						<div class="col-md-2">
                            <a href="dashboard.php?r=1&d=249&c=1&g=51">
								<div class="cylinder-wrapper">
									<div class="cylinder-value1" id="1" data-value="<?php echo $oiltank1waterlevel * 100; ?>"></div>
									<div class="cylinder-value2" id="2" data-value="<?php echo $oiltank1oillevel * 100; ?>"></div>
								</div>
                            </a>
							<h4 class="text-center">Oil Tank 1</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#79d279"></span>
                                <?php echo getLastReading(249,51,47) - getLastReading(249,100,47); ?>
                            </h5>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,100,47); ?>
                            </h5>
						</div>

						<div class="col-md-2">
                            <a href="dashboard.php?r=1&d=249&c=1&g=53">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="3" data-value="<?php echo $oiltank2waterlevel * 100; ?>"></div>
                                    <div class="cylinder-value2" id="4" data-value="<?php echo $oiltank2oillevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Oil Tank 2</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#79d279"></span>
                                <?php echo getLastReading(249,53,47) - getLastReading(249,101,47); ?>
                            </h5>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,101,47); ?>
                            </h5>
						</div>

						<div class="col-md-2">
                            <a href="dashboard.php?r=1&d=249&c=1&g=55">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="5" data-value="<?php echo $oiltank3waterlevel * 100; ?>"></div>
                                    <div class="cylinder-value2" id="6" data-value="<?php echo $oiltank3oillevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Oil Tank 3</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#79d279"></span>
                                <?php echo getLastReading(249,55,47) - getLastReading(249,102,47); ?>
                            </h5>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,102,47); ?>
                            </h5>
						</div>

						<div class="col-md-2">
                            <a href="dashboard.php?r=1&d=249&c=1&g=57">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="7" data-value="<?php echo $oiltank4waterlevel * 100; ?>"></div>
                                    <div class="cylinder-value2" id="8" data-value="<?php echo $oiltank4oillevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Oil Tank 4</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#79d279"></span>
                                <?php echo getLastReading(249,57,47) - getLastReading(249,103,47); ?>
                            </h5>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,103,47); ?>
                            </h5>
						</div>
						
						<div class="col-md-2">
                            <a href="dashboard.php?r=1&d=249&c=1&g=67">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="9" data-value="<?php echo $oiltankGBwaterlevel * 100; ?>"></div>
                                    <div class="cylinder-value2" id="10" data-value="<?php echo $oiltankGBoillevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Gunbarrel Tank</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#79d279"></span>
                                <?php echo getLastReading(249,67,47) - getLastReading(249,108,47); ?>
                            </h5>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,108,47); ?>
                            </h5>
						</div>
						
						<div class="col-md-2">
                            <a href="dashboard.php?r=1&d=249&c=1&g=69">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="11" data-value="<?php echo $oiltankBWwaterlevel * 100; ?>"></div>
                                    <div class="cylinder-value2" id="12" data-value="<?php echo $oiltankBWoillevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Test Tank</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#79d279"></span>
                                <?php echo getLastReading(249,69,47) - getLastReading(249,109,47); ?>
                            </h5>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,109,47); ?>
                            </h5>
						</div>
						
						
		 			 </div>
		 			 
		 			 
		 			 <div class="row">
				        <br> 			
		 				<h2 class="sub-header">Water Tanks</h2>
		 				
		 				<div class="col-md-3">
                            <a href="dashboard.php?r=1&d=249&c=1&g=59">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="13" data-value="<?php echo $watertank1waterlevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Water Tank 1</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,59,47); ?>
                            </h5>
						</div>

						<div class="col-md-3">
                            <a href="dashboard.php?r=1&d=249&c=1&g=61">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="14" data-value="<?php echo $watertank2waterlevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Water Tank 2</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,61,47); ?>
                            </h5>
						</div>

						<div class="col-md-3">
                            <a href="dashboard.php?r=1&d=249&c=1&g=63">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="15" data-value="<?php echo $watertank3waterlevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Water Tank 3</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,63,47); ?>
                            </h5>
						</div>

						<div class="col-md-3">
                            <a href="dashboard.php?r=1&d=249&c=1&g=65">
                                <div class="cylinder-wrapper">
                                    <div class="cylinder-value1" id="16" data-value="<?php echo $watertank4waterlevel * 100; ?>"></div>
                                </div>
                            </a>
                            <h4 class="text-center">Water Tank 4</h4>
                            <h5 class="text-center">
                                <span class="glyphicon glyphicon-tint" style="color:#002db3"></span>
                                <?php echo getLastReading(249,65,47); ?>
                            </h5>
						</div>
	
					</div>
	       
		       <?php  } ?>  
      

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript" src="http://www.amcharts.com/lib/3/amcharts.js"></script>
	<script type="text/javascript" src="http://www.amcharts.com/lib/3/serial.js"></script>
	<script type="text/javascript" src="http://www.amcharts.com/lib/3/themes/light.js"></script>


    <?php  if ($_GET['f']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if ($_GET['s'] or $_GET['g'] or !isset($_GET['d'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(29, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s'] ) { ?> 
					   
					   		window.location.replace("dashboard.php?r=1&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	<script>
		
					   		
		
 					   		$( "#newlink" ).click(function() {	

								$( "#add_modal" ).dialog( "open" );	
							});

							$( "#newlinkEXTRA" ).click(function() {	

								$( "#add_modalEXTRA" ).dialog( "open" );	
							});

							$( "#newlink2" ).click(function() {	

								$( "#add_modalWIDE" ).dialog( "open" );	
							});


							$( "#add_modal" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalEXTRA" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 650,
							  modal: true,

							});	

							$( "#add_modalWIDE" ).dialog({
							  autoOpen: false,
							  height: 'auto',
							  width: 950,
							  modal: true,

							});
			
		</script>
 	
 	
 	<?php  if (!$_GET['m'] && !$_GET['c'] && !$_GET['b'] && !$_GET['f'] ) { ?>  
   
   	<?php  if ($_GET['s'] != 4) { ?>  
   	
    	<script>
               $(function () {
				   	
				   
				   <?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					   
					        chart: {
								type: 'line',
								zoomType: 'x',

							},
					   
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
																								 
																								 
									if ($_GET['g']) {
										
										echo ' - Battery #'.$_GET['g'];
									}														 		
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center,
						
                    },

                    xAxis: {
                        type: 'datetime',
						dateTimeLabelFormats: {
						   day: '%b %d %Y'    //ex- 01 Jan 2016
						}
						
                    },
                    yAxis: {
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('../application/account_47/custom/time_series_oil_prod_47.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, {
                        name: 'Oil Sales',
                        data: <?php include('../application/account_47/custom/time_series_oil_sales_47.php'); ?>,
						color: '#79d288',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, {
                        name: 'Water Production',
                        data: <?php include('../application/account_47/custom/time_series_water_47.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, {
                        name: 'Gas Volume',
                        data: <?php include('../application/account_47/custom/time_series_gas_volume_47.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' MCF'
						 }
                   }]
                });
				   
				<?php  } ?>  
	
				 
				   <?php  if ($_SESSION['chart_type'] == 2 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'waterfall',
								zoomType: 'x',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   

                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					series: [{
                        name: 'Oil Production',
                        data: <?php include('../application/account_47/custom/time_series_oil_prod_47.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, {
                        name: 'Oil Sales',
                        data: <?php include('../application/account_47/custom/time_series_oil_sales_47.php'); ?>,
						color: '#79d288',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, {
                        name: 'Water Production',
                        data: <?php include('../application/account_47/custom/time_series_water_47.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, {
                        name: 'Gas Volume',
                        data: <?php include('../application/account_47/custom/time_series_gas_volume_47.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' MCF'
						 }
                   }]
                });
				   
				<?php  } ?>  
			   				   
	
				   
				 <?php  if ($_SESSION['chart_type'] == 3 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'bar',
									zoomType: 'x',
							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d'],24);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
    
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
					series: [{
                        name: 'Oil Production',
                        data: <?php include('../application/account_47/custom/time_series_oil_prod_47.php'); ?>,
						color: '#79d279',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, {
                        name: 'Oil Sales',
                        data: <?php include('../application/account_47/custom/time_series_oil_sales_47.php'); ?>,
						color: '#79d288',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                    }, {
                        name: 'Water Production',
                        data: <?php include('../application/account_47/custom/time_series_water_47.php'); ?>,
						color: '#002db3',
						tooltip: {
							valueSuffix: ' BBL'
						 }
                   }, {
                        name: 'Gas Volume',
                        data: <?php include('../application/account_47/custom/time_series_gas_volume_47.php'); ?>,
						color: '#C00',
						tooltip: {
							valueSuffix: ' MCF'
						 }
                   }]
                });
				   
				<?php  } ?>  
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
		
		<?php  if ($_GET['s'] == 5) { ?> 
		
		<script>
            
                for(var i=1;i<=12;i++){
                    var id1 = '#'+i;
                    var value1 = $(id1).data("value");
                    value1 = value1 + '%';
                    $(id1).css("height",value1);

                    i++;
                    var id2 = '#'+i;
                    var value2 = $(id2).data("value");
                    value2 = value2 + '%';
                    $(id2).css("height",value2);
                    $(id2).css("bottom",value1);
                }
                
                for(var i=13;i<=16;i++){
                    var id1 = '#'+i;
                    var value1 = $(id1).data("value");
                    value1 = value1 + '%';
                    $(id1).css("height",value1);
                }
		</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
   
    

		<?php include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

<?php

include("functions/functions.php");

$db = connectTwo("account_43");
$stmt=$db->query("SELECT * FROM tbl_modbus_mapping WHERE map_friendly_name IS NOT NULL");
$results=$stmt->fetchAll(PDO::FETCH_ASSOC);

foreach($results as $row){
    
    $friendly = $row['map_friendly_name'];
    $friendly = explode("_",$friendly);

    $obj = $friendly[2];
    $dev = $friendly[3];
    $tag = $friendly[5];
    $map = $row['mapID'];
    
    $stmt=$db->prepare("UPDATE tbl_modbus_mapping SET map_object_name = ?, map_device_name = ?, map_tag_name = ? WHERE mapID = ?");
    $stmt->execute(array($obj,$dev,$tag,$map));
}

?>
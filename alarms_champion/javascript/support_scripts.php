<script>

	$(function() {
			

           $("#table").DataTable({
			   "pageLength": 25
		   });
        
           $("#tableA").DataTable({
			   "pageLength": 25,
               "order": [[ 2, "desc" ]]
		   });


				
			$( "#newlink" ).click(function() {	
					
					$( "#add_modal" ).dialog( "open" );	
				});
				
				$( "#newlinkEXTRA" ).click(function() {	
					
					$( "#add_modalEXTRA" ).dialog( "open" );	
				});
				
				$( "#newlink2" ).click(function() {	
					
					$( "#add_modalWIDE" ).dialog( "open" );	
				});


				$( "#add_modal" ).dialog({
				  autoOpen: false,
				  height: 'auto',
				  width: 650,
				  modal: true,

				});	
				
				$( "#add_modalEXTRA" ).dialog({
				  autoOpen: false,
				  height: 'auto',
				  width: 650,
				  modal: true,

				});	
				
				$( "#add_modalWIDE" ).dialog({
				  autoOpen: false,
				  height: 'auto',
				  width: 950,
				  modal: true,

				});	
			
		});
</script>
<?php 
session_start();



$locations = getLocationsGeo($_SESSION['accountID']); 
?>
<script>
 var myLatLng = {lat: <?php echo $locations[10]['latitude']; ?>, lng: <?php echo $locations[10]['longitude']; ?>};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: myLatLng
  });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
			'<p><img src="images/logo.jpg"></p>'+
            '<h1 id="firstHeading" class="firstHeading"><?php echo $locations[10]['name']; ?></h1>'+
            '<div id="bodyContent">'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
	title: '<?php echo $locations[10]['name']; ?>'
  });


 marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

</script>
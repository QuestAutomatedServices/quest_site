<script>
$(function() {

	<?php if(isset($_SESSION['editArray'])){?>	
		
		$( "#edit_setting_modal" ).dialog({
		  autoOpen: true,
		  height: 'auto',
		  width: 400,
		  modal: true,

		});
		<?php unset($_SESSION['editArray']); ?>
	<?php }?>


	$('.fa-question-circle').tooltip(); 
	
	$( ".user-consult" ).click(function() {
		var controller_id = $(this).data().id;
		var address_type = $(this).data().type;
		var labelAddress = '';
		var labelPort = '';
		
		if (address_type == 'URL') {
			labelAddress = 'URL Address';
		} else if (address_type == 'Modbus') {
			labelAddress = 'Modbus ID';
		} else if (address_type == 'COM') {
			labelAddress = 'ID';
		} else {
			labelAddress = 'IP Address';
		}
		
		if (address_type == 'Modbus') {
			$( ".labelPort" ).hide( );
			$( ".portRO" ).hide( );
			$( ".hiddenBr" ).hide( );			
		} else if (address_type == 'COM')  {
			labelPort = 'Communication Port';
			$( ".labelPort" ).show( );
			$( ".portRO" ).show( );
			$( ".hiddenBr" ).show( );
		}	else {
			labelPort = 'Port Number';
			$( ".labelPort" ).show( );
			$( ".portRO" ).show( );
			$( ".hiddenBr" ).show( );
		}
		
		$( ".modal-title" ).html( address_type );
		$( ".labelAddress" ).html( labelAddress );
		$( ".labelPort" ).html( labelPort );
		
			$.ajax	 ({
					 type: "GET",
					 url: "process/controller_ip_address.php",
					 data: "id="+controller_id+"&type="+address_type,
					 cache: false,
				 	 success: function(html) {

							$( ".ip_addressRO" ).html(html);					
						
					}	
			});
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/controller_port.php",
					 data: "id="+controller_id+"&type="+address_type,
					 cache: false,
				 	 success: function(html) {

							$( ".portRO" ).html(html);					
						
					}	
			});
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/controller_serial.php",
					 data: "id="+controller_id+"&type="+address_type,
					 cache: false,
				 	 success: function(html) {

							$( ".serialCommunications" ).html(html);					
						
					}	
			});
			
			$( "#IPv4" ).dialog( "open" );

					
	});
	
	
	
	$( ".addSetting" ).click(function() {
		
		var cfgnum = $(this).data().cfg;
		var typenum = $(this).data().type;
		var devtype = $(this).data().devtype;
		
			
		$( "#paramc" ).val( cfgnum );
		$( "#paramt" ).val( typenum );
		
		if (typenum == 1) {
			$( "#devTypeLabel" ).html('Measurement');
			$(".hideBox").hide();
		} else if (typenum == 2) {
			$( "#devTypeLabel" ).html('Execution');
			$(".hideBox").hide();			
		} else if (typenum == 3) {
			$( "#devTypeLabel" ).html('Trigger');
			$(".hideBox").hide();
		} else if (typenum == 4) {
			$( "#devTypeLabel" ).html('Notification');
			$(".hideBox").hide();
		} else if (typenum == 5) {
			$( "#devTypeLabel" ).html('Function');
			$(".hideBox").show();
		}
		
		$( "#cfnNumLabel" ).html(cfgnum);	
		
			$.ajax	 ({
					 type: "GET",
					 url: "process/parameter_tag.php",
					 data: "id="+devtype+"&pt="+typenum,
					 cache: false,
				 	 success: function(html) {
						 
						 if (html == 'error') {
							$( "#tagTypeSelect" ).html('<p class="text-danger">There are no Tags for this Device Type!</p>');
							 $( ".noShowError" ).hide();
						 } else {
							$( "#tagTypeSelect" ).html(html);	 
						 }
						 

						
					}	
			});
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/structures.php",
					 data: "id="+devtype+"&pt="+typenum,
					 cache: false,
				 	 success: function(html) {
						 
						
						$( "#structureSelect" ).html(html);	 
						 
						 

						
					}	
			});
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/datatypes.php",
					 data: "id="+devtype+"&pt="+typenum,
					 cache: false,
				 	 success: function(html) {
						 
						
						$( "#dataTypeSelect" ).html(html);	 
						 
						 

						
					}	
			});
		
		$( "#add_setting_modal" ).dialog( "open" );	
			
	});
	


	$( "#add_setting_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 400,
	  modal: true,
	 
	});
	
	$('#deleteSetting').click(function(){
		$('#add_setting_modal').dialog("dismiss");
	});
	
	$( ".meacheck" ).change(function() {
		
		var id=$(this).val();
		
		//alert(id);
		
		if(this.checked) // if changed state is "CHECKED"
		{
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&s=1",
					 cache: false,
				 	 success: function(data) {

						alert('Added to Configuration');
					}	
			});
			
		} else {
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&r=1",
					 cache: false,
				 	 success: function(data) {

						alert('Removed from Configuration');
					}	
			});
			
		}
		
			
	});

	
	
	$( ".exccheck" ).change(function() {
		
		var id=$(this).val();
		
		//alert(id);
		
		if(this.checked) // if changed state is "CHECKED"
		{
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&s=2",
					 cache: false,
				 	 success: function(data) {

						alert('Added to Configuration');
					}	
			});
			
		} else {
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&r=2",
					 cache: false,
				 	 success: function(data) {

						alert('Removed from Configuration');
					}	
			});
			
		}
		
			
	});

	
	
	$( ".trgcheck" ).change(function() {
		
		var id=$(this).val();
		
		//alert(id);
		
		if(this.checked) // if changed state is "CHECKED"
		{
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&s=3",
					 cache: false,
				 	 success: function(data) {

						alert('Added to Configuration');
					}	
			});
			
		} else {
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&r=3",
					 cache: false,
				 	 success: function(data) {

						alert('Removed from Configuration');
					}	
			});
			
		}
		
			
	});
	

		
	$( ".notcheck" ).change(function() {
		
		var id=$(this).val();
		
		//alert(id);
		
		if(this.checked) // if changed state is "CHECKED"
		{
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&s=4",
					 cache: false,
				 	 success: function(data) {

						alert('Added to Configuration');
					}	
			});
			
		} else {
			
			$.ajax	 ({
					 type: "GET",
					 url: "process/device_settings.php",
					 data: "id="+id+"&r=4",
					 cache: false,
				 	 success: function(data) {

						alert('Removed from Configuration');
					}	
			});
			
		}
		
			
	});
	
	
	
	$( "#controllerdevice" ).change(function() {
		
			var id=$(this).val();
		
			$.ajax	 ({
					 type: "GET",
					 url: "process/controller_object.php",
					 data: "id="+id,
					 cache: false,
				 	 success: function(html) {

						$( "#controllerObjectSelect" ).html(html);	
					}	
			});
		
		
		$( "#objsel" ).show();		
			
	});

	
	$( "#objsel" ).change(function() {

		$( "#objdevsel" ).show();		
			
	});
	
	
	$( "#device_select" ).change(function() {

		$( "#devicebtn" ).show();		
			
	});	
	
	
	
	$( ".fa-link, .fa-chain-broken" ).click(function() {	
		
		var id = $(arguments[0].target).attr("data-id");
		var type = $(arguments[0].target).attr("data-var");
		var site = $(arguments[0].target).attr("data-site");
		var uid = $(arguments[0].target).attr("data-uid");
		var ctr = $(arguments[0].target).attr("data-ct");
		var obj = $(arguments[0].target).attr("data-obj");

		$( "#link_header" ).html($(arguments[0].target).attr("data-label"));
		$( "#link_dev_type" ).html($(arguments[0].target).attr("data-type"));
				
					$.ajax	 ({
					 type: "GET",
					 url: "process/device_config.php",
					 data: "id="+id+"&type="+type+"&site="+site+"&uid="+uid+"&ctr="+ctr+"&obj="+obj,
					 cache: false,
				 	 success: function(html) {

						$( "#link_config" ).html(html);	
					}	
						
			});
		
		$( "#add_link_modal" ).dialog( "open" );	
			
	});
	
	var winW = $(window).width() * 0.8;
	var winH = $(window).height() - 180;

	$( "#add_link_modal" ).dialog({
	  autoOpen: false,
	  height: winH,
	  width: winW,
	  modal: true,
	 
	});
	
	
	
	$( "#add_controller" ).click(function() {	
		
	
		$( "#add_con_modal" ).dialog( "open" );	
			
	});


	$( "#add_con_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});
	
	
	$( "#add_object" ).click(function() {	
		$( "#add_obj_modal" ).dialog( "open" );	
			
	});


	$( "#add_obj_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});

	
	$( "#add_device" ).click(function() {	
		$( "#add_dev_modal" ).dialog( "open" );	
			
	});


	$( "#add_dev_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
  	  close: function(){
		  
		  	$( "#controllerdevice" ).val('');
      		$( "#controllerObjectSelect" ).html('');	
        }
	 
	});
	
	
	$( "#add_measurement" ).click(function() {	
		$( "#add_measurement_modal" ).dialog( "open" );	
			
	});


	$( "#add_measurement_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});

		
	$( "#add_execution" ).click(function() {	
		$( "#add_execution_modal" ).dialog( "open" );	
			
	});


	$( "#add_execution_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});
	
		
	$( "#add_trigger" ).click(function() {	
		$( "#add_trigger_modal" ).dialog( "open" );	
			
	});


	$( "#add_trigger_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});
	
		
	$( "#add_notification" ).click(function() {	
		$( "#add_notification_modal" ).dialog( "open" );	
			
	});


	$( "#add_notification_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});

	
	$( "#add_parameter" ).click(function() {	
		$( "#add_parameter_modal" ).dialog( "open" );	
			
	});


	$( "#add_parameter_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});
	
	
	$( "#acctselect" ).change(function() {	
	
			var id=$(this).val();


			 $.ajax	 ({
					 type: "GET",
					 url: "process/set_account.php",
					 data: "id="+id,
					 cache: false,
				 	 success: function(data) {

						location.reload();
					}	
			});
	

	});
	
	

	$("#treeview").shieldTreeView();



});
</script>
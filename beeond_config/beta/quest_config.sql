/*
Navicat MySQL Data Transfer

Source Server         : Quest RDS
Source Server Version : 50635
Source Host           : questmysqlinstance.cwspgv68diat.us-east-1.rds.amazonaws.com:3306
Source Database       : quest_config

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-03-15 13:56:32
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `account_num` varchar(45) DEFAULT NULL,
  `super_account` tinyint(1) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_accounts_1_idx` (`created_by`),
  KEY `modified` (`modified_by`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO accounts VALUES ('1', 'Quest', '', '1', '1', '2016-09-04 01:09:47', '1', '2016-11-18 19:44:02', '1');
INSERT INTO accounts VALUES ('3', 'Fred\'s Drilling', '123456', '0', '1', '0000-00-00 00:00:00', '1', '2016-11-15 07:27:31', '1');
INSERT INTO accounts VALUES ('5', 'Omni Consolidated', '12121234', '0', '1', '2016-09-08 23:54:00', '1', '2016-11-07 22:20:51', '1');
INSERT INTO accounts VALUES ('8', 'Fred123', '12345', '0', '1', '2016-09-26 13:37:38', '1', '2016-11-15 07:27:47', '1');
INSERT INTO accounts VALUES ('9', 'Jackson\'s Drilling Co.', '1234564', '0', '1', '2016-11-07 16:21:38', '1', '2016-11-07 22:22:16', '1');
INSERT INTO accounts VALUES ('10', 'newTEST', '123123', '0', '1', '0000-00-00 00:00:00', '0', null, null);
INSERT INTO accounts VALUES ('11', 'newTEST22', '123123', '1', '1', '2018-02-12 21:34:51', '0', null, null);

-- ----------------------------
-- Table structure for `actions`
-- ----------------------------
DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of actions
-- ----------------------------
INSERT INTO actions VALUES ('1', 'OverfillShutdown', 'overfillshutdow', '', '1', '2016-11-16 21:11:55', '1', '2017-01-28 19:58:41', '1');
INSERT INTO actions VALUES ('2', 'HighStop', 'highsto', '', '1', '2016-11-16 23:39:32', '1', '2017-01-28 19:59:01', '1');
INSERT INTO actions VALUES ('3', 'LowStart', 'lowstar', '', '1', '2016-11-16 23:40:14', '1', '2017-01-28 19:59:11', '1');
INSERT INTO actions VALUES ('4', 'LowLevelShutdown', 'lowlevelshutdow', '', '1', '2016-11-16 23:40:42', '1', '2017-01-28 19:59:25', '1');

-- ----------------------------
-- Table structure for `action_tags`
-- ----------------------------
DROP TABLE IF EXISTS `action_tags`;
CREATE TABLE `action_tags` (
  `action_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`action_id`,`tag_id`),
  KEY `fk_action_tags_2_idx` (`tag_id`),
  CONSTRAINT `fk_action_tags_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_action_tags_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of action_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `activity_log`
-- ----------------------------
DROP TABLE IF EXISTS `activity_log`;
CREATE TABLE `activity_log` (
  `user_id` int(10) unsigned NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`,`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of activity_log
-- ----------------------------
INSERT INTO activity_log VALUES ('0', '2016-10-06 01:52:40');
INSERT INTO activity_log VALUES ('1', '2017-02-09 02:11:27');
INSERT INTO activity_log VALUES ('36', '2016-11-21 18:57:37');
INSERT INTO activity_log VALUES ('47', '2016-11-18 20:49:59');

-- ----------------------------
-- Table structure for `admin_log`
-- ----------------------------
DROP TABLE IF EXISTS `admin_log`;
CREATE TABLE `admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL,
  `object_name` varchar(45) NOT NULL,
  `table` varchar(45) DEFAULT NULL,
  `foreign_id` varchar(45) DEFAULT NULL,
  `action` varchar(20) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`object_name`),
  KEY `fk_admin_log_1_idx` (`account_id`),
  KEY `fk_admin_log_2_idx` (`created_by`),
  CONSTRAINT `fk_admin_log_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2560 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of admin_log
-- ----------------------------
INSERT INTO admin_log VALUES ('748', '1', 'Allen Bradley 1765', 'devices', '10', 'UPDATE', 'Device - Allen Bradley 1765 modified', '2016-10-18 21:48:53', '1');
INSERT INTO admin_log VALUES ('749', '1', 'Allen Bradley 1765', 'devices', '1', 'INSERT', 'Allen Bradley 1765 configured with Pressure', '2016-10-18 21:48:53', '1');
INSERT INTO admin_log VALUES ('750', '1', 'Allen Bradley 1765', 'devices', '8', 'INSERT', 'Allen Bradley 1765 configured with Savy', '2016-10-18 21:48:53', '1');
INSERT INTO admin_log VALUES ('751', '1', 'Allen Bradley 1765', 'devices', '10', 'INSERT', 'Allen Bradley 1765 configured with Critical Mass', '2016-10-18 21:48:53', '1');
INSERT INTO admin_log VALUES ('752', '5', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-18 22:27:34', '1');
INSERT INTO admin_log VALUES ('753', '5', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-18 22:28:39', '1');
INSERT INTO admin_log VALUES ('754', '5', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-18 22:28:59', '1');
INSERT INTO admin_log VALUES ('755', '5', '15\' Oil Tank', 'devices', '1', 'INSERT', '15\' Oil Tank configured with Pressuretag', '2016-10-18 22:28:59', '1');
INSERT INTO admin_log VALUES ('756', '5', '15\' Oil Tank', 'devices', '5', 'INSERT', '15\' Oil Tank configured with Davidtag', '2016-10-18 22:28:59', '1');
INSERT INTO admin_log VALUES ('757', '5', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-18 22:29:11', '1');
INSERT INTO admin_log VALUES ('758', '5', '15\' Oil Tank', 'devices', '1', 'INSERT', '15\' Oil Tank configured with Pressuretag', '2016-10-18 22:29:11', '1');
INSERT INTO admin_log VALUES ('759', '5', '15\' Oil Tank', 'devices', '3', 'INSERT', '15\' Oil Tank configured with Tank Leveltag', '2016-10-18 22:29:11', '1');
INSERT INTO admin_log VALUES ('760', '5', '15\' Oil Tank', 'devices', '5', 'INSERT', '15\' Oil Tank configured with Davidtag', '2016-10-18 22:29:11', '1');
INSERT INTO admin_log VALUES ('761', '5', '15\' Oil Tank', 'devices', '6', 'INSERT', '15\' Oil Tank configured with Beccatag', '2016-10-18 22:29:11', '1');
INSERT INTO admin_log VALUES ('762', '5', '20\' Water Tank', 'devices', '5', 'UPDATE', 'Device - 20\' Water Tank modified', '2016-10-18 22:29:58', '1');
INSERT INTO admin_log VALUES ('763', '5', '20\' Water Tank', 'devices', '1', 'INSERT', '20\' Water Tank configured with Pressuretag', '2016-10-18 22:29:58', '1');
INSERT INTO admin_log VALUES ('764', '5', '20\' Water Tank', 'devices', '3', 'INSERT', '20\' Water Tank configured with Tank Leveltag', '2016-10-18 22:29:58', '1');
INSERT INTO admin_log VALUES ('765', '5', 'Fred\'s Site', 'sites', '30', 'INSERT', 'New site - Fred\'s Site added', '2016-10-18 22:55:43', '1');
INSERT INTO admin_log VALUES ('766', '5', 'Fred\'s Site', 'sites', '30', 'UPDATE', 'Site - Fred\'s Site modified', '2016-10-18 22:56:04', '1');
INSERT INTO admin_log VALUES ('767', '5', 'Becca\'s Bonaza', 'sites', '16', 'UPDATE', 'Site - Becca\'s Bonaza modified', '2016-10-18 23:03:40', '1');
INSERT INTO admin_log VALUES ('768', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 14:58:56', '1');
INSERT INTO admin_log VALUES ('769', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 14:59:28', '1');
INSERT INTO admin_log VALUES ('770', '1', '15\' Oil Tank', 'devices', '1', 'INSERT', '15\' Oil Tank configured with Pressuretag', '2016-10-19 14:59:28', '1');
INSERT INTO admin_log VALUES ('771', '1', '15\' Oil Tank', 'devices', '3', 'INSERT', '15\' Oil Tank configured with Tank Leveltag', '2016-10-19 14:59:29', '1');
INSERT INTO admin_log VALUES ('772', '1', '15\' Oil Tank', 'devices', '4', 'INSERT', '15\' Oil Tank configured with Bettytag', '2016-10-19 14:59:29', '1');
INSERT INTO admin_log VALUES ('773', '1', '15\' Oil Tank', 'devices', '5', 'INSERT', '15\' Oil Tank configured with Davidtag', '2016-10-19 14:59:29', '1');
INSERT INTO admin_log VALUES ('774', '1', '15\' Oil Tank', 'devices', '6', 'INSERT', '15\' Oil Tank configured with Beccatag', '2016-10-19 14:59:29', '1');
INSERT INTO admin_log VALUES ('775', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 14:59:44', '1');
INSERT INTO admin_log VALUES ('776', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 14:59:49', '1');
INSERT INTO admin_log VALUES ('777', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 14:59:57', '1');
INSERT INTO admin_log VALUES ('778', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 15:00:46', '1');
INSERT INTO admin_log VALUES ('779', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 15:01:40', '1');
INSERT INTO admin_log VALUES ('780', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 15:02:05', '1');
INSERT INTO admin_log VALUES ('781', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 15:02:51', '1');
INSERT INTO admin_log VALUES ('782', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-19 15:03:01', '1');
INSERT INTO admin_log VALUES ('783', '1', '15\' Oil Tank', 'devices', '1', 'INSERT', '15\' Oil Tank configured with Pressuretag', '2016-10-19 15:03:02', '1');
INSERT INTO admin_log VALUES ('784', '1', '15\' Oil Tank', 'devices', '4', 'INSERT', '15\' Oil Tank configured with Bettytag', '2016-10-19 15:03:02', '1');
INSERT INTO admin_log VALUES ('785', '1', '15\' Oil Tank', 'devices', '6', 'INSERT', '15\' Oil Tank configured with Beccatag', '2016-10-19 15:03:02', '1');
INSERT INTO admin_log VALUES ('786', '5', 'Becca\'s Bonaza', 'sites', '16', 'UPDATE', 'Site - Becca\'s Bonaza modified', '2016-10-19 15:03:42', '1');
INSERT INTO admin_log VALUES ('787', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 16:04:22', '1');
INSERT INTO admin_log VALUES ('788', '1', '20\' Oil Tank', 'devices', '11', 'INSERT', 'New device - 20\' Oil Tank added', '2016-10-19 16:05:41', '1');
INSERT INTO admin_log VALUES ('789', '1', '20\' Oil Tank', 'devices', '11', 'DELETE', 'Device - 20\' Oil Tank deleted', '2016-10-19 16:06:06', '1');
INSERT INTO admin_log VALUES ('790', '1', 'bbb', 'devices', '12', 'INSERT', 'New device - bbb added', '2016-10-19 16:06:49', '1');
INSERT INTO admin_log VALUES ('791', '1', 'bbb', 'devices', '12', 'UPDATE', 'Device - bbb modified', '2016-10-19 16:13:58', '1');
INSERT INTO admin_log VALUES ('792', '1', 'bbb', 'devices', '12', 'UPDATE', 'Device - bbb modified', '2016-10-19 16:14:57', '1');
INSERT INTO admin_log VALUES ('793', '1', 'bbb', 'devices', '12', 'UPDATE', 'Device - bbb modified', '2016-10-19 16:18:52', '1');
INSERT INTO admin_log VALUES ('794', '1', 'bbb', 'device_registers', '1', 'INSERT', 'bbb configured with tag', '2016-10-19 16:18:52', '1');
INSERT INTO admin_log VALUES ('795', '1', 'bbb', 'device_registers', '4', 'INSERT', 'bbb configured with Pressuretag', '2016-10-19 16:18:52', '1');
INSERT INTO admin_log VALUES ('796', '1', 'bbb', 'device_registers', '6', 'INSERT', 'bbb configured with Bettytag', '2016-10-19 16:18:52', '1');
INSERT INTO admin_log VALUES ('797', '1', 'bbb', 'devices', '12', 'UPDATE', 'Device - bbb modified', '2016-10-19 16:20:20', '1');
INSERT INTO admin_log VALUES ('798', '1', 'bbb', 'device_registers', '1', 'INSERT', 'bbb configured with Pressuretag', '2016-10-19 16:20:20', '1');
INSERT INTO admin_log VALUES ('799', '1', 'bbb', 'device_registers', '4', 'INSERT', 'bbb configured with Bettytag', '2016-10-19 16:20:20', '1');
INSERT INTO admin_log VALUES ('800', '1', 'bbb', 'device_registers', '6', 'INSERT', 'bbb configured with Beccatag', '2016-10-19 16:20:20', '1');
INSERT INTO admin_log VALUES ('801', '1', 'bbb', 'devices', '12', 'UPDATE', 'Device - bbb modified', '2016-10-19 16:20:39', '1');
INSERT INTO admin_log VALUES ('802', '1', 'bbb', 'device_registers', '10', 'INSERT', 'bbb configured with Critical Masstag', '2016-10-19 16:20:39', '1');
INSERT INTO admin_log VALUES ('803', '1', 'bbb', 'devices', '12', 'DELETE', 'Device - bbb deleted', '2016-10-19 16:30:04', '1');
INSERT INTO admin_log VALUES ('804', '1', '20\' Oil Tank', 'devices', '13', 'INSERT', 'New device - 20\' Oil Tank added', '2016-10-19 16:30:55', '1');
INSERT INTO admin_log VALUES ('805', '1', '20\' Oil Tank', 'device_registers', '1', 'INSERT', '20\' Oil Tank configured with Pressuretag', '2016-10-19 16:30:55', '1');
INSERT INTO admin_log VALUES ('806', '1', '20\' Oil Tank', 'device_registers', '3', 'INSERT', '20\' Oil Tank configured with Tank Leveltag', '2016-10-19 16:30:55', '1');
INSERT INTO admin_log VALUES ('807', '1', '20\' Oil Tank', 'device_registers', '10', 'INSERT', '20\' Oil Tank configured with Critical Masstag', '2016-10-19 16:30:55', '1');
INSERT INTO admin_log VALUES ('808', '1', '20\' Oil Tank', 'devices', '13', 'UPDATE', 'Device - 20\' Oil Tank modified', '2016-10-19 16:31:12', '1');
INSERT INTO admin_log VALUES ('809', '1', '20\' Oil Tank', 'device_registers', '1', 'INSERT', '20\' Oil Tank configured with Pressuretag', '2016-10-19 16:31:13', '1');
INSERT INTO admin_log VALUES ('810', '1', '20\' Oil Tank', 'device_registers', '3', 'INSERT', '20\' Oil Tank configured with Tank Leveltag', '2016-10-19 16:31:13', '1');
INSERT INTO admin_log VALUES ('811', '1', '20\' Oil Tank', 'device_registers', '5', 'INSERT', '20\' Oil Tank configured with Davidtag', '2016-10-19 16:31:13', '1');
INSERT INTO admin_log VALUES ('812', '1', '20\' Oil Tank', 'device_registers', '10', 'INSERT', '20\' Oil Tank configured with Critical Masstag', '2016-10-19 16:31:13', '1');
INSERT INTO admin_log VALUES ('813', '5', 'Becca\'s Bonaza', 'sites', '16', 'UPDATE', 'Site - Becca\'s Bonaza modified', '2016-10-19 16:32:05', '1');
INSERT INTO admin_log VALUES ('814', '5', 'Becca\'s Bonaza', 'site_devices', '5', 'INSERT', 'Becca\'s Bonaza configured with 20\' Water Tankdevice', '2016-10-19 16:32:05', '1');
INSERT INTO admin_log VALUES ('815', '5', 'Becca\'s Bonaza', 'site_devices', '7', 'INSERT', 'Becca\'s Bonaza configured with 15\' Oil Tankdevice', '2016-10-19 16:32:05', '1');
INSERT INTO admin_log VALUES ('816', '5', 'Becca\'s Bonaza', 'site_devices', '10', 'INSERT', 'Becca\'s Bonaza configured with Allen Bradley 1765device', '2016-10-19 16:32:06', '1');
INSERT INTO admin_log VALUES ('817', '5', 'Becca\'s Bonaza', 'sites', '16', 'UPDATE', 'Site - Becca\'s Bonaza modified', '2016-10-19 16:32:25', '1');
INSERT INTO admin_log VALUES ('818', '5', 'Becca\'s Bonaza', 'site_devices', '5', 'INSERT', 'Becca\'s Bonaza configured with 20\' Water Tankdevice', '2016-10-19 16:32:25', '1');
INSERT INTO admin_log VALUES ('819', '5', 'Becca\'s Bonaza', 'site_devices', '10', 'INSERT', 'Becca\'s Bonaza configured with Allen Bradley 1765device', '2016-10-19 16:32:25', '1');
INSERT INTO admin_log VALUES ('820', '5', '!test', 'sites', '31', 'INSERT', 'New site - !test added', '2016-10-19 16:32:48', '1');
INSERT INTO admin_log VALUES ('821', '5', '!test', 'site_devices', '5', 'INSERT', '!test configured with 20\' Water Tankdevice', '2016-10-19 16:32:48', '1');
INSERT INTO admin_log VALUES ('822', '5', '!test', 'site_devices', '7', 'INSERT', '!test configured with 15\' Oil Tankdevice', '2016-10-19 16:32:48', '1');
INSERT INTO admin_log VALUES ('823', '5', '!test', 'sites', '31', 'UPDATE', 'Site - !test modified', '2016-10-19 16:36:46', '1');
INSERT INTO admin_log VALUES ('824', '5', '!test', 'site_devices', '5', 'INSERT', '!test configured with 20\' Water Tankdevice', '2016-10-19 16:36:46', '1');
INSERT INTO admin_log VALUES ('825', '5', '!test', 'site_devices', '7', 'INSERT', '!test configured with 15\' Oil Tankdevice', '2016-10-19 16:36:46', '1');
INSERT INTO admin_log VALUES ('826', '5', '!test', 'sites', '31', 'UPDATE', 'Site - !test modified', '2016-10-19 16:37:12', '1');
INSERT INTO admin_log VALUES ('827', '5', '!test', 'site_devices', '5', 'INSERT', '!test configured with 20\' Water Tankdevice', '2016-10-19 16:37:12', '1');
INSERT INTO admin_log VALUES ('828', '5', '!test', 'site_devices', '7', 'INSERT', '!test configured with 15\' Oil Tankdevice', '2016-10-19 16:37:12', '1');
INSERT INTO admin_log VALUES ('829', '5', '!test', 'sites', '31', 'UPDATE', 'Site - !test modified', '2016-10-19 16:37:22', '1');
INSERT INTO admin_log VALUES ('830', '5', '!test', 'site_devices', '5', 'INSERT', '!test configured with 20\' Water Tankdevice', '2016-10-19 16:37:22', '1');
INSERT INTO admin_log VALUES ('831', '5', '!test', 'site_devices', '7', 'INSERT', '!test configured with 15\' Oil Tankdevice', '2016-10-19 16:37:22', '1');
INSERT INTO admin_log VALUES ('832', '5', '!test', 'sites', '31', 'DELETE', 'Site - !test deleted', '2016-10-19 16:37:33', '1');
INSERT INTO admin_log VALUES ('833', '5', '!Test', 'sites', '32', 'INSERT', 'New site - !Test added', '2016-10-19 16:39:38', '1');
INSERT INTO admin_log VALUES ('834', '5', '!Test', 'site_devices', '5', 'INSERT', '!Test configured with 20\' Water Tankdevice', '2016-10-19 16:39:38', '1');
INSERT INTO admin_log VALUES ('835', '5', '!Test', 'site_devices', '13', 'INSERT', '!Test configured with 20\' Oil Tankdevice', '2016-10-19 16:39:39', '1');
INSERT INTO admin_log VALUES ('836', '1', 'David', 'registers', '5', 'UPDATE', 'Data point - David modified', '2016-10-19 17:53:40', '1');
INSERT INTO admin_log VALUES ('837', '1', 'David Niven', 'registers', '5', 'UPDATE', 'Data point - David Niven modified', '2016-10-19 17:53:54', '1');
INSERT INTO admin_log VALUES ('838', '1', 'West Terlingua', 'sites', '29', 'UPDATE', 'Site - West Terlingua modified', '2016-10-19 17:55:06', '1');
INSERT INTO admin_log VALUES ('839', '1', 'West Terlingua', 'site_devices', '5', 'INSERT', 'West Terlingua configured with 20\' Water Tankdevice', '2016-10-19 17:55:06', '1');
INSERT INTO admin_log VALUES ('840', '1', 'West Terlingua', 'site_devices', '7', 'INSERT', 'West Terlingua configured with 15\' Oil Tankdevice', '2016-10-19 17:55:06', '1');
INSERT INTO admin_log VALUES ('841', '1', 'West Terlingua', 'site_devices', '10', 'INSERT', 'West Terlingua configured with Allen Bradley 1765device', '2016-10-19 17:55:06', '1');
INSERT INTO admin_log VALUES ('842', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 18:46:31', '1');
INSERT INTO admin_log VALUES ('843', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 18:46:58', '1');
INSERT INTO admin_log VALUES ('844', '1', 'Kris', 'sites', '25', 'UPDATE', 'Site - Kris modified', '2016-10-19 18:47:14', '1');
INSERT INTO admin_log VALUES ('845', '1', 'Kris', 'site_devices', '5', 'INSERT', 'Kris configured with 20\' Water Tankdevice', '2016-10-19 18:47:14', '1');
INSERT INTO admin_log VALUES ('846', '1', 'Kris', 'site_devices', '7', 'INSERT', 'Kris configured with 15\' Oil Tankdevice', '2016-10-19 18:47:14', '1');
INSERT INTO admin_log VALUES ('847', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 19:31:13', '1');
INSERT INTO admin_log VALUES ('848', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 20:53:32', '1');
INSERT INTO admin_log VALUES ('849', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-19 22:38:09', '1');
INSERT INTO admin_log VALUES ('850', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 03:03:09', '1');
INSERT INTO admin_log VALUES ('851', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 04:05:40', '1');
INSERT INTO admin_log VALUES ('852', '1', 'Kris', 'sites', '25', 'UPDATE', 'Site - Kris modified', '2016-10-20 04:31:33', '1');
INSERT INTO admin_log VALUES ('853', '1', 'Kris', 'site_devices', '5', 'INSERT', 'Kris configured with 20\' Water Tankdevice', '2016-10-20 04:31:34', '1');
INSERT INTO admin_log VALUES ('854', '1', 'Kris', 'site_devices', '7', 'INSERT', 'Kris configured with 15\' Oil Tankdevice', '2016-10-20 04:31:34', '1');
INSERT INTO admin_log VALUES ('855', '1', 'Kris', 'site_devices', '10', 'INSERT', 'Kris configured with Allen Bradley 1765device', '2016-10-20 04:31:34', '1');
INSERT INTO admin_log VALUES ('856', '5', '123 Give it to Me', 'sites', '33', 'INSERT', 'New site - 123 Give it to Me added', '2016-10-20 04:42:12', '1');
INSERT INTO admin_log VALUES ('857', '5', '123 Give it to Me', 'site_devices', '5', 'INSERT', '123 Give it to Me configured with 20\' Water Tankdevice', '2016-10-20 04:42:12', '1');
INSERT INTO admin_log VALUES ('858', '5', '123 Give it to Me', 'site_devices', '7', 'INSERT', '123 Give it to Me configured with 15\' Oil Tankdevice', '2016-10-20 04:42:12', '1');
INSERT INTO admin_log VALUES ('859', '5', '123 Give it to Me', 'sites', '34', 'INSERT', 'New site - 123 Give it to Me added', '2016-10-20 04:42:15', '1');
INSERT INTO admin_log VALUES ('860', '5', '123 Give it to Me', 'site_devices', '5', 'INSERT', '123 Give it to Me configured with 20\' Water Tankdevice', '2016-10-20 04:42:15', '1');
INSERT INTO admin_log VALUES ('861', '5', '123 Give it to Me', 'site_devices', '7', 'INSERT', '123 Give it to Me configured with 15\' Oil Tankdevice', '2016-10-20 04:42:15', '1');
INSERT INTO admin_log VALUES ('862', '5', '123 Give it to Me', 'sites', '33', 'DELETE', 'Site - 123 Give it to Me deleted', '2016-10-20 04:42:40', '1');
INSERT INTO admin_log VALUES ('863', '5', '!Test 456', 'sites', '32', 'UPDATE', 'Site - !Test 456 modified', '2016-10-20 05:01:54', '1');
INSERT INTO admin_log VALUES ('864', '5', '!Test 456', 'site_devices', '5', 'INSERT', '!Test 456 configured with 20\' Water Tankdevice', '2016-10-20 05:01:54', '1');
INSERT INTO admin_log VALUES ('865', '5', '!Test 456', 'site_devices', '13', 'INSERT', '!Test 456 configured with 20\' Oil Tankdevice', '2016-10-20 05:01:54', '1');
INSERT INTO admin_log VALUES ('866', '1', 'Pumpers', 'departments', '4', 'UPDATE', 'Department - Pumpers  reactivated', '2016-10-20 05:31:08', '1');
INSERT INTO admin_log VALUES ('867', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 06:06:25', '1');
INSERT INTO admin_log VALUES ('868', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-20 06:51:55', '1');
INSERT INTO admin_log VALUES ('869', '1', '15\' Oil Tank', 'device_registers', '1', 'INSERT', '15\' Oil Tank configured with Pressuretag', '2016-10-20 06:51:55', '1');
INSERT INTO admin_log VALUES ('870', '1', '15\' Oil Tank', 'device_registers', '6', 'INSERT', '15\' Oil Tank configured with Beccatag', '2016-10-20 06:51:55', '1');
INSERT INTO admin_log VALUES ('871', '1', '15\' Oil Tank', 'device_registers', '8', 'INSERT', '15\' Oil Tank configured with Savytag', '2016-10-20 06:51:55', '1');
INSERT INTO admin_log VALUES ('872', '1', '15\' Oil Tank', 'device_registers', '12', 'INSERT', '15\' Oil Tank configured with Oil Leaktag', '2016-10-20 06:51:55', '1');
INSERT INTO admin_log VALUES ('873', '1', '15\' Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Oil Tank modified', '2016-10-20 06:52:24', '1');
INSERT INTO admin_log VALUES ('874', '1', '15\' Oil Tank', 'device_registers', '1', 'INSERT', '15\' Oil Tank configured with Pressuretag', '2016-10-20 06:52:24', '1');
INSERT INTO admin_log VALUES ('875', '1', '15\' Oil Tank', 'device_registers', '3', 'INSERT', '15\' Oil Tank configured with Tank Leveltag', '2016-10-20 06:52:24', '1');
INSERT INTO admin_log VALUES ('876', '1', '15\' Oil Tank', 'device_registers', '4', 'INSERT', '15\' Oil Tank configured with Bettytag', '2016-10-20 06:52:24', '1');
INSERT INTO admin_log VALUES ('877', '1', '15\' Oil Tank', 'device_registers', '5', 'INSERT', '15\' Oil Tank configured with David Niventag', '2016-10-20 06:52:24', '1');
INSERT INTO admin_log VALUES ('878', '1', '15\' Oil Tank', 'device_registers', '6', 'INSERT', '15\' Oil Tank configured with Beccatag', '2016-10-20 06:52:24', '1');
INSERT INTO admin_log VALUES ('879', '1', '15\' Oil Tank', 'device_registers', '7', 'INSERT', '15\' Oil Tank configured with Blaketag', '2016-10-20 06:52:25', '1');
INSERT INTO admin_log VALUES ('880', '1', '15\' Oil Tank', 'device_registers', '8', 'INSERT', '15\' Oil Tank configured with Savytag', '2016-10-20 06:52:25', '1');
INSERT INTO admin_log VALUES ('881', '1', '15\' Oil Tank', 'device_registers', '10', 'INSERT', '15\' Oil Tank configured with Critical Masstag', '2016-10-20 06:52:25', '1');
INSERT INTO admin_log VALUES ('882', '1', '15\' Oil Tank', 'device_registers', '12', 'INSERT', '15\' Oil Tank configured with Oil Leaktag', '2016-10-20 06:52:25', '1');
INSERT INTO admin_log VALUES ('883', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 15:21:54', '1');
INSERT INTO admin_log VALUES ('884', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 15:53:19', '1');
INSERT INTO admin_log VALUES ('885', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 16:24:30', '1');
INSERT INTO admin_log VALUES ('886', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 18:31:31', '1');
INSERT INTO admin_log VALUES ('887', '1', 'Betty', 'device_registers', '4', 'DELETE', 'Device register - Betty deleted', '2016-10-20 18:55:35', '1');
INSERT INTO admin_log VALUES ('888', '1', 'Oil Leak', 'device_registers', '12', 'DELETE', 'Device register - Oil Leak deleted', '2016-10-20 18:55:46', '1');
INSERT INTO admin_log VALUES ('889', '1', 'David Niven', 'device_registers', '5', 'DELETE', 'Device register - David Niven deleted', '2016-10-20 18:55:56', '1');
INSERT INTO admin_log VALUES ('890', '1', 'OPC-UA Server', 'devices', '14', 'INSERT', 'New device - OPC-UA Server added', '2016-10-20 18:57:16', '1');
INSERT INTO admin_log VALUES ('891', '1', 'OPC-UA Server', 'device_registers', '1', 'INSERT', 'OPC-UA Server configured with Pressure tag', '2016-10-20 18:57:16', '1');
INSERT INTO admin_log VALUES ('892', '1', 'OPC-UA Server', 'device_registers', '3', 'INSERT', 'OPC-UA Server configured with Tank Level tag', '2016-10-20 18:57:16', '1');
INSERT INTO admin_log VALUES ('893', '1', 'OPC-UA Server', 'device_registers', '4', 'INSERT', 'OPC-UA Server configured with Betty tag', '2016-10-20 18:57:16', '1');
INSERT INTO admin_log VALUES ('894', '1', 'OPC-UA Server', 'device_registers', '5', 'INSERT', 'OPC-UA Server configured with David Niven tag', '2016-10-20 18:57:17', '1');
INSERT INTO admin_log VALUES ('895', '1', 'OPC-UA Server', 'device_registers', '6', 'INSERT', 'OPC-UA Server configured with Becca tag', '2016-10-20 18:57:17', '1');
INSERT INTO admin_log VALUES ('896', '1', 'OPC-UA Server', 'device_registers', '7', 'INSERT', 'OPC-UA Server configured with Blake tag', '2016-10-20 18:57:17', '1');
INSERT INTO admin_log VALUES ('897', '1', 'OPC-UA Server', 'device_registers', '8', 'INSERT', 'OPC-UA Server configured with Savy tag', '2016-10-20 18:57:17', '1');
INSERT INTO admin_log VALUES ('898', '1', 'OPC-UA Server', 'device_registers', '10', 'INSERT', 'OPC-UA Server configured with Critical Mass tag', '2016-10-20 18:57:17', '1');
INSERT INTO admin_log VALUES ('899', '1', 'OPC-UA Server', 'device_registers', '12', 'INSERT', 'OPC-UA Server configured with Oil Leak tag', '2016-10-20 18:57:17', '1');
INSERT INTO admin_log VALUES ('900', '1', 'Betty', 'device_registers', '4', 'DELETE', 'Device register - Betty deleted', '2016-10-20 18:57:34', '1');
INSERT INTO admin_log VALUES ('901', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-20 21:30:20', '1');
INSERT INTO admin_log VALUES ('902', '1', 'Jessica', 'sites', '24', 'UPDATE', 'Site - Jessica modified', '2016-10-20 21:35:58', '1');
INSERT INTO admin_log VALUES ('903', '1', 'Jessica', 'site_devices', '13', 'INSERT', 'Jessica configured with 20\' Oil Tank device', '2016-10-20 21:35:58', '1');
INSERT INTO admin_log VALUES ('904', '1', 'Jessica', 'site_devices', '14', 'INSERT', 'Jessica configured with OPC-UA Server device', '2016-10-20 21:35:59', '1');
INSERT INTO admin_log VALUES ('905', '1', '15\' Hanz Oil Tank', 'devices', '7', 'UPDATE', 'Device - 15\' Hanz Oil Tank modified', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('906', '1', '15\' Hanz Oil Tank', 'device_registers', '1', 'INSERT', '15\' Hanz Oil Tank configured with Pressure tag', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('907', '1', '15\' Hanz Oil Tank', 'device_registers', '3', 'INSERT', '15\' Hanz Oil Tank configured with Tank Level tag', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('908', '1', '15\' Hanz Oil Tank', 'device_registers', '6', 'INSERT', '15\' Hanz Oil Tank configured with Becca tag', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('909', '1', '15\' Hanz Oil Tank', 'device_registers', '7', 'INSERT', '15\' Hanz Oil Tank configured with Blake tag', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('910', '1', '15\' Hanz Oil Tank', 'device_registers', '8', 'INSERT', '15\' Hanz Oil Tank configured with Savy tag', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('911', '1', '15\' Hanz Oil Tank', 'device_registers', '10', 'INSERT', '15\' Hanz Oil Tank configured with Critical Mass tag', '2016-10-20 21:49:20', '1');
INSERT INTO admin_log VALUES ('912', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-21 17:23:28', '1');
INSERT INTO admin_log VALUES ('913', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-21 18:20:48', '1');
INSERT INTO admin_log VALUES ('914', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-22 02:33:20', '1');
INSERT INTO admin_log VALUES ('915', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-23 16:30:05', '1');
INSERT INTO admin_log VALUES ('916', '5', 'Omni Consolidated 123', 'accounts', '5', 'UPDATE', 'Account - Omni Consolidated 123 modified', '2016-10-23 16:53:39', '1');
INSERT INTO admin_log VALUES ('917', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-10-23 16:56:16', '1');
INSERT INTO admin_log VALUES ('918', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 13:15:39', '1');
INSERT INTO admin_log VALUES ('919', '5', 'Objects', 'modules', '24', 'UPDATE', 'Module - Objects modified', '2016-11-07 14:01:14', '1');
INSERT INTO admin_log VALUES ('920', '5', 'Devices', 'modules', '0', 'INSERT', 'New module - Devices created', '2016-11-07 14:02:02', '1');
INSERT INTO admin_log VALUES ('921', '5', 'Measurements', 'modules', '40', 'UPDATE', 'Module - Measurements modified', '2016-11-07 14:02:54', '1');
INSERT INTO admin_log VALUES ('922', '5', 'Modules', 'modules', '25', 'UPDATE', 'Module - Modules modified', '2016-11-07 14:03:28', '1');
INSERT INTO admin_log VALUES ('923', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('924', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('925', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('926', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('927', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('928', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('929', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('930', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('931', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('932', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('933', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('934', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('935', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('936', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('937', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('938', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-07 14:05:21', '1');
INSERT INTO admin_log VALUES ('939', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 14:05:33', '1');
INSERT INTO admin_log VALUES ('940', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 15:10:39', '1');
INSERT INTO admin_log VALUES ('941', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 17:06:58', '1');
INSERT INTO admin_log VALUES ('942', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 18:13:50', '1');
INSERT INTO admin_log VALUES ('943', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 18:44:55', '1');
INSERT INTO admin_log VALUES ('944', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-07 22:18:47', '1');
INSERT INTO admin_log VALUES ('945', '1', 'Omni Consolidated', 'accounts', '5', 'UPDATE', 'Account - Omni Consolidated modified', '2016-11-07 22:20:51', '1');
INSERT INTO admin_log VALUES ('946', '1', 'Fred123', 'accounts', '8', 'UPDATE', 'Account - Fred123  deactivated', '2016-11-07 22:20:58', '1');
INSERT INTO admin_log VALUES ('947', '1', 'Jackson\'s Drilling', 'accounts', '9', 'INSERT', 'New account - Jackson\'s Drilling added', '2016-11-07 22:21:38', '1');
INSERT INTO admin_log VALUES ('948', '1', 'Jackson\'s Drilling Co.', 'accounts', '9', 'UPDATE', 'Account - Jackson\'s Drilling Co. modified', '2016-11-07 22:22:00', '1');
INSERT INTO admin_log VALUES ('949', '1', 'Jackson\'s Drilling Co.', 'accounts', '9', 'UPDATE', 'Account - Jackson\'s Drilling Co. modified', '2016-11-07 22:22:11', '1');
INSERT INTO admin_log VALUES ('950', '1', 'Jackson\'s Drilling Co.', 'accounts', '9', 'UPDATE', 'Account - Jackson\'s Drilling Co.  reactivated', '2016-11-07 22:22:16', '1');
INSERT INTO admin_log VALUES ('951', '1', 'Tank Stick', 'devices', '1', 'INSERT', 'New device - Tank Stick added', '2016-11-07 23:05:02', '1');
INSERT INTO admin_log VALUES ('952', '1', 'Transducer', 'devices', '2', 'INSERT', 'New device - Transducer added', '2016-11-07 23:05:32', '1');
INSERT INTO admin_log VALUES ('953', '1', 'Transfer Pump', 'devices', '3', 'INSERT', 'New device - Transfer Pump added', '2016-11-07 23:05:54', '1');
INSERT INTO admin_log VALUES ('954', '1', 'Oil Tank', 'objects', '1', 'INSERT', 'New object - Oil Tank added', '2016-11-07 23:17:55', '1');
INSERT INTO admin_log VALUES ('955', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Account - Oil Tank modified', '2016-11-07 23:21:34', '1');
INSERT INTO admin_log VALUES ('956', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Account - Oil Tank modified', '2016-11-07 23:21:55', '1');
INSERT INTO admin_log VALUES ('957', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Account - Oil Tank modified', '2016-11-07 23:24:23', '1');
INSERT INTO admin_log VALUES ('958', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:26:12', '1');
INSERT INTO admin_log VALUES ('959', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:27:01', '1');
INSERT INTO admin_log VALUES ('960', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:27:10', '1');
INSERT INTO admin_log VALUES ('961', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:28:27', '1');
INSERT INTO admin_log VALUES ('962', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:33:29', '1');
INSERT INTO admin_log VALUES ('963', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:33:55', '1');
INSERT INTO admin_log VALUES ('964', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick tag', '2016-11-07 23:33:55', '1');
INSERT INTO admin_log VALUES ('965', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:34:19', '1');
INSERT INTO admin_log VALUES ('966', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick tag', '2016-11-07 23:34:20', '1');
INSERT INTO admin_log VALUES ('967', '1', 'Oil Tank', 'object_devices', '2', 'INSERT', 'Oil Tank configured with Transducer tag', '2016-11-07 23:34:20', '1');
INSERT INTO admin_log VALUES ('968', '1', 'Oil Tank', 'objects', '1', 'UPDATE', 'Object - Oil Tank modified', '2016-11-07 23:35:03', '1');
INSERT INTO admin_log VALUES ('969', '1', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump tag', '2016-11-07 23:35:03', '1');
INSERT INTO admin_log VALUES ('970', '1', 'Oil Tank', 'objects', '1', 'DELETE', 'Object - Oil Tank deleted', '2016-11-07 23:35:20', '1');
INSERT INTO admin_log VALUES ('971', '1', 'Oil Tank', 'objects', '2', 'INSERT', 'New object - Oil Tank added', '2016-11-07 23:36:07', '1');
INSERT INTO admin_log VALUES ('972', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick tag', '2016-11-07 23:36:07', '1');
INSERT INTO admin_log VALUES ('973', '1', 'Oil Tank', 'object_devices', '2', 'INSERT', 'Oil Tank configured with Transducer tag', '2016-11-07 23:36:07', '1');
INSERT INTO admin_log VALUES ('974', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  deactivated', '2016-11-07 23:36:21', '1');
INSERT INTO admin_log VALUES ('975', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  reactivated', '2016-11-07 23:36:27', '1');
INSERT INTO admin_log VALUES ('976', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 00:13:11', '1');
INSERT INTO admin_log VALUES ('977', '1', 'Tags', 'modules', '41', 'INSERT', 'New module - Tags created', '2016-11-08 00:18:14', '1');
INSERT INTO admin_log VALUES ('978', '1', 'Modules', 'modules', '25', 'UPDATE', 'Module - Modules modified', '2016-11-08 00:18:33', '1');
INSERT INTO admin_log VALUES ('979', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('980', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('981', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('982', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('983', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('984', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('985', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('986', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('987', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('988', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('989', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('990', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('991', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('992', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('993', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('994', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-08 00:20:05', '1');
INSERT INTO admin_log VALUES ('995', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 00:20:17', '1');
INSERT INTO admin_log VALUES ('996', '1', 'AST', 'tags', '1', 'INSERT', 'New data point - AST added', '2016-11-08 00:28:37', '1');
INSERT INTO admin_log VALUES ('997', '1', 'CST', 'tags', '2', 'INSERT', 'New data point - CST added', '2016-11-08 00:28:48', '1');
INSERT INTO admin_log VALUES ('998', '1', 'BST', 'tags', '3', 'INSERT', 'New data point - BST added', '2016-11-08 00:29:02', '1');
INSERT INTO admin_log VALUES ('999', '1', 'AST', 'tags', '1', 'UPDATE', 'Data point - AST modified', '2016-11-08 00:29:50', '1');
INSERT INTO admin_log VALUES ('1000', '1', 'AST1', 'tags', '1', 'UPDATE', 'Data point - AST1 modified', '2016-11-08 00:30:06', '1');
INSERT INTO admin_log VALUES ('1001', '1', 'CST', 'tags', '2', 'DELETE', 'Tag - CST deleted', '2016-11-08 00:30:24', '1');
INSERT INTO admin_log VALUES ('1002', '1', 'AST', 'tags', '1', 'UPDATE', 'Data point - AST modified', '2016-11-08 00:30:34', '1');
INSERT INTO admin_log VALUES ('1003', '1', 'AST', 'tags', '1', 'UPDATE', 'Tag - AST  deactivated', '2016-11-08 00:30:40', '1');
INSERT INTO admin_log VALUES ('1004', '1', 'CST', 'tags', '4', 'INSERT', 'New data point - CST added', '2016-11-08 00:30:50', '1');
INSERT INTO admin_log VALUES ('1005', '1', 'CST', 'tags', '4', 'DELETE', 'Tag - CST deleted', '2016-11-08 00:30:58', '1');
INSERT INTO admin_log VALUES ('1006', '1', 'CST', 'tags', '5', 'INSERT', 'New data point - CST added', '2016-11-08 00:31:07', '1');
INSERT INTO admin_log VALUES ('1007', '1', 'AST', 'tags', '1', 'UPDATE', 'Data point - AST modified', '2016-11-08 00:31:21', '1');
INSERT INTO admin_log VALUES ('1008', '1', 'Level', 'measurements', '1', 'INSERT', 'New object - Level added', '2016-11-08 00:43:47', '1');
INSERT INTO admin_log VALUES ('1009', '1', 'Level', 'measurements', '2', 'INSERT', 'New object - Level added', '2016-11-08 00:45:12', '1');
INSERT INTO admin_log VALUES ('1010', '1', 'Level', 'measurements', '0000000001', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:45:59', '1');
INSERT INTO admin_log VALUES ('1011', '1', 'Level', 'measurements', '0000000002', 'UPDATE', 'Measurement - Level  deactivated', '2016-11-08 00:46:07', '1');
INSERT INTO admin_log VALUES ('1012', '1', 'Level', 'measurements', '0000000002', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:46:13', '1');
INSERT INTO admin_log VALUES ('1013', '1', 'Level', 'measurements', '3', 'INSERT', 'New object - Level added', '2016-11-08 00:46:23', '1');
INSERT INTO admin_log VALUES ('1014', '1', 'Level', 'measurements', '4', 'INSERT', 'New object - Level added', '2016-11-08 00:46:44', '1');
INSERT INTO admin_log VALUES ('1015', '1', 'Level', 'measurements', '5', 'INSERT', 'New object - Level added', '2016-11-08 00:50:25', '1');
INSERT INTO admin_log VALUES ('1016', '1', 'Level', 'measurements', '0000000003', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:50:53', '1');
INSERT INTO admin_log VALUES ('1017', '1', 'Level', 'measurements', '0000000004', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:51:01', '1');
INSERT INTO admin_log VALUES ('1018', '1', 'Level', 'measurements', '0000000005', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:51:06', '1');
INSERT INTO admin_log VALUES ('1019', '1', 'Level', 'measurements', '6', 'INSERT', 'New object - Level added', '2016-11-08 00:51:23', '1');
INSERT INTO admin_log VALUES ('1020', '1', 'Level', 'measurements', '7', 'INSERT', 'New object - Level added', '2016-11-08 00:52:11', '1');
INSERT INTO admin_log VALUES ('1021', '1', 'Level', 'measurements', '0000000006', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:52:27', '1');
INSERT INTO admin_log VALUES ('1022', '1', 'Level', 'measurements', '0000000007', 'DELETE', 'Measurement - Level deleted', '2016-11-08 00:52:38', '1');
INSERT INTO admin_log VALUES ('1023', '1', 'Level', 'measurements', '8', 'INSERT', 'New object - Level added', '2016-11-08 00:53:01', '1');
INSERT INTO admin_log VALUES ('1024', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:55:03', '1');
INSERT INTO admin_log VALUES ('1025', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:56:02', '1');
INSERT INTO admin_log VALUES ('1026', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:56:28', '1');
INSERT INTO admin_log VALUES ('1027', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:56:39', '1');
INSERT INTO admin_log VALUES ('1028', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:57:19', '1');
INSERT INTO admin_log VALUES ('1029', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:58:36', '1');
INSERT INTO admin_log VALUES ('1030', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 00:59:33', '1');
INSERT INTO admin_log VALUES ('1031', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 01:00:36', '1');
INSERT INTO admin_log VALUES ('1032', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 01:06:05', '1');
INSERT INTO admin_log VALUES ('1033', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 01:59:28', '1');
INSERT INTO admin_log VALUES ('1034', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 01:59:52', '1');
INSERT INTO admin_log VALUES ('1035', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 02:06:35', '1');
INSERT INTO admin_log VALUES ('1036', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Object - Level modified', '2016-11-08 02:17:09', '1');
INSERT INTO admin_log VALUES ('1037', '1', 'Level', 'measurement_tags', '1', 'INSERT', 'Level configured with AST tag', '2016-11-08 02:17:10', '1');
INSERT INTO admin_log VALUES ('1038', '1', 'Temperature', 'measurements', '9', 'INSERT', 'New object - Temperature added', '2016-11-08 02:18:46', '1');
INSERT INTO admin_log VALUES ('1039', '1', 'Temperature', 'measurements', '0000000009', 'UPDATE', 'Object - Temperature modified', '2016-11-08 02:19:14', '1');
INSERT INTO admin_log VALUES ('1040', '1', 'Temperature', 'measurement_tags', '1', 'INSERT', 'Temperature configured with AST tag', '2016-11-08 02:19:14', '1');
INSERT INTO admin_log VALUES ('1041', '1', 'Pressure', 'measurements', '10', 'INSERT', 'New object - Pressure added', '2016-11-08 02:19:39', '1');
INSERT INTO admin_log VALUES ('1042', '1', 'Pressure', 'measurements', '11', 'INSERT', 'New object - Pressure added', '2016-11-08 02:20:08', '1');
INSERT INTO admin_log VALUES ('1043', '1', 'Pressure', 'measurement_tags', '1', 'INSERT', 'Pressure configured with AST tag', '2016-11-08 02:20:08', '1');
INSERT INTO admin_log VALUES ('1044', '1', 'Pressure', 'measurement_tags', '3', 'INSERT', 'Pressure configured with BST tag', '2016-11-08 02:20:08', '1');
INSERT INTO admin_log VALUES ('1045', '1', 'Pressure', 'measurements', '0000000010', 'DELETE', 'Measurement - Pressure deleted', '2016-11-08 02:20:42', '1');
INSERT INTO admin_log VALUES ('1046', '1', 'Pressure', 'measurements', '0000000011', 'UPDATE', 'Object - Pressure modified', '2016-11-08 02:20:50', '1');
INSERT INTO admin_log VALUES ('1047', '1', 'Pressure', 'measurement_tags', '3', 'INSERT', 'Pressure configured with BST tag', '2016-11-08 02:20:50', '1');
INSERT INTO admin_log VALUES ('1048', '1', 'Cookin', 'measurements', '12', 'INSERT', 'New object - Cookin added', '2016-11-08 02:23:58', '1');
INSERT INTO admin_log VALUES ('1049', '1', 'Cookin', 'measurement_tags', '1', 'INSERT', 'Cookin configured with AST tag', '2016-11-08 02:23:58', '1');
INSERT INTO admin_log VALUES ('1050', '1', 'Cookin', 'measurement_tags', '5', 'INSERT', 'Cookin configured with CST tag', '2016-11-08 02:23:58', '1');
INSERT INTO admin_log VALUES ('1051', '1', 'Cookin', 'measurement_tags', '3', 'INSERT', 'Cookin configured with BST tag', '2016-11-08 02:23:58', '1');
INSERT INTO admin_log VALUES ('1052', '1', 'Cookin', 'measurements', '0000000012', 'UPDATE', 'Object - Cookin modified', '2016-11-08 02:24:13', '1');
INSERT INTO admin_log VALUES ('1053', '1', 'Cookin', 'measurements', '0000000012', 'UPDATE', 'Object - Cookin modified', '2016-11-08 02:24:22', '1');
INSERT INTO admin_log VALUES ('1054', '1', 'Cookin', 'measurement_tags', '1', 'INSERT', 'Cookin configured with AST tag', '2016-11-08 02:24:22', '1');
INSERT INTO admin_log VALUES ('1055', '1', 'Cookin', 'measurement_tags', '5', 'INSERT', 'Cookin configured with CST tag', '2016-11-08 02:24:22', '1');
INSERT INTO admin_log VALUES ('1056', '1', 'Sleepin', 'measurements', '13', 'INSERT', 'New object - Sleepin added', '2016-11-08 02:24:52', '1');
INSERT INTO admin_log VALUES ('1057', '1', 'Sleepin', 'measurement_tags', '1', 'INSERT', 'Sleepin configured with AST tag', '2016-11-08 02:24:52', '1');
INSERT INTO admin_log VALUES ('1058', '1', 'Sleepin', 'measurement_tags', '5', 'INSERT', 'Sleepin configured with CST tag', '2016-11-08 02:24:52', '1');
INSERT INTO admin_log VALUES ('1059', '1', 'Sleepin', 'measurements', '14', 'INSERT', 'New object - Sleepin added', '2016-11-08 02:26:11', '1');
INSERT INTO admin_log VALUES ('1060', '1', 'Sleepin', 'measurement_tags', '1', 'INSERT', 'Sleepin configured with AST tag', '2016-11-08 02:26:11', '1');
INSERT INTO admin_log VALUES ('1061', '1', 'Sleepin', 'measurement_tags', '5', 'INSERT', 'Sleepin configured with CST tag', '2016-11-08 02:26:11', '1');
INSERT INTO admin_log VALUES ('1062', '1', 'Sleepin', 'measurements', '0000000013', 'DELETE', 'Measurement - Sleepin deleted', '2016-11-08 02:28:54', '1');
INSERT INTO admin_log VALUES ('1063', '1', 'Sleepin', 'measurements', '0000000014', 'DELETE', 'Measurement - Sleepin deleted', '2016-11-08 02:29:04', '1');
INSERT INTO admin_log VALUES ('1064', '1', 'Cookin', 'measurements', '0000000012', 'DELETE', 'Measurement - Cookin deleted', '2016-11-08 02:29:11', '1');
INSERT INTO admin_log VALUES ('1065', '1', 'Cookin', 'measurements', '15', 'INSERT', 'New object - Cookin added', '2016-11-08 02:29:31', '1');
INSERT INTO admin_log VALUES ('1066', '1', 'Cookin', 'measurement_tags', '1', 'INSERT', 'Cookin configured with AST tag', '2016-11-08 02:29:31', '1');
INSERT INTO admin_log VALUES ('1067', '1', 'Cookin', 'measurement_tags', '5', 'INSERT', 'Cookin configured with CST tag', '2016-11-08 02:29:31', '1');
INSERT INTO admin_log VALUES ('1068', '1', 'Cookin', 'measurements', '0000000015', 'UPDATE', 'Object - Cookin modified', '2016-11-08 02:29:50', '1');
INSERT INTO admin_log VALUES ('1069', '1', 'Cookin', 'measurement_tags', '5', 'INSERT', 'Cookin configured with CST tag', '2016-11-08 02:29:50', '1');
INSERT INTO admin_log VALUES ('1070', '1', 'Temperature', 'measurements', '0000000009', 'UPDATE', 'Object - Temperature modified', '2016-11-08 02:30:45', '1');
INSERT INTO admin_log VALUES ('1071', '1', 'Temperature', 'measurement_tags', '1', 'INSERT', 'Temperature configured with AST tag', '2016-11-08 02:30:45', '1');
INSERT INTO admin_log VALUES ('1072', '1', 'Temperature', 'measurement_tags', '3', 'INSERT', 'Temperature configured with BST tag', '2016-11-08 02:30:45', '1');
INSERT INTO admin_log VALUES ('1073', '1', 'Temperature', 'measurements', '0000000009', 'UPDATE', 'Measurement - Temperature modified', '2016-11-08 02:34:55', '1');
INSERT INTO admin_log VALUES ('1074', '1', 'Temperature', 'measurement_tags', '3', 'INSERT', 'Temperature configured with BST tag', '2016-11-08 02:34:55', '1');
INSERT INTO admin_log VALUES ('1075', '1', 'Cookin', 'measurements', '0000000015', 'DELETE', 'Measurement - Cookin deleted', '2016-11-08 02:37:59', '1');
INSERT INTO admin_log VALUES ('1076', '1', 'Test\'s', 'measurements', '16', 'INSERT', 'New measurement - Test\'s added', '2016-11-08 02:40:13', '1');
INSERT INTO admin_log VALUES ('1077', '1', 'Test\'s', 'measurement_tags', '1', 'INSERT', 'Test\'s configured with AST tag', '2016-11-08 02:40:13', '1');
INSERT INTO admin_log VALUES ('1078', '1', 'Test\'s', 'measurement_tags', '3', 'INSERT', 'Test\'s configured with BST tag', '2016-11-08 02:40:13', '1');
INSERT INTO admin_log VALUES ('1079', '1', 'Test\'s', 'measurements', '0000000016', 'UPDATE', 'Measurement - Test\'s modified', '2016-11-08 02:40:21', '1');
INSERT INTO admin_log VALUES ('1080', '1', 'Test\'s', 'measurement_tags', '3', 'INSERT', 'Test\'s configured with BST tag', '2016-11-08 02:40:21', '1');
INSERT INTO admin_log VALUES ('1081', '1', 'Test\'s', 'measurements', '0000000016', 'DELETE', 'Measurement - Test\'s deleted', '2016-11-08 02:40:50', '1');
INSERT INTO admin_log VALUES ('1082', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1083', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1084', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1085', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1086', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1087', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1088', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1089', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1090', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1091', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1092', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1093', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1094', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1095', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1096', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1097', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1098', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-08 02:41:33', '1');
INSERT INTO admin_log VALUES ('1099', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 02:41:47', '1');
INSERT INTO admin_log VALUES ('1100', '1', 'Cookin', 'measurements', '17', 'INSERT', 'New measurement - Cookin added', '2016-11-08 03:15:52', '1');
INSERT INTO admin_log VALUES ('1101', '1', 'Cookin', 'measurement_tags', '1', 'INSERT', 'Cookin configured with AST tag', '2016-11-08 03:15:52', '1');
INSERT INTO admin_log VALUES ('1102', '1', 'Cookin', 'measurements', '0000000017', 'DELETE', 'Measurement - Cookin deleted', '2016-11-08 03:18:05', '1');
INSERT INTO admin_log VALUES ('1103', '1', 'Cookin', 'measurements', '18', 'INSERT', 'New measurement - Cookin added', '2016-11-08 03:18:34', '1');
INSERT INTO admin_log VALUES ('1104', '1', 'Cookin', 'measurement_tags', '1', 'INSERT', 'Cookin configured with AST tag', '2016-11-08 03:18:34', '1');
INSERT INTO admin_log VALUES ('1105', '1', 'Cookin', 'measurements', '0000000018', 'UPDATE', 'Measurement - Cookin modified', '2016-11-08 03:19:06', '1');
INSERT INTO admin_log VALUES ('1106', '1', 'Cookin', 'measurement_tags', '1', 'INSERT', 'Cookin configured with AST tag', '2016-11-08 03:19:06', '1');
INSERT INTO admin_log VALUES ('1107', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Measurement - Level modified', '2016-11-08 03:35:01', '1');
INSERT INTO admin_log VALUES ('1108', '1', 'Level', 'measurement_tags', '3', 'INSERT', 'Level configured with BST tag', '2016-11-08 03:35:01', '1');
INSERT INTO admin_log VALUES ('1109', '1', 'Test\'s', 'devices', '4', 'INSERT', 'New device - Test\'s added', '2016-11-08 03:43:15', '1');
INSERT INTO admin_log VALUES ('1110', '1', 'Test\'s', 'devices', '5', 'INSERT', 'New device - Test\'s added', '2016-11-08 03:45:09', '1');
INSERT INTO admin_log VALUES ('1111', '1', 'Test\'s', 'devices', '4', 'DELETE', 'Device - Test\'s deleted', '2016-11-08 03:50:10', '1');
INSERT INTO admin_log VALUES ('1112', '1', 'Test\'s', 'devices', '5', 'DELETE', 'Device - Test\'s deleted', '2016-11-08 03:50:22', '1');
INSERT INTO admin_log VALUES ('1113', '1', 'Fred\'s', 'devices', '6', 'INSERT', 'New device - Fred\'s added', '2016-11-08 03:50:38', '1');
INSERT INTO admin_log VALUES ('1114', '1', 'Fred\'s', 'device_measurements', '0000000008', 'INSERT', 'Fred\'s configured with Level measurement', '2016-11-08 03:50:38', '1');
INSERT INTO admin_log VALUES ('1115', '1', 'Fred\'s', 'device_measurements', '0000000009', 'INSERT', 'Fred\'s configured with Temperature measurement', '2016-11-08 03:50:38', '1');
INSERT INTO admin_log VALUES ('1116', '1', 'Fred\'s', 'devices', '6', 'UPDATE', 'Device - Fred\'s modified', '2016-11-08 03:52:05', '1');
INSERT INTO admin_log VALUES ('1117', '1', 'Fred\'s', 'device_measurements', '0000000011', 'INSERT', 'Fred\'s configured with Pressure measurement', '2016-11-08 03:52:05', '1');
INSERT INTO admin_log VALUES ('1118', '1', 'Fred\'s', 'devices', '6', 'DELETE', 'Device - Fred\'s deleted', '2016-11-08 03:52:39', '1');
INSERT INTO admin_log VALUES ('1119', '1', 'Tank Stick', 'devices', '1', 'UPDATE', 'Device - Tank Stick modified', '2016-11-08 03:52:54', '1');
INSERT INTO admin_log VALUES ('1120', '1', 'Tank Stick', 'device_measurements', '0000000008', 'INSERT', 'Tank Stick configured with Level measurement', '2016-11-08 03:52:54', '1');
INSERT INTO admin_log VALUES ('1121', '1', 'Tank Stick', 'devices', '1', 'UPDATE', 'Device - Tank Stick modified', '2016-11-08 04:00:56', '1');
INSERT INTO admin_log VALUES ('1122', '1', 'Tank Stick', 'device_measurements', '0000000008', 'INSERT', 'Tank Stick configured with Level measurement', '2016-11-08 04:00:56', '1');
INSERT INTO admin_log VALUES ('1123', '1', 'Tank Stick', 'device_measurements', '0000000011', 'INSERT', 'Tank Stick configured with Pressure measurement', '2016-11-08 04:00:56', '1');
INSERT INTO admin_log VALUES ('1124', '1', 'Tank Stick', 'devices', '1', 'UPDATE', 'Device - Tank Stick modified', '2016-11-08 04:01:05', '1');
INSERT INTO admin_log VALUES ('1125', '1', 'Tank Stick', 'device_measurements', '0000000008', 'INSERT', 'Tank Stick configured with Level measurement', '2016-11-08 04:01:05', '1');
INSERT INTO admin_log VALUES ('1126', '1', 'Fred\'s', 'devices', '7', 'INSERT', 'New device - Fred\'s added', '2016-11-08 04:01:20', '1');
INSERT INTO admin_log VALUES ('1127', '1', 'Fred\'s', 'device_measurements', '0000000008', 'INSERT', 'Fred\'s configured with Level measurement', '2016-11-08 04:01:20', '1');
INSERT INTO admin_log VALUES ('1128', '1', 'Fred\'s', 'device_measurements', '0000000009', 'INSERT', 'Fred\'s configured with Temperature measurement', '2016-11-08 04:01:21', '1');
INSERT INTO admin_log VALUES ('1129', '1', 'Fred\'s', 'devices', '7', 'UPDATE', 'Device - Fred\'s modified', '2016-11-08 04:01:36', '1');
INSERT INTO admin_log VALUES ('1130', '1', 'Fred\'s', 'device_measurements', '0000000018', 'INSERT', 'Fred\'s configured with Cookin measurement', '2016-11-08 04:01:36', '1');
INSERT INTO admin_log VALUES ('1131', '1', 'Fred\'s', 'devices', '7', 'DELETE', 'Device - Fred\'s deleted', '2016-11-08 04:03:11', '1');
INSERT INTO admin_log VALUES ('1132', '1', 'Fred', 'devices', '8', 'INSERT', 'New device - Fred added', '2016-11-08 04:03:29', '1');
INSERT INTO admin_log VALUES ('1133', '1', 'Fred', 'device_measurements', '0000000008', 'INSERT', 'Fred configured with Level measurement', '2016-11-08 04:03:29', '1');
INSERT INTO admin_log VALUES ('1134', '1', 'Fred', 'device_measurements', '0000000009', 'INSERT', 'Fred configured with Temperature measurement', '2016-11-08 04:03:29', '1');
INSERT INTO admin_log VALUES ('1135', '1', 'Fred', 'devices', '8', 'DELETE', 'Device - Fred deleted', '2016-11-08 04:03:51', '1');
INSERT INTO admin_log VALUES ('1136', '1', 'Level', 'device_measurements', '0000000008', 'DELETE', 'Device measurement - Level deleted', '2016-11-08 04:13:53', '1');
INSERT INTO admin_log VALUES ('1137', '1', 'Tank Stick', 'devices', '1', 'UPDATE', 'Device - Tank Stick modified', '2016-11-08 04:18:34', '1');
INSERT INTO admin_log VALUES ('1138', '1', 'Tank Stick', 'device_measurements', '0000000008', 'INSERT', 'Tank Stick configured with Level measurement', '2016-11-08 04:18:34', '1');
INSERT INTO admin_log VALUES ('1139', '1', 'Tank Stick', 'device_measurements', '0000000009', 'INSERT', 'Tank Stick configured with Temperature measurement', '2016-11-08 04:18:34', '1');
INSERT INTO admin_log VALUES ('1140', '1', 'Lucy\'s', 'devices', '9', 'INSERT', 'New device - Lucy\'s added', '2016-11-08 04:19:29', '1');
INSERT INTO admin_log VALUES ('1141', '1', 'Lucy\'s', 'device_measurements', '0000000008', 'INSERT', 'Lucy\'s configured with Level measurement', '2016-11-08 04:19:29', '1');
INSERT INTO admin_log VALUES ('1142', '1', 'Lucy\'s', 'device_measurements', '0000000011', 'INSERT', 'Lucy\'s configured with Pressure measurement', '2016-11-08 04:19:29', '1');
INSERT INTO admin_log VALUES ('1143', '1', 'Lucy\'s', 'devices', '10', 'INSERT', 'New device - Lucy\'s added', '2016-11-08 04:20:18', '1');
INSERT INTO admin_log VALUES ('1144', '1', 'Lucy\'s', 'device_measurements', '0000000008', 'INSERT', 'Lucy\'s configured with Level measurement', '2016-11-08 04:20:18', '1');
INSERT INTO admin_log VALUES ('1145', '1', 'Lucy\'s', 'device_measurements', '0000000011', 'INSERT', 'Lucy\'s configured with Pressure measurement', '2016-11-08 04:20:18', '1');
INSERT INTO admin_log VALUES ('1146', '1', 'Lucy\'s', 'devices', '10', 'DELETE', 'Device - Lucy\'s deleted', '2016-11-08 04:27:41', '1');
INSERT INTO admin_log VALUES ('1147', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  deactivated', '2016-11-08 06:40:55', '1');
INSERT INTO admin_log VALUES ('1148', '1', '123', 'objects', '3', 'INSERT', 'New object - 123 added', '2016-11-08 06:42:32', '1');
INSERT INTO admin_log VALUES ('1149', '1', '123', 'object_devices', '1', 'INSERT', '123 configured with Tank Stick device', '2016-11-08 06:42:32', '1');
INSERT INTO admin_log VALUES ('1150', '1', '123', 'objects', '3', 'UPDATE', 'Object - 123 modified', '2016-11-08 06:51:01', '1');
INSERT INTO admin_log VALUES ('1151', '1', '123', 'object_devices', '1', 'INSERT', '123 configured with Tank Stick device', '2016-11-08 06:51:01', '1');
INSERT INTO admin_log VALUES ('1152', '1', '456', 'objects', '3', 'UPDATE', 'Object - 456 modified', '2016-11-08 06:51:15', '1');
INSERT INTO admin_log VALUES ('1153', '1', '456', 'object_devices', '1', 'INSERT', '456 configured with Tank Stick device', '2016-11-08 06:51:16', '1');
INSERT INTO admin_log VALUES ('1154', '1', '456', 'object_devices', '2', 'INSERT', '456 configured with Transducer device', '2016-11-08 06:51:16', '1');
INSERT INTO admin_log VALUES ('1155', '1', '456', 'object_devices', '3', 'INSERT', '456 configured with Transfer Pump device', '2016-11-08 06:51:16', '1');
INSERT INTO admin_log VALUES ('1156', '1', '456', 'objects', '3', 'UPDATE', 'Object - 456 modified', '2016-11-08 06:51:38', '1');
INSERT INTO admin_log VALUES ('1157', '1', '456', 'object_devices', '1', 'INSERT', '456 configured with Tank Stick device', '2016-11-08 06:51:38', '1');
INSERT INTO admin_log VALUES ('1158', '1', '456', 'object_devices', '2', 'INSERT', '456 configured with Transducer device', '2016-11-08 06:51:38', '1');
INSERT INTO admin_log VALUES ('1159', '1', '456', 'object_devices', '3', 'INSERT', '456 configured with Transfer Pump device', '2016-11-08 06:51:38', '1');
INSERT INTO admin_log VALUES ('1160', '1', '456', 'objects', '3', 'UPDATE', 'Object - 456 modified', '2016-11-08 06:51:47', '1');
INSERT INTO admin_log VALUES ('1161', '1', '456', 'object_devices', '1', 'INSERT', '456 configured with Tank Stick device', '2016-11-08 06:51:47', '1');
INSERT INTO admin_log VALUES ('1162', '1', '456', 'object_devices', '2', 'INSERT', '456 configured with Transducer device', '2016-11-08 06:51:47', '1');
INSERT INTO admin_log VALUES ('1163', '1', '456', 'object_devices', '3', 'INSERT', '456 configured with Transfer Pump device', '2016-11-08 06:51:47', '1');
INSERT INTO admin_log VALUES ('1164', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  reactivated', '2016-11-08 06:51:54', '1');
INSERT INTO admin_log VALUES ('1165', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 15:33:58', '1');
INSERT INTO admin_log VALUES ('1166', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 15:34:34', '1');
INSERT INTO admin_log VALUES ('1167', '1', 'Water Tank', 'objects', '3', 'UPDATE', 'Object - Water Tank modified', '2016-11-08 15:35:59', '1');
INSERT INTO admin_log VALUES ('1168', '1', 'Water Tank', 'object_devices', '1', 'INSERT', 'Water Tank configured with Tank Stick device', '2016-11-08 15:35:59', '1');
INSERT INTO admin_log VALUES ('1169', '1', 'Water Tank', 'object_devices', '2', 'INSERT', 'Water Tank configured with Transducer device', '2016-11-08 15:35:59', '1');
INSERT INTO admin_log VALUES ('1170', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 17:03:13', '1');
INSERT INTO admin_log VALUES ('1171', '5', 'Test\'s', 'objects', '4', 'INSERT', 'New object - Test\'s added', '2016-11-08 18:01:13', '1');
INSERT INTO admin_log VALUES ('1172', '5', 'Test\'s', 'object_devices', '1', 'INSERT', 'Test\'s configured with Tank Stick device', '2016-11-08 18:01:13', '1');
INSERT INTO admin_log VALUES ('1173', '5', 'Test\'s', 'object_devices', '3', 'INSERT', 'Test\'s configured with Transfer Pump device', '2016-11-08 18:01:13', '1');
INSERT INTO admin_log VALUES ('1174', '5', 'Test\'s', 'objects', '4', 'UPDATE', 'Object - Test\'s  deactivated', '2016-11-08 18:09:14', '1');
INSERT INTO admin_log VALUES ('1175', '5', 'Test\'s', 'objects', '4', 'UPDATE', 'Object - Test\'s  reactivated', '2016-11-08 18:09:23', '1');
INSERT INTO admin_log VALUES ('1176', '5', 'Level', 'device_measurements', '0000000008', 'DELETE', 'Device measurement - Level deleted', '2016-11-08 18:35:55', '1');
INSERT INTO admin_log VALUES ('1177', '5', '!Test 456', 'sites', '32', 'UPDATE', 'Site - !Test 456  deactivated', '2016-11-08 18:53:32', '1');
INSERT INTO admin_log VALUES ('1178', '5', '123 Give it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Give it to Me modified', '2016-11-08 19:19:42', '1');
INSERT INTO admin_log VALUES ('1179', '5', '123 Give it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Give it to Me modified', '2016-11-08 19:19:55', '1');
INSERT INTO admin_log VALUES ('1180', '5', '123 Give it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Give it to Me modified', '2016-11-08 19:22:22', '1');
INSERT INTO admin_log VALUES ('1181', '5', '123 Give it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Give it to Me modified', '2016-11-08 19:23:07', '1');
INSERT INTO admin_log VALUES ('1182', '5', '123 Give it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Give it to Me modified', '2016-11-08 19:24:09', '1');
INSERT INTO admin_log VALUES ('1183', '5', '123 Give it to Me', 'site_objects', '2', 'INSERT', '123 Give it to Me configured with Oil Tank object', '2016-11-08 19:24:09', '1');
INSERT INTO admin_log VALUES ('1184', '5', '123 Give it to Me', 'site_objects', '3', 'INSERT', '123 Give it to Me configured with Water Tank object', '2016-11-08 19:24:09', '1');
INSERT INTO admin_log VALUES ('1185', '5', 'Transducer', 'devices', '2', 'UPDATE', 'Device - Transducer modified', '2016-11-08 19:33:49', '1');
INSERT INTO admin_log VALUES ('1186', '5', 'Transducer', 'device_measurements', '0000000008', 'INSERT', 'Transducer configured with Level measurement', '2016-11-08 19:33:49', '1');
INSERT INTO admin_log VALUES ('1187', '5', 'Transducer', 'device_measurements', '0000000011', 'INSERT', 'Transducer configured with Pressure measurement', '2016-11-08 19:33:50', '1');
INSERT INTO admin_log VALUES ('1188', '5', 'Transducer', 'device_measurements', '0000000018', 'INSERT', 'Transducer configured with Cookin measurement', '2016-11-08 19:33:50', '1');
INSERT INTO admin_log VALUES ('1189', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  deactivated', '2016-11-08 19:35:24', '1');
INSERT INTO admin_log VALUES ('1190', '5', 'Becca\'s Object', 'objects', '5', 'INSERT', 'New object - Becca\'s Object added', '2016-11-08 19:38:52', '1');
INSERT INTO admin_log VALUES ('1191', '5', 'Becca\'s Object', 'object_devices', '1', 'INSERT', 'Becca\'s Object configured with Tank Stick device', '2016-11-08 19:38:52', '1');
INSERT INTO admin_log VALUES ('1192', '5', 'Becca\'s Object', 'object_devices', '2', 'INSERT', 'Becca\'s Object configured with Transducer device', '2016-11-08 19:38:52', '1');
INSERT INTO admin_log VALUES ('1193', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  reactivated', '2016-11-08 19:39:14', '1');
INSERT INTO admin_log VALUES ('1194', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  deactivated', '2016-11-08 19:39:35', '1');
INSERT INTO admin_log VALUES ('1195', '5', 'Level', 'measurements', '0000000008', 'UPDATE', 'Measurement - Level  deactivated', '2016-11-08 19:39:59', '1');
INSERT INTO admin_log VALUES ('1196', '5', 'Level', 'measurements', '0000000008', 'UPDATE', 'Measurement - Level  reactivated', '2016-11-08 19:41:26', '1');
INSERT INTO admin_log VALUES ('1197', '5', 'Level', 'measurements', '0000000008', 'UPDATE', 'Measurement - Level  deactivated', '2016-11-08 19:42:36', '1');
INSERT INTO admin_log VALUES ('1198', '1', 'John Miles', 'users', '41', 'UPDATE', 'User - jmiles  deactivated', '2016-11-08 19:55:49', '1');
INSERT INTO admin_log VALUES ('1199', '1', 'Jessica', 'sites', '24', 'UPDATE', 'Site - Jessica modified', '2016-11-08 19:58:53', '1');
INSERT INTO admin_log VALUES ('1200', '1', 'Jessica', 'site_objects', '3', 'INSERT', 'Jessica configured with Water Tank object', '2016-11-08 19:58:53', '1');
INSERT INTO admin_log VALUES ('1201', '1', 'Jessica', 'site_objects', '4', 'INSERT', 'Jessica configured with Test\'s object', '2016-11-08 19:58:53', '1');
INSERT INTO admin_log VALUES ('1202', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-08 21:10:15', '1');
INSERT INTO admin_log VALUES ('1203', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 04:45:53', '1');
INSERT INTO admin_log VALUES ('1204', '1', 'Administration', 'departments', '1', 'UPDATE', 'Department - Administration  deactivated', '2016-11-09 04:50:03', '1');
INSERT INTO admin_log VALUES ('1205', '1', 'Maintenance', 'departments', '6', 'UPDATE', 'Department - Maintenance modified', '2016-11-09 04:51:27', '1');
INSERT INTO admin_log VALUES ('1206', '1', 'Transfer Pump', 'devices', '3', 'UPDATE', 'Device - Transfer Pump  deactivated', '2016-11-09 04:54:03', '1');
INSERT INTO admin_log VALUES ('1207', '1', 'Transfer Pump', 'devices', '3', 'UPDATE', 'Device - Transfer Pump  reactivated', '2016-11-09 04:54:09', '1');
INSERT INTO admin_log VALUES ('1208', '1', 'Temperature', 'measurements', '0000000009', 'UPDATE', 'Measurement - Temperature  deactivated', '2016-11-09 04:55:20', '1');
INSERT INTO admin_log VALUES ('1209', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 05:36:21', '1');
INSERT INTO admin_log VALUES ('1210', '1', 'Fred\'s Drilling', 'accounts', '3', 'UPDATE', 'Account - Fred\'s Drilling  deactivated', '2016-11-09 05:50:33', '1');
INSERT INTO admin_log VALUES ('1211', '1', 'Fred\'s Drilling', 'accounts', '3', 'UPDATE', 'Account - Fred\'s Drilling  reactivated', '2016-11-09 05:50:39', '1');
INSERT INTO admin_log VALUES ('1212', '1', 'Fred\'s Drilling1', 'accounts', '3', 'UPDATE', 'Account - Fred\'s Drilling1 modified', '2016-11-09 05:50:58', '1');
INSERT INTO admin_log VALUES ('1213', '1', 'Fred\'s Drilling', 'accounts', '3', 'UPDATE', 'Account - Fred\'s Drilling modified', '2016-11-09 05:51:14', '1');
INSERT INTO admin_log VALUES ('1214', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 07:17:50', '1');
INSERT INTO admin_log VALUES ('1215', '1', 'Oil Tank1', 'objects', '2', 'UPDATE', 'Object - Oil Tank1 modified', '2016-11-09 07:18:17', '1');
INSERT INTO admin_log VALUES ('1216', '1', 'Oil Tank1', 'object_devices', '1', 'INSERT', 'Oil Tank1 configured with Tank Stick device', '2016-11-09 07:18:17', '1');
INSERT INTO admin_log VALUES ('1217', '1', 'Oil Tank1', 'object_devices', '2', 'INSERT', 'Oil Tank1 configured with Transducer device', '2016-11-09 07:18:17', '1');
INSERT INTO admin_log VALUES ('1218', '1', 'Oil Tank1', 'object_devices', '3', 'INSERT', 'Oil Tank1 configured with Transfer Pump device', '2016-11-09 07:18:17', '1');
INSERT INTO admin_log VALUES ('1219', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-09 07:18:31', '1');
INSERT INTO admin_log VALUES ('1220', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-09 07:18:31', '1');
INSERT INTO admin_log VALUES ('1221', '1', 'Oil Tank', 'object_devices', '2', 'INSERT', 'Oil Tank configured with Transducer device', '2016-11-09 07:18:31', '1');
INSERT INTO admin_log VALUES ('1222', '1', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-09 07:18:31', '1');
INSERT INTO admin_log VALUES ('1223', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank  deactivated', '2016-11-09 07:23:26', '1');
INSERT INTO admin_log VALUES ('1224', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-09 07:23:40', '1');
INSERT INTO admin_log VALUES ('1225', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-09 07:23:40', '1');
INSERT INTO admin_log VALUES ('1226', '1', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-09 07:23:40', '1');
INSERT INTO admin_log VALUES ('1227', '1', '1234', 'objects', '6', 'INSERT', 'New object - 1234 added', '2016-11-09 07:24:44', '1');
INSERT INTO admin_log VALUES ('1228', '1', '1234', 'object_devices', '1', 'INSERT', '1234 configured with Tank Stick device', '2016-11-09 07:24:44', '1');
INSERT INTO admin_log VALUES ('1229', '1', '1234', 'object_devices', '9', 'INSERT', '1234 configured with Lucy\'s device', '2016-11-09 07:24:44', '1');
INSERT INTO admin_log VALUES ('1230', '1', 'Lucy\'s1', 'devices', '9', 'UPDATE', 'Device - Lucy\'s1 modified', '2016-11-09 07:32:23', '1');
INSERT INTO admin_log VALUES ('1231', '1', 'Lucy\'s1', 'device_measurements', '0000000011', 'INSERT', 'Lucy\'s1 configured with Pressure measurement', '2016-11-09 07:32:23', '1');
INSERT INTO admin_log VALUES ('1232', '1', 'Lucy\'s1', 'device_measurements', '0000000018', 'INSERT', 'Lucy\'s1 configured with Cookin measurement', '2016-11-09 07:32:23', '1');
INSERT INTO admin_log VALUES ('1233', '1', 'Lucy\'s', 'devices', '9', 'UPDATE', 'Device - Lucy\'s modified', '2016-11-09 07:32:38', '1');
INSERT INTO admin_log VALUES ('1234', '1', 'Lucy\'s', 'device_measurements', '0000000011', 'INSERT', 'Lucy\'s configured with Pressure measurement', '2016-11-09 07:32:38', '1');
INSERT INTO admin_log VALUES ('1235', '1', 'Lucy\'s', 'devices', '9', 'UPDATE', 'Device - Lucy\'s  deactivated', '2016-11-09 07:33:05', '1');
INSERT INTO admin_log VALUES ('1236', '1', 'Lucy\'s', 'devices', '9', 'UPDATE', 'Device - Lucy\'s  reactivated', '2016-11-09 07:33:11', '1');
INSERT INTO admin_log VALUES ('1237', '1', 'Lucy\'s', 'devices', '9', 'DELETE', 'Device - Lucy\'s deleted', '2016-11-09 07:33:31', '1');
INSERT INTO admin_log VALUES ('1238', '1', '1234', 'devices', '11', 'INSERT', 'New device - 1234 added', '2016-11-09 07:34:03', '1');
INSERT INTO admin_log VALUES ('1239', '1', '1234', 'device_measurements', '0000000011', 'INSERT', '1234 configured with Pressure measurement', '2016-11-09 07:34:03', '1');
INSERT INTO admin_log VALUES ('1240', '1', 'Cookin', 'device_measurements', '0000000018', 'DELETE', 'Device measurement - Cookin deleted', '2016-11-09 07:40:19', '1');
INSERT INTO admin_log VALUES ('1241', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 10:00:19', '1');
INSERT INTO admin_log VALUES ('1242', '1', 'Becca', 'measurements', '19', 'INSERT', 'New measurement - Becca added', '2016-11-09 10:04:42', '1');
INSERT INTO admin_log VALUES ('1243', '1', 'Becca', 'measurement_tags', '1', 'INSERT', 'Becca configured with AST tag', '2016-11-09 10:04:43', '1');
INSERT INTO admin_log VALUES ('1244', '1', 'Becca', 'measurement_tags', '5', 'INSERT', 'Becca configured with CST tag', '2016-11-09 10:04:43', '1');
INSERT INTO admin_log VALUES ('1245', '1', 'Becca', 'measurements', '0000000019', 'UPDATE', 'Measurement - Becca  deactivated', '2016-11-09 10:05:18', '1');
INSERT INTO admin_log VALUES ('1246', '1', 'Becca1', 'measurements', '0000000019', 'UPDATE', 'Measurement - Becca1 modified', '2016-11-09 10:05:41', '1');
INSERT INTO admin_log VALUES ('1247', '1', 'Becca1', 'measurement_tags', '1', 'INSERT', 'Becca1 configured with AST tag', '2016-11-09 10:05:41', '1');
INSERT INTO admin_log VALUES ('1248', '1', 'Becca1', 'measurement_tags', '3', 'INSERT', 'Becca1 configured with BST tag', '2016-11-09 10:05:41', '1');
INSERT INTO admin_log VALUES ('1249', '1', 'Becca1', 'measurement_tags', '5', 'INSERT', 'Becca1 configured with CST tag', '2016-11-09 10:05:41', '1');
INSERT INTO admin_log VALUES ('1250', '1', 'Pressure', 'measurements', '0000000011', 'UPDATE', 'Measurement - Pressure modified', '2016-11-09 10:06:11', '1');
INSERT INTO admin_log VALUES ('1251', '1', 'Pressure', 'measurement_tags', '1', 'INSERT', 'Pressure configured with AST tag', '2016-11-09 10:06:11', '1');
INSERT INTO admin_log VALUES ('1252', '1', 'Pressure', 'measurement_tags', '3', 'INSERT', 'Pressure configured with BST tag', '2016-11-09 10:06:11', '1');
INSERT INTO admin_log VALUES ('1253', '1', 'Pressure', 'measurements', '0000000011', 'UPDATE', 'Measurement - Pressure modified', '2016-11-09 10:06:36', '1');
INSERT INTO admin_log VALUES ('1254', '1', 'Pressure', 'measurement_tags', '3', 'INSERT', 'Pressure configured with BST tag', '2016-11-09 10:06:36', '1');
INSERT INTO admin_log VALUES ('1255', '1', 'Becca1', 'measurements', '0000000019', 'UPDATE', 'Measurement - Becca1 modified', '2016-11-09 10:07:05', '1');
INSERT INTO admin_log VALUES ('1256', '1', 'Becca1', 'measurements', '0000000019', 'UPDATE', 'Measurement - Becca1 modified', '2016-11-09 10:07:18', '1');
INSERT INTO admin_log VALUES ('1257', '1', 'Becca1', 'measurement_tags', '1', 'INSERT', 'Becca1 configured with AST tag', '2016-11-09 10:07:18', '1');
INSERT INTO admin_log VALUES ('1258', '1', 'Becca1', 'measurement_tags', '3', 'INSERT', 'Becca1 configured with BST tag', '2016-11-09 10:07:18', '1');
INSERT INTO admin_log VALUES ('1259', '1', 'Becca1', 'measurement_tags', '5', 'INSERT', 'Becca1 configured with CST tag', '2016-11-09 10:07:18', '1');
INSERT INTO admin_log VALUES ('1260', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Measurement - Level modified', '2016-11-09 10:07:36', '1');
INSERT INTO admin_log VALUES ('1261', '1', 'Level', 'measurement_tags', '1', 'INSERT', 'Level configured with AST tag', '2016-11-09 10:07:37', '1');
INSERT INTO admin_log VALUES ('1262', '1', 'Level', 'measurement_tags', '3', 'INSERT', 'Level configured with BST tag', '2016-11-09 10:07:37', '1');
INSERT INTO admin_log VALUES ('1263', '1', 'Level', 'measurements', '0000000008', 'UPDATE', 'Measurement - Level modified', '2016-11-09 10:08:33', '1');
INSERT INTO admin_log VALUES ('1264', '1', 'Level', 'measurement_tags', '1', 'INSERT', 'Level configured with AST tag', '2016-11-09 10:08:33', '1');
INSERT INTO admin_log VALUES ('1265', '1', 'Level', 'measurement_tags', '3', 'INSERT', 'Level configured with BST tag', '2016-11-09 10:08:34', '1');
INSERT INTO admin_log VALUES ('1266', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 14:46:32', '1');
INSERT INTO admin_log VALUES ('1267', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 17:06:57', '1');
INSERT INTO admin_log VALUES ('1268', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 17:32:45', '1');
INSERT INTO admin_log VALUES ('1269', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 19:36:54', '1');
INSERT INTO admin_log VALUES ('1270', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:41:43', '1');
INSERT INTO admin_log VALUES ('1271', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:41:43', '1');
INSERT INTO admin_log VALUES ('1272', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:41:46', '1');
INSERT INTO admin_log VALUES ('1273', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:41:47', '1');
INSERT INTO admin_log VALUES ('1274', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:41:47', '1');
INSERT INTO admin_log VALUES ('1275', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:41:47', '1');
INSERT INTO admin_log VALUES ('1276', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:41:47', '1');
INSERT INTO admin_log VALUES ('1277', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:41:48', '1');
INSERT INTO admin_log VALUES ('1278', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:41:48', '1');
INSERT INTO admin_log VALUES ('1279', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:41:48', '1');
INSERT INTO admin_log VALUES ('1280', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:41:54', '1');
INSERT INTO admin_log VALUES ('1281', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:41:54', '1');
INSERT INTO admin_log VALUES ('1282', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:42:12', '1');
INSERT INTO admin_log VALUES ('1283', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:42:12', '1');
INSERT INTO admin_log VALUES ('1284', '1', 'Level1', 'measurements', '8', 'UPDATE', 'Measurement - Level1 modified', '2016-11-09 19:42:22', '1');
INSERT INTO admin_log VALUES ('1285', '1', 'Level1', 'measurement_tags', '3', 'INSERT', 'Level1 configured with BST tag', '2016-11-09 19:42:22', '1');
INSERT INTO admin_log VALUES ('1286', '1', 'Level', 'measurements', '8', 'UPDATE', 'Measurement - Level modified', '2016-11-09 19:42:37', '1');
INSERT INTO admin_log VALUES ('1287', '1', 'Level', 'measurement_tags', '3', 'INSERT', 'Level configured with BST tag', '2016-11-09 19:42:37', '1');
INSERT INTO admin_log VALUES ('1288', '1', '123', 'measurements', '20', 'INSERT', 'New measurement - 123 added', '2016-11-09 19:43:05', '1');
INSERT INTO admin_log VALUES ('1289', '1', '123', 'measurement_tags', '1', 'INSERT', '123 configured with AST tag', '2016-11-09 19:43:05', '1');
INSERT INTO admin_log VALUES ('1290', '1', '123', 'measurement_tags', '5', 'INSERT', '123 configured with CST tag', '2016-11-09 19:43:05', '1');
INSERT INTO admin_log VALUES ('1291', '1', '123', 'measurements', '20', 'UPDATE', 'Measurement - 123  deactivated', '2016-11-09 19:43:22', '1');
INSERT INTO admin_log VALUES ('1292', '1', '123', 'measurements', '20', 'UPDATE', 'Measurement - 123 modified', '2016-11-09 19:43:37', '1');
INSERT INTO admin_log VALUES ('1293', '1', '123', 'measurement_tags', '1', 'INSERT', '123 configured with AST tag', '2016-11-09 19:43:37', '1');
INSERT INTO admin_log VALUES ('1294', '1', '123', 'measurement_tags', '5', 'INSERT', '123 configured with CST tag', '2016-11-09 19:43:37', '1');
INSERT INTO admin_log VALUES ('1295', '1', '123', 'measurements', '20', 'DELETE', 'Measurement - 123 deleted', '2016-11-09 19:43:48', '1');
INSERT INTO admin_log VALUES ('1296', '1', '123', 'tags', '6', 'INSERT', 'New data point - 123 added', '2016-11-09 19:44:48', '1');
INSERT INTO admin_log VALUES ('1297', '1', '123456', 'tags', '6', 'UPDATE', 'Data point - 123456 modified', '2016-11-09 19:45:02', '1');
INSERT INTO admin_log VALUES ('1298', '1', '123456', 'tags', '6', 'UPDATE', 'Data point - 123456 modified', '2016-11-09 19:45:11', '1');
INSERT INTO admin_log VALUES ('1299', '1', '123456', 'tags', '6', 'UPDATE', 'Tag - 123456  deactivated', '2016-11-09 19:45:18', '1');
INSERT INTO admin_log VALUES ('1300', '1', '123456', 'tags', '6', 'UPDATE', 'Tag - 123456  reactivated', '2016-11-09 19:45:23', '1');
INSERT INTO admin_log VALUES ('1301', '1', '123456', 'tags', '6', 'DELETE', 'Tag - 123456 deleted', '2016-11-09 19:45:30', '1');
INSERT INTO admin_log VALUES ('1302', '1', 'Accounts', 'modules', '22', 'UPDATE', 'Module - Accounts  reactivated', '2016-11-09 19:46:43', '1');
INSERT INTO admin_log VALUES ('1303', '1', 'Accounts', 'modules', '22', 'UPDATE', 'Module - Accounts  reactivated', '2016-11-09 19:47:00', '1');
INSERT INTO admin_log VALUES ('1304', '1', 'Admin1', 'modules', '20', 'UPDATE', 'Module - Admin1 modified', '2016-11-09 20:14:52', '1');
INSERT INTO admin_log VALUES ('1305', '1', 'Admin', 'modules', '20', 'UPDATE', 'Module - Admin modified', '2016-11-09 20:15:02', '1');
INSERT INTO admin_log VALUES ('1306', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 21:06:17', '1');
INSERT INTO admin_log VALUES ('1307', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-09 22:32:18', '1');
INSERT INTO admin_log VALUES ('1308', '1', '123', 'sites', '35', 'INSERT', 'New site - 123 added', '2016-11-09 23:00:47', '1');
INSERT INTO admin_log VALUES ('1309', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 00:04:47', '1');
INSERT INTO admin_log VALUES ('1310', '1', '456', 'objects', '7', 'INSERT', 'New object - 456 added', '2016-11-10 00:05:26', '1');
INSERT INTO admin_log VALUES ('1311', '1', '456', 'object_devices', '1', 'INSERT', '456 configured with Tank Stick device', '2016-11-10 00:05:26', '1');
INSERT INTO admin_log VALUES ('1312', '1', '456', 'object_devices', '2', 'INSERT', '456 configured with Transducer device', '2016-11-10 00:05:26', '1');
INSERT INTO admin_log VALUES ('1313', '1', '456s', 'objects', '7', 'UPDATE', 'Object - 456s modified', '2016-11-10 00:05:53', '1');
INSERT INTO admin_log VALUES ('1314', '1', '456s', 'object_devices', '1', 'INSERT', '456s configured with Tank Stick device', '2016-11-10 00:05:53', '1');
INSERT INTO admin_log VALUES ('1315', '1', '456s', 'object_devices', '11', 'INSERT', '456s configured with 1234 device', '2016-11-10 00:05:53', '1');
INSERT INTO admin_log VALUES ('1316', '1', '456s', 'objects', '7', 'DELETE', 'Object - 456s deleted', '2016-11-10 00:06:03', '1');
INSERT INTO admin_log VALUES ('1317', '1', 'aaa', 'devices', '12', 'INSERT', 'New device - aaa added', '2016-11-10 00:06:40', '1');
INSERT INTO admin_log VALUES ('1318', '1', 'aaa', 'device_measurements', '11', 'INSERT', 'aaa configured with Pressure measurement', '2016-11-10 00:06:40', '1');
INSERT INTO admin_log VALUES ('1319', '1', 'aaa', 'device_measurements', '19', 'INSERT', 'aaa configured with Becca1 measurement', '2016-11-10 00:06:40', '1');
INSERT INTO admin_log VALUES ('1320', '1', 'aaa', 'devices', '12', 'UPDATE', 'Device - aaa  deactivated', '2016-11-10 00:06:48', '1');
INSERT INTO admin_log VALUES ('1321', '1', 'aaa', 'devices', '12', 'UPDATE', 'Device - aaa  reactivated', '2016-11-10 00:07:23', '1');
INSERT INTO admin_log VALUES ('1322', '1', '1234', 'objects', '6', 'UPDATE', 'Object - 1234 modified', '2016-11-10 00:07:40', '1');
INSERT INTO admin_log VALUES ('1323', '1', '1234', 'object_devices', '1', 'INSERT', '1234 configured with Tank Stick device', '2016-11-10 00:07:40', '1');
INSERT INTO admin_log VALUES ('1324', '1', '1234', 'object_devices', '12', 'INSERT', '1234 configured with aaa device', '2016-11-10 00:07:41', '1');
INSERT INTO admin_log VALUES ('1325', '1', '111111', 'measurements', '21', 'INSERT', 'New measurement - 111111 added', '2016-11-10 00:08:03', '1');
INSERT INTO admin_log VALUES ('1326', '1', '111111', 'measurement_tags', '1', 'INSERT', '111111 configured with AST tag', '2016-11-10 00:08:03', '1');
INSERT INTO admin_log VALUES ('1327', '1', 'ccccccc', 'measurements', '21', 'UPDATE', 'Measurement - ccccccc modified', '2016-11-10 00:08:17', '1');
INSERT INTO admin_log VALUES ('1328', '1', 'ccccccc', 'measurement_tags', '1', 'INSERT', 'ccccccc configured with AST tag', '2016-11-10 00:08:17', '1');
INSERT INTO admin_log VALUES ('1329', '1', 'ccccccc', 'measurements', '21', 'DELETE', 'Measurement - ccccccc deleted', '2016-11-10 00:08:23', '1');
INSERT INTO admin_log VALUES ('1330', '1', 'MAS', 'tags', '7', 'INSERT', 'New data point - MAS added', '2016-11-10 00:10:07', '1');
INSERT INTO admin_log VALUES ('1331', '1', 'MAS', 'tags', '7', 'UPDATE', 'Data point - MAS modified', '2016-11-10 00:10:23', '1');
INSERT INTO admin_log VALUES ('1332', '1', 'MAS', 'tags', '7', 'DELETE', 'Tag - MAS deleted', '2016-11-10 00:10:30', '1');
INSERT INTO admin_log VALUES ('1333', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 01:21:39', '1');
INSERT INTO admin_log VALUES ('1334', '1', 'gas', 'tags', '8', 'INSERT', 'New data point - gas added', '2016-11-10 01:24:26', '1');
INSERT INTO admin_log VALUES ('1335', '1', 'gas', 'tags', '8', 'UPDATE', 'Tag - gas  deactivated', '2016-11-10 01:24:38', '1');
INSERT INTO admin_log VALUES ('1336', '1', 'name', 'measurements', '22', 'INSERT', 'New measurement - name added', '2016-11-10 01:28:10', '1');
INSERT INTO admin_log VALUES ('1337', '1', 'name', 'measurement_tags', '1', 'INSERT', 'name configured with AST tag', '2016-11-10 01:28:10', '1');
INSERT INTO admin_log VALUES ('1338', '1', 'hanz', 'devices', '13', 'INSERT', 'New device - hanz added', '2016-11-10 01:39:29', '1');
INSERT INTO admin_log VALUES ('1339', '1', 'hanz', 'device_measurements', '8', 'INSERT', 'hanz configured with Level measurement', '2016-11-10 01:39:30', '1');
INSERT INTO admin_log VALUES ('1340', '1', 'hanz', 'device_measurements', '11', 'INSERT', 'hanz configured with Pressure measurement', '2016-11-10 01:39:30', '1');
INSERT INTO admin_log VALUES ('1341', '1', 'hanz', 'device_measurements', '18', 'INSERT', 'hanz configured with Cookin measurement', '2016-11-10 01:39:30', '1');
INSERT INTO admin_log VALUES ('1342', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 03:23:17', '1');
INSERT INTO admin_log VALUES ('1343', '1', 'fred', 'sites', '36', 'INSERT', 'New site - fred added', '2016-11-10 03:23:46', '1');
INSERT INTO admin_log VALUES ('1344', '1', '1234', 'departments', '0', 'INSERT', 'New department - 1234 added', '2016-11-10 03:30:35', '1');
INSERT INTO admin_log VALUES ('1345', '1', '1234567', 'departments', '0', 'UPDATE', 'Department - 1234567 modified', '2016-11-10 04:00:48', '1');
INSERT INTO admin_log VALUES ('1346', '1', '1234567', 'departments', '0', 'UPDATE', 'Department - 1234567 modified', '2016-11-10 04:00:58', '1');
INSERT INTO admin_log VALUES ('1347', '1', '456', 'devices', '14', 'INSERT', 'New device - 456 added', '2016-11-10 04:02:52', '1');
INSERT INTO admin_log VALUES ('1348', '1', '456', 'device_measurements', '11', 'INSERT', '456 configured with Pressure measurement', '2016-11-10 04:02:53', '1');
INSERT INTO admin_log VALUES ('1349', '1', '456a', 'devices', '14', 'UPDATE', 'Device - 456a modified', '2016-11-10 04:03:13', '1');
INSERT INTO admin_log VALUES ('1350', '1', '456a', 'device_measurements', '8', 'INSERT', '456a configured with Level measurement', '2016-11-10 04:03:13', '1');
INSERT INTO admin_log VALUES ('1351', '1', '456a', 'device_measurements', '11', 'INSERT', '456a configured with Pressure measurement', '2016-11-10 04:03:13', '1');
INSERT INTO admin_log VALUES ('1352', '1', '456a', 'devices', '14', 'UPDATE', 'Device - 456a  reactivated', '2016-11-10 04:03:26', '1');
INSERT INTO admin_log VALUES ('1353', '1', '123456', 'groups', '0', 'INSERT', 'New group - 123456 added', '2016-11-10 04:03:58', '1');
INSERT INTO admin_log VALUES ('1354', '1', '123456a', 'groups', '0', 'UPDATE', 'Group - 123456a modified', '2016-11-10 04:04:13', '1');
INSERT INTO admin_log VALUES ('1355', '1', '123456a', 'groups', '0', 'UPDATE', 'Group - 123456a modified', '2016-11-10 04:04:29', '1');
INSERT INTO admin_log VALUES ('1356', '1', '1234', 'measurements', '23', 'INSERT', 'New measurement - 1234 added', '2016-11-10 04:05:35', '1');
INSERT INTO admin_log VALUES ('1357', '1', '1234', 'measurement_tags', '1', 'INSERT', '1234 configured with AST tag', '2016-11-10 04:05:35', '1');
INSERT INTO admin_log VALUES ('1358', '1', '1234', 'measurement_tags', '5', 'INSERT', '1234 configured with CST tag', '2016-11-10 04:05:36', '1');
INSERT INTO admin_log VALUES ('1359', '1', '1234a', 'measurements', '23', 'UPDATE', 'Measurement - 1234a modified', '2016-11-10 04:06:02', '1');
INSERT INTO admin_log VALUES ('1360', '1', '1234a', 'measurement_tags', '1', 'INSERT', '1234a configured with AST tag', '2016-11-10 04:06:02', '1');
INSERT INTO admin_log VALUES ('1361', '1', 'Temperature', 'measurements', '9', 'UPDATE', 'Measurement - Temperature  reactivated', '2016-11-10 04:06:57', '1');
INSERT INTO admin_log VALUES ('1362', '1', '1234a', 'measurements', '23', 'UPDATE', 'Measurement - 1234a  reactivated', '2016-11-10 04:07:08', '1');
INSERT INTO admin_log VALUES ('1363', '1', '1234aaa', 'measurements', '24', 'INSERT', 'New measurement - 1234aaa added', '2016-11-10 04:15:46', '1');
INSERT INTO admin_log VALUES ('1364', '1', '1234aaa', 'measurement_tags', '1', 'INSERT', '1234aaa configured with AST tag', '2016-11-10 04:15:46', '1');
INSERT INTO admin_log VALUES ('1365', '1', '1234aaa444', 'measurements', '24', 'UPDATE', 'Measurement - 1234aaa444 modified', '2016-11-10 04:16:08', '1');
INSERT INTO admin_log VALUES ('1366', '1', '1234aaa444', 'measurement_tags', '1', 'INSERT', '1234aaa444 configured with AST tag', '2016-11-10 04:16:08', '1');
INSERT INTO admin_log VALUES ('1367', '1', '1234aaa444', 'measurement_tags', '3', 'INSERT', '1234aaa444 configured with BST tag', '2016-11-10 04:16:08', '1');
INSERT INTO admin_log VALUES ('1368', '1', '1234aaa444', 'measurements', '24', 'UPDATE', 'Measurement - 1234aaa444 modified', '2016-11-10 04:16:30', '1');
INSERT INTO admin_log VALUES ('1369', '1', '1234aaa444', 'measurement_tags', '3', 'INSERT', '1234aaa444 configured with BST tag', '2016-11-10 04:16:30', '1');
INSERT INTO admin_log VALUES ('1370', '1', '1234aaa444', 'measurements', '24', 'UPDATE', 'Measurement - 1234aaa444  deactivated', '2016-11-10 04:16:37', '1');
INSERT INTO admin_log VALUES ('1371', '1', '123', 'modules', '42', 'INSERT', 'New module - 123 created', '2016-11-10 04:25:00', '1');
INSERT INTO admin_log VALUES ('1372', '1', '123aa', 'modules', '42', 'UPDATE', 'Module - 123aa modified', '2016-11-10 04:25:17', '1');
INSERT INTO admin_log VALUES ('1373', '1', '123aa', 'modules', '42', 'DELETE', 'Module - 123aa deleted', '2016-11-10 04:25:24', '1');
INSERT INTO admin_log VALUES ('1374', '1', 'Dashboard', 'modules', '39', 'UPDATE', 'Module - Dashboard modified', '2016-11-10 04:25:35', '1');
INSERT INTO admin_log VALUES ('1375', '1', 'sdtwgbw', 'objects', '8', 'INSERT', 'New object - sdtwgbw added', '2016-11-10 04:28:27', '1');
INSERT INTO admin_log VALUES ('1376', '1', 'sdtwgbw', 'object_devices', '1', 'INSERT', 'sdtwgbw configured with Tank Stick device', '2016-11-10 04:28:27', '1');
INSERT INTO admin_log VALUES ('1377', '1', 'sdtwgbw', 'objects', '8', 'UPDATE', 'Object - sdtwgbw modified', '2016-11-10 04:28:41', '1');
INSERT INTO admin_log VALUES ('1378', '1', 'sdtwgbw', 'object_devices', '1', 'INSERT', 'sdtwgbw configured with Tank Stick device', '2016-11-10 04:28:41', '1');
INSERT INTO admin_log VALUES ('1379', '1', 'sdtwgbw', 'object_devices', '11', 'INSERT', 'sdtwgbw configured with 1234 device', '2016-11-10 04:28:41', '1');
INSERT INTO admin_log VALUES ('1380', '1', 'sdtwgbw', 'object_devices', '13', 'INSERT', 'sdtwgbw configured with hanz device', '2016-11-10 04:28:41', '1');
INSERT INTO admin_log VALUES ('1381', '1', 'sdtwgbw', 'objects', '8', 'UPDATE', 'Object - sdtwgbw modified', '2016-11-10 04:28:53', '1');
INSERT INTO admin_log VALUES ('1382', '1', 'sdtwgbw', 'object_devices', '1', 'INSERT', 'sdtwgbw configured with Tank Stick device', '2016-11-10 04:28:53', '1');
INSERT INTO admin_log VALUES ('1383', '1', 'sdtwgbw', 'object_devices', '11', 'INSERT', 'sdtwgbw configured with 1234 device', '2016-11-10 04:28:53', '1');
INSERT INTO admin_log VALUES ('1384', '1', 'sdtwgbw', 'object_devices', '13', 'INSERT', 'sdtwgbw configured with hanz device', '2016-11-10 04:28:53', '1');
INSERT INTO admin_log VALUES ('1385', '1', 'sdtwgbw', 'objects', '8', 'UPDATE', 'Object - sdtwgbw  deactivated', '2016-11-10 04:28:57', '1');
INSERT INTO admin_log VALUES ('1386', '1', 'sdtwgbw', 'objects', '8', 'UPDATE', 'Object - sdtwgbw  reactivated', '2016-11-10 04:29:01', '1');
INSERT INTO admin_log VALUES ('1387', '1', 'wr5ty5y', 'sites', '37', 'INSERT', 'New site - wr5ty5y added', '2016-11-10 04:30:19', '1');
INSERT INTO admin_log VALUES ('1388', '1', 'wr5ty5y', 'site_objects', '2', 'INSERT', 'wr5ty5y configured with Oil Tank object', '2016-11-10 04:30:19', '1');
INSERT INTO admin_log VALUES ('1389', '1', 'wr5ty5yaaa', 'sites', '37', 'UPDATE', 'Site - wr5ty5yaaa modified', '2016-11-10 04:39:11', '1');
INSERT INTO admin_log VALUES ('1390', '1', 'wr5ty5yaaa', 'site_objects', '2', 'INSERT', 'wr5ty5yaaa configured with Oil Tank object', '2016-11-10 04:39:11', '1');
INSERT INTO admin_log VALUES ('1391', '1', 'wr5ty5yaaa', 'site_objects', '5', 'INSERT', 'wr5ty5yaaa configured with Becca\'s Object object', '2016-11-10 04:39:11', '1');
INSERT INTO admin_log VALUES ('1392', '1', 'aaaaaaaaaaa', 'tags', '9', 'INSERT', 'New data point - aaaaaaaaaaa added', '2016-11-10 04:40:17', '1');
INSERT INTO admin_log VALUES ('1393', '1', 'aaaaaaaaaaa123', 'tags', '9', 'UPDATE', 'Data point - aaaaaaaaaaa123 modified', '2016-11-10 04:40:32', '1');
INSERT INTO admin_log VALUES ('1394', '1', 'aaaaaaaaaaa123', 'tags', '9', 'UPDATE', 'Data point - aaaaaaaaaaa123 modified', '2016-11-10 04:40:50', '1');
INSERT INTO admin_log VALUES ('1395', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 12:57:28', '1');
INSERT INTO admin_log VALUES ('1396', '1', 'Savannah McCarroll', 'users', '43', 'INSERT', 'New user - smccarroll added', '2016-11-10 12:58:38', '1');
INSERT INTO admin_log VALUES ('1397', '1', 'Savannah McCarroll', 'user_departments', '43', 'INSERT', 'Department - 1234567 assigned', '2016-11-10 12:58:38', '1');
INSERT INTO admin_log VALUES ('1398', '1', 'Savannah McCarroll', 'user_groups', '43', 'INSERT', 'Group - Super User assigned', '2016-11-10 12:58:38', '1');
INSERT INTO admin_log VALUES ('1399', '1', 'Roy McCarroll', 'users', '44', 'INSERT', 'New user - rmccarroll added', '2016-11-10 13:11:25', '1');
INSERT INTO admin_log VALUES ('1400', '1', 'Roy McCarroll', 'user_departments', '44', 'INSERT', 'Department - 1234567 assigned', '2016-11-10 13:11:25', '1');
INSERT INTO admin_log VALUES ('1401', '1', 'Roy McCarroll', 'user_groups', '44', 'INSERT', 'Group - Super User assigned', '2016-11-10 13:11:25', '1');
INSERT INTO admin_log VALUES ('1402', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 15:46:18', '1');
INSERT INTO admin_log VALUES ('1403', '1', 'John Levinski', 'users', '45', 'INSERT', 'New user - jlevinski added', '2016-11-10 15:52:42', '1');
INSERT INTO admin_log VALUES ('1404', '1', 'John Levinski', 'user_departments', '45', 'INSERT', 'Department - wwww123 assigned', '2016-11-10 15:52:42', '1');
INSERT INTO admin_log VALUES ('1405', '1', 'John Levinski', 'user_departments', '45', 'INSERT', 'Department - Fred assigned', '2016-11-10 15:52:42', '1');
INSERT INTO admin_log VALUES ('1406', '1', 'John Levinski', 'user_groups', '45', 'INSERT', 'Group - Account Admin assigned', '2016-11-10 15:52:42', '1');
INSERT INTO admin_log VALUES ('1407', '1', 'John Levinski', 'user_groups', '45', 'INSERT', 'Group - Manager assigned', '2016-11-10 15:52:42', '1');
INSERT INTO admin_log VALUES ('1408', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 19:53:29', '1');
INSERT INTO admin_log VALUES ('1409', '1', 'John Levinski', 'users', '45', 'UPDATE', 'User - jlevinski modified', '2016-11-10 19:53:54', '1');
INSERT INTO admin_log VALUES ('1410', '1', 'John Levinski', 'users', '45', 'UPDATE', 'User - jlevinski  reactivated', '2016-11-10 20:04:21', '1');
INSERT INTO admin_log VALUES ('1411', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-10 21:50:43', '1');
INSERT INTO admin_log VALUES ('1412', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-11 03:44:03', '1');
INSERT INTO admin_log VALUES ('1413', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-11 04:09:10', '1');
INSERT INTO admin_log VALUES ('1414', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-14 17:07:48', '1');
INSERT INTO admin_log VALUES ('1415', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-14 19:37:17', '1');
INSERT INTO admin_log VALUES ('1416', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-14 21:27:45', '1');
INSERT INTO admin_log VALUES ('1417', '1', 'System', 'modules', '21', 'UPDATE', 'Module - System modified', '2016-11-14 21:28:21', '1');
INSERT INTO admin_log VALUES ('1418', '1', 'Departments', 'modules', '27', 'UPDATE', 'Module - Departments modified', '2016-11-14 21:28:45', '1');
INSERT INTO admin_log VALUES ('1419', '1', 'Groups', 'modules', '28', 'UPDATE', 'Module - Groups modified', '2016-11-14 21:28:55', '1');
INSERT INTO admin_log VALUES ('1420', '1', 'Sites', 'modules', '29', 'UPDATE', 'Module - Sites modified', '2016-11-14 21:29:03', '1');
INSERT INTO admin_log VALUES ('1421', '1', 'Users', 'modules', '30', 'UPDATE', 'Module - Users modified', '2016-11-14 21:29:16', '1');
INSERT INTO admin_log VALUES ('1422', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-14 23:24:00', '0');
INSERT INTO admin_log VALUES ('1423', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-15 01:56:12', '0');
INSERT INTO admin_log VALUES ('1424', '1', 'Sales', 'departments', '7', 'UPDATE', 'Department - Sales modified', '2016-11-15 02:15:24', '1');
INSERT INTO admin_log VALUES ('1425', '1', 'Fred', 'departments', '9', 'DELETE', 'Department - Fred deleted', '2016-11-15 02:15:32', '1');
INSERT INTO admin_log VALUES ('1426', '1', 'IT', 'departments', '0', 'UPDATE', 'Department - IT modified', '2016-11-15 02:16:01', '1');
INSERT INTO admin_log VALUES ('1427', '1', '123456a', 'groups', '0', 'DELETE', 'Group - 123456a deleted', '2016-11-15 02:16:20', '1');
INSERT INTO admin_log VALUES ('1428', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:17:09', '0');
INSERT INTO admin_log VALUES ('1429', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:17:12', '0');
INSERT INTO admin_log VALUES ('1430', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:17:14', '0');
INSERT INTO admin_log VALUES ('1431', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:17:14', '0');
INSERT INTO admin_log VALUES ('1432', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:17:14', '0');
INSERT INTO admin_log VALUES ('1433', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:17:23', '0');
INSERT INTO admin_log VALUES ('1434', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:18:42', '0');
INSERT INTO admin_log VALUES ('1435', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-15 02:19:09', '0');
INSERT INTO admin_log VALUES ('1436', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:19:33', '0');
INSERT INTO admin_log VALUES ('1437', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-15 02:20:36', '0');
INSERT INTO admin_log VALUES ('1438', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-15 02:22:01', '0');
INSERT INTO admin_log VALUES ('1439', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-15 05:36:17', '0');
INSERT INTO admin_log VALUES ('1440', '5', '123 Show it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Show it to Me modified', '2016-11-15 05:38:25', '1');
INSERT INTO admin_log VALUES ('1441', '5', '123 Show it to Me', 'site_objects', '2', 'INSERT', '123 Show it to Me configured with Oil Tank object', '2016-11-15 05:38:26', '1');
INSERT INTO admin_log VALUES ('1442', '5', '123 Show it to Me', 'site_objects', '3', 'INSERT', '123 Show it to Me configured with Water Tank object', '2016-11-15 05:38:26', '1');
INSERT INTO admin_log VALUES ('1443', '5', 'Landy\'s Revenge', 'sites', '13', 'UPDATE', 'Site - Landy\'s Revenge modified', '2016-11-15 05:40:24', '1');
INSERT INTO admin_log VALUES ('1444', '5', 'Landy\'s Revenge', 'site_objects', '2', 'INSERT', 'Landy\'s Revenge configured with Oil Tank object', '2016-11-15 05:40:24', '1');
INSERT INTO admin_log VALUES ('1445', '5', 'Landy\'s Revenge', 'site_objects', '3', 'INSERT', 'Landy\'s Revenge configured with Water Tank object', '2016-11-15 05:40:24', '1');
INSERT INTO admin_log VALUES ('1446', '5', 'Michael\'s Glory Hole', 'sites', '11', 'UPDATE', 'Site - Michael\'s Glory Hole modified', '2016-11-15 05:41:06', '1');
INSERT INTO admin_log VALUES ('1447', '5', 'Pontential is Everything', 'sites', '12', 'UPDATE', 'Site - Pontential is Everything modified', '2016-11-15 05:41:37', '1');
INSERT INTO admin_log VALUES ('1448', '5', 'Pontential is Everything', 'site_objects', '2', 'INSERT', 'Pontential is Everything configured with Oil Tank object', '2016-11-15 05:41:37', '1');
INSERT INTO admin_log VALUES ('1449', '5', 'Pontential is Everything', 'site_objects', '4', 'INSERT', 'Pontential is Everything configured with Test\'s object', '2016-11-15 05:41:37', '1');
INSERT INTO admin_log VALUES ('1450', '5', 'Pontential is Everything', 'site_objects', '6', 'INSERT', 'Pontential is Everything configured with 1234 object', '2016-11-15 05:41:37', '1');
INSERT INTO admin_log VALUES ('1451', '5', 'jkl', 'sites', '10', 'DELETE', 'Site - jkl deleted', '2016-11-15 05:41:51', '1');
INSERT INTO admin_log VALUES ('1452', '5', 'Victory or Death', 'sites', '14', 'UPDATE', 'Site - Victory or Death modified', '2016-11-15 05:42:10', '1');
INSERT INTO admin_log VALUES ('1453', '5', 'Savy\'s Future', 'sites', '18', 'UPDATE', 'Site - Savy\'s Future modified', '2016-11-15 05:42:44', '1');
INSERT INTO admin_log VALUES ('1454', '5', 'Savy\'s Future', 'site_objects', '2', 'INSERT', 'Savy\'s Future configured with Oil Tank object', '2016-11-15 05:42:44', '1');
INSERT INTO admin_log VALUES ('1455', '5', 'Savy\'s Future', 'site_objects', '4', 'INSERT', 'Savy\'s Future configured with Test\'s object', '2016-11-15 05:42:44', '1');
INSERT INTO admin_log VALUES ('1456', '5', 'Savy\'s Future', 'site_objects', '8', 'INSERT', 'Savy\'s Future configured with sdtwgbw object', '2016-11-15 05:42:44', '1');
INSERT INTO admin_log VALUES ('1457', '5', 'Yankees Good Night', 'sites', '15', 'UPDATE', 'Site - Yankees Good Night modified', '2016-11-15 05:43:22', '1');
INSERT INTO admin_log VALUES ('1458', '5', 'Roy\'s Renaissance', 'sites', '20', 'UPDATE', 'Site - Roy\'s Renaissance modified', '2016-11-15 05:43:50', '1');
INSERT INTO admin_log VALUES ('1459', '5', 'Roy\'s Renaissance', 'site_objects', '2', 'INSERT', 'Roy\'s Renaissance configured with Oil Tank object', '2016-11-15 05:43:50', '1');
INSERT INTO admin_log VALUES ('1460', '5', 'Roy\'s Renaissance', 'site_objects', '3', 'INSERT', 'Roy\'s Renaissance configured with Water Tank object', '2016-11-15 05:43:50', '1');
INSERT INTO admin_log VALUES ('1461', '5', 'Stranger Things have Happened', 'sites', '18', 'UPDATE', 'Site - Stranger Things have Happened modified', '2016-11-15 05:44:24', '1');
INSERT INTO admin_log VALUES ('1462', '5', 'Stranger Things have Happened', 'site_objects', '2', 'INSERT', 'Stranger Things have Happened configured with Oil Tank object', '2016-11-15 05:44:24', '1');
INSERT INTO admin_log VALUES ('1463', '5', 'Stranger Things have Happened', 'site_objects', '4', 'INSERT', 'Stranger Things have Happened configured with Test\'s object', '2016-11-15 05:44:25', '1');
INSERT INTO admin_log VALUES ('1464', '5', 'Stranger Things have Happened', 'site_objects', '8', 'INSERT', 'Stranger Things have Happened configured with sdtwgbw object', '2016-11-15 05:44:25', '1');
INSERT INTO admin_log VALUES ('1465', '1', '&lt;div style=', 'accounts', '3', 'UPDATE', 'Account - &lt;div style= modified', '2016-11-15 07:26:44', '1');
INSERT INTO admin_log VALUES ('1466', '1', 'Fred\'s Drilling', 'accounts', '3', 'UPDATE', 'Account - Fred\'s Drilling modified', '2016-11-15 07:27:31', '1');
INSERT INTO admin_log VALUES ('1467', '1', 'Fred\'s Drilling', 'accounts', '3', 'UPDATE', 'Account - Fred\'s Drilling modified', '2016-11-15 07:27:37', '1');
INSERT INTO admin_log VALUES ('1468', '1', 'Fred123', 'accounts', '8', 'UPDATE', 'Account - Fred123 modified', '2016-11-15 07:27:47', '1');
INSERT INTO admin_log VALUES ('1469', '5', 'Becca\'s Bonaza', 'sites', '16', 'UPDATE', 'Site - Becca\'s Bonaza modified', '2016-11-15 08:45:35', '1');
INSERT INTO admin_log VALUES ('1470', '5', 'Becca\'s Bonaza', 'site_objects', '2', 'INSERT', 'Becca\'s Bonaza configured with Oil Tank object', '2016-11-15 08:45:36', '1');
INSERT INTO admin_log VALUES ('1471', '5', 'Becca\'s Bonaza', 'site_objects', '3', 'INSERT', 'Becca\'s Bonaza configured with Water Tank object', '2016-11-15 08:45:36', '1');
INSERT INTO admin_log VALUES ('1472', '5', 'Becca\'s Bonaza', 'site_objects', '5', 'INSERT', 'Becca\'s Bonaza configured with Becca\'s Object object', '2016-11-15 08:45:36', '1');
INSERT INTO admin_log VALUES ('1473', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-15 17:07:39', '0');
INSERT INTO admin_log VALUES ('1474', '5', '123 Show it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Show it to Me modified', '2016-11-15 19:03:05', '1');
INSERT INTO admin_log VALUES ('1475', '5', '123 Show it to Me', 'site_objects', '2', 'INSERT', '123 Show it to Me configured with Oil Tank object', '2016-11-15 19:03:06', '1');
INSERT INTO admin_log VALUES ('1476', '5', '123 Show it to Me', 'site_objects', '3', 'INSERT', '123 Show it to Me configured with Water Tank object', '2016-11-15 19:03:06', '1');
INSERT INTO admin_log VALUES ('1477', '5', '123 Show it to Me', 'site_objects', '5', 'INSERT', '123 Show it to Me configured with Becca\'s Object object', '2016-11-15 19:03:06', '1');
INSERT INTO admin_log VALUES ('1478', '5', 'Pump Jack', 'objects', '5', 'UPDATE', 'Object - Pump Jack modified', '2016-11-15 19:03:43', '1');
INSERT INTO admin_log VALUES ('1479', '5', 'Pump Jack', 'object_devices', '1', 'INSERT', 'Pump Jack configured with Tank Stick device', '2016-11-15 19:03:43', '1');
INSERT INTO admin_log VALUES ('1480', '5', 'Pump Jack', 'object_devices', '2', 'INSERT', 'Pump Jack configured with Transducer device', '2016-11-15 19:03:43', '1');
INSERT INTO admin_log VALUES ('1481', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-15 19:54:46', '0');
INSERT INTO admin_log VALUES ('1482', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-15 20:03:48', '1');
INSERT INTO admin_log VALUES ('1483', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-15 20:03:48', '1');
INSERT INTO admin_log VALUES ('1484', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-15 20:03:48', '1');
INSERT INTO admin_log VALUES ('1485', '5', 'Water Tank', 'objects', '3', 'UPDATE', 'Object - Water Tank modified', '2016-11-15 20:04:32', '1');
INSERT INTO admin_log VALUES ('1486', '5', 'Water Tank', 'object_devices', '1', 'INSERT', 'Water Tank configured with Tank Stick device', '2016-11-15 20:04:32', '1');
INSERT INTO admin_log VALUES ('1487', '5', 'Water Tank', 'object_devices', '2', 'INSERT', 'Water Tank configured with Transducer device', '2016-11-15 20:04:32', '1');
INSERT INTO admin_log VALUES ('1488', '5', 'Pump Jack', 'objects', '5', 'UPDATE', 'Object - Pump Jack modified', '2016-11-15 20:04:44', '1');
INSERT INTO admin_log VALUES ('1489', '5', 'Pump Jack', 'object_devices', '1', 'INSERT', 'Pump Jack configured with Tank Stick device', '2016-11-15 20:04:44', '1');
INSERT INTO admin_log VALUES ('1490', '5', 'Pump Jack', 'object_devices', '2', 'INSERT', 'Pump Jack configured with Transducer device', '2016-11-15 20:04:44', '1');
INSERT INTO admin_log VALUES ('1491', '5', 'CMC\'s Slush', 'sites', '6', 'UPDATE', 'Site - CMC\'s Slush modified', '2016-11-15 20:37:13', '1');
INSERT INTO admin_log VALUES ('1492', '5', 'CMC\'s Slush', 'site_objects', '2', 'INSERT', 'CMC\'s Slush configured with Oil Tank object', '2016-11-15 20:37:13', '1');
INSERT INTO admin_log VALUES ('1493', '5', 'CMC\'s Slush', 'site_objects', '3', 'INSERT', 'CMC\'s Slush configured with Water Tank object', '2016-11-15 20:37:13', '1');
INSERT INTO admin_log VALUES ('1494', '5', 'Jackson\'s Hole', 'sites', '17', 'UPDATE', 'Site - Jackson\'s Hole modified', '2016-11-15 20:37:33', '1');
INSERT INTO admin_log VALUES ('1495', '5', 'Jackson\'s Hole', 'site_objects', '2', 'INSERT', 'Jackson\'s Hole configured with Oil Tank object', '2016-11-15 20:37:33', '1');
INSERT INTO admin_log VALUES ('1496', '5', 'Jackson\'s Hole', 'site_objects', '4', 'INSERT', 'Jackson\'s Hole configured with Test\'s object', '2016-11-15 20:37:33', '1');
INSERT INTO admin_log VALUES ('1497', '5', 'Jackson\'s Hole', 'site_objects', '5', 'INSERT', 'Jackson\'s Hole configured with Pump Jack object', '2016-11-15 20:37:34', '1');
INSERT INTO admin_log VALUES ('1498', '5', '456', 'sites', '38', 'INSERT', 'New site - 456 added', '2016-11-15 21:03:24', '1');
INSERT INTO admin_log VALUES ('1499', '5', '456', 'site_objects', '2', 'INSERT', '456 configured with Oil Tank object', '2016-11-15 21:03:24', '1');
INSERT INTO admin_log VALUES ('1500', '5', '456', 'site_objects', '3', 'INSERT', '456 configured with Water Tank object', '2016-11-15 21:03:24', '1');
INSERT INTO admin_log VALUES ('1501', '5', '456', 'site_objects', '5', 'INSERT', '456 configured with Pump Jack object', '2016-11-15 21:03:24', '1');
INSERT INTO admin_log VALUES ('1502', '5', '456', 'sites', '38', 'DELETE', 'Site - 456 deleted', '2016-11-15 21:10:32', '1');
INSERT INTO admin_log VALUES ('1503', '5', '!Test 456', 'sites', '32', 'UPDATE', 'Site - !Test 456  reactivated', '2016-11-15 21:12:53', '1');
INSERT INTO admin_log VALUES ('1504', '5', '123 Show it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Show it to Me  deactivated', '2016-11-15 21:17:00', '1');
INSERT INTO admin_log VALUES ('1505', '5', '123 Show it to Me', 'sites', '34', 'UPDATE', 'Site - 123 Show it to Me  reactivated', '2016-11-15 21:17:07', '1');
INSERT INTO admin_log VALUES ('1506', '5', '!Test 456', 'sites', '32', 'UPDATE', 'Site - !Test 456  deactivated', '2016-11-15 21:17:13', '1');
INSERT INTO admin_log VALUES ('1507', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 03:16:10', '0');
INSERT INTO admin_log VALUES ('1508', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-16 04:10:36', '1');
INSERT INTO admin_log VALUES ('1509', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-16 04:10:36', '1');
INSERT INTO admin_log VALUES ('1510', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-16 04:10:36', '1');
INSERT INTO admin_log VALUES ('1511', '5', 'ACME XFR - 24 Oil Tank', 'objects', '2', 'UPDATE', 'Object - ACME XFR - 24 Oil Tank modified', '2016-11-16 04:17:12', '1');
INSERT INTO admin_log VALUES ('1512', '5', 'ACME XFR - 24 Oil Tank', 'object_devices', '1', 'INSERT', 'ACME XFR - 24 Oil Tank configured with Tank Stick device', '2016-11-16 04:17:13', '1');
INSERT INTO admin_log VALUES ('1513', '5', 'ACME XFR - 24 Oil Tank', 'object_devices', '3', 'INSERT', 'ACME XFR - 24 Oil Tank configured with Transfer Pump device', '2016-11-16 04:17:13', '1');
INSERT INTO admin_log VALUES ('1514', '5', 'Sampson RR - 26 Water Tank', 'objects', '3', 'UPDATE', 'Object - Sampson RR - 26 Water Tank modified', '2016-11-16 04:22:03', '1');
INSERT INTO admin_log VALUES ('1515', '5', 'Sampson RR - 26 Water Tank', 'object_devices', '1', 'INSERT', 'Sampson RR - 26 Water Tank configured with Tank Stick device', '2016-11-16 04:22:03', '1');
INSERT INTO admin_log VALUES ('1516', '5', 'Sampson RR - 26 Water Tank', 'object_devices', '2', 'INSERT', 'Sampson RR - 26 Water Tank configured with Transducer device', '2016-11-16 04:22:03', '1');
INSERT INTO admin_log VALUES ('1517', '5', 'Drake 25 Series Pump Jack', 'objects', '5', 'UPDATE', 'Object - Drake 25 Series Pump Jack modified', '2016-11-16 04:24:36', '1');
INSERT INTO admin_log VALUES ('1518', '5', 'Drake 25 Series Pump Jack', 'object_devices', '1', 'INSERT', 'Drake 25 Series Pump Jack configured with Tank Stick device', '2016-11-16 04:24:36', '1');
INSERT INTO admin_log VALUES ('1519', '5', 'Drake 25 Series Pump Jack', 'object_devices', '2', 'INSERT', 'Drake 25 Series Pump Jack configured with Transducer device', '2016-11-16 04:24:36', '1');
INSERT INTO admin_log VALUES ('1520', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 08:31:27', '0');
INSERT INTO admin_log VALUES ('1521', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 08:39:39', '0');
INSERT INTO admin_log VALUES ('1522', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 17:16:23', '0');
INSERT INTO admin_log VALUES ('1523', '1', 'Jessica', 'sites', '24', 'UPDATE', 'Site - Jessica modified', '2016-11-16 17:17:09', '1');
INSERT INTO admin_log VALUES ('1524', '1', 'Jessica', 'site_objects', '2', 'INSERT', 'Jessica configured with ACME XFR - 24 Oil Tank object', '2016-11-16 17:17:09', '1');
INSERT INTO admin_log VALUES ('1525', '1', 'Jessica', 'site_objects', '3', 'INSERT', 'Jessica configured with Sampson RR - 26 Water Tank object', '2016-11-16 17:17:09', '1');
INSERT INTO admin_log VALUES ('1526', '1', 'Jessica', 'site_objects', '5', 'INSERT', 'Jessica configured with Drake 25 Series Pump Jack object', '2016-11-16 17:17:09', '1');
INSERT INTO admin_log VALUES ('1527', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 18:47:32', '0');
INSERT INTO admin_log VALUES ('1528', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 21:38:22', '0');
INSERT INTO admin_log VALUES ('1529', '1', 'Devices', 'modules', '42', 'INSERT', 'New module - Devices created', '2016-11-16 21:39:24', '1');
INSERT INTO admin_log VALUES ('1530', '1', 'Executions', 'modules', '43', 'INSERT', 'New module - Executions created', '2016-11-16 21:39:54', '1');
INSERT INTO admin_log VALUES ('1531', '1', 'Measurements', 'modules', '44', 'INSERT', 'New module - Measurements created', '2016-11-16 21:40:25', '1');
INSERT INTO admin_log VALUES ('1532', '1', 'Triggers', 'modules', '45', 'INSERT', 'New module - Triggers created', '2016-11-16 21:40:49', '1');
INSERT INTO admin_log VALUES ('1533', '1', 'Technician', 'groups', '6', 'UPDATE', 'Group - Technician  deactivated', '2016-11-16 22:08:16', '1');
INSERT INTO admin_log VALUES ('1534', '1', 'Technician', 'groups', '6', 'UPDATE', 'Group - Technician  reactivated', '2016-11-16 22:08:23', '1');
INSERT INTO admin_log VALUES ('1535', '1', 'Device Menu', 'modules', '1', 'UPDATE', 'Module - Device Menu modified', '2016-11-16 22:20:49', '1');
INSERT INTO admin_log VALUES ('1536', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1537', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1538', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1539', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1540', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1541', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1542', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1543', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1544', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1545', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1546', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1547', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1548', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1549', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1550', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1551', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1552', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1553', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1554', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-16 23:10:40', '1');
INSERT INTO admin_log VALUES ('1555', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 23:11:28', '0');
INSERT INTO admin_log VALUES ('1556', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1557', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1558', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1559', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1560', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1561', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1562', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1563', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1564', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1565', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1566', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1567', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1568', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1569', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1570', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1571', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1572', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1573', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1574', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1575', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1576', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-16 23:43:12', '1');
INSERT INTO admin_log VALUES ('1577', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 23:43:25', '0');
INSERT INTO admin_log VALUES ('1578', '1', 'Tags', 'modules', '41', 'DELETE', 'Module - Tags deleted', '2016-11-16 23:45:21', '1');
INSERT INTO admin_log VALUES ('1579', '1', 'Measurements', 'modules', '40', 'DELETE', 'Module - Measurements deleted', '2016-11-16 23:45:40', '1');
INSERT INTO admin_log VALUES ('1580', '1', 'Tags', 'modules', '46', 'INSERT', 'New module - Tags created', '2016-11-16 23:46:15', '1');
INSERT INTO admin_log VALUES ('1581', '1', 'Triggers', 'modules', '45', 'UPDATE', 'Module - Triggers modified', '2016-11-16 23:46:32', '1');
INSERT INTO admin_log VALUES ('1582', '1', 'Devices', 'modules', '42', 'UPDATE', 'Module - Devices modified', '2016-11-16 23:48:07', '1');
INSERT INTO admin_log VALUES ('1583', '1', 'Executions', 'modules', '43', 'UPDATE', 'Module - Executions modified', '2016-11-16 23:48:23', '1');
INSERT INTO admin_log VALUES ('1584', '1', 'Measurements', 'modules', '44', 'UPDATE', 'Module - Measurements modified', '2016-11-16 23:48:37', '1');
INSERT INTO admin_log VALUES ('1585', '1', 'Triggers', 'modules', '45', 'UPDATE', 'Module - Triggers modified', '2016-11-16 23:49:05', '1');
INSERT INTO admin_log VALUES ('1586', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1587', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1588', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1589', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1590', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1591', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1592', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1593', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1594', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1595', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1596', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1597', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1598', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1599', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1600', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1601', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1602', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1603', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1604', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1605', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-16 23:49:52', '1');
INSERT INTO admin_log VALUES ('1606', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-16 23:50:02', '0');
INSERT INTO admin_log VALUES ('1607', '1', 'Stop Motor', 'executions', '1', 'INSERT', 'New execution - Stop Motor added', '2016-11-17 00:10:36', '1');
INSERT INTO admin_log VALUES ('1608', '1', 'Stop Motor1', 'executions', '1', 'UPDATE', 'Execution - Stop Motor1 modified', '2016-11-17 00:15:11', '1');
INSERT INTO admin_log VALUES ('1609', '1', 'Stop Motor', 'executions', '1', 'UPDATE', 'Execution - Stop Motor modified', '2016-11-17 00:15:22', '1');
INSERT INTO admin_log VALUES ('1610', '1', 'Stop Motor', 'executions', '1', 'UPDATE', 'Execution - Stop Motor  deactivated', '2016-11-17 00:15:29', '1');
INSERT INTO admin_log VALUES ('1611', '1', 'Stop Motor', 'executions', '1', 'UPDATE', 'Execution - Stop Motor  reactivated', '2016-11-17 00:15:38', '1');
INSERT INTO admin_log VALUES ('1612', '1', 'Reboot OPCUA Server', 'executions', '2', 'INSERT', 'New execution - Reboot OPCUA Server added', '2016-11-17 00:16:25', '1');
INSERT INTO admin_log VALUES ('1613', '1', 'Start Motor', 'executions', '3', 'INSERT', 'New execution - Start Motor added', '2016-11-17 00:19:33', '1');
INSERT INTO admin_log VALUES ('1614', '1', 'Actions', 'modules', '47', 'INSERT', 'New module - Actions created', '2016-11-17 00:33:13', '1');
INSERT INTO admin_log VALUES ('1615', '1', 'Executions', 'modules', '43', 'UPDATE', 'Module - Executions modified', '2016-11-17 00:33:28', '1');
INSERT INTO admin_log VALUES ('1616', '1', 'Measurements', 'modules', '44', 'UPDATE', 'Module - Measurements modified', '2016-11-17 00:33:46', '1');
INSERT INTO admin_log VALUES ('1617', '1', 'Tags', 'modules', '46', 'UPDATE', 'Module - Tags modified', '2016-11-17 00:34:11', '1');
INSERT INTO admin_log VALUES ('1618', '1', 'Triggers', 'modules', '45', 'UPDATE', 'Module - Triggers modified', '2016-11-17 00:34:30', '1');
INSERT INTO admin_log VALUES ('1619', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1620', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1621', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1622', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1623', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1624', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1625', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1626', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1627', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1628', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1629', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1630', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Actions', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1631', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1632', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1633', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1634', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1635', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1636', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1637', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1638', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1639', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-17 00:34:54', '1');
INSERT INTO admin_log VALUES ('1640', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 00:35:54', '0');
INSERT INTO admin_log VALUES ('1641', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 01:14:57', '0');
INSERT INTO admin_log VALUES ('1642', '1', 'Test', 'executions', '4', 'INSERT', 'New execution - Test added', '2016-11-17 01:15:36', '1');
INSERT INTO admin_log VALUES ('1643', '1', 'Test', 'executions', '5', 'INSERT', 'New execution - Test added', '2016-11-17 01:16:44', '1');
INSERT INTO admin_log VALUES ('1644', '1', 'Test', 'executions', '6', 'INSERT', 'New execution - Test added', '2016-11-17 01:18:40', '1');
INSERT INTO admin_log VALUES ('1645', '1', 'Test', 'execution_tags', '3', 'INSERT', 'Test configured with Start Motor tag', '2016-11-17 01:18:40', '1');
INSERT INTO admin_log VALUES ('1646', '1', 'Test', 'execution_tags', '9', 'INSERT', 'Test configured with  tag', '2016-11-17 01:18:40', '1');
INSERT INTO admin_log VALUES ('1647', '1', 'Test', 'executions', '6', 'UPDATE', 'Execution - Test modified', '2016-11-17 01:21:34', '1');
INSERT INTO admin_log VALUES ('1648', '1', 'Test', 'execution_tags', '1', 'INSERT', 'Test configured with Stop Motor tag', '2016-11-17 01:21:34', '1');
INSERT INTO admin_log VALUES ('1649', '1', 'Test', 'execution_tags', '5', 'INSERT', 'Test configured with Test tag', '2016-11-17 01:21:34', '1');
INSERT INTO admin_log VALUES ('1650', '1', 'Test', 'executions', '6', 'UPDATE', 'Execution - Test modified', '2016-11-17 01:22:31', '1');
INSERT INTO admin_log VALUES ('1651', '1', 'Test', 'execution_tags', '1', 'INSERT', 'Test configured with Stop Motor tag', '2016-11-17 01:22:31', '1');
INSERT INTO admin_log VALUES ('1652', '1', 'Test', 'execution_tags', '5', 'INSERT', 'Test configured with Test tag', '2016-11-17 01:22:31', '1');
INSERT INTO admin_log VALUES ('1653', '1', 'Test', 'executions', '5', 'DELETE', 'Execution - Test deleted', '2016-11-17 01:22:46', '1');
INSERT INTO admin_log VALUES ('1654', '1', 'Test', 'executions', '4', 'DELETE', 'Execution - Test deleted', '2016-11-17 01:23:05', '1');
INSERT INTO admin_log VALUES ('1655', '1', 'Test', 'executions', '6', 'UPDATE', 'Execution - Test  deactivated', '2016-11-17 01:23:10', '1');
INSERT INTO admin_log VALUES ('1656', '1', 'Test', 'executions', '6', 'UPDATE', 'Execution - Test  reactivated', '2016-11-17 01:23:14', '1');
INSERT INTO admin_log VALUES ('1657', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 03:03:34', '0');
INSERT INTO admin_log VALUES ('1658', '1', 'Shutdown Pump', 'actions', '1', 'INSERT', 'New action - Shutdown Pump added', '2016-11-17 03:11:56', '1');
INSERT INTO admin_log VALUES ('1659', '1', 'Shutdown Pump1', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump1 modified', '2016-11-17 03:13:53', '1');
INSERT INTO admin_log VALUES ('1660', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-17 03:14:11', '1');
INSERT INTO admin_log VALUES ('1661', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump  deactivated', '2016-11-17 03:20:29', '1');
INSERT INTO admin_log VALUES ('1662', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump  reactivated', '2016-11-17 03:20:33', '1');
INSERT INTO admin_log VALUES ('1663', '1', 'AST', 'tags', '1', 'UPDATE', 'Tag - AST modified', '2016-11-17 03:55:23', '1');
INSERT INTO admin_log VALUES ('1664', '1', 'DST', 'tags', '8', 'UPDATE', 'Tag - DST modified', '2016-11-17 04:27:02', '1');
INSERT INTO admin_log VALUES ('1665', '1', 'EST', 'tags', '9', 'UPDATE', 'Tag - EST modified', '2016-11-17 04:27:43', '1');
INSERT INTO admin_log VALUES ('1666', '1', 'EST', 'tags', '9', 'UPDATE', 'Tag - EST modified', '2016-11-17 04:27:57', '1');
INSERT INTO admin_log VALUES ('1667', '1', 'AST', 'tags', '1', 'UPDATE', 'Tag - AST modified', '2016-11-17 04:28:38', '1');
INSERT INTO admin_log VALUES ('1668', '1', 'FST', 'tags', '10', 'INSERT', 'New tag - FST added', '2016-11-17 04:29:09', '1');
INSERT INTO admin_log VALUES ('1669', '1', 'AST', 'tags', '1', 'UPDATE', 'Tag - AST modified', '2016-11-17 04:32:20', '1');
INSERT INTO admin_log VALUES ('1670', '1', 'BST', 'tags', '3', 'UPDATE', 'Tag - BST modified', '2016-11-17 04:32:48', '1');
INSERT INTO admin_log VALUES ('1671', '1', 'CST', 'tags', '5', 'UPDATE', 'Tag - CST modified', '2016-11-17 04:33:16', '1');
INSERT INTO admin_log VALUES ('1672', '1', 'DST', 'tags', '8', 'UPDATE', 'Tag - DST modified', '2016-11-17 04:33:28', '1');
INSERT INTO admin_log VALUES ('1673', '1', 'EST', 'tags', '9', 'UPDATE', 'Tag - EST modified', '2016-11-17 04:33:39', '1');
INSERT INTO admin_log VALUES ('1674', '1', 'FST', 'tags', '10', 'UPDATE', 'Tag - FST modified', '2016-11-17 04:34:05', '1');
INSERT INTO admin_log VALUES ('1675', '1', 'GST', 'tags', '11', 'INSERT', 'New tag - GST added', '2016-11-17 04:36:48', '1');
INSERT INTO admin_log VALUES ('1676', '1', 'HST', 'tags', '12', 'INSERT', 'New tag - HST added', '2016-11-17 04:37:04', '1');
INSERT INTO admin_log VALUES ('1677', '1', 'IST', 'tags', '13', 'INSERT', 'New tag - IST added', '2016-11-17 04:37:22', '1');
INSERT INTO admin_log VALUES ('1678', '1', 'Startup Pump', 'actions', '2', 'INSERT', 'New action - Startup Pump added', '2016-11-17 05:39:32', '1');
INSERT INTO admin_log VALUES ('1679', '1', 'Shut off Valve', 'actions', '3', 'INSERT', 'New action - Shut off Valve added', '2016-11-17 05:40:14', '1');
INSERT INTO admin_log VALUES ('1680', '1', 'Open Valve', 'actions', '4', 'INSERT', 'New action - Open Valve added', '2016-11-17 05:40:42', '1');
INSERT INTO admin_log VALUES ('1681', '1', 'Reboot OPC Server', 'actions', '5', 'INSERT', 'New action - Reboot OPC Server added', '2016-11-17 05:41:11', '1');
INSERT INTO admin_log VALUES ('1682', '1', 'Reboot OPCUA Server', 'actions', '5', 'UPDATE', 'Action - Reboot OPCUA Server modified', '2016-11-17 05:41:33', '1');
INSERT INTO admin_log VALUES ('1683', '1', 'Tank Overflow', 'triggers', '1', 'INSERT', 'New trigger - Tank Overflow added', '2016-11-17 05:43:28', '1');
INSERT INTO admin_log VALUES ('1684', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 05:51:03', '1');
INSERT INTO admin_log VALUES ('1685', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 05:52:08', '1');
INSERT INTO admin_log VALUES ('1686', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 06:00:03', '1');
INSERT INTO admin_log VALUES ('1687', '1', 'Tank Overflow', 'trigger_actions', '1', 'INSERT', 'Tank Overflow configured with Tank Overflow object', '2016-11-17 06:00:04', '1');
INSERT INTO admin_log VALUES ('1688', '1', 'Tank Overflow', 'trigger_actions', '3', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:00:04', '1');
INSERT INTO admin_log VALUES ('1689', '1', 'Tank Overflow', 'trigger_tags', '5', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:00:04', '1');
INSERT INTO admin_log VALUES ('1690', '1', 'Tank Overflow', 'trigger_tags', '13', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:00:04', '1');
INSERT INTO admin_log VALUES ('1691', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 06:01:22', '1');
INSERT INTO admin_log VALUES ('1692', '1', 'Tank Overflow', 'trigger_actions', '1', 'INSERT', 'Tank Overflow configured with Tank Overflow object', '2016-11-17 06:01:22', '1');
INSERT INTO admin_log VALUES ('1693', '1', 'Tank Overflow', 'trigger_actions', '3', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:01:22', '1');
INSERT INTO admin_log VALUES ('1694', '1', 'Tank Overflow', 'trigger_tags', '5', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:01:22', '1');
INSERT INTO admin_log VALUES ('1695', '1', 'Tank Overflow', 'trigger_tags', '13', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:01:22', '1');
INSERT INTO admin_log VALUES ('1696', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 06:02:49', '1');
INSERT INTO admin_log VALUES ('1697', '1', 'Tank Overflow', 'trigger_actions', '1', 'INSERT', 'Tank Overflow configured with Tank Overflow object', '2016-11-17 06:02:49', '1');
INSERT INTO admin_log VALUES ('1698', '1', 'Tank Overflow', 'trigger_actions', '4', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:02:49', '1');
INSERT INTO admin_log VALUES ('1699', '1', 'Tank Overflow', 'trigger_actions', '5', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:02:50', '1');
INSERT INTO admin_log VALUES ('1700', '1', 'Tank Overflow', 'trigger_tags', '10', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:02:50', '1');
INSERT INTO admin_log VALUES ('1701', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 06:03:04', '1');
INSERT INTO admin_log VALUES ('1702', '1', 'Tank Overflow', 'trigger_actions', '1', 'INSERT', 'Tank Overflow configured with Tank Overflow object', '2016-11-17 06:03:04', '1');
INSERT INTO admin_log VALUES ('1703', '1', 'Tank Overflow', 'trigger_actions', '4', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:03:04', '1');
INSERT INTO admin_log VALUES ('1704', '1', 'Tank Overflow', 'trigger_actions', '5', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:03:04', '1');
INSERT INTO admin_log VALUES ('1705', '1', 'Tank Overflow', 'trigger_tags', '10', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:03:04', '1');
INSERT INTO admin_log VALUES ('1706', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow modified', '2016-11-17 06:03:18', '1');
INSERT INTO admin_log VALUES ('1707', '1', 'Tank Overflow', 'trigger_actions', '1', 'INSERT', 'Tank Overflow configured with Tank Overflow object', '2016-11-17 06:03:18', '1');
INSERT INTO admin_log VALUES ('1708', '1', 'Tank Overflow', 'trigger_actions', '4', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:03:18', '1');
INSERT INTO admin_log VALUES ('1709', '1', 'Tank Overflow', 'trigger_actions', '5', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:03:18', '1');
INSERT INTO admin_log VALUES ('1710', '1', 'Tank Overflow', 'trigger_tags', '10', 'INSERT', 'Tank Overflow configured with  object', '2016-11-17 06:03:18', '1');
INSERT INTO admin_log VALUES ('1711', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow  deactivated', '2016-11-17 06:03:27', '1');
INSERT INTO admin_log VALUES ('1712', '1', 'Tank Overflow', 'triggers', '1', 'UPDATE', 'Trigger - Tank Overflow  reactivated', '2016-11-17 06:03:32', '1');
INSERT INTO admin_log VALUES ('1713', '1', 'OPCUA Admins', 'groups', '0', 'INSERT', 'New group - OPCUA Admins added', '2016-11-17 06:07:27', '1');
INSERT INTO admin_log VALUES ('1714', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Admin', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1715', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Sites', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1716', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to System', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1717', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Admin Log', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1718', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Objects', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1719', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Device Menu', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1720', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Devices', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1721', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Actions', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1722', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Executions', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1723', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Measurements', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1724', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Tags', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1725', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Triggers', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1726', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Dashboard', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1727', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Help', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1728', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Submit Ticket', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1729', '1', 'OPCUA Admins', 'group_permissions', '0', 'INSERT', 'OPCUA Admins granted access to Tickets', '2016-11-17 06:11:36', '1');
INSERT INTO admin_log VALUES ('1730', '1', 'OPC UA', 'users', '46', 'INSERT', 'New user - opcua added', '2016-11-17 06:14:33', '1');
INSERT INTO admin_log VALUES ('1731', '1', 'OPC UA', 'user_departments', '46', 'INSERT', 'Department - IT assigned', '2016-11-17 06:14:33', '1');
INSERT INTO admin_log VALUES ('1732', '1', 'OPC UA', 'user_groups', '46', 'INSERT', 'Group - Manager assigned', '2016-11-17 06:14:33', '1');
INSERT INTO admin_log VALUES ('1733', '1', 'OPC UA', 'user_groups', '46', 'INSERT', 'Group - OPCUA Admins assigned', '2016-11-17 06:15:04', '1');
INSERT INTO admin_log VALUES ('1734', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 17:06:38', '0');
INSERT INTO admin_log VALUES ('1735', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-17 17:08:38', '1');
INSERT INTO admin_log VALUES ('1736', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-17 17:08:50', '1');
INSERT INTO admin_log VALUES ('1737', '1', 'Shutdown Pump1', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump1 modified', '2016-11-17 17:09:02', '1');
INSERT INTO admin_log VALUES ('1738', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-17 17:09:15', '1');
INSERT INTO admin_log VALUES ('1739', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump  reactivated', '2016-11-17 17:09:21', '1');
INSERT INTO admin_log VALUES ('1740', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 17:46:11', '0');
INSERT INTO admin_log VALUES ('1741', '1', 'Measurements', 'modules', '44', 'UPDATE', 'Module - Measurements modified', '2016-11-17 17:47:22', '1');
INSERT INTO admin_log VALUES ('1742', '1', 'Executions', 'modules', '43', 'UPDATE', 'Module - Executions modified', '2016-11-17 17:47:43', '1');
INSERT INTO admin_log VALUES ('1743', '1', 'Actions', 'modules', '47', 'UPDATE', 'Module - Actions modified', '2016-11-17 17:48:07', '1');
INSERT INTO admin_log VALUES ('1744', '1', 'Triggers', 'modules', '45', 'UPDATE', 'Module - Triggers modified', '2016-11-17 17:48:29', '1');
INSERT INTO admin_log VALUES ('1745', '1', 'Tags', 'modules', '46', 'UPDATE', 'Module - Tags modified', '2016-11-17 17:48:55', '1');
INSERT INTO admin_log VALUES ('1746', '1', 'Overheated Motor', 'triggers', '2', 'INSERT', 'New trigger - Overheated Motor added', '2016-11-17 19:46:29', '1');
INSERT INTO admin_log VALUES ('1747', '1', 'Overheated Motor', 'trigger_actions', '1', 'INSERT', 'Overheated Motor configured with Tank Overflow object', '2016-11-17 19:46:29', '1');
INSERT INTO admin_log VALUES ('1748', '1', 'Overheated Motor', 'trigger_actions', '3', 'INSERT', 'Overheated Motor configured with  object', '2016-11-17 19:46:29', '1');
INSERT INTO admin_log VALUES ('1749', '1', 'Overheated Motor', 'triggers', '3', 'INSERT', 'New trigger - Overheated Motor added', '2016-11-17 19:56:19', '1');
INSERT INTO admin_log VALUES ('1750', '1', 'Overheated Motor', 'trigger_actions', '1', 'INSERT', 'Overheated Motor configured with Shutdown Pump action', '2016-11-17 19:56:19', '1');
INSERT INTO admin_log VALUES ('1751', '1', 'Overheated Motor', 'trigger_actions', '3', 'INSERT', 'Overheated Motor configured with Shut off Valve action', '2016-11-17 19:56:19', '1');
INSERT INTO admin_log VALUES ('1752', '1', 'Overheated Motor', 'triggers', '4', 'INSERT', 'New trigger - Overheated Motor added', '2016-11-17 19:57:26', '1');
INSERT INTO admin_log VALUES ('1753', '1', 'Overheated Motor', 'trigger_actions', '1', 'INSERT', 'Overheated Motor configured with Shutdown Pump action', '2016-11-17 19:57:26', '1');
INSERT INTO admin_log VALUES ('1754', '1', 'Overheated Motor', 'trigger_actions', '3', 'INSERT', 'Overheated Motor configured with Shut off Valve action', '2016-11-17 19:57:26', '1');
INSERT INTO admin_log VALUES ('1755', '1', 'Overheated Motor', 'triggers', '2', 'DELETE', 'Trigger - Overheated Motor deleted', '2016-11-17 20:00:20', '1');
INSERT INTO admin_log VALUES ('1756', '1', 'Overheated Motor', 'triggers', '3', 'DELETE', 'Trigger - Overheated Motor deleted', '2016-11-17 20:00:26', '1');
INSERT INTO admin_log VALUES ('1757', '1', 'Stuck Valve', 'triggers', '5', 'INSERT', 'New trigger - Stuck Valve added', '2016-11-17 20:09:08', '1');
INSERT INTO admin_log VALUES ('1758', '1', 'Stuck Valve', 'triggers', '6', 'INSERT', 'New trigger - Stuck Valve added', '2016-11-17 20:11:08', '1');
INSERT INTO admin_log VALUES ('1759', '1', 'Stuck Valve', 'trigger_actions', '3', 'INSERT', 'Stuck Valve configured with Shut off Valve action', '2016-11-17 20:11:08', '1');
INSERT INTO admin_log VALUES ('1760', '1', 'Stuck Valve', 'trigger_actions', '2', 'INSERT', 'Stuck Valve configured with Startup Pump action', '2016-11-17 20:11:08', '1');
INSERT INTO admin_log VALUES ('1761', '1', 'Stuck Valve', 'trigger_tags', '10', 'INSERT', 'Stuck Valve configured with FST trigger', '2016-11-17 20:11:08', '1');
INSERT INTO admin_log VALUES ('1762', '1', 'Stuck Valve', 'triggers', '5', 'UPDATE', 'Trigger - Stuck Valve modified', '2016-11-17 20:11:40', '1');
INSERT INTO admin_log VALUES ('1763', '1', 'Stuck Valve', 'trigger_actions', '1', 'INSERT', 'Stuck Valve configured with Shutdown Pump action', '2016-11-17 20:11:40', '1');
INSERT INTO admin_log VALUES ('1764', '1', 'Stuck Valve', 'trigger_actions', '3', 'INSERT', 'Stuck Valve configured with Shut off Valve action', '2016-11-17 20:11:40', '1');
INSERT INTO admin_log VALUES ('1765', '1', 'Stuck Valve', 'trigger_tags', '5', 'INSERT', 'Stuck Valve configured with CST trigger', '2016-11-17 20:11:40', '1');
INSERT INTO admin_log VALUES ('1766', '1', 'Stuck Valve', 'trigger_tags', '13', 'INSERT', 'Stuck Valve configured with IST trigger', '2016-11-17 20:11:41', '1');
INSERT INTO admin_log VALUES ('1767', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 20:46:09', '0');
INSERT INTO admin_log VALUES ('1768', '1', 'Stuck Valve', 'triggers', '5', 'DELETE', 'Trigger - Stuck Valve deleted', '2016-11-17 20:46:43', '1');
INSERT INTO admin_log VALUES ('1769', '1', 'Electric Motor', 'devices', '15', 'INSERT', 'New device - Electric Motor added', '2016-11-17 20:49:56', '1');
INSERT INTO admin_log VALUES ('1770', '1', 'Electric Motor', 'devices', '16', 'INSERT', 'New device - Electric Motor added', '2016-11-17 20:51:21', '1');
INSERT INTO admin_log VALUES ('1771', '1', 'Electric Motor', 'devices', '15', 'DELETE', 'Device - Electric Motor deleted', '2016-11-17 20:54:37', '1');
INSERT INTO admin_log VALUES ('1772', '1', 'Electric Motor', 'devices', '16', 'DELETE', 'Device - Electric Motor deleted', '2016-11-17 20:54:43', '1');
INSERT INTO admin_log VALUES ('1773', '1', 'Electric Motor', 'devices', '17', 'INSERT', 'New device - Electric Motor added', '2016-11-17 21:11:32', '1');
INSERT INTO admin_log VALUES ('1774', '1', 'Electric Motor', 'devices', '18', 'INSERT', 'New device - Electric Motor added', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1775', '1', 'Electric Motor', 'device_executions', '1', 'INSERT', 'Electric Motor configured with Stop Motor execution', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1776', '1', 'Electric Motor', 'device_executions', '3', 'INSERT', 'Electric Motor configured with Start Motor execution', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1777', '1', 'Electric Motor', 'device_measurements', '8', 'INSERT', 'Electric Motor configured with Level measurement', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1778', '1', 'Electric Motor', 'device_measurements', '9', 'INSERT', 'Electric Motor configured with Temperature measurement', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1779', '1', 'Electric Motor', 'device_measurements', '11', 'INSERT', 'Electric Motor configured with Pressure measurement', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1780', '1', 'Electric Motor', 'device_triggers', '4', 'INSERT', 'Electric Motor configured with Overheated Motor trigger', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1781', '1', 'Electric Motor', 'device_triggers', '6', 'INSERT', 'Electric Motor configured with Stuck Valve trigger', '2016-11-17 21:12:20', '1');
INSERT INTO admin_log VALUES ('1782', '1', 'Electric Motor', 'devices', '18', 'UPDATE', 'Device - Electric Motor modified', '2016-11-17 21:18:53', '1');
INSERT INTO admin_log VALUES ('1783', '1', 'Electric Motor', 'device_executions', '2', 'INSERT', 'Electric Motor configured with Reboot OPCUA Server execution', '2016-11-17 21:18:54', '1');
INSERT INTO admin_log VALUES ('1784', '1', 'Electric Motor', 'device_executions', '6', 'INSERT', 'Electric Motor configured with Test execution', '2016-11-17 21:18:54', '1');
INSERT INTO admin_log VALUES ('1785', '1', 'Electric Motor', 'device_measurements', '18', 'INSERT', 'Electric Motor configured with Cookin measurement', '2016-11-17 21:18:54', '1');
INSERT INTO admin_log VALUES ('1786', '1', 'Electric Motor', 'device_measurements', '22', 'INSERT', 'Electric Motor configured with name measurement', '2016-11-17 21:18:54', '1');
INSERT INTO admin_log VALUES ('1787', '1', 'Electric Motor', 'device_triggers', '1', 'INSERT', 'Electric Motor configured with Tank Overflow trigger', '2016-11-17 21:18:54', '1');
INSERT INTO admin_log VALUES ('1788', '1', 'hanz', 'devices', '13', 'UPDATE', 'Device - hanz modified', '2016-11-17 21:20:46', '1');
INSERT INTO admin_log VALUES ('1789', '1', 'hanz', 'device_executions', '1', 'INSERT', 'hanz configured with Stop Motor execution', '2016-11-17 21:20:46', '1');
INSERT INTO admin_log VALUES ('1790', '1', 'hanz', 'device_executions', '3', 'INSERT', 'hanz configured with Start Motor execution', '2016-11-17 21:20:47', '1');
INSERT INTO admin_log VALUES ('1791', '1', 'hanz', 'device_measurements', '8', 'INSERT', 'hanz configured with Level measurement', '2016-11-17 21:20:47', '1');
INSERT INTO admin_log VALUES ('1792', '1', 'hanz', 'device_measurements', '11', 'INSERT', 'hanz configured with Pressure measurement', '2016-11-17 21:20:47', '1');
INSERT INTO admin_log VALUES ('1793', '1', 'hanz', 'device_measurements', '18', 'INSERT', 'hanz configured with Cookin measurement', '2016-11-17 21:20:47', '1');
INSERT INTO admin_log VALUES ('1794', '1', 'hanz', 'device_triggers', '4', 'INSERT', 'hanz configured with Overheated Motor trigger', '2016-11-17 21:20:47', '1');
INSERT INTO admin_log VALUES ('1795', '1', 'hanz', 'device_triggers', '6', 'INSERT', 'hanz configured with Stuck Valve trigger', '2016-11-17 21:20:47', '1');
INSERT INTO admin_log VALUES ('1796', '1', 'hanz', 'devices', '13', 'UPDATE', 'Device - hanz modified', '2016-11-17 21:21:19', '1');
INSERT INTO admin_log VALUES ('1797', '1', 'hanz', 'device_executions', '1', 'INSERT', 'hanz configured with Stop Motor execution', '2016-11-17 21:21:19', '1');
INSERT INTO admin_log VALUES ('1798', '1', 'hanz', 'device_executions', '2', 'INSERT', 'hanz configured with Reboot OPCUA Server execution', '2016-11-17 21:21:19', '1');
INSERT INTO admin_log VALUES ('1799', '1', 'hanz', 'device_executions', '3', 'INSERT', 'hanz configured with Start Motor execution', '2016-11-17 21:21:19', '1');
INSERT INTO admin_log VALUES ('1800', '1', 'hanz', 'device_measurements', '11', 'INSERT', 'hanz configured with Pressure measurement', '2016-11-17 21:21:20', '1');
INSERT INTO admin_log VALUES ('1801', '1', 'hanz', 'device_measurements', '18', 'INSERT', 'hanz configured with Cookin measurement', '2016-11-17 21:21:20', '1');
INSERT INTO admin_log VALUES ('1802', '1', 'hanz', 'device_measurements', '19', 'INSERT', 'hanz configured with Becca1 measurement', '2016-11-17 21:21:20', '1');
INSERT INTO admin_log VALUES ('1803', '1', 'hanz', 'device_triggers', '1', 'INSERT', 'hanz configured with Tank Overflow trigger', '2016-11-17 21:21:20', '1');
INSERT INTO admin_log VALUES ('1804', '1', 'hanz', 'device_triggers', '4', 'INSERT', 'hanz configured with Overheated Motor trigger', '2016-11-17 21:21:20', '1');
INSERT INTO admin_log VALUES ('1805', '1', 'hanz', 'devices', '13', 'DELETE', 'Device - hanz deleted', '2016-11-17 21:22:34', '1');
INSERT INTO admin_log VALUES ('1806', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-17 21:59:58', '0');
INSERT INTO admin_log VALUES ('1807', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 02:18:00', '0');
INSERT INTO admin_log VALUES ('1808', '1', 'Electric Motor', 'devices', '18', 'UPDATE', 'Device - Electric Motor modified', '2016-11-18 02:48:42', '1');
INSERT INTO admin_log VALUES ('1809', '1', 'Electric Motor', 'device_executions', '1', 'INSERT', 'Electric Motor configured with Stop Motor execution', '2016-11-18 02:48:43', '1');
INSERT INTO admin_log VALUES ('1810', '1', 'Electric Motor', 'device_executions', '3', 'INSERT', 'Electric Motor configured with Start Motor execution', '2016-11-18 02:48:43', '1');
INSERT INTO admin_log VALUES ('1811', '1', 'Electric Motor', 'device_measurements', '8', 'INSERT', 'Electric Motor configured with Level measurement', '2016-11-18 02:48:43', '1');
INSERT INTO admin_log VALUES ('1812', '1', 'Electric Motor', 'device_measurements', '9', 'INSERT', 'Electric Motor configured with Temperature measurement', '2016-11-18 02:48:43', '1');
INSERT INTO admin_log VALUES ('1813', '1', 'Electric Motor', 'device_measurements', '11', 'INSERT', 'Electric Motor configured with Pressure measurement', '2016-11-18 02:48:43', '1');
INSERT INTO admin_log VALUES ('1814', '1', 'Electric Motor', 'device_triggers', '4', 'INSERT', 'Electric Motor configured with Overheated Motor trigger', '2016-11-18 02:48:43', '1');
INSERT INTO admin_log VALUES ('1815', '1', 'Electric Motor', 'devices', '17', 'DELETE', 'Device - Electric Motor deleted', '2016-11-18 02:49:04', '1');
INSERT INTO admin_log VALUES ('1816', '1', 'Electric Motor', 'devices', '18', 'UPDATE', 'Device - Electric Motor modified', '2016-11-18 02:51:11', '1');
INSERT INTO admin_log VALUES ('1817', '1', 'Electric Motor', 'devices', '18', 'UPDATE', 'Device - Electric Motor modified', '2016-11-18 02:51:27', '1');
INSERT INTO admin_log VALUES ('1818', '1', 'Electric Motor', 'device_executions', '1', 'INSERT', 'Electric Motor configured with Stop Motor execution', '2016-11-18 02:51:27', '1');
INSERT INTO admin_log VALUES ('1819', '1', 'Electric Motor', 'device_executions', '3', 'INSERT', 'Electric Motor configured with Start Motor execution', '2016-11-18 02:51:27', '1');
INSERT INTO admin_log VALUES ('1820', '1', 'Electric Motor', 'device_measurements', '9', 'INSERT', 'Electric Motor configured with Temperature measurement', '2016-11-18 02:51:27', '1');
INSERT INTO admin_log VALUES ('1821', '1', 'Electric Motor', 'device_measurements', '11', 'INSERT', 'Electric Motor configured with Pressure measurement', '2016-11-18 02:51:28', '1');
INSERT INTO admin_log VALUES ('1822', '1', 'Electric Motor', 'device_measurements', '18', 'INSERT', 'Electric Motor configured with Cookin measurement', '2016-11-18 02:51:28', '1');
INSERT INTO admin_log VALUES ('1823', '1', 'Electric Motor', 'device_triggers', '4', 'INSERT', 'Electric Motor configured with Overheated Motor trigger', '2016-11-18 02:51:28', '1');
INSERT INTO admin_log VALUES ('1824', '1', 'JST', 'tags', '14', 'INSERT', 'New tag - JST added', '2016-11-18 02:57:29', '1');
INSERT INTO admin_log VALUES ('1825', '1', 'KST', 'tags', '15', 'INSERT', 'New tag - KST added', '2016-11-18 02:57:43', '1');
INSERT INTO admin_log VALUES ('1826', '1', 'LST', 'tags', '16', 'INSERT', 'New tag - LST added', '2016-11-18 02:57:58', '1');
INSERT INTO admin_log VALUES ('1827', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 03:21:42', '0');
INSERT INTO admin_log VALUES ('1828', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:03:03', '1');
INSERT INTO admin_log VALUES ('1829', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:40:02', '1');
INSERT INTO admin_log VALUES ('1830', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:42:25', '1');
INSERT INTO admin_log VALUES ('1831', '5', 'Startup Pump', 'actions', '2', 'UPDATE', 'Action - Startup Pump modified', '2016-11-18 04:42:52', '1');
INSERT INTO admin_log VALUES ('1832', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:51:05', '1');
INSERT INTO admin_log VALUES ('1833', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:52:50', '1');
INSERT INTO admin_log VALUES ('1834', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:53:28', '1');
INSERT INTO admin_log VALUES ('1835', '5', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 04:53:41', '1');
INSERT INTO admin_log VALUES ('1836', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 05:20:14', '0');
INSERT INTO admin_log VALUES ('1837', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 05:20:39', '1');
INSERT INTO admin_log VALUES ('1838', '1', 'Shutdown Pump', 'action_tags', '14', 'INSERT', 'Shutdown Pump configured with JST tag', '2016-11-18 05:20:39', '1');
INSERT INTO admin_log VALUES ('1839', '1', 'Shutdown Pump', 'action_tags', '16', 'INSERT', 'Shutdown Pump configured with LST tag', '2016-11-18 05:20:39', '1');
INSERT INTO admin_log VALUES ('1840', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 05:21:14', '1');
INSERT INTO admin_log VALUES ('1841', '1', 'Shutdown Pump', 'action_tags', '15', 'INSERT', 'Shutdown Pump configured with KST tag', '2016-11-18 05:21:14', '1');
INSERT INTO admin_log VALUES ('1842', '1', 'Reboot OPCUA Server', 'actions', '5', 'UPDATE', 'Action - Reboot OPCUA Server modified', '2016-11-18 05:22:12', '1');
INSERT INTO admin_log VALUES ('1843', '1', 'Reboot OPCUA Server', 'action_tags', '14', 'INSERT', 'Reboot OPCUA Server configured with JST tag', '2016-11-18 05:22:12', '1');
INSERT INTO admin_log VALUES ('1844', '1', 'Reboot OPCUA Server', 'actions', '5', 'UPDATE', 'Action - Reboot OPCUA Server modified', '2016-11-18 05:22:14', '1');
INSERT INTO admin_log VALUES ('1845', '1', 'Reboot OPCUA Server', 'action_tags', '14', 'INSERT', 'Reboot OPCUA Server configured with JST tag', '2016-11-18 05:22:14', '1');
INSERT INTO admin_log VALUES ('1846', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 05:23:10', '1');
INSERT INTO admin_log VALUES ('1847', '1', 'Shutdown Pump', 'action_tags', '14', 'INSERT', 'Shutdown Pump configured with JST tag', '2016-11-18 05:23:10', '1');
INSERT INTO admin_log VALUES ('1848', '1', 'Shutdown Pump', 'action_tags', '16', 'INSERT', 'Shutdown Pump configured with LST tag', '2016-11-18 05:23:10', '1');
INSERT INTO admin_log VALUES ('1849', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump modified', '2016-11-18 05:23:24', '1');
INSERT INTO admin_log VALUES ('1850', '1', 'Shutdown Pump', 'action_tags', '15', 'INSERT', 'Shutdown Pump configured with KST tag', '2016-11-18 05:23:24', '1');
INSERT INTO admin_log VALUES ('1851', '1', 'Just Because', 'actions', '6', 'INSERT', 'New action - Just Because added', '2016-11-18 05:24:43', '1');
INSERT INTO admin_log VALUES ('1852', '1', 'Just Because', 'action_tags', '14', 'INSERT', 'Just Because configured with JST tag', '2016-11-18 05:24:43', '1');
INSERT INTO admin_log VALUES ('1853', '1', 'Just Because', 'action_tags', '15', 'INSERT', 'Just Because configured with KST tag', '2016-11-18 05:24:43', '1');
INSERT INTO admin_log VALUES ('1854', '1', 'Just Because', 'actions', '6', 'UPDATE', 'Action - Just Because modified', '2016-11-18 05:25:00', '1');
INSERT INTO admin_log VALUES ('1855', '1', 'Just Because', 'action_tags', '14', 'INSERT', 'Just Because configured with JST tag', '2016-11-18 05:25:00', '1');
INSERT INTO admin_log VALUES ('1856', '1', 'Just Because', 'actions', '6', 'UPDATE', 'Action - Just Because modified', '2016-11-18 05:25:20', '1');
INSERT INTO admin_log VALUES ('1857', '1', 'Just Because', 'action_tags', '15', 'INSERT', 'Just Because configured with KST tag', '2016-11-18 05:25:20', '1');
INSERT INTO admin_log VALUES ('1858', '1', 'Just Because', 'action_tags', '16', 'INSERT', 'Just Because configured with LST tag', '2016-11-18 05:25:20', '1');
INSERT INTO admin_log VALUES ('1859', '1', 'Just Because', 'actions', '6', 'UPDATE', 'Action - Just Because modified', '2016-11-18 05:25:32', '1');
INSERT INTO admin_log VALUES ('1860', '1', 'Just Because', 'action_tags', '14', 'INSERT', 'Just Because configured with JST tag', '2016-11-18 05:25:32', '1');
INSERT INTO admin_log VALUES ('1861', '1', 'Just Because', 'action_tags', '16', 'INSERT', 'Just Because configured with LST tag', '2016-11-18 05:25:32', '1');
INSERT INTO admin_log VALUES ('1862', '1', 'Overheated Motor', 'triggers', '4', 'UPDATE', 'Trigger - Overheated Motor modified', '2016-11-18 05:26:21', '1');
INSERT INTO admin_log VALUES ('1863', '1', 'Overheated Motor', 'trigger_actions', '1', 'INSERT', 'Overheated Motor configured with Shutdown Pump action', '2016-11-18 05:26:21', '1');
INSERT INTO admin_log VALUES ('1864', '1', 'Overheated Motor', 'trigger_actions', '3', 'INSERT', 'Overheated Motor configured with Shut off Valve action', '2016-11-18 05:26:21', '1');
INSERT INTO admin_log VALUES ('1865', '1', 'Overheated Motor', 'trigger_tags', '10', 'INSERT', 'Overheated Motor configured with FST trigger', '2016-11-18 05:26:21', '1');
INSERT INTO admin_log VALUES ('1866', '1', 'Overheated Motor', 'triggers', '4', 'UPDATE', 'Trigger - Overheated Motor modified', '2016-11-18 05:28:25', '1');
INSERT INTO admin_log VALUES ('1867', '1', 'Overheated Motor', 'trigger_actions', '2', 'INSERT', 'Overheated Motor configured with Startup Pump action', '2016-11-18 05:28:25', '1');
INSERT INTO admin_log VALUES ('1868', '1', 'Overheated Motor', 'trigger_actions', '6', 'INSERT', 'Overheated Motor configured with Just Because action', '2016-11-18 05:28:25', '1');
INSERT INTO admin_log VALUES ('1869', '1', 'Overheated Motor', 'trigger_tags', '5', 'INSERT', 'Overheated Motor configured with CST trigger', '2016-11-18 05:28:26', '1');
INSERT INTO admin_log VALUES ('1870', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 15:32:28', '0');
INSERT INTO admin_log VALUES ('1871', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 16:17:38', '0');
INSERT INTO admin_log VALUES ('1872', '5', 'Site', 'sites', '38', 'INSERT', 'New site - Site added', '2016-11-18 17:26:45', '1');
INSERT INTO admin_log VALUES ('1873', '5', 'Site', 'site_objects', '2', 'INSERT', 'Site configured with ACME XFR - 24 Oil Tank object', '2016-11-18 17:26:45', '1');
INSERT INTO admin_log VALUES ('1874', '5', 'Site', 'site_objects', '3', 'INSERT', 'Site configured with Sampson RR - 26 Water Tank object', '2016-11-18 17:26:45', '1');
INSERT INTO admin_log VALUES ('1875', '5', 'Site', 'site_objects', '5', 'INSERT', 'Site configured with Drake 25 Series Pump Jack object', '2016-11-18 17:26:45', '1');
INSERT INTO admin_log VALUES ('1876', '5', 'Account Admins', 'groups', '10', 'INSERT', 'New group - Account Admins added', '2016-11-18 17:47:07', '1');
INSERT INTO admin_log VALUES ('1877', '5', 'IT', 'groups', '11', 'INSERT', 'New group - IT added', '2016-11-18 17:47:52', '1');
INSERT INTO admin_log VALUES ('1878', '5', 'Pumpers', 'groups', '12', 'INSERT', 'New group - Pumpers added', '2016-11-18 17:48:05', '1');
INSERT INTO admin_log VALUES ('1879', '5', 'Administration', 'departments', '8', 'INSERT', 'New department - Administration added', '2016-11-18 17:49:23', '1');
INSERT INTO admin_log VALUES ('1880', '5', 'Office', 'departments', '10', 'INSERT', 'New department - Office added', '2016-11-18 18:00:56', '1');
INSERT INTO admin_log VALUES ('1881', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 19:21:58', '0');
INSERT INTO admin_log VALUES ('1882', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Admin', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1883', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Departments', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1884', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Groups', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1885', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Sites', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1886', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Users', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1887', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Dashboard', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1888', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Help', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1889', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Submit Ticket', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1890', '5', 'Account Admins', 'group_permissions', '10', 'INSERT', 'Account Admins granted access to Tickets', '2016-11-18 19:23:05', '1');
INSERT INTO admin_log VALUES ('1891', '5', 'Adam West', 'user_groups', '10', 'INSERT', 'Adam West configured with Account Admins group', '2016-11-18 19:28:36', '1');
INSERT INTO admin_log VALUES ('1892', '5', 'Adam West', 'user_groups', '10', 'INSERT', 'Adam West configured with Account Admins group', '2016-11-18 19:30:19', '1');
INSERT INTO admin_log VALUES ('1893', '5', 'Adam West', 'user_groups', '11', 'INSERT', 'Adam West configured with IT group', '2016-11-18 19:30:20', '1');
INSERT INTO admin_log VALUES ('1894', '5', 'Adam West', 'user_groups', '10', 'INSERT', 'Adam West configured with Account Admins group', '2016-11-18 19:30:45', '1');
INSERT INTO admin_log VALUES ('1895', '5', 'Adam West', 'user_groups', '11', 'INSERT', 'Adam West configured with IT group', '2016-11-18 19:30:45', '1');
INSERT INTO admin_log VALUES ('1896', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-18 19:31:23', '0');
INSERT INTO admin_log VALUES ('1897', '5', 'Adam West', 'user_departments', '8', 'INSERT', 'Adam West configured with Administration department', '2016-11-18 19:33:18', '36');
INSERT INTO admin_log VALUES ('1898', '5', 'Adam West', 'user_departments', '8', 'INSERT', 'Adam West configured with Administration department', '2016-11-18 19:37:48', '36');
INSERT INTO admin_log VALUES ('1899', '5', 'Adam West', 'user_departments', '8', 'INSERT', 'Adam West configured with Administration department', '2016-11-18 19:38:21', '36');
INSERT INTO admin_log VALUES ('1900', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-18 19:49:56', '0');
INSERT INTO admin_log VALUES ('1901', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-18 20:13:35', '0');
INSERT INTO admin_log VALUES ('1902', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-18 20:14:03', '0');
INSERT INTO admin_log VALUES ('1903', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 20:15:06', '0');
INSERT INTO admin_log VALUES ('1904', '1', 'John Technician', 'users', '47', 'INSERT', 'New user - tech added', '2016-11-18 20:16:30', '1');
INSERT INTO admin_log VALUES ('1905', '1', 'John Technician', 'user_groups', '6', 'INSERT', 'John Technician configured with Technician group', '2016-11-18 20:17:04', '1');
INSERT INTO admin_log VALUES ('1906', '1', 'Technicians', 'groups', '6', 'UPDATE', 'Group - Technicians modified', '2016-11-18 20:20:34', '1');
INSERT INTO admin_log VALUES ('1907', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Admin', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1908', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Sites', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1909', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to System', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1910', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Objects', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1911', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Device Menu', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1912', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Executions', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1913', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Measurements', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1914', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Triggers', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1915', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Actions', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1916', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Tags', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1917', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Modules', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1918', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Dashboard', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1919', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Help', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1920', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Submit Ticket', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1921', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Tickets', '2016-11-18 20:21:26', '1');
INSERT INTO admin_log VALUES ('1922', '1', 'John Technician', 'users', '47', 'LOGIN', 'tech logged in', '2016-11-18 20:22:07', '0');
INSERT INTO admin_log VALUES ('1923', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 20:23:01', '0');
INSERT INTO admin_log VALUES ('1924', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Admin', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1925', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Sites', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1926', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to System', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1927', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Objects', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1928', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Device Menu', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1929', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Executions', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1930', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Measurements', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1931', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Triggers', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1932', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Actions', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1933', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Tags', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1934', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Dashboard', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1935', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Help', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1936', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Submit Ticket', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1937', '1', 'Technicians', 'group_permissions', '6', 'INSERT', 'Technicians granted access to Tickets', '2016-11-18 20:23:15', '1');
INSERT INTO admin_log VALUES ('1938', '1', 'John Technician', 'users', '47', 'LOGIN', 'tech logged in', '2016-11-18 20:23:27', '0');
INSERT INTO admin_log VALUES ('1939', '1', 'John Technician', 'users', '47', 'LOGIN', 'tech logged in', '2016-11-18 20:49:59', '0');
INSERT INTO admin_log VALUES ('1940', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 20:52:57', '0');
INSERT INTO admin_log VALUES ('1941', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 21:25:06', '0');
INSERT INTO admin_log VALUES ('1942', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 21:38:18', '0');
INSERT INTO admin_log VALUES ('1943', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:03:15', '1');
INSERT INTO admin_log VALUES ('1944', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:03:16', '1');
INSERT INTO admin_log VALUES ('1945', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:03:16', '1');
INSERT INTO admin_log VALUES ('1946', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:03:41', '1');
INSERT INTO admin_log VALUES ('1947', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:03:41', '1');
INSERT INTO admin_log VALUES ('1948', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:03:41', '1');
INSERT INTO admin_log VALUES ('1949', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:05:56', '1');
INSERT INTO admin_log VALUES ('1950', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:05:56', '1');
INSERT INTO admin_log VALUES ('1951', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:05:56', '1');
INSERT INTO admin_log VALUES ('1952', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:06:17', '1');
INSERT INTO admin_log VALUES ('1953', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:06:17', '1');
INSERT INTO admin_log VALUES ('1954', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:06:17', '1');
INSERT INTO admin_log VALUES ('1955', '5', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:07:24', '1');
INSERT INTO admin_log VALUES ('1956', '5', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:07:24', '1');
INSERT INTO admin_log VALUES ('1957', '5', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:07:24', '1');
INSERT INTO admin_log VALUES ('1958', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 22:37:54', '0');
INSERT INTO admin_log VALUES ('1959', '1', 'Electric Motor', 'devices', '18', 'UPDATE', 'Device - Electric Motor modified', '2016-11-18 22:40:25', '1');
INSERT INTO admin_log VALUES ('1960', '1', 'Electric Motor', 'device_executions', '1', 'INSERT', 'Electric Motor configured with Stop Motor execution', '2016-11-18 22:40:25', '1');
INSERT INTO admin_log VALUES ('1961', '1', 'Electric Motor', 'device_executions', '3', 'INSERT', 'Electric Motor configured with Start Motor execution', '2016-11-18 22:40:25', '1');
INSERT INTO admin_log VALUES ('1962', '1', 'Electric Motor', 'device_measurements', '9', 'INSERT', 'Electric Motor configured with Temperature measurement', '2016-11-18 22:40:25', '1');
INSERT INTO admin_log VALUES ('1963', '1', 'Electric Motor', 'device_measurements', '11', 'INSERT', 'Electric Motor configured with Pressure measurement', '2016-11-18 22:40:25', '1');
INSERT INTO admin_log VALUES ('1964', '1', 'Electric Motor', 'device_measurements', '18', 'INSERT', 'Electric Motor configured with Cookin measurement', '2016-11-18 22:40:25', '1');
INSERT INTO admin_log VALUES ('1965', '1', 'Electric Motor', 'device_triggers', '4', 'INSERT', 'Electric Motor configured with Overheated Motor trigger', '2016-11-18 22:40:26', '1');
INSERT INTO admin_log VALUES ('1966', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:41:01', '1');
INSERT INTO admin_log VALUES ('1967', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:41:01', '1');
INSERT INTO admin_log VALUES ('1968', '1', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:41:01', '1');
INSERT INTO admin_log VALUES ('1969', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-18 22:42:27', '1');
INSERT INTO admin_log VALUES ('1970', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-18 22:42:27', '1');
INSERT INTO admin_log VALUES ('1971', '1', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-18 22:42:27', '1');
INSERT INTO admin_log VALUES ('1972', '1', 'Pump Jack', 'objects', '5', 'UPDATE', 'Object - Pump Jack modified', '2016-11-18 22:54:22', '1');
INSERT INTO admin_log VALUES ('1973', '1', 'Pump Jack', 'object_devices', '1', 'INSERT', 'Pump Jack configured with Tank Stick device', '2016-11-18 22:54:22', '1');
INSERT INTO admin_log VALUES ('1974', '1', 'Pump Jack', 'object_devices', '2', 'INSERT', 'Pump Jack configured with Transducer device', '2016-11-18 22:54:22', '1');
INSERT INTO admin_log VALUES ('1975', '1', 'Water Tank', 'objects', '3', 'UPDATE', 'Object - Water Tank modified', '2016-11-18 22:55:11', '1');
INSERT INTO admin_log VALUES ('1976', '1', 'Water Tank', 'object_devices', '1', 'INSERT', 'Water Tank configured with Tank Stick device', '2016-11-18 22:55:11', '1');
INSERT INTO admin_log VALUES ('1977', '1', 'Water Tank', 'object_devices', '2', 'INSERT', 'Water Tank configured with Transducer device', '2016-11-18 22:55:11', '1');
INSERT INTO admin_log VALUES ('1978', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-18 23:40:51', '0');
INSERT INTO admin_log VALUES ('1979', '5', 'Yankees Good Night', 'sites', '15', 'UPDATE', 'Site - Yankees Good Night modified', '2016-11-18 23:52:15', '1');
INSERT INTO admin_log VALUES ('1980', '5', 'Yankees Good Night', 'site_objects', '2', 'INSERT', 'Yankees Good Night configured with Oil Tank object', '2016-11-18 23:52:15', '1');
INSERT INTO admin_log VALUES ('1981', '5', 'Yankees Good Night', 'site_objects', '3', 'INSERT', 'Yankees Good Night configured with Water Tank object', '2016-11-18 23:52:15', '1');
INSERT INTO admin_log VALUES ('1982', '5', 'Yankees Good Night', 'site_objects', '5', 'INSERT', 'Yankees Good Night configured with Pump Jack object', '2016-11-18 23:52:15', '1');
INSERT INTO admin_log VALUES ('1983', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-19 14:26:36', '0');
INSERT INTO admin_log VALUES ('1984', '1', 'Oil Pump', 'devices', '19', 'INSERT', 'New device - Oil Pump added', '2016-11-19 14:28:09', '1');
INSERT INTO admin_log VALUES ('1985', '1', 'Oil Pump', 'device_executions', '6', 'INSERT', 'Oil Pump configured with Test execution', '2016-11-19 14:28:09', '1');
INSERT INTO admin_log VALUES ('1986', '1', 'Oil Pump', 'device_measurements', '11', 'INSERT', 'Oil Pump configured with Pressure measurement', '2016-11-19 14:28:10', '1');
INSERT INTO admin_log VALUES ('1987', '1', 'Oil Pump', 'device_measurements', '19', 'INSERT', 'Oil Pump configured with Becca1 measurement', '2016-11-19 14:28:10', '1');
INSERT INTO admin_log VALUES ('1988', '1', 'Oil Pump', 'device_triggers', '6', 'INSERT', 'Oil Pump configured with Stuck Valve trigger', '2016-11-19 14:28:10', '1');
INSERT INTO admin_log VALUES ('1989', '1', 'Oil Pump1', 'devices', '19', 'UPDATE', 'Device - Oil Pump1 modified', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1990', '1', 'Oil Pump1', 'device_executions', '1', 'INSERT', 'Oil Pump1 configured with Stop Motor execution', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1991', '1', 'Oil Pump1', 'device_executions', '6', 'INSERT', 'Oil Pump1 configured with Test execution', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1992', '1', 'Oil Pump1', 'device_measurements', '8', 'INSERT', 'Oil Pump1 configured with Level measurement', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1993', '1', 'Oil Pump1', 'device_measurements', '11', 'INSERT', 'Oil Pump1 configured with Pressure measurement', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1994', '1', 'Oil Pump1', 'device_measurements', '19', 'INSERT', 'Oil Pump1 configured with Becca1 measurement', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1995', '1', 'Oil Pump1', 'device_triggers', '1', 'INSERT', 'Oil Pump1 configured with Tank Overflow trigger', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1996', '1', 'Oil Pump1', 'device_triggers', '6', 'INSERT', 'Oil Pump1 configured with Stuck Valve trigger', '2016-11-19 14:29:21', '1');
INSERT INTO admin_log VALUES ('1997', '1', 'Oil Pump', 'devices', '19', 'UPDATE', 'Device - Oil Pump modified', '2016-11-19 14:29:44', '1');
INSERT INTO admin_log VALUES ('1998', '1', 'Oil Pump', 'device_executions', '1', 'INSERT', 'Oil Pump configured with Stop Motor execution', '2016-11-19 14:29:44', '1');
INSERT INTO admin_log VALUES ('1999', '1', 'Oil Pump', 'device_executions', '6', 'INSERT', 'Oil Pump configured with Test execution', '2016-11-19 14:29:44', '1');
INSERT INTO admin_log VALUES ('2000', '1', 'Oil Pump', 'device_measurements', '8', 'INSERT', 'Oil Pump configured with Level measurement', '2016-11-19 14:29:44', '1');
INSERT INTO admin_log VALUES ('2001', '1', 'Oil Pump', 'device_measurements', '11', 'INSERT', 'Oil Pump configured with Pressure measurement', '2016-11-19 14:29:45', '1');
INSERT INTO admin_log VALUES ('2002', '1', 'Oil Pump', 'device_measurements', '19', 'INSERT', 'Oil Pump configured with Becca1 measurement', '2016-11-19 14:29:45', '1');
INSERT INTO admin_log VALUES ('2003', '1', 'Oil Pump', 'device_triggers', '1', 'INSERT', 'Oil Pump configured with Tank Overflow trigger', '2016-11-19 14:29:45', '1');
INSERT INTO admin_log VALUES ('2004', '1', 'Oil Pump', 'device_triggers', '6', 'INSERT', 'Oil Pump configured with Stuck Valve trigger', '2016-11-19 14:29:45', '1');
INSERT INTO admin_log VALUES ('2005', '1', 'Kill Site', 'executions', '7', 'INSERT', 'New execution - Kill Site added', '2016-11-19 14:38:10', '1');
INSERT INTO admin_log VALUES ('2006', '1', 'Kill Site', 'execution_tags', '1', 'INSERT', 'Kill Site configured with Stop Motor tag', '2016-11-19 14:38:10', '1');
INSERT INTO admin_log VALUES ('2007', '1', 'Kill Site', 'execution_tags', '11', 'INSERT', 'Kill Site configured with  tag', '2016-11-19 14:38:11', '1');
INSERT INTO admin_log VALUES ('2008', '1', 'Kill Site1', 'executions', '7', 'UPDATE', 'Execution - Kill Site1 modified', '2016-11-19 14:38:24', '1');
INSERT INTO admin_log VALUES ('2009', '1', 'Kill Site1', 'execution_tags', '8', 'INSERT', 'Kill Site1 configured with  tag', '2016-11-19 14:38:24', '1');
INSERT INTO admin_log VALUES ('2010', '1', 'Kill Site', 'executions', '7', 'UPDATE', 'Execution - Kill Site modified', '2016-11-19 14:38:38', '1');
INSERT INTO admin_log VALUES ('2011', '1', 'Kill Site', 'execution_tags', '11', 'INSERT', 'Kill Site configured with  tag', '2016-11-19 14:38:38', '1');
INSERT INTO admin_log VALUES ('2012', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-19 20:21:18', '0');
INSERT INTO admin_log VALUES ('2013', '1', 'Heater Treater', 'devices', '20', 'INSERT', 'New device - Heater Treater added', '2016-11-19 20:32:23', '1');
INSERT INTO admin_log VALUES ('2014', '1', 'Heater Treater', 'device_executions', '1', 'INSERT', 'Heater Treater configured with Stop Motor execution', '2016-11-19 20:32:24', '1');
INSERT INTO admin_log VALUES ('2015', '1', 'Heater Treater', 'device_measurements', '9', 'INSERT', 'Heater Treater configured with Temperature measurement', '2016-11-19 20:32:24', '1');
INSERT INTO admin_log VALUES ('2016', '1', 'Heater Treater', 'device_measurements', '11', 'INSERT', 'Heater Treater configured with Pressure measurement', '2016-11-19 20:32:24', '1');
INSERT INTO admin_log VALUES ('2017', '1', 'Heater Treater', 'device_measurements', '18', 'INSERT', 'Heater Treater configured with Cookin measurement', '2016-11-19 20:32:24', '1');
INSERT INTO admin_log VALUES ('2018', '1', 'Heater Treater', 'device_triggers', '4', 'INSERT', 'Heater Treater configured with Overheated Motor trigger', '2016-11-19 20:32:24', '1');
INSERT INTO admin_log VALUES ('2019', '1', 'Heater Treater', 'device_triggers', '6', 'INSERT', 'Heater Treater configured with Stuck Valve trigger', '2016-11-19 20:32:24', '1');
INSERT INTO admin_log VALUES ('2020', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-19 20:57:26', '0');
INSERT INTO admin_log VALUES ('2021', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-19 22:29:51', '0');
INSERT INTO admin_log VALUES ('2022', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-19 22:51:51', '0');
INSERT INTO admin_log VALUES ('2023', '1', 'Stop Motor', 'executions', '1', 'UPDATE', 'Execution - Stop Motor modified', '2016-11-19 23:12:50', '1');
INSERT INTO admin_log VALUES ('2024', '1', 'Stop Motor', 'execution_tags', '1', 'INSERT', 'Stop Motor configured with Stop Motor tag', '2016-11-19 23:12:50', '1');
INSERT INTO admin_log VALUES ('2025', '1', 'Stop Motor', 'execution_tags', '8', 'INSERT', 'Stop Motor configured with  tag', '2016-11-19 23:12:50', '1');
INSERT INTO admin_log VALUES ('2026', '1', 'Notify Super', 'executions', '8', 'INSERT', 'New execution - Notify Super added', '2016-11-19 23:42:29', '1');
INSERT INTO admin_log VALUES ('2027', '1', 'Notify Super', 'execution_tags', '1', 'INSERT', 'Notify Super configured with Stop Motor tag', '2016-11-19 23:42:30', '1');
INSERT INTO admin_log VALUES ('2028', '1', 'Notify Super', 'execution_tags', '11', 'INSERT', 'Notify Super configured with  tag', '2016-11-19 23:42:30', '1');
INSERT INTO admin_log VALUES ('2029', '1', 'Notify Super1', 'executions', '8', 'UPDATE', 'Execution - Notify Super1 modified', '2016-11-19 23:43:11', '1');
INSERT INTO admin_log VALUES ('2030', '1', 'Notify Super1', 'execution_tags', '8', 'INSERT', 'Notify Super1 configured with Notify Super1 tag', '2016-11-19 23:43:11', '1');
INSERT INTO admin_log VALUES ('2031', '1', 'Notify Super1', 'execution_tags', '11', 'INSERT', 'Notify Super1 configured with  tag', '2016-11-19 23:43:11', '1');
INSERT INTO admin_log VALUES ('2032', '1', 'Notify Super', 'executions', '8', 'UPDATE', 'Execution - Notify Super modified', '2016-11-19 23:43:29', '1');
INSERT INTO admin_log VALUES ('2033', '1', 'Notify Super', 'execution_tags', '8', 'INSERT', 'Notify Super configured with Notify Super tag', '2016-11-19 23:43:30', '1');
INSERT INTO admin_log VALUES ('2034', '1', 'Notify Super', 'execution_tags', '11', 'INSERT', 'Notify Super configured with  tag', '2016-11-19 23:43:30', '1');
INSERT INTO admin_log VALUES ('2035', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 00:16:29', '0');
INSERT INTO admin_log VALUES ('2036', '1', 'Catastrophic Faliure', 'triggers', '7', 'INSERT', 'New trigger - Catastrophic Faliure added', '2016-11-20 01:18:38', '1');
INSERT INTO admin_log VALUES ('2037', '1', 'Catastrophic Faliure', 'trigger_actions', '5', 'INSERT', 'Catastrophic Faliure configured with Reboot OPCUA Server action', '2016-11-20 01:18:38', '1');
INSERT INTO admin_log VALUES ('2038', '1', 'Catastrophic Faliure', 'trigger_actions', '3', 'INSERT', 'Catastrophic Faliure configured with Shut off Valve action', '2016-11-20 01:18:38', '1');
INSERT INTO admin_log VALUES ('2039', '1', 'Catastrophic Faliure', 'trigger_actions', '1', 'INSERT', 'Catastrophic Faliure configured with Shutdown Pump action', '2016-11-20 01:18:38', '1');
INSERT INTO admin_log VALUES ('2040', '1', 'Catastrophic Faliure', 'trigger_tags', '5', 'INSERT', 'Catastrophic Faliure configured with CST tag', '2016-11-20 01:18:38', '1');
INSERT INTO admin_log VALUES ('2041', '1', 'Catastrophic Faliure1', 'triggers', '7', 'UPDATE', 'Trigger - Catastrophic Faliure1 modified', '2016-11-20 01:19:01', '1');
INSERT INTO admin_log VALUES ('2042', '1', 'Catastrophic Faliure1', 'trigger_actions', '4', 'INSERT', 'Catastrophic Faliure1 configured with Open Valve action', '2016-11-20 01:19:01', '1');
INSERT INTO admin_log VALUES ('2043', '1', 'Catastrophic Faliure1', 'trigger_tags', '13', 'INSERT', 'Catastrophic Faliure1 configured with IST tag', '2016-11-20 01:19:02', '1');
INSERT INTO admin_log VALUES ('2044', '1', 'Catastrophic Faliure1', 'triggers', '7', 'UPDATE', 'Trigger - Catastrophic Faliure1 modified', '2016-11-20 01:19:23', '1');
INSERT INTO admin_log VALUES ('2045', '1', 'Catastrophic Faliure1', 'trigger_actions', '1', 'INSERT', 'Catastrophic Faliure1 configured with Shutdown Pump action', '2016-11-20 01:19:23', '1');
INSERT INTO admin_log VALUES ('2046', '1', 'Catastrophic Faliure1', 'trigger_actions', '3', 'INSERT', 'Catastrophic Faliure1 configured with Shut off Valve action', '2016-11-20 01:19:23', '1');
INSERT INTO admin_log VALUES ('2047', '1', 'Catastrophic Faliure1', 'trigger_actions', '5', 'INSERT', 'Catastrophic Faliure1 configured with Reboot OPCUA Server action', '2016-11-20 01:19:24', '1');
INSERT INTO admin_log VALUES ('2048', '1', 'Catastrophic Faliure1', 'trigger_tags', '13', 'INSERT', 'Catastrophic Faliure1 configured with IST tag', '2016-11-20 01:19:24', '1');
INSERT INTO admin_log VALUES ('2049', '1', 'Catastrophic Faliure', 'triggers', '7', 'UPDATE', 'Trigger - Catastrophic Faliure modified', '2016-11-20 01:19:34', '1');
INSERT INTO admin_log VALUES ('2050', '1', 'Catastrophic Faliure', 'trigger_actions', '1', 'INSERT', 'Catastrophic Faliure configured with Shutdown Pump action', '2016-11-20 01:19:34', '1');
INSERT INTO admin_log VALUES ('2051', '1', 'Catastrophic Faliure', 'trigger_actions', '3', 'INSERT', 'Catastrophic Faliure configured with Shut off Valve action', '2016-11-20 01:19:34', '1');
INSERT INTO admin_log VALUES ('2052', '1', 'Catastrophic Faliure', 'trigger_actions', '5', 'INSERT', 'Catastrophic Faliure configured with Reboot OPCUA Server action', '2016-11-20 01:19:34', '1');
INSERT INTO admin_log VALUES ('2053', '1', 'Catastrophic Faliure', 'trigger_tags', '13', 'INSERT', 'Catastrophic Faliure configured with IST tag', '2016-11-20 01:19:34', '1');
INSERT INTO admin_log VALUES ('2054', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 04:16:37', '0');
INSERT INTO admin_log VALUES ('2055', '1', 'Now is the Time', 'actions', '7', 'INSERT', 'New action - Now is the Time added', '2016-11-20 05:02:14', '1');
INSERT INTO admin_log VALUES ('2056', '1', 'Now is the Time', 'action_tags', '14', 'INSERT', 'Now is the Time configured with JST tag', '2016-11-20 05:02:14', '1');
INSERT INTO admin_log VALUES ('2057', '1', 'Now is the Time', 'action_tags', '15', 'INSERT', 'Now is the Time configured with KST tag', '2016-11-20 05:02:14', '1');
INSERT INTO admin_log VALUES ('2058', '1', 'Now is the Time1', 'actions', '7', 'UPDATE', 'Action - Now is the Time1 modified', '2016-11-20 05:02:37', '1');
INSERT INTO admin_log VALUES ('2059', '1', 'Now is the Time1', 'action_tags', '15', 'INSERT', 'Now is the Time1 configured with KST tag', '2016-11-20 05:02:37', '1');
INSERT INTO admin_log VALUES ('2060', '1', 'Now is the Time1', 'action_tags', '16', 'INSERT', 'Now is the Time1 configured with LST tag', '2016-11-20 05:02:37', '1');
INSERT INTO admin_log VALUES ('2061', '1', 'Now is the Time', 'actions', '7', 'UPDATE', 'Action - Now is the Time modified', '2016-11-20 05:03:00', '1');
INSERT INTO admin_log VALUES ('2062', '1', 'Now is the Time', 'action_tags', '16', 'INSERT', 'Now is the Time configured with LST tag', '2016-11-20 05:03:00', '1');
INSERT INTO admin_log VALUES ('2063', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump  deactivated', '2016-11-20 05:17:15', '1');
INSERT INTO admin_log VALUES ('2064', '1', 'Shutdown Pump', 'actions', '1', 'UPDATE', 'Action - Shutdown Pump  reactivated', '2016-11-20 05:17:19', '1');
INSERT INTO admin_log VALUES ('2065', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 06:01:46', '0');
INSERT INTO admin_log VALUES ('2066', '1', 'Transducer', 'devices', '2', 'UPDATE', 'Device - Transducer modified', '2016-11-20 07:36:33', '1');
INSERT INTO admin_log VALUES ('2067', '1', 'Transducer', 'device_executions', '2', 'INSERT', 'Transducer configured with Reboot OPCUA Server execution', '2016-11-20 07:36:33', '1');
INSERT INTO admin_log VALUES ('2068', '1', 'Transducer', 'device_executions', '3', 'INSERT', 'Transducer configured with Start Motor execution', '2016-11-20 07:36:33', '1');
INSERT INTO admin_log VALUES ('2069', '1', 'Transducer', 'device_measurements', '8', 'INSERT', 'Transducer configured with Level measurement', '2016-11-20 07:36:33', '1');
INSERT INTO admin_log VALUES ('2070', '1', 'Transducer', 'device_measurements', '11', 'INSERT', 'Transducer configured with Pressure measurement', '2016-11-20 07:36:34', '1');
INSERT INTO admin_log VALUES ('2071', '1', 'Transducer', 'device_triggers', '1', 'INSERT', 'Transducer configured with Tank Overflow trigger', '2016-11-20 07:36:34', '1');
INSERT INTO admin_log VALUES ('2072', '1', 'Transducer', 'device_triggers', '7', 'INSERT', 'Transducer configured with Catastrophic Faliure trigger', '2016-11-20 07:36:34', '1');
INSERT INTO admin_log VALUES ('2073', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 17:35:17', '0');
INSERT INTO admin_log VALUES ('2074', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 17:35:36', '0');
INSERT INTO admin_log VALUES ('2075', '1', 'Oil Tank', 'objects', '2', 'UPDATE', 'Object - Oil Tank modified', '2016-11-20 17:36:52', '1');
INSERT INTO admin_log VALUES ('2076', '1', 'Oil Tank', 'object_devices', '1', 'INSERT', 'Oil Tank configured with Tank Stick device', '2016-11-20 17:36:52', '1');
INSERT INTO admin_log VALUES ('2077', '1', 'Oil Tank', 'object_devices', '3', 'INSERT', 'Oil Tank configured with Transfer Pump device', '2016-11-20 17:36:52', '1');
INSERT INTO admin_log VALUES ('2078', '1', 'Oil Tank', 'object_devices', '18', 'INSERT', 'Oil Tank configured with Electric Motor device', '2016-11-20 17:36:52', '1');
INSERT INTO admin_log VALUES ('2079', '1', 'Oil Tank', 'object_devices', '19', 'INSERT', 'Oil Tank configured with Oil Pump device', '2016-11-20 17:36:52', '1');
INSERT INTO admin_log VALUES ('2080', '1', 'Oil Tank', 'object_devices', '20', 'INSERT', 'Oil Tank configured with Heater Treater device', '2016-11-20 17:36:52', '1');
INSERT INTO admin_log VALUES ('2081', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 19:33:44', '0');
INSERT INTO admin_log VALUES ('2082', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 20:39:03', '0');
INSERT INTO admin_log VALUES ('2083', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 21:07:28', '0');
INSERT INTO admin_log VALUES ('2084', '1', 'Administration', 'departments', '1', 'UPDATE', 'Department - Administration modified', '2016-11-20 21:26:32', '1');
INSERT INTO admin_log VALUES ('2085', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-20 23:07:42', '0');
INSERT INTO admin_log VALUES ('2086', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-21 05:38:47', '0');
INSERT INTO admin_log VALUES ('2087', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-21 18:24:07', '0');
INSERT INTO admin_log VALUES ('2088', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-21 18:25:01', '0');
INSERT INTO admin_log VALUES ('2089', '5', 'Adam West', 'users', '36', 'LOGIN', 'awest logged in', '2016-11-21 18:57:37', '0');
INSERT INTO admin_log VALUES ('2090', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-21 21:20:43', '0');
INSERT INTO admin_log VALUES ('2091', '5', 'Comanche #1', 'sites', '13', 'UPDATE', 'Site - Comanche #1 modified', '2016-11-21 21:38:00', '1');
INSERT INTO admin_log VALUES ('2092', '5', 'Comanche #1', 'site_objects', '2', 'INSERT', 'Comanche #1 configured with Oil Tank object', '2016-11-21 21:38:00', '1');
INSERT INTO admin_log VALUES ('2093', '5', 'Comanche #1', 'site_objects', '3', 'INSERT', 'Comanche #1 configured with Water Tank object', '2016-11-21 21:38:01', '1');
INSERT INTO admin_log VALUES ('2094', '5', 'Comanche #1', 'sites', '13', 'UPDATE', 'Site - Comanche #1 modified', '2016-11-21 21:39:06', '1');
INSERT INTO admin_log VALUES ('2095', '5', 'Comanche #1', 'site_objects', '2', 'INSERT', 'Comanche #1 configured with Oil Tank object', '2016-11-21 21:39:06', '1');
INSERT INTO admin_log VALUES ('2096', '5', 'Comanche #1', 'site_objects', '3', 'INSERT', 'Comanche #1 configured with Water Tank object', '2016-11-21 21:39:06', '1');
INSERT INTO admin_log VALUES ('2097', '5', 'Comanche #1', 'sites', '13', 'UPDATE', 'Site - Comanche #1 modified', '2016-11-21 21:40:27', '1');
INSERT INTO admin_log VALUES ('2098', '5', 'Comanche #1', 'site_objects', '2', 'INSERT', 'Comanche #1 configured with Oil Tank object', '2016-11-21 21:40:27', '1');
INSERT INTO admin_log VALUES ('2099', '5', 'Comanche #1', 'site_objects', '3', 'INSERT', 'Comanche #1 configured with Water Tank object', '2016-11-21 21:40:27', '1');
INSERT INTO admin_log VALUES ('2100', '5', 'Comanche #2', 'sites', '17', 'UPDATE', 'Site - Comanche #2 modified', '2016-11-21 21:41:26', '1');
INSERT INTO admin_log VALUES ('2101', '5', 'Comanche #2', 'site_objects', '2', 'INSERT', 'Comanche #2 configured with Oil Tank object', '2016-11-21 21:41:26', '1');
INSERT INTO admin_log VALUES ('2102', '5', 'Comanche #2', 'site_objects', '4', 'INSERT', 'Comanche #2 configured with Test\'s object', '2016-11-21 21:41:27', '1');
INSERT INTO admin_log VALUES ('2103', '5', 'Comanche #2', 'site_objects', '5', 'INSERT', 'Comanche #2 configured with Pump Jack object', '2016-11-21 21:41:27', '1');
INSERT INTO admin_log VALUES ('2104', '5', 'Comanche #3', 'sites', '11', 'UPDATE', 'Site - Comanche #3 modified', '2016-11-21 21:43:24', '1');
INSERT INTO admin_log VALUES ('2105', '5', 'Comanche #3', 'site_objects', '2', 'INSERT', 'Comanche #3 configured with Oil Tank object', '2016-11-21 21:43:25', '1');
INSERT INTO admin_log VALUES ('2106', '5', 'Comanche #3', 'site_objects', '3', 'INSERT', 'Comanche #3 configured with Water Tank object', '2016-11-21 21:43:25', '1');
INSERT INTO admin_log VALUES ('2107', '5', 'Comanche #3', 'site_objects', '5', 'INSERT', 'Comanche #3 configured with Pump Jack object', '2016-11-21 21:43:25', '1');
INSERT INTO admin_log VALUES ('2108', '5', 'Apache #1', 'sites', '16', 'UPDATE', 'Site - Apache #1 modified', '2016-11-21 21:54:19', '1');
INSERT INTO admin_log VALUES ('2109', '5', 'Apache #1', 'site_objects', '2', 'INSERT', 'Apache #1 configured with Oil Tank object', '2016-11-21 21:54:19', '1');
INSERT INTO admin_log VALUES ('2110', '5', 'Apache #1', 'site_objects', '3', 'INSERT', 'Apache #1 configured with Water Tank object', '2016-11-21 21:54:19', '1');
INSERT INTO admin_log VALUES ('2111', '5', 'Apache #1', 'site_objects', '5', 'INSERT', 'Apache #1 configured with Pump Jack object', '2016-11-21 21:54:19', '1');
INSERT INTO admin_log VALUES ('2112', '5', 'Apache #2', 'sites', '5', 'UPDATE', 'Site - Apache #2 modified', '2016-11-21 21:54:39', '1');
INSERT INTO admin_log VALUES ('2113', '5', 'Apache #3', 'sites', '12', 'UPDATE', 'Site - Apache #3 modified', '2016-11-21 21:55:25', '1');
INSERT INTO admin_log VALUES ('2114', '5', 'Apache #3', 'site_objects', '2', 'INSERT', 'Apache #3 configured with Oil Tank object', '2016-11-21 21:55:25', '1');
INSERT INTO admin_log VALUES ('2115', '5', 'Apache #3', 'site_objects', '4', 'INSERT', 'Apache #3 configured with Test\'s object', '2016-11-21 21:55:25', '1');
INSERT INTO admin_log VALUES ('2116', '5', 'Apache #3', 'site_objects', '6', 'INSERT', 'Apache #3 configured with 1234 object', '2016-11-21 21:55:25', '1');
INSERT INTO admin_log VALUES ('2117', '5', 'Apache #3', 'sites', '12', 'UPDATE', 'Site - Apache #3 modified', '2016-11-21 21:56:43', '1');
INSERT INTO admin_log VALUES ('2118', '5', 'Apache #3', 'site_objects', '2', 'INSERT', 'Apache #3 configured with Oil Tank object', '2016-11-21 21:56:43', '1');
INSERT INTO admin_log VALUES ('2119', '5', 'Apache #3', 'site_objects', '4', 'INSERT', 'Apache #3 configured with Test\'s object', '2016-11-21 21:56:43', '1');
INSERT INTO admin_log VALUES ('2120', '5', 'Apache #3', 'site_objects', '6', 'INSERT', 'Apache #3 configured with 1234 object', '2016-11-21 21:56:43', '1');
INSERT INTO admin_log VALUES ('2121', '5', 'Apache #3', 'sites', '12', 'UPDATE', 'Site - Apache #3 modified', '2016-11-21 21:57:33', '1');
INSERT INTO admin_log VALUES ('2122', '5', 'Apache #3', 'site_objects', '2', 'INSERT', 'Apache #3 configured with Oil Tank object', '2016-11-21 21:57:33', '1');
INSERT INTO admin_log VALUES ('2123', '5', 'Apache #3', 'site_objects', '4', 'INSERT', 'Apache #3 configured with Test\'s object', '2016-11-21 21:57:33', '1');
INSERT INTO admin_log VALUES ('2124', '5', 'Apache #3', 'site_objects', '6', 'INSERT', 'Apache #3 configured with 1234 object', '2016-11-21 21:57:33', '1');
INSERT INTO admin_log VALUES ('2125', '5', 'Comanche #4', 'sites', '19', 'UPDATE', 'Site - Comanche #4 modified', '2016-11-21 21:59:19', '1');
INSERT INTO admin_log VALUES ('2126', '5', 'Comanche #4', 'site_objects', '3', 'INSERT', 'Comanche #4 configured with Water Tank object', '2016-11-21 21:59:19', '1');
INSERT INTO admin_log VALUES ('2127', '5', 'Comanche #4', 'site_objects', '5', 'INSERT', 'Comanche #4 configured with Pump Jack object', '2016-11-21 21:59:19', '1');
INSERT INTO admin_log VALUES ('2128', '5', 'Comanche #4', 'sites', '19', 'UPDATE', 'Site - Comanche #4 modified', '2016-11-21 21:59:58', '1');
INSERT INTO admin_log VALUES ('2129', '5', 'Comanche #4', 'site_objects', '3', 'INSERT', 'Comanche #4 configured with Water Tank object', '2016-11-21 21:59:59', '1');
INSERT INTO admin_log VALUES ('2130', '5', 'Comanche #4', 'site_objects', '5', 'INSERT', 'Comanche #4 configured with Pump Jack object', '2016-11-21 21:59:59', '1');
INSERT INTO admin_log VALUES ('2131', '5', 'Comanche #5', 'sites', '6', 'UPDATE', 'Site - Comanche #5 modified', '2016-11-21 22:01:07', '1');
INSERT INTO admin_log VALUES ('2132', '5', 'Comanche #5', 'site_objects', '2', 'INSERT', 'Comanche #5 configured with Oil Tank object', '2016-11-21 22:01:07', '1');
INSERT INTO admin_log VALUES ('2133', '5', 'Comanche #5', 'site_objects', '3', 'INSERT', 'Comanche #5 configured with Water Tank object', '2016-11-21 22:01:07', '1');
INSERT INTO admin_log VALUES ('2134', '5', 'Comanche #6', 'sites', '7', 'UPDATE', 'Site - Comanche #6 modified', '2016-11-21 22:01:28', '1');
INSERT INTO admin_log VALUES ('2135', '5', 'Iroquois #1', 'sites', '34', 'UPDATE', 'Site - Iroquois #1 modified', '2016-11-21 22:04:01', '1');
INSERT INTO admin_log VALUES ('2136', '5', 'Iroquois #1', 'site_objects', '2', 'INSERT', 'Iroquois #1 configured with Oil Tank object', '2016-11-21 22:04:01', '1');
INSERT INTO admin_log VALUES ('2137', '5', 'Iroquois #1', 'site_objects', '3', 'INSERT', 'Iroquois #1 configured with Water Tank object', '2016-11-21 22:04:01', '1');
INSERT INTO admin_log VALUES ('2138', '5', 'Iroquois #1', 'site_objects', '5', 'INSERT', 'Iroquois #1 configured with Pump Jack object', '2016-11-21 22:04:01', '1');
INSERT INTO admin_log VALUES ('2139', '5', 'Iroquois #2', 'sites', '30', 'UPDATE', 'Site - Iroquois #2 modified', '2016-11-21 22:04:20', '1');
INSERT INTO admin_log VALUES ('2140', '5', 'Iroquois #3', 'sites', '20', 'UPDATE', 'Site - Iroquois #3 modified', '2016-11-21 22:05:13', '1');
INSERT INTO admin_log VALUES ('2141', '5', 'Iroquois #3', 'site_objects', '2', 'INSERT', 'Iroquois #3 configured with Oil Tank object', '2016-11-21 22:05:13', '1');
INSERT INTO admin_log VALUES ('2142', '5', 'Iroquois #3', 'site_objects', '3', 'INSERT', 'Iroquois #3 configured with Water Tank object', '2016-11-21 22:05:13', '1');
INSERT INTO admin_log VALUES ('2143', '5', 'Iroquois #3', 'sites', '20', 'UPDATE', 'Site - Iroquois #3 modified', '2016-11-21 22:05:49', '1');
INSERT INTO admin_log VALUES ('2144', '5', 'Iroquois #3', 'site_objects', '2', 'INSERT', 'Iroquois #3 configured with Oil Tank object', '2016-11-21 22:05:49', '1');
INSERT INTO admin_log VALUES ('2145', '5', 'Iroquois #3', 'site_objects', '3', 'INSERT', 'Iroquois #3 configured with Water Tank object', '2016-11-21 22:05:49', '1');
INSERT INTO admin_log VALUES ('2146', '5', 'Iroquois #1', 'sites', '34', 'UPDATE', 'Site - Iroquois #1 modified', '2016-11-21 22:06:40', '1');
INSERT INTO admin_log VALUES ('2147', '5', 'Iroquois #1', 'site_objects', '2', 'INSERT', 'Iroquois #1 configured with Oil Tank object', '2016-11-21 22:06:40', '1');
INSERT INTO admin_log VALUES ('2148', '5', 'Iroquois #1', 'site_objects', '3', 'INSERT', 'Iroquois #1 configured with Water Tank object', '2016-11-21 22:06:40', '1');
INSERT INTO admin_log VALUES ('2149', '5', 'Iroquois #1', 'site_objects', '5', 'INSERT', 'Iroquois #1 configured with Pump Jack object', '2016-11-21 22:06:40', '1');
INSERT INTO admin_log VALUES ('2150', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-22 01:37:58', '0');
INSERT INTO admin_log VALUES ('2151', '5', 'Golden Field #1', 'sites', '22', 'UPDATE', 'Site - Golden Field #1 modified', '2016-11-22 01:42:54', '1');
INSERT INTO admin_log VALUES ('2152', '5', 'Golden Field #1', 'site_objects', '5', 'INSERT', 'Golden Field #1 configured with Pump Jack object', '2016-11-22 01:42:54', '1');
INSERT INTO admin_log VALUES ('2153', '5', 'Golden Field #2', 'sites', '18', 'UPDATE', 'Site - Golden Field #2 modified', '2016-11-22 01:43:13', '1');
INSERT INTO admin_log VALUES ('2154', '5', 'Golden Field #2', 'site_objects', '2', 'INSERT', 'Golden Field #2 configured with Oil Tank object', '2016-11-22 01:43:13', '1');
INSERT INTO admin_log VALUES ('2155', '5', 'Golden Field #2', 'site_objects', '4', 'INSERT', 'Golden Field #2 configured with Test\'s object', '2016-11-22 01:43:13', '1');
INSERT INTO admin_log VALUES ('2156', '5', 'Golden Field #2', 'site_objects', '8', 'INSERT', 'Golden Field #2 configured with sdtwgbw object', '2016-11-22 01:43:13', '1');
INSERT INTO admin_log VALUES ('2157', '5', 'Golden Field #3', 'sites', '38', 'UPDATE', 'Site - Golden Field #3 modified', '2016-11-22 01:43:33', '1');
INSERT INTO admin_log VALUES ('2158', '5', 'Golden Field #3', 'site_objects', '2', 'INSERT', 'Golden Field #3 configured with Oil Tank object', '2016-11-22 01:43:33', '1');
INSERT INTO admin_log VALUES ('2159', '5', 'Golden Field #3', 'site_objects', '3', 'INSERT', 'Golden Field #3 configured with Water Tank object', '2016-11-22 01:43:33', '1');
INSERT INTO admin_log VALUES ('2160', '5', 'Golden Field #3', 'site_objects', '5', 'INSERT', 'Golden Field #3 configured with Pump Jack object', '2016-11-22 01:43:33', '1');
INSERT INTO admin_log VALUES ('2161', '5', 'Golden Field #4', 'sites', '21', 'UPDATE', 'Site - Golden Field #4 modified', '2016-11-22 01:43:48', '1');
INSERT INTO admin_log VALUES ('2162', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-22 02:17:48', '0');
INSERT INTO admin_log VALUES ('2163', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-22 03:59:12', '0');
INSERT INTO admin_log VALUES ('2164', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-22 21:15:17', '0');
INSERT INTO admin_log VALUES ('2165', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-22 22:20:01', '0');
INSERT INTO admin_log VALUES ('2166', '5', 'Comanche #5', 'sites', '39', 'INSERT', 'New site - Comanche #5 added', '2016-11-22 22:33:50', '1');
INSERT INTO admin_log VALUES ('2167', '5', 'Comanche #5', 'site_objects', '2', 'INSERT', 'Comanche #5 configured with Oil Tank object', '2016-11-22 22:33:51', '1');
INSERT INTO admin_log VALUES ('2168', '5', 'Comanche #5', 'site_objects', '3', 'INSERT', 'Comanche #5 configured with Water Tank object', '2016-11-22 22:33:51', '1');
INSERT INTO admin_log VALUES ('2169', '5', 'Comanche #5', 'site_objects', '5', 'INSERT', 'Comanche #5 configured with Pump Jack object', '2016-11-22 22:33:51', '1');
INSERT INTO admin_log VALUES ('2170', '5', 'Comanche #7', 'sites', '39', 'UPDATE', 'Site - Comanche #7 modified', '2016-11-22 22:34:07', '1');
INSERT INTO admin_log VALUES ('2171', '5', 'Comanche #7', 'site_objects', '2', 'INSERT', 'Comanche #7 configured with Oil Tank object', '2016-11-22 22:34:07', '1');
INSERT INTO admin_log VALUES ('2172', '5', 'Comanche #7', 'site_objects', '3', 'INSERT', 'Comanche #7 configured with Water Tank object', '2016-11-22 22:34:07', '1');
INSERT INTO admin_log VALUES ('2173', '5', 'Comanche #7', 'site_objects', '5', 'INSERT', 'Comanche #7 configured with Pump Jack object', '2016-11-22 22:34:07', '1');
INSERT INTO admin_log VALUES ('2174', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-22 23:38:37', '0');
INSERT INTO admin_log VALUES ('2175', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-23 00:00:41', '0');
INSERT INTO admin_log VALUES ('2176', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-23 00:16:01', '0');
INSERT INTO admin_log VALUES ('2177', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-23 00:27:13', '0');
INSERT INTO admin_log VALUES ('2178', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-24 14:36:42', '0');
INSERT INTO admin_log VALUES ('2179', '1', 'Object Menu', 'modules', '24', 'UPDATE', 'Module - Object Menu modified', '2016-11-24 15:00:38', '1');
INSERT INTO admin_log VALUES ('2180', '1', 'Objects', 'modules', '48', 'INSERT', 'New module - Objects created', '2016-11-24 15:01:09', '1');
INSERT INTO admin_log VALUES ('2181', '1', 'Object Images', 'modules', '49', 'INSERT', 'New module - Object Images created', '2016-11-24 15:01:49', '1');
INSERT INTO admin_log VALUES ('2182', '1', 'Images', 'modules', '49', 'UPDATE', 'Module - Images modified', '2016-11-24 15:02:44', '1');
INSERT INTO admin_log VALUES ('2183', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2184', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2185', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2186', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2187', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2188', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2189', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2190', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2191', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Object Menu', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2192', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2193', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Images', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2194', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2195', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2196', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2197', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2198', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2199', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Actions', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2200', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2201', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2202', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2203', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2204', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2205', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-24 15:03:03', '1');
INSERT INTO admin_log VALUES ('2206', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-24 15:03:14', '0');
INSERT INTO admin_log VALUES ('2207', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-24 16:01:46', '0');
INSERT INTO admin_log VALUES ('2208', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-27 17:37:55', '0');
INSERT INTO admin_log VALUES ('2209', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-27 21:20:29', '0');
INSERT INTO admin_log VALUES ('2210', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-27 22:23:33', '0');
INSERT INTO admin_log VALUES ('2211', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-28 05:45:11', '0');
INSERT INTO admin_log VALUES ('2212', '1', 'test123', 'object_images', null, 'INSERT', 'New object image - test123 added', '2016-11-28 05:58:44', '0');
INSERT INTO admin_log VALUES ('2213', '1', 'test123', 'object_images', '4', 'DELETE', 'Object image - test123 deleted', '2016-11-28 06:00:59', '1');
INSERT INTO admin_log VALUES ('2214', '1', 'Testing', 'object_images', null, 'INSERT', 'New object image - Testing added', '2016-11-28 06:01:32', '0');
INSERT INTO admin_log VALUES ('2215', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-28 16:31:21', '0');
INSERT INTO admin_log VALUES ('2216', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-28 17:04:51', '0');
INSERT INTO admin_log VALUES ('2217', '1', 'Pump Jack', 'object_images', '1', 'UPDATE', 'Object image - Pump Jack  deactivated', '2016-11-28 17:41:45', '1');
INSERT INTO admin_log VALUES ('2218', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-28 17:55:54', '0');
INSERT INTO admin_log VALUES ('2219', '1', 'Pump Jack', 'object_images', '1', 'UPDATE', 'Object image - Pump Jack  reactivated', '2016-11-28 18:07:01', '1');
INSERT INTO admin_log VALUES ('2220', '1', 'Testing', 'object_images', '5', 'UPDATE', 'Object image - Testing  deactivated', '2016-11-28 18:07:09', '1');
INSERT INTO admin_log VALUES ('2221', '1', 'Testing', 'object_images', '5', 'UPDATE', 'Object image - Testing  reactivated', '2016-11-28 18:07:40', '1');
INSERT INTO admin_log VALUES ('2222', '1', 'Oil Tank', 'object_images', '2', 'UPDATE', 'Object image - Oil Tank  deactivated', '2016-11-28 18:23:24', '1');
INSERT INTO admin_log VALUES ('2223', '1', 'Oil Tank', 'object_images', '2', 'UPDATE', 'Object image - Oil Tank  reactivated', '2016-11-28 18:23:30', '1');
INSERT INTO admin_log VALUES ('2224', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-28 21:04:26', '0');
INSERT INTO admin_log VALUES ('2225', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-29 16:56:00', '0');
INSERT INTO admin_log VALUES ('2226', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-29 17:27:36', '0');
INSERT INTO admin_log VALUES ('2227', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-29 17:56:34', '0');
INSERT INTO admin_log VALUES ('2228', '1', 'Manufacturers', 'modules', '50', 'INSERT', 'New module - Manufacturers created', '2016-11-29 19:00:56', '1');
INSERT INTO admin_log VALUES ('2229', '1', 'Models', 'modules', '51', 'INSERT', 'New module - Models created', '2016-11-29 19:01:35', '1');
INSERT INTO admin_log VALUES ('2230', '1', 'Manufacturers', 'modules', '52', 'INSERT', 'New module - Manufacturers created', '2016-11-29 19:02:31', '1');
INSERT INTO admin_log VALUES ('2231', '1', 'Manufacturers', 'modules', '53', 'INSERT', 'New module - Manufacturers created', '2016-11-29 19:02:31', '1');
INSERT INTO admin_log VALUES ('2232', '1', 'Manufacturers', 'modules', '54', 'INSERT', 'New module - Manufacturers created', '2016-11-29 19:02:31', '1');
INSERT INTO admin_log VALUES ('2233', '1', 'Manufacturers', 'modules', '55', 'INSERT', 'New module - Manufacturers created', '2016-11-29 19:02:31', '1');
INSERT INTO admin_log VALUES ('2234', '1', 'Manufacturers', 'modules', '53', 'DELETE', 'Module - Manufacturers deleted', '2016-11-29 19:03:10', '1');
INSERT INTO admin_log VALUES ('2235', '1', 'Manufacturers', 'modules', '52', 'DELETE', 'Module - Manufacturers deleted', '2016-11-29 19:03:24', '1');
INSERT INTO admin_log VALUES ('2236', '1', 'Manufacturers', 'modules', '55', 'DELETE', 'Module - Manufacturers deleted', '2016-11-29 19:03:43', '1');
INSERT INTO admin_log VALUES ('2237', '1', 'Models', 'modules', '56', 'INSERT', 'New module - Models created', '2016-11-29 19:04:15', '1');
INSERT INTO admin_log VALUES ('2238', '1', 'Models', 'modules', '51', 'UPDATE', 'Module - Models modified', '2016-11-29 19:04:45', '1');
INSERT INTO admin_log VALUES ('2239', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2240', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2241', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2242', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2243', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2244', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2245', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2246', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2247', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Object Menu', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2248', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2249', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Images', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2250', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2251', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2252', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2253', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2254', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2255', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2256', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2257', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Actions', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2258', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2259', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2260', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2261', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2262', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2263', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2264', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2265', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-11-29 19:06:16', '1');
INSERT INTO admin_log VALUES ('2266', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-29 19:06:29', '0');
INSERT INTO admin_log VALUES ('2267', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-29 20:57:41', '0');
INSERT INTO admin_log VALUES ('2268', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-30 01:02:47', '0');
INSERT INTO admin_log VALUES ('2269', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-30 01:47:32', '0');
INSERT INTO admin_log VALUES ('2270', '1', 'fred', 'manufacturers', '1', 'INSERT', 'New Object manufacturer - fred added', '2016-11-30 02:17:51', '1');
INSERT INTO admin_log VALUES ('2271', '1', 'fred1', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred1 modified', '2016-11-30 02:27:18', '1');
INSERT INTO admin_log VALUES ('2272', '1', 'fred1', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred1 modified', '2016-11-30 02:27:34', '1');
INSERT INTO admin_log VALUES ('2273', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred modified', '2016-11-30 02:28:32', '1');
INSERT INTO admin_log VALUES ('2274', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred modified', '2016-11-30 02:33:17', '1');
INSERT INTO admin_log VALUES ('2275', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred modified', '2016-11-30 02:34:09', '1');
INSERT INTO admin_log VALUES ('2276', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred modified', '2016-11-30 02:34:35', '1');
INSERT INTO admin_log VALUES ('2277', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - fred modified', '2016-11-30 02:35:38', '1');
INSERT INTO admin_log VALUES ('2278', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Manufacturer - fred  reactivated', '2016-11-30 02:35:54', '1');
INSERT INTO admin_log VALUES ('2279', '1', 'fred', 'manufacturers', '1', 'UPDATE', 'Manufacturer - fred  deactivated', '2016-11-30 02:35:59', '1');
INSERT INTO admin_log VALUES ('2280', '1', 'Fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - Fred modified', '2016-11-30 02:47:36', '1');
INSERT INTO admin_log VALUES ('2281', '1', 'Fred1', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - Fred1 modified', '2016-11-30 02:48:16', '1');
INSERT INTO admin_log VALUES ('2282', '1', 'Fred', 'manufacturers', '1', 'UPDATE', 'Object Manufacturer - Fred modified', '2016-11-30 02:48:27', '1');
INSERT INTO admin_log VALUES ('2283', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-30 03:59:31', '0');
INSERT INTO admin_log VALUES ('2284', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-11-30 16:41:33', '0');
INSERT INTO admin_log VALUES ('2285', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-01 02:42:13', '0');
INSERT INTO admin_log VALUES ('2286', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-01 21:24:12', '0');
INSERT INTO admin_log VALUES ('2287', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-02 17:18:27', '0');
INSERT INTO admin_log VALUES ('2288', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-02 19:13:07', '0');
INSERT INTO admin_log VALUES ('2289', '1', 'Test 123', 'manufacturers', '2', 'INSERT', 'New Device manufacturer - Test 123 added', '2016-12-02 19:47:23', '1');
INSERT INTO admin_log VALUES ('2290', '1', 'Test 123a', 'manufacturers', '2', 'UPDATE', 'Device Manufacturer - Test 123a modified', '2016-12-02 19:47:38', '1');
INSERT INTO admin_log VALUES ('2291', '1', 'Test 123', 'manufacturers', '2', 'UPDATE', 'Device Manufacturer - Test 123 modified', '2016-12-02 19:47:48', '1');
INSERT INTO admin_log VALUES ('2292', '1', 'Test 123', 'manufacturers', '2', 'UPDATE', 'Device Manufacturer - Test 123 modified', '2016-12-02 19:48:00', '1');
INSERT INTO admin_log VALUES ('2293', '1', 'Test 123', 'manufacturers', '2', 'UPDATE', 'Manufacturer - Test 123  deactivated', '2016-12-02 19:48:07', '1');
INSERT INTO admin_log VALUES ('2294', '1', 'Test 123', 'manufacturers', '2', 'UPDATE', 'Manufacturer - Test 123  reactivated', '2016-12-02 19:48:11', '1');
INSERT INTO admin_log VALUES ('2295', '1', 'Test 123', 'manufacturers', '2', 'DELETE', 'Manufacturer - Test 123 deleted', '2016-12-02 19:49:45', '1');
INSERT INTO admin_log VALUES ('2296', '1', 'Testing 123', 'manufacturers', '3', 'INSERT', 'New Device manufacturer - Testing 123 added', '2016-12-02 19:50:24', '1');
INSERT INTO admin_log VALUES ('2297', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-02 20:43:22', '0');
INSERT INTO admin_log VALUES ('2298', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-03 05:49:41', '0');
INSERT INTO admin_log VALUES ('2299', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-05 15:02:56', '0');
INSERT INTO admin_log VALUES ('2300', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-05 15:37:09', '0');
INSERT INTO admin_log VALUES ('2301', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-05 19:46:09', '0');
INSERT INTO admin_log VALUES ('2302', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-05 21:19:27', '0');
INSERT INTO admin_log VALUES ('2303', '1', 'Tank Stick', 'devices', '1', 'UPDATE', 'Device - Tank Stick  deactivated', '2016-12-05 22:20:25', '1');
INSERT INTO admin_log VALUES ('2304', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-05 23:15:07', '0');
INSERT INTO admin_log VALUES ('2305', '1', 'Images', 'modules', '49', 'UPDATE', 'Module - Images modified', '2016-12-06 00:30:02', '1');
INSERT INTO admin_log VALUES ('2306', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-06 00:48:40', '0');
INSERT INTO admin_log VALUES ('2307', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-06 00:58:35', '0');
INSERT INTO admin_log VALUES ('2308', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-06 01:02:30', '0');
INSERT INTO admin_log VALUES ('2309', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-14 21:10:13', '0');
INSERT INTO admin_log VALUES ('2310', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-14 21:45:47', '0');
INSERT INTO admin_log VALUES ('2311', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-15 00:13:53', '0');
INSERT INTO admin_log VALUES ('2312', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-15 01:38:57', '0');
INSERT INTO admin_log VALUES ('2313', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-15 02:17:24', '0');
INSERT INTO admin_log VALUES ('2314', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-15 03:25:04', '0');
INSERT INTO admin_log VALUES ('2315', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-16 01:25:40', '0');
INSERT INTO admin_log VALUES ('2316', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-16 15:18:09', '0');
INSERT INTO admin_log VALUES ('2317', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-16 15:18:34', '0');
INSERT INTO admin_log VALUES ('2318', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-16 16:56:28', '0');
INSERT INTO admin_log VALUES ('2319', '1', '123', 'sites', '40', 'INSERT', 'New site - 123 added', '2016-12-16 16:57:13', '1');
INSERT INTO admin_log VALUES ('2320', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-16 17:30:34', '0');
INSERT INTO admin_log VALUES ('2321', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-16 19:04:50', '0');
INSERT INTO admin_log VALUES ('2322', '1', '456', 'sites', '41', 'INSERT', 'New site - 456 added', '2016-12-16 20:02:32', '1');
INSERT INTO admin_log VALUES ('2323', '1', '456', 'sites', '42', 'INSERT', 'New site - 456 added', '2016-12-16 20:02:50', '1');
INSERT INTO admin_log VALUES ('2324', '1', '456', 'sites', '42', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:03:23', '1');
INSERT INTO admin_log VALUES ('2325', '1', '456', 'sites', '41', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:03:33', '1');
INSERT INTO admin_log VALUES ('2326', '1', '456', 'sites', '43', 'INSERT', 'New site - 456 added', '2016-12-16 20:04:37', '1');
INSERT INTO admin_log VALUES ('2327', '1', '456', 'sites', '44', 'INSERT', 'New site - 456 added', '2016-12-16 20:05:19', '1');
INSERT INTO admin_log VALUES ('2328', '1', '456', 'sites', '44', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:05:37', '1');
INSERT INTO admin_log VALUES ('2329', '1', '456', 'sites', '43', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:05:46', '1');
INSERT INTO admin_log VALUES ('2330', '1', '123', 'sites', '35', 'DELETE', 'Site - 123 deleted', '2016-12-16 20:05:53', '1');
INSERT INTO admin_log VALUES ('2331', '1', '456', 'sites', '45', 'INSERT', 'New site - 456 added', '2016-12-16 20:22:33', '1');
INSERT INTO admin_log VALUES ('2332', '1', '456', 'sites', '46', 'INSERT', 'New site - 456 added', '2016-12-16 20:23:03', '1');
INSERT INTO admin_log VALUES ('2333', '1', '456', 'sites', '45', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:23:23', '1');
INSERT INTO admin_log VALUES ('2334', '1', '456', 'sites', '46', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:23:33', '1');
INSERT INTO admin_log VALUES ('2335', '1', '456', 'sites', '47', 'INSERT', 'New site - 456 added', '2016-12-16 20:23:40', '1');
INSERT INTO admin_log VALUES ('2336', '1', '456', 'sites', '47', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:24:10', '1');
INSERT INTO admin_log VALUES ('2337', '1', '456', 'sites', '48', 'INSERT', 'New site - 456 added', '2016-12-16 20:24:24', '1');
INSERT INTO admin_log VALUES ('2338', '1', '456', 'sites', '48', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:26:00', '1');
INSERT INTO admin_log VALUES ('2339', '1', '456', 'sites', '49', 'INSERT', 'New site - 456 added', '2016-12-16 20:26:12', '1');
INSERT INTO admin_log VALUES ('2340', '1', '456', 'sites', '49', 'DELETE', 'Site - 456 deleted', '2016-12-16 20:27:58', '1');
INSERT INTO admin_log VALUES ('2341', '1', 'aaa', 'sites', '50', 'INSERT', 'New site - aaa added', '2016-12-16 21:03:06', '1');
INSERT INTO admin_log VALUES ('2342', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-17 00:27:54', '0');
INSERT INTO admin_log VALUES ('2343', '1', 'Modules', 'modules', '25', 'UPDATE', 'Module - Modules modified', '2016-12-17 00:42:58', '1');
INSERT INTO admin_log VALUES ('2344', '1', 'Device Menu', 'modules', '1', 'UPDATE', 'Module - Device Menu modified', '2016-12-17 00:43:15', '1');
INSERT INTO admin_log VALUES ('2345', '1', 'Object Menu', 'modules', '24', 'UPDATE', 'Module - Object Menu modified', '2016-12-17 00:43:39', '1');
INSERT INTO admin_log VALUES ('2346', '1', 'Controllers', 'modules', '57', 'INSERT', 'New module - Controllers created', '2016-12-17 00:44:17', '1');
INSERT INTO admin_log VALUES ('2347', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2348', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2349', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2350', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2351', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2352', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2353', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2354', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2355', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Controllers', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2356', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Object Menu', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2357', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2358', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Images', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2359', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2360', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2361', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2362', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2363', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2364', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2365', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2366', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Actions', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2367', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2368', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2369', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2370', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2371', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2372', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2373', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2374', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-12-17 00:47:39', '1');
INSERT INTO admin_log VALUES ('2375', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-17 00:47:52', '0');
INSERT INTO admin_log VALUES ('2376', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-17 07:00:09', '0');
INSERT INTO admin_log VALUES ('2377', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-19 18:42:34', '0');
INSERT INTO admin_log VALUES ('2378', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-19 19:26:43', '0');
INSERT INTO admin_log VALUES ('2379', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-19 21:01:32', '0');
INSERT INTO admin_log VALUES ('2380', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-19 21:01:50', '0');
INSERT INTO admin_log VALUES ('2381', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-20 17:29:59', '0');
INSERT INTO admin_log VALUES ('2382', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-23 19:24:15', '0');
INSERT INTO admin_log VALUES ('2383', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-27 16:39:27', '0');
INSERT INTO admin_log VALUES ('2384', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-27 17:56:58', '0');
INSERT INTO admin_log VALUES ('2385', '1', 'Controllers', 'modules', '58', 'INSERT', 'New module - Controllers created', '2016-12-27 18:13:00', '1');
INSERT INTO admin_log VALUES ('2386', '1', 'Controller Menu', 'modules', '57', 'UPDATE', 'Module - Controller Menu modified', '2016-12-27 18:13:37', '1');
INSERT INTO admin_log VALUES ('2387', '1', 'Controllers', 'modules', '58', 'UPDATE', 'Module - Controllers modified', '2016-12-27 18:14:22', '1');
INSERT INTO admin_log VALUES ('2388', '1', 'Manufacturers', 'modules', '59', 'INSERT', 'New module - Manufacturers created', '2016-12-27 18:14:46', '1');
INSERT INTO admin_log VALUES ('2389', '1', 'Manufacturers', 'modules', '59', 'UPDATE', 'Module - Manufacturers modified', '2016-12-27 18:15:38', '1');
INSERT INTO admin_log VALUES ('2390', '1', 'Manufacturers', 'modules', '59', 'UPDATE', 'Module - Manufacturers modified', '2016-12-27 18:16:19', '1');
INSERT INTO admin_log VALUES ('2391', '1', 'Models', 'modules', '60', 'INSERT', 'New module - Models created', '2016-12-27 18:16:50', '1');
INSERT INTO admin_log VALUES ('2392', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2393', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Departments', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2394', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Groups', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2395', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Sites', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2396', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Users', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2397', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to System', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2398', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Accounts', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2399', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Admin Log', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2400', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Controller Menu', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2401', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Controllers', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2402', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2403', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2404', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Object Menu', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2405', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Objects', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2406', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Images', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2407', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2408', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2409', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Device Menu', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2410', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Devices', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2411', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Executions', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2412', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Measurements', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2413', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Triggers', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2414', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Actions', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2415', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tags', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2416', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Manufacturers', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2417', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Models', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2418', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Modules', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2419', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Dashboard', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2420', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Help', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2421', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Submit Ticket', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2422', '1', 'Super User', 'group_permissions', '1', 'INSERT', 'Super User granted access to Tickets', '2016-12-27 18:17:38', '1');
INSERT INTO admin_log VALUES ('2423', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-27 18:17:52', '0');
INSERT INTO admin_log VALUES ('2424', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-27 23:34:11', '0');
INSERT INTO admin_log VALUES ('2425', '1', '123', 'manufacturers', '4', 'INSERT', 'New Controller manufacturer - 123 added', '2016-12-27 23:36:35', '1');
INSERT INTO admin_log VALUES ('2426', '1', '123', 'manufacturers', '4', 'UPDATE', 'Manufacturer - 123  deactivated', '2016-12-27 23:36:43', '1');
INSERT INTO admin_log VALUES ('2427', '1', '123', 'manufacturers', '4', 'UPDATE', 'Manufacturer - 123  reactivated', '2016-12-27 23:36:52', '1');
INSERT INTO admin_log VALUES ('2428', '1', 'Fred\'s Widgets', 'manufacturers', '4', 'UPDATE', 'Controller Manufacturer - Fred\'s Widgets modified', '2016-12-27 23:37:48', '1');
INSERT INTO admin_log VALUES ('2429', '1', 'Controllers', 'modules', '58', 'UPDATE', 'Module - Controllers modified', '2016-12-27 23:46:11', '1');
INSERT INTO admin_log VALUES ('2430', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-28 16:07:58', '0');
INSERT INTO admin_log VALUES ('2431', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-28 16:44:54', '0');
INSERT INTO admin_log VALUES ('2432', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2016-12-30 20:25:06', '0');
INSERT INTO admin_log VALUES ('2433', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-01-04 19:51:36', '0');
INSERT INTO admin_log VALUES ('2434', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-01-05 16:51:54', '0');
INSERT INTO admin_log VALUES ('2435', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-01-05 17:24:47', '0');
INSERT INTO admin_log VALUES ('2436', '1', 'Sioux Tank Battery', 'sites', '51', 'INSERT', 'New site - Sioux Tank Battery added', '2017-01-05 17:42:21', '1');
INSERT INTO admin_log VALUES ('2437', '1', 'Sioux Tank Battery', 'sites', '52', 'INSERT', 'New site - Sioux Tank Battery added', '2017-01-05 17:42:40', '1');
INSERT INTO admin_log VALUES ('2438', '1', 'Sioux Tank Battery', 'sites', '53', 'INSERT', 'New site - Sioux Tank Battery added', '2017-01-05 17:42:57', '1');
INSERT INTO admin_log VALUES ('2439', '1', 'Sioux Tank Battery', 'sites', '52', 'DELETE', 'Site - Sioux Tank Battery deleted', '2017-01-05 17:43:20', '1');
INSERT INTO admin_log VALUES ('2440', '1', 'Sioux Tank Battery', 'sites', '53', 'DELETE', 'Site - Sioux Tank Battery deleted', '2017-01-05 17:43:25', '1');
INSERT INTO admin_log VALUES ('2441', '1', 'Triangle Reseach', 'manufacturers', '5', 'INSERT', 'New Controller manufacturer - Triangle Reseach added', '2017-01-05 17:59:35', '1');
INSERT INTO admin_log VALUES ('2442', '1', 'Triangle Reseach', 'manufacturers', '6', 'INSERT', 'New Controller manufacturer - Triangle Reseach added', '2017-01-05 18:01:01', '1');
INSERT INTO admin_log VALUES ('2443', '1', 'Triangle Reseach', 'manufacturers', '7', 'INSERT', 'New Controller manufacturer - Triangle Reseach added', '2017-01-05 18:01:52', '1');
INSERT INTO admin_log VALUES ('2444', '1', 'Triangle Reseach', 'manufacturers', '8', 'INSERT', 'New Controller manufacturer - Triangle Reseach added', '2017-01-05 18:01:56', '1');
INSERT INTO admin_log VALUES ('2445', '1', 'Triangle Reseach', 'manufacturers', '6', 'DELETE', 'Manufacturer - Triangle Reseach deleted', '2017-01-05 18:02:04', '1');
INSERT INTO admin_log VALUES ('2446', '1', 'Triangle Reseach', 'manufacturers', '7', 'DELETE', 'Manufacturer - Triangle Reseach deleted', '2017-01-05 18:02:08', '1');
INSERT INTO admin_log VALUES ('2447', '1', 'Triangle Reseach', 'manufacturers', '8', 'DELETE', 'Manufacturer - Triangle Reseach deleted', '2017-01-05 18:02:12', '1');
INSERT INTO admin_log VALUES ('2448', '1', 'Tank1', 'objects', '9', 'INSERT', 'New object - Tank1 added', '2017-01-05 18:20:23', '1');
INSERT INTO admin_log VALUES ('2449', '1', 'Tank1', 'objects', '10', 'INSERT', 'New object - Tank1 added', '2017-01-05 18:20:27', '1');
INSERT INTO admin_log VALUES ('2450', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-01-05 19:30:34', '0');
INSERT INTO admin_log VALUES ('2451', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-01-05 21:53:15', '0');
INSERT INTO admin_log VALUES ('2452', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-01-28 19:21:43', '0');
INSERT INTO admin_log VALUES ('2453', '1', 'UpperLevel', 'measurements', '8', 'UPDATE', 'Measurement - UpperLevel modified', '2017-01-28 19:34:57', '1');
INSERT INTO admin_log VALUES ('2454', '1', 'BottomLevel', 'measurements', '9', 'UPDATE', 'Measurement - BottomLevel modified', '2017-01-28 19:35:40', '1');
INSERT INTO admin_log VALUES ('2455', '1', 'InterfaceLevel', 'measurements', '11', 'UPDATE', 'Measurement - InterfaceLevel modified', '2017-01-28 19:36:15', '1');
INSERT INTO admin_log VALUES ('2456', '1', 'TotalLevel', 'measurements', '18', 'UPDATE', 'Measurement - TotalLevel modified', '2017-01-28 19:36:42', '1');
INSERT INTO admin_log VALUES ('2457', '1', 'OilVolume', 'measurements', '19', 'UPDATE', 'Measurement - OilVolume modified', '2017-01-28 19:37:28', '1');
INSERT INTO admin_log VALUES ('2458', '1', 'WaterVolume', 'measurements', '22', 'UPDATE', 'Measurement - WaterVolume modified', '2017-01-28 19:37:55', '1');
INSERT INTO admin_log VALUES ('2459', '1', 'Temperature', 'measurements', '23', 'UPDATE', 'Measurement - Temperature modified', '2017-01-28 19:38:42', '1');
INSERT INTO admin_log VALUES ('2460', '1', 'Pressure', 'measurements', '24', 'UPDATE', 'Measurement - Pressure modified', '2017-01-28 19:39:32', '1');
INSERT INTO admin_log VALUES ('2461', '1', 'OverfillAcknowledge', 'executions', '7', 'UPDATE', 'Execution - OverfillAcknowledge modified', '2017-01-28 19:55:44', '1');
INSERT INTO admin_log VALUES ('2462', '1', 'LowLevelAcknowledge', 'executions', '8', 'UPDATE', 'Execution - LowLevelAcknowledge modified', '2017-01-28 19:56:08', '1');
INSERT INTO admin_log VALUES ('2463', '1', 'HighLevel', 'triggers', '7', 'UPDATE', 'Trigger - HighLevel modified', '2017-01-28 19:56:44', '1');
INSERT INTO admin_log VALUES ('2464', '1', 'HighBand', 'triggers', '4', 'UPDATE', 'Trigger - HighBand modified', '2017-01-28 19:57:08', '1');
INSERT INTO admin_log VALUES ('2465', '1', 'LowBand', 'triggers', '6', 'UPDATE', 'Trigger - LowBand modified', '2017-01-28 19:57:21', '1');
INSERT INTO admin_log VALUES ('2466', '1', 'LowLevel', 'triggers', '1', 'UPDATE', 'Trigger - LowLevel modified', '2017-01-28 19:57:40', '1');
INSERT INTO admin_log VALUES ('2467', '1', 'LowBand', 'triggers', '6', 'UPDATE', 'Trigger - LowBand modified', '2017-01-28 19:57:49', '1');
INSERT INTO admin_log VALUES ('2468', '1', 'HighLevel', 'triggers', '7', 'UPDATE', 'Trigger - HighLevel modified', '2017-01-28 19:57:57', '1');
INSERT INTO admin_log VALUES ('2469', '1', 'OverfillShutdown', 'actions', '1', 'UPDATE', 'Action - OverfillShutdown modified', '2017-01-28 19:58:41', '1');
INSERT INTO admin_log VALUES ('2470', '1', 'HighStop', 'actions', '2', 'UPDATE', 'Action - HighStop modified', '2017-01-28 19:59:01', '1');
INSERT INTO admin_log VALUES ('2471', '1', 'LowStart', 'actions', '3', 'UPDATE', 'Action - LowStart modified', '2017-01-28 19:59:12', '1');
INSERT INTO admin_log VALUES ('2472', '1', 'LowLevelShutdown', 'actions', '4', 'UPDATE', 'Action - LowLevelShutdown modified', '2017-01-28 19:59:25', '1');
INSERT INTO admin_log VALUES ('2473', '1', 'Now is the Time', 'actions', '7', 'DELETE', 'Action - Now is the Time deleted', '2017-01-28 19:59:38', '1');
INSERT INTO admin_log VALUES ('2474', '1', 'Just Because', 'actions', '6', 'DELETE', 'Action - Just Because deleted', '2017-01-28 19:59:48', '1');
INSERT INTO admin_log VALUES ('2475', '1', 'Reboot OPCUA Server', 'actions', '5', 'DELETE', 'Action - Reboot OPCUA Server deleted', '2017-01-28 19:59:54', '1');
INSERT INTO admin_log VALUES ('2476', '1', 'Reboot OPCUA Server', 'executions', '2', 'DELETE', 'Execution - Reboot OPCUA Server deleted', '2017-01-28 20:00:10', '1');
INSERT INTO admin_log VALUES ('2477', '1', 'Start Motor', 'executions', '3', 'DELETE', 'Execution - Start Motor deleted', '2017-01-28 20:00:15', '1');
INSERT INTO admin_log VALUES ('2478', '1', 'Stop Motor', 'executions', '1', 'DELETE', 'Execution - Stop Motor deleted', '2017-01-28 20:00:20', '1');
INSERT INTO admin_log VALUES ('2479', '1', 'Test', 'executions', '6', 'DELETE', 'Execution - Test deleted', '2017-01-28 20:00:25', '1');
INSERT INTO admin_log VALUES ('2480', '1', 'UpperLevel', 'tags', '14', 'UPDATE', 'Tag - UpperLevel modified', '2017-01-28 20:02:11', '1');
INSERT INTO admin_log VALUES ('2481', '1', 'BottomLevel', 'tags', '15', 'UPDATE', 'Tag - BottomLevel modified', '2017-01-28 20:02:37', '1');
INSERT INTO admin_log VALUES ('2482', '1', 'UpperLevel', 'tags', '14', 'UPDATE', 'Tag - UpperLevel modified', '2017-01-28 20:02:55', '1');
INSERT INTO admin_log VALUES ('2483', '1', 'OverfillAcknowledge', 'tags', '1', 'UPDATE', 'Tag - OverfillAcknowledge modified', '2017-01-28 20:04:01', '1');
INSERT INTO admin_log VALUES ('2484', '1', 'OverfillAcknowledge', 'executions', '7', 'UPDATE', 'Execution - OverfillAcknowledge modified', '2017-01-28 20:04:27', '1');
INSERT INTO admin_log VALUES ('2485', '1', 'Boolean', 'tags', '1', 'UPDATE', 'Tag - Boolean modified', '2017-01-28 20:04:52', '1');
INSERT INTO admin_log VALUES ('2486', '1', 'OverfillAcknowledge', 'executions', '7', 'UPDATE', 'Execution - OverfillAcknowledge modified', '2017-01-28 20:05:05', '1');
INSERT INTO admin_log VALUES ('2487', '1', 'Boolean', 'tags', '16', 'UPDATE', 'Tag - Boolean modified', '2017-01-28 20:05:34', '1');
INSERT INTO admin_log VALUES ('2488', '1', 'Unsigned Integer', 'tags', '8', 'UPDATE', 'Tag - Unsigned Integer modified', '2017-01-28 20:06:12', '1');
INSERT INTO admin_log VALUES ('2489', '1', 'Signed Integer', 'tags', '11', 'UPDATE', 'Tag - Signed Integer modified', '2017-01-28 20:06:49', '1');
INSERT INTO admin_log VALUES ('2490', '1', 'Unsigned Floating Point', 'tags', '3', 'UPDATE', 'Tag - Unsigned Floating Point modified', '2017-01-28 20:07:23', '1');
INSERT INTO admin_log VALUES ('2491', '1', 'Boolean', 'tags', '9', 'UPDATE', 'Tag - Boolean modified', '2017-01-28 20:07:56', '1');
INSERT INTO admin_log VALUES ('2492', '1', 'Signed Integer', 'tags', '15', 'UPDATE', 'Tag - Signed Integer modified', '2017-01-28 20:08:20', '1');
INSERT INTO admin_log VALUES ('2493', '1', 'Unsigned Integer', 'tags', '12', 'UPDATE', 'Tag - Unsigned Integer modified', '2017-01-28 20:08:48', '1');
INSERT INTO admin_log VALUES ('2494', '1', 'Unsigned Floating Point', 'tags', '14', 'UPDATE', 'Tag - Unsigned Floating Point modified', '2017-01-28 20:09:25', '1');
INSERT INTO admin_log VALUES ('2495', '1', 'Signed Floating Point', 'tags', '5', 'UPDATE', 'Tag - Signed Floating Point modified', '2017-01-28 20:10:59', '1');
INSERT INTO admin_log VALUES ('2496', '1', 'Signed Floating Point', 'tags', '10', 'UPDATE', 'Tag - Signed Floating Point modified', '2017-01-28 20:11:26', '1');
INSERT INTO admin_log VALUES ('2497', '1', 'Signed Floating Point', 'tags', '13', 'UPDATE', 'Tag - Signed Floating Point modified', '2017-01-28 20:12:15', '1');
INSERT INTO admin_log VALUES ('2498', '1', 'Signed Integer', 'tags', '17', 'INSERT', 'New tag - Signed Integer added', '2017-01-28 20:12:57', '1');
INSERT INTO admin_log VALUES ('2499', '1', 'Unsigned Integer', 'tags', '18', 'INSERT', 'New tag - Unsigned Integer added', '2017-01-28 20:13:57', '1');
INSERT INTO admin_log VALUES ('2500', '1', 'Unsigned Floating Point', 'tags', '19', 'INSERT', 'New tag - Unsigned Floating Point added', '2017-01-28 20:15:02', '1');
INSERT INTO admin_log VALUES ('2501', '1', 'Boolean', 'tags', '20', 'INSERT', 'New tag - Boolean added', '2017-01-28 20:16:00', '1');
INSERT INTO admin_log VALUES ('2502', '1', 'Signed Integer', 'tags', '21', 'INSERT', 'New tag - Signed Integer added', '2017-01-28 20:16:15', '1');
INSERT INTO admin_log VALUES ('2503', '1', 'Unsigned Integer', 'tags', '22', 'INSERT', 'New tag - Unsigned Integer added', '2017-01-28 20:16:34', '1');
INSERT INTO admin_log VALUES ('2504', '1', 'Unsigned Integer', 'tags', '23', 'INSERT', 'New tag - Unsigned Integer added', '2017-01-28 20:16:35', '1');
INSERT INTO admin_log VALUES ('2505', '1', 'Signed Integer', 'tags', '21', 'UPDATE', 'Tag - Signed Integer modified', '2017-01-28 20:16:41', '1');
INSERT INTO admin_log VALUES ('2506', '1', 'Signed Floating Point', 'tags', '24', 'INSERT', 'New tag - Signed Floating Point added', '2017-01-28 20:17:01', '1');
INSERT INTO admin_log VALUES ('2507', '1', 'Unsigned Floating Point', 'tags', '25', 'INSERT', 'New tag - Unsigned Floating Point added', '2017-01-28 20:17:26', '1');
INSERT INTO admin_log VALUES ('2508', '1', 'UpperLevel', 'measurements', '8', 'UPDATE', 'Measurement - UpperLevel modified', '2017-01-28 20:17:43', '1');
INSERT INTO admin_log VALUES ('2509', '1', 'LowLevelAcknowledge', 'executions', '8', 'UPDATE', 'Execution - LowLevelAcknowledge modified', '2017-01-28 20:18:19', '1');
INSERT INTO admin_log VALUES ('2510', '1', 'OverfillAcknowledge', 'executions', '7', 'UPDATE', 'Execution - OverfillAcknowledge modified', '2017-01-28 20:18:25', '1');
INSERT INTO admin_log VALUES ('2511', '1', 'HighBand', 'triggers', '4', 'UPDATE', 'Trigger - HighBand modified', '2017-01-28 20:18:47', '1');
INSERT INTO admin_log VALUES ('2512', '1', 'Unsigned Integer', 'tags', '23', 'DELETE', 'Tag - Unsigned Integer deleted', '2017-01-28 20:19:10', '1');
INSERT INTO admin_log VALUES ('2513', '1', 'OverfillAcknowledge', 'executions', '7', 'UPDATE', 'Execution - OverfillAcknowledge modified', '2017-01-28 20:19:41', '1');
INSERT INTO admin_log VALUES ('2514', '1', 'LowLevelAcknowledge', 'executions', '8', 'UPDATE', 'Execution - LowLevelAcknowledge modified', '2017-01-28 20:19:48', '1');
INSERT INTO admin_log VALUES ('2515', '1', 'LowLevelAcknowledge', 'executions', '8', 'UPDATE', 'Execution - LowLevelAcknowledge modified', '2017-01-28 20:19:58', '1');
INSERT INTO admin_log VALUES ('2516', '1', 'HighBand', 'triggers', '4', 'UPDATE', 'Trigger - HighBand modified', '2017-01-28 20:20:16', '1');
INSERT INTO admin_log VALUES ('2517', '1', 'HighLevel', 'triggers', '7', 'UPDATE', 'Trigger - HighLevel modified', '2017-01-28 20:20:23', '1');
INSERT INTO admin_log VALUES ('2518', '1', 'LowBand', 'triggers', '6', 'UPDATE', 'Trigger - LowBand modified', '2017-01-28 20:20:29', '1');
INSERT INTO admin_log VALUES ('2519', '1', 'LowLevel', 'triggers', '1', 'UPDATE', 'Trigger - LowLevel modified', '2017-01-28 20:21:21', '1');
INSERT INTO admin_log VALUES ('2520', '1', 'HighBand', 'triggers', '4', 'UPDATE', 'Trigger - HighBand modified', '2017-01-28 20:21:28', '1');
INSERT INTO admin_log VALUES ('2521', '1', 'HighBand', 'triggers', '4', 'UPDATE', 'Trigger - HighBand modified', '2017-01-28 20:25:49', '1');
INSERT INTO admin_log VALUES ('2522', '1', 'HighBand', 'triggers', '4', 'UPDATE', 'Trigger - HighBand modified', '2017-01-28 20:25:56', '1');
INSERT INTO admin_log VALUES ('2523', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-01 00:22:03', '0');
INSERT INTO admin_log VALUES ('2524', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-01 00:33:03', '0');
INSERT INTO admin_log VALUES ('2525', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-01 00:33:50', '0');
INSERT INTO admin_log VALUES ('2526', '1', 'Triangle Research Inc', 'manufacturers', '4', 'UPDATE', 'Controller Manufacturer - Triangle Research Inc modified', '2017-02-01 00:40:50', '1');
INSERT INTO admin_log VALUES ('2527', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-01 21:58:36', '0');
INSERT INTO admin_log VALUES ('2528', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-03 16:20:08', '0');
INSERT INTO admin_log VALUES ('2529', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-03 16:21:43', '0');
INSERT INTO admin_log VALUES ('2530', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-03 17:54:14', '0');
INSERT INTO admin_log VALUES ('2531', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-03 19:20:15', '0');
INSERT INTO admin_log VALUES ('2532', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-03 19:33:21', '0');
INSERT INTO admin_log VALUES ('2533', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-03 20:07:51', '0');
INSERT INTO admin_log VALUES ('2534', '1', 'aaa', 'sites', '50', 'UPDATE', 'Site - aaa modified', '2017-02-03 20:44:14', '1');
INSERT INTO admin_log VALUES ('2535', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-09 01:41:51', '0');
INSERT INTO admin_log VALUES ('2536', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-09 02:11:27', '0');
INSERT INTO admin_log VALUES ('2537', '1', 'Sioux Tank Battery', 'sites', '51', 'UPDATE', 'Site - Sioux Tank Battery modified', '2017-02-09 02:34:38', '1');
INSERT INTO admin_log VALUES ('2538', '1', 'Oil Tank', 'objects', '2', 'DELETE', 'Object - Oil Tank deleted', '2017-02-09 02:36:46', '1');
INSERT INTO admin_log VALUES ('2539', '1', 'Water Tank', 'objects', '3', 'DELETE', 'Object - Water Tank deleted', '2017-02-09 02:36:51', '1');
INSERT INTO admin_log VALUES ('2540', '1', 'Test\'s', 'objects', '4', 'DELETE', 'Object - Test\'s deleted', '2017-02-09 02:36:56', '1');
INSERT INTO admin_log VALUES ('2541', '1', 'Pump Jack', 'objects', '5', 'DELETE', 'Object - Pump Jack deleted', '2017-02-09 02:37:03', '1');
INSERT INTO admin_log VALUES ('2542', '1', '1234', 'objects', '6', 'DELETE', 'Object - 1234 deleted', '2017-02-09 02:37:31', '1');
INSERT INTO admin_log VALUES ('2543', '1', 'sdtwgbw', 'objects', '8', 'DELETE', 'Object - sdtwgbw deleted', '2017-02-09 02:37:35', '1');
INSERT INTO admin_log VALUES ('2544', '1', 'Triangle Research Inc', 'manufacturers', '6', 'INSERT', 'New Object manufacturer - Triangle Research Inc added', '2017-02-09 02:38:12', '1');
INSERT INTO admin_log VALUES ('2545', '1', 'IDEC', 'manufacturers', '7', 'INSERT', 'New Object manufacturer - IDEC added', '2017-02-09 02:38:39', '1');
INSERT INTO admin_log VALUES ('2546', '1', 'Turck', 'manufacturers', '8', 'INSERT', 'New Object manufacturer - Turck added', '2017-02-09 02:38:48', '1');
INSERT INTO admin_log VALUES ('2547', '1', 'TankStick1', 'devices', '11', 'UPDATE', 'Device - TankStick1 modified', '2017-02-09 02:44:41', '1');
INSERT INTO admin_log VALUES ('2548', '1', 'Tank1', 'objects', '9', 'UPDATE', 'Object - Tank1 modified', '2017-02-09 02:48:25', '1');
INSERT INTO admin_log VALUES ('2549', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-10 16:12:09', '0');
INSERT INTO admin_log VALUES ('2550', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-10 16:43:27', '0');
INSERT INTO admin_log VALUES ('2551', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-10 19:43:19', '0');
INSERT INTO admin_log VALUES ('2552', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-10 20:17:21', '0');
INSERT INTO admin_log VALUES ('2553', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-10 21:01:09', '0');
INSERT INTO admin_log VALUES ('2554', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-10 21:31:23', '0');
INSERT INTO admin_log VALUES ('2555', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-13 17:53:06', '0');
INSERT INTO admin_log VALUES ('2556', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-13 19:53:03', '0');
INSERT INTO admin_log VALUES ('2557', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-16 16:09:00', '0');
INSERT INTO admin_log VALUES ('2558', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-02-16 20:59:54', '0');
INSERT INTO admin_log VALUES ('2559', '1', 'Quest Automated', 'users', '1', 'LOGIN', 'admin logged in', '2017-03-01 17:54:52', '0');

-- ----------------------------
-- Table structure for `controllers`
-- ----------------------------
DROP TABLE IF EXISTS `controllers`;
CREATE TABLE `controllers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of controllers
-- ----------------------------

-- ----------------------------
-- Table structure for `controller_objects`
-- ----------------------------
DROP TABLE IF EXISTS `controller_objects`;
CREATE TABLE `controller_objects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller_id` int(10) unsigned NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `sms_num` varchar(15) DEFAULT NULL,
  `last_report` datetime DEFAULT NULL,
  `last_restart` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_site_objects_1_idx` (`controller_id`),
  KEY `fk_site_objects_2_idx` (`object_id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of controller_objects
-- ----------------------------
INSERT INTO controller_objects VALUES ('56', '6', '16', 'Test Transfer Pump', null, null, null, null, null, null, '1', '2017-05-11 20:20:49', null);
INSERT INTO controller_objects VALUES ('57', '7', '13', 'West Tank', null, null, null, null, null, null, '1', '2018-02-14 21:14:29', null);
INSERT INTO controller_objects VALUES ('58', '7', '13', 'East Tank', null, null, null, null, null, null, '1', '2018-02-14 21:14:46', null);

-- ----------------------------
-- Table structure for `data_types`
-- ----------------------------
DROP TABLE IF EXISTS `data_types`;
CREATE TABLE `data_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of data_types
-- ----------------------------
INSERT INTO data_types VALUES ('2', 'BOOL');
INSERT INTO data_types VALUES ('3', 'SINT');
INSERT INTO data_types VALUES ('4', 'UINT');
INSERT INTO data_types VALUES ('6', 'SFLP');

-- ----------------------------
-- Table structure for `departments`
-- ----------------------------
DROP TABLE IF EXISTS `departments`;
CREATE TABLE `departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '		',
  `account_id` int(11) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `manager_id` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_departments_1_idx` (`account_id`),
  KEY `fk_departments_2_idx` (`created_by`),
  KEY `modified` (`modified_by`),
  CONSTRAINT `fk_departments_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of departments
-- ----------------------------
INSERT INTO departments VALUES ('1', '1', 'Administration', '2', '1', '2016-09-05 06:56:46', '1', '2016-11-20 21:26:32', '1');
INSERT INTO departments VALUES ('4', '1', 'Pumpers', '10', '1', '2016-09-05 17:48:55', '1', '2016-10-20 05:31:08', '1');
INSERT INTO departments VALUES ('6', '1', 'Maintenance', '2', '1', '2016-09-09 15:49:57', '2', '2016-11-09 04:51:27', '1');
INSERT INTO departments VALUES ('7', '1', 'Sales', '2', '1', '2016-09-11 23:11:03', '1', '2016-11-15 02:15:24', '1');
INSERT INTO departments VALUES ('8', '5', 'Administration', '36', '1', '2016-11-18 11:49:23', '1', '2016-11-18 17:50:11', null);
INSERT INTO departments VALUES ('10', '5', 'Office', '0', '1', '2016-11-18 12:00:56', '1', null, null);

-- ----------------------------
-- Table structure for `devices`
-- ----------------------------
DROP TABLE IF EXISTS `devices`;
CREATE TABLE `devices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `man_name` varchar(45) DEFAULT NULL,
  `mod_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `deviceID` int(11) DEFAULT NULL,
  `manufacturers_id` int(11) NOT NULL,
  `device_type_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of devices
-- ----------------------------
INSERT INTO devices VALUES ('32', 'Flow Meter QRT', null, null, null, null, '1', '2017-02-14 14:11:44', null, '2018-02-13 21:59:38', null, '111', '16', '1');
INSERT INTO devices VALUES ('33', 'Flow Meter TTT', null, null, null, null, '1', '2017-02-14 15:04:54', null, '2018-02-12 21:01:51', null, '113', '16', '1');
INSERT INTO devices VALUES ('34', 'Tranducer', null, null, null, null, '1', '2017-02-21 15:05:50', null, '2018-03-06 16:51:27', null, '143', '15', '2');
INSERT INTO devices VALUES ('37', 'Tank Stick 1', null, null, null, null, '1', '2017-05-02 14:30:40', null, '2018-03-14 14:56:16', null, '11', '16', '3');
INSERT INTO devices VALUES ('38', 'test', null, null, null, null, '1', '2018-02-12 20:56:39', null, null, null, '12324321', '15', '3');
INSERT INTO devices VALUES ('40', 'linearTest', null, null, null, null, '1', '2018-02-14 18:43:24', null, null, null, '12', '15', '4');
INSERT INTO devices VALUES ('41', 'Tank Stick 2', null, null, null, null, '1', '2018-03-14 14:53:38', null, null, null, '12', '15', '3');

-- ----------------------------
-- Table structure for `devices_links`
-- ----------------------------
DROP TABLE IF EXISTS `devices_links`;
CREATE TABLE `devices_links` (
  `linkID` int(11) NOT NULL AUTO_INCREMENT,
  `link_to` int(11) DEFAULT NULL,
  `link_to_type` int(11) DEFAULT NULL,
  `link_to_column` int(11) DEFAULT NULL,
  `link_to_row` int(11) DEFAULT NULL,
  `link_to_controller` int(11) DEFAULT NULL,
  `link_to_object` int(11) DEFAULT NULL,
  `link_from` int(11) DEFAULT NULL,
  `link_from_type` int(11) DEFAULT NULL,
  `link_from_column` int(11) DEFAULT NULL,
  `link_from_row` int(11) DEFAULT NULL,
  `link_from_controller` int(11) DEFAULT NULL,
  `link_from_object` int(11) DEFAULT NULL,
  `siteID` int(11) DEFAULT NULL,
  PRIMARY KEY (`linkID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of devices_links
-- ----------------------------
INSERT INTO devices_links VALUES ('14', '449', '1', '1', '2', '6', '56', '449', '4', '1', '1', '6', '56', '29');
INSERT INTO devices_links VALUES ('15', '449', '3', '1', '1', '6', '56', '449', '1', '1', '2', '6', '56', '29');
INSERT INTO devices_links VALUES ('16', '450', '3', '3', '1', '7', '57', '450', '1', '1', '2', '7', '57', '56');

-- ----------------------------
-- Table structure for `device_executions`
-- ----------------------------
DROP TABLE IF EXISTS `device_executions`;
CREATE TABLE `device_executions` (
  `configID` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `regnum` int(10) unsigned NOT NULL,
  `cfn` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `parameter_units` varchar(25) DEFAULT NULL,
  `parameter_address` varchar(50) DEFAULT NULL,
  `operator_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of device_executions
-- ----------------------------
INSERT INTO device_executions VALUES ('1', '31', '1', '1', '8', null, null, '0', '0', '2017-02-24 22:27:49', null);
INSERT INTO device_executions VALUES ('2', '31', '1', '2', '8', null, null, '0', '0', '2017-02-24 22:28:22', null);
INSERT INTO device_executions VALUES ('3', '31', '1', '1', '7', null, null, '0', '0', '2017-02-24 22:31:14', null);
INSERT INTO device_executions VALUES ('5', '37', '1', '2', '8', 'units', '1.1111', '4', '2', '2018-02-20 18:49:41', null);

-- ----------------------------
-- Table structure for `device_functions`
-- ----------------------------
DROP TABLE IF EXISTS `device_functions`;
CREATE TABLE `device_functions` (
  `configID` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `regnum` int(10) unsigned NOT NULL,
  `cfn` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `parameter_units` varchar(25) DEFAULT NULL,
  `parameter_address` varchar(50) DEFAULT NULL,
  `operator_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of device_functions
-- ----------------------------

-- ----------------------------
-- Table structure for `device_measurements`
-- ----------------------------
DROP TABLE IF EXISTS `device_measurements`;
CREATE TABLE `device_measurements` (
  `configID` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `regnum` int(10) unsigned NOT NULL,
  `cfn` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `parameter_units` varchar(25) DEFAULT NULL,
  `parameter_address` varchar(50) DEFAULT NULL,
  `operator_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of device_measurements
-- ----------------------------
INSERT INTO device_measurements VALUES ('21', '37', '1', '1', '8', 'Feet', '', '0', '6', '2018-02-20 21:55:43', null);
INSERT INTO device_measurements VALUES ('23', '37', '2', '1', '11', 'Feet', '', '0', '6', '2018-03-14 14:37:46', null);

-- ----------------------------
-- Table structure for `device_notifications`
-- ----------------------------
DROP TABLE IF EXISTS `device_notifications`;
CREATE TABLE `device_notifications` (
  `configID` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `regnum` int(10) unsigned NOT NULL,
  `cfn` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `parameter_units` varchar(25) DEFAULT NULL,
  `parameter_address` varchar(50) DEFAULT NULL,
  `operator_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of device_notifications
-- ----------------------------
INSERT INTO device_notifications VALUES ('1', '31', '1', '1', '5', null, null, '0', '0', '2017-02-24 22:28:04', null);
INSERT INTO device_notifications VALUES ('2', '31', '2', '1', '6', null, null, '0', '0', '2017-02-24 22:28:09', null);

-- ----------------------------
-- Table structure for `device_triggers`
-- ----------------------------
DROP TABLE IF EXISTS `device_triggers`;
CREATE TABLE `device_triggers` (
  `configID` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(10) unsigned NOT NULL,
  `regnum` int(10) unsigned NOT NULL,
  `cfn` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  `parameter_units` varchar(25) DEFAULT NULL,
  `parameter_address` varchar(50) DEFAULT NULL,
  `operator_id` int(11) DEFAULT NULL,
  `data_type_id` int(11) DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of device_triggers
-- ----------------------------
INSERT INTO device_triggers VALUES ('1', '37', '1', '1', '6', '2', '', '6', '3', '2018-02-20 22:01:13', null);
INSERT INTO device_triggers VALUES ('2', '37', '1', '3', '7', '2', '', '0', '4', '2018-02-20 22:01:35', null);

-- ----------------------------
-- Table structure for `device_types`
-- ----------------------------
DROP TABLE IF EXISTS `device_types`;
CREATE TABLE `device_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of device_types
-- ----------------------------
INSERT INTO device_types VALUES ('1', 'Flow Meter');
INSERT INTO device_types VALUES ('2', 'Transducer');
INSERT INTO device_types VALUES ('3', 'Tank Stick');
INSERT INTO device_types VALUES ('4', 'Linear Actuator');

-- ----------------------------
-- Table structure for `executions`
-- ----------------------------
DROP TABLE IF EXISTS `executions`;
CREATE TABLE `executions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of executions
-- ----------------------------
INSERT INTO executions VALUES ('7', '3', 'OverfillAcknowledge', 'overfillacknowledge', '', '1', '2016-11-19 08:38:10', '1', '2017-02-24 18:14:19', '1');
INSERT INTO executions VALUES ('8', '3', 'LowLevelAcknowledge', 'lowlevelacknowledg', '', '1', '2016-11-19 17:42:29', '1', '2018-02-12 17:39:47', '1');

-- ----------------------------
-- Table structure for `execution_tags`
-- ----------------------------
DROP TABLE IF EXISTS `execution_tags`;
CREATE TABLE `execution_tags` (
  `execution_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`execution_id`,`tag_id`),
  KEY `fk_execution_tags_2_idx` (`tag_id`),
  CONSTRAINT `fk_execution_tags_1` FOREIGN KEY (`execution_id`) REFERENCES `executions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_execution_tags_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of execution_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `functions`
-- ----------------------------
DROP TABLE IF EXISTS `functions`;
CREATE TABLE `functions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` int(10) NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `type` varchar(45) NOT NULL,
  `message` varchar(45) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(45) DEFAULT NULL,
  `name` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of functions
-- ----------------------------
INSERT INTO functions VALUES ('1', '3', '0', '', '', '2018-02-12 17:49:15', null, 'test');

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_groups_1_idx` (`account_id`),
  KEY `fk_groups_2_idx` (`created_by`),
  KEY `modified` (`modified_by`),
  CONSTRAINT `fk_groups_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO groups VALUES ('1', '1', 'Super User', null, '1', '2016-09-04 01:12:02', '1', '2016-11-20 21:39:21', '1');
INSERT INTO groups VALUES ('2', '1', 'Account Admin', '123\'s', '1', '2016-09-04 02:32:43', '1', '2016-10-04 04:17:50', '1');
INSERT INTO groups VALUES ('3', '1', 'Pumper', null, '1', '2016-09-06 01:25:00', '1', '2016-10-04 04:17:50', '1');
INSERT INTO groups VALUES ('4', '1', 'Manager', null, '1', '2016-09-06 02:12:15', '1', '2016-10-04 04:17:50', '1');
INSERT INTO groups VALUES ('6', '1', 'Technicians', '', '1', '2016-09-06 02:31:47', '1', '2016-11-18 20:20:34', '1');
INSERT INTO groups VALUES ('7', '1', 'Maintenance', null, '1', '2016-09-06 02:32:48', '1', '2016-10-04 04:17:50', null);
INSERT INTO groups VALUES ('9', '1', 'Test', 'descript\'s', '1', '2016-09-09 10:00:17', '1', '2016-10-04 04:17:50', '1');
INSERT INTO groups VALUES ('10', '5', 'Account Admins', '', '1', '2016-11-18 11:47:07', '1', null, null);
INSERT INTO groups VALUES ('11', '5', 'IT', '', '1', '2016-11-18 11:47:52', '1', null, null);
INSERT INTO groups VALUES ('12', '5', 'Pumpers', '', '1', '2016-11-18 11:48:05', '1', null, null);

-- ----------------------------
-- Table structure for `group_permissions`
-- ----------------------------
DROP TABLE IF EXISTS `group_permissions`;
CREATE TABLE `group_permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `module_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group_id`),
  KEY `permission` (`module_id`),
  KEY `fk_group_permissions_1_idx` (`created_by`),
  CONSTRAINT `fk_group_permissions_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_group_permissions_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=399 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of group_permissions
-- ----------------------------
INSERT INTO group_permissions VALUES ('252', '10', '20', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('253', '10', '27', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('254', '10', '28', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('255', '10', '29', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('256', '10', '30', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('257', '10', '39', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('258', '10', '31', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('259', '10', '33', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('260', '10', '32', '2016-11-18 19:23:05', '1');
INSERT INTO group_permissions VALUES ('276', '6', '20', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('277', '6', '29', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('278', '6', '21', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('279', '6', '24', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('280', '6', '1', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('281', '6', '43', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('282', '6', '44', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('283', '6', '45', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('284', '6', '47', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('285', '6', '46', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('286', '6', '39', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('287', '6', '31', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('288', '6', '33', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('289', '6', '32', '2016-11-18 20:23:15', '1');
INSERT INTO group_permissions VALUES ('368', '1', '20', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('369', '1', '27', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('370', '1', '28', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('371', '1', '29', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('372', '1', '30', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('373', '1', '21', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('374', '1', '22', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('375', '1', '23', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('376', '1', '57', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('377', '1', '58', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('378', '1', '59', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('379', '1', '60', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('380', '1', '24', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('381', '1', '48', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('382', '1', '49', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('383', '1', '50', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('384', '1', '51', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('385', '1', '1', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('386', '1', '42', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('387', '1', '43', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('388', '1', '44', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('389', '1', '45', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('390', '1', '47', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('391', '1', '46', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('392', '1', '54', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('393', '1', '56', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('394', '1', '25', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('395', '1', '39', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('396', '1', '31', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('397', '1', '33', '2016-12-27 18:17:38', '1');
INSERT INTO group_permissions VALUES ('398', '1', '32', '2016-12-27 18:17:38', '1');

-- ----------------------------
-- Table structure for `manufacturers`
-- ----------------------------
DROP TABLE IF EXISTS `manufacturers`;
CREATE TABLE `manufacturers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `primary_contact` varchar(100) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of manufacturers
-- ----------------------------
INSERT INTO manufacturers VALUES ('15', 'Acme', null, 'http://www.yahoo.com', null, '1', '2017-02-14 12:01:03', null, '2018-02-12 19:57:58', null);
INSERT INTO manufacturers VALUES ('16', 'Trident', null, 'http://www.google.com', null, '1', '2017-02-14 12:48:45', null, null, null);

-- ----------------------------
-- Table structure for `measurements`
-- ----------------------------
DROP TABLE IF EXISTS `measurements`;
CREATE TABLE `measurements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` int(11) DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `units` varchar(45) DEFAULT NULL,
  `threshold` varchar(45) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of measurements
-- ----------------------------
INSERT INTO measurements VALUES ('8', '3', 'UpperLevel', null, 'Tank Stick Upper Level', 'Inches', '1', '1', '2016-11-07 18:53:01', '1', '2017-02-24 17:43:45', '1');
INSERT INTO measurements VALUES ('9', '3', 'BottomLevel', null, 'Tank Stick Bottom Level', 'Inches', '1', '1', '2016-11-07 20:18:46', '1', '2018-02-14 21:10:48', '1');
INSERT INTO measurements VALUES ('11', '3', 'InterfaceLevel', null, 'Tank Stick Interface Level', 'Inches', '1', '1', '2016-11-07 20:20:08', '1', '2017-02-24 17:43:46', '1');
INSERT INTO measurements VALUES ('18', '3', 'TotalVolume', null, 'Tank Stick Total Level', 'Inches', '1', '1', '2016-11-07 21:18:34', '1', '2018-02-12 17:33:55', '1');
INSERT INTO measurements VALUES ('19', '3', 'OilVolume', null, 'Tank Stick Oil Volume', 'Inches', '1', '1', '2016-11-09 04:04:42', '1', '2018-02-12 21:02:31', '1');
INSERT INTO measurements VALUES ('22', '3', 'WaterVolume', null, 'Tank Stick Water Volume', 'Inches', '1', '1', '2016-11-09 19:28:10', '1', '2017-02-24 17:43:49', '1');
INSERT INTO measurements VALUES ('23', '3', 'Temperature', null, 'Tank Stick Temperature', 'Farenheight', '1', '1', '2016-11-09 22:05:35', '1', '2017-02-24 17:43:50', '1');

-- ----------------------------
-- Table structure for `measurement_tags`
-- ----------------------------
DROP TABLE IF EXISTS `measurement_tags`;
CREATE TABLE `measurement_tags` (
  `measurement_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`measurement_id`,`tag_id`),
  KEY `fk_measurement_tags_2_idx` (`tag_id`),
  CONSTRAINT `fk_measurement_tags_1` FOREIGN KEY (`measurement_id`) REFERENCES `measurements` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_measurement_tags_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of measurement_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `models`
-- ----------------------------
DROP TABLE IF EXISTS `models`;
CREATE TABLE `models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `man_id` int(11) unsigned NOT NULL,
  `image_id` int(10) unsigned NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `mod_type` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_models_1_idx` (`man_id`),
  KEY `fk_models_2_idx` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of models
-- ----------------------------
INSERT INTO models VALUES ('1', '1', '1', '123', 'Device', null, '1', '2016-11-29 22:11:00', '1', '0000-00-00 00:00:00', '0');
INSERT INTO models VALUES ('2', '1', '2', '456', 'Device', null, '1', '2016-11-29 22:11:30', '1', null, null);
INSERT INTO models VALUES ('3', '1', '3', '789', 'Object', null, '1', '2016-11-29 22:12:00', '1', null, null);

-- ----------------------------
-- Table structure for `model_images`
-- ----------------------------
DROP TABLE IF EXISTS `model_images`;
CREATE TABLE `model_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model_id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) NOT NULL,
  `thumb_name` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_model_images_1_idx` (`model_id`),
  CONSTRAINT `fk_model_images_1` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of model_images
-- ----------------------------

-- ----------------------------
-- Table structure for `modules`
-- ----------------------------
DROP TABLE IF EXISTS `modules`;
CREATE TABLE `modules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '		',
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(45) DEFAULT NULL,
  `controller` varchar(45) DEFAULT NULL,
  `method` varchar(45) DEFAULT NULL,
  `order_by` tinyint(3) DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `super_user` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent` (`parent_id`),
  KEY `modified` (`modified_by`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of modules
-- ----------------------------
INSERT INTO modules VALUES ('1', '21', 'Device Menu', 'devices', '', '5', '1', '0', '2016-11-07 08:02:02', '1', '2016-12-17 00:43:15', '1');
INSERT INTO modules VALUES ('20', '0', 'Admin', '', '', '1', '1', '0', '2016-09-30 10:59:35', '1', '2016-11-09 20:15:02', '1');
INSERT INTO modules VALUES ('21', '20', 'System', '', '', '5', '1', '1', '2016-09-30 11:25:15', '1', '2016-11-14 21:28:21', '1');
INSERT INTO modules VALUES ('22', '21', 'Accounts', 'accounts', '', '1', '1', '0', '2016-09-30 11:29:51', '1', '2016-10-04 04:25:23', '1');
INSERT INTO modules VALUES ('23', '21', 'Admin Log', 'admin_log', '', '2', '1', '0', '2016-09-30 11:35:15', '1', '2016-10-04 04:25:23', null);
INSERT INTO modules VALUES ('24', '21', 'Object Menu', '', '', '4', '1', '0', '2016-09-30 11:47:04', '1', '2016-12-17 00:43:39', '1');
INSERT INTO modules VALUES ('25', '21', 'Modules', 'modules', '', '6', '1', '0', '2016-09-30 11:47:38', '1', '2016-12-17 00:42:57', '1');
INSERT INTO modules VALUES ('27', '20', 'Departments', 'departments', '', '1', '1', '0', '2016-09-30 11:48:31', '1', '2016-11-14 21:28:45', '1');
INSERT INTO modules VALUES ('28', '20', 'Groups', 'groups', '', '2', '1', '0', '2016-09-30 11:48:52', '1', '2016-11-14 21:28:54', '1');
INSERT INTO modules VALUES ('29', '20', 'Sites', 'sites', '', '3', '1', '0', '2016-09-30 11:51:51', '1', '2016-11-14 21:29:03', '1');
INSERT INTO modules VALUES ('30', '20', 'Users', 'users', '', '4', '1', '0', '2016-09-30 11:52:43', '1', '2016-11-14 21:29:16', '1');
INSERT INTO modules VALUES ('31', '0', 'Help', '', '', '3', '1', '0', '2016-09-30 12:02:18', '1', '2016-10-04 04:25:23', '1');
INSERT INTO modules VALUES ('32', '31', 'Tickets', 'help', 'tickets', '2', '1', '0', '2016-09-30 15:31:59', '1', '2016-10-04 04:25:23', '1');
INSERT INTO modules VALUES ('33', '31', 'Submit Ticket', 'help', 'submit_ticket', '1', '1', '0', '2016-09-30 15:32:54', '1', '2016-10-04 04:25:23', '1');
INSERT INTO modules VALUES ('39', '0', 'Dashboard', 'dashboard', '', '2', '1', '0', '2016-10-01 13:48:02', '1', '2016-11-10 04:25:35', '1');
INSERT INTO modules VALUES ('42', '1', 'Devices', 'devices', '', '1', '1', '0', '2016-11-16 15:39:24', '1', '2016-11-16 23:48:06', '1');
INSERT INTO modules VALUES ('43', '1', 'Executions', 'executions', '', '2', '1', '0', '2016-11-16 15:39:53', '1', '2016-11-17 17:47:42', '1');
INSERT INTO modules VALUES ('44', '1', 'Measurements', 'measurements', '', '3', '1', '0', '2016-11-16 15:40:25', '1', '2016-11-17 17:47:22', '1');
INSERT INTO modules VALUES ('45', '1', 'Triggers', 'triggers', '', '4', '1', '0', '2016-11-16 15:40:49', '1', '2016-11-17 17:48:29', '1');
INSERT INTO modules VALUES ('46', '1', 'Tags', 'tags', '', '6', '1', '0', '2016-11-16 17:46:15', '1', '2016-11-17 17:48:55', '1');
INSERT INTO modules VALUES ('47', '1', 'Actions', 'actions', '', '5', '1', '0', '2016-11-16 18:33:13', '1', '2016-11-17 17:48:07', '1');
INSERT INTO modules VALUES ('48', '24', 'Objects', 'objects', '', '1', '1', '0', '2016-11-24 09:01:09', '1', null, null);
INSERT INTO modules VALUES ('49', '24', 'Images', 'models', 'images', '2', '1', '0', '2016-11-24 09:01:49', '1', '2016-12-06 00:30:02', '1');
INSERT INTO modules VALUES ('50', '24', 'Manufacturers', 'manufacturers', 'objects', '3', '1', '0', '2016-11-29 13:00:55', '1', null, null);
INSERT INTO modules VALUES ('51', '24', 'Models', 'models', 'objects', '4', '1', '0', '2016-11-29 13:01:35', '1', '2016-11-29 19:04:45', '1');
INSERT INTO modules VALUES ('54', '1', 'Manufacturers', 'manufacturers', 'devices', '7', '1', '0', '2016-11-29 13:02:31', '1', null, null);
INSERT INTO modules VALUES ('56', '1', 'Models', 'models', 'devices', '8', '1', '0', '2016-11-29 13:04:15', '1', null, null);
INSERT INTO modules VALUES ('57', '21', 'Controller Menu', '', '', '3', '1', '0', '2016-12-16 18:44:17', '1', '2016-12-27 18:13:37', '1');
INSERT INTO modules VALUES ('58', '57', 'Controllers', 'controllers', '', '1', '1', '0', '2016-12-27 12:13:00', '1', '2016-12-27 23:46:11', '1');
INSERT INTO modules VALUES ('59', '57', 'Manufacturers', 'manufacturers', 'controllers', '2', '1', '0', '2016-12-27 12:14:46', '1', '2016-12-27 18:16:19', '1');
INSERT INTO modules VALUES ('60', '57', 'Models', 'models', 'controllers', '3', '1', '0', '2016-12-27 12:16:50', '1', null, null);

-- ----------------------------
-- Table structure for `notifications`
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` int(10) NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `type` varchar(45) NOT NULL,
  `message` varchar(45) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(45) DEFAULT NULL,
  `name` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_notifications_1_idx` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of notifications
-- ----------------------------
INSERT INTO notifications VALUES ('3', '3', '0', '', '', '2017-02-13 20:42:15', null, 'OverfillShutdown');
INSERT INTO notifications VALUES ('5', '3', '0', '', '', '2017-02-13 20:42:47', null, 'LowStart');
INSERT INTO notifications VALUES ('6', '3', '0', '', '', '2017-02-13 20:42:55', null, 'LowLevelShutdown');
INSERT INTO notifications VALUES ('8', '3', '0', '', '', '2017-05-11 16:20:10', null, 'HighStop');

-- ----------------------------
-- Table structure for `objects`
-- ----------------------------
DROP TABLE IF EXISTS `objects`;
CREATE TABLE `objects` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_id` int(10) unsigned DEFAULT NULL,
  `model_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `man_name` varchar(45) DEFAULT NULL,
  `mod_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `objectID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `image` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of objects
-- ----------------------------
INSERT INTO objects VALUES ('13', null, null, 'Tank', null, null, null, null, '1', '2017-02-14 12:35:07', null, '2018-02-13 21:37:22', null, '1');

-- ----------------------------
-- Table structure for `object_devices`
-- ----------------------------
DROP TABLE IF EXISTS `object_devices`;
CREATE TABLE `object_devices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `ip_address` varchar(254) DEFAULT NULL,
  `address_type` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of object_devices
-- ----------------------------
INSERT INTO object_devices VALUES ('449', '29', '6', '56', '37', '2017-05-11 20:25:21', null, '11', 'Modbus');
INSERT INTO object_devices VALUES ('450', '56', '7', '57', '37', '2018-02-14 21:18:04', null, '11', 'Modbus');
INSERT INTO object_devices VALUES ('451', '56', '7', '58', '37', '2018-02-14 21:35:31', null, '12', 'Modbus');
INSERT INTO object_devices VALUES ('452', '56', '7', '57', '37', '2018-03-14 14:50:04', null, '12', 'Modbus');

-- ----------------------------
-- Table structure for `object_images`
-- ----------------------------
DROP TABLE IF EXISTS `object_images`;
CREATE TABLE `object_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image_name` varchar(255) NOT NULL,
  `thumb_name` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of object_images
-- ----------------------------

-- ----------------------------
-- Table structure for `object_parameters`
-- ----------------------------
DROP TABLE IF EXISTS `object_parameters`;
CREATE TABLE `object_parameters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of object_parameters
-- ----------------------------
INSERT INTO object_parameters VALUES ('1', 'Height');
INSERT INTO object_parameters VALUES ('2', 'Perimeter');

-- ----------------------------
-- Table structure for `operators`
-- ----------------------------
DROP TABLE IF EXISTS `operators`;
CREATE TABLE `operators` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(254) DEFAULT NULL,
  `operand_type_index` int(3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operators
-- ----------------------------
INSERT INTO operators VALUES ('1', '<', '1');
INSERT INTO operators VALUES ('2', '<=', '2');
INSERT INTO operators VALUES ('3', '=', '3');
INSERT INTO operators VALUES ('4', '<>', '4');
INSERT INTO operators VALUES ('6', '>=', '5');
INSERT INTO operators VALUES ('7', '>', '6');
INSERT INTO operators VALUES ('8', '+', '7');
INSERT INTO operators VALUES ('9', '-', '8');
INSERT INTO operators VALUES ('10', '*', '9');
INSERT INTO operators VALUES ('11', '/', '10');
INSERT INTO operators VALUES ('12', 'OR', '11');
INSERT INTO operators VALUES ('13', 'AND', '12');

-- ----------------------------
-- Table structure for `permissions`
-- ----------------------------
DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `controller` varchar(45) NOT NULL,
  `method` varchar(45) NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_permissions_1_idx` (`created_by`),
  KEY `modified` (`modified_by`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of permissions
-- ----------------------------

-- ----------------------------
-- Table structure for `settings`
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `input_type` varchar(45) DEFAULT NULL,
  `active` tinyint(4) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_settings_1_idx` (`created_by`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO settings VALUES ('1', 'Pagination', 'text', '1', '0000-00-00 00:00:00', '1');
INSERT INTO settings VALUES ('2', 'Home Page', 'select_module', '1', '0000-00-00 00:00:00', '1');

-- ----------------------------
-- Table structure for `sites`
-- ----------------------------
DROP TABLE IF EXISTS `sites`;
CREATE TABLE `sites` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(45) NOT NULL DEFAULT 'Online',
  `status_change` datetime DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(254) DEFAULT NULL,
  `address_type` varchar(254) DEFAULT NULL,
  `parameter_id` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account` (`account_id`),
  CONSTRAINT `fk_sites_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sites
-- ----------------------------
INSERT INTO sites VALUES ('56', '1', 'Quest Demo', null, '0.000000000000000', '0.000000000000000', '1', 'Online', null, '2018-02-13 21:25:19', '0', null, null, '166.130.76.71', 'IPv4', null);

-- ----------------------------
-- Table structure for `site_attribs`
-- ----------------------------
DROP TABLE IF EXISTS `site_attribs`;
CREATE TABLE `site_attribs` (
  `id` int(11) NOT NULL,
  `site_id` int(10) unsigned NOT NULL,
  `site_attrib_type` int(10) unsigned NOT NULL,
  `measurement_id` int(10) unsigned NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_site_attribs_1_idx` (`site_id`),
  KEY `fk_site_attribs_2_idx` (`measurement_id`),
  KEY `fk_site_attribs_3_idx` (`site_attrib_type`),
  CONSTRAINT `fk_site_attribs_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_site_attribs_2` FOREIGN KEY (`measurement_id`) REFERENCES `measurements` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_site_attribs_3` FOREIGN KEY (`site_attrib_type`) REFERENCES `site_attrib_types` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_attribs
-- ----------------------------

-- ----------------------------
-- Table structure for `site_attrib_types`
-- ----------------------------
DROP TABLE IF EXISTS `site_attrib_types`;
CREATE TABLE `site_attrib_types` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_attrib_types
-- ----------------------------

-- ----------------------------
-- Table structure for `site_controllers`
-- ----------------------------
DROP TABLE IF EXISTS `site_controllers`;
CREATE TABLE `site_controllers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(11) unsigned NOT NULL,
  `name` varchar(254) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `address_type` varchar(50) DEFAULT NULL,
  `controller_id` int(11) unsigned DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_site_remote_controllers_1_idx` (`site_id`),
  KEY `fk_site_remote_controllers_2_idx` (`controller_id`),
  CONSTRAINT `fk_site_remote_controllers_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_site_remote_controllers_2` FOREIGN KEY (`controller_id`) REFERENCES `controllers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_controllers
-- ----------------------------
INSERT INTO site_controllers VALUES ('7', '56', 'IDEC FC6A', '192.168.2.10', null, null, '2018-02-14 21:14:07', null);

-- ----------------------------
-- Table structure for `site_objects`
-- ----------------------------
DROP TABLE IF EXISTS `site_objects`;
CREATE TABLE `site_objects` (
  `site_id` int(10) unsigned NOT NULL,
  `object_id` int(10) unsigned NOT NULL,
  `latitude` decimal(18,15) DEFAULT NULL,
  `longitude` decimal(18,15) DEFAULT NULL,
  `sms_num` varchar(15) DEFAULT NULL,
  `last_report` datetime DEFAULT NULL,
  `last_restart` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned NOT NULL,
  PRIMARY KEY (`site_id`,`object_id`),
  KEY `site` (`site_id`),
  KEY `fk_site_devices_3_idx` (`created_by`),
  KEY `fk_site_devices_2_idx` (`object_id`),
  CONSTRAINT `fk_site_devices_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_site_devices_2` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_objects
-- ----------------------------
INSERT INTO site_objects VALUES ('41', '21', null, null, null, null, null, '1', '2016-12-15 00:03:42', '1');
INSERT INTO site_objects VALUES ('41', '22', null, null, null, null, null, '1', '2016-12-15 00:03:42', '1');

-- ----------------------------
-- Table structure for `site_states`
-- ----------------------------
DROP TABLE IF EXISTS `site_states`;
CREATE TABLE `site_states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `state` varchar(45) NOT NULL,
  `class` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of site_states
-- ----------------------------
INSERT INTO site_states VALUES ('1', 'Good to Go', 'online', 'green', 'pump_jack_green.png', null);
INSERT INTO site_states VALUES ('2', 'Comm Problem', 'offline', 'gray', 'pump_jack_gray.png', null);
INSERT INTO site_states VALUES ('3', 'Major Problem', 'error', 'red', 'pump_jack_red.png', null);
INSERT INTO site_states VALUES ('4', 'Minor Problem', 'issue', 'yellow', 'pump_jack_yellow.png', null);

-- ----------------------------
-- Table structure for `tags`
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `tag_type` varchar(25) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tags
-- ----------------------------
INSERT INTO tags VALUES ('1', 'Boolean', 'bool', 'Description\'s 123', 'execution', '1', '2016-11-07 18:28:37', '1', '2017-01-28 20:04:52', '1');
INSERT INTO tags VALUES ('3', 'Unsigned Floating Point', 'uflp', '', 'execution', '1', '2016-11-07 18:29:01', '1', '2017-01-28 20:07:23', '1');
INSERT INTO tags VALUES ('5', 'Signed Floating Point', 'sflp', '', 'measurement', '1', '2016-11-07 18:31:07', '1', '2017-01-28 20:10:59', '1');
INSERT INTO tags VALUES ('8', 'Unsigned Integer', 'uint', '', 'execution', '1', '2016-11-09 19:24:26', '1', '2017-01-28 20:06:12', '1');
INSERT INTO tags VALUES ('9', 'Boolean', 'bool', '', 'measurement', '1', '2016-11-09 22:40:17', '1', '2017-01-28 20:07:55', '1');
INSERT INTO tags VALUES ('10', 'Signed Floating Point', 'sflp', '', 'execution', '1', '2016-11-16 22:29:08', '1', '2017-01-28 20:11:26', '1');
INSERT INTO tags VALUES ('11', 'Signed Integer', 'sint', '', 'execution', '1', '2016-11-16 22:36:48', '1', '2017-01-28 20:06:49', '1');
INSERT INTO tags VALUES ('12', 'Unsigned Integer', 'uint', '', 'measurement', '1', '2016-11-16 22:37:04', '1', '2017-01-28 20:08:48', '1');
INSERT INTO tags VALUES ('13', 'Signed Floating Point', 'sflp', '', 'action', '1', '2016-11-16 22:37:22', '1', '2017-01-28 20:12:15', '1');
INSERT INTO tags VALUES ('14', 'Unsigned Floating Point', 'uflp', '', 'measurement', '1', '2016-11-17 20:57:29', '1', '2017-01-28 20:09:25', '1');
INSERT INTO tags VALUES ('15', 'Signed Integer', 'sint', '', 'measurement', '1', '2016-11-17 20:57:43', '1', '2017-01-28 20:08:20', '1');
INSERT INTO tags VALUES ('16', 'Boolean', 'bool', '', 'action', '1', '2016-11-17 20:57:58', '1', '2017-01-28 20:05:34', '1');
INSERT INTO tags VALUES ('17', 'Signed Integer', 'sint', '', 'action', '1', '2017-01-28 14:12:57', '1', null, null);
INSERT INTO tags VALUES ('18', 'Unsigned Integer', 'uint', '', 'action', '1', '2017-01-28 14:13:56', '1', null, null);
INSERT INTO tags VALUES ('19', 'Unsigned Floating Point', 'uflp', '', 'action', '1', '2017-01-28 14:15:02', '1', null, null);
INSERT INTO tags VALUES ('20', 'Boolean', 'bool', '', 'trigger', '1', '2017-01-28 14:15:59', '1', null, null);
INSERT INTO tags VALUES ('21', 'Signed Integer', 'sint', '', 'trigger', '1', '2017-01-28 14:16:14', '1', '2017-01-28 20:16:41', '1');
INSERT INTO tags VALUES ('22', 'Unsigned Integer', 'uint', '', 'trigger', '1', '2017-01-28 14:16:34', '1', null, null);
INSERT INTO tags VALUES ('24', 'Signed Floating Point', 'sflp', '', 'trigger', '1', '2017-01-28 14:17:01', '1', null, null);
INSERT INTO tags VALUES ('25', 'Unsigned Floating Point', 'uflp', '', 'trigger', '1', '2017-01-28 14:17:26', '1', null, null);

-- ----------------------------
-- Table structure for `tag_data`
-- ----------------------------
DROP TABLE IF EXISTS `tag_data`;
CREATE TABLE `tag_data` (
  `site_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `value` float NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`site_id`,`tag_id`,`created`),
  KEY `fk_tag_data_2_idx` (`tag_id`),
  CONSTRAINT `fk_tag_data_1` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_tag_data_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tag_data
-- ----------------------------

-- ----------------------------
-- Table structure for `tickets`
-- ----------------------------
DROP TABLE IF EXISTS `tickets`;
CREATE TABLE `tickets` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL,
  `module_id` int(11) unsigned NOT NULL,
  `issue` varchar(255) NOT NULL,
  `status` varchar(45) DEFAULT 'Open',
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) DEFAULT NULL,
  `resolved` datetime DEFAULT NULL,
  `resolved_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tickets_1_idx` (`account_id`),
  KEY `fk_tickets_2_idx` (`module_id`),
  CONSTRAINT `fk_tickets_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tickets_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tickets
-- ----------------------------
INSERT INTO tickets VALUES ('1', '1', '39', 'Need new features...', 'Open', '2016-10-03 09:11:33', '1', '2016-10-03 14:11:33', null, null, null);
INSERT INTO tickets VALUES ('2', '1', '27', 'Can\'t add a new...', 'Open', '2016-10-03 21:07:13', '1', '2016-10-04 02:07:13', null, null, null);
INSERT INTO tickets VALUES ('3', '1', '31', 'Doesn\'t work...', 'Open', '2016-10-03 21:11:00', '1', '2016-10-04 02:11:00', null, null, null);
INSERT INTO tickets VALUES ('4', '1', '31', 'Doesn\'t work...', 'Open', '2016-10-03 21:11:17', '1', '2016-10-04 02:11:17', null, null, null);
INSERT INTO tickets VALUES ('5', '1', '23', '1234', 'Open', '2016-10-03 21:58:08', '1', '2016-10-04 02:58:08', null, null, null);

-- ----------------------------
-- Table structure for `triggers`
-- ----------------------------
DROP TABLE IF EXISTS `triggers`;
CREATE TABLE `triggers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `device_type_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `xml_name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `threshold` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` datetime DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of triggers
-- ----------------------------
INSERT INTO triggers VALUES ('1', '3', 'LowLevel', 'lowlevel', '', null, '1', '2016-11-16 23:43:28', '1', '2017-02-24 18:15:37', '1');
INSERT INTO triggers VALUES ('4', '3', 'HighHighLevel', 'highband', '', null, '1', '2016-11-17 13:57:26', '1', '2018-02-14 21:41:29', '1');
INSERT INTO triggers VALUES ('6', '3', 'LowLowLevel', 'lowband', '', null, '1', '2016-11-17 14:11:08', '1', '2018-02-14 21:41:53', '1');
INSERT INTO triggers VALUES ('7', '3', 'HighLevel', 'highlevel', '', null, '1', '2016-11-19 19:18:38', '1', '2017-02-24 18:15:40', '1');

-- ----------------------------
-- Table structure for `trigger_actions`
-- ----------------------------
DROP TABLE IF EXISTS `trigger_actions`;
CREATE TABLE `trigger_actions` (
  `trigger_id` int(10) unsigned NOT NULL,
  `action_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`trigger_id`,`action_id`),
  KEY `fk_trigger_actions_2_idx` (`action_id`),
  CONSTRAINT `fk_trigger_actions_1` FOREIGN KEY (`trigger_id`) REFERENCES `triggers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_trigger_actions_2` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trigger_actions
-- ----------------------------

-- ----------------------------
-- Table structure for `trigger_tags`
-- ----------------------------
DROP TABLE IF EXISTS `trigger_tags`;
CREATE TABLE `trigger_tags` (
  `trigger_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`trigger_id`,`tag_id`),
  KEY `fk_trigger_tags_2_idx` (`tag_id`),
  CONSTRAINT `fk_trigger_tags_1` FOREIGN KEY (`trigger_id`) REFERENCES `triggers` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_trigger_tags_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of trigger_tags
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(11) unsigned NOT NULL DEFAULT '1',
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `user_name` varchar(45) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  `modified` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `deleted_by` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `account` (`account_id`),
  KEY `modified_by` (`modified_by`),
  CONSTRAINT `fk_users_1` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO users VALUES ('1', '1', 'Nick', 'Morton', 'configAdmin', 'nmorton@quest-automated.com', '$2y$10$P6fTpMzdS42bSTCM7rQBo.4HKdqNf4mcWRGDGUqOlITjZtAnJYGru', '1', '0000-00-00 00:00:00', '0', null, null, '0', '0');
INSERT INTO users VALUES ('2', '1', 'Test', 'Test', 'Test', 'test', '$2y$10$VESl9FyZsYKr/UG1Ko0sVe3Kw/RFDYsOZ5eFdFJATzgMleIEQq9YS', '1', '2018-02-14 19:50:45', '0', null, null, '0', '0');

-- ----------------------------
-- Table structure for `user_departments`
-- ----------------------------
DROP TABLE IF EXISTS `user_departments`;
CREATE TABLE `user_departments` (
  `user_id` int(11) unsigned NOT NULL,
  `department_id` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`department_id`),
  KEY `user` (`user_id`),
  KEY `department` (`department_id`),
  CONSTRAINT `fk_user_departments_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_departments_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_departments
-- ----------------------------

-- ----------------------------
-- Table structure for `user_groups`
-- ----------------------------
DROP TABLE IF EXISTS `user_groups`;
CREATE TABLE `user_groups` (
  `user_id` int(11) unsigned NOT NULL,
  `group_id` int(11) unsigned NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`user_id`),
  KEY `user` (`user_id`),
  KEY `group` (`group_id`),
  CONSTRAINT `fk_user_groups_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_groups_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_groups
-- ----------------------------

-- ----------------------------
-- Table structure for `user_settings`
-- ----------------------------
DROP TABLE IF EXISTS `user_settings`;
CREATE TABLE `user_settings` (
  `user_id` int(10) unsigned NOT NULL,
  `home_page` varchar(45) DEFAULT NULL,
  `pagination` tinyint(3) unsigned DEFAULT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_settings_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user_settings
-- ----------------------------

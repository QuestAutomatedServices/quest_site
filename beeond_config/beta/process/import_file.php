<?php
define('HOSTNAME','localhost');
define('DB_USERNAME','root');
define('DB_PASSWORD','');
define('DB_NAME', 'quest_config');
//global $con;
$con = mysqli_connect(HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME) or die ("error");
// Check connection
if(mysqli_connect_errno($con))  echo "Failed to connect MySQL: " .mysqli_connect_error();

include("../app/functions.php");
error_reporting(E_ALL | E_STRICT);
	if (isset($_POST['import'])) 
	{
		if($_FILES['file']['name'])
		{
			//Choose extensions
			$allowedExtensions = array("xls","xlsx","csv");
			//Get file extension
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			
			//Validate if file is of extension xls or xlsx
			if(in_array($ext, $allowedExtensions)) {
				include("Classes/PHPExcel/IOFactory.php");
				$file = $_FILES['file']['tmp_name'];
				try {
					
					//Load the excel(.xls/.xlsx) file
					$objPHPExcel = PHPExcel_IOFactory::load($file);
				} catch (Exception $e) {
					 die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
				}
				
				//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
				$sheet = $objPHPExcel->getSheet(0);
				//It returns the highest number of rows
				$total_rows = $sheet->getHighestRow();
				//It returns the highest number of columns
				$total_columns = $sheet->getHighestDataColumn();
						
				$query = "insert into `import_data` (`SiteName`,`SiteLatitude`,`SiteLongitude`,`SiteAddress`,`SiteAddressType`,`ControllerName`,`ControllerAddress`,`ControllerAddressType`,`ObjectName`,`ObjectType`,`ObjectParameter`,
				`ObjectParameterName`,`ObjectParameterValue`,`DeviceName`,`DeviceType`,`DeviceManufacturer`,`OperationType`, `TagName`,`TagDescription`,`TagDeviceType`,`TagDeviceManufacturer`, `DataType`,`Site`, `Controller`, `Object`, `Device`, `Operation`, `GroupNumber`, `Tag`,  `NodeID`) VALUES ";

				//Loop through each row of the worksheet
                for($row =2; $row <= $total_rows; $row++) {
					//Read a single row of data and store it as a array.
                    //This line of code selects range of the cells like A1:D1
                    $single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);

					$query .= "(";
					foreach($single_row[0] as $key=>$value) {
							$value = str_replace("'","",$value);
							$query .= "'".$value."',";
					}
					$query = substr($query, 0, -1);
                    $query .= "),";
				}
				$query = substr($query, 0, -1);
						
				if(!mysqli_query($con, $query)) {
					printf("Errormessage: %s\n", mysqli_error($con));
				}
				
				if(mysqli_affected_rows($con) > 0) {    
                        return '<span class="msg">Database table updated!</span>';
						/* insertExcel(); */
                    } else {
                        echo '<span class="msg">Can\'t update database table! try again.</span>';
                    } 
			} 
			else {
				echo '<span class="msg">This type of file is not allowed!</span>';
			}
		} 
		else 
		{
			echo '<span class="msg">Select an excel file first!</span>';
		}
	} 
	
?>
<?php 
include("../app/functions.php");
$attr = getAttributes($_GET['id'],$_GET['type']);

if($attr && ($_GET['type'] == 'Modbus' || $_GET['type'] == 'COM' )) {
$html = '

<div class="form-group">
	<label for="baudRate">Baud Rate</label>
	<input type="text" class="form-control" disabled name="baudRate" id="baudRate" value="'.$attr['baud_rate'].'">												
 </div>
 <div class="form-group">
	<label for="parity">Parity</label>
	<input type="text" class="form-control" disabled name="parity" id="parity" value="'.$attr['parity'].'">												
 </div>
 <div class="form-group">
	<label for="parity">Data Bits</label>
	<input type="text" class="form-control" disabled name="dataBits" id="dataBits" value="'.$attr['data_bits'].'">												
 </div>
 <div class="form-group">
	<label for="stopBits">Stop Bits</label>
	<input type="text" class="form-control" disabled name="stopBits" id="stopBits" value="'.$attr['stop_bits'].'">												
 </div>
 <div class="form-group">
	<label for="flowControl">Flow Control</label>
	<input type="text" class="form-control" disabled name="flowControl" id="flowControl" value="'.$attr['flow_control'].'">												
 </div>

';
} else {
	$html = '';
}

echo $html;


?> 
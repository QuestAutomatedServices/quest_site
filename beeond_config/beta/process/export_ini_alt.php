<?php

include("../app/functions.php");

$db = connect();

$id = $_GET['id'];

$stmt = $db->prepare("SELECT * FROM site_controllers WHERE id=:id");
	
$stmt->bindValue(':id', $id, PDO::PARAM_INT);	
$stmt->execute();
$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (empty($results[0]['port'])) {
	$results[0]['port'] = 4840;
}


function write_ini_file($assoc_arr, $path, $has_sections = FALSE)
{
    $content = "";
    if ($has_sections) {
        foreach ($assoc_arr as $key => $elem) {
            $content .= "[" . $key . "]\n";
            foreach ($elem as $key2 => $elem2) {
                if (is_array($elem2)) {
                    for ($i = 0; $i < count($elem2); $i++) {
                        $content .= $key2 . "[] = " . $elem2[$i] . "\n\n";
                    }
                } else if ($elem2 == "") $content .= $key2 . " \n";
                else $content .= $key2 . " = " . $elem2 . "\n\n";
            }
        }
    } else {
        foreach ($assoc_arr as $key => $elem) {
            if (is_array($elem)) {
                for ($i = 0; $i < count($elem); $i++) {
                    $content .= $key . "[] = " . $elem[$i] . "\n\n";
                }
            } else if ($elem == "") $content .= $key . " = \n\n";
            else $content .= $key . " = " . $elem . "\n\n";
        }
    }
    if (!$handle = fopen($path, 'w')) {
        return false;
    }
    $success = fwrite($handle, $content);
    fclose($handle);
    return $success;
	exit;
}




$sampleData = array(
    'log' => array(
		'; Sets the log out level.' => '',
		'; Valid values are:' => '',
		';           1-3 debug' => '',
		';           4-5 Warning' => '',
		';           6   error' => '',
	 	'LEVEL' => 5,
		'; Enables logging to be sent to standard output' => '',
		'STDOUTPUT' => 'true',
    ),
	'main' => array(
		'; Sets the interval, in minutes,  that data is sent to the remote end point' => '',
		'; Value of 15 is the default' => '',
		'CHECK_SCHEMA' => 15,
		'; Update/read data in sec default 60 sec' => '',
		'UPDATE_DATA' => 10,
		'; Updatewriting data in mssec default 500 mssec' => '',
		'WRITE_DATA' => 500,
		'; Start the executable as a daemon' => '',
		'; The default value is false' => '',
		'DAEMON' => 'false',
		'; Simulation of changing data for testing when plc read static data by adding %timer' => '',
		'SIMULATE_CHANGING_DATA' => 'false',
    ),
	'adapter' => array(	
		'; Select the class of adapter to be used to communicaiton with local device' => '',
		'; Valid values are' => '',
		';             ModbusSimulatorAdaptor' => '',
		';             ModbusTCPAdapter' => '',
		'CLASS' => 'ModbusRTUAdapter',
		'; Specify the local device communications adapter settings' => '',
		'; Timeout for communications devices given in seconds' => '',
		'TIMEOUT' => 3,
		'; TCP/IP Devices' => '',
		'; Valid configurable options are:' => '',
		';           ip' => '',
		';           port' => '',
        'IPADDRESS' => $results[0]['ip_address'],
		'SERVICEPORT' => $results[0]['port'],
		'; Serial / RS232 Devices' => '',
		'; Port specification to use' => '',
		'; PORT = /dev/ttyUSB0' => '',
		' ' => '',
		'; Baud Rate' => '',
		'; BAUD = 9600' => '',
		'  ' => '',
		'; Parity' => '',
		'; PARITY = N' => '',
		'   ' => '',
		'; Data Bits' => '',
		'; DATABITS = 8' => '',
		'' => '',
		'; Stop Bits' => '',
		'; STOPBITS = 1' => '',
		'    ' => '',
		'; Flow Control' => '',
		'; RTS = 0' => '',
		'   ' => '',
    ),
	'data provider' => array(
		'; Class used to select driver to use to communicate with device' => '',
		'; 	Ex: CLASS = LufkinRpcDataProvider' => '',
		'; 			CLASS = PlcDataProvider' => '',
		'CLASS' => 'LufkinRpcDataProvider',
		'; Register Address space to utilize' => '',
		'; 		Ex: REGISTER ADDRESSES = idec' => '',
		'; REGISTER ADDRESSES = idec' => '',
		'' => '',
		'; Protocol to use' => '',
		'; PROTOCOL  = protocol' => '',
		'          ' => '',
		'; Timeout in seconds' => '',
		'TIMEOUT' => 2,
        'DELAY' => 500,
    ),
	'client' => array(
		'; Plugin for client to use' => '',
		'; Valid values to user are' => '',
		'; 		OpcUaClient' => '',
		'; 		LcmClient' => '',
		'CLASS' => 'OpcUaClient',
		'; Endpoints PLC_DATA_POINT' => '',
		'; 		EX: ENDPOINT = opc.tcp://localhost:16664' => '',
		'; 			   ENDPOINT = opc.tcp://debian:48010' => '',
		'ENDPOINT' => 'opc.tcp://localhost:48010',
    ),
	'test' => array(
		'FC' => '0',
		'FC_ACK' => 10,
        'WR_DATA' => 1,
		'RD_DATA' => 2,
    ),
	'idec' => array(
		'FC' => '1;',
		'FC_ACK' => '51 ; 100',
        'WR_DATA' => '100 ; 101',
		'RD_DATA' => '130 ; 200',
		'RD_FUN' => 3,
    ),
	'triangle' => array(
		'FC' => '3;',
		'FC_ACK' => '51;',
        'WR_DATA' => '41101;',
		'RD_DATA' => '41201;',
    ),
	'protocol' => array(
		'; Commands' => '',
		'GET DATA' => '1;',
		'GET DATA ACK' => '3;',
        'SET DATA' => '2;',
		'READY' => '5;',
		'RESET' => '7;',
		'; Status/Mode' => '',
        'NORMAL MODE' => '77;',
		'CONFIG MODE' => '97;',
		'; Methods' => '',
		'; There are four possible methods to use. Their' => '',
		'; order/enumeration values are set here' => '',
        'MEASUREMENT' => 1,
		'EXECUTION' => 2,
		'TRIGGER' => 3,
        'NOTIFICATION' => 4,
		'FUNCTION' => 5,
    ),
	'schema parser' => array(
		'; Plugin for SchemaParser' => '',
		'; Valid values are' => '',
		'; 		TestSchemaParser' => '',
		'; 		OpcUaSchemaParser' => '',
		'CLASS' => 'OpcUASchemaParser',
		'; Schema File' => '',
		'; 	For file on local system location provide the Full path and filename' => '',
		'; 		Ex: SOURCE = /usr/local/quest/etc/quest/opc-ua/' => '',
		'; 	For file at remote location provide the full URI' => '',
		'; 		Ex: SOURCE = http://192.168.1.11/questdemo.xml' => '',
		'SOURCE' => '../demo/questdemo.xml ; ',
	));
write_ini_file($sampleData, './datasourcegateway.ini', true);

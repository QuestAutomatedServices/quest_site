<?php 
include("../app/functions.php");
$ctob = getStructures($current = null,$_GET['pt']);

if($ctob) {
	
$html = '<select class="form-control" name="structure_id" id="structure" >';
	
	$html .= '<option value="">No Structure</option>';

		foreach ($ctob as $ob) {
			
			if ($ob['name'] == $current) {
				$html .= '<option value="'.$ob['configID'].'" selected="selected">'.$ob['name'].'</option>';
			} else {
				$html .= '<option value="'.$ob['configID'].'">'.$ob['name'].'</option>';
			}
		}

	$html .= '</select>';
} else {
	$html = '<select class="form-control" name="structure_id" id="structure" >';
	
	$html .= '<option value="">No Structure</option>';
	
	$html .= '</select>';
}

	echo $html;

?>
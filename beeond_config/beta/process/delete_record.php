<?php

include("../app/functions.php");

$type = $_GET['type'];

switch($type){
		
	case "oj":
		$table = "objects";
		break;
	case "mf":
		$table = "manufacturers";
		break;
	case "dv":
		$table = "devices";
		break;
	case "vt":
		$table = "device_types";
		break;
	case "mr":
		$table = "measurements";
		break;
	case "ex":
		$table = "executions";
		break;
	case "tr":
		$table = "triggers";
		break;
	case "ns":
		$table = "notifications";
		break;
	case "fs":
		$table = "functions";
		break;
	case "dt":
		$table = "data_types";
		break;
	case "op":
		$table = "operators";
		break;
	case "od":
		$table = "object_devices";
		break;
	case "co":
		$table = "controller_objects";
		break;
	case "sc":
		$table = "site_controllers";
		break;
	case "s":
		$table = "sites";
		break;
	case "opa":
		$table = "object_parameters";
		break;
}

if($type == "od" or $type == "co" or $type == "sc" or $type == "opa"){
	$location = $_SERVER['HTTP_REFERER'];
}
elseif ($type == "s") {
	$location = "../index.php";
}
else {
	$location = "../index.php?$type=1";
}

if($type == "co") {
	deleteObject($table,$_GET['id']);
} elseif($type == "sc"){
	deleteController($table,$_GET['id']);
} elseif($type == "s"){
	deleteSite($table,$_GET['id']);
} elseif($type == "dv"){
	deleteDevice($table,$_GET['id']);
} elseif($type == "od"){
	deleteInstance($table,$_GET['id']);
} else {
	deleteRecord($table,$_GET['id']);
}

header("location: $location");
?>
<?php 
session_start();

error_reporting (E_ALL ^ E_NOTICE);


if(!isset($_SESSION['accountID'])){
	session_destroy();
	header("location: login.php");
}

include("app/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Configuration Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">

	 
     
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
     
      <link id="themecss" rel="stylesheet" type="text/css" href="app/all.min.css" />
      
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	  
	  
    <![endif]-->
	
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php" tabindex=-1>Quest Configuration Portal</a>
		  
        </div>
        <div class="nav navbar-nav pull-right">
			<div class="navbar-brand"> <?php echo $_SESSION['username'];  ?> </div>
			<a href="logout.php" class="navbar-brand" tabindex=-1>Logout</a>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-list">
           
         	<!--- <?php //echo getAccountSelect(''); 
			?> 
			<li><a href="index.php?acc=1">+ New Account</a></li>
			-->

          <!-- <li class="nav-header">Tools</li>
           
			  <li><a href="index.php">Sites</a></li> -->
			  <!--
			  <li><a href="index.php?f=1">File Manager</a></li>
			  <li><a href="index.php?m=1">Map</a></li>
			  <li><a href="index.php?u=1">Users</a></li>
			  -->
				

			  <li class="nav-header">Configured Items</li>
			  
				<li><a href="index.php" tabindex=-1>Sites</a></li>
				
				<li><a href="index.php?ctr=1" tabindex=-1>Controllers</a></li>
          		
			  	<li><a href="index.php?oj=1" tabindex=-1>Objects</a></li>

          		<li><a href="index.php?dv=1" tabindex=-1>Devices</a></li>
          		
         <li class="nav-header"><a data-toggle="collapse" href="#collapse1" style="color:#333;padding:0;margin-top:10px;" tabindex=-1>Global Setup</a></li>
		 
				<div id="collapse1" class="panel-collapse collapse" style="padding:10px;">
					<li style="padding:10px 5px;"><a href="index.php?mf=1">Manufacturers</a></li> 
					<li style="padding:10px 5px;"><a href="index.php?vt=1">Device Types</a></li>
					<li style="padding:10px 5px;"><a href="index.php?mr=1">Measurements</a></li>
					<li style="padding:10px 5px;"><a href="index.php?tr=1">Triggers</a></li>
					<li style="padding:10px 5px;"><a href="index.php?ex=1">Executions</a></li>	
					<li style="padding:10px 5px;"><a href="index.php?ns=1">Notifications</a></li>
					<li style="padding:10px 5px;"><a href="index.php?fs=1">Functions</a></li>
					<li style="padding:10px 5px;"><a href="index.php?dt=1">Data Types</a></li>
					<li style="padding:10px 5px;"><a href="index.php?op=1">Operands</a></li>
					<li style="margin-top:50px;margin-left:5%;"><a href="index.php?tb=1" class="btn btn-primary btn-lg" ><span class="glyphicon glyphicon-trash"></span></a></li>
				</div>
			<!--  
		 <li class="nav-header">Account</li>
			 	<li><a href="logout.php">Logout</a></li>
			-->
			
			

          </ul>
        </div>
		
		
        
         <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          				<?php if ((empty($_GET) ) || ($_GET['hi'] == 1)) { ?>
          
          						<h1 class="page-header">Sites - <?php echo getAccountName($_SESSION['accountID']); ?></h1>
								<?php 
								if($_GET['hi'] == 1) {
								?>
									<button type="button" class="btn btn-primary" style="float:right;margin-left:10px;">Export Template</button>
									<a href="index.php">+ New Site</a>
									<form class="form-inline pull-right" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>">
  										<div class="form-group">
											<span class='label label-info' id="upload-file-info"></span>
											 <label class="btn btn-primary" for="my-file-selector">
												<input id="my-file-selector" type="file" name="file" style="display:none" 
												onchange="$('#upload-file-info').html(this.files[0].name)">
												Choose File
											</label>
											<button type="submit" name="import" class="btn btn-primary" style="float:right;margin-left:10px;">Import</button>
										</div>
									</form>
	
								
								<?php
								} else {
									?>
									
									<a href="process/export_template.php"><button type="button" class="btn btn-primary" style="float:right;margin-left:10px;">Export Template</button></a>
									<a href="index.php?hi=1">+ New Site</a>
									<form class="form-inline pull-right" method="post" enctype="multipart/form-data" action="<?php echo $_SERVER['PHP_SELF'];?>">
  										<div class="form-group">
											<span class='label label-info' id="upload-file-info"></span>
											 <label class="btn btn-primary" for="my-file-selector">
												<input id="my-file-selector" type="file" name="file" style="display:none" 
												onchange="$('#upload-file-info').html(this.files[0].name)">
												Choose File
											</label>	
											<button type="submit" name="import" class="btn btn-primary" style="float:right;margin-left:10px;">Import</button>
										</div>
									</form>
									
									<?php
								}
								
								
								

										if (($_GET['hi'] == 1) && !isset($_GET['id'])) {
								?>
								
								<form method="post" class="form-inline" action="process/add_site.php" style="margin-top:20px;">
									<div class="form-group">
										<input name="account_id" value="<?php echo $_SESSION['accountID']; ?>" type="hidden">
										<input name="created_by" value="<?php echo $_SESSION['userID']; ?>" type="hidden">
										<input name="active" value="1" type="hidden">
										<input name="status" value="Online" type="hidden">
										<input type="text" class="form-control" name="name" placeholder="Name">
										<input type="text" class="form-control" name="latitude" placeholder="Latitude">
										<input type="text" class="form-control" name="longitude" placeholder="Longitude">
													
										<?php echo getSelectIPAddressType();?>
										<input type="text" class="form-control" name="ip_address" placeholder="IP Address">
												
										<button type="submit" class="btn btn-default">Submit</button>
											
									</div>
								</form>
								
								<?php 
								
										}
										
								?>
								
								
								
								
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Site Name</th>
									  <th>Latitude</th>
									  <th>Longitude</th>
									  <th>Communication Address</th>
									  <th>Site Status</th>
									  <th>XML</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$sites = getAccountSites($_SESSION['accountID']); 

										foreach ($sites as $site) {

											$class = '';

											if ($site['status'] == 'Problem') {
												$class = 'class="danger"';
											}

											if ($site['status'] == 'Offline') {
												$class = 'class="info"';
											}

											if ($site['status'] == 'Error') {
												$class = 'class="warning"';
											}
											
											if(!isset($_GET['id'])) {

											echo '<tr '.$class.'>
												  <td><a href="index.php?s='.$site['id'].'">'.$site['name'].'</a></td>
												  <td>'.$site['latitude'].'</td>
												  <td>'.$site['longitude'].'</td>
												  <td>'.$site['address_type'].' - '.$site['ip_address'].'</td>
												  <td>'.$site['status'].'</td>
												  <td><a href="xml.php?s='.$site['id'].'">Download</a></td>
												  <td><a href="index.php?hi=1&id='.$site['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a><a href="process/delete_record.php?type=s&id='.$site['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
												  <span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span></a><a href="process/export_file.php?id='.$site['id'].'" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-export"></span>
													</a></td>
												</tr>';
												
											} else {
												echo '<tr '.$class.'>';
												if($site['id'] == $_GET['id']) {
													echo '<form method="post" action="process/update_site.php">
														<input type="hidden" name="id" value="'.$_GET['id'].'">
														<input name="account_id" value="'.$_SESSION['accountID'].'" type="hidden">
														<input name="created_by" value="'.$_SESSION['userID'].'" type="hidden">
														<input name="active" value="1" type="hidden">
														<input name="status" value="Online" type="hidden">
														<td><input type="text" class="form-control" name="name" value="'.$site['name'].'"></td>
														<td><input type="text" class="form-control" name="latitude" value="'.$site['latitude'].'"></td>
														<td><input type="text" class="form-control" name="longitude" value="'.$site['longitude'].'"></td>
														<td><select class="form-control" name="address_type" style="width:45%;float:left;" >
															<option value="">--Type--</option>
															<option value="IPv4">IPv4</option>
															<option value="IPv6">IPv6</option>
															</select>
															<input type="text" class="form-control" name="ip_address" value="'.$site['ip_address'].'" style="width:50%;float:right;">
														</td>
														<td><input type="text" class="form-control" name="status" value="'.$site['status'].'"></td>
														<td><a href="xml.php?s='.$site['id'].'">Download</a></td>														
														<td>		
														<button type="submit" class="btn btn-default">Submit</button>
														<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
														</td>
													
												</form>';
													
												}else{
													echo '
												  <td>'.$site['name'].'</td>
												  <td>'.$site['latitude'].'</td>
												  <td>'.$site['longitude'].'</td>
												  <td>'.$site['address_type'].' - '.$site['ip_address'].'</td>
												  <td>'.$site['status'].'</td>
												  <td><a href="xml.php?s='.$site['id'].'">Download</a></td>
												  <td></td>
												';
												}
												
												
												'</tr>';
												
											}
										}

								   ?>
								  </tbody>
								</table>
								
							
							<?php
							
							/// CODE TO IMPORT EXCEL
							define('HOSTNAME','localhost');
							define('DB_USERNAME','root');
							define('DB_PASSWORD','');
							define('DB_NAME', 'quest_config');
							//global $con;
							$con = mysqli_connect(HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME) or die ("error");
							// Check connection
							if(mysqli_connect_errno($con))  echo "Failed to connect MySQL: " .mysqli_connect_error();

								if (isset($_POST['import'])) 
								{
									if($_FILES['file']['name'])
									{
										//Choose extensions
										$allowedExtensions = array("xls","xlsx","csv");
										//Get file extension
										$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
										
										//Validate if file is of extension xls or xlsx
										if(in_array($ext, $allowedExtensions)) {
											include("Classes/PHPExcel/IOFactory.php");
											$file = $_FILES['file']['tmp_name'];
											try {
												
												//Load the excel(.xls/.xlsx) file
												$objPHPExcel = PHPExcel_IOFactory::load($file);
											} catch (Exception $e) {
												 die('Error loading file "' . pathinfo($file, PATHINFO_BASENAME). '": ' . $e->getMessage());
											}
											
											//An excel file may contains many sheets, so you have to specify which one you need to read or work with.
											$sheet = $objPHPExcel->getSheet(0);
											//It returns the highest number of rows
											$total_rows = $sheet->getHighestRow();
											//It returns the highest number of columns
											$total_columns = $sheet->getHighestDataColumn();
											$colNumber = PHPExcel_Cell::columnIndexFromString($total_columns);
											
											
											if($colNumber == 22) {
												$query = "insert into `import_data` (`SiteName`,`SiteLatitude`,`SiteLongitude`,`SiteAddress`,`SiteAddressType`,`ControllerName`,`ControllerAddress`,`ControllerAddressType`,`ObjectName`,`ObjectType`,`ObjectParameter`,
												`ObjectParameterName`,`ObjectParameterValue`,`DeviceName`,`DeviceType`,`DeviceManufacturer`,`OperationType`, `TagName`,`TagDescription`,`TagDeviceType`,`TagDeviceManufacturer`, `DataType`) VALUES ";
											} else {
												$query = "insert into `import_data` (`SiteName`,`SiteLatitude`,`SiteLongitude`,`SiteAddress`,`SiteAddressType`,`ControllerName`,`ControllerAddress`,`ControllerAddressType`,`ObjectName`,`ObjectType`,`ObjectParameter`,
												`ObjectParameterName`,`ObjectParameterValue`,`DeviceName`,`DeviceType`,`DeviceManufacturer`,`OperationType`, `TagName`,`TagDescription`,`TagDeviceType`,`TagDeviceManufacturer`, `DataType`, `Site`, `Controller`, `Object`, `Device`, `Operation`, `GroupNumber`, `TagNumber`, `NodeID`) VALUES ";
											}
											//Loop through each row of the worksheet
											for($row =2; $row <= $total_rows; $row++) {
												//Read a single row of data and store it as a array.
												//This line of code selects range of the cells like A1:D1
												$single_row = $sheet->rangeToArray('A' . $row . ':' . $total_columns . $row, NULL, TRUE, FALSE);

												$query .= "(";
												foreach($single_row[0] as $key=>$value) {
														$value = str_replace("'","",$value);
														$query .= "'".$value."',";
												}
												$query = substr($query, 0, -1);
												$query .= "),";
											}
											$query = substr($query, 0, -1);
													
											if(!mysqli_query($con, $query)) {
												printf("Errormessage: %s\n", mysqli_error($con));
											}
											
											if(mysqli_affected_rows($con) > 0) {
													if($colNumber == 22) {
														/* echo 'standard'; */
														insertStandardExcel(); 
													} else 
													{
														/* echo 'configured'; */
														insertConfiguredExcel();												
													}												
													echo '<script language="javascript">';
													echo 'alert("Database Updated")';
													echo '</script>';
													
												} else {
													echo '<script language="javascript">';
													echo 'alert("Can\'t update database table! try again.")';
													echo '</script>';
												} 
										} 
										else {
											echo '<script language="javascript">';
											echo 'alert("This type of file is not allowed!")';
											echo '</script>';
										}
									} 
									else 
									{
										echo '<script language="javascript">';
										echo 'alert("Select an excel file first!")';
										echo '</script>';

									}
								} 
								
							?>	
								
                            
                            
                            <?php } ?>
							
							
							<?php if (isset($_GET['ctr']) && !isset($_GET['s']) ) { ?>
						
								<h1 class="page-header">Controllers</h1>
								<?php 
								if (($_GET['h'] == 1)) {
									
								?>
								<a href="index.php?ctr=1">+ New Controller</a>
								
								<?php
								} else {
								?>
									<a href="index.php?ctr=1&h=1">+ New Controller</a>
								<?php
								}
										if (($_GET['h'] == 1)) {
								?>
								
								<form method="post" class="form-inline" action="process/add_site_controller.php" style="margin-top:20px;">
									<div class="form-group">
										<input name="account_id" value="<?php echo $_SESSION['accountID']; ?>" type="hidden">
										<input name="created_by" value="<?php echo $_SESSION['userID']; ?>" type="hidden">
										<input name="active" value="1" type="hidden">
										<input name="status" value="Online" type="hidden">
										<input type="text" class="form-control" name="name" placeholder="Name">
										<input type="text" class="form-control" name="ip_address" placeholder="IP Address" id="ip_address">
										
													
										<?php echo getControllerIPAddressType();?>
										<?php echo getSitesSelect($current=null);?>
										
										<input type="checkbox" name="OPC" class="form-control" value="OPC" onclick="OPCFunction()" id="check" style="margin:0;">									
										<span style="font-size:13px;">OPC UA Enable</span>
										<input type="text" class="form-control" name="schemaPrefix" placeholder="Prefix" id="schemaPrefix" style="display:none; width:100px;">
										<label id="tag" style="display:none;">TagName</label>
										<input type="text" class="form-control" name="schemaSuffix" placeholder="Suffix" id="schemaSuffix" style="display:none; width:100px;">
										<button type="submit" class="btn btn-default" style="margin-left:10px;">Submit</button>
											
									</div>
								</form>
								
								<?php 
								
										}
										
								?>
								
								<br>
								
          
								<table class="table table-striped table-bordered" style="margin-top:20px;border-collapse:collapse;">
								  <thead>
									<tr>
									  <th style="width:25%;">Controller Name</th>
									  <th style="width:30%;">Communication Address</th>
									  <th style="width:25%;">Site Name</th>
									  <th style="width:20%;">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$controllers = getAllSiteControllers(); 

										foreach ($controllers as $controller) {
											
											if(!isset($_GET['id'])){

											echo '<tr>
												  <td>'.$controller['name'].'</td>
												  <td>'.$controller['address_type'].' <button type="button" class="btn user-consult" style="float:right;" data-toggle="modal" data-target="#IPv4" data-id="'.$controller['controller_id'].'" data-type = "'.$controller['address_type'].'"><b>...</b></button></td>
												  <td>'.$controller['site_name'].'</td>
												  <td><a href="index.php?ctr=1&id='.$controller['controller_id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a><a href="process/delete_record.php?type=sc&id='.$controller['controller_id'].'" onclick="return deleteItem()" style="padding-left:15px;">
												  <span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span></a>';
												  if($controller['address_type'] == 'Modbus' || $controller['address_type'] == 'COM') {
													  echo '
													  <a href="process/export_ini.php?id='.$controller['controller_id'].'" style="padding-left:15px;">
														<span class="glyphicon glyphicon-export"></span>
														</a> ';
												  } else {
													  echo '
													  <a href="process/export_ini_alt.php?id='.$controller['controller_id'].'" style="padding-left:15px;">
														<span class="glyphicon glyphicon-export"></span>
														</a> ';
												  }
												  echo '
													</td>
												</tr>';										
											} else {
												echo '<tr>';
												if($controller['controller_id'] == $_GET['id']) {
													echo '<form method="post" action="process/update_controller.php">
													<input type="hidden" name="id" value="'.$_GET['id'].'">
													<input name="account_id" value="'.$_SESSION['accountID'].'" type="hidden">
													<input type="hidden" name="created_by" value="'.$_SESSION['userID'].'" >
													<input name="active" value="1" type="hidden">
													<input name="status" value="Online" type="hidden">

													<td><input type="text" class="form-control" name="name" value="'.getControllerName($_GET['id']).'"></td>
													<td><select class="form-control" name="address_type" id="address_type1" style="width:45%;float:left;" >
														<option value="">--Type--</option>
														<option value="#myModal1">IPv4</option>
														<option value="#myModal2">IPv6</option>
														<option value="#myModal3">Serial Communications</option>
														<option value="#myModal4">URL</option>
														</select></td>
													<td>'.$controller['site_name'].'</td>
													<input type="hidden" name="site_id" value="'.$controller['site_id'].'">
													<td><button type="submit" class="btn btn-default">Submit</button>
													<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button></td></tr>
													</form>';
													
												}else{
													echo '
														<td>'.$controller['name'].'</td>
														<td>'.$controller['address_type'].' - '.$controller['ip_address'].'</td>
														<td>'.$controller['site_name'].'</td>
														<td></td>
													';
													
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
								
								<div id="myModal1" class="modal fade" role="dialog">
								  <div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h2 class="modal-title">IPv4</h2>
									  </div>
									  <div class="modal-body info">
										<form method="post" onSubmit="return confirmV4Submit()"  action="process/update_attributes.php">
											<div class="form-group">
												<?php $attr = getAttributes($_GET['id'],'IPv4');?>
												<label for="ip_address">IP Address</label>
												<input type="text" class="form-control" name="ip_address" id="ipAddressv4" value="<?php echo $attr['ip_address']; ?>" >		
												<br>
												<label for="port">Port Number</label>
												<input type="text" class="form-control" name="port" id="portNumberv4" value="<?php echo $attr['port'];  ?>">																					
											 </div>
											 <input type="hidden" name="address_type" value="IPv4">
											 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
											 <div class="form-group">
												<button type="submit" class="btn btn-default">Submit</button>
											</div>
										</form>
									  </div>
									  <div class="modal-footer addHEIGHT">
									  </div>
									</div>
								  </div>
								</div>
								
								<div id="myModal2" class="modal fade" role="dialog">
								  <div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h2 class="modal-title">IPv6</h2>
									  </div>
									  <div class="modal-body info">
										<form method="post"  onSubmit="return confirmV6Submit()"  action="process/update_attributes.php">
											<div class="form-group">
												<?php $attr = getAttributes($_GET['id'],'IPv6');?>
												<label for="ip_address">IP Address</label>
												<input type="text" class="form-control" name="ip_address" id="ipAddressv6" value="<?php echo $attr['ip_address']; ?>" >		
												<br>
												<label for="port">Port Number</label>
												<input type="text" class="form-control" name="port" id="portNumberv6" value="<?php echo $attr['port'];  ?>">												
											 </div>
											 <input type="hidden" name="address_type" value="IPv6">
											 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
											 <div class="form-group">
												<button type="submit" class="btn btn-default">Submit</button>
											</div>
										</form>
									  </div>
									  <div class="modal-footer addHEIGHT">
									  </div>
									</div>
								  </div>
								</div>
								
								<div id="myModal3" class="modal fade" role="dialog">
								  <div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h2 class="modal-title">Serial Communications</h2>
									  </div>
									  <div class="modal-body info">
										<form method="post" onSubmit="return confirmSerialSubmit()" action="process/update_attributes.php">
											<?php $attr = getAttributes($_GET['id'],'Modbus');?>
											
											<?php if($attr['address_type'] == 'Modbus') { ?>
												<div class="form-group">
													<div class="radio">
													  <label style="font-size:19px;"><input type="radio" name="address_type" value="Modbus" checked="checked">ModbusRTU / RS485</label>
													</div>
													<div class="radio">
													  <label style="font-size:19px;"><input type="radio" name="address_type" value="COM">Serial / RS232</label>
													</div>
												</div>
											<?php } else { ?>
												<div class="form-group">
												<div class="radio">
												  <label style="font-size:19px;"><input type="radio" name="address_type" value="Modbus">ModbusRTU / RS485</label>
												</div>
												<div class="radio">
												  <label style="font-size:19px;"><input type="radio" name="address_type" value="COM" checked="checked">Serial / RS232</label>
												</div>
											</div>
											<?php } ?>
											<div class="form-group">
												<label for="ip_address">Modbus ID</label>
												<input type="text" class="form-control" name="ip_address" id="modbusID" value="<?php echo $attr['ip_address']; ?>" >														
											</div>
											<div class="form-group">
												<label for="port">Port Number</label>
												<input type="text" class="form-control" name="port" id="modbusPort" value="<?php echo $attr['port']; ?>" >														
											</div>
											<div class="form-group">
												<label for="baudRate">Baud Rate</label>
												<input type="text" class="form-control" name="baudRate" id="baudRate" value="<?php echo $attr['baud_rate'];  ?>">												
											 </div>
											 <div class="form-group">
												<label for="parity">Parity</label>
												<input type="text" class="form-control" name="parity" id="parity" value="<?php echo $attr['parity'];  ?>">												
											 </div>
											 <div class="form-group">
												<label for="parity">Data Bits</label>
												<input type="text" class="form-control" name="dataBits" id="dataBits" value="<?php echo $attr['data_bits'];  ?>">												
											 </div>
											 <div class="form-group">
												<label for="stopBits">Stop Bits</label>
												<input type="text" class="form-control" name="stopBits" id="stopBits" value="<?php echo $attr['stop_bits'];  ?>">												
											 </div>
											 <div class="form-group">
												<label for="flowControl">Flow Control</label>
												<input type="text" class="form-control" name="flowControl" id="flowControl" value="<?php echo $attr['flow_control'];  ?>">												
											 </div>
											 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
											 <div class="form-group">
												<button type="submit" class="btn btn-default">Submit</button>
											</div>
										</form>
									  </div>
									  <div class="modal-footer addHEIGHT">
									  </div>
									</div>
								  </div>
								</div>
								
								<div id="myModal4" class="modal fade" role="dialog">
								  <div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h2 class="modal-title">URL</h2>
									  </div>
									  <div class="modal-body info">
										<form method="post" onSubmit="return confirmURLSubmit()" action="process/update_attributes.php">
										<?php $attr = getAttributes($_GET['id'],'URL');?>
											<div class="form-group">
												<label for="ip_address">URL Address</label>
												<input type="text" class="form-control" name="ip_address" id="ipAddressURL" value="<?php echo $attr['ip_address']; ?>" >		
												<br>
												<label for="port">Port Number</label>
												<input type="text" class="form-control" name="port" id="portNumberURL" value="<?php echo $attr['port'];  ?>">												
											 </div>
											 <input type="hidden" name="address_type" value="URL">
											 <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>" >
											 <div class="form-group">
												<button type="submit" class="btn btn-default">Submit</button>
											</div>
										</form>
									  </div>
									  <div class="modal-footer addHEIGHT">
									  </div>
									</div>
								  </div>
								</div>
								
								
								<div id="IPv4" class="modal fade" role="dialog" style="display:none;">
								  <div class="modal-dialog">
									<!-- Modal content-->
									<div class="modal-content">
									  <div class="modal-header">
										<button type="button" class="close" data-dismiss="modal">×</button>
										<h2 class="modal-title"></h2>
									  </div>
									  <div class="modal-body info">
										<form method="post" action="process/update_attributes.php">
											<div class="form-group">
												<label for="ip_address" class="labelAddress"></label>
												<div class="ip_addressRO"></div>	
												<br>
												<label for="port" class="labelPort"></label>
												<div class="portRO"></div>
												<br class="hiddenBr">
												<div class="serialCommunications">													
												</div>
											 </div>
											 <input type="hidden" name="address_type" value="IPv4">										 
										</form>
									  </div>
									  <div class="modal-footer addHEIGHT">
									  </div>
									</div>
								  </div>
								</div>
								
								
						
						<?php } ?>
						
						<?php if (isset($_GET['tb']) ) { ?>
						
								<h1 class="page-header">Trash Bin</h1>

								<h3>Sites</h3>
								<table class="table table-striped table-bordered">
								  <thead>
									<tr>
									  <th class="noOrder">Site Name</th>
									  <th class="noOrder" style="width:50%;">Action</th>
									</tr>
								  </thead>
								  <tbody>


									<?php
									
										
										$sites = getAllSiteNames(); 

										foreach ($sites as $site) {

											echo '<tr>
												  <td>'.$site['name'].'</td>
												  <td><a href="process/restore_record.php?type=s&id='.$site['id'].'" onclick="return deleteItem()">Restore</td>
												</tr>';
										}

									
									?>
          
								   </tbody>
								</table>
								
								<h3>Controllers</h3>
								<table class="table table-striped table-bordered">
								  <thead>
									<tr>
									  <th class="noOrder">Controller Name</th>
									  <th class="noOrder" style="width:50%;">Action</th>
									</tr>
								  </thead>
								  <tbody>


									<?php
									
										
										$controllers = getAllControllers(); 

										foreach ($controllers as $controller) {

											echo '<tr>
												  <td>'.$controller['name'].'</td>
												  <td><a href="process/restore_record.php?type=sc&id='.$controller['_id'].'" onclick="return deleteItem()">Restore</td>
												</tr>';
										}

									
									?>
          
								   </tbody>
								</table>
								
								<h3>Objects</h3>
								<table class="table table-striped table-bordered">
								  <thead>
									<tr>
									  <th class="noOrder">Object Name</th>
									  <th class="noOrder" style="width:50%;">Action</th>
									</tr>
								  </thead>
								  <tbody>


									<?php
									
										
										$objects = getAllControllerObjects(); 

										foreach ($objects as $object) {

											echo '<tr>
												  <td>'.$object['name'].'</td>
												  <td><a href="process/restore_record.php?type=co&id='.$object['id'].'" onclick="return deleteItem()">Restore</td>
												</tr>';
										}

									
									?>
          
								   </tbody>
								</table>	
		  
								<h3>Devices</h3>
								<table class="table table-striped table-bordered">
								  <thead>
									<tr>
									  <th class="noOrder">Device Name</th>
									  <th class="noOrder" style="width:50%;">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 
								   
								   
										$devices = getAllObjectDevices(); 

										foreach ($devices as $device) {

											echo '<tr>
												  <td>'.$device['name'].'</td>
												  <td><a href="process/restore_record.php?type=od&id='.$device['id'].'" onclick="return deleteItem()">Restore</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
                            
							
								<a href="process/restore_record.php?type=ra" class="btn btn-primary btn-lg pull-right" onclick="return deleteItem()">Restore All</a>
								<a href="process/restore_record.php?type=da" class="btn btn-primary btn-lg pull-right" onclick="return deleteItem()" style="margin-right:10px;">Delete All</a>
						
						<?php } ?>
			 
          				<?php if (isset($_GET['nns']) ) { ?>
          
          						<h1 class="page-header">New Site - <?php echo getAccountName($_SESSION['accountID']); ?></h1>
			 
								<div class="well">
									<form method="post" action="process/add_site.php">
									<input name="account_id" value="<?php echo $_SESSION['accountID']; ?>" type="hidden">
									<input name="created_by" value="<?php echo $_SESSION['userID']; ?>" type="hidden">
									<input name="active" value="1" type="hidden">
									<input name="status" value="Online" type="hidden">
											
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Site Name</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="name"></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Latitude</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="latitude"></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Longitude</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="longitude"></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Address Type</label></div>
												<div class="col-sm-9"><?php echo getSelectIPAddressType();?></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">IP Address</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="ip_address"></div>
											</div><br><br><br>
											
										 <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
			 					</div>
                            
                            
                            <?php } ?>
							
							
						<?php if (isset($_GET['us']) ) { ?>
          
          						<h1 class="page-header">Update Site - <?php echo getSiteName($_GET['id']); ?></h1>
			 
								<div class="well">
									<form method="post" action="process/update_site.php">
									<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
									<input name="account_id" value="<?php echo $_SESSION['accountID']; ?>" type="hidden">
									<input name="created_by" value="<?php echo $_SESSION['userID']; ?>" type="hidden">
									<input name="active" value="1" type="hidden">
									<input name="status" value="Online" type="hidden">
											
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Address Type</label></div>
												<div class="col-sm-9"><?php echo getIPAddressType();?></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">IP Address</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="ip_address" value="<?php echo getSiteIP($_GET['id']); ?>"></div>
											</div><br><br><br>
											

										 <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
			 					</div>
                            
                            
                            <?php } ?>
							
							<?php if (isset($_GET['uc']) ) { ?>
          
          						<h1 class="page-header">Update Controller - <?php echo getControllerName($_GET['id']); ?></h1>
			 
								<div class="well">
									<form method="post" action="process/update_controller.php">
									<input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
									<input name="account_id" value="<?php echo $_SESSION['accountID']; ?>" type="hidden">
									<input name="created_by" value="<?php echo $_SESSION['userID']; ?>" type="hidden">
									<input name="active" value="1" type="hidden">
									<input name="status" value="Online" type="hidden">
									
											<div class="form-group">
												<div class="col-sm-3"><label for="name">Name</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="name" value="<?php echo getControllerName($_GET['id']); ?>"></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Address Type</label></div>
												<div class="col-sm-9"><?php echo getControllerIPAddressType();?></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">IP Address</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="ip_address" value="<?php echo getControllerIP($_GET['id']); ?>"></div>
											</div><br><br><br>
											

										 <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
			 					</div>
                            
                            
                            <?php } ?>
			 
          				<?php if (isset($_GET['acc']) ) { ?>
          
          						<h1 class="page-header">New Account</h1>
			 
								<div class="well">
									<form method="post" action="process/add_account.php">
											
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Account Name</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="account_name"></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Account Number</label></div>
												<div class="col-sm-9"><input type="text" class="form-control" name="account_num"></div>
											</div><br><br><br>
											<div class="form-group">	
												<div class="col-sm-3"><label for="email">Super Account</label></div>
												<div class="col-sm-9"><?php echo getSuperAccountSelect();?></div>
											</div><br><br><br>

										 <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
			 					</div>
                            
                            
                            <?php } ?>
                            
                            <?php if (isset($_GET['ob'])) { 	 ?>
                            
                            	<h1 class="page-header">Controller Object Linkages</h1>
                            	
                            	
                            	<?php
									
									echo '<div class="well">
									
										<h2><a href="index.php?s='.$_GET['s'].'">'.getSiteName($_GET['s']).'</a></h2>
										<h3>'.getControllerName($_GET['c']).'</h3>
										<h4>'.getObjectName($_GET['ob']).'</h4>
										
										</div>';
			
								?>
                           	
                           	<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Device Name</th>
									  <th>Links To</th>
									  <th>Links From</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 


                            		$devs = getObjectDevices($_GET['ob']);
	
									//echo "<pre>";
									//print_r($devs);
	
										foreach ($devs as $dv) {

											echo '<tr>
												  <td>'.$dv['name'].'</td>
												  <td>'.checkLinksTo($dv['objDevID'],$linkTypeArray = null).'</td>
												  <td>'.checkLinksFrom($dv['objDevID'],$linkTypeArray = null).'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
                            	
                            <?php } ?>
                            
                            <?php if (isset($_GET['s']) && !isset($_GET['ob'])) { 	 ?>
          
          						<h1 class="page-header" >Visual Configurator</h1>
          						
									<div class="btn-group" style="margin-bottom:20px;">
										<a href="#" class="btn btn-primary" id="add_controller">Add Controller</a>
										<a href="#" class="btn btn-primary" id="add_object">Add Object</a>
										<a href="#" class="btn btn-primary" id="add_device">Add Device</a>
										<a href="xml.php?s=<?php echo $_GET['s']; ?>" class="btn btn-primary" target="_blank">View XML</a>
										
									</div>
								
								
								
         						<?php if ($_SESSION['linkage_start'] == 1) { ?>
         						
								 	<a href="index.php?s=<?php echo $_SESSION['link_to']['site']; ?>&c=<?php echo $_SESSION['link_to']['ctr']; ?>&ob=<?php echo $_SESSION['link_to']['obj']; ?>" class="list-group-item list-group-item-danger pull-right">You have a LINKAGE started on <?php echo $_SESSION['link_to']['tag']; ?> tag</a>
								 	
								<?php } ?>
          						
          						 <?php echo getSitesSelectAlt($_GET['s']);?>
								 
								 <br>
          						
          						<!-- <form class="form-inline" method="post" action="process/update_site.php" style="margin-top: 20px;margin-bottom:20px;">
          								<input type="hidden" name="id" value="<?php //echo $_GET['s']; ?>">
									  <div class="form-group">
										<label class="sr-only" for="ipaddress">Address</label>
										<input type="text" class="form-control" name="ip_address" placeholder="IP Address" value="<?php //echo getSiteIP($_GET['s']); ?>" size="50">
										
										<select name="address_type" class="form-control">
									  <option>IPv4</option>
									  <option>IPv6</option>
									 
									  <option>URL</option>

									</select>
									
										
									  </div>
	
									  <button type="submit" class="btn btn-default">Update</button>
									</form> -->
									
									
									<?php 
											$st = getSiteName($_GET['s']);

											echo '<li class="list-group-item list-group-item-danger" style="font-size:18px;">'.$st.'';
											echo '<a href="process/delete_record.php?type=s&id='.$_GET['s'].'" onclick="return deleteItem()" style="padding-left:15px;" class="pull-right">
												  <span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span></a>';											
									?>
									
          						<ul class="siteconfig">
									<?php 

											$ctrs = getSiteControllers($_GET['s']); 
											

											foreach ($ctrs as $ct) {

												echo '<li class="list-group-item list-group-item-success"">'.$ct['name'];
												echo '<a href="process/delete_record.php?type=sc&id='.$ct['id'].'" onclick="return deleteItem()" style="padding-left:15px;" class="pull-right">
																						<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
																						</a>';
												

													$ctob = getControllerObjects($ct['id']);


													if ($ctob) {

														echo '<ul>';

														foreach ($ctob as $ob) {
															$obdv = getObjectDevices($ob['contObjID']);

																if ($obdv) {
																	echo '<li class="list-group-item list-group-item-info">'.$ob['objname'].' ('.$ob['name'].') [<a href="index.php?s='.$_GET['s'].'&c='.$ct['id'].'&ob='.$ob['contObjID'].'">view linkages</a>]';
																} else {
																	echo '<li class="list-group-item list-group-item-info">'.$ob['objname'].' ('.$ob['name'].')';
																}
																	echo '<a href="process/delete_record.php?type=co&id='.$ob['contObjID'].'" onclick="return deleteItem()" style="padding-left:15px;" class="pull-right">
																								<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
																								</a>';

																$obdv = getObjectDevices($ob['contObjID']);

																if ($obdv) {

																	echo '<ul>';

																	foreach ($obdv as $dv) {
																		
																		$addclass_1 = '';
																		$addclass_2 = '';
																		$addclass_3 = '';
																		$addclass_4 = '';
																		$addclass_5 = '';	


																		echo '<li id="'.$dv['objDevID'].'" class="list-group-item list-group-item-warning">'.$dv['name'].'';
																				echo '<a href="process/delete_record.php?type=od&id='.$dv['objDevID'].'" onclick="return deleteItem()" style="padding-left:15px;" class="pull-right">
																						<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
																						</a>';
																				echo '<ul>';
																		
																				//if linked fa-link
																		
																				if ($_SESSION['link_to']['uid'] == $dv['objDevID'] ) {
																					
																					$num = $_SESSION['link_to']['type'];
																				
																					if ($num == 1) {
																						$addclass_1 = ' class="list-group-item-danger" ';
																					}
																					if ($num == 2) {
																						$addclass_2 = ' class="list-group-item-danger" ';
																					}																
																					if ($num == 3) {
																						$addclass_3 = ' class="list-group-item-danger" ';
																					}																
																					if ($num == 4) {
																						$addclass_4 = ' class="list-group-item-danger" ';
																					}																
																					if ($num == 5) {
																						$addclass_5 = ' class="list-group-item-danger" ';
																					}	
																				} 																				
																		
																				if ($_GET['dev'] == $dv['objDevID'] ) {
																					
																					$num = $_GET['type'];
																				
																					if ($num == 1) {
																						$addclass_1 = ' class="list-group-item-danger" ';
																					}
																					if ($num == 2) {
																						$addclass_2 = ' class="list-group-item-danger" ';
																					}																
																					if ($num == 3) {
																						$addclass_3 = ' class="list-group-item-danger" ';
																					}																
																					if ($num == 4) {
																						$addclass_4 = ' class="list-group-item-danger" ';
																					}																
																					if ($num == 5) {
																						$addclass_5 = ' class="list-group-item-danger" ';
																					}	
																				} 
																		
																				echo '<li '.$addclass_1.'>Measurements <i class="fa '.checkLinkage(1,$dv['objDevID']).'" aria-hidden="true" data-site="'.$_GET['s'].'" data-uid="'.$dv['objDevID'].'" data-id="'.$dv['id'].'" data-ct="'.$ct['id'].'" data-obj="'.$ob['contObjID'].'" data-var="1" data-type="Measurement" data-label="('.$ct['name'].')  '.$ob['objname'].' - '.$dv['name'].'"></i></li>';

																				echo '<li '.$addclass_3.'>Triggers <i class="fa '.checkLinkage(3,$dv['objDevID']).'" aria-hidden="true" data-site="'.$_GET['s'].'" data-uid="'.$dv['objDevID'].'" data-id="'.$dv['id'].'" data-ct="'.$ct['id'].'" data-obj="'.$ob['contObjID'].'" data-var="3" data-type="Trigger" data-label="('.$ct['name'].')  '.$ob['objname'].' - '.$dv['name'].'"></i></li>';
																				
																				echo '<li '.$addclass_2.'>Executions <i class="fa '.checkLinkage(2,$dv['objDevID']).'" aria-hidden="true" data-site="'.$_GET['s'].'" data-uid="'.$dv['objDevID'].'" data-id="'.$dv['id'].'" data-ct="'.$ct['id'].'" data-obj="'.$ob['contObjID'].'" data-var="2" data-type="Execution" data-label="('.$ct['name'].')  '.$ob['objname'].' - '.$dv['name'].'"></i></li>';
																				
																				echo '<li '.$addclass_4.'>Notifications <i class="fa '.checkLinkage(4,$dv['objDevID']).'" aria-hidden="true" data-site="'.$_GET['s'].'" data-uid="'.$dv['objDevID'].'" data-id="'.$dv['id'].'" data-ct="'.$ct['id'].'" data-obj="'.$ob['contObjID'].'" data-var="4" data-type="Notification" data-label="('.$ct['name'].')  '.$ob['objname'].' - '.$dv['name'].'"></i></li>';

																				echo '<li '.$addclass_5.'>Functions <i class="fa '.checkLinkage(5,$dv['objDevID']).'" aria-hidden="true" data-site="'.$_GET['s'].'" data-uid="'.$dv['objDevID'].'" data-id="'.$dv['id'].'" data-ct="'.$ct['id'].'" data-obj="'.$ob['contObjID'].'" data-var="5" data-type="Function" data-label="('.$ct['name'].')  '.$ob['objname'].' - '.$dv['name'].'"></i></li>';
																		
																		echo '</ul>';

																		echo '</li>';

																	}

																	echo '</ul>';

																}



															echo '</li>';
														}


														echo '</ul>';

													}




												echo '</li>';
											}

											
										?>
								</ul>
         						</li>
         						
         						<div id="add_link_modal" title="Manage Linkages" style="display: none;padding:20px;">
         						
									<div id="link_header" class="well"></div>
									
									<?php if ($_SESSION['linkage_start'] == 1) { ?>

										<h4>Choose the slot to link FROM this <span id="link_dev_type"></span>:</h4>

									<?php } else { ?>

										<h4>Choose the slot to link TO this <span id="link_dev_type"></span>:</h4>

									<?php } ?> 
									
									
									
									
									
										<div id="link_config" style="overflow-x:auto; width:auto;"></div>
									
									
<?php /*?>									<!--add tag names-->  <!--use broken link if no linkages-->
									
										<div class="col-sm-3"><p class="text-danger">Group 1</p>
										
											<?php 		for ($i=1; $i<=16; $i++) {

															echo $i.' <input type="checkbox"></br>';
														}?>
										
										</div>

									<div class="col-sm-3"><p class="text-danger">Group 2</p>
										
											<?php 		for ($i=1; $i<=16; $i++) {

															echo $i.' <input type="checkbox"></br>';
														}?>
										
										</div>
										
										
									<div class="col-sm-3"><p class="text-danger">Group 3</p>
										
											<?php 		for ($i=1; $i<=16; $i++) {

															echo $i.' <input type="checkbox"></br>';
														}?>
										
										</div>
										
										
										<div class="col-sm-3"><p class="text-danger">Group 4</p>
										
											<?php 		for ($i=1; $i<=16; $i++) {

															echo $i.' <input type="checkbox"></br>';
														}?>
										
										</div><?php */?>
										
										
										
									
									
									
								</div>
          						
      								
								<div id="add_con_modal" title="Add Controller" style="display: none;padding:20px;">
									
									<h4>Add a Controller to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form class="form" method="post" action="process/add_site_controller.php">
 										<input type="hidden" name="site_id" value="<?php echo $_GET['s']; ?>">
  										<div class="form-group">
  											<input type="text" class="form-control" name="name" placeholder="Name" ><br>
											<input type="text" class="form-control" name="ip_address" placeholder="IP Address" ><br>
							     		  <select name="address_type" class="form-control">
											  <option>IPv4</option>
											  <option>IPv6</option>
											  <option>Modbus</option>
											  <option>Com</option>
											  <option>URL</option>
											</select><br>

								     		<input type="text" class="form-control" name="parameter_id" placeholder="Object Parameter ID" ><br>
											<input type="checkbox" name="OPC" class="form-control" value="OPC" onclick="OPCFunction()" id="check" style="margin:0;width:15px;height:13px;display:inline;">									
											<span>OPC UA Enable</span> <br>
											<input type="text" class="form-control" name="schemaPrefix" placeholder="Prefix" id="schemaPrefix" style="display:none; margin-top:10px; width:40%;">
											<label id="tag" style="display:none;">TagName</label>
											<input type="text" class="form-control" name="schemaSuffix" placeholder="Suffix" id="schemaSuffix" style="display:none; margin-top:10px; width:40%;">
									     </div>
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>



								<div id="add_obj_modal" title="Add Object" style="display: none;padding:20px;">
									
									<h4>Add an Object to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form class="form" method="post" action="process/add_controller_object.php">
 										<input type="hidden" name="site_id" value="<?php echo $_GET['s']; ?>">
  										<div class="form-group">
												
											<select name="controller_id" class="form-control">
											<option value="">--Choose Controller--</option>
											<?php 
													   
													$ctrs = getSiteControllers($_GET['s']); 

													foreach ($ctrs as $ct) {
														echo '<option value="'.$ct['id'].'">'.$ct['name'].' - '.$ct['ip_address'].'</option>';
													}
			
											?>
							     			</select>
							     			
									     </div>
									     
									     <div class="form-group">
							     			
							     			<select name="object_id" class="form-control">
											<option value="">--Choose Object Type--</option>
											<?php 
													   
													$objs = getObjects(); 

													foreach ($objs as $ob) {
														echo '<option value="'.$ob['id'].'">'.$ob['name'].'</option>';
													}
			
											?>
							     			</select>
								     
									     </div>
									     
									     <div class="form-group">
  											<input type="text" class="form-control" name="name" placeholder="Name" ><br>
									     </div>
									     
									    
									     
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
	
         					
         					
         						<div id="add_dev_modal" title="Add Device" style="display: none;padding:20px;">
									
									<h4>Add a Device to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form class="form" method="post" action="process/add_object_device.php">
									<input type="hidden" name="site_id" value="<?php echo $_GET['s']; ?>">
									
										<div class="form-group">
											<select name="controller_id" id="controllerdevice" class="form-control">
											<option value="">--Choose Controller--</option>
											<?php 

													$ctrs = getSiteControllers($_GET['s']); 

													foreach ($ctrs as $ct) {
														echo '<option value="'.$ct['id'].'">'.$ct['name'].' - '.$ct['ip_address'].'</option>';
													}

											?>
											</select>
										</div>
						     			
						     			<div class="form-group" id="objsel" style="display: none;">
											<div id="controllerObjectSelect"></div>
										</div>
						     			
						     			<div class="form-group" id="objdevsel" style="display: none;">
											<?php echo getDeviceSelect($current = null); ?><br>

											
											<select name="address_type" class="form-control">
													  <option>IPv4</option>
													  <option>IPv6</option>
													  <option>Modbus</option>
													  <option>Com</option>
													  <option>URL</option>

													</select>
											<br>
											
										<label class="sr-only" for="ipaddress">Address</label>
										<input type="text" class="form-control" name="ip_address" placeholder="IP Address" size="50">

										
											

										</div>
						     			
						     			
							     			
									     <div class="form-group" id="devicebtn"  style="display: none;">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
         					
          					<?php } ?>
          					
          					
          					
          					<?php if (isset($_GET['f'])) { ?>
          
          						<h1 class="page-header">File Manager</h1>
          						
          						<div id="elfinder"></div>
							
							<?php } ?>
							
							

							<?php  if ($_GET['m'] == 1) { ?>  

								<h2 class="sub-header">Site Map</h2>

								<div id="map"></div>

							<?php  } ?>  
							
							
							
							<?php  if ($_GET['mf'] == 1) { ?>  

								<h2 class="sub-header">Manufacturers <i data-toggle="tooltip" title="You can add Manufacturers to this list for later assignment to devices." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_manufacturer.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Manufacturer Name" >
								     		<input type="url" class="form-control" name="website" placeholder="Website" >
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Manufacturer Name</th>
									  <th>Website</th>
									  <th># Devices</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getManufacturers(); 
								
										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td><a href="index.php?dv=1&mfr='.$m['id'].'">'.$m['name'].'</a></td>
												  <td><a href="'.$m['website'].'" target="_blank">'.$m['website'].'</a></td>
												  <td>'.getMfrDeviceCount($m['id']).'</td>
												  <td>
												  <a href="index.php?mf=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=mf&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="9" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="website" value="'.$m['website'].'"></td>
													<td>'.getMfrDeviceCount($m['id']).'</a></td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													  <td>'.$m['website'].'</td>
													  <td>'.getMfrDeviceCount($m['id']).'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}
	

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
							
							
							<?php  if ($_GET['oj'] == 1) { ?>  

								<h1 class="page-header">Objects <i data-toggle="tooltip" title="Objects are generic components to which you can assign one or  more devices.  You can also assign a unique ID." class="fa fa-question-circle" aria-hidden="true"></i></h1>
								
								<?php
								if (($_GET['h'] == 1)) {
									
								?>
									<a href="index.php?oj=1">+ New Object</a>
								
								<?php
								} else {
								?>
									<a href="index.php?oj=1&h=1">+ New Object</a>
								<?php
								}
										if ($_GET['h'] == 1) {
									?>
										<form class="form-inline" method="post" action="process/add_object.php" style="margin-top:20px;">
											<div class="form-group">
												<input type="text" class="form-control" name="name" placeholder="Object Name" >
												<!--<input type="text" class="form-control" name="objectID" placeholder="Object ID" >-->
												<button type="submit" class="btn btn-default">Submit</button>
											</div>
										</form>
									
									<?php
										}
									?>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Object Name</th>
									  <th>Configure</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$obj = getObjects(); 
										/*
										foreach ($obj as $o) {

											echo '<tr '.$class.'>
												  <td>'.$o['name'].'</td>
												  <td>'.$o['objectID'].'</td>
												   <td><a href="index.php?pr=1&object='.$o['id'].'">Click here</a></td>
												</tr>';
										}*/
										foreach ($obj as $o) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$o['name'].'</td>
												  <td><a href="index.php?pr=1&object='.$o['id'].'">Click here</a></td>
												  <td>
												  <a href="index.php?oj=1&id='.$o['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
												  </a>
												  <a href="process/delete_record.php?type=oj&id='.$o['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>
												  </td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($o['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="10" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$o['name'].'"></td></td>
													<td><a href="index.php?pr=1&object='.$o['id'].'">Click here</a></td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$o['name'].'</td>
		
													  <td><a href="index.php?pr=1&object='.$o['id'].'">Click here</a></td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
							
							
							
							<?php  if ($_GET['dv'] == 1) { ?>  

								<h1 class="page-header">Devices <i data-toggle="tooltip" title="Devices are manufacturer specific components that you can add to an Object." class="fa fa-question-circle" aria-hidden="true"></i></h1>
									
									<?php if ($_GET['mfr']) { 
										
											$mfr = getManufacturerData($_GET['mfr']);

										 ?>
										 
									<div class="well info" style="margin-bottom:20px;">You are currently viewing devices manufactured by <strong><?php echo $mfr[0]['name']; ?></strong>. <a href="index.php?dv=1">Click here to view all.</a>  </div>
									
									<?php } ?>
									<?php
										if ($_GET['h'] == 1) {
									?>
									
											<a href="index.php?dv=1">+ New Device</a>
									
									<?php
										} else {
									?>
											<a href="index.php?dv=1&h=1">+ New Device</a>
									<?php
										}
										if ($_GET['h'] == 1) {
									?>
										<form class="form-inline" method="post" action="process/add_device.php" style="margin-top:20px;">
											<div class="form-group">
												<input type="text" class="form-control" name="name" placeholder="Device Name" tabindex=1>
												<!--<input type="text" class="form-control" name="deviceID" placeholder="Device ID" > -->
												<?php echo  getMfrSelect($current = null); ?>
												<?php echo  getDeviceTypeSelect($current = null); ?>
												<button type="submit" class="btn btn-default">Submit</button>
											</div>
										</form>
									<?php
										}
									?>
									
								<table class="table table-striped table-bordered" id="example"  style="margin-top:20px;">
								  <thead style="">
									<tr>
									  <th>Device Name</th>
									  <th>Device ID</th>
									  <th class="select-filter">Manufacturer</th>
								  	  <th class="select-filter">Type</th>
									  <th>Configure</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$dev = getDevices(); 
										/*
										foreach ($dev as $d) {

											echo '<tr '.$class.'>
												  <td>'.$d['name'].'</td>
												  <td>'.$d['deviceID'].'</td>
												  <td>'.$d['mfrname'].'</td>
												  <td>'.$d['devtype'].'</td>
												  <td><a href="index.php?st=1&device='.$d['id'].'&cfn=1">Click here</a></td>
												</tr>';
										}*/
														  
										foreach ($dev as $d) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$d['name'].'</td>
												  <td>'.$d['deviceID'].'</td>
												  <td>'.$d['mfrname'].'</td>
												  <td>'.$d['devtype'].'</td>
												  <td><a href="index.php?st=1&device='.$d['id'].'&cfn=1">Click here</a></td>
												  <td>
												  <a href="index.php?dv=1&id='.$d['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=dv&id='.$d['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>													
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($d['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="11" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$d['name'].'"></td>
													<td><input type="text" class="form-control" name ="deviceID" value="'.$d['deviceID'].'"></td>
													<td>'.getMfrSelect($d['manufacturers_id']).'</td>
													<td>'.getDeviceTypeSelect($d['device_type_id']).'</td>
													<td><a href="index.php?st=1&device='.$d['id'].'&cfn=1">Click here</a></td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$d['name'].'</td>
													  <td>'.$d['deviceID'].'</td>
													  <td>'.$d['mfrname'].'</td>
													  <td>'.$d['devtype'].'</td>
													  <td><a href="index.php?st=1&device='.$d['id'].'&cfn=1">Click here</a></td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>	
								
								</table>
	
							<?php  } ?>  
							
			
							
							<?php  if ($_GET['vt'] == 1) { ?>  

								<h2 class="sub-header">Device Types <i data-toggle="tooltip" title="You can add Device Types to this list." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_device_type.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Device Type" >

											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Device Type</th>
										<th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getDeviceTypes(); 

										foreach ($mfg as $m) {
											
											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>
												  <a href="index.php?vt=1&id='.$m['id'].'">
												  	<span class="glyphicon glyphicon-pencil"></span>
												  </a>
												  <a href="process/delete_record.php?type=vt&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>													  
												  </td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="1" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}

										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  				
							
							
							<?php  if ($_GET['dt'] == 1) { ?>  

								<h2 class="sub-header">Data Types <i data-toggle="tooltip" title="You can add Data Types to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_data_type.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Data Type" >

											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Data Type</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getDataTypes(); 

										foreach ($mfg as $m) {
											
											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>
												  <a href="index.php?dt=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=dt&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>														
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="7" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
							
							<?php  if ($_GET['op'] == 1) { ?>  

								<h2 class="sub-header">Operands <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Operand" >
								     		<input type="text" class="form-control" name="operand_type_index" placeholder="Type Index" >
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Operand</th>
									  <th>Type Index</th>
									  <th>Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getOperands(); 

										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['operand_type_index'].'</td>
												  <td>
												  <a href="index.php?op=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=op&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>														
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="8" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="operand_type_index" value="'.$m['operand_type_index'].'"></td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													  <td>'.$m['operand_type_index'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
	
							
							<?php  if ($_GET['mr'] == 1) { ?>  

								<h2 class="sub-header">Measurement Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_measurement.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
											<input type="text" class="form-control" name="description" placeholder="Description" >
							     			<?php echo getDeviceTypeSelect($current = null); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th style="width:35%">Tag</th>
									  <th style="width:35%">Description</th>
									  <th style="width:15%">Device Type</th>
									  <th style="width:15%">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getMeasurements(); 

										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td title="'.$m['description'].'">'.$m['shortDescription'].'</td>
												  <td>'.$m['devname'].'</td>
												  <td>
												  <a href="index.php?mr=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=mr&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>														
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="2" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="description" value="'.$m['description'].'"></td>
													<td>'.getDeviceTypeSelect($m['device_type_id']).'</td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
														<td>'.$m['description'].'</td>
													  <td>'.$m['devname'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  						
	
							
							<?php  if ($_GET['ex'] == 1) { ?>  

								<h2 class="sub-header">Execution Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_execution.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
											<input type="text" class="form-control" name="description" placeholder="Description" >
							     			<?php echo getDeviceTypeSelect($current = null); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th style="width:35%">Tag</th>
									  <th style="width:35%">Description</th>
									  <th style="width:15%">Device Type</th>
									  <th style="width:15%">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getExecutions(); 

										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td title="'.$m['description'].'">'.$m['shortDescription'].'</td>
												  <td>'.$m['devname'].'</td>
												  <td>
												  <a href="index.php?ex=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=ex&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>													
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="3" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="description" value="'.$m['description'].'"></td>
													<td>'.getDeviceTypeSelect($m['device_type_id']).'</td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													<td>'.$m['description'].'</td>
													  <td>'.$m['devname'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  						

							
							
							
							<?php  if ($_GET['ns'] == 1) { ?>  

								<h2 class="sub-header">Notification Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_notification.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
											<input type="text" class="form-control" name="description" placeholder="Description" >
							     			<?php echo getDeviceTypeSelect($current = null); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th style="width:35%">Tag</th>
									  <th style="width:35%">Description</th>
									  <th style="width:15%">Device Type</th>
									  <th style="width:15%">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getNotifications(); 

										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td title="'.$m['description'].'">'.$m['shortDescription'].'</td>
												  <td>'.$m['devname'].'</td>
												  <td>
												  <a href="index.php?ns=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=ns&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>														
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="5" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="description" value="'.$m['description'].'"></td>
													<td>'.getDeviceTypeSelect($m['device_type_id']).'</td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													<td>'.$m['description'].'</td>
													  <td>'.$m['devname'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  	

							<?php  if ($_GET['tr'] == 1) { ?>  

								<h2 class="sub-header">Trigger Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_trigger.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
											<input type="text" class="form-control" name="description" placeholder="Description" >
							     			<?php echo getDeviceTypeSelect($current = null); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th style="width:35%">Tag</th>
									  <th style="width:35%">Description</th>
									  <th style="width:15%">Device Type</th>
									  <th style="width:15%">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getTriggers(); 

										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td title="'.$m['description'].'">'.$m['shortDescription'].'</td>
												  <td>'.$m['devname'].'</td>
												  <td>
												  <a href="index.php?tr=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=tr&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>													
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="4" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="description" value="'.$m['description'].'"></td>
													<td>'.getDeviceTypeSelect($m['device_type_id']).'</td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													<td>'.$m['description'].'</td>
													  <td>'.$m['devname'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  						
														
							<?php  if ($_GET['fs'] == 1) { ?>  

								<h2 class="sub-header">Function Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_function.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
											<input type="text" class="form-control" name="description" placeholder="Description" >
							     			<?php echo getDeviceTypeSelect($current = null); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th style="width:35%">Tag</th>
									  <th style="width:35%">Description</th>
									  <th style="width:15%">Device Type</th>
									  <th style="width:15%">Action</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getFunctions(); 

										foreach ($mfg as $m) {

											if(!isset($_GET['id'])){
												echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td title="'.$m['description'].'">'.$m['shortDescription'].'</td>
												  <td>'.$m['devname'].'</td>
												  <td>
												  <a href="index.php?fs=1&id='.$m['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a>
												  <a href="process/delete_record.php?type=fs&id='.$m['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
          											<span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span>
												  </a>														
													</td>
												</tr>';
											}else{
												echo '<tr '.$class.'>';
												if($m['id'] == $_GET['id']){
													echo '<form method="post" action="process/update.php">
													<input name="pt" value="6" type="hidden">
													<input name="id" value="'.$_GET['id'].'" type="hidden">
													<td><input type="text" class="form-control" name ="name" value="'.$m['name'].'"></td>
													<td><input type="text" class="form-control" name ="description" value="'.$m['description'].'"></td>
													<td>'.getDeviceTypeSelect($m['device_type_id']).'</td>
															<td>
												  			<button type="submit" class="btn btn-default">Submit</button>
															<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button>
															</td></form>';
												}else{
													echo '<td>'.$m['name'].'</td>
													<td>'.$m['description'].'</td>
													  <td>'.$m['devname'].'</td>
													  <td></td>';
												}
												echo '</tr>';
											}
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  					
																												
							
							<?php  if ($_GET['st'] == 1) { ?>  
								
								
								<?php $dev = getDeviceData($_GET['device']); ?>

								<h2 class="sub-header">Device Settings <i data-toggle="tooltip" title="Devices settings below are specific to the device type you are configuring.  If you don't see a setting that you need, you can add one below." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
								<div class="well info">You are currently configuring: <strong><?php echo $dev[0]['name']; ?></strong>, device type <strong><?php echo $dev[0]['devtype']; ?></strong>, manufactured by <strong><a href="index.php?dv=1&mfr=<?php echo $dev[0]['mfrid']; ?>"><?php echo $dev[0]['mfrname']; ?></a></strong>. The lists below contains options for a <strong><?php echo $dev[0]['devtype']; ?></strong> only! </div>
								
								
								<?php  if (!$_GET['smr']) { ?>
								<form class="form-inline" method="post" action="process/add_controller_type.php">
  										<div class="form-group">
											<input type="hidden" class="form-control" name="id" style="width:50px;" value="<?php echo ($_GET['device']); ?>" >
											<input type="hidden" class="form-control" id="dbconfig" style="width:50px;" value="<?php echo getNumConfigSelect($_GET['device']);  ?>" >
								     		<?php echo  getDevTypeSelect($_GET['device']);
											
											$ts = ControllerType($_GET['device']);
											
											foreach ($ts as $t) {
											if($t['controller_type'] == "Expanded") {
											?>
											
												<input type="number" min="4" class="form-control" id="num_config" name="num_config" style="width:70px;" value="<?php echo getNumConfigSelect($_GET['device']); ?>" >
											
											<?php } else { ?>
												
												<input type="number" class="form-control" id="num_config" name="num_config" style="width:50px;display:none;" value="<?php echo getNumConfigSelect($_GET['device']); ?>" >
											
											<?php  }}?>
											<button type="submit" class="btn btn-primary" onclick="return changeConfig()">Apply</button>
										</div>
									</form>
								<div class="btn-group" style="margin: 10px 0;">
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1&smr=1" class="btn btn-success devicesetup"><?php echo $dev[0]['name']; ?> Summary</a>
								</div>
			 					<br>
			 					<div style="margin-bottom: 20px;overflow:auto;overflow-y:hidden;white-space:nowrap;height:50px; width:100%;">
									
									
									<?php $dev2 = getNumConfig($_GET['device']);
										
										foreach ($dev2 as $d) {
											for ($i=1; $i<=$d['num_config']; $i++) {
									?>
																
									
												<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=<?php echo $i;?>" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == $i) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Configuration <?php echo $i;?></a>
										
										
									<?php
											}
										}
									?>
										
										
									
								</div>
								<?php  } else { ?>
								<div class="btn-group" style="margin-bottom: 10px;">
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1" class="btn btn-success devicesetup"><?php echo $dev[0]['name']; ?> Configuration</a>
								</div>
			 					<br>
			 					<div class="btn-group" style="margin-bottom: 20px;">
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 1) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Measurements</a>
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=2&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 2) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Triggers</a>	
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=3&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 3) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Executions</a>														
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=4&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 4) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Notifications</a>
	
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=5&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 5) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Functions</a>							
								</div>
								
								<?php  } ?>
									
									<p class="text-danger">You can have up to 16 settings on each Configuration! NOTE:  Triggers and Notifications MUST be balanced!</p>
									
							
								<!--standard configuration-->
								<?php  if (!$_GET['smr']) {  ?>  		
										
											<div class="row">

										<?php $cfn = $_GET['cfn']; ?>
										<?php $device = $_GET['device']; ?>
												
										<table class="table table-striped table-bordered table-hover" style="margin-top:20px;">
										  <thead>
											<tr>
											  <th class="noOrder">Reg #</th>
											  <th class="noOrder">Measurement <?php echo $cfn; ?> <a href="#" class="pull-right addSetting" style="color:#5cb85c;" data-cfg="<?php echo $_GET['cfn']; ?>" data-type="1" data-devtype="<?php echo $dev[0]['device_type_id']; ?>"><span class="glyphicon glyphicon-plus"></span></a></th>
											  <th class="noOrder">Trigger <?php echo $cfn; ?> <a href="#" class="pull-right addSetting" style="color:#5cb85c;" data-cfg="<?php echo $_GET['cfn']; ?>" data-type="3" data-devtype="<?php echo $dev[0]['device_type_id']; ?>"><span class="glyphicon glyphicon-plus"></span></a></th>
											  <th class="noOrder">Execution <?php echo $cfn; ?> <a href="#" class="pull-right addSetting" style="color:#5cb85c;" data-cfg="<?php echo $_GET['cfn']; ?>" data-type="2" data-devtype="<?php echo $dev[0]['device_type_id']; ?>"><span class="glyphicon glyphicon-plus"></span></a></th>					  
											  <th class="noOrder">Notification <?php echo $cfn; ?> <a href="#" class="pull-right addSetting" style="color:#5cb85c;" data-cfg="<?php echo $_GET['cfn']; ?>" data-type="4" data-devtype="<?php echo $dev[0]['device_type_id']; ?>"><span class="glyphicon glyphicon-plus"></span></a></th>
											  <th class="noOrder">Function <?php echo $cfn; ?> <a href="#" class="pull-right addSetting" style="color:#5cb85c;" data-cfg="<?php echo $_GET['cfn']; ?>" data-type="5" data-devtype="<?php echo $dev[0]['device_type_id']; ?>"><span class="glyphicon glyphicon-plus"></span></a></th>
											</tr>
										  </thead>
										  <tbody>
											<!--  /*
											  			for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$_GET['cfn'].'" data-id="'.$i.'" data-type="5" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>';
																	$cs = checkSetting(5,$_GET['device'],$i,$_GET['cfn']);
																	if($cs == NULL){
																		echo '<td></td>';
																	}else{
																		echo '<td>'.$cs.'<a href="process/delete_device_setting.php?type=f&d='.$_GET['device'].'&r='.$i.'&c='.$_GET['cfn'].'" onclick="return deleteItem()"><span class="glyphicon glyphicon-remove" style="color:#d9534f;"></span></a></td>';
																	}
																echo '</tr>';
														}--->
														   

											  <?php 
														

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr>';
															echo '<td>'.$i.'</td>';
															
															for($j = 1; $j <= 5; $j++){
																if ($j == 2) {
																	$j = 3;
																}
																else if ($j == 3) {
																	$j =2;
																}
																else {
																	$j = $j;
																}
												
																$cs = checkSetting($j,$device,$i,$cfn);
																//echo "<pre>";
																//print_r($cs);
																
																if ($j == 3) {
																	$j = 2;
																}
																else if ($j == 2) {
																	$j =3;
																}
																else {
																	$j = $j;
																}
																
																if ($j == 1) {
																	$type = 'measurement';
																} elseif ($j == 2) {
																	$type = 'trigger';
																} elseif ($j == 3) {
																	$type = 'execution';
																} elseif ($j == 4) {
																	$type = 'notification';
																} elseif ($j == 5) {
																	$type = 'function';
																}
																
				
																
																if($cs == NULL){
																	echo '<td></td>';
																}else{
																	$cst = checkStructure($cs['configID'],$type);
																	if($cst['name'] == 'STRUCT') {
																		echo '<td>'.$cs['name'].'<a href="process/delete_device_setting.php?t='.$type.'&id='.$cs['configID'].'" onclick="return deleteStructure()" class="pull-right editSetting"> <span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;margin-left:5px;"></span></a> <a href="process/edit_tag.php?t='.$j.'&id='.$cs['configID'].'" class="pull-right editSetting"> <span class="glyphicon glyphicon-pencil"></span></a></td>';
																	} else {
																		echo '<td>'.$cs['name'].'<a href="process/delete_device_setting.php?t='.$type.'&id='.$cs['configID'].'" onclick="return deleteItem()" class="pull-right editSetting"> <span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;margin-left:5px;"></span></a> <a href="process/edit_tag.php?t='.$j.'&id='.$cs['configID'].'" class="pull-right editSetting"> <span class="glyphicon glyphicon-pencil"></span></a></td>';
																	}
																}	
															}
																
															echo '</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>


		  
											  
											<div id="add_setting_modal" title="Device Parameters" style="display: none;padding:20px;">

												<h4>Add <span id="devTypeLabel"></span> - Config #<span id="cfnNumLabel"></span></h4>

												<form action="process/device_settings.php" method="post">
													<input name="device_id" type="hidden" value="<?php echo $_GET['device']; ?>">
													<input name="cfn" id="paramc" type="hidden" value="">
													<input name="type" id="paramt" type="hidden" value="">
												
													<div class="form-group">
														<label for="id">Tag</label>														
														<div id="tagTypeSelect"></div>
													 </div>

													 <div class="form-group">
													 	<label for="id">Units</label>														
														<input type="text" class="form-control" name="parameter_units" placeholder="Units" id="units">
													 </div>
													 
													 
													 <div class="form-group">
														<label for="id">Data Type</label>														
														<div id="dataTypeSelect"></div>
													 </div>
													 
													<div class="form-group">
													 	<label for="id">Data Address</label>														
														<input type="text" class="form-control" name="parameter_address" placeholder="Address" id="address">
													 </div>
													 
													 <div class="form-group">
													 	<label for="id">Operand Type</label>														
														<?php echo getOperatorSelect($current = null); ?>
													 </div>
													 
													 <div class="form-group">
														<label for="id">Structures</label>														
														<div id="structureSelect"></div>
													 </div>
													 
						

													<div class="form-group" style="text-align: center;">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>
											
											<?php if(isset($_SESSION['editArray'])){?>	
			 
											<div id="edit_setting_modal" title="Device Parameters" style="display: none;padding:20px;">

												<h4>Edit <?php echo $_SESSION['editArray']['edit_type']; ?></span> - Config #<?php echo $_SESSION['editArray']['edit_cfn']; ?></h4>

												<form action="process/update.php" method="post">
													<input name="id" type="hidden" value="<?php echo $_SESSION['editArray']['edit_configID']; ?>">
													<input name="pt" type="hidden" value="12">
													<input name="type" type="hidden" value="<?php echo $_SESSION['editArray']['edit_type']; ?>">
												
													<div class="form-group">
														<label for="id">Tag</label>														
														<?php echo getTagTypeSelect($_SESSION['editArray']['edit_type'],$_SESSION['editArray']['edit_deviceID'],$_SESSION['editArray']['edit_tag_id']); ?>
													 </div>
													 
													 <?php
													
														$datatype = getDataType($_SESSION['editArray']['edit_data_type_id']);
														
														
													 ?>
										
													
													 <div class="form-group noShowError">
													 	<label for="id">Units</label>
														<?php 
														if($datatype['name'] != "STRUCT") {
															?>
														<input type="text" class="form-control" name="parameter_units" placeholder="Units" value="<?php echo $_SESSION['editArray']['edit_parameter_units']; ?>" id="units2">
														<?php
														} else {
														?>
														<input type="text" class="form-control" name="parameter_units" disabled placeholder="Units" value="<?php echo $_SESSION['editArray']['edit_parameter_units']; ?>" id="units2">
														<?php
														}
														?>
													 </div>

													<div class="form-group noShowError">
													 	<label for="id">Data Type</label>														
														<?php echo getDataTypeSelectE($_SESSION['editArray']['edit_data_type_id'],$_SESSION['editArray']['edit_type']); ?>
													 </div>
													 
													<div class="form-group noShowError">
													 	<label for="id">Data Address</label>
															<?php 
														if($datatype['name'] != "STRUCT") {
															?>														
														<input type="text" class="form-control" name="parameter_address" placeholder="Address" value="<?php echo $_SESSION['editArray']['edit_parameter_address']; ?>" id="address2">
														<?php
														} else {
														?>
														<input type="text" class="form-control" disabled name="parameter_address" placeholder="Address" value="<?php echo $_SESSION['editArray']['edit_parameter_address']; ?>" id="address2">
														<?php
														}
														?>
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Operand Type</label>
														<?php 
														if($datatype['name'] != "STRUCT") {
															?>	
														<?php echo getOperatorSelectE($_SESSION['editArray']['edit_operator_id'],0); ?>
														<?php
														} else {
														?>
														<?php echo getOperatorSelectE($_SESSION['editArray']['edit_operator_id'],1); 
														
														}
														?>
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Structures</label>
														<?php 
														if($datatype['name'] != "STRUCT") {
															?>
														<?php echo getStructuresOnEdit($_SESSION['editArray']['edit_structure_id'],$_SESSION['editArray']['edit_type'],0); ?>	
														<?php
														} else {
														?>	
														<?php echo getStructuresOnEdit($_SESSION['editArray']['edit_structure_id'],$_SESSION['editArray']['edit_type'],1);
														}
														?>															
													 </div>
													 
													 <?php 
														if($datatype['name'] == "STRUCT") {
															?>
													 <div class="form-group noShowError">
													 	<label for="id">Members</label>
														<?php echo getMembers($_SESSION['editArray']['edit_configID']);  ?>
													</div>
													 <?php
													 
														}
													 ?>
						
													 <div class="form-group noShowError" style="text-align: center;">
														<button type="submit" class="btn btn-default">Submit</button>		
													</div>
												</form>

											</div>
												
											<?php }?>
<!--
											<div id="add_measurement_modal" title="Add Measurement" style="display: none;padding:20px;">

												<h4>Add Measurement</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>


													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>


											<div id="add_execution_modal" title="Add Execution" style="display: none;padding:20px;">

												<h4>Add Execution</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>

													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>



											<div id="add_trigger_modal" title="Add Trigger" style="display: none;padding:20px;">

												<h4>Add Trigger</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>

													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>


											<div id="add_notification_modal" title="Add Notification" style="display: none;padding:20px;">

												<h4>Add Notification</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>

													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>-->



									 </div>
											
								<?php  } else { ?>
								<!--END standard configuration--> 
											
								<!--summary view--> 
		  
										<?php $type = $_GET['cfn']; ?>
										<?php $device = $_GET['device']; ?>
												
		  
		  								<?php switch($type){
											   case 1:
											   		$header = 'Measurement';
											   		break;
											   case 2:
											   		$header = 'Trigger';
											   		break;
											   case 3:
											   		$header = 'Execution';
											   		break;
											   case 4:
											   		$header = 'Notification';
											   		break;
											   case 5:
											   		$header = 'Function';
											   		break;
											  }
											   
										?>
											   
	
					
										<table class="table table-striped table-bordered table-hover" style="margin-top:20px;">
										  <thead>
											<tr>
											  <th class="noOrder">Reg #</th>
											  <?php 
											  $dev2 = getNumConfig($_GET['device']);
										
												foreach ($dev2 as $d) {
											  
											  
											  
													for($i=1;$i<=$d['num_config'];$i++){ ?>
														<th class="noOrder"><?php echo $header.' '.$i; ?></th>
												<?php } }?>
											</tr>
										  </thead>
										  <tbody>

											  <?php 
														

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr>';
															echo '<td>'.$i.'</td>';
															
															$dev2 = getNumConfig($_GET['device']);
										
															foreach ($dev2 as $d) {
															
															
															for($j = 1; $j <= $d['num_config']; $j++){
																
																if ($type == 2) {
																	$type = 3;
																}
																else if ($type == 3) {
																	$type =2;
																}
																else {
																	$type = $type;
																}
																
																$cs = checkSetting($type,$_GET['device'],$i,$j);
																if($cs == NULL){
																	echo '<td></td>';
																}else{
																	echo '<td>'.$cs['name'].'</td>';
																}	
															}
															}
															echo '</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>	
										
																	
												<div id="add_setting_modal" title="Device Parameters" style="display: none;padding:20px;">

												<h4>Add <span id="devTypeLabel"></span> - Config #<span id="cfnNumLabel"></span></h4>

												<form action="process/device_settings.php" method="post">
													<input name="device_id" type="hidden" value="<?php echo $_GET['device']; ?>">
													<input name="regnum" id="paramn" type="hidden" value="">
													<input name="cfn" id="paramc" type="hidden" value="">
													<input name="type" id="paramt" type="hidden" value="">
													<input name="smr" type="hidden" value="1">
												
													<div class="form-group">
														<label for="id">Tag</label>														
														<div id="tagTypeSelect"></div>
													 </div>

													 <div class="form-group noShowError">
													 	<label for="id">Units</label>														
														<input type="text" class="form-control" name="parameter_units" placeholder="Units">
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Operand</label>														
														<?php echo getOperatorSelect(); ?>
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Data Type</label>														
														<?php echo getDataTypeSelect(); ?>
													 </div>

													 <div class="form-group noShowError">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>														
																																						
											
								<?php  } ?>
								<!--END summary view-->  
												
							<?php  } ?>  
							
							
							
							
							
							<?php  if ($_GET['pr'] == 1) { ?>  
								
								
								<h1 class="page-header">Object Parameters <i data-toggle="tooltip" title="Up to 8 parameters can be added to an Object." class="fa fa-question-circle" aria-hidden="true"></i></h1>
								<a href="index.php?pr=1&object=<?php echo $_GET['object']; ?>&h=1">+ New Parameter</a>
								
								<?php
										if (($_GET['h'] == 1)) {
								?>
								
								<form method="post" class="form-inline" action="process/add_parameter.php" style="margin-top:20px;">
									<div class="form-group">
										<input type="text" class="form-control" name="name" placeholder="Name">
										<input type="text" class="form-control" name="description" placeholder="Description">
										<input type="text" class="form-control" name="value" placeholder="Value">
										
										<input type="hidden" class="form-control" name="object_id" value= "<?php echo $_GET['object']; ?>">

										<button type="submit" class="btn btn-default">Submit</button>
											
									</div>
								</form>
								
								<?php 
								
										}
										
								?>
							
								
								<table class="table table-striped table-bordered sortable" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Parameter Name</th>
									  <th>Description</th>
									  <th>Value</th>
									  <th>Action</th>
									</tr>
								  </thead>

	
									  <tbody>
								  
									  <?php 

											$par = getParameters($_GET['object']); 

											foreach ($par as $p) {

												if(!isset($_GET['id'])){
													
												echo '<tr '.$class.'>
													  <td>'.$p['name'].'</td>
													  <td>'.$p['description'].'</td>
													  <td>'.$p['value'].'</td>
													  <td><a href="index.php?pr=1&object='.$_GET['object'].'&id='.$p['id'].'">
          											<span class="glyphicon glyphicon-pencil"></span>
													</a><a href="process/delete_record.php?type=opa&id='.$p['id'].'" onclick="return deleteItem()" style="padding-left:15px;">
												  <span class="glyphicon glyphicon-remove-sign" style="color:#d9534f;"></span></a></td>
													</tr>';
												}
												else {
													echo '<tr>';
													if($p['id'] == $_GET['id']) {
														echo '<form method="post" action="process/update_parameter.php">
														<input type="hidden" name="id" value="'.$_GET['id'].'">
														<td><input type="text" class="form-control" name="name" value="'.$p['name'].'"></td>
														<td><input type="text" class="form-control" name="description" value="'.$p['description'].'"></td>
														<td><input type="text" class="form-control" name="value" value="'.$p['value'].'"></td>
														<td><button type="submit" class="btn btn-default">Submit</button>
														<button type="cancel" class="btn btn-default" onclick="window.history.go(-1);return false;">Cancel</button></td>
														</form>';
													} else {
														echo'
															<td>'.$p['name'].'</td>
														<td>'.$p['description'].'</td>
														<td>'.$p['value'].'</td>
														<td></td>
														
														';
													}
													echo '</tr>';
												}
											
											}
									   ?>
									   
									 </tbody>
									</table>
								  
								  </div>
								  
								 
								 <div id="add_parameter_modal" title="Add Parameter" style="display: none;padding:20px;">
									
									<h4>Add Parameter</h4>
									
									<form>
  										<div class="form-group">														
											<input type="text" class="form-control" name="measurement_name" placeholder="Name">
									     </div>
									     
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
								  
								 					
							<?php  } ?>  
       
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
	 
		
    <script src="bootstrap/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	

   
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="bootstrap/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
	
	
	<script src="https://kryogenix.org/code/browser/sorttable/sorttable.js"></script>
	  
	<script>
	$(document).ready(function() {
    $('#example').DataTable( {
		"paging" : false,
		'sDom': 't' ,
        initComplete: function () {
            this.api().columns('.select-filter').every( function () {
                var column = this;
                var select = $('<select style="margin-left:8px; float:right;"><option value="">No Filter</option></select>')
                    .appendTo( $(column.header()) )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }
    } );
} );
		
			
		function deleteItem() {
			if (confirm("Are you sure?")) {
				// continue
			}else{
				return false;
			}
		}
		
		function confirmV4Submit() {
			
			var ip_address = $('#ipAddressv4').val();
			var port = $('#portNumberv4').val();
			
			if(ip_address == "" || port == "")
			{
				if (confirm("Some fields are empty and they will be filled with default values, are you sure?")) {
					// continue
				}else{
					return false;
				}
			}
		}
		
		function confirmV6Submit() {
			
			var ip_address = $('#ipAddressv6').val();
			var port = $('#portNumberv6').val();
			
			if(ip_address == "" || port == "")
			{
				if (confirm("Some fields are empty and they will be filled with default values, are you sure?")) {
					// continue
				}else{
					return false;
				}
			}
		}
		
		function confirmURLSubmit() {
			
			var ip_address = $('#ipAddressURL').val();
			var port = $('#portNumberURL').val();
			
			if(ip_address == "" || port == "")
			{
				if (confirm("Some fields are empty and they will be filled with default values, are you sure?")) {
					// continue
				}else{
					return false;
				}
			}
		}
		
		function confirmSerialSubmit() {
			
			var ip_address = $('#modbusID').val();
			var port = $('#modbusPort').val();
			var baudRate = $('#baudRate').val();
			var parity = $('#parity').val();
			var dataBits = $('#dataBits').val();
			var stopBits = $('#stopBits').val();
			var flowControl = $('#flowControl').val();
			
			if(ip_address == "" || port == "" || baudRate == "" || parity == "" || dataBits == "" || stopBits == "" || flowControl == "")
			{
				if (confirm("Some fields are empty and they will be filled with default values, are you sure?")) {
					// continue
				}else{
					return false;
				}
			}
		}
		
		function deleteStructure() {
			if (confirm("This structure may have tags on it, are you sure you want to delete it?")) {
				// continue
			}else{
				return false;
			}
		}
		
		function changeConfig() {
			var target = $('#controller_type').val();
			var targetConfig = $('#num_config').val();
			var dbConfig = $('#dbconfig').val();
			
			console.log(targetConfig);
			console.log(dbConfig);

			if (target == "Standard" || dbConfig > targetConfig ){
				if (confirm("Configuration items will be lost, are you sure? ")) {
					// continue
				}else{
					return false;
				}
			}
			
		}
		
		function changeDropdown()
		{
			var combo = document.getElementById("data_type_id_edit");
			var selected = combo.options[combo.selectedIndex].text;
			if (selected == "STRUCT") {
				console.log(selected);
				document.getElementById("units").disabled = true;
				document.getElementById("address").disabled = true;
				document.getElementById("operator").disabled = true;
				document.getElementById("structure").disabled = true;
			} else {
				console.log(selected);
				document.getElementById("units").disabled = false;
				document.getElementById("address").disabled = false;
				document.getElementById("operator").disabled = false;
				document.getElementById("structure").disabled = false;
			}
		}
		
		function changeDropdown2()
		{
			var combo = document.getElementById("data_type_id_edit2");
			var selected = combo.options[combo.selectedIndex].text;
			if (selected == "STRUCT") {
				console.log(selected);
				document.getElementById("units2").disabled = true;
				document.getElementById("address2").disabled = true;
				document.getElementById("operator2").disabled = true;
				document.getElementById("structure2").disabled = true;
			} else {
				console.log(selected);
				document.getElementById("units2").disabled = false;
				document.getElementById("address2").disabled = false;
				document.getElementById("operator2").disabled = false;
				document.getElementById("structure2").disabled = false;
			}
		}
		
				
		$('#controller_type').change(function(){
			if (this.value==='Standard')
			{
				$("#num_config").css("display", "none");
			}
			else {
				$("#num_config").css("display", "inline");
			}
		});

		
		
		$("table th").addClass("headerSortUp");
		$("table th.noOrder").removeClass("headerSortUp");
		
		$("tr").removeClass("odd");
		
		$(function() {

		  // We can attach the `fileselect` event to all file inputs on the page
		  $(document).on('change', ':file', function() {
			var input = $(this),
				numFiles = input.get(0).files ? input.get(0).files.length : 1,
				label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
			input.trigger('fileselect', [numFiles, label]);
		  });

		  // We can watch for our custom `fileselect` event like this
		  $(document).ready( function() {
			  $(':file').on('fileselect', function(event, numFiles, label) {

				  var input = $(this).parents('.input-group').find(':text'),
					  log = numFiles > 1 ? numFiles + ' files selected' : label;

				  if( input.length ) {
					  input.val(log);
				  } else {
					  if( log ) alert(log);
				  }

			  });
		  });
		  
		});
		
		
		function OPCFunction() 
		{
			var checkBox = document.getElementById("check");
			var schema = document.getElementById("schema");
			var tag = document.getElementById("tag");
			console.log(checkBox);
			console.log(schema);
			if (checkBox.checked == true){
				schemaPrefix.style.display = "inline";
				schemaSuffix.style.display = "inline";
				tag.style.display = "inline";
			} else {
			   schemaPrefix.style.display = "none";
			   schemaSuffix.style.display = "none";
			   tag.style.display = "none";
			}
		}
		
		
		var placeholderText = {"IPv4":"IP Address","IPv6":"IP Address","Serial Communications":"Modbus ID","COM / RS232":"Serial Port","URL":"URL Address"};

		$("#address_type").on("change",function() {
			var selection = document.getElementById("address_type");
			var inputBox = document.getElementById("ip_address");
			
			var selectedVal = $('#address_type').find(':selected').text();
			if (placeholderText[selectedVal] !== undefined) {
				inputBox.placeholder = placeholderText[selectedVal];
			}
		});
		
		 $("#address_type1").on("change", function() {
		   var sOptionVal = $(this).val();
		   if (/modal/i.test(sOptionVal)) {
			 var $selectedOption = $(sOptionVal);
			 $selectedOption.modal('show');
		   }
		 });
		 
		
	</script>
	

  
		 <?php if (isset($_GET['s'])) { ?>

			<script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

		<?php  } ?> 
    
    
        <?php  if ($_GET['f']) { ?>  
        
			<!-- Require JS (REQUIRED) -->
			<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
			<script data-main="./elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    	<?php  } ?> 
    	
        <?php  if ($_GET['m']) { ?>  
        
        		<script src="http://maps.google.com/maps/api/js"></script>

				<?php include('app/google_maps.php'); ?>
                
        <?php  } ?>  
    
    <?php include("app/common_scripts.php"); ?>
    
  </body>
</html>

<?php 

$message = $_GET['message'];
$message = str_replace("_"," ",$message);

$response = '<?xml version="1.0" encoding="UTF-8"?>
<Response>
	<Say voice="alice">There is an alarm for register number 300 within device 210</Say>
	<Say voice="alice">'.$message.'</Say>
	<Gather action="processCallInput.php" method="GET">
		<Say voice="alice">You may choose a following option or hangup</Say>
		<Say voice="alice">Press one to turn off alarms for this register</Say>
		<Say voice="alice">Press two to call the next contact</Say>
		<Say voice="alice">Press three to mute this alarm for one hour</Say>
	</Gather>
	<Say voice="alice">Goodbye!</Say>
</Response>';

echo $response;

?>
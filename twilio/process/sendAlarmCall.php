<?php

require_once "/var/www/html/twilio/twilio-php-master/Twilio/autoload.php";

use Twilio\Rest\Client;
    
function twilioCallDemo($number,$message){
	
    // Step 2: Set our AccountSid and AuthToken from https://twilio.com/console
    $AccountSid = "AC98d01a06cc09b7d6e8b6ae47f74691fa";
    $AuthToken = "b3321108d2513f6a45cc4243a7559036";

    // Step 3: Instantiate a new Twilio Rest Client
    $client = new Client($AccountSid, $AuthToken);
	
	$number = '+1'.$number;
	$message = str_replace(" ","_",$message);

    try {
        // Initiate a new outbound call
        $call = $client->account->calls->create(
            // Step 4: Change the 'To' number below to whatever number you'd like 
            // to call.
            $number,

            // Step 5: Change the 'From' number below to be a valid Twilio number 
            // that you've purchased or verified with Twilio.
            "+19189923063",

            // Step 6: Set the URL Twilio will request when the call is answered.
            array("url" => "https://www.qautomated.com/twilio/process/callMessage.php?message=$message")
        );
        echo "Started call: " . $call->sid;
    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
}

?>
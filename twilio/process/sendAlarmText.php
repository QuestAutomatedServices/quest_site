 <?php
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
require_once "/var/www/html/twilio/twilio-php-master/Twilio/autoload.php";

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

function twilioTextDemo($number,$body) {
	
	// Your Account SID and Auth Token from twilio.com/console
	$sid = 'AC98d01a06cc09b7d6e8b6ae47f74691fa';
	$token = 'b3321108d2513f6a45cc4243a7559036';
	$client = new Client($sid, $token);

	// Use the client to do fun stuff like send text messages!
	
	$number = '+1'.$number;
	
	$client->messages->create(
		// the number you'd like to send the message to
		$number,
		array(
			// A Twilio phone number you purchased at twilio.com/console
			'from' => '+19189923063',
			// the body of the text message you'd like to send
			'body' => 'There is an alarm for register number 300 within device 210: '.$body,

		)
	);	
}

?>
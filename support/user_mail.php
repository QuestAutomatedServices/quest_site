<?php

include("PHPMailer_5.2.0/class.phpmailer.php");

function sendUserMail($info){
	
	$user = getUserName($info['ticket_userID']);
	$account = getAccountName($info['ticket_accountID']);
	$title = $info['ticket_title'];
	$category = getCategoryName($info['ticket_categoryID']);
	$message = nl2br($info['message_content']);
	
	$ticketID = $info['ticketID'];
	$link = "http://qautomated.com/support/support.php?f=getTicket&t=$ticketID";
	
	$body = '<h3>New Support Ticket!</h3><br><br>
			<p>Information on the created ticket:</p>
			<p>User: '.$user.'</p>
			<p>Account: '.$account.'</p>
			<p>Category: '.$category.'</p>
			<p>Title: '.$title.'</p>
			<p>Message:<br>'.$message.'</p>
			
			<p>To reply to this ticket follow this URL:<br>'.$link.'</p>
			
			<img src="http://qautomated.com/reporting/images/logo.jpg">';
	
	$mail = new PHPMailer(); // defaults to using php "mail()"

	$mail->SetFrom('do-not-reply@questsupport.net', 'Quest Automated Services Support');
	$mail->AddReplyTo('do-not-reply@questsupport.net','Quest Automated Services Support');

	//$mail->AddBCC('markmorton@phpprousa.com', 'Mark Morton');
	$mail->AddAddress('mmorton@quest-automated.com', 'Mark Morton');
	$mail->AddAddress('nmorton@quest-automated.com', 'Nick Morton');


	$mail->Subject = "New Support Ticket";

	$mail->MsgHTML($body);

	$mail->Send();
}

function sendUserMailTwo($messageInfo,$ticketInfo){
	
	$ticketUser = getUserName($ticketInfo['ticket_userID']);
	$title = $ticketInfo['ticket_title'];
	$message = nl2br($messageInfo['message_content']);
	$messageUser = getUserName($messageInfo['message_userID']);
	
	$ticketID = $ticketInfo['ticketID'];
	$link = "http://qautomated.com/support/support.php?f=getTicket&t=$ticketID";
	
	$mail = new PHPMailer(); // defaults to using php "mail()"
	
	if($messageInfo['message_userID'] == $ticketInfo['ticket_userID']){
		
		$body = '<h3>New Support Message</h3>
				<p>There is a new message on the support ticket "'.$title.'"</p>

				<p>Response Information:</p>
				<p>User: '.$messageUser.'</p>
				<p>Message:<br>'.$message.'</p>

				<p>To reply to this ticket follow this URL:<br>'.$link.'</p>

				<img src="http://qautomated.com/reporting/images/logo.jpg">';
		
		$mail->AddAddress('mmorton@quest-automated.com', 'Mark Morton');
		$mail->AddAddress('nmorton@quest-automated.com', 'Nick Morton');	
		
	}else{
		
		$body = '<h3>New Support Message</h3>
				<p>'.$ticketUser.', you have a new message on your ticket "'.$title.'"</p>

				<p>Response Information:</p>
				<p>User: '.$messageUser.'</p>
				<p>Message:<br>'.$message.'</p>

				<p>To reply to this ticket follow this URL:<br>'.$link.'</p>
				<p>Thank you for using Quest Automated Services Support!</p>

				<img src="http://qautomated.com/reporting/images/logo.jpg">';
		
		$mail->AddAddress(getUserEmail($ticketInfo['ticket_userID']), $ticketUser);
	}
	
	$mail->SetFrom('do-not-reply@questsupport.net', 'Quest Automated Services Support');
	$mail->AddReplyTo('do-not-reply@questsupport.net','Quest Automated Services Support');

	$mail->Subject = "Support Ticket Response";

	$mail->MsgHTML($body);

	$mail->Send();
}
 

?>
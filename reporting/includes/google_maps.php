<?php $location = getLocationsGeo(); ?>
<script>

 var myLatLng = {lat: <?php echo $location[1]; ?>, lng: <?php echo $location[2]; ?>};

  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 8,
    center: myLatLng
  });

        var contentString = '<div id="content">'+
            '<div id="siteNotice">'+
            '</div>'+
			'<p><img src="images/logo.jpg"></p>'+
            '<h1 id="firstHeading" class="firstHeading"><?php echo $location[0]; ?></h1>'+
            '<div id="bodyContent">'+
            '<p><b><?php echo $location[0]; ?></b>, <a href="portal.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1">click for document manager</a></p>'+
            '</div>'+
            '</div>';

        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

  var marker = new google.maps.Marker({
    position: myLatLng,
    map: map,
	title: '<?php echo $location[0]; ?>'
  });


 marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

</script>
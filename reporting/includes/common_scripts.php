<script>
$(function() {

	$(".phone").mask("(999) 999-9999");
	
	$(".datepicker").datepicker();

	$( "#tabs" ).tabs();
	$( "#tabs_edit" ).tabs();

    $( ".accordion" ).accordion({ active: false,collapsible: true});
	

	$( "#newlink" ).click(function() {	
			$( "#add_modal" ).dialog( "open" );	
	});


	$( "#add_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	
	
	




	$( ".fa-file-image-o" ).click(function() {	

/*				$.ajax({
						  type: "Get",
						  dataType: "json",
						  cache: false,
						  url: "process/ajax_data.php",
						  data: {"id": $(arguments[0].target).attr("data-id"),"type":1},
						  success: function(data) {

						
						  }
						}); 
*/

			$( "#config_modal" ).dialog({
			  autoOpen: true,
			  height: 650,
			  width: 650,
			  modal: true,
			  title: $(arguments[0].target).attr("data-name")
			 
			});	

	});



	$( ".devsel" ).change(function() {	
	


			var num = $(arguments[0].target).attr("data-id");

			var id=$(this).val();
			var dataString = 'id='+ id + '&num='+ num;
			
			if (id != "") {

				$("#related_register_" + num).html(''); 
			
				 $.ajax	 ({
					 type: "GET",
					 url: "ajax_registers.php",
					 data: dataString,
					 cache: false,
					 success: function(html)
				 {
						$("#related_register_" + num).html(html); 
						
						} 
				 });
	
			} else {

				$("#related_register_" + num).html(''); 
				
			}
	});




	$( ".notes" ).click(function() {	
	
		alert('test');
	
			$( "#notes_modal" ).dialog({
			  autoOpen: true,
			  height: 650,
			  width: 650,
			  modal: true,
			  title: $(arguments[0].target).attr("data-name")
			 
			});	

	});


	$( ".fa-clock-o" ).click(function() {	


			$( "#alarms_modal" ).dialog({
			  autoOpen: true,
			  height: 650,
			  width: 650,
			  modal: true,
			  title: $(arguments[0].target).attr("data-name")
			 
			});	

	});




    $('#locations_table').DataTable();

    $('#accounts_table').DataTable( {
		
		  "pageLength": 25
		
		});


    $('#devices_table').DataTable({
		
		"order": [[ 4, "desc" ]],	
		
		"processing": true,
		"serverSide": true,
		
		  "ajax": {

            "url": "process/server_processing_registers.php?id=<?php echo $_GET['id']; ?>",
            "type": "POST"
        },
		
		"language": {

    	"infoFiltered": ""

 	 },
		
  		"fnRowCallback": function (nRow, aData, iDisplayIndex) {

                // Bind click event
                $(nRow).click(function() {
				
					
					if ($(arguments[0].target).attr("data-type") == 'notes') {

							$.ajax({
							  type: "Get",
							  dataType: "json",
							  cache: false,
							  url: "process/ajax_data.php",
							  data: {"id": $(arguments[0].target).attr("data-id"),"type":1},
							  
						   		success: function(data) {
													
									//console.log(data);

									//set ID for form
									$( "#regID" ).val(data[0].regID);

								
									//reset div
									$( "#notestable" ).html('<br /><br />');     
										
									//add notes to div
									$.each(data, function(n, elem) {
										
											if (elem.register_note) {
										  		$( "#notestable" ).append(elem.register_note + '<br />' + elem.register_note_timestamp + '<br /><br />' );  
											
											} else {
												
												$( "#notestable" ).append('No notes');
											}
									});
									
									//open modal
									$( "#notes_modal" ).dialog({
									  autoOpen: true,
									  height: 700,
									  width: 850,
									  modal: true,
									  title: data[0].register_name
									 
									});	
	
							   },

							}); 							

								
					} else if ($(arguments[0].target).attr("data-type") == 'alarms') {


						$.ajax({
							  type: "Get",
							  dataType: "json",
							  cache: false,
							  url: "process/ajax_data.php",
							  data: {"id": $(arguments[0].target).attr("data-id"),"type":2},
							  
						   		success: function(data) {
													
									//console.log(data);


									//reset div
									$( "#alarmstable" ).html('<br /><br />');     
										
									//add notes to div
									$.each(data, function(n, elem) {
										
											if (elem.alarm_label) {
												
												if (elem.alarm_status == 1) {
													var alarm = 'On';	
												} else {
													var alarm = 'Off';	
												}

												if (elem.warn_status == 1) {
													var warn = 'On';	
												} else {
													var warn = 'Off';	
												}

												
										  		$( "#alarmstable" ).append(elem.alarm_label + '<br /> Alarm Threshold: ' + elem.alarm_threshold + '(' + alarm + ')<br /> Warn Threshold: ' + elem.warn_threshold + '(' + warn + ')');  
											
											} else {
												
												$( "#alarmstable" ).append('No Alarms set up');
											}
									});
									
									
									//open modal
									$( "#alarms_modal" ).dialog({
									  autoOpen: true,
									  height: 300,
									  width: 500,
									  modal: true,
									  title: data[0].register_name
									 
									});	
	
							   },

							}); 

					
					} else if ($(arguments[0].target).attr("data-type") == 'haul') {



						$.ajax({
							  type: "Get",
							  dataType: "json",
							  cache: false,
							  url: "process/ajax_data.php",
							  data: {"id": $(arguments[0].target).attr("data-id"),"type":3},
							  
						   		success: function(data) {
													
									//console.log(data);
									
									//set ID for form
									$( "#regID" ).val(data[0].regID);



									//reset div
									$( "#haultable" ).html('<br /><br />');     
										
									//add notes to div
									$.each(data, function(n, elem) {
										
		
									});
									
									

									//open modal
									$( "#haul_modal" ).dialog({
									  autoOpen: true,
									  height: 300,
									  width: 500,
									  modal: true,
									 title: data[0].register_name
									 
									});	
	
							   },

							}); 


					
					
					} else if ($(arguments[0].target).attr("data-type") == 'log') {
	
	
							$.ajax({
							  type: "Get",
							  dataType: "json",
							  cache: false,
							  url: "process/ajax_data.php",
							  data: {"id": $(arguments[0].target).attr("data-id"),"type":4},
							  
						   		success: function(data) {
													
									//console.log(data);


									//set ID for form
									$( "#regID" ).val(data[0].regID);


									//reset div
									$( "#logtable" ).html('<br /><br />');     
										
									//add notes to div
									$.each(data, function(n, elem) {
										
		
									});
									
									

										//open modal
									$( "#log_modal" ).dialog({
									  autoOpen: true,
									  height: 300,
									  width: 500,
									  modal: true,
									  title: data[0].register_name
									 
									});	
							   },

							}); 
	


					
					}

                });

                return nRow;
			
		  }				
		
		});


/*	 $('#chart').highcharts({
        chart: {
            type: 'pie',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: 'Browser market shares at a specific website, 2014'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: 'Browser share',
            data: [
                ['Firefox', 45.0],
                ['IE', 26.8],
                {
                    name: 'Chrome',
                    y: 12.8,
                    sliced: true,
                    selected: true
                },
                ['Safari', 8.5],
                ['Opera', 6.2],
                ['Others', 0.7]
            ]
        }]
    });
*/



    $.getJSON('process/time_series.php?id=<?php echo $_GET['id']; ?>&n=<?php echo $_GET['n']; ?>', function (data) {

        $('#chart').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: '<?php echo  $_SESSION['currentname']; ?>'
            },
			
			tooltip: {
            	headerFormat: '<b>{point.x:%b %e, %Y}: </b><br>',
            	pointFormat: '{point.y:f} <?php echo  $_SESSION['currentunit']; ?>'
        	},
			
            subtitle: {
                text: document.ontouchstart === undefined ?
                        'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
            },
            xAxis: {
                type: 'datetime'
            },
            yAxis: {
                title: {
                    text: 'Register Reading'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Reading',
                data: data
            }]
        });
    });


});
</script>
<?php 

function connect() {

	$host_name = "localhost";
	$database = "quest_csi"; // Change your database name
	$username = "beta_dev";          // Your database user id 
	$password = "HoleN123";          // Your password
	
	//////// Do not Edit below /////////
	try {
		$db = new PDO('mysql:host='.$host_name.';dbname='.$database, $username, $password);
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}

	return $db;
}


function truncateTable($table) {
	
	$db = connect();
	
	$stmt = $db->prepare("TRUNCATE $table ");
	$stmt->execute();

}



function getLogbookArray($mo,$yr,$type,$tb)  {
	
	$db = connect();
	
	if ($type == 1) { 
		$stmt = $db->prepare("SELECT * FROM tbl_logbook WHERE logbook_month = '$mo' AND logbook_year = '$yr' AND logbook_battery = '$tb' AND logbook_pressures IS NOT NULL ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return unserialize($results[0]['logbook_pressures']);	
	} else if ($type == 2) {
		$stmt = $db->prepare("SELECT * FROM tbl_logbook WHERE logbook_month = '$mo' AND logbook_year = '$yr' AND logbook_battery = '$tb' AND logbook_levels IS NOT NULL ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return unserialize($results[0]['logbook_levels']);
	} else if ($type == 3) {
		$stmt = $db->prepare("SELECT * FROM tbl_logbook WHERE logbook_month = '$mo' AND logbook_year = '$yr' AND logbook_battery = '$tb' AND logbook_sales IS NOT NULL ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return unserialize($results[0]['logbook_sales']);
	} else if ($type == 4) {
		$stmt = $db->prepare("SELECT * FROM tbl_logbook WHERE logbook_month = '$mo' AND logbook_year = '$yr' AND logbook_battery = '$tb' AND logbook_gas IS NOT NULL ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return unserialize($results[0]['logbook_gas']);
	}
	
}



function getLogbookSettings($tb)  {
	
	$db = connect();
	
		$stmt = $db->prepare("SELECT * FROM tbl_logbook_settings WHERE logbook_battery = '$tb' ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		return unserialize($results[0]['logbook_settings']);
	
	
}


function getLogbookNotes($date)  {
	
	$parts = explode("/",$date);
	
	$date = $parts[2]."-".$parts[0]."-".$parts[1];
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_logbook_notes WHERE logbook_notes_date = ?  ");
	$stmt->execute(array($date));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}





function getReportingDate()  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_reporting_date ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['reporting_date'];
}



function updateReportingDate($date)  {
	
	$db = connect();
	
	//$stmt = $db->prepare("UPDATE tbl_reporting_date SET reporting_date= NOW() ");
	
	$stmt = $db->prepare("UPDATE tbl_reporting_date SET reporting_date='$date' ");
	
	$stmt->execute();


}



function insertAccounts($xml) {
	
	$db = connect();
	
	foreach($xml->children() as $child) {
		
		$stmt = $db->prepare("INSERT INTO tbl_accounts (account_name, account_id, account_active, account_updated) VALUES(:field1,:field2,:field3,:field4)");
	
		$stmt->execute(array(':field1' => $child->name, ':field2' => $child->id, ':field3' => $child->active, ':field4' => date("Y-m-d H:i:s", time()) ) );

	}
	
}


function insertDevices($xml) {
	
	$db = connect();
	
	foreach($xml->children() as $child) {
		
		$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_deviceID=:id ");
		$stmt->bindValue(':id',$child->deviceid, PDO::PARAM_INT);
		$stmt->execute();
		$rows = $stmt->rowCount();

		if ($rows == 0) {

		$stmt = $db->prepare("INSERT INTO tbl_devices (device_siteID, device_deviceID, device_name, device_accountID, device_latitude, device_longitude, device_sms, device_last_report, device_restart, device_updated) VALUES(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9,:field10)");
	
		$stmt->execute(array(':field1' => $child->siteid, ':field2' => $child->deviceid, ':field3' => $child->name, ':field4' => $child->accountid, ':field5' => $child->latitude, ':field6' => $child->longitude, ':field7' => $child->sms, ':field8' => $child->lastreport, ':field9' => $child->restarttime, ':field10' => date("Y-m-d H:i:s", time()) ) );
			
		}

	}
	
}


function insertRegisters($xml,$deviceid) {
	
	$db = connect();
	
	foreach($xml->children() as $child) {
		
		
		$stmt = $db->prepare("SELECT * FROM tbl_registers WHERE register_id=:id ");
		$stmt->bindValue(':id',$child->id, PDO::PARAM_INT);
		$stmt->execute();
		$rows = $stmt->rowCount();

		if ($rows == 0) {
			

			$stmt = $db->prepare("INSERT INTO tbl_registers (register_id, register_number, register_name, register_unit, register_deviceID, register_updated) VALUES(:field1,:field2,:field3,:field4,:field5,:field6)");

			$stmt->execute(array(':field1' => $child->id, ':field2' => $child->registernumber, ':field3' => $child->name, ':field4' => $child->unit, ':field5' => $deviceid, ':field6' => date("Y-m-d H:i:s", time()) ) );
		}


	}
	
}



function insertReading($regnum,$readdate,$reading,$device) {
	
	$db = connect();
	
	$stmt = $db->prepare("INSERT IGNORE INTO tbl_register_history (register_number, register_date, register_reading, register_deviceID) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $regnum, ':field2' => $readdate, ':field3' => $reading, ':field4' => $device) );

	
}



function getPLLabel($val)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_pl_values WHERE pl_code LIKE '".round($val,0)."' ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['pl_value'];
}



function getLastReading($dev,$reg,$offset=0,$convert=null)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg ORDER BY register_date DESC LIMIT $offset,1 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($convert) {
		return getPLLabel($results[0]['register_reading']);
	} else {
		return $results[0]['register_reading'];
	}
	
	
}


function getLastReadingDate($dev,$reg,$offset=0)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg ORDER BY register_date DESC LIMIT $offset,1 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return formatTimestamp($results[0]['register_date']);
	
	
	
}



function getLastReadingTS($dev,$reg,$offset=0)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg ORDER BY register_date DESC LIMIT $offset,1 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['register_date'];
}



function insertSettingChange($device,$reg,$val,$user) {
	
	$db = connect();
	
	$stmt = $db->prepare("INSERT INTO tbl_setting_changes (userID, deviceID, register_number, setting_value) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $user, ':field2' => $device, ':field3' => $reg, ':field4' => $val) );

	
}


function getLastRegChange($device,$reg) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_setting_changes WHERE deviceID = $device AND register_number = $reg ORDER by timestamp DESC LIMIT 1 ");
	
	$stmt->execute();

	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($results) {
		return 'Changed to '.$results[0]['setting_value'].' on '.formatTimestampOffset($results[0]['timestamp'],5);
	}
	
}



function getReadingsDeviceRegRange($dev,$start,$end)  {
	
	$db = connect();
	
	if ($dev == 226) {
		$html = '<td>&nbsp;</td>';
	} else {
		$html = '';
	}
	

	
	for ($i=$start;$i<=$end;$i++) {

		
		$stmt = $db->prepare("SELECT * FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $i ORDER BY register_date DESC LIMIT 1 ");
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		
		if ($results[0]['register_reading']) {
			$html .= '<td>'.getWriteRegLink($dev,$i,$results[0]['register_reading']).'</td>';
		} else {
			$html .= '<td>&nbsp;</td>';
		}
		
		
		
	}
	

	return $html;
}




function getAccounts()  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_accounts ORDER BY account_name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getDevicesByAccount($id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_accountID = $id ORDER BY device_name  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getPLDevicesByAccount($id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_accountID = $id AND device_pl = 1 ORDER BY device_name  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}




function getPLDevicesByAccountSelect($current,$id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_accountID = $id AND device_pl = 1 ORDER BY device_name  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
	$html = '<select class="form-control" name="deviceID">';

	$html .= '<option value="">--Choose Device--</option>';

	
		foreach ($results as $key=>$result) {

				if ($current == $result['device_deviceID']) {
					$html .= '<option value="'.$result['device_deviceID'].'" selected="selected">'.$result['device_name'].'</option>';
				} else {
					$html .= '<option value="'.$result['device_deviceID'].'">'.$result['device_name'].'</option>';
				}
		}

	$html .= '</select>';


	
	return $html;
}



function getPOCDevicesByAccountSelect($current,$id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_accountID = $id AND device_poc = 1 ORDER BY device_name  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
	$html = '<select class="form-control" name="deviceID">';

	$html .= '<option value="">--Choose Device--</option>';

	
		foreach ($results as $key=>$result) {

				if ($current == $result['device_deviceID']) {
					$html .= '<option value="'.$result['device_deviceID'].'" selected="selected">'.$result['device_name'].'</option>';
				} else {
					$html .= '<option value="'.$result['device_deviceID'].'">'.$result['device_name'].'</option>';
				}
		}

	$html .= '</select>';


	
	return $html;
}


function getDevicesByAccountSelect($current,$id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_accountID = $id ORDER BY device_name  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
	$html = '<select class="form-control" name="deviceID">';

	$html .= '<option value="">--Choose Device--</option>';

	
		foreach ($results as $key=>$result) {

				if ($current == $result['device_deviceID']) {
					$html .= '<option value="'.$result['device_deviceID'].'" selected="selected">'.$result['device_name'].'</option>';
				} else {
					$html .= '<option value="'.$result['device_deviceID'].'">'.$result['device_name'].'</option>';
				}
		}

	$html .= '</select>';


	
	return $html;
}



function getPOCListAll($id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT DISTINCT(poc_label), deviceID FROM tbl_poc_config ORDER BY poc_label ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getPOCListByDevice($id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT DISTINCT(poc_label) FROM tbl_poc_config WHERE deviceID = $id");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getPOCConfig($id,$key)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_poc_config WHERE deviceID = $id AND poc_label = '$key' ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getPOCConfigData($id,$reg)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_poc_config WHERE deviceID = $id AND registerID_read = '$reg' ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getDeviceData($id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_deviceID = $id");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0];
}




function getDevices()  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_devices WHERE device_accountID IN (42,45,46,24) ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getRegisters($device)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_registers WHERE register_deviceID = ? ");
	$stmt->execute(array($device));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getTagDescription($tag)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_tag_info WHERE tag_name = '?' ");
	$stmt->execute(array($tag));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['tag_description'];
}


function getControlRegisters($device)  {
	
	$db = connect();
	
	if ($device == 228) {
		$start = 411;
		$end = 573;
	} else if ($device == 232) {
		$start = 411;
		$end = 573;
	} else if ($device == 230) {
		$start = 411;
		$end = 573;
	} else if ($device == 227) {
		$start = 411;
		$end = 573;
	} else if ($device == 231) {
		$start = 411;
		$end = 573;
	} else if ($device == 225) {
		$start = 411;
		$end = 573;
	} else if ($device == 237) {
		$start = 411;
		$end = 573;
	} else if ($device == 223) {
		$start = 550;
		$end = 573;
	} else if ($device == 229) {
		$start = 50;
		$end = 73;
	}
	
	$stmt = $db->prepare("SELECT r.register_number, r.register_name, s.categoryID, s.reg_label, s.reg_units  FROM tbl_registers as r LEFT JOIN tbl_register_settings as s on r.register_deviceID=s.register_deviceID AND r.register_number=s.register_number WHERE r.register_deviceID = ? AND r.register_number >= $start AND r.register_number <= $end ORDER BY r.register_number ");  //s.categoryID,
	$stmt->execute(array($device));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getWriteRegLink($device,$register,$value=null) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_registers WHERE register_deviceID = ? AND register_number = ? ");
	$stmt->execute(array($device,$register));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$stmt = $db->prepare("SELECT * FROM tbl_registers WHERE register_deviceID = ? AND register_number != ? AND register_name = ?  ");
	$stmt->execute(array($device,$register,$results[0]['register_name']));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	if ($value) {
		return '<a href="control.php?edit=1&r='.$device.'&n='.$results[0]['register_number'].'">'.$value.'</a>';	
	} else {
		return '<a href="control.php?edit=1&r='.$device.'&n='.$results[0]['register_number'].'">Click Here</a>';
	}
	
}


function getPMColumns($dev=227) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT r.register_number, r.register_name, c.register_category  FROM tbl_registers as r LEFT JOIN tbl_register_settings as s ON r.register_deviceID = s.register_deviceID AND r.register_number = s.register_number LEFT JOIN tbl_register_categories as c ON s.categoryID=c.categoryID WHERE r.register_deviceID = ? AND r.register_number >= 37 AND r.register_number <= 502 ORDER BY c.register_category, r.register_name");
	$stmt->execute(array($dev));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
	
}


function getPMArray() {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_pm_fields");
	$stmt->execute(array($dev));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return unserialize($results[0]['field_array']);
	
}
	
function getPMFieldArray($dev=227) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_pm_fields");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$array = unserialize($results[0]['field_array']);
	$array = $array['pmfield'];
	
	$idstring = implode(",",$array);
	
	$stmt = $db->prepare("SELECT *  FROM tbl_registers WHERE register_deviceID = ? AND register_number IN (".$idstring.") ORDER BY register_pl_order, register_name ");
	$stmt->execute(array($dev));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results;
}



function getPMFieldData($dev=227) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_pm_fields");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$array = unserialize($results[0]['field_array']);
	$array = $array['pmfield'];
	
	$countlimit = count($array);
	
	$idstring = implode(",",$array);
	
	$stmt = $db->prepare("SELECT * FROM tbl_register_history WHERE register_deviceID = $dev AND register_number IN (".$idstring.") ORDER BY register_date DESC LIMIT $countlimit ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	return $results;
}




function getCTOColumns($dev=227) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT *  FROM tbl_registers as r LEFT JOIN tbl_register_settings as s ON r.register_deviceID = s.register_deviceID AND r.register_number = s.register_number LEFT JOIN tbl_register_categories as c ON s.categoryID=c.categoryID   WHERE r.register_deviceID = ? ORDER BY c.register_category, r.register_name");
	$stmt->execute(array($dev));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
	
}


function getCTOArray() {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_cto_fields");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return unserialize($results[0]['field_array']);
	
}



function getCTOFieldArray($dev=231) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_cto_fields");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$array = unserialize($results[0]['field_array']);
	$array = $array['ctofield'];
	
	$idstring = implode(",",$array);
	
	$stmt = $db->prepare("SELECT *  FROM tbl_registers WHERE register_deviceID = ? AND register_number IN (".$idstring.") ORDER BY register_name");
	$stmt->execute(array($dev));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results;
}


function getRegReadingByDate($dev,$reg,$date,$factor) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND date(register_date) = '$date' AND time(register_date) < '14:00' ORDER BY register_date DESC LIMIT 1  ");
							
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	if ($results[0]['register_reading']){
		return ($factor-$results[0]['register_reading']);
	} else {
		return '';
	}
	
	
}


function getTotalOilSalesByDate($dev,$reg,$date,$factor) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT sum(sales_calculation) as totsale FROM tbl_register_calculations WHERE register_deviceID = $dev AND register_number = $reg AND date(calculation_date) = '$date' AND calculation_type = 1  ");
							
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	if ($results[0]['totsale'] > 0){
		
		//return ($factor-$results[0]['totsale']);
		return $results[0]['totsale'];
		
	} else {
		return '';
	}
	
	
}



function getTotalOilProducedByDate($dev,$reg,$date,$factor) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT sum(production_calculation) as totprod FROM tbl_register_calculations WHERE register_deviceID = $dev AND register_number = $reg AND date(calculation_date) = '$date' AND calculation_type = 1  ");
							
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	if ($results[0]['totprod'] > 0){
		
		//return ($factor-$results[0]['totsale']);
		return $results[0]['totprod'];
		
	} else {
		return '';
	}
	
	
}




function getTotalWaterProducedByDate($dev,$reg,$date) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT sum(water_production_calculation) as totprod FROM tbl_register_calculations WHERE register_deviceID = $dev AND register_number = $reg AND date(calculation_date) = '$date' AND calculation_type = 2  ");
							
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	if ($results[0]['totprod'] > 0){
		
		//return ($factor-$results[0]['totsale']);
		return $results[0]['totprod'];
		
	} else {
		return '';
	}
	
	
}




function getTotalWaterTruckedByDate($dev,$reg,$date) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT sum(water_trucked_calculation) as totprod FROM tbl_register_calculations WHERE register_deviceID = $dev AND register_number = $reg AND date(calculation_date) = '$date' AND calculation_type = 2  ");
							
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	if ($results[0]['totprod'] > 0){
		
		//return ($factor-$results[0]['totsale']);
		return $results[0]['totprod'];
		
	} else {
		return '';
	}
	
	
}



function getGasReading($dev,$time,$reg) {
	
	$db = connect();
	
	//$date = date('Y-m-d', strtotime("-1 day"));
	$date = date('Y-m-d');
	
	
	if ($time == 1) {
			$stmt = $db->prepare("SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE r.register_deviceID = $dev AND r.register_number = 7 AND h.register_number=r.register_number AND h.register_deviceID=r.register_deviceID AND date(h.register_date) = '".$date."' ORDER BY h.register_date DESC LIMIT 1  ");
	
	} else if ($time == 2) {
			$stmt = $db->prepare("SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE r.register_deviceID = $dev AND r.register_number = 9 AND h.register_number=r.register_number AND h.register_deviceID=r.register_deviceID AND date(h.register_date) = '".$date."'  ORDER BY h.register_date DESC LIMIT 1  ");
		
	} else {
			$stmt = $db->prepare("SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE r.register_deviceID = $dev AND r.register_number = $reg AND h.register_number=r.register_number AND h.register_deviceID=r.register_deviceID AND date(h.register_date) = '".$date."'  ORDER BY h.register_date DESC LIMIT 1  ");
	
	}
	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	return $results[0]['register_reading'];
}



function getTotalTubingReading($geo,$date=null) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
	if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(11));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(11));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(10));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(11));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(11));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(11));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($date . "+1 day"));
					
					
					if ($date != '') {
						$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND date(register_date) = '$targetdate' AND time(register_date) < '14:00' ORDER BY register_date DESC LIMIT 1  ");
					} else {
						$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg ORDER BY register_date DESC LIMIT 1  ");
					}
										
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					$totalpressure += $results[0]['register_reading'];
					
				}
				
			}
			
		
	return number_format($totalpressure,2);
}



function getTubingReadingsRange($geo,$startdate) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
	if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(11));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(11));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(11));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(11));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(11));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(11));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($startdate . "+1 day"));

					
					$stmt = $db->prepare("SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ");
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					//print_r($results);
					//exit();
					
										
				}
				
			}
			
		foreach ($results as $result) {
				
				$array[] = array((strtotime($result['register_date'])*1000),number_format($result['register_reading'],2));	
			}
			
		
	return $array;
}


function getTotalCasingReading($geo,$date=null) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
	if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(10));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(10));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(10));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(10));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(10));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(10));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($date . "+1 day"));
					
					
					if ($date != '') {
						$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND date(register_date) = '$targetdate' AND time(register_date) < '14:00' ORDER BY register_date DESC LIMIT 1  ");
					} else {
						$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg ORDER BY register_date DESC LIMIT 1  ");
					}
										
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					$totalpressure += $results[0]['register_reading'];
					
				}
				
			}
			
		
	return number_format($totalpressure,2);
}





function getCasingReadingsRange($geo,$startdate) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
	if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(10));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(10));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(10));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(10));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(10));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(10));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($startdate . "+1 day"));
					
					//echo "SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ";
				
					$stmt = $db->prepare("SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ");
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					//print_r($results);
					//exit();
					
										
				}
				
			}
			
		foreach ($results as $result) {
				
					$array[] = array((strtotime($result['register_date'])*1000),number_format($result['register_reading'],2));		
			}
			
		
	return $array;
}




function getTotalGasReading($geo,$date=null) {
	
	$db = connect();
	
	$totalgas = 0 ;
	
	
	if ($geo == 1) {
		
			$devices = array(223=>array(9));
		
			//$devices = array(223=>array(7,289),225=>array(7),227=>array(7),228=>array(7),229=>array(7,107,207,307),230=>array(7),231=>array(7),232=>array(7));
		
	} else if ($geo == 2) {
		
			$devices = array(223=>array(291));
		
	} else if ($geo == 3) {
		
			$devices = '';	
		
	} else if ($geo == 4) {
		
			$devices = array(223=>array(7,289),225=>array(7),227=>array(7),228=>array(7),229=>array(7,107,207,307),230=>array(7),231=>array(7),232=>array(7));
	
	} else if ($geo == 5) {
		
			$devices = '';	

	} else if ($geo == 6) {
		
			$devices = '';	
	
	} else if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(9));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(9));
	
	} else if ($geo == 9) {
		
		
		//////////Cunningham-Davis B 5-74
		
		$devices = array(229=>array(9));
		
	} else if ($geo == 10) {
		
				//////////Dora Cunningham DO A1
		
		$devices = array(229=>array(409));
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(9));
		
	} else if ($geo == 12) {
		
		/////////Homann #2
		
		$devices = array(229=>array(109));
		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(9));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(9));
		
	} else if ($geo == 16) {
		
		/////////RH Cummins 1
		
		$devices = array(229=>array(209));
		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(9));
		
	} else if ($geo == 18) {
		
		/////////Northrup-Lindsey 1
		
		$devices = array(237=>array(9));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($date . "+1 day"));
					
					
					if ($date != '') {
						$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND date(register_date) = '$date' AND time(register_date) < '14:00' ORDER BY register_date DESC LIMIT 1  ");
					} else {
						$stmt = $db->prepare("SELECT register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg ORDER BY register_date DESC LIMIT 1  ");
					}
										
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					$totalgas += $results[0]['register_reading'];
					
				}
				
			}
			
		
	return number_format($totalgas,2);
}




function getGasReadingsRange($geo,$startdate) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(5));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(5));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(5));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(5));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(5));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(5));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($startdate . "+1 day"));
					
					//echo "SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ";
				
					$stmt = $db->prepare("SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ");
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					//print_r($results);
					//exit();
					
										
				}
				
			}
			
		foreach ($results as $result) {
				
					$array[] = array((strtotime($result['register_date'])*1000),number_format($result['register_reading'],2));		
			}
			
		
	return $array;
}






function getGasDiffReadingsRange($geo,$startdate) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(2));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(2));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(2));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(2));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(2));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(2));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($startdate . "+1 day"));
					
					//echo "SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ";
				
					$stmt = $db->prepare("SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ");
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					//print_r($results);
					//exit();
					
										
				}
				
			}
			
		foreach ($results as $result) {
				
					$array[] = array((strtotime($result['register_date'])*1000),number_format($result['register_reading'],2));		
			}
			
		
	return $array;
}



function getGasStatReadingsRange($geo,$startdate) {
	
	$db = connect();
	
	$totalpressure = 0 ;
	
	
if ($geo == 7) {
		
		/////////Bayliss #1
		
		$devices = array(227=>array(3));
	
	} else if ($geo == 8) {
		
		/////////Cunningham-Davis ET2
		
		$devices = array(231=>array(3));
	
		
	} else if ($geo == 11) {
		
		
		/////////Homann #1
		
		$devices = array(228=>array(3));
		

		
	} else if ($geo == 13) {
		
			/////////Homann #3
		
		$devices = array(230=>array(3));
		
	} else if ($geo == 14) {
		
		
		//////////////JB Riley 1A
		
		$devices = array(232=>array(3));
		

		
	} else if ($geo == 17) {
		
		/////////TS Riley F1
		
		$devices = array(225=>array(3));
	
	}	
	
	
			foreach ($devices as $dev=>$registers) {
				
				//$registers = array(7,107,207,289,307);
				
				foreach ($registers as $reg) {
					
					$targetdate = date("Y-m-d", strtotime($startdate . "+1 day"));
					
					//echo "SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ";
				
					$stmt = $db->prepare("SELECT register_date, register_reading FROM tbl_register_history WHERE register_deviceID = $dev AND register_number = $reg AND register_date BETWEEN '".$startdate." 14:00' AND '".$targetdate." 13:59' ORDER BY register_date  ");
					
					$stmt->execute();
					$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
					
					//print_r($results);
					//exit();
					
										
				}
				
			}
			
		foreach ($results as $result) {
				
					$array[] = array((strtotime($result['register_date'])*1000),number_format($result['register_reading'],2));		
			}
			
		
	return $array;
}


function getOilReading($dev,$time,$reg) {
	
	$db = connect();
	
	
	//if ($time == 1) {
			$stmt = $db->prepare("SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE r.register_deviceID = $dev AND r.register_number = $reg AND h.register_number=r.register_number AND h.register_deviceID=r.register_deviceID ORDER BY h.register_date DESC LIMIT 1  ");
	//} else {
			//$stmt = $db->prepare("SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE r.register_deviceID = $dev AND r.register_number = $reg AND h.register_number=r.register_number AND h.register_deviceID=r.register_deviceID ORDER BY h.register_date DESC LIMIT 1  ");
	//}
	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	return $results[0]['register_reading'];
}


function getWaterReading($dev,$time) {
	
	$db = connect();
	
	
	if ($time == 1) {
			$stmt = $db->prepare("SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE r.register_deviceID = $dev AND r.register_number = 794 AND h.register_number=r.register_number AND h.register_deviceID=r.register_deviceID ORDER BY h.register_date DESC LIMIT 1  ");
	
	} 
	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	return $results[0]['register_reading'];
}



function getOilSold($dev,$date,$reg) {
	
	$db = connect();
	
		if ($reg != '') {
			$stmt = $db->prepare("SELECT calculation_date, sum(sales_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID = $dev  AND register_number = $reg  AND calculation_type = 1 AND calculation_date = '".$date."'  GROUP BY calculation_date ORDER BY calculation_date");
		} else {
			
			$stmt = $db->prepare("SELECT calculation_date, sum(sales_calculation) as totsales FROM `tbl_register_calculations` WHERE register_deviceID = $dev  AND register_number = $reg  AND calculation_type = 1 AND calculation_date = '".$date."'  GROUP BY calculation_date ORDER BY calculation_date");
		}
	
	
	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	return $results[0]['totsales'];
}


function getOilProduced($dev,$date,$reg) {
	
	$db = connect();
	
	if ($reg != '') {
		$stmt = $db->prepare("SELECT calculation_date, sum(production_calculation) as totprod FROM `tbl_register_calculations` WHERE register_deviceID = $dev AND register_number = $reg  AND calculation_type = 1 AND calculation_date = '".$date."'  GROUP BY calculation_date ORDER BY calculation_date");
	} else {
		$stmt = $db->prepare("SELECT calculation_date, sum(production_calculation) as totprod FROM `tbl_register_calculations` WHERE register_deviceID = $dev  AND calculation_type = 1 AND calculation_date = '".$date."'  GROUP BY calculation_date ORDER BY calculation_date");
	}
	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
	
	return $results[0]['totprod'];
}




function getAdminUsers()  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_users WHERE user_level = 1 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	
	$html = '<h1 class="page-header">Admin Users</h1>';

	$html .= '<form action="process/add_user.php" method="POST" class="form-inline">
				<input type="hidden" name="user_level" value="1">
				
			<div class="form-group">
					<input type="text" class="form-control" name="user_email" placeholder="User Email">
			  </div>
			 
			 	<div class="form-group">
					<input type="text" class="form-control" name="user_name" placeholder="User Name">
			  </div>
			  
			  <div class="form-group">
					<input type="password" class="form-control" name="user_password" placeholder="Password">
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>';
	
	
		$html .= '<table class="table table-striped table-bordered" cellspacing="0" style="margin-top:20px;width:400px;">';


	$html .= '<thead><tr><th>Name</th><th>Email</th></tr></thead><tbody>';
		
		foreach ($results as $row) {
			

			
			$html .= '<tr><td>'.$row['user_name'].'</td><td>'.$row['user_email'].'</td></tr>';
			
	
			
		}

	$html .= '</tbody><tfoot></tfoot></table></table>';

	
	return $html;

}



function getUsers()  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_users WHERE account_id = ? ");
	$stmt->execute(array($_GET['id']));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	
	$html = '<h1 class="page-header">Users - '.getAccountName($_GET['id']).'</h1>';

	$html .= '<form action="process/add_user.php" method="POST" class="form-inline">
				<input type="hidden" name="user_level" value="2">
				<input type="hidden" name="account_id" value="'.$_GET['id'].'">
				
			<div class="form-group">
					<input type="text" class="form-control" name="user_email" placeholder="User Email">
			  </div>
			 
			 	<div class="form-group">
					<input type="text" class="form-control" name="user_name" placeholder="User Name">
			  </div>
			  
			  <div class="form-group">
					<input type="password" class="form-control" name="user_password" placeholder="Password">
			  </div>
			  
			  <button type="submit" class="btn btn-default">Submit</button>
			</form>';
	
	
		$html .= '<table class="table table-striped table-bordered" cellspacing="0" style="margin-top:20px;width:400px;">';


	$html .= '<thead><tr><th>Name</th><th>Email</th></tr></thead><tbody>';
		
		foreach ($results as $row) {
			

			
			$html .= '<tr><td>'.$row['user_name'].'</td><td>'.$row['user_email'].'</td></tr>';
			
	
			
		}

	$html .= '</tbody><tfoot></tfoot></table></table>';

	
	return $html;

}


function getRegCatSelect($current,$id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_register_categories ORDER BY register_category ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
	$html = '<select class="form-control" name="categoryID['.$id.']">';

	$html .= '<option value="">--Choose Category--</option>';

	
		foreach ($results as $key=>$result) {

				if ($current == $result['categoryID']) {
					$html .= '<option value="'.$result['categoryID'].'" selected="selected">'.$result['register_category'].'</option>';
				} else {
					$html .= '<option value="'.$result['categoryID'].'">'.$result['register_category'].'</option>';
				}
		}

	$html .= '</select>';


	
	return $html;
}



function getRegisterData($device,$register) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_registers WHERE register_deviceID = ? AND register_number = ? ");
	$stmt->execute(array($device,$register));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getTankDiaryCount($device,$battery,$date) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM tbl_tank_diary WHERE accountID = ".$_SESSION['account_id']." AND deviceID = ".$device." AND tank_battery = ".$battery." AND diary_date = '".$date."'  ");

	$stmt->execute();
	$rows = $stmt->rowCount();

	return $rows;
}


function getTankDiaryText($device,$battery,$date) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM tbl_tank_diary WHERE accountID = ".$_SESSION['account_id']." AND deviceID = ".$device." AND tank_battery = ".$battery." AND diary_date = '".$date."'  ");

	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($results[0]['diary_text'] != '') {
		return substr($results[0]['diary_text'],0,15).'...';	
	}
	
}




function getWellGoal($goalnum) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM tbl_goals WHERE goal_number = ".$goalnum." ORDER BY  goal_date DESC, goalID DESC  LIMIT 1 ");

	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($results[0]['goal_value'] != '') {
		return '<a class="goal_link" data-num="'.$goalnum.'">'.$results[0]['goal_value'].'</a>';	
	} else {
		return '<a class="goal_link" data-num="'.$goalnum.'">Set</a>';	
	}
	
	
}


/*function getWellGoalTotal() { 
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM tbl_goals WHERE goal_number = ".$goalnum." ORDER BY  goal_date DESC, goalID DESC  LIMIT 1 ");
 
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if ($results[0]['goal_value'] != '') {
		return '<a class="goal_link" data-num="'.$goalnum.'">'.$results[0]['goal_value'].'</a>';	
	} else {
		return '<a class="goal_link" data-num="'.$goalnum.'">Set</a>';	
	}
	 
	
}*/


function getNotes()  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM tbl_notes as n LEFT JOIN tbl_geo as g ON n.note_geo=g.geoID LEFT JOIN tbl_users as u ON n.userID=u.userID LEFT JOIN tbl_devices as d ON n.note_device=d.device_deviceID  WHERE n.accountID = ? ORDER BY n.note_timestamp DESC  ");
	$stmt->execute(array($_SESSION['account_id']));
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


/*function getAccountSites($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE account_id=:id ORDER BY name ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}




function getAccountSelect($current)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM accounts ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
	$html = '<select class="form-control" id="acctselect" name="id" style="margin-bottom:20px;">';

	$html .= '<option value="">--Choose Account--</option>';

	
		foreach ($results as $key=>$result) {

				if ($current == $result['id']) {
					$html .= '<option value="'.$result['id'].'" selected="selected">'.$result['name'].'</option>';
				} else {
					$html .= '<option value="'.$result['id'].'">'.$result['name'].'</option>';
				}
		}

	$html .= '</select>';


	
	return $html;
}




function getSiteName($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['name'];
}


function getSiteControllers($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE site_id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function insertSiteController($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO site_controllers (`name`,`site_id`,`created`, `ip_address`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['site_id'], ':field3' => date("Y-m-d H:i:s", time()), ':field4' => $post['ip_address'] ) );


	
}



function getLocationsGeo($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE account_id=:id ORDER BY name ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;

}



function getManufacturers() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM manufacturers ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getManufacturerData($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM manufacturers WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}




function getMfrDeviceCount($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM devices WHERE manufacturers_id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();

	return $rows;
}



function getMfrSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM manufacturers ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="manufacturers_id" >';

		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}



function insertManufacturer($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO manufacturers (`name`,`website`,`active`,`created`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['website'], ':field3' => 1, ':field4' => date("Y-m-d H:i:s", time()) ) );


	
}





function getObjects() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM objects ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertObject($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO objects (`name`,`objectID`,`active`,`created`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['objectID'], ':field3' => 1, ':field4' => date("Y-m-d H:i:s", time()) ) );


}



function getControllerObjects($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT *, c.id as contObjID, c.name as objname  FROM controller_objects as c LEFT JOIN objects as o ON c.object_id=o.id WHERE c.controller_id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function insertControllerObject($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO controller_objects (`controller_id`, `object_id`, `name`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['controller_id'], ':field2' => $post['object_id'], ':field3' => $post['name'] ) );


	
}



function getObjectDevices($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT *, o.id as objDevID  FROM object_devices as o LEFT JOIN devices as d ON o.device_id=d.id  WHERE o.object_id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertObjectDevice($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO object_devices (`site_id`, `controller_id`, `object_id`, `device_id`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['site_id'], ':field2' => $post['controller_id'], ':field3' => $post['object_id'], ':field4' => $post['device_id'] ) );


	
}


function getParameters() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM object_parameters ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}




function getDevices() {
	
	$db = connect();
	
	if ($_GET['mfr']) {
		
		$stmt = $db->prepare("SELECT d.id, d.name, d.deviceID, m.name as mfrname, t.name as devtype FROM devices as d LEFT JOIN manufacturers as m ON d.manufacturers_id=m.id LEFT JOIN device_types as t ON d.device_type_id=t.id  WHERE d.manufacturers_id=:id ORDER BY d.name ");
		$stmt->bindValue(':id', $_GET['mfr'], PDO::PARAM_INT);
		
	} else{
		
		$stmt = $db->prepare("SELECT d.id, d.name, d.deviceID, m.name as mfrname, t.name as devtype FROM devices as d LEFT JOIN manufacturers as m ON d.manufacturers_id=m.id LEFT JOIN device_types as t ON d.device_type_id=t.id ORDER BY d.name ");
	
	}

	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getDeviceData($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT d.id, d.name, d.deviceID,  d.device_type_id, m.name as mfrname, m.id as mfrid, t.name as devtype FROM devices as d LEFT JOIN manufacturers as m ON d.manufacturers_id=m.id LEFT JOIN device_types as t ON d.device_type_id=t.id WHERE d.id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getDeviceSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM devices ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" id="device_select" name="device_id" >';
	$html .= '<option value="">Choose Device</option>';
	
		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}





function getDeviceTypeSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_types ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="device_type_id" >';

		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}



function getDeviceTypes() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_types ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function insertDeviceType($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
	
	$stmt->execute(array(':field1' => $post['name'] ) );


	
}



function insertDevice($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO devices (`name`,`deviceID`, `manufacturers_id`, `device_type_id`, `active`,`created`) VALUES(:field1,:field2,:field3,:field4,:field5,:field6)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['deviceID'],  ':field3' => $post['manufacturers_id'],  ':field4' => $post['device_type_id'], ':field5' => 1, ':field6' => date("Y-m-d H:i:s", time()) ) );

	return $lastId = $db->lastInsertId();

	
}



function insertDeviceMeasurement($vars) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM device_measurements WHERE `device_id`=:did AND `cfn`=:cid ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();

	$stmt = $db->prepare("INSERT INTO device_measurements (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_unit`, `operator_id`, `data_type_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_unit'], ':field6' =>  $vars['operator_id'], ':field7' =>  $vars['data_type_id'] ) );

	//return $lastId = $db->lastInsertId();

	
}




function removeDeviceMeasurement($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_measurements WHERE `device_id`=:did AND `measurement_id`=:mid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':mid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function insertDeviceExecution($vars) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_executions WHERE `device_id`=:did AND `cfn`=:cid ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$stmt = $db->prepare("INSERT INTO device_executions (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_unit`, `operator_id`, `data_type_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' => ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_unit'], ':field6' =>  $vars['operator_id'], ':field7' =>  $vars['data_type_id'] ) );

	
}




function removeDeviceExecution($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_executions WHERE `device_id`=:did AND `execution_id`=:eid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':eid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function insertDeviceTrigger($vars) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM device_triggers WHERE `device_id`=:did AND `cfn`=:cid ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$stmt = $db->prepare("INSERT INTO device_triggers (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_unit`, `operator_id`, `data_type_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_unit'], ':field6' =>  $vars['operator_id'], ':field7' =>  $vars['data_type_id'] ) );

	
}




function removeDeviceTrigger($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_triggers WHERE `device_id`=:did AND `trigger_id`=:tid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':tid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function insertDeviceNotification($vars) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_notifications WHERE `device_id`=:did AND `cfn`=:cid ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$stmt = $db->prepare("INSERT INTO device_notifications (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_unit`, `operator_id`, `data_type_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_unit'], ':field6' =>  $vars['operator_id'], ':field7' =>  $vars['data_type_id'] ) );

	
}




function removeDeviceNotification($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_notifications WHERE `device_id`=:did AND `notification_id`=:nid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':nid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function checkSetting($type,$dev,$id,$cfn) {
	
	$db = connect();
	
	if ($type == 1) {
				
		$stmt = $db->prepare("SELECT m.name, o.name as opname FROM device_measurements as d LEFT JOIN measurements as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index  WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");

	} else if ($type == 2) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname FROM device_executions as d LEFT JOIN executions as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
		
	} else if ($type == 3) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname FROM device_triggers as d LEFT JOIN triggers as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
	
	} else if ($type == 4) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname FROM device_notifications as d LEFT JOIN notifications as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
	
	}

	$stmt->bindValue(':did', $dev, PDO::PARAM_INT);
	$stmt->bindValue(':mid', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cid', $cfn, PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	if ($rows == 1) {
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		return $results[0]['name'].' '.$results[0]['opname'];
	}
	
	
	
}
	



function getTagsByDeviceTypeAndParameterType($devtype,$tagtype) {

	if ($tagtype == 1) {
		$table = 'measurements';
	} else if ($tagtype == 2) {
		$table = 'executions';	
	} else if ($tagtype == 3) {
		$table = 'triggers';	
	} else if ($tagtype == 4) {
		$table = 'notifications';	
	}
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM ".$table." WHERE device_type_id=".$devtype." ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
	
}
	


function getMeasurements() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, t.name as devname FROM measurements as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getExecutions() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, t.name as devname FROM executions as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getTriggers() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, t.name as devname FROM triggers as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getOperatorSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM operators  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="operator_id" >';
	
	$html .= '<option value="">No Operand</option>';

		foreach ($results as $mfr) {
			
			if ($mfr['operand_type_index'] == $current) {
				$html .= '<option value="'.$mfr['operand_type_index'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['operand_type_index'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}


function getOperands() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM operators ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertOperand($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO operators (`name`,`operand_type_index`) VALUES(:field1,:field2)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['operand_type_index'] ) );


	
}



function getDataTypeSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM data_types  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="data_type_id" >';

	$html .= '<option value="">No Data Type</option>';
	
		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}



function getDataTypes() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM data_types ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertDataType($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
	
	$stmt->execute(array(':field1' => $post['name'] ) );


	
}



function getNotifications() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, t.name as devname FROM notifications as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}*/



?>
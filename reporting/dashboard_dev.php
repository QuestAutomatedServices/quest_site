<?php 
session_start();
include('connections/mysql.php');
include('functions/functions.php');
include('app/functions.php');

if (!isset($_SESSION['auth'])) {
	header('location: login.php');
	exit();		
}


function convert16($val) {
	
	$int = intval($val);
	$dec = $val-$int;
	$factor = $dec*16;
	
	return $int.' ft. '.round($factor,0).'/16 in.';
	
}



/*(1) Subtract 10, the number of whole inches.

10.89 - 10 = .89

(2) Multiply .89 x 16 to get the number of 16th's inches remaining.

.89 x 16 = 14.24

14.24 = the number of 16th's of an inch. This is the numerator of the fraction. Apply the rules for rounding

14.24 rounds to 14 because .24 is less than .5

Therefore, 10.89in. =(approximately) 10 and 14/16ths inch(es)
(You may have to reduce.)*/

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

$regions = array('Global','Oklahoma','Texas');

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="../beta/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../beta/bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../beta/bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="../beta/bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">Keystone Hughes - Richland Resources Corporation</a>
       
               </div>
        <div id="navbar" class="navbar-collapse collapse">
       <ul class="nav navbar-nav navbar-right">
<?php /*?>     	<li <?php if (!isset($_GET['r']) && !isset($_GET['tb']) && !isset($_GET['po'])  && !isset($_GET['sm']) && !isset($_GET['pl'])) echo 'class="active"' ?>><a href="dashboard_dev.php">Global <span class="sr-only">(current)</span></a></li><?php */?>
                      
          <li <?php if ($_GET['r'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?r=1">Oklahoma</a></li>
            <li <?php if ($_GET['sm'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?sm=1">Device Summary</a></li>
             <li <?php if ($_GET['tb'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?tb=1">Tank Batteries</a></li>
            <li <?php if ($_GET['po'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?po=1">Pump Off Controllers</a></li>
            <li <?php if ($_GET['pl'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?pl=1">Plunger Lift Analysis</a></li>
             <li><a href="control.php?r=227">Control Settings</a></li>
                 	 
                  	<li><a href="process/logout.php">Logout</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <?php /*?><li <?php if (!isset($_GET['r']) && !isset($_GET['tb']) && !isset($_GET['sm']) && !isset($_GET['po']) && !isset($_GET['pl'])) echo 'class="active"' ?>><a href="dashboard_dev.php">Global <span class="sr-only">(current)</span></a></li><?php */?>
                      
             <li <?php if ($_GET['r'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?r=1">Oklahoma</a></li>
             
             <?php if (isset($_GET['r'])) { ?>
          <ul>
                   
           		
           		
            
            
            <?php	
					  
					//echo '<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=192&ct=1">Crane</a>
							
							echo '<li>Crane
					
								<ul>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=224">Ector County POC</a></li>
								
								</ul>
								
					
					
					</li>';
									  
									  
					//echo '<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=192&ct=1">Ector</a>
	
						echo '<li>Ector
					
								<ul>
								
									
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=226">JD Slater 1</a></li>

								
								</ul>
								
					
					
					</li>';
									  
					//echo '<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=192&sct=1">Gaines</a>
	
							echo '<li>Gaines
					
								<ul>
								
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=227">Bayliss #1</a></li>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=231">Cunningham-Davis ET2</a></li>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=223">Gaines County Compressor (1)</a></li>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=223">Gaines County Compressor (2)</a></li>							
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=228">Homann #1</a></li>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=230">Homann #3</a></li>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=232">JB Riley 1A</a></li>
									<li><a href="dashboard_dev.php?r='.$_GET['r'].'&d=225">TS Riley F1</a></li>


								
								</ul>
								
					
					
					</li>';

					
			?>
           	
           	
           	
            	
          </ul>
         <?php } ?>
             
              <li <?php if ($_GET['sm'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?sm=1">Device Summary</a></li>
           <li <?php if ($_GET['tb'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?tb=1">Tank Batteries</a></li>
            <li <?php if ($_GET['po'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?po=1">Pump Off Controllers</a></li>
            <li <?php if ($_GET['pl'] == 1) echo 'class="active"' ?>><a href="dashboard_dev.php?pl=1">Plunger Lift Analysis</a></li>
             <li><a href="control.php?r=227">Control Settings</a></li>
                  	           
			
         
         
         
          
         
           
			 <li><a href="process/logout.php">Logout</a></li>
          </ul>
          
          
        
         
        
         
       
        
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
        
        		
         
        <?php  if (!isset($_GET['d']) && !isset($_GET['tb'])  && !isset($_GET['r'])  && !isset($_GET['po'])   && !isset($_GET['gs'])   && !isset($_GET['pl'])   && !isset($_GET['pls']) ) { ?>  
        
        
       	
        <?php  } ?>  
        
         <?php  if (isset($_GET['sm']) ) { ?>  
        
              <h1 class="page-header">Summary</h1>
              
              <div class="alert alert-info" role="alert">Click a reading to see reading history</div>
              
              
               <div class="table-responsive">
              
              
              	<table class="table table-striped">

									<thead>
									<tr>
									  <th>Device</th>
									   <th>Gas Flow</th>
									    <th>Gas Flow Yesterday</th>
									    <th>Oil Top Level 1</th>
									     <th>Oil Top Level 2</th>
									      <th>Water Top Level 1</th>

									</tr>
								    </thead>
								    
								   <tbody>
								   
								        <?php 
	
												
											
			  		
												$devices = getDevicesByAccount(42);

												foreach ($devices as $device) {
													
													if ($device['device_deviceID'] == 223) {
													
																
														echo '<tr>
																<td>'.$device['device_name'].' (1)</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=7">'.getGasReading($device['device_deviceID'],1,'').'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=9">'.getGasReading($device['device_deviceID'],2,'').'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														echo '<tr>
																<td>'.$device['device_name'].' (2)</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=289">'.getGasReading($device['device_deviceID'],3,289).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=291">'.getGasReading($device['device_deviceID'],4,291).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														
													} else if ($device['device_deviceID'] == 229) {
													
																
														echo '<tr>
																<td>'.$device['device_name'].' (1)</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=7">'.getGasReading($device['device_deviceID'],1,'').'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=9">'.getGasReading($device['device_deviceID'],2,'').'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														echo '<tr>
																<td>'.$device['device_name'].' (2)</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=107">'.getGasReading($device['device_deviceID'],5,107).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=109">'.getGasReading($device['device_deviceID'],6,109).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';
														
														echo '<tr>
																<td>'.$device['device_name'].' (3)</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=207">'.getGasReading($device['device_deviceID'],5,207).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=209">'.getGasReading($device['device_deviceID'],6,209).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';	
														
														echo '<tr>
																<td>'.$device['device_name'].' (4)</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=307">'.getGasReading($device['device_deviceID'],5,307).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=309">'.getGasReading($device['device_deviceID'],6,309).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>

															</tr>';	
														
														
													} else if ($device['device_deviceID'] == 226) {
														
														echo '<tr>
																<td>'.$device['device_name'].'</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=790">'.convert16((15.6-getOilReading($device['device_deviceID'],1,790))).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=792">'.convert16((15.6-getOilReading($device['device_deviceID'],2,792))).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=794">'.convert16((15.6-getWaterReading($device['device_deviceID'],1))).'</a></td>
																<td>&nbsp;</td>
																										
													
															</tr>';
														
													} else if ($device['device_deviceID'] == 224) {
														
															echo '<tr>
																<td>PJ Lea A&B Tanks</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=790">'.convert16((15.6-getOilReading($device['device_deviceID'],1,790))).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=792">'.convert16((15.6-getOilReading($device['device_deviceID'],2,792))).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																										
													
															</tr>';
														
															echo '<tr>
																<td>PJ Lea C&D Tanks</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=794">'.convert16((15.6-getOilReading($device['device_deviceID'],1,794))).'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=796">'.convert16((15.6-getOilReading($device['device_deviceID'],2,796))).'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																										
													
															</tr>';
														
																										
													} else {
														
												
													
														echo '<tr>
																<td>'.$device['device_name'].'</td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=7">'.getGasReading($device['device_deviceID'],1,'').'</a></td>
																<td><a href="dashboard_dev.php?c=1&d='.$device['device_deviceID'].'&g=9">'.getGasReading($device['device_deviceID'],2,'').'</a></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																										
													
															</tr>';
												}

												}
			  						?>
								   
								   
									</tbody>
           
			</table>
            
			</div>
            
          </div>
			


          


        <?php  } ?>  
        
        
        
          <?php  if (isset($_GET['r']) ) { ?>  
        
        
              <?php /*?><h1 class="page-header"><?php if (!isset($_GET['r'])) { echo $regions[0].' Overview'; } else if (!isset($_GET['d'])) { echo $regions[$_GET['r']].' Overview'; } ?><?php if (isset($_GET['d'])) echo getDeviceName($_GET['d']);  ?></h1>
              
              	<div class="btn-group">
                  
                  <a href="dashboard_dev.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=1" class="btn btn-primary <?php if ($_GET['s'] == 1) echo 'active' ?>">POCs</a>
                  
                  
                  <a href="dashboard_dev.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&s=2" class="btn btn-primary <?php if ($_GET['s'] == 2) echo 'active' ?>">Plunger Lifts</a>


              </div>
              
              	<div class="btn-group">
                  
                  <a href="#" id="choosechart" class="btn btn-primary">Choose Chart</a>

       
                  <a href="dashboard_dev.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&c=1" class="btn btn-primary <?php if ($_GET['c'] == 1) echo 'active' ?>">Report Config</a>
            
                  <a href="dashboard_dev.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&b=1" class="btn btn-primary <?php if ($_GET['b'] == 1) echo 'active' ?>">Digital Log Book</a>
                  
				  <a href="dashboard_dev.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&f=1" class="btn btn-primary <?php if ($_GET['f'] == 1) echo 'active' ?>">Document Manager</a>                  
           
           			<a href="dashboard_dev.php?r=<?php echo $_GET['r']; ?>&d=<?php echo $_GET['d']; ?>&m=1" class="btn btn-primary <?php if ($_GET['m'] == 1) echo 'active' ?>">Map</a>
           		
            </div>
               
          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
			<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
			<span></span> <b class="caret"></b></div>
			
		
            
             <div id="chart"></div><?php */?>

				
		<h2 class="sub-header">Production & Sales</h2>
          
           <div class="table-responsive">
           
             		 <?php  if (isset($_GET['r']) && !isset($_GET['d']) ) { ?> 
             		 
             		   
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
									  <th>Geo</th>
									  <th>Date</th>
									  <th>Gas Prod</th>
									  <th>Gas Sold</th>
									  <th>Goal</th>
									  <th>Var</th>								  
									  <th>Oil Prod</th>
									  <th>Oil Sold</th>
									  <th>Goal</th>
									  <th>Var</th>							  
									  <th>Water Prod</th>
									  <th>Tubing psi</th>
									  <th>Casing psi</th>
									  <th>Line psi</th>
									  <th>Downtime</th>	
									</tr>
								    </thead>
								    
								   <tbody><!--STATE TBODY-->
               
									<tr> <!--STATE ROW-->
									<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#2" aria-hidden="true" title="Show Detail"></i> <?php echo $regions[$_GET['r']];?></td>	
									<td><?php echo date('m/d/Y');?></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									</tr> <!--STATE ROW-->
									
									<tr id="2" class="collapse"> <!--STATE COLLAPSE ROW-->
								
									<td colspan="14">  <!--STATE COLLAPSE TD-->
									
									
										<table class="table table-striped" style="background-color:azure;border: 1px solid #D7D7D7;"><!--COUNTY TABLE-->

										<thead>
										<tr>
										  <th>Geo</th>
										  <th>Date</th>
										  <th>Gas Prod</th>
										  <th>Gas Sold</th>
										  <th>Goal</th>
										  <th>Var</th>								  
										  <th>Oil Prod</th>
										  <th>Oil Sold</th>
										  <th>Goal</th>
										  <th>Var</th>							  
										  <th>Water Prod</th>
										  <th>Tubing psi</th>
										  <th>Casing psi</th>
										  <th>Line psi</th>
										  <th>Downtime</th>	
										</tr>
										</thead>
										
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#4" aria-hidden="true" title="Show Detail"></i> Crane</td>
										<td><?php echo date('m/d/Y');?></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="4" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:coral;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr>
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td>Ector County POC</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#5" aria-hidden="true" title="Show Detail"></i> Ector</td>	
										<td><?php echo date('m/d/Y');?></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
									<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="5" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:coral;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr>
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td>JD Slater 1</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->
										
										<tr> <!--COUNTY ROW-->
										<td><i class="fa fa-plus-square" data-toggle="collapse" data-target="#6" aria-hidden="true" title="Show Detail"></i> Gaines</td>	
										<td><?php echo date('m/d/Y');?></td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										</tr> <!--COUNTY ROW-->
										
										<tr id="6" class="collapse"> <!--COUNTY COLLAPSE ROW-->
								
										<td colspan="14">  <!--COUNTY COLLAPSE TD-->
										
												<table class="table table-striped" style="background-color:coral;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr>
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tbody>
												
												<tr> <!--SITE ROW-->
												<td>Bayliss #1</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->

												<tr> <!--SITE ROW-->
												<td>Cunningham-Davis ET2</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td>Gaines County Compressor (1)</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td>Gaines County Compressor (2)</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->		
												
												<tr> <!--SITE ROW-->
												<td>Homann #1</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td>Homann #2</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->	
												
												<tr> <!--SITE ROW-->
												<td>Homann #3</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td>JB Riley 1A</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												<tr> <!--SITE ROW-->
												<td>TS Riley F1</td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->																
										

												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
										
										</td><!--COUNTY COLLAPSE TD-->
					
										</tr> <!--COUNTY COLLAPSE ROW-->

										</tbody><!--COUNTY TBODY-->
           					
            							</table><!--COUNTY TABLE-->
									
									
									</td><!--STATE COLLAPSE TD-->
					
									</tr> <!--STATE COLLAPSE ROW-->
									
															  
							       </tbody><!--STATE TBODY-->
           					
            					</table><!--STATE TABLE-->

           
           					 <?php  } ?> 
           					 
           					 
           					  <?php  if ($_GET['d'] ) { ?> 
           					  
           					  <table class="table table-striped" style="background-color:coral;border: 1px solid #D7D7D7;"><!--SITE TABLE-->

												<thead>
												<tr>
												  <th>Geo</th>
												  <th>Date</th>
												  <th>Gas Prod</th>
												  <th>Gas Sold</th>
												  <th>Goal</th>
												  <th>Var</th>								  
												  <th>Oil Prod</th>
												  <th>Oil Sold</th>
												  <th>Goal</th>
												  <th>Var</th>							  
												  <th>Water Prod</th>
												  <th>Tubing psi</th>
												  <th>Casing psi</th>
												  <th>Line psi</th>
												  <th>Downtime</th>	
												</tr>
												</thead>
												
												<tr> <!--SITE ROW-->
												<td><?php echo getDeviceName($_GET['d']);?></td>	
												<td><?php echo date('m/d/Y');?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												</tr> <!--SITE ROW-->
												
												</tbody><!--SITE TBODY-->

												</table><!--SITE TABLE-->
           					  
           					   <?php  } ?> 
            
          </div>
			


          


        <?php  } ?>  

        
        
        
            <?php  if (isset($_GET['tb'])) { ?>  
        
        
        		 <h1 class="page-header">Tank Battery Summary</h1>
        		 
        		<!-- <div id="chart"></div>-->
        		
        			<div class="alert alert-info" role="alert">Click a reading to see reading history</div>
        			
        
        
          			<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
										<th colspan="3">&nbsp;</th>
										<th colspan="3" class="warning">Oil Tank 1</th>
										<th colspan="3" class="info">Oil Tank 2</th>
										<th colspan="3" class="danger">Water Tank</th>
									</tr>
									
									<tr>
									  <th>Gauge Sheet</th>
									  <th>Battery Name</th>
									  <th>Status</th>
									  <th class="warning">Current</th>
									  <th class="warning">30 Day Avg</th>
									  <th class="warning">7 Day Avg</th>
									  <th class="info">Current</th>
									  <th class="info">30 Day Avg</th>
									  <th class="info">7 Day Avg</th>
									  <th class="danger">Current</th>
									  <th class="danger">30 Day Avg</th>
									  <th class="danger">7 Day Avg</th>
									  
									</tr>
								    </thead>
								    
								    <tbody>
												
									<tr> <!--SITE ROW-->
									<!--<td><a href="dashboard_dev.php?gs=1"><i class="fa fa-search" aria-hidden="true"></i></a> <i class="fa fa-file-excel-o" aria-hidden="true"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>	-->
									<td>Coming Soon</td>
									<td>JD Slator #1 Tanks</td>
									<td class="success">Good</td>
									<td><a href="dashboard_dev.php?c=1&d=226&g=790"><?php echo convert16((15.6-getLastReading(226,790))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard_dev.php?c=1&d=226&g=792"><?php echo convert16((15.6-getLastReading(226,792))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard_dev.php?c=1&d=226&g=794"><?php echo convert16((15.6-getLastReading(226,794))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
																											
	
									</tr> <!--SITE ROW-->

									<tr> <!--SITE ROW-->
<!--									<td><i class="fa fa-search" aria-hidden="true"></i> <i class="fa fa-file-excel-o" aria-hidden="true"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>	-->
									<td>Coming Soon</td>	
									<td>PJ Lea A&B Tanks</td>
									<td class="success">Good</td>
									<td><a href="dashboard_dev.php?c=1&d=224&g=790"><?php echo convert16((15.6-getLastReading(224,790))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard_dev.php?c=1&d=224&g=792"><?php echo convert16((15.6-getLastReading(224,792))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
	
									</tr> <!--SITE ROW-->
									
									<tr> <!--SITE ROW-->
<!--									<td><i class="fa fa-search" aria-hidden="true"></i> <i class="fa fa-file-excel-o" aria-hidden="true"></i> <i class="fa fa-file-pdf-o" aria-hidden="true"></i></td>	-->
									<td>Coming Soon</td>
									<td>PJ Lea C&D Tanks</td>
									<td class="success">Good</td>
									<td><a href="dashboard_dev.php?c=1&d=224&g=794"><?php echo convert16((15.6-getLastReading(224,794))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td><a href="dashboard_dev.php?c=1&d=224&g=796"><?php echo convert16((15.6-getLastReading(224,796))); ?></a></td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
	
									</tr> <!--SITE ROW-->

									</tbody><!--SITE TBODY-->
								    
								    
								    
								    
			  </table>
        
        
        
            <?php  } ?>  
            
            
            
             <?php  if (isset($_GET['pl'])) { 
			  
			  
			  		$columns = getPMFieldArray();	
					
					$data = getPMFieldData();
					
						/*echo "<pre>";
						print_r($data);*/
			  
			  	?>  
        
        
        		 <h1 class="page-header">Plunger Lift Analysis</h1>
     	        
     	         <div class="alert alert-info" role="alert">Click a reading to see reading history</div>
      	        
      	        	<p>[<a href="dashboard_dev.php?pls=1">choose columns</a>]</p>
       	        
       	             
          				<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr><th>Timestamp</th>
									  	<?php  foreach ($columns as $col) {
					
												echo '<th>'.$col['register_name'].'</th>';


												} 
										?> 								  
									</tr>
								    </thead>
								    
								    <tbody>
								    
								    		<tr>
												
											<?php  
												
												echo '<td>'.formatTimestamp($data[0]['register_date']).'</td>';
	
												foreach ($data as $rd) {
					
												echo '<td><a href="dashboard_dev.php?c=1&d='.$rd['register_deviceID'].'&g='.$rd['register_number'].'">'.$rd['register_reading'].'</a></td>';


												} 
										?> 			
												
											</tr>
								

									</tbody><!--SITE TBODY-->
								    
								    
								    
								    
			  					</table>
        	        
        
        
            <?php  } ?>  
            
            
            
            <?php  if (isset($_GET['pls'])) { ?>  
            
            	
            		<h1 class="page-header">Plunger Lift Column Settings</h1>
            		
            		<form method="post" action="process/pm_fields.php">
            		
            		<?php  
							$array = getPMArray();				 
											 
							$fields = getPMColumns();
											 
							//print_r($array);
							
							foreach ($fields as $field) {
								
								if (in_array($field['register_number'],$array['pmfield'])) {
									echo '<p><input name="pmfield[]" type="checkbox" value="'.$field['register_number'].'" checked="checked"> '.$field['register_name'].'</p>';
								} else {
									echo '<p><input name="pmfield[]" type="checkbox" value="'.$field['register_number'].'"> '.$field['register_name'].'</p>';
								}
								
								
							}
							
					?>  
           
            			<button type="submit" class="btn btn-default">Submit</button>
       				
        				</form>
        	
            
            
             <?php  } ?> 
            
            
             <?php  if (isset($_GET['gs'])) { ?>  
        
        
        		 <h1 class="page-header">Gauge Sheet</h1>
        		 
        		<div class="row" style="margin-bottom: 10px;">
				<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
				<span></span> <b class="caret"></b></div>
       			</div>
        
          			<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									<tr>
										<th colspan="2">&nbsp;</th>
										<th colspan="4" class="warning">Oil Tank 1</th>
										<th colspan="4" class="info">Oil Tank 2</th>
										<th colspan="4" class="danger">Water Tank</th>
									</tr>
									
									<tr>
								  	  <th>Day</th>
								  	  <th>Diary</th>
								  	  
									  <th class="warning">Top Level</th>
									  <th class="warning">Oil Stock</th>
									  <th class="warning">Oil Hauls</th>
									  <th class="warning">BOPD</th>
									  
									  <th class="info">Top Level</th>
									  <th class="info">Oil Stock</th>
									  <th class="info">Oil Hauls</th>
									  <th class="info">BOPD</th>
									  
									  <th class="danger">Top Level</th>
									  <th class="danger">Oil Stock</th>
									  <th class="danger">Oil Hauls</th>
									  <th class="danger">BOPD</th>
									  
									  
									</tr>
								    </thead>
								    
								    <tbody>
									

									 <?php  for ($i=1; $i<=31; $i++) { ?>  						
										<tr> <!--SITE ROW-->
										<td><?php echo $i;?></td>	
										<td><i class="fa fa-book gauge_diary" aria-hidden="true"></i></td>
										<td>&nbsp</td>	
										<td>&nbsp</td>
										<td>&nbsp</td>
										<td>&nbsp</td>
										<td>&nbsp</td>	
										<td>&nbsp</td>
										<td>&nbsp</td>
										<td>&nbsp</td>										
										<td>&nbsp</td>	
										<td>&nbsp</td>
										<td>&nbsp</td>
										<td>&nbsp</td>
										</tr> <!--SITE ROW-->
									 <?php  }?>  
									

									</tbody><!--SITE TBODY-->
								    
								    
								</table>
								
								
								    <div id="diary_modal" title="Gauge Diary" style="display: none;">
								    
								    		<form method="post" action="">
											<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
											 <input type="hidden" name="log_field_type" value="Calculation">
											 
											   <div class="form-group">
											  
												<label for="log_book_field">Entry</label>
												<textarea class="form-control" rows="3"></textarea>
												

											  </div>
											  
											  <button type="submit" class="btn btn-default">Submit</button>
											</form>
											
											
											<ul class="list-group" style="margin-top: 20px;">

	
												<li class="list-group-item">Note here 5/3/2017 by Keystone</li>
												<li class="list-group-item">Note here 5/3/2017 by Keystone</li>
											
											</ul>
								    
								    	
							   
							   
  									</div>
        
        
        
            <?php  } ?>  
        
        
        
        
        
        
         <?php  if (isset($_GET['po'])) { ?>  
        
        
        		 <h1 class="page-header">POC Summary</h1>
        		 
        		 <div class="alert alert-info" role="alert">Click a reading to see reading history</div>
        
        
          					<div class="table-responsive">
           
             
								<table class="table table-striped" style="width:5000px;"><!--STATE TABLE-->

									<thead>
									
									<tr>
									  <th>POC Name</th>
									  	<th>Comm Status</th>
										<th>Controller Mode</th>
										<th>Operation Mode</th>
										<th>Auto Downtime Mode</th>
										<th>Inferred Prod current bbl</th>
										<th>Inferred Prod yesterdays bbl</th>
										<th>Percent Runtime current percent</th>
										<th>Percent Runtime yesterdays percent</th>
										<th>Todays Total Strokes</th>
										<th>Yesterdays Total Strokes</th>
										<th>Timed On Hours</th>
										<th>Timed On Minutes</th>
										<th>Timed Off Hours</th>
										<th>Timed Off Minutes</th>
										<th>Pump Fillage Last Stroke</th>
										<th>Ave Gross Pump Efficiency %</th>
										<th>Pump Fillage Violation Setpoint %</th>
										<th>Gauge Off Time Hours</th>
										<th>Peak Load lbs</th>
										<th>Pump Card Min Load</th>
										<th>Stroke Length inches</th>
										<th>Rod Odometer</th>
										<th>Reserved</th>
										<th>Strokes Per Minute</th>								  
									</tr>
								    </thead>
								    
								    <tbody>
								    
								<!-- 1 through 24 PJ Lea A2 Read Registers
									51 through 74 PJ Lea B3 Read Registers
									101 through 124 PJ Lea B4 Read Registers
									151 through 174 PJ Lea B5 Read Registers
									201 through 224 PL Lea C1 Read Registers
									251 through 274 PJ Lea C2 Read Registers
									301 through 324 PJ Lea C3 Read Registers
									351 through 374 PJ Lea D1 Read Registers-->

												
										
									<tr>
									  <td>PJ Lea A2</td>
							  			<?php echo getReadingsDeviceRegRange(224,1,24); ?>  
									</tr>
									
									<tr>
									  <td>PJ Lea B3</td>
							  			<?php echo getReadingsDeviceRegRange(224,51,74); ?>  
									</tr>										
										
									<tr>
									  <td>PJ Lea B4</td>
							  			<?php echo getReadingsDeviceRegRange(224,101,124); ?>  
									</tr>								
									
									<tr>
									  <td>PJ Lea B5</td>
							  			<?php echo getReadingsDeviceRegRange(224,151,174); ?>  
									</tr>	

									<tr>
									  <td>PJ Lea C1</td>
							  			<?php echo getReadingsDeviceRegRange(224,201,224); ?>  
									</tr>	
									
									<tr>
									  <td>PJ Lea C2</td>
							  			<?php echo getReadingsDeviceRegRange(224,251,274); ?>  
									</tr>	
									
									<tr>
									  <td>PJ Lea C3</td>
							  			<?php echo getReadingsDeviceRegRange(224,301,324); ?>  
									</tr>	
									
									<tr>
									  <td>PJ Lea D1</td>
							  			<?php echo getReadingsDeviceRegRange(224,351,374); ?>  
									</tr>	
																																		
									<tr>
									  <td>JD Slator 1</td>
							  			<?php echo getReadingsDeviceRegRange(226,1,23); ?>  
									</tr>	
																																				
									</tbody><!--SITE TBODY-->
								    
								    
								    
								    
			 					 </table>
        
        
        
            <?php  } ?>  
            
            
            
            
                     <?php  if (isset($_GET['ct'])) { ?>  
        
        
        		 <h1 class="page-header">County/Field Overview</h1>
        		 
        		 
       		      		<h2>Wells</h2>
        		      
          					<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr>

									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  
									</tr>
								    </thead>
								    
								    <tbody>
												
								

									</tbody><!--SITE TBODY-->
		
								    
									  </table>
									  
			  					</div>
			  					
			  					
			  					
			  					
			  					<h2>Compressors</h2>
        		      
          					<div class="table-responsive">
           
             
								<table class="table table-striped"><!--STATE TABLE-->

									<thead>
									
									<tr>

									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  <th>Column</th>
									  
									</tr>
								    </thead>
								    
								    <tbody>
												
								

									</tbody><!--SITE TBODY-->
		
								    
									  </table>
									  
			  					</div>
			  					
			  					
			  					<h2>Field Variance</h2>
			  					
			  					<div class="alert alert-success" role="alert">
										Variance in MCF
								</div>
			  					
			  					

        
        
          <?php  } ?> 


         


        <?php  if ($_GET['m'] == 1) { ?>  

          	<h2 class="sub-header">Location & Status</h2>

			<div class="alert-success">Device Properly Working</div>
			<div id="map"></div>

        <?php  } ?>  

        <?php  if ($_GET['f'] == 1) { ?>  
          	


          	<h2 class="sub-header">Document Manager</h2>

			<div id="elfinder"></div>

        <?php  } ?>  
        
        

        <?php  if ($_GET['c'] == 1) { ?>  

            	
          	 <?php  if ($_GET['g']) { ?>  
          	 
          	 
          	 		<?php if ($_GET['start'] ) { ?>
				   
						<h2 class="sub-header"><?php echo $_GET['start'];?> - <?php echo $_GET['end'];?>  Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g']); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>&start=<?php echo $_GET['start'];?>&end=<?php echo $_GET['end'];?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } else { ?>
           
						<h2 class="sub-header">3 Day Register Readings - <?php echo getRegisterName($_GET['d'],$_GET['g']); ?> <a href="excel_readings.php?d=<?php echo $_GET['d']; ?>&g=<?php echo $_GET['g']; ?>"><i class="fa fa-table" aria-hidden="true" title="Download All Readings"></i></a></h2>
				   
				    <?php } ?>
          	 

          	  	
          	  	<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>GMT Date</th>
							  <th>TZ Date</th>
							  <th>Reading</th>
							</tr>
							
						<?php $readings = getRegisterReadings($_GET['d'],$_GET['g']); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {

									echo '<tr>
											<td>'.$reading['register_number'].'</td>
											<td>'.formatTimestamp($reading['register_date']).'</td>
											<td>'.formatTimestampOffset($reading['register_date'],7).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  </thead>
						  <tbody>
          	 
          	 
          	 <?php  } else  { ?>  
          	 
          	  	<h2 class="sub-header">Reporting Configuration</h2>
          	  	
          	  	<p>Reporting Time Range <input name="config_start_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_start_time"); ?>"> to <input name="config_end_time" class="timepicker" type="text" value="<?php echo getAccountConfigTime("config_end_time"); ?>"> </p>
            
						<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							  <th>Label</th>
							  <th>Config</th>
							  <th>Measurement Factor</th>
							   <th>Alarm</th>
							  <th>Produce/Sell</th>
							  <th>View Readings</th>
							</tr>
						  </thead>
						  <tbody>

						<?php $regs = getRegistersByDevice($_GET['d']); 

							//echo "<pre>";
							//print_r($regs);

							foreach ($regs as $reg) {
								
								$noprodsell = '';
								$prodsell = '';
								
								if ($reg['register_prod_sell'] == 1) {
									$noprodsell = ' checked="checked" ';
								}
								
								if ($reg['register_prod_sell'] == 2) {
									$prodsell = ' checked="checked" ';
								}
								
								if ($reg['register_config'] != '') {
									$configset = '  class="success" ';									
								} else {
									$configset = '';	
								}
								
								if ($reg['register_label'] == '') {
									
									$reg['register_label'] = $reg['register_name'];
								}
								

								echo '<tr '.$configset.'>
										<td id="'.$reg['register_number'].'">'.$reg['register_number'].'</td>
										<td><input name="register_label" class="reglabel" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="25" type="text" value="'.$reg['register_label'].'"> <i class="fa fa-info-circle fa-lg" aria-hidden="true" title="#'.$reg['register_number'].'<br>'.$reg['register_label'].'<br>'.$reg['register_name'].'"></i></td>
										<td>'.getRegistersConfigSelect($_GET['d'],$reg['register_number'],$reg['register_config']).'</td>
										<td><input name="measurement_factor" class="regfactor" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'" size="8" type="text" value="'.$reg['register_factor'].'"></td>
										<td>&nbsp;</td>
										<td><input name="produce_sell_'.$reg['register_number'].'" class="regprodsell" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  type="radio" value="1" '.$noprodsell.' > No <input name="produce_sell_'.$reg['register_number'].'" data-id="'.$reg['register_number'].'"  data-type="'.$_GET['d'].'"  class="regprodsell" type="radio" value="2"  '.$prodsell.'> Yes</td>
										<td><a href="dashboard_dev.php?r='.$_GET['r'].'&d='.$_GET['d'].'&c=1&g='.$reg['register_number'].'">View</a></td>
									</tr>'; 	

							}

						?>

                         </tbody>
            		</table>
            
            	 <?php  } ?>  

        <?php  } ?>  



     
        
         <?php  if ($_GET['b'] == 1) { 
							  
							  
		 ?>  
		
		
			<h2 class="sub-header">Digital Log Book [<a href="#" id="entryview">entry view</a>]</h2>
			
			<p><strong>Add New Field </strong> - Please choose a type: </p>
			
			 
			
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="1"> This field will compare entered log book values to auto calculated values
				  </label>
				</div>
				
				<div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="2"> This field will be a calculation of up to 3 other fields
				  </label>
				</div>
				
			  <div class="radio">
				  <label>
					<input type="radio" name="fieldtype" class="logfield" value="3"> This field is for entry only
				  </label>
				</div>
				

					
				<div id="log_field1" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Comparison">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
						<?php echo  getLogCompareField(); ?> 
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				
				<div id="log_field2" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Calculation">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Calculated Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Calculated Field Name"> = 
						
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name"> 
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
						
						<?php echo  getMathSelect(); ?> 
						<label for="log_book_calcs" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_calcs[]" placeholder="Field Name">
									
						
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	

				<div id="log_field3" style="display: none;">

					<form method="post" action="process/add_log_book_field.php"  class="form-inline">
					<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
				     <input type="hidden" name="log_field_type" value="Entry Only">
					  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit Field</button>
					</form>

				</div>	
		
				
				<div id="entry_modal" title="Log Book Entry" style="display: none;">
						
					<h4>Enter Reading</h4>
					
					   <div class="table-responsive">
						<table class="table table-striped" style="width:600px;">

						  <tbody>

						  <tr>
							  <td>Date</td>
							  <td><input type="text" class="form-control datepicker" name="reading_date" placeholder="Enter Date"></td>
						</tr>
						
						<tr>
							  <td>Time</td>
							  <td><input type="time" class="form-control" name="reading_time" placeholder="Enter Time"></td>
						</tr>
						
						
						 <?php 

								$fields = getLogBookFieldsByDevice($_GET['d']);

								//print_r($fields);

							foreach ($fields as $key=>$field) {  

							echo '<tr>
									  <td>'.$field['log_book_field'].'</td>
									  <td><input type="text" class="form-control" name="'.$field['logfieldID'].'" placeholder="Enter Value"></td>
								</tr>';

							}
					   ?>

						              </tbody>
            </table>
          </div>
						<p>Comments</p>

				</div>


        
         
          <div class="table-responsive" style="margin-top: 30px;">
           
            <p><strong>Current Fields </strong></p>
           
            <table class="table table-striped" style="width:700px;">

              <tbody>
              
<!--              <tr>
				  <td>Date</td>
				  <td><input type="text" class="form-control datepicker" name="'.$field['logfieldID'].'" placeholder="Enter Date"></td>
			</tr>-->
               
               <?php 
					
						$fields = getLogBookFieldsByDevice($_GET['d']);
						
						//print_r($fields);

					foreach ($fields as $key=>$field) {  
						
						if ($field['log_field_type'] == 'Comparison') {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>Compare to: '.$field['log_book_comparison'].'</td>
						   </tr>';
	
						} else if ($field['log_field_type'] == 'Calculation') {
							
							$calcfields = unserialize($field['log_book_calcs']);
							$calcops = unserialize($field['log_book_math']);
							$calcstring = '';
							
							foreach ($calcfields as $key=>$calc) {
								
								$calcstring .= $calc.' '.$calcops[$key].' ';
							}
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>'.$calcstring.'</td>
						   </tr>';

					
						} else  {
							
							 echo '<tr>
							  <td><a href="#" class="log_field_edit" data-id="'.$field['logfieldID'].'">'.$field['log_book_field'].'</a></td>
							  <td>'.$field['log_field_type'].'</td>
							  <td>&nbsp;</td>
						   </tr>';
														
						}
						
			   		 
					
					}
			   ?>
              </tbody>
            </table>
          </div>
          
          
          
          	   <div id="log_modal" title="Edit Log Book Field" style="display: none;">
          	   
          	   	<form method="post" action="process/edit_log_book_field.php"  class="form-inline">
          	   	<input type="hidden" name="deviceID" value="<?php echo $_GET['d']; ?>">
					<input type="hidden" name="logfieldID" id="logfieldID_edit">
						  <div class="form-group">
						<label for="log_book_field" class="sr-only">Field Name</label>
						<input type="text" class="form-control" name="log_book_field" id="log_book_field_edit" placeholder="Field Name">
					  </div>
					  <button type="submit" class="btn btn-default">Submit</button>
					</form>
							   
							   
			</div>

        <?php  } ?>  




        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../beta/jquery/external/jquery/jquery.js"></script>
		
    <script src="../beta/bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="../beta/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../beta/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    

    <?php  if ($_GET['f']) { ?>  
    <!-- Require JS (REQUIRED) -->
	<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
	<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    <?php  } ?> 
    
    
    <?php  if ($_GET['s'] or $_GET['g'] or !isset($_GET['d'])) { ?>  
    
     <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />


	<script>
               $(function () {
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(29, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['s'] ) { ?>
					   		
					   		window.location.replace("dashboard_dev.php?d=<?php echo $_GET['d']; ?>&s=<?php echo $_GET['s']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
					   <?php } else if ($_GET['g'] ) { ?> 
					   
					   		window.location.replace("dashboard_dev.php?d=<?php echo $_GET['d']; ?>&c=1&g=<?php echo $_GET['g']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

					   <?php } else if ($_GET['r'] && !$_GET['s'] ) { ?> 
					   
					   		window.location.replace("dashboard_dev.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
												
					   <?php } else { ?> 
					   
					   		window.location.replace("dashboard_dev.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>


 	<?php  } ?> 
 	
 	
 	<?php  if (!$_GET['m'] && !$_GET['c'] && !$_GET['b'] && !$_GET['f']  && !$_GET['gs'] ) { ?>  
   
   	<?php  if ($_GET['s'] != 4) { ?>  
   	
    	<script>
               $(function () {
				   
				   <?php  if (!isset($_SESSION['chart_type']) or $_SESSION['chart_type'] == 1 ) { ?>  
                
				   Highcharts.chart('chart', {
                    title: {
                        text: 'Site<?php 	//echo getDeviceName($_GET['d']);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
                    },
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('process/time_series.php'); ?>
                    }, {
                        name: 'Oil Sales',
                        data: <?php include('process/time_series_sales.php'); ?>
                    }, {
                        name: 'Water Production',
                        data: <?php include('process/time_series_water.php'); ?>
                   }, {
                        name: 'Water Trucked',
                        data: <?php include('process/time_series_water_trucked.php'); ?>
                   }]
                });
				   
				<?php  } ?>  
	
				 
				   <?php  if ($_SESSION['chart_type'] == 2 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'waterfall',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d']);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('process/time_series.php'); ?>
                    }, {
                        name: 'Oil Sales',
                        data: <?php include('process/time_series_sales.php'); ?>
                    }, {
                        name: 'Water Production',
                        data: <?php include('process/time_series_water.php'); ?>
                   }, {
                        name: 'Water Trucked',
                        data: <?php include('process/time_series_water_trucked.php'); ?>
                   }]
                });
				   
				<?php  } ?>  
			   				   
	
				   
				 <?php  if ($_SESSION['chart_type'] == 3 ) { ?>  
                
				   Highcharts.chart('chart', {
					   
					        chart: {
								type: 'bar',

							},
					   
                    title: {
                        text: '<?php 	echo getDeviceName($_GET['d']);
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                        xAxis: {
							
							type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day
						tickWidth: 0,
						gridLineWidth: 1,
						labels: {
							align: 'left',
							x: 3,
							y: -3
						}
					},
					   
                    yAxis: {
                        title: {
                            text: 'Barrels (bbl)'
                        },
                      
                    },
                    tooltip: {
                        valueSuffix: 'bbl'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Oil Production',
                        data: <?php include('process/time_series.php'); ?>
                    }, {
                        name: 'Oil Sales',
                        data: <?php include('process/time_series_sales.php'); ?>
                    }, {
                        name: 'Water Production',
                        data: <?php include('process/time_series_water.php'); ?>
                   }, {
                        name: 'Water Trucked',
                        data: <?php include('process/time_series_water_trucked.php'); ?>
                   }]
                });
				   
				<?php  } ?>  
				   
				   
			   
			   });
			
		</script>
	
		
		<?php  } ?>  
		
		<?php  if ($_GET['s'] == 4) { ?> 
		
		<script>
			
			$(function () {

				Highcharts.chart('container-speed', {

					chart: {
						type: 'gauge',
						plotBackgroundColor: null,
						plotBackgroundImage: null,
						plotBorderWidth: 0,
						plotShadow: false
					},

					title: {
						text: '<?php echo $pressures[0]['register_label']; ?>'
					},

					pane: {
						startAngle: -150,
						endAngle: 150,
						background: [{
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFF'],
									[1, '#333']
								]
							},
							borderWidth: 0,
							outerRadius: '109%'
						}, {
							backgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#333'],
									[1, '#FFF']
								]
							},
							borderWidth: 1,
							outerRadius: '107%'
						}, {
							// default background
						}, {
							backgroundColor: '#DDD',
							borderWidth: 0,
							outerRadius: '105%',
							innerRadius: '103%'
						}]
					},
					
					credits: {
						enabled: false
					},

					// the value axis
					yAxis: {
						min: -20,
						max: 20,

						minorTickInterval: 'auto',
						minorTickWidth: 1,
						minorTickLength: 10,
						minorTickPosition: 'inside',
						minorTickColor: '#666',

						tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
						tickColor: '#666',
						labels: {
							step: 2,
							rotation: 'auto'
						},
						title: {
							text: 'psi'
						},
						plotBands: [{
							from: <?php echo $gauge['gauge_start_good']; ?>,
							to: <?php echo $gauge['gauge_end_good']; ?>,
							color: '#55BF3B' // green
						}, {
							from: <?php echo $gauge['gauge_start_warn']; ?>,
							to: <?php echo $gauge['gauge_end_warn']; ?>,
							color: '#DDDF0D' // yellow
						}, {
							from: <?php echo $gauge['gauge_start_bad']; ?>,
							to: <?php echo $gauge['gauge_end_bad']; ?>,
							color: '#DF5353' // red
						}]
					},

					series: [{
						name: '<?php echo $pressures[0]['register_label']; ?>',
						data: [<?php echo $pressures[0]['register_reading']; ?>],
						tooltip: {
							valueSuffix: ' psi'
						}
					}]

				});
			});
			
	
			</script>

		<?php  } ?>  
		
 	<?php  } ?>  
   
   
    
      <?php  if ($_GET['m'] == 1) { ?>  

				<?php include('includes/google_maps.php'); ?>
                
        <?php  } ?>  

		<?php include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

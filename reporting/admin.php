<?php 
session_start();
include('connections/mysql.php');
include('functions/functions.php');
include('app/functions.php');

if ($_SESSION['auth'] != 1) {
	header('location: login.php');
	exit();		
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="../beta/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../beta/bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../beta/bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="../beta/bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery.timepicker.css"/>
	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Admin - <?php echo $_SESSION['user_name']; ?> </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li <?php if (!isset($_GET['f'])) echo 'class="active"' ?>><a href="index.php">Accounts</a></li>
            <li <?php if ($_GET['f'] == 'getAdminUsers') echo 'class="active"' ?>><a href="admin.php?f=getAdminUsers">Admin Users</a></li>
            <li><a href="process/logout.php">Logout</a></li>

         </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li <?php if (!isset($_GET['f'])) echo 'class="active"' ?>><a href="admin.php">Accounts <span class="sr-only">(current)</span></a></li>
            <li <?php if ($_GET['f'] == 'getAdminUsers') echo 'class="active"' ?>><a href="admin.php?f=getAdminUsers">Admin Users</a></li>
            <li><a href="process/logout.php">Logout</a></li>
          </ul>
          
      
        
        </div>
   
             <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
         
         		
         		<?php if (!isset($_GET['f'])) {  ?>
         		
					<h1 class="page-header">Accounts</h1>


						<table class="table table-striped">

						<thead><tr><th>Account Name</th><th>Active</th><th>Last Updated</th><th>Devices</th><th>Users</th></tr></thead>

							<tbody>

							<?php $accts = getAccounts();  

									foreach ($accts as $acct) {

										echo '<tr><td><a href="process/set_account.php?id='.$acct['account_id'].'">'.$acct['account_name'].'</a></td><td>'.($acct['account_active'] == 1 ? 'Yes' : 'No').'</td><td>'.formatTimestamp($acct['account_updated']).'</td><td><a href="admin.php?f=getDevices&id='.$acct['account_id'].'">View</a></td><td><a href="admin.php?f=getUsers&id='.$acct['account_id'].'">Manage</a></td></tr>';

									}

							 ?>

							</tbody>

						</table>
      	 
      				 <?php } ?>
      				 
      				<?php if ($_GET['f']) {  
				 			
									//echo $_GET['f']();
				 			
							}	
				 
				 		?>
      				 
      				 
      				<?php if ($_GET['f'] == 'getDevices') {  ?>
         		
					<h1 class="page-header">Devices</h1>


						<table class="table table-striped">

						<thead><tr><th>Device Name</th><th>Last Report</th></tr></thead>

							<tbody>

							<?php $accts = getDevicesByAccount($_GET['id']);  

									foreach ($accts as $acct) {

										echo '<tr><td>'.$acct['device_name'].'</td><td>'.formatTimestamp($acct['device_last_report']).'</td></tr>';

									}

							 ?>

							</tbody>

						</table>
      	 
      				 <?php } ?>
      	 
      	 
	      	 </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../beta/jquery/external/jquery/jquery.js"></script>
		
    <script src="../beta/bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="../beta/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.timepicker.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../beta/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    

  		<?php include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

<?php 
function generate_random_string($name_length = 9) {
	$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!%$#*@';
	return substr(str_shuffle($alpha_numeric), 0, $name_length);
}


function formatDateMYSQL($date) {
//// this function is to format date for MYSQL


	if ($date == '') {
		
		return NULL;
		
	} else {
		
		$parts = explode("/",$date);
	
		return $parts[2]."-".$parts[0]."-".$parts[1];
	
	}
} //formatDateMYSQL


function formatDate($timestamp) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("m/d/Y", strtotime($timestamp));
	
	return $result;
	}
	
} //formatDate


function formatTimestamp($timestamp) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("m/d/Y g:i A", strtotime($timestamp) );
	
	return $result;
	}
	
} //formatTimestamp


function getRegisterValue($reg,$sub,$date) {
	
	
	$qry = mysql_query("SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 177 AND date(register_date) = '$date' AND hour(register_date) = '11' ORDER BY register_date DESC LIMIT 1 ");
	$row = mysql_fetch_assoc($qry);
	
	if ($sub) {
		$qry2 = mysql_query("SELECT * FROM tbl_register_history WHERE register_number = $sub AND register_deviceID = 177 AND date(register_date) = '$date' AND hour(register_date) = '11' ORDER BY register_date DESC LIMIT 1 ");
		$row2 = mysql_fetch_assoc($qry2);

		return ($row['register_reading'] - $row2['register_reading']);
		
	} else {
		return $row['register_reading'];
	}
	
}








function getRegisterValues($reg,$date,$date2) {
	
	$qry = mysql_query("SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 177 AND date(register_date) = '$date' AND hour(register_date) = '11' ORDER BY register_date DESC LIMIT 1 ");
	$row = mysql_fetch_assoc($qry);

	$qry = mysql_query("SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 177 AND register_date BETWEEN '".$row['register_date']."' AND '".$date2." 11:59:59' ORDER BY register_date DESC ");
	
	while ($row = mysql_fetch_assoc($qry)) {
		
		$array[] = $row;	
		
	}
	
	$production = 0;
	$sales = 0;
	$last = count($array) - 1;
	
	foreach ($array as $key=>$reading) {
		
		//If (New – Old) > 0, add to production
		//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

		if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
			$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
			$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
			$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		}
			 	
		
	}
		
		
	$prevday = ($array[$last]['register_reading']);
	$today = number_format(($array[$last]['register_reading']+$production),2);

	return array($array,$prevday,$production,$sales,$today,$array[0]['register_reading']);
	
	
}




function getMTDValue($type) {
	
	if ($type == 1) {

		$qry = mysql_query("SELECT sum(total_BOPD) as totalval FROM tbl_daily_totals WHERE month(total_date) = '".date('m')."' AND year(total_date) = '".date('Y')."' ");
		
	} else if ($type == 2) {
	
		$qry = mysql_query("SELECT sum(total_BOSold) as totalval FROM tbl_daily_totals WHERE month(total_date) = '".date('m')."' AND year(total_date) = '".date('Y')."' ");
	}
	
	$row = mysql_fetch_assoc($qry);


	return $row['totalval'];
	
	
}



function getLocations() {
	
	$html = '<div id="locations">';

	$html .= '<table width="100%" border="1" cellspacing="0" cellpadding="5" id="locations_table">';

	$html .= '<thead><tr><th>Status</th><th>Site Name</th><th>Configuration</th><th>SMS</th><th>Last Report</th><th>Last Restart</th></tr></thead><tbody>';

	$qry = mysql_query("SELECT * FROM tbl_devices WHERE device_accountID = ".$_SESSION['account_id']."   ");

	while ($data = mysql_fetch_assoc($qry)) {
		
		$xml = simplexml_load_file("https://questdashboard.net/datamine.php?querytype=pingdevice&deviceid=".$data['device_deviceID']."");
		
		//echo "<pre>";
		//print_r($xml);
		if ($xml->success == 1) {
			$status = 'Live';
			$statusstyle = 'style="background-color:#009966;color:#FFF;"';		
		} else {
			$status = 'Offline';		
			$statusstyle = 'style="background-color:#CC0000;color:#FFF;"';		
		}
		
		$html .= '<tr><td '.$statusstyle.'>'.$status.'</td><td><a href="dashboard.php?f=getRegisters&id='.$data['device_deviceID'].'">'.$data['device_name'].'</a></td><td><i class="fa fa-file-image-o" data-name="'.$data['device_name'].'"> <span data-name="'.$data['device_name'].'">View Site Configuration</span> </i></td><td>'.$data['device_sms'].'</td><td>'.formatTimestamp($data['device_last_report']).'</td><td>'.formatTimestamp($data['device_restart']).'</td></tr>';

		
	}
	

	$html .= '</tbody><tfoot></tfoot></table></table>';

	$html .= '</div>';

	$html .= '<div id="map"></div>';

	$html .= '<div id="config_modal" style="display:none;"></div>';

	return $html;
	
	
}


function getLocationsGeo() {


	$qry = mysql_query("SELECT * FROM tbl_devices WHERE device_accountID = ".$_SESSION['account_id']."   ");

	while ($data = mysql_fetch_assoc($qry)) {
		
		$array[] = array($data['device_name'],$data['device_latitude'],$data['device_longitude']);
	
	}


	  return json_encode($array);;

}



function getRegisterReading($id,$reg) {
	
	
	$qry = mysql_query("SELECT * FROM tbl_register_history WHERE register_deviceID = $id AND register_number = $reg ORDER BY register_date DESC LIMIT 1 ");
	$row = mysql_fetch_assoc($qry);

	return $row;
	
}




function getRegisters() {
	
	$html = '<div id="locations">';

	$html .= '<table width="100%" border="1" cellspacing="0" cellpadding="5" id="devices_table">';

	//$html .= '<thead><tr><th>Register Name</th></tr></thead><tbody>';

	$html .= '<thead><tr><th>Tools</th><th>Register Name</th><th>Unit</th><th>Last Reading</th><th>Last Reading Time</th></tr></thead><tbody>';

/*	$qry = mysql_query("SELECT * FROM tbl_registers WHERE register_deviceID = ".$_GET['id']." ORDER BY register_name ");

	while ($data = mysql_fetch_assoc($qry)) {
		
		$reading = getRegisterReading($_GET['id'],$data['register_number']);
		
		if ($_GET['n'] == $data['register_number']) {
			$class = 'class="active"';
			$_SESSION['currentname'] = $data['register_name'];
			$_SESSION['currentunit'] = $data['register_unit'];
		} else {
			$class = '';		
		}
		
		$html .= '<tr '.$class.'><td><i class="fa fa-pencil" data-name="'.$data['register_name'].'"> <span data-name="'.$data['register_name'].'">Notes</span> </i> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-clock-o" data-name="'.$data['register_name'].'"> <span data-name="'.$data['register_name'].'">Alarms</span> </i> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-rss" data-name="'.$data['register_name'].'"> <span data-name="'.$data['register_name'].'">Ping</span> </i> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-area-chart"> <a href="dashboard.php?f=getRegisters&id='.$_GET['id'].'&n='.$data['register_number'].'" class="fa">Chart</a> </i> </i> </td><td>'.$data['register_name'].'</td><td>'.$data['register_unit'].'</td><td>'.$reading['register_reading'].'</td><td>'.formatTimestamp($reading['register_date']).'</td></tr>';

		
	}
	*/

	$html .= '</tbody><tfoot></tfoot></table></table>';

	$html .= '</div>';


	if ($_GET['n']) {
		$qry = mysql_query("SELECT * FROM tbl_registers WHERE register_deviceID = ".$_GET['id']." AND register_number = ".$_GET['n']."  ");
		$data = mysql_fetch_assoc($qry);
		$_SESSION['currentname'] = $data['register_name'];
		$_SESSION['currentunit'] = $data['register_unit'];

		$html .= '<div id="chart" style="min-width: 800px; height: 400px; margin: 0 auto"></div>';
	}

	$html .= '<div id="notes_modal" style="display:none;">
			
			<form action="process/add_note.php" method="post">
				<input name="regID" id="regID" type="hidden"  />
				<textarea name="register_note" cols="85" rows="10"></textarea>
				<input name="submit" type="submit" value="Submit" />
			</form>	
			
			<div id="notestable"></div>
	
	</div>';

	$html .= '<div id="alarms_modal" style="display:none;"><div id="alarmstable"></div></div>';

	$html .= '<div id="log_modal" style="display:none;">
			
			<form action="process/add_log.php" method="post">
				<input name="regID" id="regID" type="hidden"  />
				<input name="log_reading" type="text" placeholder="Reading"  />
				<input name="log_date" type="text" class="datepicker" placeholder="Date" />
				<input name="submit" type="submit" value="Submit" />
			</form>	
	
	
				<div id="logtable"></div>
		
	
	</div>';


	$html .= '<div id="haul_modal" style="display:none;">
			
			<form action="process/add_haul.php" method="post">
				<input name="regID" id="regID" type="hidden"  />
				<input name="haul_reading" type="text" placeholder="Reading"  />
				<input name="haul_date" type="text" class="datepicker" placeholder="Date" />
				<input name="submit" type="submit" value="Submit" />
			</form>	
			
			
			<div id="haultable"></div>

	
	</div>';



	$html .= '<div style="height:500px;"></div>';

	return $html;
	
	
}



function getDeviceCount($id) {
	
	
	$qry = mysql_query("SELECT * FROM tbl_devices WHERE device_accountID = $id ");
	$count = mysql_num_rows($qry);

	return $count;
	
}




function getDeviceSelect($current,$id,$num)  {

	$html = '<select name="device_deviceID[]" class="devsel" data-id="'.$num.'" style="width:200px;">';

	$html .= '<option value="">--Choose Device--</option>';

	$qry = mysql_query("SELECT * FROM tbl_devices WHERE device_accountID = $id ORDER BY device_name ");
	
		while ($data = mysql_fetch_array($qry)) {

			if ($current == $data['device_deviceID']) {
				$html .= '<option value="'.$data['device_deviceID'].'" selected="selected">'.$data['device_name'].'</option>';
			} else {
				$html .= '<option value="'.$data['device_deviceID'].'">'.$data['device_name'].'</option>';
			}			
			
		}

	$html .= '</select>';
	
	return $html;
}



function getRegisterSelect($id)  {

	$html = '<select name="register_id[]"  class="regsel" style="width:300px;margin-right:20px;">';

	$html .= '<option value="">--Choose Register--</option>';

	$qry = mysql_query("SELECT * FROM tbl_registers WHERE register_deviceID = $id ORDER BY register_name ");
	
		while ($data = mysql_fetch_array($qry)) {

				$html .= '<option value="'.$data['register_id'].'">'.$data['register_name'].'</option>';
			
		}

	$html .= '</select>';

	$html .= 'Alt Name <input name="alt_name[]" type="text" /> (optional)';

	
	return $html;
}




function getRegisterCount($id) {
	
	$count = 0;
	
	$qry = mysql_query("SELECT * FROM tbl_devices WHERE device_accountID = $id ");
	
	while ($data = mysql_fetch_assoc($qry)) {


		$sqry = mysql_query("SELECT * FROM tbl_registers WHERE register_deviceID = ".$data['device_deviceID']." ");
		$count += mysql_num_rows($sqry);


	}
	return $count;
	
}


function getAccountName($id) {
	
	$qry = mysql_query("SELECT * FROM tbl_accounts WHERE account_id = $id   ") or die(mysql_error());
	$data = mysql_fetch_assoc($qry);
	
	return $data['account_name'];	
	
}
		
		



function getAccounts() {
	
	$html = '<div id="locations">';

	$html .= '<table width="100%" border="1" cellspacing="0" cellpadding="5" id="accounts_table">';

	$html .= '<thead><tr><th>Account</th><th>Number Devices</th><th>Number Registers</th><th>Status</th></tr></thead><tbody>';

	$qry = mysql_query("SELECT * FROM tbl_accounts ORDER BY account_name   ");

	while ($data = mysql_fetch_assoc($qry)) {
		
		if ($data['account_active'] == 1) {
			$status = 'Active';
		} else {
			$status = 'Inactive';		
		}
		
		$html .= '<tr><td><a href="config.php?f=getConfig&id='.$data['account_id'].'">'.$data['account_name'].'</a></td><td>'.getDeviceCount($data['account_id']).'</td><td>'.getRegisterCount($data['account_id']).'</td><td>'.$status.'</td></tr>';

	}

	$html .= '</tbody><tfoot></tfoot></table></table>';

	$html .= '</div>';

	return $html;
	
	
}


function getConfig() {


	$html = '<div id="locations">';

		$html .= '<p><a href="config.php">&laquo; Back to Accounts</a></p>';
	
		$html .= '<h2>'.getAccountName($_GET['id']).'</h2>';
	
		$html .= '<form action="../process/email_config.php" method="post">';

		$html .= '<p><label>Send Email</label><input name="send_email" type="radio" value="1" checked="checked" /> Yes  <input name="send_email" type="radio" value="2" /> No</p>';
		$html .= '<p><label>Emails</label><input name="email_addresses" type="text" size="100" /></p>';
		$html .= '<p><label>Timing</label><input name="email_timing" type="radio" value="1" checked="checked" /> Daily  <input name="email_timing" type="radio" value="2" /> Hourly</p>';
		$html .= '<p><label>Add Grand Total</label><input name="grand_total" type="radio" value="1" checked="checked" /> Yes  <input name="grand_total" type="radio" value="2" /> No</p>';
		$html .= '<p><label>Add Month Total</label><input name="month_total" type="radio" value="1" checked="checked" /> Yes  <input name="month_total" type="radio" value="2" /> No</p>';
		$html .= '<p><label>Monthly Summary</label><input name="month_summary" type="radio" value="1" checked="checked" /> Yes  <input name="month_summary" type="radio" value="2" /> No</p>';


		$html .= '<hr>';

		$html .= '<h3>Production/Sales</h3>';

		$html .= '<div class="accordion">
		
					  <h3>Section 1</h3>
					
						  <div>
							<p><label>Label</label><input name="section_one_label" type="text" size="50" /></p>
							<p><label>Feet Factor</label><input name="feet_factor_one" type="text" size="10" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],1).' <span id="related_register_1"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],2).' <span id="related_register_2"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],3).' <span id="related_register_3"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],4).' <span id="related_register_4"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],5).' <span id="related_register_5"></p>
						  </div>
					
					  <h3>Section 2</h3>

						  <div>
							<p><label>Label</label><input name="section_two_label" type="text" size="50" /></p>
							<p><label>Feet Factor</label><input name="feet_factor_two" type="text" size="10" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],6).' <span id="related_register_6"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],7).' <span id="related_register_7"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],8).' <span id="related_register_8"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],9).' <span id="related_register_9"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],10).' <span id="related_register_10"></p>
						  </div>

					
					  <h3>Section 3</h3>
					
						  <div>
							<p><label>Label</label><input name="section_three_label" type="text" size="50" /></p>
							<p><label>Feet Factor</label><input name="feet_factor_three" type="text" size="10" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],11).' <span id="related_register_11"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],12).' <span id="related_register_12"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],13).' <span id="related_register_13"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],14).' <span id="related_register_14"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],15).' <span id="related_register_15"></p>
						  </div>

					  <h3>Section 4</h3>

						  <div>
							<p><label>Label</label><input name="section_four_label" type="text" size="50" /></p>
							<p><label>Feet Factor</label><input name="feet_factor_four" type="text" size="10" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],16).' <span id="related_register_16"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],17).' <span id="related_register_17"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],18).' <span id="related_register_18"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],19).' <span id="related_register_19"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],20).' <span id="related_register_20"></p>
						  </div>

					  <h3>Section 5</h3>

						  <div>
							<p><label>Label</label><input name="section_five_label" type="text" size="50" /></p>
							<p><label>Feet Factor</label><input name="feet_factor_five" type="text" size="10" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],21).' <span id="related_register_21"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],22).' <span id="related_register_22"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],23).' <span id="related_register_23"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],24).' <span id="related_register_24"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],25).' <span id="related_register_25"></p>
						  </div>

					  <h3>Section 6</h3>
					
						  <div>
							<p><label>Label</label><input name="section_six_label" type="text" size="50" /></p>
							<p><label>Feet Factor</label><input name="feet_factor_six" type="text" size="10" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],26).' <span id="related_register_26"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],27).' <span id="related_register_27"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],28).' <span id="related_register_28"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],29).' <span id="related_register_29"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],30).' <span id="related_register_30"></p>
						  </div>
		
					</div>';


		$html .= '<h3>Flow Meters</h3>';

		$html .= '<div class="accordion">
		
					  <h3>Section 1</h3>
					
						  <div>
							<p><label>Label</label><input name="section_seven_label" type="text" size="50" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],31).' <span id="related_register_31"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],32).' <span id="related_register_32"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],33).' <span id="related_register_33"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],34).' <span id="related_register_34"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],35).' <span id="related_register_35"></p>
						  </div>
					
					</div>';
	

		$html .= '<h3>Injected Waters</h3>';

		$html .= '<div class="accordion">
		
					  <h3>Section 1</h3>
					
						  <div>
							<p><label>Label</label><input name="section_eight_label" type="text" size="50" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],36).' <span id="related_register_36"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],37).' <span id="related_register_37"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],38).' <span id="related_register_38"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],39).' <span id="related_register_39"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],40).' <span id="related_register_40"></p>
						  </div>
					
					</div>';

	
		$html .= '<h3>Pressures</h3>';


		$html .= '<div class="accordion">
		
					  <h3>Section 1</h3>
					
						  <div>
							<p><label>Label</label><input name="section_nine_label" type="text" size="50" /></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],41).' <span id="related_register_41"></span></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],42).' <span id="related_register_42"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],43).' <span id="related_register_43"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],44).' <span id="related_register_44"></p>
							<p><label>Device/Register</label>'.getDeviceSelect('',$_GET['id'],45).' <span id="related_register_45"></p>
						  </div>
					
					</div>';

		$html .= '<input name="submit" type="submit" value="Submit" /> </form>';


	$html .= '</div>';





	return $html;
	
	
}

?>
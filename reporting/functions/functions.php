<?php 
function generate_random_string($name_length = 9) {
	$alpha_numeric = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!%$#*@';
	return substr(str_shuffle($alpha_numeric), 0, $name_length);
}


function formatDateMYSQL($date) {
//// this function is to format date for MYSQL


	if ($date == '') {
		
		return NULL;
		
	} else {
		
		$parts = explode("/",$date);
	
		return $parts[2]."-".$parts[0]."-".$parts[1];
	
	}
} //formatDateMYSQL


function formatDate($timestamp) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("m/d/Y", strtotime($timestamp));
	
	return $result;
	}
	
} //formatDate


function formatTimestamp($timestamp) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("m/d/Y g:i A", strtotime($timestamp) );
	
	return $result;
	}
	
} //formatTimestamp


function formatTime($timestamp) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("g:i A", strtotime($timestamp) );
	
	return $result;
	}
	
} //formatTimestamp



function formatTimeOffset($timestamp,$offset) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("g:i A", strtotime($timestamp) - (3600 * $offset) );
	
	return $result;
	}
	
} //formatTimestamp


function formatTimestampOffset($timestamp,$offset) {
//// this function is to format timestamps

	if ($timestamp != NULL) {
	$result = date("m/d/Y g:i A", strtotime($timestamp) - (3600 * $offset) );
	
	return $result;
	}
	
} //formatTimestamp



function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) {

    $dates = array();
    $current = strtotime($first);
    $last = strtotime($last);

    while( $current <= $last ) {

        $dates[] = date($output_format, $current);
        $current = strtotime($step, $current);
    }

    return $dates;
}



function getGaugeSettings($id) {

	global $link;	
	$qry = mysqli_query($link,"SELECT * FROM tbl_gauge_settings WHERE deviceID = $id " );
	$data = mysqli_fetch_assoc($qry);

	return $data;	
}



function getRegisterValue($reg,$sub,$date) {
	global $link;	
	
	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 214 AND date(register_date) = '$date' AND hour(register_date) = '12' ORDER BY register_date DESC LIMIT 1 ");
	$row = mysqli_fetch_assoc($qry);
	
	if ($sub) {
		$qry2 = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $sub AND register_deviceID = 214 AND date(register_date) = '$date' AND hour(register_date) = '12' ORDER BY register_date DESC LIMIT 1 ");
		$row2 = mysqli_fetch_assoc($qry2);

		return ($row['register_reading'] - $row2['register_reading']);
		
	} else {
		return $row['register_reading'];
	}
	
}




function getTotalsByRegister($id) {
	global $link;	
	
	
	if ($_GET['start']) {
		$qry = mysqli_query($link,"SELECT * FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 1 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' ORDER BY calculation_date DESC, register_number ");
	} else {
		$qry = mysqli_query($link,"SELECT * FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 1 ORDER BY calculation_date DESC, register_number LIMIT 7 ");
	}
	

	
	while ($row = mysqli_fetch_assoc($qry)) {
		$array[] = $row;
	}
	
	
	return $array;

	
}


function getTotalsByRegisterWater($id) {
	global $link;	

	
	if ($_GET['start']) {
		$qry = mysqli_query($link,"SELECT * FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 2 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' ORDER BY calculation_date DESC, register_number ");
	} else {
		$qry = mysqli_query($link,"SELECT * FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 2 ORDER BY calculation_date DESC, register_number LIMIT 7 ");
	}
	
	
	while ($row = mysqli_fetch_assoc($qry)) {
		$array[] = $row;
	}
	
	
	return $array;

	
}


function getTotalsArray($id) {
	global $link;	

	
	if ($_GET['start']) {
		
		$qry = mysqli_query($link,"SELECT calculation_date, sum(production_calculation) as totprod,  sum(production_calculation_in) as totprodin, sum(sales_calculation) as totsales, sum(sales_calculation_in) as totsalesin  FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 1 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' GROUP BY calculation_date ORDER BY calculation_date DESC ");

	} else {
		$qry = mysqli_query($link,"SELECT calculation_date, sum(production_calculation) as totprod,  sum(production_calculation_in) as totprodin, sum(sales_calculation) as totsales, sum(sales_calculation_in) as totsalesin  FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 1 GROUP BY calculation_date ORDER BY calculation_date DESC LIMIT 7 ");
	}
	
	
	
	
	while ($row = mysqli_fetch_assoc($qry)) {
		$array[] = $row;
	}
	
	
	return $array;

	
}


function getTotalsArrayWater($id) {
	global $link;	

	
	if ($_GET['start']) {
		
		$qry = mysqli_query($link,"SELECT calculation_date, sum(water_production_calculation) as totprod,  sum(water_trucked_calculation) as tottrucked  FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 2 AND calculation_date >= '".formatDateMYSQL($_GET['start'])."' AND calculation_date <= '".formatDateMYSQL($_GET['end'])."' GROUP BY calculation_date ORDER BY calculation_date DESC ");

	} else {
		$qry = mysqli_query($link,"SELECT calculation_date, sum(water_production_calculation) as totprod, sum(water_trucked_calculation) as tottrucked FROM `tbl_register_calculations` WHERE register_deviceID = $id AND calculation_type = 2 GROUP BY calculation_date ORDER BY calculation_date DESC LIMIT 7 ");
	}
	
	
	
	
	while ($row = mysqli_fetch_assoc($qry)) {
		$array[] = $row;
	}
	
	
	return $array;

	
}



function getLastReadingsArrayPressure($id) {
	global $link;	
	
	$qry = mysqli_query($link,"SELECT * FROM `tbl_registers` as r LEFT JOIN tbl_register_history as h ON r.register_number=h.register_number AND r.register_deviceID=h.register_deviceID  WHERE r.register_deviceID = $id AND r.register_config = 'Pressure' ORDER BY h.register_date DESC LIMIT 1 ");
	
	while ($row = mysqli_fetch_assoc($qry)) {
		$array[] = $row;
	}
	
	
	return $array;

	
}




function getRegisterName($id,$reg) {

	global $link;	
	$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = $id AND register_number = $reg " );
	$data = mysqli_fetch_assoc($qry);

	return $data['register_name'];	
}



function getRegisterConfig($id,$reg) {

	global $link;	
	$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = $id AND register_number = $reg " );
	$data = mysqli_fetch_assoc($qry);

	return $data;	
}




function getRegisterValues($reg,$date,$date2) {
	global $link;	
	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 214 AND date(register_date) = '$date' AND hour(register_date) = '12' ORDER BY register_date DESC LIMIT 1 ");
	$row = mysqli_fetch_assoc($qry);

	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = 214 AND register_date BETWEEN '".$row['register_date']."' AND '".$date2." 11:59:59' ORDER BY register_date DESC ");
	
	while ($row = mysqli_fetch_assoc($qry)) {
		
		$array[] = $row;	
		
	}
	
	$production = 0;
	$sales = 0;
	$last = count($array) - 1;
	
	foreach ($array as $key=>$reading) {
		
		//If (New – Old) > 0, add to production
		//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

		if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
			$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
			$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
			$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		}
			 	
		
	}
		
		
	$prevday = ($array[$last]['register_reading']);
	$today = number_format(($array[$last]['register_reading']+$production),2);

	return array($array,$prevday,round($production,2),$sales,$today,$array[0]['register_reading']);
	
	
}




function getRegisterReadings($id,$reg) {
	
	global $link;
		
	if ($_GET['start']) {
		
		$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = $id AND date(register_date) >= '".formatDateMYSQL($_GET['start'])."' AND date(register_date) <= '".formatDateMYSQL($_GET['end'])."'  ORDER BY register_date DESC ");


	} else {
		$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = $id AND date(register_date) >= '".date('Y-m-d', strtotime('-2 days'))."'  ORDER BY register_date DESC ");
	}
	
	
	
	


	while ($row = mysqli_fetch_assoc($qry)) {

		$array[] = $row;	

	}


	return $array;

	
}


function getRegisterReadingsByDate($id,$reg,$date) {
	
	global $link;
	
	$dateplus1 = date('Y-m-d', strtotime($date . ' +1 day'));
		
	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = $reg AND register_deviceID = $id AND (date(register_date) = '$date' and hour(register_date) >= 14) or register_number = $reg AND register_deviceID = $id AND (date(register_date) = '$dateplus1' and hour(register_date) < 14)   ORDER BY register_date DESC ");


	while ($row = mysqli_fetch_assoc($qry)) {

		$array[] = $row;	

	}


	return $array;

	
}


function getRegisterValuesConfig($id,$reg,$date,$date2) {
	global $link;	

		$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = ".$id." AND register_id = ".$reg."  ");
		$data = mysqli_fetch_assoc($qry);
	
	
	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = ".$data['register_number']." AND register_deviceID = $id AND date(register_date) = '$date' AND hour(register_date) = '11' ORDER BY register_date DESC LIMIT 1 ");
	$row = mysqli_fetch_assoc($qry);

	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_number = ".$data['register_number']." AND register_deviceID = $id AND register_date BETWEEN '".$row['register_date']."' AND '".$date2." 11:59:59' ORDER BY register_date DESC ");
	
	while ($row = mysqli_fetch_assoc($qry)) {
		
		$array[] = $row;	
		
	}
	
	$production = 0;
	$sales = 0;
	$last = count($array) - 1;
	
	foreach ($array as $key=>$reading) {
		
		//If (New – Old) > 0, add to production
		//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

		if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
			$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
			$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
			$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
		}
			 	
		
	}
		
		
	$prevday = ($array[$last]['register_reading']);
	$today = number_format(($array[$last]['register_reading']+$production),2);

	return array($array,$prevday,round($production,2),$sales,$today,$array[0]['register_reading']);
	
	
}




function getMTDValue($type) {
	global $link;	
	if ($type == 1) {

		$qry = mysqli_query($link,"SELECT sum(total_BOPD) as totalval FROM tbl_daily_totals WHERE month(total_date) = '".date('m')."' AND year(total_date) = '".date('Y')."' ");
		
	} else if ($type == 2) {
	
		$qry = mysqli_query($link,"SELECT sum(total_BOSold) as totalval FROM tbl_daily_totals WHERE month(total_date) = '".date('m')."' AND year(total_date) = '".date('Y')."' ");
	}
	
	$row = mysqli_fetch_assoc($qry);


	return $row['totalval'];
	
	
}



function getDevicesByRegion($id,$reg) {

	global $link;	
	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_accountID = ".$id." AND device_region = '$reg' ORDER BY device_name  ");

	while ($data = mysqli_fetch_assoc($qry)) {
		$array[] = $data;
	}

	return $array;	
}


function getDeviceName($id) {

	global $link;	
	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_deviceID = ".$id." ");
	$data = mysqli_fetch_assoc($qry);

	return $data['device_name'];	
}



function getLocations() {
	global $link;	
	$html = '<div id="locations">';

	$html .= '<table width="100%" border="1" cellspacing="0" cellpadding="5" id="locations_table">';

	$html .= '<thead><tr><th>Status</th><th>Site Name</th><th>Configuration</th><th>SMS</th><th>Last Report</th><th>Last Restart</th></tr></thead><tbody>';

	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_accountID = ".$_SESSION['account_id']."   ");

	while ($data = mysqli_fetch_assoc($qry)) {
		
		$xml = simplexml_load_file("https://questdashboard.net/datamine.php?querytype=pingdevice&deviceid=".$data['device_deviceID']."");
		
		//echo "<pre>";
		//print_r($xml);
		if ($xml->success == 1) {
			$status = 'Live';
			$statusstyle = 'style="background-color:#009966;color:#FFF;"';		
		} else {
			$status = 'Offline';		
			$statusstyle = 'style="background-color:#CC0000;color:#FFF;"';		
		}
		
		$html .= '<tr><td '.$statusstyle.'>'.$status.'</td><td><a href="dashboard.php?f=getRegisters&id='.$data['device_deviceID'].'">'.$data['device_name'].'</a></td><td><i class="fa fa-file-image-o" data-name="'.$data['device_name'].'"> <span data-name="'.$data['device_name'].'">View Site Configuration</span> </i></td><td>'.$data['device_sms'].'</td><td>'.formatTimestamp($data['device_last_report']).'</td><td>'.formatTimestamp($data['device_restart']).'</td></tr>';

		
	}
	

	$html .= '</tbody><tfoot></tfoot></table></table>';

	$html .= '</div>';

	$html .= '<div id="map"></div>';

	$html .= '<div id="config_modal" style="display:none;"></div>';

	return $html;
	
	
}


function getLocationsGeo() {
	global $link;

	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_deviceID = ".$_GET['d']."   ");

	while ($data = mysqli_fetch_assoc($qry)) {
		
		$array = array($data['device_name'],$data['device_latitude'],$data['device_longitude']);
	
	}
	

	  return $array;

}



function getRegisterReading($id,$reg) {
	
	global $link;
	$qry = mysqli_query($link,"SELECT * FROM tbl_register_history WHERE register_deviceID = $id AND register_number = $reg ORDER BY register_date DESC LIMIT 1 ");
	$row = mysqli_fetch_assoc($qry);

	return $row;
	
}




//function getRegisters() {
//	global $link;	
//	
//	$html = '<div id="locations">';
//
//	$html .= '<table width="100%" border="1" cellspacing="0" cellpadding="5" id="devices_table">';
//
//	//$html .= '<thead><tr><th>Register Name</th></tr></thead><tbody>';
//
//	$html .= '<thead><tr><th>Tools</th><th>Register Name</th><th>Unit</th><th>Last Reading</th><th>Last Reading Time</th></tr></thead><tbody>';
//
///*	$qry = mysqli_query("SELECT * FROM tbl_registers WHERE register_deviceID = ".$_GET['id']." ORDER BY register_name ");
//
//	while ($data = mysqli_fetch_assoc($qry)) {
//		
//		$reading = getRegisterReading($_GET['id'],$data['register_number']);
//		
//		if ($_GET['n'] == $data['register_number']) {
//			$class = 'class="active"';
//			$_SESSION['currentname'] = $data['register_name'];
//			$_SESSION['currentunit'] = $data['register_unit'];
//		} else {
//			$class = '';		
//		}
//		
//		$html .= '<tr '.$class.'><td><i class="fa fa-pencil" data-name="'.$data['register_name'].'"> <span data-name="'.$data['register_name'].'">Notes</span> </i> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-clock-o" data-name="'.$data['register_name'].'"> <span data-name="'.$data['register_name'].'">Alarms</span> </i> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-rss" data-name="'.$data['register_name'].'"> <span data-name="'.$data['register_name'].'">Ping</span> </i> &nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-area-chart"> <a href="dashboard.php?f=getRegisters&id='.$_GET['id'].'&n='.$data['register_number'].'" class="fa">Chart</a> </i> </i> </td><td>'.$data['register_name'].'</td><td>'.$data['register_unit'].'</td><td>'.$reading['register_reading'].'</td><td>'.formatTimestamp($reading['register_date']).'</td></tr>';
//
//		
//	}
//	*/
//
//	$html .= '</tbody><tfoot></tfoot></table></table>';
//
//	$html .= '</div>';
//
//	if ($_GET['n']) {
//		$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = ".$_GET['id']." AND register_number = ".$_GET['n']."  ");
//		$data = mysqli_fetch_assoc($qry);
//		$_SESSION['currentname'] = $data['register_name'];
//		$_SESSION['currentunit'] = $data['register_unit'];
//
//		$html .= '<div id="chart" style="min-width: 800px; height: 400px; margin: 0 auto"></div>';
//	}
//
//	$html .= '<div id="notes_modal" style="display:none;">
//			
//			<form action="process/add_note.php" method="post">
//				<input name="regID" id="regID" type="hidden"  />
//				<textarea name="register_note" cols="85" rows="10"></textarea>
//				<input name="submit" type="submit" value="Submit" />
//			</form>	
//			
//			<div id="notestable"></div>
//	
//	</div>';
//
//	$html .= '<div id="alarms_modal" style="display:none;"><div id="alarmstable"></div></div>';
//
//	$html .= '<div id="log_modal" style="display:none;">
//			
//			<form action="process/add_log.php" method="post">
//				<input name="regID" id="regID" type="hidden"  />
//				<input name="log_reading" type="text" placeholder="Reading"  />
//				<input name="log_date" type="text" class="datepicker" placeholder="Date" />
//				<input name="submit" type="submit" value="Submit" />
//			</form>	
//	
//	
//				<div id="logtable"></div>
//		
//	
//	</div>';
//
//
//	$html .= '<div id="haul_modal" style="display:none;">
//			
//			<form action="process/add_haul.php" method="post">
//				<input name="regID" id="regID" type="hidden"  />
//				<input name="haul_reading" type="text" placeholder="Reading"  />
//				<input name="haul_date" type="text" class="datepicker" placeholder="Date" />
//				<input name="submit" type="submit" value="Submit" />
//			</form>	
//			
//			
//			<div id="haultable"></div>
//
//	
//	</div>';
//
//
//
//	$html .= '<div style="height:500px;"></div>';
//
//	return $html;
//	
//	
//}



function getDeviceCount($id) {
	global $link;	
	
	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_accountID = $id ");
	$count = mysqli_num_rows($qry);

	return $count;
	
}




function getDeviceSelect($current,$id,$num)  {
	global $link;

	$keynum = $num-1;
	
	$html = '<select name="device_deviceID[]" class="devsel" data-id="'.$num.'" style="width:200px;">';

	$html .= '<option value="">--Choose Device--</option>';

	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_accountID = $id ORDER BY device_name ");
	
		while ($data = mysqli_fetch_array($qry)) {

			if ($current[$keynum]==$data['device_deviceID']) {
				$html .= '<option value="'.$data['device_deviceID'].'" selected="selected">'.$data['device_name'].'</option>';
			} else {
				$html .= '<option value="'.$data['device_deviceID'].'">'.$data['device_name'].'</option>';
			}			
			
		}

	$html .= '</select>';
	
	return $html;
}



function getLogBookFieldsByDevice($id)  {
	global $link;

	$qry = mysqli_query($link,"SELECT * FROM tbl_log_book_fields WHERE deviceID = $id ");
	
	while ($data = mysqli_fetch_array($qry)) {
		
		$array[] = $data;

	}
	
	
	return $array;
}






function getRegistersByDevice($id)  {
	global $link;

	$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = $id ORDER BY register_number ");
	
	while ($data = mysqli_fetch_array($qry)) {
		
		$array[] = $data;

	}
	
	
	return $array;
}






function getRegisterSelect($id,$current,$name,$num,$feet)  {
	global $link;
	$html = '<select name="register_id['.$num.']"  class="regsel" style="width:300px;margin-right:20px;">';

	$html .= '<option value="">--Choose Register--</option>';

	$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = $id ORDER BY register_name ");
	
		while ($data = mysqli_fetch_array($qry)) {

				if ($current[$num]==$data['register_id']) {
					$html .= '<option value="'.$data['register_id'].'" selected="selected">'.$data['register_name'].'</option>';
				} else {
					$html .= '<option value="'.$data['register_id'].'">'.$data['register_name'].'</option>';
				}
		}

	$html .= '</select>';

	$html .= 'Label <input name="alt_name['.$num.']" type="text" value="'.$name[$num].'" /> (REQUIRED) ';
	
	$html .= '&nbsp;&nbsp;&nbsp;Feet Factor <input name="feet_factor['.$num.']" type="text" value="'.$feet[$num].'" />';

	
	return $html;
}




function getRegisterCount($id) {
	global $link;	
	$count = 0;
	
	$qry = mysqli_query($link,"SELECT * FROM tbl_devices WHERE device_accountID = $id ");
	
	while ($data = mysqli_fetch_assoc($qry)) {


		$sqry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = ".$data['device_deviceID']." ");
		$count += mysqli_num_rows($sqry);


	}
	return $count;
	
}


function getAccountName($id) {
	global $link;
	$qry = mysqli_query($link,"SELECT * FROM tbl_accounts WHERE account_id = $id   ") or die(mysqli_error());
	$data = mysqli_fetch_assoc($qry);
	
	return $data['account_name'];	
	
}


function getAccountConfigTime($field) {
	global $link;
	$qry = mysqli_query($link,"SELECT $field FROM tbl_report_config WHERE config_accountID = ".$_SESSION['account_id']." ") or die(mysqli_error());
	$data = mysqli_fetch_assoc($qry);
	
	return $data[$field];	
	
}
		

function getAccountConfig($id) {
	global $link;
	$qry = mysqli_query($link,"SELECT * FROM tbl_report_config WHERE configID = $id   ") or die(mysqli_error());
	$data = mysqli_fetch_assoc($qry);
	
	return unserialize($data['config_array']);	
	
}


function getConfigTitle($id) {
	global $link;
	$qry = mysqli_query($link,"SELECT * FROM tbl_report_config WHERE configID = $id   ") or die(mysqli_error());
	$data = mysqli_fetch_assoc($qry);
	
	return $data['report_title'];	
	
}



function getAccountConfigList($id) {
	global $link;
	$qry = mysqli_query($link,"SELECT * FROM tbl_report_config WHERE config_accountID = $id   ") or die(mysqli_error());
	
	$html .= '<ul>';	
	
	while ($data = mysqli_fetch_assoc($qry)) {
		
		$html .= '<li><a href="config.php?f=getConfig2&id='.$id.'&configID='.$data['configID'].'">'.$data['report_title'].'</a></li>';	
	
	}

	$html .= '</ul>';	
	
	return $html;
}
		



/*function getAccounts() {
	global $link;
	$html = '<div id="locations">';

	$html .= '<table width="100%" border="1" cellspacing="0" cellpadding="5" id="accounts_table">';

	$html .= '<thead><tr><th>Account</th><th>Number Devices</th><th>Number Registers</th><th>Status</th></tr></thead><tbody>';

	$qry = mysqli_query($link,"SELECT * FROM tbl_accounts ORDER BY account_name   ");

	while ($data = mysqli_fetch_assoc($qry)) {
		
		if ($data['account_active'] == 1) {
			$status = 'Active';
		} else {
			$status = 'Inactive';		
		}
		
		$html .= '<tr><td><a href="config.php?f=getConfig2&id='.$data['account_id'].'">'.$data['account_name'].'</a></td><td>'.getDeviceCount($data['account_id']).'</td><td>'.getRegisterCount($data['account_id']).'</td><td>'.$status.'</td></tr>';

	}

	$html .= '</tbody><tfoot></tfoot></table></table>';

	$html .= '</div>';

	return $html;
	
	
}*/



function getHourSelect ($current,$type) {
	
		if ($type == 1) {
			$html = '<select name="report_hour">';
		} else if ($type == 2) {
			$html = '<select name="range_start">';
		} else if ($type == 3) {
			$html = '<select name="range_end">';
		}

			for($i = 0; $i < 24; $i++) {
				
					if ($i < 12 ) {
						$ampm = 'AM';
					} else {
						$ampm = 'PM';
					}

 					if ($i == 0) {
						$l = '12';		
					} else if ($i > 12) {
						$l = $i-12;	
					} else {
						$l = $i;	
					}
					
					if ($i == $current) {
						$html .= '<option value="'.$i.'" selected="selected">'.$l.':00 '.$ampm.'</option>';
					} else {
						$html .= '<option value="'.$i.'">'.$l.':00 '.$ampm.'</option>';
					}
			}

		$html .= '</select>';

		return $html;
	
	
}


function getConfig() {

	
	if (!isset($_GET['configID'])) {


			$html = '<div id="locations">';

				$html .= '<p><a href="config.php">&laquo; Back to Accounts</a></p>';

				$html .= '<h2>'.getAccountName($_GET['id']).' Report List (click to edit)</h2>';

				$html .= getAccountConfigList($_GET['id']);

				$html .= '<h2>Create New Report</h2>';

				$html .= '<form action="../process/add_email_config.php" method="post"><input name="account_id" value="'.$_GET['id'].'" type="hidden" />';
		
				$html .= '<p><label>Title</label><input name="email_title" type="text" size="100" /></p>';

				$html .= '<input name="submit" type="submit" value="Submit" style="margin-top:20px;width:100px;height:50px;" /> </form>';

			$html .= '</div>';

	
	} else {
	
	
			$config = getAccountConfig($_GET['configID']);
			
			//echo "<pre>";
			//print_r($config);
		
			$html = '<div id="locations">';
			
				$html .= '<p><a href="config.php">&laquo; Back to Accounts</a> | <a href="config.php?f=getConfig&id='.$_GET['id'].'">Report List</a></p>';


				if ($_GET['sent'] == 1) {
					
					$html .= '<div class="alert">Email Sent</div>';
						
				}
							
				$html .= '<h2>'.getAccountName($_GET['id']).'</h2>';
			
				$html .= '<form action="../process/email_config.php" method="post"><input name="configID" value="'.$_GET['configID'].'" type="hidden" /><input name="account_id" value="'.$_GET['id'].'" type="hidden" />';
		
				
				if ($config['send_email'] == 2) {
					$html .= '<p><label>Send Email</label><input name="send_email" type="radio" value="1" /> Yes  <input name="send_email" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Send Email</label><input name="send_email" type="radio" value="1" checked="checked" /> Yes  <input name="send_email" type="radio" value="2" /> No</p>';
				}
				
		
				if ($config == "") {
					$html .= '<p><label>Title</label><input name="email_title" type="text" size="100" value="'.getConfigTitle($_GET['configID']).'" /></p>';
				} else {
					$html .= '<p><label>Title</label><input name="email_title" type="text" size="100" value="'.$config['email_title'].'" /></p>';
				}
		
				$html .= '<p><label>Emails</label><input name="email_addresses" type="text" size="100" value="'.$config['email_addresses'].'" /></p>';
		
		
				if ($config['email_timing'] == 1) {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" checked="checked" /> Hourly <input name="email_timing" type="radio" value="2" /> Daily  <input name="email_timing" type="radio" value="3" /> Weekly  <input name="email_timing" type="radio" value="4" /> Monthly  </p>';
				} else if ($config['email_timing'] == 3) {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" /> Hourly <input name="email_timing" type="radio" value="2" /> Daily  <input name="email_timing" type="radio" value="3" checked="checked" /> Weekly  <input name="email_timing" type="radio" value="4" /> Monthly  </p>';
				} else if ($config['email_timing'] == 4) {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" /> Hourly <input name="email_timing" type="radio" value="2" /> Daily  <input name="email_timing" type="radio" value="3" /> Weekly  <input name="email_timing" type="radio" value="4" checked="checked" /> Monthly  </p>';
				} else {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" /> Hourly <input name="email_timing" type="radio" value="2" checked="checked" /> Daily  <input name="email_timing" type="radio" value="3" /> Weekly  <input name="email_timing" type="radio" value="4" /> Monthly  </p>';
				}
		
					$html .= '<p><label>Time</label> '.getHourSelect($config['report_hour']).'</p>';
		
		
				$html .= '<h3>Oil</h3>';
		
				if ($config['grand_total'] == 2) {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total" type="radio" value="1" /> Yes  <input name="grand_total" type="radio" value="2" checked="checked"/> No</p>';
				} else {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total" type="radio" value="1" checked="checked" /> Yes  <input name="grand_total" type="radio" value="2" /> No</p>';
				}
		
		
				if ($config['month_total'] == 2) {
					$html .= '<p><label>Add Month Total</label><input name="month_total" type="radio" value="1" /> Yes  <input name="month_total" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Add Month Total</label><input name="month_total" type="radio" value="1" checked="checked" /> Yes  <input name="month_total" type="radio" value="2" /> No</p>';
				}
		
				$html .= '<h3>Water</h3>';
		
		
				if ($config['grand_total_water'] == 2) {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total_water" type="radio" value="1" /> Yes  <input name="grand_total_water" type="radio" value="2" checked="checked"/> No</p>';
				} else {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total_water" type="radio" value="1" checked="checked" /> Yes  <input name="grand_total_water" type="radio" value="2" /> No</p>';
				}
		
		
		
				if ($config['month_total_water'] == 2) {
					$html .= '<p><label>Add Month Total</label><input name="month_total_water" type="radio" value="1" /> Yes  <input name="month_total_water" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Add Month Total</label><input name="month_total_water" type="radio" value="1" checked="checked" /> Yes  <input name="month_total_water" type="radio" value="2" /> No</p>';
				}
		
		
		
		/*		if ($config['month_summary'] == 2) {
					$html .= '<p><label>Monthly Summary</label><input name="month_summary" type="radio" value="1" /> Yes  <input name="month_summary" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Monthly Summary</label><input name="month_summary" type="radio" value="1" checked="checked" /> Yes  <input name="month_summary" type="radio" value="2" /> No</p>';
				}
		*/
				
		
		
				$html .= '<hr>';
		
				$html .= '<h3>Production/Sales</h3>';
		
				$html .= '<div class="accordion">
				
							  <h3>Section 1</h3>
							
								  <div>
									<p><label>Label</label><input name="section_one_label" type="text" size="50" value="'.$config['section_one_label'].'"  /> * REQUIRED FOR EMAIL</p>';
									
									
									if ($config['include_bopd_1'] == 1) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_1" type="radio" value="1" checked="checked" />Yes <input name="include_bopd_1" type="radio" value="2" />No</p>';					
									} else if ($config['include_bopd_1'] == 2) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_1" type="radio" value="1" />Yes <input name="include_bopd_1" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_1" type="radio" value="1" />Yes <input name="include_bopd_1" type="radio" value="2" />No</p>';					
									}
		
		
									if ($config['include_wpd_1'] == 1) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_1" type="radio" value="1" checked="checked" />Yes <input name="include_wpd_1" type="radio" value="2" />No</p>';					
									} else if ($config['include_wpd_1'] == 2) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_1" type="radio" value="1" />Yes <input name="include_wpd_1" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_1" type="radio" value="1" />Yes <input name="include_wpd_1" type="radio" value="2" />No</p>';					
									}
		
				
									
									if (isset($config['device_deviceID'][0]) && isset($config['register_id'][0])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],1).' <span id="related_register_1">'.getRegisterSelect($config['device_deviceID'][0],$config['register_id'],$config['alt_name'],0,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],1).' <span id="related_register_1"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][1]) && isset($config['register_id'][1])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],2).' <span id="related_register_2">'.getRegisterSelect($config['device_deviceID'][1],$config['register_id'],$config['alt_name'],1,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],2).' <span id="related_register_2"></span></p>';
									}
		
									if (isset($config['device_deviceID'][2]) && isset($config['register_id'][2])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],3).' <span id="related_register_3">'.getRegisterSelect($config['device_deviceID'][2],$config['register_id'],$config['alt_name'],2,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],3).' <span id="related_register_3"></span></p>';
									}
		
									if (isset($config['device_deviceID'][3]) && isset($config['register_id'][3])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],4).' <span id="related_register_4">'.getRegisterSelect($config['device_deviceID'][3],$config['register_id'],$config['alt_name'],3,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],4).' <span id="related_register_4"></span></p>';
									}
		
									if (isset($config['device_deviceID'][4]) && isset($config['register_id'][4])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],5).' <span id="related_register_5">'.getRegisterSelect($config['device_deviceID'][4],$config['register_id'],$config['alt_name'],4,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],5).' <span id="related_register_5"></span></p>';
									}
		
		
			
						  $html .= '</div>
							
							  <h3>Section 2</h3>
		
								  <div>
									<p><label>Label</label><input name="section_two_label" type="text" size="50" value="'.$config['section_two_label'].'" /> * REQUIRED FOR EMAIL</p>';
		
									if ($config['include_bopd_2'] == 1) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_2" type="radio" value="1" checked="checked" />Yes <input name="include_bopd_2" type="radio" value="2" />No</p>';					
									} else if ($config['include_bopd_2'] == 2) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_2" type="radio" value="1" />Yes <input name="include_bopd_2" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_2" type="radio" value="1" />Yes <input name="include_bopd_2" type="radio" value="2" />No</p>';					
									}
		
		
									if ($config['include_wpd_2'] == 1) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_2" type="radio" value="1" checked="checked" />Yes <input name="include_wpd_2" type="radio" value="2" />No</p>';					
									} else if ($config['include_wpd_2'] == 2) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_2" type="radio" value="1" />Yes <input name="include_wpd_2" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_2" type="radio" value="1" />Yes <input name="include_wpd_2" type="radio" value="2" />No</p>';					
									}
		
		
			
									if (isset($config['device_deviceID'][5]) && isset($config['register_id'][5])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],6).' <span id="related_register_6">'.getRegisterSelect($config['device_deviceID'][5],$config['register_id'],$config['alt_name'],5,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],6).' <span id="related_register_6"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][6]) && isset($config['register_id'][6])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],7).' <span id="related_register_7">'.getRegisterSelect($config['device_deviceID'][6],$config['register_id'],$config['alt_name'],6,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],7).' <span id="related_register_7"></span></p>';
									}
		
									if (isset($config['device_deviceID'][7]) && isset($config['register_id'][7])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],8).' <span id="related_register_8">'.getRegisterSelect($config['device_deviceID'][7],$config['register_id'],$config['alt_name'],7,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],8).' <span id="related_register_8"></span></p>';
									}
		
									if (isset($config['device_deviceID'][8]) && isset($config['register_id'][8])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],9).' <span id="related_register_9">'.getRegisterSelect($config['device_deviceID'][8],$config['register_id'],$config['alt_name'],8,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],9).' <span id="related_register_9"></span></p>';
									}
		
									if (isset($config['device_deviceID'][9]) && isset($config['register_id'][9])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],10).' <span id="related_register_10">'.getRegisterSelect($config['device_deviceID'][9],$config['register_id'],$config['alt_name'],9,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],10).' <span id="related_register_10"></span></p>';
									}						
									
									
							$html .= '</div>
		
							
							  <h3>Section 3</h3>
							
								  <div>
									<p><label>Label</label><input name="section_three_label" type="text" size="50" value="'.$config['section_three_label'].'"  /> * REQUIRED FOR EMAIL</p>';
		
		
									if ($config['include_bopd_3'] == 1) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_3" type="radio" value="1" checked="checked" />Yes <input name="include_bopd_3" type="radio" value="2" />No</p>';					
									} else if ($config['include_bopd_3'] == 2) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_3" type="radio" value="1" />Yes <input name="include_bopd_3" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_3" type="radio" value="1" />Yes <input name="include_bopd_3" type="radio" value="2" />No</p>';					
									}
		
		
									if ($config['include_wpd_3'] == 1) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_3" type="radio" value="1" checked="checked" />Yes <input name="include_wpd_3" type="radio" value="2" />No</p>';					
									} else if ($config['include_wpd_3'] == 2) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_3" type="radio" value="1" />Yes <input name="include_wpd_3" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_3" type="radio" value="1" />Yes <input name="include_wpd_3" type="radio" value="2" />No</p>';					
									}
									
		
		
									if (isset($config['device_deviceID'][10]) && isset($config['register_id'][10])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],11).' <span id="related_register_11">'.getRegisterSelect($config['device_deviceID'][10],$config['register_id'],$config['alt_name'],10,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],11).' <span id="related_register_11"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][11]) && isset($config['register_id'][11])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],12).' <span id="related_register_12">'.getRegisterSelect($config['device_deviceID'][11],$config['register_id'],$config['alt_name'],11,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],12).' <span id="related_register_12"></span></p>';
									}
		
									if (isset($config['device_deviceID'][12]) && isset($config['register_id'][12])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],13).' <span id="related_register_13">'.getRegisterSelect($config['device_deviceID'][12],$config['register_id'],$config['alt_name'],12,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],13).' <span id="related_register_13"></span></p>';
									}
		
									if (isset($config['device_deviceID'][13]) && isset($config['register_id'][13])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],14).' <span id="related_register_14">'.getRegisterSelect($config['device_deviceID'][13],$config['register_id'],$config['alt_name'],13,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],14).' <span id="related_register_14"></span></p>';
									}
		
									if (isset($config['device_deviceID'][14]) && isset($config['register_id'][14])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],15).' <span id="related_register_15">'.getRegisterSelect($config['device_deviceID'][14],$config['register_id'],$config['alt_name'],14,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],15).' <span id="related_register_15"></span></p>';
									}						
									
									
		
							$html .= '</div>
		
							  <h3>Section 4</h3>
		
								  <div>
									<p><label>Label</label><input name="section_four_label" type="text" size="50" value="'.$config['section_four_label'].'" /> * REQUIRED FOR EMAIL</p>';
									
		
									if ($config['include_bopd_4'] == 1) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_4" type="radio" value="1" checked="checked" />Yes <input name="include_bopd_4" type="radio" value="2" />No</p>';					
									} else if ($config['include_bopd_4'] == 2) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_4" type="radio" value="1" />Yes <input name="include_bopd_4" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_4" type="radio" value="1" />Yes <input name="include_bopd_4" type="radio" value="2" />No</p>';					
									}
		
		
									if ($config['include_wpd_4'] == 1) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_4" type="radio" value="1" checked="checked" />Yes <input name="include_wpd_4" type="radio" value="2" />No</p>';					
									} else if ($config['include_wpd_4'] == 2) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_4" type="radio" value="1" />Yes <input name="include_wpd_4" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_4" type="radio" value="1" />Yes <input name="include_wpd_4" type="radio" value="2" />No</p>';					
									}
		
									
									if (isset($config['device_deviceID'][15]) && isset($config['register_id'][15])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],16).' <span id="related_register_16">'.getRegisterSelect($config['device_deviceID'][15],$config['register_id'],$config['alt_name'],15,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],16).' <span id="related_register_16"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][16]) && isset($config['register_id'][16])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],17).' <span id="related_register_17">'.getRegisterSelect($config['device_deviceID'][16],$config['register_id'],$config['alt_name'],16,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],17).' <span id="related_register_17"></span></p>';
									}
		
									if (isset($config['device_deviceID'][17]) && isset($config['register_id'][17])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],18).' <span id="related_register_18">'.getRegisterSelect($config['device_deviceID'][17],$config['register_id'],$config['alt_name'],17,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],18).' <span id="related_register_18"></span></p>';
									}
		
									if (isset($config['device_deviceID'][18]) && isset($config['register_id'][18])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],19).' <span id="related_register_19">'.getRegisterSelect($config['device_deviceID'][18],$config['register_id'],$config['alt_name'],18,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],19).' <span id="related_register_19"></span></p>';
									}
		
									if (isset($config['device_deviceID'][19]) && isset($config['register_id'][19])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],20).' <span id="related_register_20">'.getRegisterSelect($config['device_deviceID'][19],$config['register_id'],$config['alt_name'],19,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],20).' <span id="related_register_20"></span></p>';
									}						
							
							
							
							
							$html .= '</div>
		
							  <h3>Section 5</h3>
		
								  <div>
									<p><label>Label</label><input name="section_five_label" type="text" size="50" value="'.$config['section_five_label'].'" /> * REQUIRED FOR EMAIL</p>';
		
		
									if ($config['include_bopd_5'] == 1) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_5" type="radio" value="1" checked="checked" />Yes <input name="include_bopd_5" type="radio" value="2" />No</p>';					
									} else if ($config['include_bopd_5'] == 2) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_5" type="radio" value="1" />Yes <input name="include_bopd_5" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_5" type="radio" value="1" />Yes <input name="include_bopd_5" type="radio" value="2" />No</p>';					
									}
		
		
									if ($config['include_wpd_5'] == 1) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_5" type="radio" value="1" checked="checked" />Yes <input name="include_wpd_5" type="radio" value="2" />No</p>';					
									} else if ($config['include_wpd_5'] == 2) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_5" type="radio" value="1" />Yes <input name="include_wpd_5" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_5" type="radio" value="1" />Yes <input name="include_wpd_5" type="radio" value="2" />No</p>';					
									}
		
									
									if (isset($config['device_deviceID'][20]) && isset($config['register_id'][20])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],21).' <span id="related_register_21">'.getRegisterSelect($config['device_deviceID'][20],$config['register_id'],$config['alt_name'],20,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],21).' <span id="related_register_21"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][21]) && isset($config['register_id'][21])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],22).' <span id="related_register_22">'.getRegisterSelect($config['device_deviceID'][21],$config['register_id'],$config['alt_name'],21,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],22).' <span id="related_register_22"></span></p>';
									}
		
									if (isset($config['device_deviceID'][22]) && isset($config['register_id'][22])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],23).' <span id="related_register_23">'.getRegisterSelect($config['device_deviceID'][22],$config['register_id'],$config['alt_name'],22,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],23).' <span id="related_register_23"></span></p>';
									}
		
									if (isset($config['device_deviceID'][23]) && isset($config['register_id'][23])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],24).' <span id="related_register_24">'.getRegisterSelect($config['device_deviceID'][23],$config['register_id'],$config['alt_name'],23,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],24).' <span id="related_register_24"></span></p>';
									}
		
									if (isset($config['device_deviceID'][24]) && isset($config['register_id'][24])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],25).' <span id="related_register_25">'.getRegisterSelect($config['device_deviceID'][24],$config['register_id'],$config['alt_name'],24,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],25).' <span id="related_register_25"></span></p>';
									}						
									
									
							
							
							$html .= '</div>
		
							  <h3>Section 6</h3>
							
								  <div>
									<p><label>Label</label><input name="section_six_label" type="text" size="50" value="'.$config['section_six_label'].'" /> * REQUIRED FOR EMAIL</p>';
		
		
									if ($config['include_bopd_6'] == 1) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_6" type="radio" value="1" checked="checked" />Yes <input name="include_bopd_6" type="radio" value="2" />No</p>';					
									} else if ($config['include_bopd_6'] == 2) {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_6" type="radio" value="1" />Yes <input name="include_bopd_6" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include BOPD</label><input name="include_bopd_6" type="radio" value="1" />Yes <input name="include_bopd_6" type="radio" value="2" />No</p>';					
									}
		
		
									if ($config['include_wpd_6'] == 1) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_6" type="radio" value="1" checked="checked" />Yes <input name="include_wpd_6" type="radio" value="2" />No</p>';					
									} else if ($config['include_wpd_6'] == 2) {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_6" type="radio" value="1" />Yes <input name="include_wpd_6" type="radio" value="2" checked="checked" />No</p>';		
									} else  {
										$html .= '<p><label>Include WPD</label><input name="include_wpd_6" type="radio" value="1" />Yes <input name="include_wpd_6" type="radio" value="2" />No</p>';					
									}
								  
		
									if (isset($config['device_deviceID'][25]) && isset($config['register_id'][25])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],26).' <span id="related_register_26">'.getRegisterSelect($config['device_deviceID'][25],$config['register_id'],$config['alt_name'],25,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],26).' <span id="related_register_26"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][26]) && isset($config['register_id'][26])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],27).' <span id="related_register_27">'.getRegisterSelect($config['device_deviceID'][26],$config['register_id'],$config['alt_name'],26,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],27).' <span id="related_register_27"></span></p>';
									}
		
									if (isset($config['device_deviceID'][27]) && isset($config['register_id'][27])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],28).' <span id="related_register_28">'.getRegisterSelect($config['device_deviceID'][27],$config['register_id'],$config['alt_name'],27,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],28).' <span id="related_register_28"></span></p>';
									}
		
									if (isset($config['device_deviceID'][28]) && isset($config['register_id'][28])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],29).' <span id="related_register_29">'.getRegisterSelect($config['device_deviceID'][28],$config['register_id'],$config['alt_name'],28,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],29).' <span id="related_register_29"></span></p>';
									}
		
									if (isset($config['device_deviceID'][29]) && isset($config['register_id'][29])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],30).' <span id="related_register_30">'.getRegisterSelect($config['device_deviceID'][29],$config['register_id'],$config['alt_name'],29,$config['feet_factor']).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],30).' <span id="related_register_30"></span></p>';
									}						
		
		
		
		
								  
							$html .= '</div>
				
							</div>';
		
		
				$html .= '<h3>Flow Meters</h3>';
		
				$html .= '<div class="accordion">
				
							  <h3>Section 1</h3>
							
								  <div>
									<p><label>Label</label><input name="section_seven_label" type="text" size="50" value="'.$config['section_seven_label'].'" /></p>';
		
									if (isset($config['device_deviceID'][30]) && isset($config['register_id'][30])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],31).' <span id="related_register_31">'.getRegisterSelect($config['device_deviceID'][30],$config['register_id'],$config['alt_name'],30).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],31).' <span id="related_register_31"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][31]) && isset($config['register_id'][31])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],32).' <span id="related_register_32">'.getRegisterSelect($config['device_deviceID'][31],$config['register_id'],$config['alt_name'],31).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],32).' <span id="related_register_32"></span></p>';
									}
		
									if (isset($config['device_deviceID'][32]) && isset($config['register_id'][32])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],33).' <span id="related_register_33">'.getRegisterSelect($config['device_deviceID'][32],$config['register_id'],$config['alt_name'],32).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],33).' <span id="related_register_33"></span></p>';
									}
		
									if (isset($config['device_deviceID'][33]) && isset($config['register_id'][33])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],34).' <span id="related_register_34">'.getRegisterSelect($config['device_deviceID'][33],$config['register_id'],$config['alt_name'],33).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],34).' <span id="related_register_34"></span></p>';
									}
		
									if (isset($config['device_deviceID'][34]) && isset($config['register_id'][34])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],35).' <span id="related_register_35">'.getRegisterSelect($config['device_deviceID'][34],$config['register_id'],$config['alt_name'],34).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],35).' <span id="related_register_35"></span></p>';
									}						
		
		
		
						$html .= '</div>
							
							</div>';
			
		
				$html .= '<h3>Injected Waters</h3>';
		
				$html .= '<div class="accordion">
				
							  <h3>Section 1</h3>
							
								  <div>
									<p><label>Label</label><input name="section_eight_label" type="text" size="50" value="'.$config['section_eight_label'].'" /></p>';
									
		
									if (isset($config['device_deviceID'][35]) && isset($config['register_id'][35])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],36).' <span id="related_register_36">'.getRegisterSelect($config['device_deviceID'][35],$config['register_id'],$config['alt_name'],35).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],36).' <span id="related_register_36"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][36]) && isset($config['register_id'][36])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],37).' <span id="related_register_37">'.getRegisterSelect($config['device_deviceID'][36],$config['register_id'],$config['alt_name'],36).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],37).' <span id="related_register_37"></span></p>';
									}
		
									if (isset($config['device_deviceID'][37]) && isset($config['register_id'][37])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],38).' <span id="related_register_38">'.getRegisterSelect($config['device_deviceID'][37],$config['register_id'],$config['alt_name'],37).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],38).' <span id="related_register_38"></span></p>';
									}
		
									if (isset($config['device_deviceID'][38]) && isset($config['register_id'][38])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],39).' <span id="related_register_39">'.getRegisterSelect($config['device_deviceID'][38],$config['register_id'],$config['alt_name'],38).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],39).' <span id="related_register_39"></span></p>';
									}
		
									if (isset($config['device_deviceID'][39]) && isset($config['register_id'][39])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],40).' <span id="related_register_40">'.getRegisterSelect($config['device_deviceID'][39],$config['register_id'],$config['alt_name'],39).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],40).' <span id="related_register_40"></span></p>';
									}						
									
						
						
						$html .= '</div>
							
							</div>';
		
			
				$html .= '<h3>Pressures</h3>';
		
		
				$html .= '<div class="accordion">
				
							  <h3>Section 1</h3>
							
								  <div>
									<p><label>Label</label><input name="section_nine_label" type="text" size="50" value="'.$config['section_nine_label'].'" /></p>';
		
		
									if (isset($config['device_deviceID'][40]) && isset($config['register_id'][40])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],41).' <span id="related_register_41">'.getRegisterSelect($config['device_deviceID'][40],$config['register_id'],$config['alt_name'],40).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],41).' <span id="related_register_41"></span></p>';
									}
		
		
									if (isset($config['device_deviceID'][41]) && isset($config['register_id'][41])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],42).' <span id="related_register_42">'.getRegisterSelect($config['device_deviceID'][41],$config['register_id'],$config['alt_name'],41).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],42).' <span id="related_register_42"></span></p>';
									}
		
									if (isset($config['device_deviceID'][42]) && isset($config['register_id'][42])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],43).' <span id="related_register_43">'.getRegisterSelect($config['device_deviceID'][42],$config['register_id'],$config['alt_name'],42).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],43).' <span id="related_register_43"></span></p>';
									}
		
									if (isset($config['device_deviceID'][43]) && isset($config['register_id'][43])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],44).' <span id="related_register_44">'.getRegisterSelect($config['device_deviceID'][43],$config['register_id'],$config['alt_name'],43).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],44).' <span id="related_register_44"></span></p>';
									}
		
									if (isset($config['device_deviceID'][44]) && isset($config['register_id'][44])) {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],45).' <span id="related_register_45">'.getRegisterSelect($config['device_deviceID'][44],$config['register_id'],$config['alt_name'],44).'</span></p>';
									} else {
										  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],45).' <span id="related_register_45"></span></p>';
									}						
		
		
						$html .= '</div>
							
							</div>';
		
				$html .= '<input name="submit" type="submit" value="Submit" style="margin-top:20px;width:100px;height:50px;" /> </form>';
		
		
			$html .= '</div>';


	}


	return $html;
	
	
}




function getConfig2() {

	
	if (!isset($_GET['configID'])) {


			$html = '<div id="locations">';

				$html .= '<p><a href="config.php">&laquo; Back to Accounts</a></p>';

				$html .= '<h2>'.getAccountName($_GET['id']).' Report List (click to edit)</h2>';

				$html .= getAccountConfigList($_GET['id']);

				$html .= '<h2>Create New Report</h2>';

				$html .= '<form action="../process/add_email_config.php" method="post"><input name="account_id" value="'.$_GET['id'].'" type="hidden" />';
		
				$html .= '<p><label>Title</label><input name="email_title" type="text" size="100" /></p>';

				$html .= '<input name="submit" type="submit" value="Submit" style="margin-top:20px;width:100px;height:50px;" /> </form>';

			$html .= '</div>';

	
	} else {
	
	
			$config = getAccountConfig($_GET['configID']);
			
			//echo "<pre>";
			//print_r($config);
		
			$html = '<div id="locations">';
			
				$html .= '<p><a href="config.php">&laquo; Back to Accounts</a> | <a href="config.php?f=getConfig2&id='.$_GET['id'].'">Report List</a></p>';


				if ($_GET['sent'] == 1) {
					
					$html .= '<div class="alert">Email Sent</div>';
						
				}
							
				$html .= '<h2>'.getAccountName($_GET['id']).'</h2>';
			
				$html .= '<form action="../process/email_config.php" method="post"><input name="configID" value="'.$_GET['configID'].'" type="hidden" /><input name="account_id" value="'.$_GET['id'].'" type="hidden" />';
		
				
				if ($config['send_email'] == 2) {
					$html .= '<p><label>Send Email</label><input name="send_email" type="radio" value="1" /> Yes  <input name="send_email" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Send Email</label><input name="send_email" type="radio" value="1" checked="checked" /> Yes  <input name="send_email" type="radio" value="2" /> No</p>';
				}
				
		
				if ($config == "") {
					$html .= '<p><label>Title</label><input name="email_title" type="text" size="100" value="'.getConfigTitle($_GET['configID']).'" /></p>';
				} else {
					$html .= '<p><label>Title</label><input name="email_title" type="text" size="100" value="'.$config['email_title'].'" /></p>';
				}
		
				$html .= '<p><label>Emails</label><input name="email_addresses" type="text" size="100" value="'.$config['email_addresses'].'" /></p>';
		
		
				if ($config['email_timing'] == 1) {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" checked="checked" /> Hourly <input name="email_timing" type="radio" value="2" /> Daily  <input name="email_timing" type="radio" value="3" /> Weekly  <input name="email_timing" type="radio" value="4" /> Monthly  </p>';
				} else if ($config['email_timing'] == 3) {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" /> Hourly <input name="email_timing" type="radio" value="2" /> Daily  <input name="email_timing" type="radio" value="3" checked="checked" /> Weekly  <input name="email_timing" type="radio" value="4" /> Monthly  </p>';
				} else if ($config['email_timing'] == 4) {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" /> Hourly <input name="email_timing" type="radio" value="2" /> Daily  <input name="email_timing" type="radio" value="3" /> Weekly  <input name="email_timing" type="radio" value="4" checked="checked" /> Monthly  </p>';
				} else {
					$html .= '<p><label>Frequency</label><input name="email_timing" type="radio" value="1" /> Hourly <input name="email_timing" type="radio" value="2" checked="checked" /> Daily  <input name="email_timing" type="radio" value="3" /> Weekly  <input name="email_timing" type="radio" value="4" /> Monthly  </p>';
				}
		

				$html .= '<p><label>Email Send Time</label> '.getHourSelect($config['report_hour'],1).'</p>';
				$html .= '<p><label>Time Range</label> '.getHourSelect($config['range_start'],2).' to '.getHourSelect($config['range_end'],3).'</p>';
		
		
				$html .= '<h3>Oil</h3>';
		
				if ($config['grand_total'] == 2) {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total" type="radio" value="1" /> Yes  <input name="grand_total" type="radio" value="2" checked="checked"/> No</p>';
				} else {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total" type="radio" value="1" checked="checked" /> Yes  <input name="grand_total" type="radio" value="2" /> No</p>';
				}
		
		
				if ($config['month_total'] == 2) {
					$html .= '<p><label>Add Month Total</label><input name="month_total" type="radio" value="1" /> Yes  <input name="month_total" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Add Month Total</label><input name="month_total" type="radio" value="1" checked="checked" /> Yes  <input name="month_total" type="radio" value="2" /> No</p>';
				}
		
				$html .= '<h3>Water</h3>';
		
		
				if ($config['grand_total_water'] == 2) {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total_water" type="radio" value="1" /> Yes  <input name="grand_total_water" type="radio" value="2" checked="checked"/> No</p>';
				} else {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total_water" type="radio" value="1" checked="checked" /> Yes  <input name="grand_total_water" type="radio" value="2" /> No</p>';
				}
		
		
		
				if ($config['month_total_water'] == 2) {
					$html .= '<p><label>Add Month Total</label><input name="month_total_water" type="radio" value="1" /> Yes  <input name="month_total_water" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Add Month Total</label><input name="month_total_water" type="radio" value="1" checked="checked" /> Yes  <input name="month_total_water" type="radio" value="2" /> No</p>';
				}


				$html .= '<h3>Gas</h3>';
		
		
				if ($config['grand_total_gas'] == 2) {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total_gas" type="radio" value="1" /> Yes  <input name="grand_total_gas" type="radio" value="2" checked="checked"/> No</p>';
				} else {
					$html .= '<p><label>Add Grand Total</label><input name="grand_total_gas" type="radio" value="1" checked="checked" /> Yes  <input name="grand_total_gas" type="radio" value="2" /> No</p>';
				}
		
		
		
				if ($config['month_total_gas'] == 2) {
					$html .= '<p><label>Add Month Total</label><input name="month_total_gas" type="radio" value="1" /> Yes  <input name="month_total_gas" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Add Month Total</label><input name="month_total_gas" type="radio" value="1" checked="checked" /> Yes  <input name="month_total_gas" type="radio" value="2" /> No</p>';
				}

		
		
		
		/*		if ($config['month_summary'] == 2) {
					$html .= '<p><label>Monthly Summary</label><input name="month_summary" type="radio" value="1" /> Yes  <input name="month_summary" type="radio" value="2" checked="checked" /> No</p>';
				} else {
					$html .= '<p><label>Monthly Summary</label><input name="month_summary" type="radio" value="1" checked="checked" /> Yes  <input name="month_summary" type="radio" value="2" /> No</p>';
				}
		*/
				
		
		
				$html .= '<hr>';
		
				$html .= '<h3>Production/Sales</h3>';
		
				$html .= '<div class="accordion">
				
							  <h3>Oil</h3>
							
								  <div>
									<p><label>Label</label><input name="section_one_label" type="text" size="50" value="'.$config['section_one_label'].'"  /> * REQUIRED FOR EMAIL</p>
									
									<p style="font-weight:bold;">Entries in this section will be added to OIL totals ONLY</p>';
									
									
									for ($i=0; $i<10; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		
		
			
									

						  $html .= '</div>
							
							  <h3>Gas</h3>
		
								  <div>
									<p><label>Label</label><input name="section_two_label" type="text" size="50" value="'.$config['section_two_label'].'" /> * REQUIRED FOR EMAIL</p>
									
									
									<p style="font-weight:bold;">Entries in this section will be added to GAS totals ONLY</p>';
		
		
									for ($i=10; $i<20; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
				
									
									
							$html .= '</div>
		
							
							  <h3>Water</h3>
							
								  <div>
									<p><label>Label</label><input name="section_three_label" type="text" size="50" value="'.$config['section_three_label'].'"  /> * REQUIRED FOR EMAIL</p>
									
									
									<p style="font-weight:bold;">Entries in this section will be added to WATER totals ONLY</p>';

		
		
									for ($i=20; $i<30; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
				
									
									
		
							$html .= '</div>';
		
								  
							$html .= '</div>
				
							</div>';
		
		
				$html .= '<h3>Flow Meters</h3>';
		
				$html .= '<div class="accordion">
				
							  <h3>Oil</h3>
							
								  <div>
									<p><label>Label</label><input name="section_four_label" type="text" size="50" value="'.$config['section_four_label'].'" /></p>
									
									<p style="font-weight:bold;">Entries in this section will be added to OIL totals ONLY</p>';
		
									for ($i=30; $i<40; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		
		
		
						$html .= '</div>
						
							<h3>Gas</h3>
									
									<div>
										<p><label>Label</label><input name="section_five_label" type="text" size="50" value="'.$config['section_five_label'].'" /></p>
										
										<p style="font-weight:bold;">Entries in this section will be added to GAS totals ONLY</p>';
							
							
									for ($i=40; $i<50; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		


						$html .= '</div>
						
						<h3>Water</h3>
						
								<div>
										<p><label>Label</label><input name="section_six_label" type="text" size="50" value="'.$config['section_six_label'].'" /></p>
										
										<p style="font-weight:bold;">Entries in this section will be added to Water totals ONLY</p>';

									for ($i=50; $i<60; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		

				$html .= '</div></div>';
			
		
				$html .= '<h3>Injected Waters</h3>';
		
				$html .= '<div class="accordion">
				
						<h3>Water</h3>
						
							
							<div>
							
								<p><label>Label</label><input name="section_seven_label" type="text" size="50" value="'.$config['section_seven_label'].'" /></p>
										
									<p style="font-weight:bold;">Entries in this section will be added to Water totals ONLY</p>';

									for ($i=80; $i<90; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		

				$html .= '</div></div>';

		
			
				$html .= '<h3>Pressures</h3>';
		
		
				$html .= '<div class="accordion">
				
							  <h3>PSI</h3>
							
								  <div>
									<p><label>Label</label><input name="section_eight_label" type="text" size="50" value="'.$config['section_eight_label'].'" /></p>';
		
									for ($i=90; $i<100; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		
		
		
/*						$html .= '</div>
						
							<h3>Gas</h3>
							
									<div>
										<p><label>Label</label><input name="section_nine_label" type="text" size="50" value="'.$config['section_nine_label'].'" /></p>';
							
							
									for ($i=100; $i<110; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}
		


						$html .= '</div>
						
						<h3>Water</h3>
						
								<div>
									<p><label>Label</label><input name="section_ten_label" type="text" size="50" value="'.$config['section_ten_label'].'" /></p>';

									for ($i=110; $i<120; $i++) {
									
										if (isset($config['device_deviceID'][$i]) && isset($config['register_id'][$i])) {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'">'.getRegisterSelect($config['device_deviceID'][$i],$config['register_id'],$config['alt_name'],$i,$config['feet_factor']).'</span></p>';
										} else {
											  $html .= '<p><label>Device/Register</label>'.getDeviceSelect($config['device_deviceID'],$_GET['id'],$i+1).' <span id="related_register_'.($i+1).'"></span></p>';
										}
									
									}*/
		

				$html .= '</div></div>';
		
				$html .= '<input name="submit" type="submit" value="Submit" style="margin-top:20px;margin-bottom:150px;width:100px;height:50px;" /> </form>';
		
		
			$html .= '</div>';


	}


	return $html;
	
	
}


function getRegistersConfigSelect($id,$reg,$current,$static=null)  {

	global $link;
	
	if ($static == 1) {
		
		$array = array('None','Oil - Production and Sales','Oil - Tank Level Feet','Water - Meter','Water - Trucked','Water - Injected','Water - Tank Level Feet','Gas - Well Meter','Gas - Check Meter','Gas - Sales Meter','Gas - Flare Meter','Pressure');

		$html = '<select name="register_reporting_config" data-id="'.$reg.'"  data-type="'.$id.'" class="form-control" >';

		$html .= '<option value="">--Choose Reporting Value--</option>';
		
	} else {
		
		$array = array('None','Oil - Production and Sales','Oil - Tank Level Feet','Water - Meter','Water - Trucked','Water - Injected','Water - Tank Level Feet','Gas - Well Meter','Gas - Check Meter','Gas - Sales Meter','Gas - Flare Meter','Pressure');

		$html = '<select name="register_reporting_config" data-id="'.$reg.'"  data-type="'.$id.'" class="configsel" >';

		$html .= '<option value="">--Choose Configuration--</option>';
	}



	foreach ($array as $config) {
		
				if ($current == $config) {
					$html .= '<option value="'.$config.'" selected="selected">'.$config.'</option>';
				} else {
					$html .= '<option value="'.$config.'">'.$config.'</option>';
				}
		}

	$html .= '</select>';

	
	return $html;
}




function getLogCompareField($current)  {

	global $link;
	
		$array = array('Oil Production (ft/in)','Barrels Produced','Oil Sold (ft/in)','Barrels Sold');

		$html = '<select name="log_book_comparison" class="form-control" >';

		$html .= '<option value="">--Choose Comparison--</option>';


	foreach ($array as $config) {
		
				if ($current == $config) {
					$html .= '<option value="'.$config.'" selected="selected">'.$config.'</option>';
				} else {
					$html .= '<option value="'.$config.'">'.$config.'</option>';
				}
		}

	$html .= '</select>';

	
	return $html;
}



function getMathSelect()  {

	global $link;
	
		$array = array('+','-','*','/');

		$html = '<select name="log_book_math[]" class="form-control" >';

		$html .= '<option value="">--Choose Operator--</option>';


	foreach ($array as $config) {
		
				if ($current == $config) {
					$html .= '<option value="'.$config.'" selected="selected">'.$config.'</option>';
				} else {
					$html .= '<option value="'.$config.'">'.$config.'</option>';
				}
		}

	$html .= '</select>';

	
	return $html;
}


?>

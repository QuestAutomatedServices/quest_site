<?php 
include("/var/www/html/quest/reporting/app/functions.php");

ini_set('max_execution_time', '-1'); 
ini_set('memory_limit',"-1");

//error_reporting(E_ALL);

$devices = getDevices();

$startDate = getReportingDate();

if (getReportingDate() > date('Y-m-d')) {
	
	$startDateR = date('Y-m-d H:i:s');
	$startDateR = str_replace(" ","T",$startDateR);

	$endDate = date('Y-m-d 23:59:59');
	$endDate = str_replace(" ","T",$endDate);


	
} else {

	$startDateR = date('Y-m-d H:i:s', strtotime($startDate));
	$startDateR = str_replace(" ","T",$startDateR);

	$endDate = date('Y-m-d H:i:s', strtotime($startDate. ' +1 day'));
	$endDate = str_replace(" ","T",$endDate);

	updateReportingDate($endDate);
	
}




foreach ($devices as $device) {
	
	$regs = getRegisters($device['device_deviceID']);
	

	
	foreach ($regs as $reg) {
		
		$curl = curl_init();
			

			curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curl, CURLOPT_URL, "https://questdashboard.net/datamine.php?querytype=registerhistory&deviceid=".$device['device_deviceID']."&reg=".$reg['register_number']."&startdate=$startDateR&enddate=$endDate");
		
			$xml = curl_exec($curl);
			curl_close($curl);

		
			$xml = simplexml_load_string($xml);
		
		
				foreach($xml->children() as $child) {
				
						foreach($child as $info) {
								
		
							insertReading($reg['register_number'],$info->attributes()->{date},$info[0],$device['device_deviceID']);
							
								
						}
				}

			
		
	}

}

//mail("markmorton@phpprousa.com","Readings done","CRON complete");

?>
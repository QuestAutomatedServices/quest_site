<?php 
include("/var/www/html/quest/reporting/connections/mysql.php");
include("/var/www/html/quest/reporting/functions/functions.php");
include("/var/www/html/quest/reporting/app/functions.php");


$dates = date_range('2017-01-01',date('Y-m-d'));

$devices = getDevices();

/*print_r($devices );
exit();
*/

//truncateTable('tbl_register_calculations');

foreach ($devices as $device) {
	
	$devid = $device['device_deviceID'];
	
	$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = $devid AND register_config = 'Oil - Production and Sales'");
	
	while ($data = mysqli_fetch_array($qry)) {
		

		$regnum = $data['register_number'];
		
		foreach ($dates as $key=>$date) {

				$array = getRegisterReadingsByDate($devid,$regnum,$date);

				$register = getRegisterConfig($devid,$regnum);

				$production = 0;
				$sales = 0;
				$last = count($array) - 1;
			
			


				foreach ($array as $key=>$reading) {

					//If (New – Old) > 0, add to production
					//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

					if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
						$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
						$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
						$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					}


				}

				$factoredprod = ($production*$register['register_factor']);
			   $factoredsales = ($sales*$register['register_factor']);
			 

				mysqli_query($link,"DELETE FROM tbl_register_calculations WHERE register_number = $regnum AND register_deviceID = $devid AND calculation_date = '$date' AND calculation_type = 1 ") or die(mysql_error());

				mysqli_query($link,"INSERT INTO tbl_register_calculations (register_number, register_deviceID, production_calculation, production_calculation_in, sales_calculation, sales_calculation_in, calculation_date, calculation_type) VALUES ($regnum, $devid, $factoredprod, $production, abs($factoredsales), abs($sales), '$date', 1) ") or die(mysql_error());
			
		}

	}





	$qry = mysqli_query($link,"SELECT * FROM tbl_registers WHERE register_deviceID = $devid AND register_config = 'Water - Tank Level Feet'");
	
	while ($data = mysqli_fetch_array($qry)) {
		

		$regnum = $data['register_number'];
		
		foreach ($dates as $key=>$date) {

				$array = getRegisterReadingsByDate($devid,$regnum,$date);

				$register = getRegisterConfig($devid,$regnum);

				$production = 0;
				$sales = 0;
				$last = count($array) - 1;
			
			


				foreach ($array as $key=>$reading) {

					//If (New – Old) > 0, add to production
					//If ((New – Old) < 0) && (abs(New – Old) > 0.05), add to sales

					if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > 0) {
						$production +=	($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < -0.05) {
						$sales += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					} else if ($key != $last && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) > -0.05 && ($array[$key]['register_reading'] - $array[$key+1]['register_reading']) < 0) {
						$production += ($array[$key]['register_reading'] - $array[$key+1]['register_reading']);
					}


				}

			
			   $factoredprod = ($production*$register['register_factor']);
			   $factoredsales = ($sales*$register['register_factor']);
			

				mysqli_query($link,"DELETE FROM tbl_register_calculations WHERE register_number = $regnum AND register_deviceID = $devid AND calculation_date = '$date' AND calculation_type = 2 ") or die(mysql_error());

				mysqli_query($link,"INSERT INTO tbl_register_calculations (register_number, register_deviceID, water_production_calculation, water_trucked_calculation, calculation_date, calculation_type) VALUES ($regnum, $devid, $factoredprod, abs($factoredsales), '$date', 2) ") or die(mysql_error());
			
		}

	}

}
?>
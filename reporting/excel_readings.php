<?php
session_start();
include('connections/mysql.php');
include('functions/functions.php');
ini_set('max_execution_time', '-1'); 
ini_set('memory_limit',"-1");

if (!isset($_SESSION['auth'])) {
	header('location: login.php');
	exit();		
}

$_SESSION['account_id'] = 19;

/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */


/** PHPExcel */
require_once 'phpExcel/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(10);



// Add some data
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Site Name')
            ->setCellValue('B1', 'Register #')
            ->setCellValue('C1', 'Register Label')
            ->setCellValue('D1', 'Reading Date (GMT)')
            ->setCellValue('E1', 'Reading');


	$line = 2;

	$sitename = getDeviceName($_GET['d']);

	if ($_GET['start']) {
		$startdate = formatDateMYSQL($_GET['start']);
		$endate = formatDateMYSQL($_GET['end']);

		$qry = mysqli_query($link,"SELECT * FROM tbl_register_history as h, tbl_registers as r WHERE h.register_number = r.register_number AND h.register_deviceID = r.register_deviceID AND h.register_number = '".$_GET['g']."' AND h.register_deviceID = '".$_GET['d']."' AND date(h.register_date) >= '$startdate' AND date(h,register_date) <= '$endate' ORDER BY h.register_date DESC ");
	
	} else {
		
			
		$qry = mysqli_query($link,"SELECT * FROM  tbl_register_history as h, tbl_registers as r WHERE  h.register_number = r.register_number AND h.register_deviceID = r.register_deviceID AND h.register_number = '".$_GET['g']."' AND h.register_deviceID = '".$_GET['d']."' AND date(h.register_date) >= '".date('Y-m-d', strtotime('-2 days'))."' ORDER BY h.register_date DESC ");
	}

	while ($row = mysqli_fetch_assoc($qry)) {
		
		
			// Add some data
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$line, $sitename)
				->setCellValue('B'.$line, $row['register_number'])
				->setCellValue('C'.$line, $row['register_name'])
				->setCellValue('D'.$line, formatTimestampOffset($row['register_date'],5))
				->setCellValue('E'.$line, $row['register_reading']);

			$line++; 

	}
			

$objPHPExcel->getActiveSheet()->setTitle('Readings Export');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="readings_export.xlsx"');
header('Cache-Control: max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;


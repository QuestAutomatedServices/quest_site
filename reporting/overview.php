<?php 
session_start();
include('connections/mysql.php');
include('app/functions.php');
include('functions/functions.php');

if (!isset($_SESSION['auth']) or $_SESSION['account_id'] != 45 ) {
	header('location: login.php');
	exit();		
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta http-equiv="refresh" content="30">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Portal </title>

    <!-- Bootstrap core CSS -->
    <link href="../beta/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../beta/bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../beta/bootstrap/css/dashboard.css" rel="stylesheet">

    <script src="../beta/bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    <link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="../beta/jquery/jquery.timepicker.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/bootstrap-datetimepicker.min.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/MonthPicker.css"/>

	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          
          <a class="navbar-brand" href="#">Wagner Oil Company</a>
       
                 
       </div>
        
      </div>
      
      <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right controlmenu">
           
           
           		<?php 
			  
			  			echo '<li><a href="overview.php">Dashboard</a></li>';
			  
			  			if (isset($_GET['start'])) { 
							
						  echo '<li><a href="overview.php?start='.$_GET['start'].'&end='.$_GET['end'].'&d=51">North Tank</a></li>';
						  echo '<li><a href="overview.php?start='.$_GET['start'].'&end='.$_GET['end'].'&d=61">South Tank</a></li>';
							
						} else {
							
						  echo '<li><a href="overview.php?d=51">North Tank</a></li>';
						  echo '<li><a href="overview.php?d=61">South Tank</a></li>';
							
						}

			  		     echo '<li><a href="process/logout.php">Logout</a></li>';
			  	?>
		  </ul>
			  </div>
      
    </nav>

    <div class="container-fluid">
     
       <div class="row">
      
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
           	
           		<?php 
			  		
			  			echo '<li><a href="overview.php">Dashboard</a></li>';
			  
			  
			  			if (isset($_GET['start'])) { 
							
						  echo '<li><a href="overview.php?start='.$_GET['start'].'&end='.$_GET['end'].'&d=51">North Tank</a></li>';
						  echo '<li><a href="overview.php?start='.$_GET['start'].'&end='.$_GET['end'].'&d=61">South Tank</a></li>';
							
						} else {
							
						  echo '<li><a href="overview.php?d=51">North Tank</a></li>';
						  echo '<li><a href="overview.php?d=61">South Tank</a></li>';
							
						}
		  		
			  		    //echo '<li><a href="logbook.php?st=1">Settings</a></li>'; 
			  			echo '<li><a href="process/logout.php">Logout</a></li>';
			  	?>
         

			<ul>
        </div>
        
        	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	
        	
        	<?php if (!isset($_GET['d'])) { 
				
					if ($_GET['start']) {
						$datesrange = date_range($_GET['start'],$_GET['end']);
					} else {
						$datesrange = date_range(date("Y-m-d", strtotime("-7 days")),date("Y-m-d"));
						//$datesrange = array(date("Y-m-d"),date("Y-m-d", strtotime("-1 day")),date("Y-m-d", strtotime("-2 days")));
					}

				rsort($datesrange);
				//print_r($datesrange);
							
				
				?>
        		
        		<h1>Dashboard</h1>
        		
        		       	          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b></div>
        		
        		<div id="chart"></div>
        		
        		<h3>Current Level Summary</h3>
        		
        		
        		    <table class="table table-striped" style="margin-bottom: 75px;">

							<thead>
							<tr>			  
							  <th>North Oil Level</th>
							  <th>North Water Level</th>
							  <th>North Top Level</th>
							  <th>South Oil Level</th>
							  <th>South Water Level</th>
							  <th>South Top Level</th>
							</tr>
							</thead>

						   <tbody>
						   
							   <td><a href="overview.php?d=52"><?php echo getLastReading(238,52); ?></a></td>
						   		<td><a href="overview.php?d=51"><?php echo getLastReading(238,51); ?></a></td>
						   		<td><a href="overview.php?d=50"><?php echo getLastReading(238,50); ?></a></td>
						   		<td><a href="overview.php?d=62"><?php echo getLastReading(238,62); ?></a></td>
						   		<td><a href="overview.php?d=61"><?php echo getLastReading(238,61); ?></a></td>
						   		<td><a href="overview.php?d=60"><?php echo getLastReading(238,60); ?></a></td>
						   
						   	</tbody>
				</table>
        		
        			<h3>Water Production & Transfers</h3>
        			
        				<table class="table table-striped">

							<thead>
							<tr>
							  <th>Date</th>					  
							  <th>Water Prod North (BBLS)</th>
							  <th>Water Prod South (BBLS)</th>
							  <th>Water Trans North (BBLS)</th>
							  <th>Water Trans South (BBLS)</th>
							  <th>Comments</th>
							</tr>
							</thead>

						   <tbody>
						   
						   <?php 
									
								foreach ($datesrange as $targetdate) {
									
										if (getTankDiaryCount(238,1,$targetdate) == 1) {
												$color = 'cornflowerblue';
											} else {
												$color = 'darkgrey';
											}
											
									
									echo '<tr>
											<td>'.formatDate($targetdate).'</td>
											<td><a href="overview.php?d=51">'.getTotalWaterProducedByDate(238,51,$targetdate).'</a></td>
											<td><a href="overview.php?d=61">'.getTotalWaterProducedByDate(238,61,$targetdate).'</a></td>
											<td><a href="overview.php?d=51">'.getTotalWaterTruckedByDate(238,51,$targetdate).'</a></td>
											<td><a href="overview.php?d=61">'.getTotalWaterTruckedByDate(238,61,$targetdate).'</a></td>
											
											
											<td style="max-width: 120px;"><i class="fa fa-book gauge_diary" aria-hidden="true" data-date="'.$targetdate.'" style="color: '.$color.';"></i> '.getTankDiaryText(238,1,$targetdate).'</td>
																					
											</tr>';	
									
								}

 ?>
				
       	
       	
       				  </tbody>
				</table>
       	   	
       							    <div id="diary_modal" title="Water Production Diary" style="display: none;">
								    
								    		<form method="post" action="process/battery_diary.php">
											<input type="hidden" name="deviceID" value="238">
											<input type="hidden" name="tank_battery" value="1">
											<input type="hidden" name="diary_date" id="diarymodaldate">
											 
											   <div class="form-group">
											  
												<label for="log_book_field">Entry</label>
												<textarea class="form-control" name="diary_text" id="diary_text_edit" rows="5"></textarea>
												

											  </div>
											  
											  <button type="submit" class="btn btn-default">Submit</button>
											</form>
											
											
											<ul class="list-group" style="margin-top: 20px;">


											
											</ul>
								    
								    	
							   
							   
  									</div>
        	
        	<?php } ?>
        	
        	
        	<?php if (isset($_GET['d'])) { ?>
        
        		<h1><?php echo getRegisterName(238,$_GET['d']); ?></h1>
       	
       	          	 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;">
					<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
					<span></span> <b class="caret"></b></div>
       	
       			
        			<table class="table table-striped">
						  <thead>
							<tr>
							  <th>Register #</th>
							 <!-- <th>GMT Date</th>-->
							  <th>CDT Date (GMT -5)</th>
							  <th>Reading</th>
							</tr>
							</thead>
							
							<tbody>
							
						<?php $readings = getRegisterReadings(238,$_GET['d']); 

									 
						    if ($readings) {

								foreach ($readings as $reading) {
									
									if ($reading['register_number'] == 51) {
										$regname = 'North Tank 1 Water Level';
									} else {
										$regname = 'South Tank 2 Water Level';
									}
									

									echo '<tr>
											<td>'.getRegisterName(238,$_GET['d']).'</td>
											<td>'.formatTimestampOffset($reading['register_date'],5).'</td>
											<td>'.$reading['register_reading'].'</td>

										</tr>'; 	

								}
								
							} else {
								
									echo '<tr>
											<td class="danger" colspan="4">No readings</td>
										</tr>'; 	
							}

						?>
							
						  
						  </tbody>
				</table>
       	                  	
       	        <?php } ?>
        	                  	
		   </div>
      
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="../beta/jquery/external/jquery/jquery.js"></script>
		
    <script src="../beta/bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript" src="../beta/jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="../beta/jquery/jquery.timepicker.js"></script>
	<script type="text/javascript" src="jquery/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript" src="jquery/MonthPicker.js"></script>
	
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../beta/bootstrap/js/ie10-viewport-bug-workaround.js"></script>
    
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/highcharts-more.js"></script>

	<script src="https://code.highcharts.com/modules/solid-gauge.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    
         <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
	<!-- Include Date Range Picker -->
	<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
  	
<script>
               $(function () {
   
				   
				
			      <?php if ($_GET['start'] ) { ?>
				   
				   
				   
				    var start = moment('<?php echo $_GET['start']; ?>');
					 var end = moment('<?php echo $_GET['end']; ?>');

				   
				   
				    <?php } else { ?>
           
					 var start = moment().subtract(29, 'days');
					 var end = moment();
				   
				    <?php } ?>

				function cb(start, end) {
					$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
				}

				$('#reportrange').daterangepicker({
					startDate: start,
					endDate: end,
					ranges: {
					   'Today': [moment(), moment()],
					   'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
					   'Last 7 Days': [moment().subtract(6, 'days'), moment()],
					   'Last 30 Days': [moment().subtract(29, 'days'), moment()],
					   'This Month': [moment().startOf('month'), moment().endOf('month')],
					   'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
					}
				}, cb);

				   	cb(start, end);
			   
			   
				   $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
					   
					   <?php if ($_GET['d'] ) { ?>
					   		
					   		window.location.replace("overview.php?d=<?php echo $_GET['d']; ?>&start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );

												
					   <?php } else { ?> 
					   
					   		window.location.replace("overview.php?start=" + picker.startDate.format('MM/DD/YYYY') + "&end="  + picker.endDate.format('MM/DD/YYYY') );
					   
				   
					   <?php } ?>
					 
					});
			   
			   
			   
			   });
			
		</script>
		
		
		<?php  if (!isset($_GET['d'])) { ?>  
   	
    	<script>
               $(function () {
				   
               
				   Highcharts.chart('chart', {
                    title: {
                        text: '<?php 	
																								 

								echo 'Water Production & Transfers';
									
																							 
									
								 
								 
								 /*if ($_GET['s'] == 2) { 
											echo 'Water Usage';
										} else if ($_GET['s'] == 3) { 
											echo 'Gas Usage';
										} else if ($_GET['s'] == 4) { 
											echo 'Pressure Levels';
										} else { 
											echo 'Oil Production/Sales';
										} */
								?>',
                        x: -20 //center
                    },

                    xAxis: {
                        type: 'datetime',
						tickInterval: 1 * 24 * 3600 * 1000, // one day,
						
							labels: {
	
								 format: '{value:%b %d}',

							}
                    },
                    yAxis: {
                        title: {
                            text: 'BBLS'
                        },
                        plotLines: [{
                            value: 0,
                            width: 1,
                            color: '#808080'
                        }]
                    },
                    tooltip: {
                        //valueSuffix: 'bbl'tooltip: {
						xDateFormat: '%m/%d/%Y',
                   },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [{
                        name: 'Water Production North',
                        data: <?php include('process/time_series_water_wagner.php'); ?>,
						color: '#003cb3'
                   },{
                        name: 'Water Production South',
                        data: <?php include('process/time_series_water_wagner2.php'); ?>,
						color: '#207EB1'
                   },{
                        name: 'Water Transferred North',
                        data: <?php include('process/time_series_water_wagner_prod.php'); ?>,
						color: '#41A430'
                   },{
                        name: 'Water Transferred South',
                        data: <?php include('process/time_series_water_wagner_prod2.php'); ?>,
						color: '#589814'
                   }]
                });
				   
				   
				   
				   $( ".gauge_diary" ).click(function() {	
		
			var ddate = $(arguments[0].target).attr("data-date");	
		
			$( "#diarymodaldate" ).val(ddate);
		
			$.ajax	 ({
				 type: "GET",
				 url: "process/get_tank_diary_entry.php",
				 data: "date="+ddate+"&d=238&bt=1",
				 cache: false,
				 success: function(html) {

					$( "#diary_text_edit" ).val(html);	
				}	
			});
		

			$( "#diary_modal" ).dialog( "open" );
			
	});
 

	$( "#diary_modal" ).dialog({
	  autoOpen: false,
	  height: 'auto',
	  width: 650,
	  modal: true,
	 
	});	
				   
				   
				   
				   
				   
					   });
			
		</script>
		
				<?php  } ?>  
 
	<?php //include('includes/portal_scripts.php'); ?>
    
  </body>
</html>

<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");

if ($_GET['pt'] == 1) {
	
	$table = 'tbl_alarm_contacts';
	$key = 'acontactID';
	$id = $_GET['cid'];
	
    $ac = $_GET['ac'];
	$location = "../alarms.php?f=getAlarmConfigContacts&ac=$ac";
	
}elseif ($_GET['pt'] == 2) {
	
	$table = 'tbl_alarm_configs';
	$key = 'aconfigID';
	$id = $_GET['ac'];
	
	$location = "../alarms.php?f=getConfigureAlarms";
	
}

deleteRecord($id,$table,$key,$_SESSION['user_accountID']);

header("location: $location");


?>
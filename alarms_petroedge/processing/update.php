<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");

/////////// .      PT 2 not in use

if($_POST['pt'] == 1) {

    $table = 'tbl_alarm_configs';
    $key = 'aconfigID';
    
    $ac=$_POST['id'];
    $location = "../alarms.php?f=getAlarmConfig&ac=$ac";
	
}

unset($_POST['pt']);
updateRecordTwo($_POST,$table,$key,300);

header("location: $location");

?>
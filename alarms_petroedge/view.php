<?php
	
	if($_SERVER['SERVER_PORT'] != '443'){

	  header("location: https://www.qautomated.com" . $_SERVER['REQUEST_URI']);
	  exit();

	 }

	session_start();

	if(!isset($_SESSION['auth'])){
		$_SESSION['v'] = 2;
		header("location: ../quest/login.php");
	}

	include("functions/functions.php");
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Automated Services Alarms</title>
    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
      
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    
    <link href="datatables/media/css/dataTables.bootstrap.min.css" rel="stylesheet">
      
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">
    
    <script src="https://use.fontawesome.com/96c3b1dce4.js"></script>

    <link rel="stylesheet" href="jquery/jquery-ui.min.css"/>
  
  </head>
  

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
			<h4 style="color:white; text-align: center;">Reading Alarms</h4>
        </div>
      </div>
    </nav>

    <div class="container-fluid" style="margin-bottom:100px;">
      
        <div class="main">
            
				<?php if(isset($_SESSION['auth'])){
	
						echo getUnseenAlarms($_SESSION['userID'],43); 
			
						}?>             
            
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
    
    
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
		
    <script src="bootstrap/js/bootstrap.js"></script>
    
    <script src="jquery/jquery-ui.min.js"></script>
	  
	<script>
	
	  $('.messageViewer').click(function() {
		  var id = $(this).attr('messageID');
		  var content = $('#messageContent' + id).text();
		  
		  alert(content);
		 
	  })
	  
	  $('.checkedViewer').click(function() {
		  var id4 = $(this).attr('checkedID');
		  $('checkedOptions' + id4).dialog();
	  })
		
	  $('.muteOptViewer').click(function() {
		  var id2 = $(this).attr('muteOptID');
		  $('#muteOptions' + id2).dialog();
		  
	  })
		
	  $('.controllerOptViewer').click(function() {
		  var id3 = $(this).attr('controllerOptID');
		  var content3 = $('#controllerOptions' + id3).html();
		  
		  alert(content3);
		  
	  })
		
		
	</script>
    
  </body>
</html>


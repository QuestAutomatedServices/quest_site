<?php 
session_start();
if (!isset($_SESSION['auth'])) {
	session_destroy();
	header("location: login.php");
	exit();
}

include("../functions/functions.php");

/////////// .      PT 2 not in use

if($_GET['pt'] == 1) {

	$i = $_GET['i'];
	
	$accid = $_GET['a'];
	$db = "account_$accid";
	
	if($_SESSION['auth'] == 1){
		$location = "../alarms.php?f=getRegisterConfig&i=$i&a=$accid";
	}else{
		$location = "../alarms.php?f=getRegisterConfig&i=$i";
	}
	
	updateRegisterAlarmStatus($db,$_GET['r'],$_GET['d'],$_GET['s']);
}elseif($_POST['pt'] == 2) {
	
	if($_POST['optionsRadios'] == 1){
		$contactID = NULL;
	}elseif($_POST['optionsRadios'] == 2){
		$contactID = $_POST['register_contactID'];
	}
	
	$accid = $_POST['a'];
	$db = "account_$accid";
	
	$r = $_POST['r'];
	$d = $_POST['d'];
	
	$location = "../alarms.php?f=getRegAlarmConfigs&r=$r&d=$d&a=$accid";
	
	updateRegisterContactID($db,$r,$d,$contactID);
	
}elseif($_POST['pt'] == 3){
		
	$id = $_POST['aconfigID'];
	$r = $_POST['regID'];
	$d = $_POST['devID'];
	$a = $_POST['accountID'];
	
	unset($_POST['accountID']);
	unset($_POST['regID']);
	unset($_POST['devID']);
	
	if($_POST['optionsRadios'] == 1){
		$_POST['aconfig_contactID'] = 0;
	}
	unset($_POST['optionsRadios']);
	
	$location = "../alarms.php?f=getRegAlarmConfigs&r=$r&d=$d&a=$a";
	
	updateRecordConfig($_POST,$id,$a);
}elseif($_POST['pt'] == 4){

	unset($_POST['pt']);
	updateRecord($_POST,'tbl_users','userID');
	
	$c = $_POST['id'];
	
	$location = "../alarms.php?f=getContact&c=$c";
}elseif($_POST['pt'] == 5){
	
	$timeFactor = $_POST['muteValue'];
	$muteTimestamp = getMuteTimestamp($timeFactor);
	$_POST['aconfig_muteTime'] = $muteTimestamp;
	
	unset($_POST['muteValue']);
	unset($_POST['pt']);
	
	updateRecordTwo($_POST,'tbl_alarmConfigs','aconfigID',$_SESSION['user_accountID']);
	
	$location = "../view.php";
}

header("location: $location");

?>
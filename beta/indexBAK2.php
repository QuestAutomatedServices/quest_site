<?php 
session_start();
error_reporting(E_ALL);

if ($_SERVER['REMOTE_ADDR'] != '68.36.73.102') {
	header('location: noaccess.php');
	exit();		
}


include("app/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Configuration Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">
     
     
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
     
      <link id="themecss" rel="stylesheet" type="text/css" href="app/all.min.css" />
      
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Quest Configuration Portal</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-list">
           
         	<?php echo getAccountSelect(''); ?>

           <li class="nav-header">Tools</li>
           
			  <li><a href="index.php">Sites</a></li>
			  <li><a href="index.php?f=1">File Manager</a></li>
			  <li><a href="index.php?m=1">Map</a></li>
			  <li><a href="index.php?u=1">Users</a></li>


          <li class="nav-header">Configuration</li>
          		<li><a href="index.php?mf=1">Manufacturers</a></li>
			  	<li><a href="index.php?ob=1">Objects</a></li>
          		<li><a href="index.php?dv=1">Devices</a></li>
          		
          		
         	<li class="nav-header">Global</li>
         		<li><a href="index.php?vt=1">Device Types</a></li>
          		<li><a href="index.php?mr=1">Measurements</a></li>
          		<li><a href="index.php?ex=1">Executions</a></li>
          		<li><a href="index.php?tr=1">Triggers</a></li>
          		<li><a href="index.php?ns=1">Notifications</a></li>
          		<li><a href="index.php?fs=1">Functions</a></li>
          		<li><a href="index.php?dt=1">Data Types</a></li>
          		<li><a href="index.php?op=1">Operands</a></li>


          </ul>
        </div>
        
         <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          				<?php if (empty($_GET) ) { ?>
          
          						<h1 class="page-header">Sites</h1>
          
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>Site Name</th>
									  <th>Site Status</th>
									  <th>XML</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$sites = getAccountSites($_SESSION['accountID']); 

										foreach ($sites as $site) {

											$class = '';

											if ($site['status'] == 'Problem') {
												$class = 'class="danger"';
											}

											if ($site['status'] == 'Offline') {
												$class = 'class="info"';
											}

											if ($site['status'] == 'Error') {
												$class = 'class="warning"';
											}

											echo '<tr '.$class.'>
												  <td><a href="index.php?s='.$site['id'].'">'.$site['name'].'</a></td>
												  <td>'.$site['status'].'</td>
												  <td><a href="xml.php?s='.$site['id'].'">Download</a></td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
                            
                            
                            <?php } ?>
                            
                            
                            
                            <?php if (isset($_GET['s'])) { 	 ?>
          
          						<h1 class="page-header">Visual Configurator</h1>
          						
          							       	<div class="btn-group">
												<a href="#" class="btn btn-primary" id="add_controller">Add Controller</a>
												<a href="#" class="btn btn-primary" id="add_object">Add Object</a>
												<a href="#" class="btn btn-primary" id="add_device">Add Device</a>
											</div>
          		
          						
          						
          						<div class="theme-light">
									<ul id="treeview" >
										
										<li data-icon-cls="treeview-icon icon-folder" data-expanded="true"> Site: <?php echo getSiteName($_GET['s']); ?> 
																
											<ul>
												<li><input type="text" class="form-control" name="site_ip_address" placeholder="IP Address" ></li>
												
													
												<li data-icon-cls="treeview-icon icon-folder" data-expanded="true">Controllers
													
													<ul>
															<?php 
														  
														  		$ctrs = getSiteControllers($_GET['s']); 
														  		 

																foreach ($ctrs as $ct) {
																	
																	echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">'.$ct['name'].' <input type="text" class="form-control" name="site_ip_address" placeholder="IP Address" value="'.$ct['ip_address'].'" style="width:150px;float:left;" >';
																	
																		$ctob = getControllerObjects($ct['id']);

																	
																		if ($ctob) {
																			
																			echo '<ul>';
																			
																			foreach ($ctob as $ob) {
																																							
																				echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">'.$ob['objname'].' ('.$ob['name'].')';
																				
																					$obdv = getObjectDevices($ob['contObjID']);
																				
																					if ($obdv) {
																						
																						echo '<ul>';
																			
																						foreach ($obdv as $dv) {
																															
																														
																							echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">'.$dv['name'].'';
																							
	/*																							echo '<ul>';
																							
																									echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Measurements</li>';
																							
																									echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Executions</li>';

																									echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Triggers</li>';

																									echo '<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Notifications</li>';
																							
																								echo '</ul>';*/

																							echo '</li>';
																							
																						}
																						
																						echo '</ul>';
																						
																					}
																			
																				
																				
																				echo '</li>';
																			}

																			
																			echo '</ul>';
																			
																		}
																	
																			
																	
																	
																	echo '</li>';
																}
														  		
															
															?>
													</ul>
												
												</li>
												
												
																								
											
												
												
												
											</ul>
											
										</li>

									</ul>
								</div>
								
								<div id="add_con_modal" title="Add Controller" style="display: none;padding:20px;">
									
									<h4>Add a Controller to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form class="form" method="post" action="process/add_site_controller.php">
 										<input type="hidden" name="site_id" value="<?php echo $_GET['s']; ?>">
  										<div class="form-group">
  											<input type="text" class="form-control" name="name" placeholder="Name" ><br>
											<input type="text" class="form-control" name="ip_address" placeholder="IP Address" >
									     </div>
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>



								<div id="add_obj_modal" title="Add Object" style="display: none;padding:20px;">
									
									<h4>Add an Object to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form class="form" method="post" action="process/add_controller_object.php">
 										<input type="hidden" name="site_id" value="<?php echo $_GET['s']; ?>">
  										<div class="form-group">
												
											<select name="controller_id" class="form-control">
											<option value="">--Choose Controller--</option>
											<?php 
													   
													$ctrs = getSiteControllers($_GET['s']); 

													foreach ($ctrs as $ct) {
														echo '<option value="'.$ct['id'].'">'.$ct['name'].' - '.$ct['ip_address'].'</option>';
													}
			
											?>
							     			</select>
							     			
									     </div>
									     
									     <div class="form-group">
							     			
							     			<select name="object_id" class="form-control">
											<option value="">--Choose Object Type--</option>
											<?php 
													   
													$objs = getObjects(); 

													foreach ($objs as $ob) {
														echo '<option value="'.$ob['id'].'">'.$ob['name'].'</option>';
													}
			
											?>
							     			</select>
								     
									     </div>
									     
									     <div class="form-group">
  											<input type="text" class="form-control" name="name" placeholder="Name" ><br>
									     </div>
									     
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
	
         					
         					
         						<div id="add_dev_modal" title="Add Device" style="display: none;padding:20px;">
									
									<h4>Add a Device to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form class="form" method="post" action="process/add_object_device.php">
									<input type="hidden" name="site_id" value="<?php echo $_GET['s']; ?>">
									
										<div class="form-group">
											<select name="controller_id" id="controllerdevice" class="form-control">
											<option value="">--Choose Controller--</option>
											<?php 

													$ctrs = getSiteControllers($_GET['s']); 

													foreach ($ctrs as $ct) {
														echo '<option value="'.$ct['id'].'">'.$ct['name'].' - '.$ct['ip_address'].'</option>';
													}

											?>
											</select>
										</div>
						     			
						     			<div class="form-group" id="objsel" style="display: none;">
											<div id="controllerObjectSelect"></div>
										</div>
						     			
						     			<div class="form-group" id="objdevsel" style="display: none;">
											<?php echo getDeviceSelect(); ?>
										</div>
							     			
									     <div class="form-group" id="devicebtn"  style="display: none;">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
         					
          					<?php } ?>
          					
          					
          					
          					<?php if (isset($_GET['f'])) { ?>
          
          						<h1 class="page-header">File Manager</h1>
          						
          						<div id="elfinder"></div>
							
							<?php } ?>

							
							<?php  if ($_GET['m'] == 1) { ?>  

								<h2 class="sub-header">Site Map</h2>

								<div id="map"></div>

							<?php  } ?>  
														
							

							<?php  if ($_GET['m'] == 1) { ?>  

								<h2 class="sub-header">Site Map</h2>

								<div id="map"></div>

							<?php  } ?>  
							
							
							
							<?php  if ($_GET['mf'] == 1) { ?>  

								<h2 class="sub-header">Manufacturers <i data-toggle="tooltip" title="You can add Manufacturers to this list for later assignment to devices." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_manufacturer.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Manufacturer Name" >
								     		<input type="url" class="form-control" name="website" placeholder="Website" >
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Manufacturer Name</th>
									  <th>Website</th>
									  <th># Devices</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getManufacturers(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td><a href="index.php?dv=1&mfr='.$m['id'].'">'.$m['name'].'</a></td>
												  <td><a href="'.$m['website'].'" target="_blank">'.$m['website'].'</a></td>
												  <td>'.getMfrDeviceCount($m['id']).'</a></td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
							
							
							<?php  if ($_GET['ob'] == 1) { ?>  

								<h2 class="sub-header">Objects <i data-toggle="tooltip" title="Objects are generic components to which you can assign one or  more devices.  You can also assign a unique ID." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_object.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Object Name" >
								     		<input type="text" class="form-control" name="objectID" placeholder="Object ID" >
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Object Name</th>
									  <th>Object ID</th>
									  <th>Configure</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$obj = getObjects(); 

										foreach ($obj as $o) {

											echo '<tr '.$class.'>
												  <td>'.$o['name'].'</td>
												  <td>'.$o['objectID'].'</td>
												   <td><a href="index.php?pr=1&object='.$o['id'].'">Click here</a></td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
							
							
							
							<?php  if ($_GET['dv'] == 1) { ?>  

								<h2 class="sub-header">Devices <i data-toggle="tooltip" title="Devices are manufacturer specific components that you can add to an Object." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<?php if ($_GET['mfr']) { 
										
											$mfr = getManufacturerData($_GET['mfr']);

										 ?>
										 
									<div class="well info">You are currently viewing devices manufactured by <strong><?php echo $mfr[0]['name']; ?></strong>. <a href="index.php?dv=1">Click here to view all.</a>  </div>
									
									<?php } ?>
								
									<form class="form-inline" method="post" action="process/add_device.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Device Name" >
								     		<input type="text" class="form-control" name="deviceID" placeholder="Device ID" >
								     		<?php echo  getMfrSelect(); ?>
								     		<?php echo  getDeviceTypeSelect(); ?>
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Device Name</th>
									  <th>Device ID</th>
									  <th>Manufacturer</th>
								  	  <th>Type</th>
									  <th>Configure</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$dev = getDevices(); 

										foreach ($dev as $d) {

											echo '<tr '.$class.'>
												  <td>'.$d['name'].'</td>
												  <td>'.$d['deviceID'].'</td>
												  <td>'.$d['mfrname'].'</td>
												  <td>'.$d['devtype'].'</td>
												  <td><a href="index.php?st=1&device='.$d['id'].'&cfn=1">Click here</a></td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
			
							
							<?php  if ($_GET['vt'] == 1) { ?>  

								<h2 class="sub-header">Device Types <i data-toggle="tooltip" title="You can add Device Types to this list." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_device_type.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Device Type" >

											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Device Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getDeviceTypes(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  				
							
							
							<?php  if ($_GET['dt'] == 1) { ?>  

								<h2 class="sub-header">Data Types <i data-toggle="tooltip" title="You can add Data Types to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_data_type.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Data Type" >

											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Data Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getDataTypes(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
							
							
							<?php  if ($_GET['op'] == 1) { ?>  

								<h2 class="sub-header">Operands <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Operand" >
								     		<input type="text" class="form-control" name="operand_type_index" placeholder="Type Index" >
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Operand</th>
									  <th>Type Index</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getOperands(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['operand_type_index'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  
	
							
							<?php  if ($_GET['mr'] == 1) { ?>  

								<h2 class="sub-header">Measurement Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
							     			<?php echo getDeviceTypeSelect(); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Tag</th>
									  <th>Device Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getMeasurements(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['devname'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  						
	
							
							<?php  if ($_GET['ex'] == 1) { ?>  

								<h2 class="sub-header">Execution Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
							     			<?php echo getDeviceTypeSelect(); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Tag</th>
									  <th>Device Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getExecutions(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['devname'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  						

							
							
							
							<?php  if ($_GET['ns'] == 1) { ?>  

								<h2 class="sub-header">Notification Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
							     			<?php echo getDeviceTypeSelect(); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Tag</th>
									  <th>Device Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getNotifications(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['devname'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  	

							<?php  if ($_GET['tr'] == 1) { ?>  

								<h2 class="sub-header">Trigger Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
							     			<?php echo getDeviceTypeSelect(); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Tag</th>
									  <th>Device Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getTriggers(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['devname'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  						
														
							<?php  if ($_GET['fs'] == 1) { ?>  

								<h2 class="sub-header">Function Tags <i data-toggle="tooltip" title="You can add Operands to this list for later assignment to parameters." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
									<form class="form-inline" method="post" action="process/add_operand.php">
  										<div class="form-group">
											<input type="text" class="form-control" name="name" placeholder="Tag" >
							     			<?php echo getDeviceTypeSelect(); ?>
								     		
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
									
								<table class="table table-striped" style="margin-top:20px;">
								  <thead>
									<tr>
									  <th>Tag</th>
									  <th>Device Type</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$mfg = getFunctions(); 

										foreach ($mfg as $m) {

											echo '<tr '.$class.'>
												  <td>'.$m['name'].'</td>
												  <td>'.$m['devname'].'</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
	
							<?php  } ?>  					
																												
							
							<?php  if ($_GET['st'] == 1) { ?>  
								
								
								<?php $dev = getDeviceData($_GET['device']); ?>

								<h2 class="sub-header">Device Settings <i data-toggle="tooltip" title="Devices settings below are specific to the device type you are configuring.  If you don't see a setting that you need, you can add one below." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
								<div class="well info">You are currently configuring: <strong><?php echo $dev[0]['name']; ?></strong>, device type <strong><?php echo $dev[0]['devtype']; ?></strong>, manufactured by <strong><a href="index.php?dv=1&mfr=<?php echo $dev[0]['mfrid']; ?>"><?php echo $dev[0]['mfrname']; ?></a></strong>. The lists below contain options for a <strong><?php echo $dev[0]['devtype']; ?></strong> only! </div>
								
								
								<?php  if (!$_GET['smr']) { ?>
								<div class="btn-group" style="margin-bottom: 20px;">
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1&smr=1" class="btn btn-primary devicesetup"><?php echo $dev[0]['name']; ?> Summary</a>
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 1) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Configuration 1</a>
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=2" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 2) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Configuration 2</a>				
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=3" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 3) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Configuration 3</a>				
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=4" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 4) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Configuration 4</a>
								
								</div>
								<?php  } else { ?>
								<div class="btn-group" style="margin-bottom: 20px;">
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1" class="btn btn-primary devicesetup"><?php echo $dev[0]['name']; ?> Configuration</a>
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=1&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 1) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Measurements</a>
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=2&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 2) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Executions</a>				
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=3&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 3) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Triggers</a>				
									
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=4&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 4) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Notifications</a>
	
									<a href="index.php?st=1&device=<?php echo $_GET['device']; ?>&cfn=5&smr=1" class="btn btn-primary devicesetup <?php if ($_GET['cfn'] == 4) echo "active"; ?> " ><?php echo $dev[0]['name']; ?> Functions</a>							
								</div>
								
								<?php  } ?>
									
									<p class="text-danger">You can have up to 16 settings on each Configuration! NOTE:  Triggers and Notifications MUST be balanced!</p>
									
							
								<!--standard configuration-->
								<?php  if (!$_GET['smr']) { ?>  		
										
											<div class="row">

											  <div class="col-sm-2" style="background-color:#F0F0F0;padding:10px;">Measurement <?php echo $_GET['cfn']; ?> 
											 <!-- <a href="#" id="add_measurement" class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>-->

												 <table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$_GET['cfn'].'" data-id="'.$i.'" data-type="1" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(1,$_GET['device'],$i,$_GET['cfn']).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>

											  <div class="col-sm-2" style="background-color:aliceblue;padding:10px;">Execution <?php echo $_GET['cfn']; ?> 
											 <!-- <a href="#" id="add_execution" class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>-->

												<table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getExecutions(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$_GET['cfn'].'" data-id="'.$i.'" data-type="2" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(2,$_GET['device'],$i,$_GET['cfn']).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>


											  </div>

											  <div class="col-sm-2" style="background-color:#F0F0F0;padding:10px;">Trigger <?php echo $_GET['cfn']; ?> 
											  <!--<a href="#" id="add_trigger" class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>-->

												<table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getTriggers(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$_GET['cfn'].'" data-id="'.$i.'" data-type="3" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(3,$_GET['device'],$i,$_GET['cfn']).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>



											  </div>

											  <div class="col-sm-2" style="background-color:aliceblue;padding:10px;">Notification <?php echo $_GET['cfn']; ?> <!--<a href="#" id="add_notification" class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>-->

												<table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getNotifications(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$_GET['cfn'].'" data-id="'.$i.'" data-type="4" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(4,$_GET['device'],$i,$_GET['cfn']).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>


											  </div>
											  
											  
											  
											 <div class="col-sm-2" style="background-color:#F0F0F0;padding:10px;">Function <?php echo $_GET['cfn']; ?> <!--<a href="#" id="add_notification" class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>-->

												<table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getNotifications(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$_GET['cfn'].'" data-id="'.$i.'" data-type="5" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(5,$_GET['device'],$i,$_GET['cfn']).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>


											  </div>
											  
											  
											<div id="add_setting_modal" title="Device Parameters" style="display: none;padding:20px;">

												<h4>Add <span id="devTypeLabel"></span> - Config #<span id="cfnNumLabel"></span></h4>

												<form action="process/device_settings.php" method="post">
													<input name="device_id" type="hidden" value="<?php echo $_GET['device']; ?>">
													<input name="regnum" id="paramn" type="hidden" value="">
													<input name="cfn" id="paramc" type="hidden" value="">
													<input name="type" id="paramt" type="hidden" value="">
												
													<div class="form-group">
														<label for="id">Tag</label>														
														<div id="tagTypeSelect"></div>
													 </div>

													 <div class="form-group noShowError">
													 	<label for="id">Units</label>														
														<input type="text" class="form-control" name="parameter_units" placeholder="Units">
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Operand</label>														
														<?php echo getOperatorSelect(); ?>
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Data Type</label>														
														<?php echo getDataTypeSelect(); ?>
													 </div>

													 <div class="form-group noShowError">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>


<!--
											<div id="add_measurement_modal" title="Add Measurement" style="display: none;padding:20px;">

												<h4>Add Measurement</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>


													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>


											<div id="add_execution_modal" title="Add Execution" style="display: none;padding:20px;">

												<h4>Add Execution</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>

													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>



											<div id="add_trigger_modal" title="Add Trigger" style="display: none;padding:20px;">

												<h4>Add Trigger</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>

													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>


											<div id="add_notification_modal" title="Add Notification" style="display: none;padding:20px;">

												<h4>Add Notification</h4>

												<form>
													<div class="form-group">														
														<input type="text" class="form-control" name="measurement_name" placeholder="Name">
													 </div>

													 <div class="form-group">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>-->



									 </div>
											
								<?php  } else { ?>
								<!--END standard configuration--> 
											
								<!--summary view--> 
										
										
										<?php if ($_GET['cfn'] == 1) { ?>
										
											<div class="row">
											  
											  
												  <?php 

														for ($r=1; $r<=4; $r++) {

												   ?>

											  <div class="col-sm-3" style="background-color:#F0F0F0;padding:10px;">Measurement <?php echo $r; ?>
												 <table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$r.'" data-id="'.$i.'" data-type="1" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(1,$_GET['device'],$i,$r).'</td>
																</tr>';
															
														}

												   ?>

												 </tbody>
												</table>

											  </div>
											  
											  
												  <?php } ?>
											  
										</div>
										<?php } ?>
										
										
										<?php if ($_GET['cfn'] == 2) { ?>
										<div class="row">
											  
											  
												  <?php 

														for ($r=1; $r<=4; $r++) {

												   ?>

											  <div class="col-sm-3" style="background-color:aliceblue;padding:10px;">Execution <?php echo $r; ?>
												 <table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$r.'" data-id="'.$i.'" data-type="2" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(2,$_GET['device'],$i,$r).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>
											  
											  
												  <?php } ?>
											  
										</div>
										<?php } ?>

									
										<?php if ($_GET['cfn'] == 3) { ?>
										<div class="row">
											  
											  
												  <?php 

														for ($r=1; $r<=4; $r++) {

												   ?>

											  <div class="col-sm-3" style="background-color:#F0F0F0;padding:10px;">Trigger <?php echo $r; ?>
												 <table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$r.'" data-id="'.$i.'" data-type="3" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(3,$_GET['device'],$i,$r).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>
											  
											  
												  <?php } ?>
											  
										</div>
										<?php } ?>
											
										  <?php if ($_GET['cfn'] == 4) { ?>
										  <div class="row">
											  
											  
												  <?php 

														for ($r=1; $r<=4; $r++) {

												   ?>

											  <div class="col-sm-3" style="background-color:aliceblue;padding:10px;">Notification <?php echo $r; ?>
												 <table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$r.'" data-id="'.$i.'" data-type="4" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(4,$_GET['device'],$i,$r).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>
											  
											  
												  <?php } ?>
											  
										</div>	
										<?php } ?>		
										
								
									  <?php if ($_GET['cfn'] == 5) { ?>
										  <div class="row">
											  
											  
												  <?php 

														for ($r=1; $r<=4; $r++) {

												   ?>

											  <div class="col-sm-3" style="background-color:#F0F0F0;padding:10px;">Functions <?php echo $r; ?>
												 <table class="table table-striped table-hover" style="margin-top:20px; ">

												  <tbody>

												  <?php 

														//$mea = getMeasurements(); 

														for ($i=1; $i<=16; $i++) {

															echo '<tr class="setting_row" data-cfg="'.$r.'" data-id="'.$i.'" data-type="5" data-devtype="'.$dev[0]['device_type_id'].'">
																	<td>'.$i.'</td>
																    <td>'.checkSetting(5,$_GET['device'],$i,$r).'</td>
																</tr>';
														}

												   ?>

												 </tbody>
												</table>

											  </div>
											  
											  
												  <?php } ?>
											  
										</div>	
										<?php } ?>		
										
																	
												<div id="add_setting_modal" title="Device Parameters" style="display: none;padding:20px;">

												<h4>Add <span id="devTypeLabel"></span> - Config #<span id="cfnNumLabel"></span></h4>

												<form action="process/device_settings.php" method="post">
													<input name="device_id" type="hidden" value="<?php echo $_GET['device']; ?>">
													<input name="regnum" id="paramn" type="hidden" value="">
													<input name="cfn" id="paramc" type="hidden" value="">
													<input name="type" id="paramt" type="hidden" value="">
													<input name="smr" type="hidden" value="1">
												
													<div class="form-group">
														<label for="id">Tag</label>														
														<div id="tagTypeSelect"></div>
													 </div>

													 <div class="form-group noShowError">
													 	<label for="id">Units</label>														
														<input type="text" class="form-control" name="parameter_units" placeholder="Units">
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Operand</label>														
														<?php echo getOperatorSelect(); ?>
													 </div>
													 
													 <div class="form-group noShowError">
													 	<label for="id">Data Type</label>														
														<?php echo getDataTypeSelect(); ?>
													 </div>

													 <div class="form-group noShowError">
														<button type="submit" class="btn btn-default">Submit</button>
													</div>
												</form>

											</div>														
																																						
											
								<?php  } ?>
								<!--END summary view-->  
												
							<?php  } ?>  
							
							
							
							
							
							<?php  if ($_GET['pr'] == 1) { ?>  
								
								
								<h2 class="sub-header">Object Parameters <i data-toggle="tooltip" title="Up to 8 parameters can be added to an Object." class="fa fa-question-circle" aria-hidden="true"></i></h2>
								
								

								  <div style="background-color:#F0F0F0;padding:10px;">Parameters <a href="#" id="add_parameter" class="pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Add</a>
								  
									 <table class="table table-striped" style="margin-top:20px; ">
	
									  <tbody>
								  
									  <?php 

											$par = getParameters(); 

											foreach ($par as $p) {

												echo '<tr '.$class.'>
													  <td>'.$p['name'].'</td>
  													  <td><input class="form-control" type="checkbox"></td>
													</tr>';
											}

									   ?>
									   
									 </tbody>
									</table>
								  
								  </div>
								  
								 
								 <div id="add_parameter_modal" title="Add Parameter" style="display: none;padding:20px;">
									
									<h4>Add Parameter</h4>
									
									<form>
  										<div class="form-group">														
											<input type="text" class="form-control" name="measurement_name" placeholder="Name">
									     </div>
									     
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
								  
								 					
							<?php  } ?>  
       
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/docs/dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="bootstrap/docs/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
  
		 <?php if (isset($_GET['s'])) { ?>

			<script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

		<?php  } ?> 
    
    
        <?php  if ($_GET['f']) { ?>  
        
			<!-- Require JS (REQUIRED) -->
			<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
			<script data-main="./elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    	<?php  } ?> 
    	
        <?php  if ($_GET['m']) { ?>  
        
        		<script src="http://maps.google.com/maps/api/js"></script>

				<?php include('app/google_maps.php'); ?>
                
        <?php  } ?>  
    
    <?php include("app/common_scripts.php"); ?>
    
  </body>
</html>

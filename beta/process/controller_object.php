<?php 
include("../app/functions.php");
$ctob = getControllerObjects($_GET['id']);

if ($ctob) {
	
	$html = '<select name="object_id" class="form-control">
	<option value="">--Choose Object--</option>';

			foreach ($ctob as $ob) {
				$html .= '<option value="'.$ob['contObjID'].'">'.$ob['objname'].' ('.$ob['name'].')</option>';
			}


	$html .= '</select>';

} else {
	
	$html = '<p class="text-danger">You must add Objects to this Controller before adding a Device!</p>';
	
}


echo $html;

?>
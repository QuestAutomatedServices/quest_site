<?php

include("../app/functions.php");

$type = $_GET['type'];

switch($type){
	
	case "od":
		$table = "object_devices";
		break;
	case "co":
		$table = "controller_objects";
		break;
	case "sc":
		$table = "site_controllers";
		break;
	case "s":
		$table = "sites";
		break;
	case "ra":
		$table = "sites";
		break;
	case "da":
		$table = "sites";
		break;
}


$location = $_SERVER['HTTP_REFERER'];

if ($type == "ra"){
	restoreAllRecords();
}
else if ($type == "da"){
	deleteAllRecords();
} else {
	restoreRecord($table,$_GET['id']);
}

header("location: $location");
?>
<?php 

session_start();
include("../app/functions.php");

$id = $_GET['id'];

switch($_GET['t']){
	case "1":
		$type = "measurements";
		break;
	case "2":
		$type = "triggers";
		break;
	case "3":
		$type = "executions";
		break;
	case "4":
		$type = "notifications";
		break;
	case "5":
		$type = "functions";
		break;
}

$table = "device_$type";

$data = getTagData($table,$id);

$_SESSION['editArray']['edit_type'] = rtrim($type,"s ");
$_SESSION['editArray']['edit_cfn'] = $data['cfn'];
$_SESSION['editArray']['edit_deviceID'] = $data['device_id'];
$_SESSION['editArray']['edit_configID'] = $data['configID'];
$_SESSION['editArray']['edit_tag_id'] = $data['tag_id'];
$_SESSION['editArray']['edit_parameter_units'] = $data['parameter_units'];
$_SESSION['editArray']['edit_parameter_address'] = $data['parameter_address'];
$_SESSION['editArray']['edit_operator_id'] = $data['operator_id'];
$_SESSION['editArray']['edit_data_type_id'] = $data['data_type_id'];
$_SESSION['editArray']['edit_structure_id'] = $data['structure_id'];

header('Location: ' . $_SERVER['HTTP_REFERER']);

?>
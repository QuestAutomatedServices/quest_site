<?php
include("../app/functions.php");

$filename = 'Site' . $_GET['id'] . '.csv';
// export csv

define('HOSTNAME','localhost');
define('DB_USERNAME','root');
define('DB_PASSWORD','');
define('DB_NAME', 'quest_config');
//global $con;
$conn = mysqli_connect(HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME) or die ("error");
// Check connection
if(mysqli_connect_errno($conn))  echo "Failed to connect MySQL: " .mysqli_connect_error();

	$site = $_GET['id'];
    $sql_query = "(SELECT s.Name SiteName,s.Latitude SiteLatitude,s.Longitude SiteLongitude,s.ip_address SiteAddress,s.address_type SiteAddressType,sc.name ControllerName,sc.ip_address ControllerAddress,
	sc.address_type ControllerAddressType,co.name ObjectName,o.name ObjectType,COUNT(op.name) ObjectParameter,GROUP_CONCAT(CONCAT('', op.name, '') ORDER BY op.id SEPARATOR ';' ) AS ObjectParameterName,
	GROUP_CONCAT(CONCAT('', op.value, '') ORDER BY op.id SEPARATOR ';' ) AS ObjectParameterValue,d.name DeviceName,dt.name DeviceType,m.name DeviceManufacturer,'Measurement' OperationType,me.name TagName,me.description TagDescription,
	(SELECT name from device_types WHERE id = me.device_type_id) as TagDeviceType,m.name TagDeviceManufacturer,dm.parameter_address as TagAddress,dty.name  `Data Type` from sites s, 
	site_controllers sc,controller_objects co,objects o,object_devices od,devices d,device_types dt,manufacturers m,measurements me,device_measurements dm,object_parameters op,data_types dty 
	WHERE s.id = ".$site." AND s.id = sc.site_id AND sc.id = co.controller_id AND co.object_id = o.id AND s.id = od.site_id AND sc.id = od.controller_id AND co.id = od.object_id AND od.device_id = d.id AND 
	d.device_type_id = dt.id AND d.manufacturers_id = m.id AND dt.id = me.device_type_id AND me.id = dm.tag_id AND d.id = dm.device_id AND o.id = op.object_id AND dty.id = dm.data_type_id AND s.trash_bin = 0 AND sc.trash_bin = 0 
	AND co.trash_bin = 0 AND od.trash_bin = 0 GROUP BY op.object_id,me.name) 

	UNION ALL 

	(SELECT s.Name SiteName,s.Latitude SiteLatitude,s.Longitude SiteLongitude,s.ip_address SiteAddress,s.address_type SiteAddressType,sc.name ControllerName,sc.ip_address ControllerAddress,
	sc.address_type ControllerAddressType,co.name ObjectName,o.name ObjectType,COUNT(op.name) ObjectParameter,GROUP_CONCAT(CONCAT('', op.name, '') ORDER BY op.id SEPARATOR ';' ) AS ObjectParameterName,
	GROUP_CONCAT(CONCAT('', op.value, '') ORDER BY op.id SEPARATOR ';' ) AS ObjectParameterValue,d.name DeviceName,dt.name DeviceType,m.name DeviceManufacturer,'Trigger' OperationType,t.name TagName ,t.description TagDescription,
	(SELECT name from device_types WHERE id = t.device_type_id) as TagDeviceType,m.name TagDeviceManufacturer,det.parameter_address as TagAddress,dty.name  `Data Type` from sites s, 
	site_controllers sc,controller_objects co,objects o,object_devices od,devices d,device_types dt,manufacturers m,triggers t,device_triggers det,object_parameters op,data_types dty 
	WHERE s.id = ".$site." AND s.id = sc.site_id AND sc.id = co.controller_id AND co.object_id = o.id AND s.id = od.site_id AND sc.id = od.controller_id AND co.id = od.object_id AND od.device_id = d.id AND 
	d.device_type_id = dt.id AND d.manufacturers_id = m.id AND dt.id = t.device_type_id AND t.id = det.tag_id AND d.id = det.device_id AND o.id = op.object_id AND dty.id = det.data_type_id AND s.trash_bin = 0 AND sc.trash_bin = 0 
	AND co.trash_bin = 0 AND od.trash_bin = 0 GROUP BY op.object_id,t.name);";

    // Gets the data from the database
    $result = $conn->query($sql_query);

    $f = fopen('php://temp', 'wt');
    $first = true;
    while ($row = $result->fetch_assoc()) {
        if ($first) {
            fputcsv($f, array_keys($row));
            $first = false;
        }
        fputcsv($f, $row);
    } // end while

    $conn->close();

    $size = ftell($f);
    rewind($f);

    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: $size");
    // Output to browser with appropriate mime type, you choose ;)
    header("Content-type: text/x-csv");
    header("Content-type: text/csv");
    header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    fpassthru($f);
    exit;




?>
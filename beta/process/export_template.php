<?php
include("../app/functions.php");

$filename = 'Template.csv';
// export csv

define('HOSTNAME','localhost');
define('DB_USERNAME','root');
define('DB_PASSWORD','');
define('DB_NAME', 'quest_config');
//global $con;
$conn = mysqli_connect(HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_NAME) or die ("error");
// Check connection
if(mysqli_connect_errno($conn))  echo "Failed to connect MySQL: " .mysqli_connect_error();

    $sql_query = "(SELECT * FROM import_data)";

    // Gets the data from the database
    $result = $conn->query($sql_query);

    $f = fopen('php://temp', 'wt');
    $first = true;
    
	fputcsv($f, array('SiteName', 'SiteLatitude', 'SiteLongitude', 'SiteAddress', 'SiteAddressType','ControllerName','ControllerAddress','ControllerAddressType','ObjectName','ObjectType','ObjectParameter',
				'ObjectParameterName','ObjectParameterValue','DeviceName','DeviceType','DeviceManufacturer','OperationType', 'TagName','TagDescription','TagDeviceType','TagDeviceManufacturer', 'DataType'));

    $conn->close();

    $size = ftell($f);
    rewind($f);

    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Length: $size");
    // Output to browser with appropriate mime type, you choose ;)
    header("Content-type: text/x-csv");
    header("Content-type: text/csv");
    header("Content-type: application/csv");
    header("Content-Disposition: attachment; filename=$filename");
    fpassthru($f);
    exit;




?>
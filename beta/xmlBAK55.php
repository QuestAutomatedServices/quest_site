<?php 
session_start();
error_reporting(E_ALL);

if ($_SERVER['REMOTE_ADDR'] != '68.36.73.102') {
	header('location: noaccess.php');
	exit();		
}

include("app/functions.php"); 

header("Content-type: text/xml");

$xml = '<!-- ********************** MainTemplate Rev F.xml ************************* -->
<UANodeSet xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:uax="http://opcfoundation.org/UA/2008/02/Types.xsd" xmlns="http://opcfoundation.org/UA/2011/03/UANodeSet.xsd"  xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <NamespaceUris>
    <Uri>http://questautomatedservices.com/opcua/</Uri>
  </NamespaceUris>
  <Aliases>
    <Alias Alias="Boolean">i=1</Alias>
    <Alias Alias="SByte">i=2</Alias>
    <Alias Alias="Byte">i=3</Alias>
    <Alias Alias="Int16">i=4</Alias>
    <Alias Alias="UInt16">i=5</Alias>
    <Alias Alias="Int32">i=6</Alias>
    <Alias Alias="UInt32">i=7</Alias>
    <Alias Alias="Int64">i=8</Alias>
    <Alias Alias="UInt64">i=9</Alias>
    <Alias Alias="Float">i=10</Alias>
    <Alias Alias="Double">i=11</Alias>
    <Alias Alias="String">i=12</Alias>
    <Alias Alias="DateTime">i=13</Alias>
    <Alias Alias="Guid">i=14</Alias>
    <Alias Alias="ByteString">i=15</Alias>
    <Alias Alias="XmlElement">i=16</Alias>
    <Alias Alias="NodeId">i=17</Alias>
    <Alias Alias="ExpandedNodeId">i=18</Alias>
    <Alias Alias="StatusCode">i=19</Alias>
    <Alias Alias="QualifiedName">i=20</Alias>
    <Alias Alias="LocalizedText">i=21</Alias>
    <Alias Alias="Structure">i=22</Alias>
    <Alias Alias="Number">i=26</Alias>
    <Alias Alias="Integer">i=27</Alias>
    <Alias Alias="UInteger">i=28</Alias>
    <Alias Alias="Enumeration">i=29</Alias>
    <Alias Alias="Image">i=30</Alias>
    <Alias Alias="Organizes">i=35</Alias>
    <Alias Alias="HasEventSource">i=36</Alias>
    <Alias Alias="HasModellingRule">i=37</Alias>
    <Alias Alias="HasEncoding">i=38</Alias>
    <Alias Alias="HasDescription">i=39</Alias>
    <Alias Alias="HasTypeDefinition">i=40</Alias>
    <Alias Alias="HasSubtype">i=45</Alias>
    <Alias Alias="HasProperty">i=46</Alias>
    <Alias Alias="HasComponent">i=47</Alias>
    <Alias Alias="HasNotifier">i=48</Alias>
    <Alias Alias="IdType">i=256</Alias>
    <Alias Alias="Duration">i=290</Alias>
    <Alias Alias="NumericRange">i=291</Alias>
    <Alias Alias="UtcTime">i=294</Alias>
    <Alias Alias="Argument">i=296</Alias>
    <Alias Alias="ServerState">i=852</Alias>
    <Alias Alias="Range">i=884</Alias>
    <Alias Alias="Tag">i=ns=1;i=2016</Alias>
  </Aliases>
  <!--******************************************************************************************************************************
  ***************************** QUEST AUTOMATION SERVICE ***************************************************************************
  **********************************************************************************************************************************-->  
  <!--**************************************  TYPES DEFINITIONS *********************************************************************-->
  <!-- ********************** Site Type Definition************************* -->
  <UAObjectType NodeId="ns=1;i=1011" BrowseName="1:SiteType">
    <DisplayName>SiteType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=58</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6111</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6112</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6115</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6113</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6114</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5110</Reference>
    </References>
  </UAObjectType>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1011" NodeId="ns=1;i=6111" BrowseName="1:Address">
    <DisplayName>Address</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=63</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;i=1011</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1011" NodeId="ns=1;i=6112" BrowseName="1:AddressType" >
    <DisplayName>AddressType</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=63</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;i=1011</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1011" NodeId="ns=1;i=6115" BrowseName="1:ControllerNumber">
    <DisplayName>Controller Number</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1011</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1011" NodeId="ns=1;i=6113" BrowseName="1:Location">
    <DisplayName>Location</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1011</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1011" NodeId="ns=1;i=6114" BrowseName="1:LocationType" >
    <DisplayName>Location Type</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1011</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAObject ParentNodeId="ns=1;i=1011" NodeId="ns=1;i=5110" BrowseName="1:Controllers">
    <DisplayName>Controllers</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1022</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1011</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <!-- ***End Site Type Definition*** -->
  <!-- ************************Controller Type Definition************************ -->
  <UAObjectType NodeId="ns=1;i=1012" BrowseName="1:ControllerType">
    <DisplayName>ControllerType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=58</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6120</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6121</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6122</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6125</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5120</Reference>
    </References>
  </UAObjectType>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1012" NodeId="ns=1;i=6120" BrowseName="1:Id">
    <DisplayName> Id</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1012</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1012" NodeId="ns=1;i=6121" BrowseName="1:Address">
    <DisplayName>Address</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=63</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;i=1012</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1012" NodeId="ns=1;i=6122" BrowseName="1:AddressType" >
    <DisplayName>AddressType</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=63</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;i=1012</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1012" NodeId="ns=1;i=6125" BrowseName="1:ObjectNumber">
    <DisplayName>Object Number</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1012</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAObject ParentNodeId="ns=1;i=1012" NodeId="ns=1;i=5120" BrowseName="1:Objects">
    <DisplayName>Objects</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1023</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1012</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <!-- ***End Controller Type Definition*** -->
  <!-- ********************* Object Type Definition **************************** -->
  <UAObjectType NodeId="ns=1;i=1013" BrowseName="1:ObjectType">
    <DisplayName>ObjectType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=58</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6130</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6135</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5130</Reference>
    </References>
  </UAObjectType>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1013" NodeId="ns=1;i=6130" BrowseName="1:Id">
    <DisplayName> Id</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1013</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1013" NodeId="ns=1;i=6135" BrowseName="1:DeviceNumber">
    <DisplayName>Device Number</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1013</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAObject ParentNodeId="ns=1;i=1013" NodeId="ns=1;i=5130" BrowseName="1:Devices">
    <DisplayName>Devicess</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1024</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1013</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <!-- *** End Object Type Definition *** -->
  <!-- *************************************** Device Type Definition ***************************************** -->
  <UAObjectType NodeId="ns=1;i=1014" BrowseName="1:DeviceType">
    <DisplayName>DeviceType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=58</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6140</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6141</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6142</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5141</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5142</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5143</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5144</Reference>
      <Reference ReferenceType="Organizes">ns=1;i=5145</Reference>
    </References>
  </UAObjectType>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=6140" BrowseName="1:Id">
    <DisplayName> Id</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=6141" BrowseName="1:Address">
    <DisplayName>Address</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=63</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=6142" BrowseName="1:AddressType" >
    <DisplayName>AddressType</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=63</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAObject ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=5141" BrowseName="1:Measurements">
    <DisplayName>Measurements</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1026</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <UAObject ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=5142" BrowseName="1:Executions">
    <DisplayName>Executions</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1027</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <UAObject ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=5143" BrowseName="1:Triggers">
    <DisplayName>Triggers</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1028</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <UAObject ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=5144" BrowseName="1:Notifications">
    <DisplayName>Notifications</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1029</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <UAObject ParentNodeId="ns=1;i=1014" NodeId="ns=1;i=5145" BrowseName="1:Functions">
    <DisplayName>Functions</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1030</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1014</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAObject>
  <!--End Device Type Definition --> 
  <!-- *************************************** Operation Type Definitions ************************************* -->
  <UAObjectType NodeId="ns=1;i=1015" BrowseName="1:OperationGroupeType">
    <DisplayName>OperationGroupeType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=58</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6153</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6154</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=6155</Reference>
      <Reference ReferenceType="HasComponent">ns=1;i=6157</Reference>
    </References>
  </UAObjectType>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1015" NodeId="ns=1;i=6153" BrowseName="1:OperationGroupeId" >
    <DisplayName>Operation Id</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1015</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1015" NodeId="ns=1;i=6154" BrowseName="1:OperationTypeGroup">
    <DisplayName>Group Id</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1015</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=1015" NodeId="ns=1;i=6155" BrowseName="1:DataPointNumber">
    <DisplayName>Data Point Number</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=1015</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAObject ParentNodeId="ns=1;i=1015" NodeId="ns=1;i=6157" BrowseName="1:Tags">
    <DisplayName>Tags</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1031</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;i=1015</Reference>
      <Reference ReferenceType="HasModellingRule">i=80</Reference>
    </References>
  </UAObject>
  <!-- *** End Operation Type Definitions *** -->
  <!-- *************************************** Tag Type Definitions ******************************************* -->
  <!-- Tag Variable Type Definitions -->
  <UAVariableType NodeId="ns=1;i=2016" BrowseName="TagVariableType" DataType="Number" ValueRank="-2">
    <DisplayName>TagVariableType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=63</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=2161</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=2162</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=2163</Reference>
    </References>
  </UAVariableType>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=2016" NodeId="ns=1;i=2161" BrowseName="Id">
    <DisplayName>Id</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=2016</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=2016" NodeId="ns=1;i=2162" BrowseName="Address">
    <DisplayName>Address</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=2016</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=2016" NodeId="ns=1;i=2163" BrowseName="OperandTypeIndex">
    <DisplayName>Operand</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=2016</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;i=2016" NodeId="ns=1;i=2164" BrowseName="OperationTypeIndex">
    <DisplayName>Operation Index</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=2016</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <!-- end Tag Variable Type Definitions -->

  <!-- Address Variable Type Definitions -->
  <UAVariableType NodeId="ns=1;i=2017" BrowseName="AddressVariableType" DataType="String" ValueRank="-2">
    <DisplayName>AddressVariableType</DisplayName>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=63</Reference>
      <Reference ReferenceType="HasProperty">ns=1;i=2165</Reference>
    </References>
  </UAVariableType>
  <UAVariable DataType="String" ParentNodeId="ns=1;i=2017" NodeId="ns=1;i=2165" BrowseName="AddressType">
    <DisplayName>AddressType</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;i=2016</Reference>
      <Reference ReferenceType="HasModellingRule">i=78</Reference>
    </References>
  </UAVariable>
  <!-- end Address Variable Type Definitions -->

  <!-- *************************************** Quest Folder Types Definitions. ******************************* -->
  <UAObjectType NodeId="ns=1;i=1022" BrowseName="ControllersContainerType">
    <DisplayName>Controllers Container Type</DisplayName>
    <Description>The container type for Controllers that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=61</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1023" BrowseName="ObjectsContainerType">
    <DisplayName>ObjectsContainerType</DisplayName>
    <Description>The container type for Objects that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=61</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1024" BrowseName="DevicesContainerType">
    <DisplayName>DevicesContainerType</DisplayName>
    <Description>The container type for Devices that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=61</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1025" BrowseName="OperationsContainerType">
    <DisplayName>OperationsContainerType</DisplayName>
    <Description>The type for Operations that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=61</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1026" BrowseName="MeasurumentsContainerType">
    <DisplayName>MeasurumentsContainerType</DisplayName>
    <Description>The type for Measuruments that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">ns=1;i=1025</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1027" BrowseName="ExecutionsContainerType">
    <DisplayName>ExecutionsContainerType</DisplayName>
    <Description>The type for Executions that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">ns=1;i=1025</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1028" BrowseName="TriggersContainerType">
    <DisplayName>TriggersContainerType</DisplayName>
    <Description>The type for Triggers that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">ns=1;i=1025</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1029" BrowseName="NotificationsContainerType">
    <DisplayName>NotificationsContainerType</DisplayName>
    <Description>The type for Notifications that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">ns=1;i=1025</Reference>
    </References>
  </UAObjectType>
  <UAObjectType NodeId="ns=1;i=1030" BrowseName="FunctionsContainerType">
    <DisplayName>FunctionsContainerType</DisplayName>
    <Description>The type for Functions that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">ns=1;i=1025</Reference>
    </References>
  </UAObjectType>
    <UAObjectType NodeId="ns=1;i=1031" BrowseName="TagsContainerType">
    <DisplayName>TagsContainerType</DisplayName>
    <Description>The type for Tags that organize other nodes.</Description>
    <References>
      <Reference ReferenceType="HasSubtype" IsForward="false">i=61</Reference>
    </References>
  </UAObjectType>
  
  
  	 <!-- ************************* SiteTemplate Rev F.xml ************************* -->
     <!-- ************************* Site 1 ************************* -->

  <UAObject NodeId="ns=1;s=Site1" BrowseName="1:'.str_replace(" ","",getSiteName($_GET['s'])).'">
    <DisplayName>'.getSiteName($_GET['s']).'</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1011</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">i=84</Reference>
      <Reference ReferenceType="HasComponent">ns=1;s=Site1.Address</Reference>
      <Reference ReferenceType="HasComponent">ns=1;s=Site1.ControllerNumber</Reference>
      <Reference ReferenceType="HasComponent">ns=1;s=Site1.Location</Reference>
      <Reference ReferenceType="Organizes">ns=1;s=Site1.Controllers</Reference>
    </References>
  </UAObject>
  <UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1" NodeId="ns=1;s=Site1.Address" BrowseName="1:Address" >
    <DisplayName>Address</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=2017</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1</Reference>
	  <Reference ReferenceType="HasProperty">ns=1;s=Site1.AddressType</Reference>
    </References>
    <Value>
      <uax:String>'.getSiteIP($_GET['s']).'</uax:String>
    </Value>
  </UAVariable>
  <UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1.Address" NodeId="ns=1;s=Site1.AddressType" BrowseName="1:AddressType">
    <DisplayName>AddressType</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Address</Reference>
    </References>
    <Value>
      <uax:String>'.getSiteAddressType($_GET['s']).'</uax:String>
    </Value>
  </UAVariable>
  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1" NodeId="ns=1;s=Site1.ControllerNumber" BrowseName="1:ControllerNumber">
    <DisplayName>Controller Number</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1</Reference>
    </References>
    <Value>
      <uax:UInt16>'.getSiteControllerCount($_GET['s']).'</uax:UInt16>
    </Value>
  </UAVariable>
  <UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1" NodeId="ns=1;s=Site1.Location" BrowseName="1:Location" >
    <DisplayName>Location</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=2017</Reference>
      <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1</Reference>
	   <Reference ReferenceType="HasProperty">ns=1;s=Site1.LocationType</Reference>
    </References>
    <Value>
      <uax:String>'.getSiteGeo($_GET['s']).'</uax:String>
    </Value>
  </UAVariable>
  <UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1.Location" NodeId="ns=1;s=Site1.LocationType" BrowseName="1:LocationType">
    <DisplayName>Location Type</DisplayName>
    <References>
      <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
      <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Location</Reference>
    </References>
    <Value>
      <uax:String>[U:LOCATION_TYPE_VAL]</uax:String>
    </Value>
  </UAVariable>
  <UAObject NodeId="ns=1;s=Site1.Controllers" BrowseName="1:Controllers">
    <DisplayName>Controllers</DisplayName>
    <Description Locale="en-US">Controllers associated to their site.</Description>
    <References>
      <Reference ReferenceType="HasTypeDefinition">ns=1;i=1022</Reference>
      <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1</Reference>';

			for ($i=1; $i <= getSiteControllerCount($_GET['s']); $i++ ) {
				$xml .= '<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$i.'</Reference>';
			}

    $xml .= '</References>
  </UAObject>
 <!-- End  Site1  -->';


	$ctrs = getSiteControllers($_GET['s']); 


		$controllernum = 1;

		foreach ($ctrs as $ct) {
			
				
			  $xml .= '<!-- ************************* ControllerTemplate Rev F.xml ************************* -->
			  
			     <!-- ************************* Controller'.$controllernum.' ************************* -->
				  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'" BrowseName="1:'.str_replace(" ","",$ct['name']).'">
					<DisplayName>'.$ct['name'].'</DisplayName>
					<References>
					  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1012</Reference>
					  <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controllers</Reference>
					  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Id</Reference>
					  <Reference ReferenceType="HasComponent">ns=1;s=Site1.Controller'.$controllernum.'.Address</Reference>
					  <Reference ReferenceType="HasProperty">ns=1;s=Site1.s=Site1.Controller'.$controllernum.'.ObjectNumber</Reference>
					  <Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Objects</Reference>
					</References>
				  </UAObject>
				   <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Id" BrowseName="1:Id">
					<DisplayName>Id</DisplayName>
					<References>
					  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
					  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'</Reference>
					</References>
					<Value>
					  <uax:UInt16>'.$controllernum.'</uax:UInt16>
					</Value>
				  </UAVariable>
				  <UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Address" BrowseName="1:Address" >
					<DisplayName>Address</DisplayName>
					<References>
					  <Reference ReferenceType="HasTypeDefinition">ns=1;i=2017</Reference>
					  <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'</Reference>
					  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.AddressType</Reference>
					</References>
					<Value>
					  <uax:String>'.$ct['ip_address'].'</uax:String>
					</Value>
				  </UAVariable>
				  <UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Address" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.AddressType" BrowseName="1:AddressType">
					<DisplayName>Address Type</DisplayName>
					<References>
					  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
					  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Address</Reference>
					</References>
					<Value>
					  <uax:String>'.$ct['address_type'].'</uax:String>
					</Value>
				  </UAVariable>
				  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.ObjectNumber" BrowseName="1:ObjectNumber">
					<DisplayName>Object Number</DisplayName>
					<References>
					  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
					  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'</Reference>
					</References>
					<Value>
					  <uax:UInt16>'.getControllerObjectsCount($ct['id']).'</uax:UInt16>
					</Value>
				  </UAVariable>
				  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Objects" BrowseName="1:Objects">
					<DisplayName>Objects</DisplayName>
					<Description Locale="en-US"></Description>
					<References>
					  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1023</Reference>';
					  	
						for ($i=1; $i <= getControllerObjectsCount($ct['id']); $i++ ) {
							$xml .= '<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$i.'</Reference>';
						}
			
			
			
					 $xml .= '</References>
				  </UAObject>
				  <!-- End Controller'.$controllernum.'  -->';
				
			
			$ctob = getControllerObjects($ct['id']);
			
			
			$objectnum = 1;
			
			foreach ($ctob as $ob) {
				
				$xml .= '<!-- ************************* ObjectTemplate Rev F.xml  ************************* -->
				
				
							<!-- ************************* Object'.$objectnum.' ************************* -->
							<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'" BrowseName="1:'.str_replace(" ","",$ob['name']).'">
							  <DisplayName>'.$ob['name'].'</DisplayName>
							  <References>
								<Reference ReferenceType="HasTypeDefinition">ns=1;i=1013</Reference>
								<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Objects</Reference>
								<Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Id</Reference>
								<Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.DeviceNumber</Reference>
								<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Devices</Reference>
							  </References>
							</UAObject>
							<UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Id" BrowseName="1:Id">
							  <DisplayName> Id</DisplayName>
							  <References>
								<Reference ReferenceType="HasTypeDefinition">i=68</Reference>
								<Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'</Reference>
							  </References>
							  <Value>
								<uax:UInt16>'.$objectnum.'</uax:UInt16>
							  </Value>
							</UAVariable>
							<UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.DeviceNumber" BrowseName="1:DeviceNumber">
							  <DisplayName>Device Number</DisplayName>
							  <References>
								<Reference ReferenceType="HasTypeDefinition">i=68</Reference>
								<Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'</Reference>
							  </References>
							  <Value>
								<uax:UInt16>'.getObjectDevicesCount($ob['id']).'</uax:UInt16>
							  </Value>
							</UAVariable>
							<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Devices" BrowseName="1:Devices">
							  <DisplayName>Devices</DisplayName>
							  <Description Locale="en-US">Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Devices</Description>
							  <References>
								<Reference ReferenceType="HasTypeDefinition">ns=1;i=1024</Reference>
								<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'</Reference>';
				
								for ($i=1; $i <= getObjectDevicesCount($ob['id']); $i++ ) {
									$xml .= '<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$i.'</Reference>';
								}
				
					  $xml .= '</References>
							</UAObject>
							<!-- End Object'.$objectnum.'  -->';
							
									
								
							$obdv = getObjectDevices($ob['contObjID']);
			
							$devicenum = 1;

							foreach ($obdv as $dv) {
								
										
									$xml .= '<!-- ************************* DeviceTemplate Rev F.xml ************************* -->
									
									
											 <!-- ************************* Device'.$devicenum.' ************************* -->
												<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'" BrowseName="1:'.str_replace(" ","",$dv['name']).'">
												  <DisplayName>'.$dv['name'].'</DisplayName>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=1014</Reference>
													<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Devices</Reference>
													<Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Id</Reference>
													<Reference ReferenceType="HasComponent">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Address</Reference>
													<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Measurements</Reference>
													<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Executions</Reference>
													<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Triggers</Reference>
													<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Notifications</Reference>
													<Reference ReferenceType="Organizes">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Functions</Reference>
												  </References>
												</UAObject>
												<UAVariable DataType="UInt16"  ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Id" BrowseName="1:Id">
												  <DisplayName> Id</DisplayName>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">i=68</Reference>
													<Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>
												  </References>
												  <Value>
													<uax:UInt16>'.$devicenum.'</uax:UInt16>
												  </Value>
												</UAVariable>
												<UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Address" BrowseName="1:Address">
												  <DisplayName>Address</DisplayName>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=2017</Reference>
													<Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>
													<Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.AddressType</Reference>
												  </References>
												  <Value>
													<uax:String>'.$dv['ip_address'].'</uax:String>
												  </Value>
												</UAVariable>
												<UAVariable DataType="String"  ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.AddressType" BrowseName="1:AddressType">
												  <DisplayName>Address Type</DisplayName>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">i=63</Reference>
													<Reference ReferenceType="HasProperty" IsForward="false">s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Address</Reference>
												  </References>
												  <Value>
													<uax:String>'.$dv['address_type'].'</uax:String>
												  </Value>
												</UAVariable>
												<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Measurements" BrowseName="1:Measurements">
												  <DisplayName>Measurements</DisplayName>
												  <Description Locale="en-US"></Description>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=1026</Reference>
													<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>';
													
													$operationnumber = 1;
													$operationname = 'Measurements';
								
													for ($i=1; $i<=4; $i++) {
								
													$xml .= '<!-- ************************* OperationTemplate Rev F.xml ************************* -->
														<!--Operation'.$operationnumber.'.Group'.$i.'-->
														  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" BrowseName="1:'.$operationname.''.$i.'">
															<DisplayName>'.$operationname.''.$i.'</DisplayName>
															<Description Locale="en-US"></Description>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1015</Reference>
															  <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.'.$operationname.'s</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationType</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber</Reference>';    
															  
														
															for ($t=1; $t<=16; $t++) {
														
																$xml .= '<!-- ************************* Tag'.$t.' ************************* -->
															  <UAVariable DataType="[U:TAG_DATATYPE_NUM]" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" BrowseName="1:[U:TAG_BROWSE_NAME]">
																<DisplayName>[U:TAG_DISPLAY_NAME]</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">ns=1;i=2016</Reference>
																  <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex</Reference>
																  [U:REF_TO_OTHER_TAG]
																</References>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id" BrowseName="Id">
																<DisplayName> Id</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																 <Value>
																   <uax:UInt16>'.$t.'</uax:UInt16>
																 </Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address" BrowseName="Address">
																<DisplayName>Address</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_ADDRESS_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex" BrowseName="OperandTypeIndex">
																<DisplayName>Operand Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPRAND_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex" BrowseName="OperationTypeIndex">
																<DisplayName>Operation Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPERATION_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <!--End Tag'.$t.'-->';
																
															}

														
															$xml .= '</References>
														  </UAObject>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeId" BrowseName="1:OperationTypeId" >
															<DisplayName> Operation Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$operationnumber.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup" BrowseName="1:OperationGroupId">
															<DisplayName> Group Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$i.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber" BrowseName="1:DataPointNumber">
															<DisplayName> Data Point Number</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>[U:TAG_QUANTITY_VAL]</uax:UInt16>
															</Value>
														  </UAVariable>
														<!-- End Operation'.$operationnumber.'.Group'.$i.'-->';
													
														}
													
												 $xml .= '</References>
												</UAObject>
												<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Executions" BrowseName="1:Executions">
												  <DisplayName>Executions</DisplayName>
												  <Description Locale="en-US"></Description>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=1027</Reference>
													<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>';
								
								
													$operationnumber = 2;
													$operationname = 'Executions';
								
													for ($i=1; $i<=4; $i++) {
								
													$xml .= '<!-- ************************* OperationTemplate Rev F.xml ************************* -->
														<!--Operation'.$operationnumber.'.Group'.$i.'-->
														  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" BrowseName="1:'.$operationname.''.$i.'">
															<DisplayName>'.$operationname.''.$i.'</DisplayName>
															<Description Locale="en-US"></Description>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1015</Reference>
															  <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.'.$operationname.'s</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationType</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber</Reference>';    
															  
														
															for ($t=1; $t<=16; $t++) {
														
																$xml .= '<!-- ************************* Tag'.$t.' ************************* -->
															  <UAVariable DataType="[U:TAG_DATATYPE_NUM]" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" BrowseName="1:[U:TAG_BROWSE_NAME]">
																<DisplayName>[U:TAG_DISPLAY_NAME]</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">ns=1;i=2016</Reference>
																  <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex</Reference>
																  [U:REF_TO_OTHER_TAG]
																</References>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id" BrowseName="Id">
																<DisplayName> Id</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																 <Value>
																   <uax:UInt16>'.$t.'</uax:UInt16>
																 </Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address" BrowseName="Address">
																<DisplayName>Address</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_ADDRESS_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex" BrowseName="OperandTypeIndex">
																<DisplayName>Operand Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPRAND_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex" BrowseName="OperationTypeIndex">
																<DisplayName>Operation Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPERATION_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <!--End Tag'.$t.'-->';
																
															}

														
															$xml .= '</References>
														  </UAObject>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeId" BrowseName="1:OperationTypeId" >
															<DisplayName> Operation Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$operationnumber.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup" BrowseName="1:OperationGroupId">
															<DisplayName> Group Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$i.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber" BrowseName="1:DataPointNumber">
															<DisplayName> Data Point Number</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>[U:TAG_QUANTITY_VAL]</uax:UInt16>
															</Value>
														  </UAVariable>
														<!-- End Operation'.$operationnumber.'.Group'.$i.'-->';
													
														}
								
												   $xml .= '</References>
												</UAObject>
												<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Triggers" BrowseName="1:Triggers">
												  <DisplayName>Triggers</DisplayName>
												  <Description Locale="en-US"></Description>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=1028</Reference>
													<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>';
								
													$operationnumber = 3;
													$operationname = 'Triggers';
								
													for ($i=1; $i<=4; $i++) {
								
													$xml .= '<!-- ************************* OperationTemplate Rev F.xml ************************* -->
														<!--Operation'.$operationnumber.'.Group'.$i.'-->
														  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" BrowseName="1:'.$operationname.''.$i.'">
															<DisplayName>'.$operationname.''.$i.'</DisplayName>
															<Description Locale="en-US"></Description>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1015</Reference>
															  <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.'.$operationname.'s</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationType</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber</Reference>';    
															  
														
															for ($t=1; $t<=16; $t++) {
														
																$xml .= '<!-- ************************* Tag'.$t.' ************************* -->
															  <UAVariable DataType="[U:TAG_DATATYPE_NUM]" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" BrowseName="1:[U:TAG_BROWSE_NAME]">
																<DisplayName>[U:TAG_DISPLAY_NAME]</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">ns=1;i=2016</Reference>
																  <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex</Reference>
																  [U:REF_TO_OTHER_TAG]
																</References>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id" BrowseName="Id">
																<DisplayName> Id</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																 <Value>
																   <uax:UInt16>'.$t.'</uax:UInt16>
																 </Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address" BrowseName="Address">
																<DisplayName>Address</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_ADDRESS_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex" BrowseName="OperandTypeIndex">
																<DisplayName>Operand Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPRAND_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex" BrowseName="OperationTypeIndex">
																<DisplayName>Operation Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPERATION_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <!--End Tag'.$t.'-->';
																
															}

														
															$xml .= '</References>
														  </UAObject>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeId" BrowseName="1:OperationTypeId" >
															<DisplayName> Operation Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$operationnumber.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup" BrowseName="1:OperationGroupId">
															<DisplayName> Group Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$i.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber" BrowseName="1:DataPointNumber">
															<DisplayName> Data Point Number</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>[U:TAG_QUANTITY_VAL]</uax:UInt16>
															</Value>
														  </UAVariable>
														<!-- End Operation'.$operationnumber.'.Group'.$i.'-->';
													
														}
								
								
													
												  $xml .= '</References>
												</UAObject>
												<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Notifications" BrowseName="1:Notifications">
												  <DisplayName>Notifications</DisplayName>
												  <Description Locale="en-US"></Description>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=1029</Reference>
													<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>';
								
								
													$operationnumber = 4;
													$operationname = 'Notifications';
								
													for ($i=1; $i<=4; $i++) {
								
													$xml .= '<!-- ************************* OperationTemplate Rev F.xml ************************* -->
														<!--Operation'.$operationnumber.'.Group'.$i.'-->
														  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" BrowseName="1:'.$operationname.''.$i.'">
															<DisplayName>'.$operationname.''.$i.'</DisplayName>
															<Description Locale="en-US"></Description>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1015</Reference>
															  <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.'.$operationname.'s</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationType</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber</Reference>';    
															  
														
															for ($t=1; $t<=16; $t++) {
														
																$xml .= '<!-- ************************* Tag'.$t.' ************************* -->
															  <UAVariable DataType="[U:TAG_DATATYPE_NUM]" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" BrowseName="1:[U:TAG_BROWSE_NAME]">
																<DisplayName>[U:TAG_DISPLAY_NAME]</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">ns=1;i=2016</Reference>
																  <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex</Reference>
																  [U:REF_TO_OTHER_TAG]
																</References>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id" BrowseName="Id">
																<DisplayName> Id</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																 <Value>
																   <uax:UInt16>'.$t.'</uax:UInt16>
																 </Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address" BrowseName="Address">
																<DisplayName>Address</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_ADDRESS_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex" BrowseName="OperandTypeIndex">
																<DisplayName>Operand Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPRAND_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex" BrowseName="OperationTypeIndex">
																<DisplayName>Operation Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPERATION_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <!--End Tag'.$t.'-->';
																
															}

														
															$xml .= '</References>
														  </UAObject>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeId" BrowseName="1:OperationTypeId" >
															<DisplayName> Operation Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$operationnumber.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup" BrowseName="1:OperationGroupId">
															<DisplayName> Group Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$i.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber" BrowseName="1:DataPointNumber">
															<DisplayName> Data Point Number</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>[U:TAG_QUANTITY_VAL]</uax:UInt16>
															</Value>
														  </UAVariable>
														<!-- End Operation'.$operationnumber.'.Group'.$i.'-->';
													
														}
								
													
												   $xml .= '</References>
												</UAObject>
												<UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Functions" BrowseName="1:Functions">
												  <DisplayName>Functions</DisplayName>
												  <Description Locale="en-US"></Description>
												  <References>
													<Reference ReferenceType="HasTypeDefinition">ns=1;i=1030</Reference>
													<Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'</Reference>';
								
								
													$operationnumber = 5;
													$operationname = 'Functions';
								
													for ($i=1; $i<=4; $i++) {
								
													$xml .= '<!-- ************************* OperationTemplate Rev F.xml ************************* -->
														<!--Operation'.$operationnumber.'.Group'.$i.'-->
														  <UAObject NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" BrowseName="1:'.$operationname.''.$i.'">
															<DisplayName>'.$operationname.''.$i.'</DisplayName>
															<Description Locale="en-US"></Description>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">ns=1;i=1015</Reference>
															  <Reference ReferenceType="Organizes" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.'.$operationname.'s</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationType</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup</Reference>
															  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber</Reference>';    
															  
														
															for ($t=1; $t<=16; $t++) {
														
																$xml .= '<!-- ************************* Tag'.$t.' ************************* -->
															  <UAVariable DataType="[U:TAG_DATATYPE_NUM]" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" BrowseName="1:[U:TAG_BROWSE_NAME]">
																<DisplayName>[U:TAG_DISPLAY_NAME]</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">ns=1;i=2016</Reference>
																  <Reference ReferenceType="HasComponent" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex</Reference>
																  <Reference ReferenceType="HasProperty">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex</Reference>
																  [U:REF_TO_OTHER_TAG]
																</References>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Id" BrowseName="Id">
																<DisplayName> Id</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																 <Value>
																   <uax:UInt16>'.$t.'</uax:UInt16>
																 </Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.Address" BrowseName="Address">
																<DisplayName>Address</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_ADDRESS_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperandTypeIndex" BrowseName="OperandTypeIndex">
																<DisplayName>Operand Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPRAND_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'.OperationTypeIndex" BrowseName="OperationTypeIndex">
																<DisplayName>Operation Index</DisplayName>
																<References>
																  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
																  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.Tag'.$t.'</Reference>
																</References>
																<Value>
																  <uax:UInt16>[U:TAG_OPERATION_INDEX_VAL]</uax:UInt16>
																</Value>
															  </UAVariable>
															  <!--End Tag'.$t.'-->';
																
															}

														
															$xml .= '</References>
														  </UAObject>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeId" BrowseName="1:OperationTypeId" >
															<DisplayName> Operation Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$operationnumber.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.OperationTypeGroup" BrowseName="1:OperationGroupId">
															<DisplayName> Group Id</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>'.$i.'</uax:UInt16>
															</Value>
														  </UAVariable>
														  <UAVariable DataType="UInt16" ParentNodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'" NodeId="ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'.DataPointNumber" BrowseName="1:DataPointNumber">
															<DisplayName> Data Point Number</DisplayName>
															<References>
															  <Reference ReferenceType="HasTypeDefinition">i=68</Reference>
															  <Reference ReferenceType="HasProperty" IsForward="false">ns=1;s=Site1.Controller'.$controllernum.'.Object'.$objectnum.'.Device'.$devicenum.'.Operation'.$operationnumber.'.Group'.$i.'</Reference>
															</References>
															<Value>
															  <uax:UInt16>[U:TAG_QUANTITY_VAL]</uax:UInt16>
															</Value>
														  </UAVariable>
														<!-- End Operation'.$operationnumber.'.Group'.$i.'-->';
													
														}
													
												   $xml .= '</References>
												</UAObject>
												<!-- END Device'.$devicenum.'-->';
									
									
									
								$devicenum++;
							
							}
				
				
			
				$objectnum++;
			
			}
			
			
			$controllernum++;
			

		}

  $xml .= '</UANodeSet>';

echo $xml;
?>
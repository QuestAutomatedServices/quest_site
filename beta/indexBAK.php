<?php 
session_start();

if ($_SERVER['REMOTE_ADDR'] != '68.36.73.102') {
	header('location: noaccess.php');
	exit();		
}

include("app/functions.php"); ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="quest.ico">

    <title>Quest Configuration Portal</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="bootstrap/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="bootstrap/css/dashboard.css" rel="stylesheet">
     
     
    <link rel="stylesheet" type="text/css" href="jquery/jquery-ui.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.structure.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery-ui.theme.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.dataTables.css"/>
	<link rel="stylesheet" type="text/css" href="jquery/jquery.timepicker.css"/>
	<script src="https://use.fontawesome.com/3c5251b351.js"></script>
     
      <link id="themecss" rel="stylesheet" type="text/css" href="app/all.min.css" />
      
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="bootstrap/assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Quest Configuration Portal</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="#">Dashboard</a></li>

          </ul>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-list">
           
         	<?php echo getAccountSelect(''); ?>

           <li class="nav-header">Tools</li>
           
			  <li><a href="index.php">Sites</a></li>
			  <li><a href="index.php?f=1">File Manager</a></li>
			  <li><a href="index.php?m=1">Map</a></li>
			  <li><a href="index.php">Users</a></li>
			  <li><a href="index.php">Objects</a></li>
			  <li><a href="index.php">Devices</a></li>

          

          </ul>
        </div>
        
         <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          
          				<?php if (!isset($_GET['s']) && !isset($_GET['f']) && !isset($_GET['m'])) { ?>
          
          						<h1 class="page-header">Sites</h1>
          
								<table class="table table-striped">
								  <thead>
									<tr>
									  <th>Site Name</th>
									  <th>Site Status</th>
									  <th>XML</th>
									</tr>
								  </thead>
								  <tbody>

								   <?php 

										$sites = getAccountSites($_SESSION['accountID']); 

										foreach ($sites as $site) {

											$class = '';

											if ($site['status'] == 'Problem') {
												$class = 'class="danger"';
											}

											if ($site['status'] == 'Offline') {
												$class = 'class="info"';
											}

											if ($site['status'] == 'Error') {
												$class = 'class="warning"';
											}

											echo '<tr '.$class.'>
												  <td><a href="index.php?s='.$site['id'].'">'.$site['name'].'</a></td>
												  <td>'.$site['status'].'</td>
												  <td>link</td>
												</tr>';
										}

								   ?>
								  </tbody>
								</table>
                            
                            
                            <?php } ?>
                            
                            
                            
                            <?php if (isset($_GET['s'])) { ?>
          
          						<h1 class="page-header">Visual Configurator</h1>
          						
          							       	<div class="btn-group">
												<a href="#" class="btn btn-primary" id="add_controller">Add Controller</a>
												<a href="#" class="btn btn-primary" id="add_object">Add Object</a>
												<a href="#" class="btn btn-primary" id="add_device">Add Device</a>
											</div>
          		
          						
          						
          						<div class="theme-light">
									<ul id="treeview" >
										
										<li data-icon-cls="treeview-icon icon-folder" data-expanded="false"> <?php echo getSiteName($_GET['s']); ?> 
																
											<ul>
												<li><input type="text" class="form-control" name="gauge_start_good" placeholder="IP Address" ></li>
												
													
												<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Controller 1
													
													<ul>
														<li><input type="text" class="form-control" name="gauge_start_good" placeholder="IP Address" ></li>
													
														<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Objects
														
															<ul>
															
																<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Tank
																
																	<ul>
																		<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Devices
																		
																			<ul>
																			
																				<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Tank Stick
																				
																					<ul>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Device ID" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Device Address" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Operation Type" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Operation Type Index" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Tag Quantity" ></li>
																						
																						<li>Measurement</li>
																						<li>Execution</li>
																						<li>Trigger</li>
																						<li>Notifications</li>
																						
																					</ul>
																				
																				</li>
																				
																				
																			</ul>
																		
																		</li>
																	</ul>
																
																</li>
															
															</ul>
														
														</li>
													</ul>
												
												</li>
												
												
												<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Controller 2
													
													<ul>
														<li><input type="text" class="form-control" name="gauge_start_good" placeholder="IP Address" ></li>
													
														<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Objects
														
															<ul>
															
																<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Tank
																
																	<ul>
																		<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Devices
																		
																			<ul>
																			
																				<li data-icon-cls="treeview-icon icon-folder" data-expanded="false">Tank Stick
																				
																					<ul>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Device ID" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Device Address" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Operation Type" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Operation Type Index" ></li>
																						
																						<li><input type="text" class="form-control" name="gauge_start_good" placeholder="Tag Quantity" ></li>
																						
																						<li>Measurement</li>
																						<li>Execution</li>
																						<li>Trigger</li>
																						<li>Notifications</li>
																						
																					</ul>
																				
																				</li>
																				
																				
																			</ul>
																		
																		</li>
																	</ul>
																
																</li>
															
															</ul>
														
														</li>
													</ul>
												
												</li>
												
												
											
												
												
												
											</ul>
											
										</li>

									</ul>
								</div>
								
								<div id="add_con_modal" title="Add Controller" style="display: none;padding:20px;">
									
									<h4>Add a Controller to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form>
  										<div class="form-group">
											<input type="text" class="form-control" name="gauge_start_good" placeholder="IP Address" >
									     </div>
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>



								<div id="add_obj_modal" title="Add Object" style="display: none;padding:20px;">
									
									<h4>Add an Object to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form>
  										<div class="form-group">
												
											<select class="form-control">
											<option value="">--Choose Controller--</option>
							     			<option value="171">Controller 1 - IP:12.45.67.889</option>
							     			<option value="172">Controller 2 - IP:77.15.43.106</option>
							     			</select>
							     			
									     </div>
									     
									     <div class="form-group">
							     			
							     			<select class="form-control">
											<option value="">--Choose Object Type--</option>
							     			<option value="171">Tank</option>
							     			<option value="172">Water Well</option>
							     			</select>
								     
									     </div>
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
	
         					
         					
         						<div id="add_dev_modal" title="Add Device" style="display: none;padding:20px;">
									
									<h4>Add a Device to <?php echo getSiteName($_GET['s']); ?> </h4>
									
									<form>
  										<div class="form-group">
												
											<select class="form-control">
											<option value="">--Choose Object--</option>
							     			<option value="171">Tank</option>
							     			</select>
							     			
									     </div>
									     
									     <div class="form-group">
							     			
							     			<select class="form-control">
											<option value="">--Choose Device--</option>
							     			<option value="171">Tank Stick</option>
							     			</select>
								     
									     </div>
									     <div class="form-group">
											<button type="submit" class="btn btn-default">Submit</button>
										</div>
									</form>
									
								</div>
         					
          					<?php } ?>
          					
          					
          					
          					<?php if (isset($_GET['f'])) { ?>
          
          						<h1 class="page-header">File Manager</h1>
          						
          						<div id="elfinder"></div>
							
							<?php } ?>
							
							

							<?php  if ($_GET['m'] == 1) { ?>  

								<h2 class="sub-header">Site Map</h2>

								<div id="map"></div>

							<?php  } ?>  
       
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="jquery/external/jquery/jquery.js"></script>
		
    <script src="bootstrap/docs/dist/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="jquery/jquery-ui.js"></script>
	<script type="text/javascript" src="jquery/jquery.dataTables.js"></script>
	<script type="text/javascript" src="jquery/jquery.maskedinput.js"></script>
	<script type="text/javascript" src="jquery/jquery.validate.js"></script>
	<script type="text/javascript" src="jquery/jquery.timepicker.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
   
    <!-- Just to make our placeholder images work. Don't actually copy the next line! -->
    <script src="bootstrap/docs/assets/js/vendor/holder.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="bootstrap/docs/assets/js/ie10-viewport-bug-workaround.js"></script>
  
		 <?php if (isset($_GET['s'])) { ?>

			<script type="text/javascript" src="//www.shieldui.com/shared/components/latest/js/shieldui-all.min.js"></script>

		<?php  } ?> 
    
    
        <?php  if ($_GET['f']) { ?>  
        
			<!-- Require JS (REQUIRED) -->
			<!-- Rename "main.default.js" to "main.js" and edit it if you need configure elFInder options or any things -->
			<script data-main="elFinder/main.default.js" src="//cdnjs.cloudflare.com/ajax/libs/require.js/2.3.2/require.min.js"></script>

    	<?php  } ?> 
    	
        <?php  if ($_GET['m']) { ?>  
        
        		<script src="http://maps.google.com/maps/api/js"></script>

				<?php include('app/google_maps.php'); ?>
                
        <?php  } ?>  
    
    <?php include("app/common_scripts.php"); ?>
    
  </body>
</html>

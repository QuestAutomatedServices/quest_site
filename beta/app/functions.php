<?php 

function connect() {

	$host_name = "questmysqlinstance.cwspgv68diat.us-east-1.rds.amazonaws.com:3306";
	$database = "quest_config"; // Change your database name
	$username = "quest_master";          // Your database user id 
	$password = "HoleN123";            // Your password
	
	//////// Do not Edit below /////////
	try {
		$db = new PDO('mysql:host='.$host_name.';dbname='.$database, $username, $password);
		//echo "connected";
	} catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}

	return $db;
}

function updateRecord($POST,$table,$key)  {
	
	$db = connect(); 
	
	foreach ($POST as $k=>$value) {
	
		if ($k != 'submit' && $k != 'id' && $k != 'pt' && $k != 'cid'   ) {

			$fields = $fields .  "`".$k . "` = '".addslashes($value). "' ,";	

		}
	}
		
	$fields = rtrim($fields,",");
	
	
	$stmt = $db->prepare("UPDATE $table SET $fields WHERE $key = ".$POST['id']." ");
	
	$stmt->execute();

}


function getAccountSites($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE account_id=:id and trash_bin = 0 ORDER BY name ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}




function getAccountSelect($current)  {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM accounts ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	
	$html = '<select class="form-control" id="acctselect" name="id" style="margin-bottom:20px;">';

	$html .= '<option value="">--Choose Account--</option>';

	
		foreach ($results as $key=>$result) {

				if ($current == $result['id']) {
					$html .= '<option value="'.$result['id'].'" selected="selected">'.$result['name'].'</option>';
				} else {
					$html .= '<option value="'.$result['id'].'">'.$result['name'].'</option>';
				}
		}

	$html .= '</select>';


	
	return $html;
}

function getAccountName($id){
	
	$db = connect();
	
	$stmt=$db->query("SELECT * FROM accounts WHERE id = $id");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results[0]['name'];
}


function getSiteName($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['name'];
}

function getAllSiteNames() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE trash_bin = 1 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getSiteIP($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['ip_address'];
}

function getControllerIP($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['ip_address'];
}



function getSiteAddressType($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['address_type'];
}


function getSiteControllers($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE site_id=:id and trash_bin = 0 ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function getSiteId($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT site_id FROM site_controllers WHERE id=:id and trash_bin = 0 ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function getAllControllers() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE trash_bin = 1 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function getAllSiteControllers() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT sc.name,sc.id as controller_id,sc.site_id as site_id, s.name as site_name,sc.ip_address,sc.address_type,sc.baud_rate,sc.parity,sc.stop_bits,sc.flow_control FROM site_controllers sc LEFT JOIN sites s on s.id = sc.site_id WHERE sc.trash_bin = 0 ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getSiteControllerCount($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE site_id=:id AND trash_bin = 0");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();

	return $rows;
}


function getSiteGeo($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['latitude'].','.$results[0]['longitude'];
}



function insertSiteController($post) {
	
	$db = connect();
	
	if (isset($_POST['OPC'])) {
		$opc = 1;
	} else {
		$opc = 0;
	}
	
	if ($_POST['schemaPrefix'] != '') {
		$prefix = $_POST['schemaPrefix'];
	} else {
		$prefix = 'N/A';
	}
	
	if ($_POST['schemaSuffix'] != '') {
		$suffix = $_POST['schemaSuffix'];
	} else {
		$suffix = 'N/A';
	}
	

	$stmt = $db->prepare("INSERT INTO site_controllers (`name`,`site_id`,`created`, `ip_address`, `OPC_UA_enable`,`address_type`,`prefix_schema`,`suffix_schema`) VALUES(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['site_id'], ':field3' => date("Y-m-d H:i:s", time()), ':field4' => $post['ip_address'], ':field5' => $opc , ':field6' => $post['address_type'], ':field7' => $prefix, ':field8' => $suffix ) );


	
}



function updateSite($post)  {
	
	$db = connect();
	
	$stmt = $db->prepare("UPDATE sites SET ip_address = '".$post['ip_address']."',address_type = '".$post['address_type']."',  latitude = '".$post['latitude']."', longitude = '".$post['longitude']."',  name = '".$post['name']."' WHERE id = ".$post['id']."  ");
	
	$stmt->execute();


}

function updateController($post)  {
	
	$db = connect();
	
	$stmt = $db->prepare("UPDATE site_controllers SET ip_address = '".$post['ip_address']."', name = '".$post['name']."' WHERE id = ".$post['id']."  ");
	
	$stmt->execute();


}

function updateAttributes($post)  {
	
	$db = connect();
	
	if($_POST['address_type'] == 'Modbus'){
		$stmt = $db->prepare("UPDATE site_controllers SET address_type = '".$post['address_type']."',  baud_rate= '".$post['baudRate']."',  parity= '".$post['parity']."',  stop_bits= '".$post['stopBits']."',  flow_control= '".$post['flowControl']."',  port= NULL WHERE id = ".$post['id']."  ");
	} else {
		$stmt = $db->prepare("UPDATE site_controllers SET address_type = '".$post['address_type']."', baud_rate= NULL,  parity= NULL,  stop_bits= NULL,  flow_control= NULL,  port= '".$post['port']."' WHERE id = ".$post['id']."  ");
	}
	$stmt->execute();

}

function getAttributes($id,$address_type) {
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE id=:id AND address_type=:address_type");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->bindValue(':address_type', $address_type, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0];
		
}



function getLocationsGeo($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE account_id=:id ORDER BY name ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;

}



function getManufacturers() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM manufacturers ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getManufacturerData($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM manufacturers WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}




function getMfrDeviceCount($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM devices WHERE manufacturers_id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();

	return $rows;
}



function getMfrSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM manufacturers ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="manufacturers_id" >';

		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}

function getSitesSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM sites WHERE trash_bin = 0 ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="site_id" >';

		foreach ($results as $site) {
			
			if ($site['id'] == $current) {
				$html .= '<option value="'.$site['id'].'" selected="selected">'.$site['name'].'</option>';
			} else {
				$html .= '<option value="'.$site['id'].'">'.$site['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}


function getDevTypeSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT controller_type FROM devices WHERE id=:current ");
	$stmt->bindValue(':current', $current, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="controller_type" id="controller_type" >';
	
	
			if ($results[0][controller_type] == 'Standard') {
				$html .= '<option value="Standard" selected="selected">Standard</option>';
				$html .= '<option value="Expanded">Expanded</option>';
			} else {
				$html .= '<option value="Standard">Standard</option>';
				$html .= '<option value="Expanded" selected="selected">Expanded</option>';
			}

	$html .= '</select>';


	return $html;	
	
}

function getNumConfigSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT num_config FROM devices WHERE id=:current ");
	$stmt->bindValue(':current', $current, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0][num_config];	
	
}

function getNumConfig($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT num_config FROM devices WHERE id=:current ");
	$stmt->bindValue(':current', $current, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function ControllerType($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT controller_type FROM devices WHERE id=:current ");
	$stmt->bindValue(':current', $current, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertManufacturer($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO manufacturers (`name`,`website`,`active`,`created`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['website'], ':field3' => 1, ':field4' => date("Y-m-d H:i:s", time()) ) );


	
}





function getObjects() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM objects ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertObject($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO objects (`name`,`objectID`,`active`,`created`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['objectID'], ':field3' => 1, ':field4' => date("Y-m-d H:i:s", time()) ) );


}



function getControllerName($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM site_controllers WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['name'];
}



function getControllerObjects($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT *, c.id as contObjID, c.name as objname  FROM controller_objects as c LEFT JOIN objects as o ON c.object_id=o.id WHERE c.controller_id=:id and c.trash_bin = 0 ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function getAllControllerObjects() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * from controller_objects WHERE trash_bin = 1");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getControllerObjectsCount($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT *, c.id as contObjID, c.name as objname  FROM controller_objects as c LEFT JOIN objects as o ON c.object_id=o.id WHERE c.controller_id=:id AND c.trash_bin = 0 ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();

	return $rows;
}



function insertControllerObject($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO controller_objects (`controller_id`, `object_id`, `name`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['controller_id'], ':field2' => $post['object_id'], ':field3' => $post['name'] ) );


	
}


//Function to import excel file with nodeID columns
function insertConfiguredExcel() {
	$db = connect();
	
	
	///SITES
	//Preparing Sites Data 
	$stmt = $db->query("SELECT distinct SiteName,Site,SiteLatitude,SiteLongitude,SiteAddress,SiteAddressType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Sites from Excel
	foreach ($results as $site) {
		$value = $site['SiteName'];
		$latitude = $site['SiteLatitude'];
		$longitude = $site['SiteLongitude'];
		$address = $site['SiteAddress'];
		$address_type = $site['SiteAddressType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $value, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if(!$results) {
			$stmt = $db->prepare("INSERT INTO sites (`account_id`,`name`,`trash_bin`,`latitude`,`longitude`,`ip_address`,`address_type`) VALUES(1,:field1,0,:field2,:field3,:field4,:field5)");
			$stmt->execute(array(':field1' => $value ,':field2' => $latitude ,':field3' => $longitude ,':field4' => $address ,':field5' => $address_type) );
		} else {
			/// ADD CONFIRMATION THAT WILL ADD DATA TO AN EXISTING SITE
			echo '<script language="javascript">';
			echo 'confirm("Are you sure?")';
			echo '</script>';

		}
	}
	

	////CONTROLLERS
	//Preparing Controllers Data 
	$stmt = $db->query("SELECT distinct SiteName,ControllerName,Site,Controller,ControllerAddress,ControllerAddressType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Controllers from Excel
	foreach ($results as $controller) {
		$valueController = $controller['ControllerName'];
		$valueSite = $controller['SiteName'];
		$address = $controller['ControllerAddress'];
		$address_type = $controller['ControllerAddressType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueSite, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$site_id = $results[0]['id'];
		
		$stmt = $db->prepare("INSERT INTO site_controllers (`name`,`site_id`,`trash_bin`,`ip_address`,`address_type`) VALUES(:field1,:field2,0,:field3,:field4)");
		$stmt->execute(array(':field1' => $valueController,':field2' => $site_id,':field3' => $address,':field4' => $address_type ) );
	}
	
	
	////OBJECTS
	//Preparing Objects Data 
	$stmt = $db->query("SELECT distinct SiteName,ControllerName,ObjectName,Site,Controller,Object,ObjectType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Objects from Excel
	foreach ($results as $object) {
		$valueObject = $object['ObjectName'];
		$valueController = $object['ControllerName'];
		$valueSite = $object['SiteName'];
		$objectTemplate = $object['ObjectType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueSite, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$site_id = $results[0]['id'];
		
		
		$stmt = $db->prepare("SELECT id FROM site_controllers WHERE name =:id AND site_id =:id2 AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueController, PDO::PARAM_INT);
		$stmt->bindValue(':id2', $site_id, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$controller_id = $results[0]['id'];
		
		//Checking Object Template name on objects table
		$stmt = $db->prepare("SELECT id FROM objects WHERE name =:id");
		$stmt->bindValue(':id', $objectTemplate, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		//IF name exists then create instance of that object template
		//IF NOT exists then create template and then instance of that new template
		if($results) {
		
			$object_id = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO controller_objects (`name`,`controller_id`,`object_id`,`trash_bin`) VALUES(:field1,:field2,:field3,0)");
			$stmt->execute(array(':field1' => $valueObject,':field2' => $controller_id,':field3' => $object_id ) );
		} else {
			
			$stmt = $db->prepare("INSERT INTO objects (`name`,`active`,`created`) VALUES(:field1,1,:field2)");
			$stmt->execute(array(':field1' => $objectTemplate,':field2' => date("Y-m-d H:i:s", time())  ) );
			
			$stmt = $db->prepare("SELECT id FROM objects WHERE name =:id");
			$stmt->bindValue(':id', $objectTemplate, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$object_id = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO controller_objects (`name`,`controller_id`,`object_id`,`trash_bin`) VALUES(:field1,:field2,:field3,0)");
			$stmt->execute(array(':field1' => $valueObject,':field2' => $controller_id,':field3' => $object_id ) );
		}
		
		///OBJECT PARAMETERS 
		$stmt = $db->prepare("SELECT distinct ObjectParameter,ObjectParameterName, ObjectParameterValue FROM import_data WHERE ObjectName=:id");
		$stmt->bindValue(':id', $valueObject, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($results as $parameter) {
			$parameterName = $parameter['ObjectParameterName'];
			$parameterValue = $parameter['ObjectParameterValue'];
			$parameterNumber = $parameter['ObjectParameter'];
					
			$parameterName = explode(";",$parameterName);
			$parameterValue = explode(";",$parameterValue);
			
			for($i=0; $i <$parameterNumber ; $i++){
				//Checking Parameter name on object_parameters table
				$stmt = $db->prepare("SELECT id FROM object_parameters WHERE name =:id AND object_id =:id2");
				$stmt->bindValue(':id', $parameterName[$i], PDO::PARAM_INT);
				$stmt->bindValue(':id2', $object_id, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				if(!$results) {
					$stmt = $db->prepare("INSERT INTO object_parameters (`name`,`value`,`object_id`) VALUES(:field1,:field2,:field3)");
					$stmt->execute(array(':field1' => $parameterName[$i],':field2' => $parameterValue[$i],':field3' => $object_id ) );
				}
			}
		}
		
	}
	
	
	////DEVICES
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct SiteName,ControllerName,ObjectName,DeviceName,Site,Controller,Object,Device,DeviceManufacturer,DeviceType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Devices from Excel
	foreach ($results as $device) {
		$valueDevice = $device['DeviceName'];
		$valueObject = $device['ObjectName'];
		$valueController = $device['ControllerName'];
		$valueSite = $device['SiteName'];
		$deviceManufacturer = $device['DeviceManufacturer'];
		$deviceType = $device['DeviceType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueSite, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$site_id = $results[0]['id'];		
		
		$stmt = $db->prepare("SELECT id FROM site_controllers WHERE name =:id AND site_id =:id2 AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueController, PDO::PARAM_INT);
		$stmt->bindValue(':id2', $site_id, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$controller_id = $results[0]['id'];
		
		$stmt = $db->prepare("SELECT id FROM controller_objects WHERE name =:id AND controller_id =:id2 AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueObject, PDO::PARAM_INT);
		$stmt->bindValue(':id2', $controller_id, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$object_id = $results[0]['id'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id");
		$stmt->bindValue(':id', $valueDevice, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//IF name exists then create instance of that device template
		//IF NOT exists then create template and then instance of that new template
		/* if($results) {
			$device_id = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO object_devices (`site_id`,`controller_id`,`object_id`,`device_id`,`trash_bin`) VALUES(:field1,:field2,:field3,:field4,0)");
			$stmt->execute(array(':field1' => $site_id,':field2' => $controller_id,':field3' => $object_id,':field4' => $device_id ) );
		} else { */
			
			//Checking Manufacturer name on manufacturers table
			$stmt = $db->prepare("SELECT id FROM manufacturers WHERE name =:id");
			$stmt->bindValue(':id', $deviceManufacturer, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			//IF NOT exists then create manufacturer
			if(!$results) {
				$stmt = $db->prepare("INSERT INTO manufacturers (`name`,`active`,`created`) VALUES(:field1,1,:field2)");
				$stmt->execute(array(':field1' => $deviceManufacturer,':field2' => date("Y-m-d H:i:s", time())  ) );
				
				$stmt = $db->prepare("SELECT id FROM manufacturers WHERE name =:id");
				$stmt->bindValue(':id', $deviceManufacturer, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$manufacturer_id = $results[0]['id'];
			} else {
				$manufacturer_id = $results[0]['id'];
			}
			
			//Checking number of configurations
			$stmt = $db->query("SELECT distinct GroupNumber FROM import_data");
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach ($results as $group) {
				$group = $group['GroupNumber'];
				$groupNumber = substr($group,5);
			}
			
			if($groupNumber > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			
			$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
			$stmt->bindValue(':id', $deviceType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$device_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $deviceType  ) );
				
				$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
				$stmt->bindValue(':id', $deviceType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$device_type_id = $results[0]['id'];
			}
			
			$stmt = $db->prepare("INSERT INTO devices (`name`,`active`,`created`,`manufacturers_id`,`controller_type`,`num_config`,`device_type_id`) VALUES(:field1,1,:field2,:field3,:field4,:field5,:field6)");
			$stmt->execute(array(':field1' => $valueDevice,':field3' => $manufacturer_id,':field2' => date("Y-m-d H:i:s", time()),':field4' => $controller_type,':field5' => $groupNumber,':field6' => $device_type_id ) );
			
			$valueDeviceGlobal = $db->insert_id;
			$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
			$stmt->bindValue(':id', $valueDevice, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$device_id_global = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO object_devices (`site_id`,`controller_id`,`object_id`,`device_id`,`trash_bin`) VALUES(:field1,:field2,:field3,:field4,0)");
			$stmt->execute(array(':field1' => $site_id,':field2' => $controller_id,':field3' => $object_id,':field4' => $device_id_global ) );
		/* } */

	}
	
	
	//// MEASUREMENTS 
	//Preparing Measurements TagName Data 
	$stmt = $db->query("SELECT distinct TagName,TagDeviceType,TagDescription FROM import_data WHERE Operation = 'Operation1'");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Measurements from Excel
	foreach ($results as $measurement) {
		$value = $measurement['TagName'];
		$device_type = $measurement['TagDeviceType'];
		$description = $measurement['TagDescription'];
		
		//Checking device_type name on device_types table
		$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
		$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if($results) {
			$device_type_id = $results[0]['id'];
		} else {
			$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
			$stmt->execute(array(':field1' => $device_type  ) );
			
			$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
			$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$device_type_id = $results[0]['id'];
		}
		
		//Checking tag name on table 
		$stmt = $db->prepare("SELECT id FROM measurements WHERE name =:id");
		$stmt->bindValue(':id', $value, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if(!$results) {
			$stmt = $db->prepare("INSERT INTO measurements (`name`,`created`,`description`,`device_type_id`) VALUES(:field1,:field2,:field4,:field3)");
			$stmt->execute(array(':field1' => $value,':field2' => date("Y-m-d H:i:s", time()),':field4' => $description ,':field3' => $device_type_id  ) );
		}
	}
	
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting Measurements Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE Operation = 'Operation1' AND DeviceName =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($results as $measurement) {
			
			$groupNumber = substr($measurement['GroupNumber'],5);
			$tagNumber = substr($measurement['TagNumber'],3);
			$tagName = $measurement['TagName'];
			$dataType = $measurement['DataType'];
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			$stmt = $db->prepare("SELECT id FROM measurements WHERE name =:id");
			$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach ($results as $result) {
				$stmt = $db->prepare("INSERT INTO device_measurements (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
				$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id  ) );
			}
		}
	}
	
	
	///// TRIGGGERS
	//Preparing Triggers TagName Data 
	$stmt = $db->query("SELECT distinct TagName,TagDeviceType,TagDescription FROM import_data WHERE Operation = 'Operation2'");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Triggers from Excel
	foreach ($results as $trigger) {
		$value = $trigger['TagName'];
		$device_type = $trigger['TagDeviceType'];
		$description = $trigger['TagDescription']; 
		
		//Checking device_type name on device_types table
		$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
		$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if($results) {
			$device_type_id = $results[0]['id'];
		} else {
			$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
			$stmt->execute(array(':field1' => $device_type  ) );
			
			$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
			$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$device_type_id = $results[0]['id'];
		}
		
		//Checking tag name on table 
		$stmt = $db->prepare("SELECT id FROM triggers WHERE name =:id");
		$stmt->bindValue(':id', $value, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if(!$results) {
			$stmt = $db->prepare("INSERT INTO triggers (`name`,`created`,`description`,`device_type_id`) VALUES(:field1,:field2,:field4,:field3)");
			$stmt->execute(array(':field1' => $value,':field2' => date("Y-m-d H:i:s", time()),':field4' => $description ,':field3' => $device_type_id  ) );
		}
	}
	
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting Triggers Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE Operation = 'Operation2' AND DeviceName=:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($results as $trigger) {
			
			$groupNumber = substr($trigger['GroupNumber'],5);
			$tagNumber = substr($trigger['TagNumber'],3);
			$tagName = $trigger['TagName'];
			$dataType = $trigger['DataType'];
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			$stmt = $db->prepare("SELECT id FROM triggers WHERE name =:id");
			$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach ($results as $result) {
				$stmt = $db->prepare("INSERT INTO device_triggers (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
				$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id ) );
			}
		}
	} 
	
	//DELETE IMPORT DATA
	$stmt=$db->query("DELETE FROM import_data");
	
	
}

//Function to import excel file without nodeID columns
function insertStandardExcel() {
	$db = connect();
	
	
	///SITES
	//Preparing Sites Data 
	$stmt = $db->query("SELECT distinct SiteName,SiteLatitude,SiteLongitude,SiteAddress,SiteAddressType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Sites from Excel
	foreach ($results as $site) {
		$value = $site['SiteName'];
		$latitude = $site['SiteLatitude'];
		$longitude = $site['SiteLongitude'];
		$address = $site['SiteAddress'];
		$address_type = $site['SiteAddressType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $value, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if(!$results) {
			$stmt = $db->prepare("INSERT INTO sites (`account_id`,`name`,`trash_bin`,`latitude`,`longitude`,`ip_address`,`address_type`) VALUES(1,:field1,0,:field2,:field3,:field4,:field5)");
			$stmt->execute(array(':field1' => $value ,':field2' => $latitude ,':field3' => $longitude ,':field4' => $address ,':field5' => $address_type) );
		} else {
			/// ADD CONFIRMATION THAT WILL ADD DATA TO AN EXISTING SITE
			echo '<script language="javascript">';
			echo 'confirm("Are you sure?")';
			echo '</script>';

		}
	}
	
	
	
	////CONTROLLERS
	//Preparing Controllers Data 
	$stmt = $db->query("SELECT distinct SiteName,ControllerName,ControllerAddress,ControllerAddressType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Controllers from Excel
	foreach ($results as $controller) {
		$valueController = $controller['ControllerName'];
		$valueSite = $controller['SiteName'];
		$address = $controller['ControllerAddress'];
		$address_type = $controller['ControllerAddressType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueSite, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$site_id = $results[0]['id'];
		
		$stmt = $db->prepare("INSERT INTO site_controllers (`name`,`site_id`,`trash_bin`,`ip_address`,`address_type`) VALUES(:field1,:field2,0,:field3,:field4)");
		$stmt->execute(array(':field1' => $valueController,':field2' => $site_id,':field3' => $address,':field4' => $address_type ) );
	}
	
	
	////OBJECTS
	//Preparing Objects Data 
	$stmt = $db->query("SELECT distinct SiteName,ControllerName,ObjectName,ObjectType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Objects from Excel
	foreach ($results as $object) {
		$valueObject = $object['ObjectName'];
		$valueController = $object['ControllerName'];
		$valueSite = $object['SiteName'];
		$objectTemplate = $object['ObjectType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueSite, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$site_id = $results[0]['id'];
		
		
		$stmt = $db->prepare("SELECT id FROM site_controllers WHERE name =:id AND site_id =:id2 AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueController, PDO::PARAM_INT);
		$stmt->bindValue(':id2', $site_id, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$controller_id = $results[0]['id'];
		
		//Checking Object Template name on objects table
		$stmt = $db->prepare("SELECT id FROM objects WHERE name =:id");
		$stmt->bindValue(':id', $objectTemplate, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		//IF name exists then create instance of that object template
		//IF NOT exists then create template and then instance of that new template
		if($results) {
		
			$object_id = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO controller_objects (`name`,`controller_id`,`object_id`,`trash_bin`) VALUES(:field1,:field2,:field3,0)");
			$stmt->execute(array(':field1' => $valueObject,':field2' => $controller_id,':field3' => $object_id ) );
		} else {
			
			$stmt = $db->prepare("INSERT INTO objects (`name`,`active`,`created`) VALUES(:field1,1,:field2)");
			$stmt->execute(array(':field1' => $objectTemplate,':field2' => date("Y-m-d H:i:s", time())  ) );
			
			$stmt = $db->prepare("SELECT id FROM objects WHERE name =:id");
			$stmt->bindValue(':id', $objectTemplate, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$object_id = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO controller_objects (`name`,`controller_id`,`object_id`,`trash_bin`) VALUES(:field1,:field2,:field3,0)");
			$stmt->execute(array(':field1' => $valueObject,':field2' => $controller_id,':field3' => $object_id ) );
		}
		
		///OBJECT PARAMETERS 
		$stmt = $db->prepare("SELECT distinct ObjectParameter,ObjectParameterName, ObjectParameterValue FROM import_data WHERE ObjectName=:id");
		$stmt->bindValue(':id', $valueObject, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($results as $parameter) {
			$parameterName = $parameter['ObjectParameterName'];
			$parameterValue = $parameter['ObjectParameterValue'];
			$parameterNumber = $parameter['ObjectParameter'];
					
			$parameterName = explode(";",$parameterName);
			$parameterValue = explode(";",$parameterValue);
			
			for($i=0; $i <$parameterNumber ; $i++){
				//Checking Parameter name on object_parameters table
				$stmt = $db->prepare("SELECT id FROM object_parameters WHERE name =:id AND object_id =:id2");
				$stmt->bindValue(':id', $parameterName[$i], PDO::PARAM_INT);
				$stmt->bindValue(':id2', $object_id, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				if(!$results) {
					$stmt = $db->prepare("INSERT INTO object_parameters (`name`,`value`,`object_id`) VALUES(:field1,:field2,:field3)");
					$stmt->execute(array(':field1' => $parameterName[$i],':field2' => $parameterValue[$i],':field3' => $object_id ) );
				}
			}
		}
		
	}
	
	
	////DEVICES
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct SiteName,ControllerName,ObjectName,DeviceName,DeviceManufacturer,DeviceType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Devices from Excel
	foreach ($results as $device) {
		$valueDevice = $device['DeviceName'];
		$valueObject = $device['ObjectName'];
		$valueController = $device['ControllerName'];
		$valueSite = $device['SiteName'];
		$deviceManufacturer = $device['DeviceManufacturer'];
		$deviceType = $device['DeviceType'];
		
		$stmt = $db->prepare("SELECT id FROM sites WHERE name =:id AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueSite, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$site_id = $results[0]['id'];		
		
		$stmt = $db->prepare("SELECT id FROM site_controllers WHERE name =:id AND site_id =:id2 AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueController, PDO::PARAM_INT);
		$stmt->bindValue(':id2', $site_id, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$controller_id = $results[0]['id'];
		
		$stmt = $db->prepare("SELECT id FROM controller_objects WHERE name =:id AND controller_id =:id2 AND trash_bin = 0 ");
		$stmt->bindValue(':id', $valueObject, PDO::PARAM_INT);
		$stmt->bindValue(':id2', $controller_id, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$object_id = $results[0]['id'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id");
		$stmt->bindValue(':id', $valueDevice, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

		//IF name exists then create instance of that device template
		//IF NOT exists then create template and then instance of that new template
		/* if($results) {
			$device_id = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO object_devices (`site_id`,`controller_id`,`object_id`,`device_id`,`trash_bin`) VALUES(:field1,:field2,:field3,:field4,0)");
			$stmt->execute(array(':field1' => $site_id,':field2' => $controller_id,':field3' => $object_id,':field4' => $device_id ) );
		} else { */
			
			//Checking Manufacturer name on manufacturers table
			$stmt = $db->prepare("SELECT id FROM manufacturers WHERE name =:id");
			$stmt->bindValue(':id', $deviceManufacturer, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			
			//IF NOT exists then create manufacturer
			if(!$results) {
				$stmt = $db->prepare("INSERT INTO manufacturers (`name`,`active`,`created`) VALUES(:field1,1,:field2)");
				$stmt->execute(array(':field1' => $deviceManufacturer,':field2' => date("Y-m-d H:i:s", time())  ) );
				
				$stmt = $db->prepare("SELECT id FROM manufacturers WHERE name =:id");
				$stmt->bindValue(':id', $deviceManufacturer, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$manufacturer_id = $results[0]['id'];
			} else {
				$manufacturer_id = $results[0]['id'];
			}
			
			//Checking number of configurations
			/* $stmt = $db->query("SELECT distinct GroupNumber FROM import_data");
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach ($results as $group) {
				$group = $group['GroupNumber'];
				$groupNumber = substr($group,5);
			}
			
			if($groupNumber > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			 */
			 
			$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
			$stmt->bindValue(':id', $deviceType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$device_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $deviceType  ) );
				
				$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
				$stmt->bindValue(':id', $deviceType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$device_type_id = $results[0]['id'];
			}
			
			$stmt = $db->prepare("INSERT INTO devices (`name`,`active`,`created`,`manufacturers_id`,`controller_type`,`num_config`,`device_type_id`) VALUES(:field1,1,:field2,:field3,:field4,:field5,:field6)");
			$stmt->execute(array(':field1' => $valueDevice,':field3' => $manufacturer_id,':field2' => date("Y-m-d H:i:s", time()),':field4' => 'Standard',':field5' => 4,':field6' => $device_type_id ) );
			
			$valueDeviceGlobal = $db->insert_id;
			$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
			$stmt->bindValue(':id', $valueDevice, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$device_id_global = $results[0]['id'];
			$stmt = $db->prepare("INSERT INTO object_devices (`site_id`,`controller_id`,`object_id`,`device_id`,`trash_bin`) VALUES(:field1,:field2,:field3,:field4,0)");
			$stmt->execute(array(':field1' => $site_id,':field2' => $controller_id,':field3' => $object_id,':field4' => $device_id_global ) );
		/* } */

	} 
	 
	
	//// OPERATIONS
	//Preparing Operations TagName Data 
	$stmt = $db->query("SELECT distinct TagName,TagDeviceType,TagDescription,OperationType FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Operations from Excel
	foreach ($results as $measurement) {
		$value = $measurement['TagName'];
		$device_type = $measurement['TagDeviceType'];
		$description = $measurement['TagDescription'];
		$operation = $measurement['OperationType'];
		$operation = strtolower($operation);
		$operation = $operation . "s";
		
		
		//Checking device_type name on device_types table
		$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
		$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if($results) {
			$device_type_id = $results[0]['id'];
		} else {
			$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
			$stmt->execute(array(':field1' => $device_type  ) );
			
			$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
			$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$device_type_id = $results[0]['id'];
		}

		//Checking tag name on table 
		$stmt = $db->prepare("SELECT id FROM $operation WHERE name =:id");
		$stmt->bindValue(':id', $value, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if(!$results){
			$stmt = $db->prepare("INSERT INTO ".$operation." (`name`,`created`,`description`,`device_type_id`) VALUES (:field1,:field2,:field4,:field3)");
			$stmt->execute(array(':field1' => $value,':field2' => date("Y-m-d H:i:s", time()),':field4' => $description ,':field3' => $device_type_id  ) );
		}
	}
	
	
	///MEASUREMENTS
	 //Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting Measurements Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE DeviceName =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$tagNumber = 1;
		$groupNumber = 1;
		
		foreach ($results as $measurement) {
			
			$tagName = $measurement['TagName'];
			$dataType = $measurement['DataType'];
			$operation = $measurement['OperationType'];
			$operation = strtolower($operation);
			$operation = $operation . "s";
			
			if($tagNumber == 17) {
				$groupNumber = $groupNumber + 1;
				$tagNumber = 1;
			}
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			if($operation == 'measurements'){
				$stmt = $db->prepare("SELECT id FROM $operation WHERE name =:id");
				$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($results as $result) {
					$stmt = $db->prepare("INSERT INTO device_$operation (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
					$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id  ) );
				}
				$tagNumber = $tagNumber + 1;
			}
			
			$configTotal = $groupNumber;
			
			
			//NUMBER OF CONFIGURATIONS AND CONTROLLER TYPE
			if($configTotal > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			
			$stmt = $db->prepare("UPDATE Devices set controller_type = '".$controller_type."', num_config = '".$configTotal."' WHERE id = '".$device_id."' ");
			$stmt->execute();
			
		}
	}
	
	
	///TRIGGERS
	 //Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting Triggers Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE DeviceName =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$tagNumber = 1;
		$groupNumber = 1;
		
		foreach ($results as $measurement) {
			
			$tagName = $measurement['TagName'];
			$dataType = $measurement['DataType'];
			$operation = $measurement['OperationType'];
			$operation = strtolower($operation);
			$operation = $operation . "s";
			
			if($tagNumber == 17) {
				$groupNumber = $groupNumber + 1;
				$tagNumber = 1;
			}
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			if($operation == 'triggers'){
				$stmt = $db->prepare("SELECT id FROM $operation WHERE name =:id");
				$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($results as $result) {
					$stmt = $db->prepare("INSERT INTO device_$operation (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
					$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id  ) );
				}
				$tagNumber = $tagNumber + 1;
			}
				
			if($groupNumber > $configTotal) {
				$configTotal = $groupNumber;
			}
			
			
			//NUMBER OF CONFIGURATIONS AND CONTROLLER TYPE
			if($configTotal > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			
			$stmt = $db->prepare("UPDATE Devices set controller_type = '".$controller_type."', num_config = '".$configTotal."' WHERE id = '".$device_id."' ");
			$stmt->execute();

		}
	}
	
	//EXECUTIONS
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting Executions Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE DeviceName =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$tagNumber = 1;
		$groupNumber = 1;
		
		foreach ($results as $measurement) {
			
			$tagName = $measurement['TagName'];
			$dataType = $measurement['DataType'];
			$operation = $measurement['OperationType'];
			$operation = strtolower($operation);
			$operation = $operation . "s";
			
			if($tagNumber == 17) {
				$groupNumber = $groupNumber + 1;
				$tagNumber = 1;
			}
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			if($operation == 'executions'){
				$stmt = $db->prepare("SELECT id FROM $operation WHERE name =:id");
				$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($results as $result) {
					$stmt = $db->prepare("INSERT INTO device_$operation (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
					$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id  ) );
				}
				$tagNumber = $tagNumber + 1;
			}
				
			if($groupNumber > $configTotal) {
				$configTotal = $groupNumber;
			}
			
			
			//NUMBER OF CONFIGURATIONS AND CONTROLLER TYPE
			if($configTotal > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			
			$stmt = $db->prepare("UPDATE Devices set controller_type = '".$controller_type."', num_config = '".$configTotal."' WHERE id = '".$device_id."' ");
			$stmt->execute();

		}
	}
	
	
	//NOTIFICATIONS
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting notifications Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE DeviceName =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$tagNumber = 1;
		$groupNumber = 1;
		
		foreach ($results as $measurement) {
			
			$tagName = $measurement['TagName'];
			$dataType = $measurement['DataType'];
			$operation = $measurement['OperationType'];
			$operation = strtolower($operation);
			$operation = $operation . "s";
			
			if($tagNumber == 17) {
				$groupNumber = $groupNumber + 1;
				$tagNumber = 1;
			}
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			if($operation == 'notifications'){
				$stmt = $db->prepare("SELECT id FROM $operation WHERE name =:id");
				$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($results as $result) {
					$stmt = $db->prepare("INSERT INTO device_$operation (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
					$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id  ) );
				}
				$tagNumber = $tagNumber + 1;
			}
				
			if($groupNumber > $configTotal) {
				$configTotal = $groupNumber;
			}
			
			
			//NUMBER OF CONFIGURATIONS AND CONTROLLER TYPE
			if($configTotal > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			
			$stmt = $db->prepare("UPDATE Devices set controller_type = '".$controller_type."', num_config = '".$configTotal."' WHERE id = '".$device_id."' ");
			$stmt->execute();

		}
	}
	
	
	//FUNCTIONS
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id ORDER BY id DESC LIMIT 1");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting functions Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE DeviceName =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$tagNumber = 1;
		$groupNumber = 1;
		
		foreach ($results as $measurement) {
			
			$tagName = $measurement['TagName'];
			$dataType = $measurement['DataType'];
			$operation = $measurement['OperationType'];
			$operation = strtolower($operation);
			$operation = $operation . "s";
			
			if($tagNumber == 17) {
				$groupNumber = $groupNumber + 1;
				$tagNumber = 1;
			}
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			if($operation == 'functions'){
				$stmt = $db->prepare("SELECT id FROM $operation WHERE name =:id");
				$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				foreach ($results as $result) {
					$stmt = $db->prepare("INSERT INTO device_$operation (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
					$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id_global,':field5' => $data_type_id  ) );
				}
				$tagNumber = $tagNumber + 1;
			}
			
			if($groupNumber > $configTotal) {
				$configTotal = $groupNumber;
			}
			

			//NUMBER OF CONFIGURATIONS AND CONTROLLER TYPE
			if($configTotal > 4) {
				$controller_type = 'Expanded';
			} else {
				$controller_type = 'Standard';
			}
			
			$stmt = $db->prepare("UPDATE Devices set controller_type = '".$controller_type."', num_config = '".$configTotal."' WHERE id = '".$device_id."' ");
			$stmt->execute();

		}
	}
	
	
	
	/*
	///// TRIGGGERS
	//Preparing Triggers TagName Data 
	$stmt = $db->query("SELECT distinct TagName,TagDeviceType,TagDescription FROM import_data WHERE Operation = 'Operation2'");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	//Inserting Triggers from Excel
	foreach ($results as $trigger) {
		$value = $trigger['TagName'];
		$device_type = $trigger['TagDeviceType'];
		$description = $trigger['TagDescription']; 
		
		//Checking device_type name on device_types table
		$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
		$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		if($results) {
			$device_type_id = $results[0]['id'];
		} else {
			$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
			$stmt->execute(array(':field1' => $device_type  ) );
			
			$stmt = $db->prepare("SELECT id FROM device_types WHERE name =:id");
			$stmt->bindValue(':id', $device_type, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			$device_type_id = $results[0]['id'];
		}
		
		$stmt = $db->prepare("INSERT INTO triggers (`name`,`created`,`description`,`device_type_id`) VALUES(:field1,:field2,:field4,:field3)");
		$stmt->execute(array(':field1' => $value,':field2' => date("Y-m-d H:i:s", time()),':field4' => $description ,':field3' => $device_type_id  ) );
	}
	
	//Preparing Devices Data 
	$stmt = $db->query("SELECT distinct DeviceName FROM import_data");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $device) {
		$deviceName = $device['DeviceName'];
		
		//Checking Device Template name on devices table
		$stmt = $db->prepare("SELECT id FROM devices WHERE name =:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		$device_id = $results[0]['id'];
		
		//Preparing and Inserting Triggers Tags Data 
		$stmt = $db->prepare("SELECT * FROM import_data WHERE Operation = 'Operation2' AND DeviceName=:id");
		$stmt->bindValue(':id', $deviceName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($results as $trigger) {
			
			$groupNumber = substr($trigger['GroupNumber'],5);
			$tagNumber = substr($trigger['Tag'],3);
			$tagName = $trigger['TagName'];
			$dataType = $trigger['DataType'];
			
			//Checking data type on table
			$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			if($results) {
				$data_type_id = $results[0]['id'];
			} else {
				$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
				$stmt->execute(array(':field1' => $dataType ) );
				
				$stmt = $db->prepare("SELECT id FROM data_types WHERE name =:id ORDER BY id ASC LIMIT 1");
				$stmt->bindValue(':id', $dataType, PDO::PARAM_INT);
				$stmt->execute();
				$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
				
				$data_type_id = $results[0]['id'];
			}
			
			$stmt = $db->prepare("SELECT id FROM triggers WHERE name =:id ORDER BY id ASC LIMIT 1");
			$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
			$stmt->execute();
			$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
			foreach ($results as $result) {
				$stmt = $db->prepare("INSERT INTO device_triggers (`device_id`,`regnum`,`cfn`,`tag_id`,`data_type_id`) VALUES(:field4,:field1,:field2,:field3,:field5)");
				$stmt->execute(array(':field1' => $tagNumber,':field2' => $groupNumber,':field3' => $result['id'],':field4' => $device_id,':field5' => $data_type_id ) );
			}
		}
	}  */
	
	//DELETE IMPORT DATA
	$stmt=$db->query("DELETE FROM import_data");
	
	
}


function getObjectName($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM controller_objects WHERE id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['name'];
}




function getObjectDevices($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT *, o.id as objDevID  FROM object_devices as o LEFT JOIN devices as d ON o.device_id=d.id  WHERE o.object_id=:id and o.trash_bin = 0 ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function getAllObjectDevices() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT d.name, od.id FROM object_devices od LEFT JOIN devices d on d.id = od.device_id WHERE od.trash_bin = 1");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getObjectDevicesCount($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT *, o.id as objDevID  FROM object_devices as o LEFT JOIN devices as d ON o.device_id=d.id  WHERE o.object_id=:id AND o.trash_bin = 0 ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();

	return $rows;
}




function getObjectDeviceName($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT d.name  FROM object_devices as o LEFT JOIN devices as d ON o.device_id=d.id  WHERE o.id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0]['name'];
}


function insertObjectDevice($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO object_devices (`site_id`, `controller_id`, `object_id`, `device_id`, `ip_address`, `address_type`) VALUES(:field1,:field2,:field3,:field4,:field5,:field6)");
	
	$stmt->execute(array(':field1' => $post['site_id'], ':field2' => $post['controller_id'], ':field3' => $post['object_id'], ':field4' => $post['device_id'], ':field5' => $post['ip_address'], ':field6' => $post['address_type'] ) );


	
}



function insertDeviceLink($to,$from) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO devices_links (link_to, link_to_type, link_to_column, link_to_row, link_to_controller, link_to_object, link_from, link_from_type, link_from_column, link_from_row, link_from_controller, link_from_object, siteID) VALUES(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9,:field10,:field11,:field12,:field13)");
	
	$stmt->execute(array(':field1' => $to['uid'], ':field2' => $to['type'], ':field3' => $to['col'],  ':field4' => $to['row'], ':field5' => $to['ctr'],  ':field6' => $to['obj'], ':field7' => $from['uid'], ':field8' => $from['type'], ':field9' => $from['col'],  ':field10' => $from['row'], ':field11' => $from['ctr'], ':field12' => $from['obj'],  ':field13' => $from['site']) );


	
}



function checkLinksTo($uid,$linkTypeArray) {
	
	$linkTypeArray = array(1=>'Measurement',2=>'Execution',3=>'Trigger',4=>'Notification',5=>'Function');
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM devices_links WHERE `link_to`=:uid   ");
	
	$stmt->bindValue(':uid', $uid, PDO::PARAM_INT);
	
	$stmt->execute();
	
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	if ($results) {
		
		$html = '<ul>';
		
			foreach ($results as $link) {
				$html .= '<li>'.$linkTypeArray[$link['link_to_type']].' ('.$link['link_to_column'].'/'.$link['link_to_row'].') <=  <a href="index.php?s='.$link['siteID'].'&ctr='.$link['link_from_controller'].'&obj='.$link['link_from_object'].'&dev='.$link['link_from'].'&type='.$link['link_from_type'].'#'.$link['link_from'].'">'.getControllerName($link['link_from_controller']).' - '.getObjectName($link['link_from_object']).' - '.getObjectDeviceName($link['link_from']).' - '.$linkTypeArray[$link['link_from_type']].' ('.getTagNameLinkage($link['link_to'],$link['link_from_type'],$link['link_from_column'],$link['link_from_row']).')</a> </li>';
			}
		
		
		$html .= '</ul>';
		
		return $html;
		
		
	} else {
		return 'None';
	}
	
	
}


function checkLinksFrom($uid,$linkTypeArray ) {
	
	$linkTypeArray = array(1=>'Measurement',2=>'Execution',3=>'Trigger',4=>'Notification',5=>'Function');
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM devices_links WHERE `link_from`=:uid   ");
	
	$stmt->bindValue(':uid', $uid, PDO::PARAM_INT);
	
	$stmt->execute();
	
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	if ($results) {
		
		$html = '<ul>';
		
			foreach ($results as $link) {
				$html .= '<li>'.$linkTypeArray[$link['link_from_type']].' ('.$link['link_from_column'].'/'.$link['link_from_row'].') =>  <a href="index.php?s='.$link['siteID'].'&ctr='.$link['link_to_controller'].'&obj='.$link['link_to_object'].'&dev='.$link['link_to'].'&type='.$link['link_to_type'].'#'.$link['link_to'].'">'.getControllerName($link['link_to_controller']).' - '.getObjectName($link['link_to_object']).' - '.getObjectDeviceName($link['link_to']).' - '.$linkTypeArray[$link['link_to_type']].' ('.getTagNameLinkage($link['link_to'],$link['link_to_type'],$link['link_to_column'],$link['link_to_row']).')</a> </li>';
			}
		
		
		$html .= '</ul>';
		
		return $html;
		
		
	} else {
		return 'None';
	}
	
	
}


function checkLinkage($type,$uid) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT * FROM devices_links WHERE `link_to`=:uid AND `link_to_type`=:type OR `link_from`=:uid AND `link_from_type`=:type  ");
	
	$stmt->bindValue(':type', $type, PDO::PARAM_INT);
	$stmt->bindValue(':uid', $uid, PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();

	if ($rows > 0) {
		return 'fa-link text-primary';
	} else {
		return 'fa-chain-broken';
	}
	

}

function getTagNameLinkage($current,$operation,$column,$row) {
	$db = connect();
	if($operation == 1) {
		$stmt = $db->prepare("SELECT tag_id from device_measurements WHERE device_id in(SELECT device_id from object_devices where id =:current) And regnum =:column and cfn =:row");
	} else if ($operation == 2) {
		$stmt = $db->prepare("SELECT tag_id from device_executions WHERE device_id in(SELECT device_id from object_devices where id =:current) And regnum =:column and cfn =:row");
	} else if ($operation == 3) {
		$stmt = $db->prepare("SELECT tag_id from device_triggers WHERE device_id in(SELECT device_id from object_devices where id =:current) And regnum =:column and cfn =:row");
	} else if ($operation == 4) {
		$stmt = $db->prepare("SELECT tag_id from device_notifications WHERE device_id in(SELECT device_id from object_devices where id =:current) And regnum =:column and cfn =:row");
	} else {
		$stmt = $db->prepare("SELECT tag_id from device_functions WHERE device_id in(SELECT device_id from object_devices where id =:current) And regnum =:column and cfn =:row");
	}
	
	$stmt->bindValue(':current', $current, PDO::PARAM_INT);
	$stmt->bindValue(':column', $column, PDO::PARAM_INT);
	$stmt->bindValue(':row', $row, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	foreach ($results as $tag) {
		$tagName = $tag['tag_id'];
		if($operation == 1) {
			$stmt = $db->prepare("SELECT name from measurements where id =:id");
		} else if ($operation == 2) { 
			$stmt = $db->prepare("SELECT name from executions where id =:id");
		} else if ($operation == 3) { 
			$stmt = $db->prepare("SELECT name from triggers where id =:id");
		} else if ($operation == 4) { 
			$stmt = $db->prepare("SELECT name from notifications where id =:id");
		} else {
			$stmt = $db->prepare("SELECT name from functions where id =:id");
		}
		
		$stmt->bindValue(':id', $tagName, PDO::PARAM_INT);
		$stmt->execute();
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		return $results[0]['name'];
	}
}

function getParameters($current) {
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM object_parameters WHERE object_id=:current ORDER BY name ");
	$stmt->bindValue(':current', $current, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function updateParameter($post)  {
	
	$db = connect();
	
	$stmt = $db->prepare("UPDATE object_parameters SET name = '".$post['name']."',  description = '".$post['description']."', value = '".$post['value']."' WHERE id = ".$post['id']."  ");
	
	$stmt->execute();


}

function insertParameter($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO object_parameters (`name`, `description`, `value`, `object_id`) VALUES(:field1,:field2,:field3,:field4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['description'], ':field3' => $post['value'], ':field4' => $post['object_id'] ) );


	
}


function getDevices() {
	
	$db = connect();
	
	if ($_GET['mfr']) {
		
		$stmt = $db->prepare("SELECT d.id, d.name, d.deviceID, d.manufacturers_id, d.device_type_id, m.name as mfrname, t.name as devtype FROM devices as d LEFT JOIN manufacturers as m ON d.manufacturers_id=m.id LEFT JOIN device_types as t ON d.device_type_id=t.id  WHERE d.manufacturers_id=:id ORDER BY d.name ");
		$stmt->bindValue(':id', $_GET['mfr'], PDO::PARAM_INT);
		
	} else{
		
		$stmt = $db->prepare("SELECT d.id, d.name, d.deviceID, d.manufacturers_id, d.device_type_id, m.name as mfrname, t.name as devtype FROM devices as d LEFT JOIN manufacturers as m ON d.manufacturers_id=m.id LEFT JOIN device_types as t ON d.device_type_id=t.id ORDER BY d.name ");
	
	}

	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function getDeviceData($id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT d.id, d.name, d.deviceID,  d.device_type_id, m.name as mfrname, m.id as mfrid, t.name as devtype FROM devices as d LEFT JOIN manufacturers as m ON d.manufacturers_id=m.id LEFT JOIN device_types as t ON d.device_type_id=t.id WHERE d.id=:id ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function getDeviceSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM devices ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" id="device_select" name="device_id" >';
	$html .= '<option value="">Choose Device</option>';
	
		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}


function getDeviceOpGroups($id) {
	
	$oparray = '';
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_measurements WHERE device_id=:id GROUP BY cfn ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$oparray['device_measurements_groups'] = $rows;

	$stmt = $db->prepare("SELECT * FROM device_executions WHERE device_id=:id GROUP BY cfn ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$oparray['device_executions_groups'] = $rows;
	
	$stmt = $db->prepare("SELECT * FROM device_triggers WHERE device_id=:id GROUP BY cfn ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$oparray['device_triggers_groups'] = $rows;

	$stmt = $db->prepare("SELECT * FROM device_notifications WHERE device_id=:id GROUP BY cfn ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$oparray['device_notifications_groups'] = $rows;
	
	$stmt = $db->prepare("SELECT * FROM device_functions WHERE device_id=:id GROUP BY cfn ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	$oparray['device_functions_groups'] = $rows;
	return $oparray;
	
}



function getDeviceOpGroupTags($id,$cfn) {
	
	$oparray = '';
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_measurements as d, measurements as m WHERE d.device_id=:id AND d.tag_id=m.id AND d.cfn=:cfn  ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cfn', $cfn, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$oparray['device_measurements_group_tags'] = $results;

	$stmt = $db->prepare("SELECT * FROM device_executions as d, executions as e WHERE d.device_id=:id AND d.tag_id=e.id AND d.cfn=:cfn  ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cfn', $cfn, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$oparray['device_executions_group_tags'] = $results;

	$stmt = $db->prepare("SELECT * FROM device_triggers as d, triggers as t WHERE d.device_id=:id AND d.tag_id=t.id AND d.cfn=:cfn  ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cfn', $cfn, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$oparray['device_triggers_group_tags'] = $results;

	$stmt = $db->prepare("SELECT * FROM device_notifications as d, notifications as n WHERE d.device_id=:id AND d.tag_id=n.id AND d.cfn=:cfn  ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cfn', $cfn, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$oparray['device_notifications_group_tags'] = $results;

	$stmt = $db->prepare("SELECT * FROM device_functions as d, functions as f WHERE d.device_id=:id AND d.tag_id=f.id AND d.cfn=:cfn  ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cfn', $cfn, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$oparray['device_functions_group_tags'] = $results;

	return $oparray;
	
}


function getDeviceTypeSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_types ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="device_type_id" >';

		foreach ($results as $mfr) {
			
			if ($mfr['id'] == $current) {
				$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}



function getDeviceTypes() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM device_types ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function insertDeviceType($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO device_types (`name`) VALUES(:field1)");
	
	$stmt->execute(array(':field1' => $post['name'] ) );


	
}



function insertDevice($post) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT MAX(deviceID) + 1 as counter from devices");
	
	$stmt->execute();
	
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach ($results as $m) {
		$counter = $m['counter'];
	}

	$stmt = $db->prepare("INSERT INTO devices (`name`,`deviceID`, `manufacturers_id`, `device_type_id`, `active`,`created`,`controller_type`,`num_config`) VALUES(:field1,:field2,:field3,:field4,:field5,:field6,'Standard',4)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $counter,  ':field3' => $post['manufacturers_id'],  ':field4' => $post['device_type_id'], ':field5' => 1, ':field6' => date("Y-m-d H:i:s", time()) ) );

	return $lastId = $db->lastInsertId();

	
}



function insertDeviceMeasurement($vars) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT regnum FROM device_measurements WHERE `device_id`=:did AND `cfn`=:cid ORDER BY regnum DESC LIMIT 1");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$rows = $rows[0]['regnum'];

	$stmt = $db->prepare("INSERT INTO device_measurements (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_units`, `parameter_address`, `operator_id`, `data_type_id`, `structure_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_units'], ':field6' =>  $vars['parameter_address'], ':field7' =>  $vars['operator_id'], ':field8' =>  $vars['data_type_id'], ':field9' =>  $vars['structure_id'] ) );

	//return $lastId = $db->lastInsertId();

	
}




function removeDeviceMeasurement($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_measurements WHERE `device_id`=:did AND `measurement_id`=:mid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':mid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function insertDeviceExecution($vars) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT regnum FROM device_executions WHERE `device_id`=:did AND `cfn`=:cid ORDER BY regnum DESC LIMIT 1 ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$rows = $rows[0]['regnum'];
	
	$stmt = $db->prepare("INSERT INTO device_executions (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_units`, `parameter_address`, `operator_id`, `data_type_id`, `structure_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' => ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_units'], ':field6' =>  $vars['parameter_address'], ':field7' =>  $vars['operator_id'], ':field8' =>  $vars['data_type_id'], ':field9' =>  $vars['structure_id'] ) );

	
}




function removeDeviceExecution($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_executions WHERE `device_id`=:did AND `execution_id`=:eid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':eid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function insertDeviceTrigger($vars) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT regnum FROM device_triggers WHERE `device_id`=:did AND `cfn`=:cid ORDER BY regnum DESC LIMIT 1");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$rows = $rows[0]['regnum'];
	
	$stmt = $db->prepare("INSERT INTO device_triggers (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_units`, `parameter_address`, `operator_id`, `data_type_id`, `structure_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_units'], ':field6' =>  $vars['parameter_address'], ':field7' =>  $vars['operator_id'], ':field8' =>  $vars['data_type_id'], ':field9' =>  $vars['structure_id'] ) );

	
}




function removeDeviceTrigger($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_triggers WHERE `device_id`=:did AND `trigger_id`=:tid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':tid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}



function insertDeviceNotification($vars) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT regnum FROM device_notifications WHERE `device_id`=:did AND `cfn`=:cid ORDER BY regnum DESC LIMIT 1 ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$rows = $rows[0]['regnum'];
	
	$stmt = $db->prepare("INSERT INTO device_notifications (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_units`, `parameter_address`, `operator_id`, `data_type_id`, `structure_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_units'], ':field6' =>  $vars['parameter_address'], ':field7' =>  $vars['operator_id'], ':field8' =>  $vars['data_type_id'], ':field9' =>  $vars['structure_id'] ) );

	
}




function removeDeviceNotification($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_notifications WHERE `device_id`=:did AND `notification_id`=:nid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':nid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}




function insertDeviceFunction($vars) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT regnum FROM device_functions WHERE `device_id`=:did AND `cfn`=:cid ORDER BY regnum DESC LIMIT 1 ");

	$stmt->bindValue(':did', $vars['device_id'], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $vars['cfn'], PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	$rows = $rows[0]['regnum'];
	
	$stmt = $db->prepare("INSERT INTO device_functions (`device_id`, `regnum`, `cfn`, `tag_id`, `parameter_units`, `parameter_address`, `operator_id`, `data_type_id`, `structure_id`) 
	VALUES
	(:field1,:field2,:field3,:field4,:field5,:field6,:field7,:field8,:field9)");
	
	$stmt->execute(array(':field1' => $vars['device_id'], ':field2' =>  ($rows+1), ':field3' =>  $vars['cfn'], ':field4' =>  $vars['tag_id'], ':field5' =>  $vars['parameter_units'], ':field6' =>  $vars['parameter_address'], ':field7' =>  $vars['operator_id'], ':field8' =>  $vars['data_type_id'], ':field9' =>  $vars['structure_id'] ) );

	
}




function removeDeviceFunction($vars) {
	
	$db = connect();
	
	$v = explode("_",$vars);

	$stmt = $db->prepare("DELETE FROM device_functions WHERE `device_id`=:did AND `notification_id`=:nid AND `cfn`=:cid ");
	
	$stmt->bindValue(':did', $v[0], PDO::PARAM_INT);
	$stmt->bindValue(':nid', $v[1], PDO::PARAM_INT);
	$stmt->bindValue(':cid', $v[2], PDO::PARAM_INT);
	
	$stmt->execute();

	//return $lastId = $db->lastInsertId();

	
}


function checkSetting($type,$dev,$id,$cfn) {
	
	$db = connect();
	
	if ($type == 1) {
				
		$stmt = $db->prepare("SELECT m.name, o.name as opname, d.configID FROM device_measurements as d LEFT JOIN measurements as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index  WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");

	} else if ($type == 2) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname, d.configID FROM device_executions as d LEFT JOIN executions as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
		
	} else if ($type == 3) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname, d.configID FROM device_triggers as d LEFT JOIN triggers as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
	
	} else if ($type == 4) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname, d.configID FROM device_notifications as d LEFT JOIN notifications as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
	
	} else if ($type == 5) {
		
		$stmt = $db->prepare("SELECT m.name, o.name as opname, d.configID FROM device_functions as d LEFT JOIN functions as m ON d.tag_id=m.id LEFT JOIN operators as o ON d.operator_id=o.operand_type_index WHERE d.`device_id`=:did AND d.`regnum`=:mid AND d.`cfn`=:cid ");
	
	}

	$stmt->bindValue(':did', $dev, PDO::PARAM_INT);
	$stmt->bindValue(':mid', $id, PDO::PARAM_INT);
	$stmt->bindValue(':cid', $cfn, PDO::PARAM_INT);
	
	$stmt->execute();
	$rows = $stmt->rowCount();
	
	if ($rows == 1) {
		$results = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		return $results[0];
	}
	
	
	
}

function checkStructure($id,$type) {
	
	$db = connect();
	
	if($type=='measurement'){
		$stmt = $db->prepare("SELECT dt.name from device_measurements dm, data_types dt WHERE dm.data_type_id = dt.id AND configID=:id");
	} else if ($type == 'trigger') {
		$stmt = $db->prepare("SELECT dt.name from device_triggers dtr, data_types dt WHERE dtr.data_type_id = dt.id AND configID=:id");
	} else if ($type == 'execution') {
		$stmt = $db->prepare("SELECT dt.name from device_executions de, data_types dt WHERE de.data_type_id = dt.id AND configID=:id");
	} else if ($type == 'notification') {
		$stmt = $db->prepare("SELECT dt.name from device_notifications dn, data_types dt WHERE dn.data_type_id = dt.id AND configID=:id");
	} else {
		$stmt = $db->prepare("SELECT dt.name from device_functions df, data_types dt WHERE df.data_type_id = dt.id AND configID=:id");
	}
	
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results[0];
}

function checkStructureByType($id,$type) {
	
	$db = connect();
	
	if($type=='measurement'){
		$stmt = $db->prepare("SELECT dt.name from device_measurements dm, data_types dt WHERE dm.data_type_id = dt.id");
	} else {
		$stmt = $db->prepare("SELECT dt.name from device_triggers dtr, data_types dt WHERE dtr.data_type_id = dt.id AND configID=:id");
	}
	
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results[0];
}
	



function getTagsByDeviceTypeAndParameterType($devtype,$tagtype) {

	if ($tagtype == 1) {
		$table = 'measurements';
	} else if ($tagtype == 2) {
		$table = 'executions';	
	} else if ($tagtype == 3) {
		$table = 'triggers';	
	} else if ($tagtype == 4) {
		$table = 'notifications';	
	} else if ($tagtype == 5) {
		$table = 'functions';
	}
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM ".$table." WHERE device_type_id=".$devtype." ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
	
}

function getTagsByDeviceTypeAndParameterTypeTwo($type,$dtype) {

	$table = $type."s";

	$db = connect();

	$stmt = $db->prepare("SELECT * FROM ".$table." WHERE device_type_id=".$dtype." ORDER BY name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
	
}
	


function getMeasurements() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, m.device_type_id, t.name as devname,m.description,LEFT(M.description,64) shortDescription FROM measurements as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function insertMeasurement($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO measurements (`name`,`device_type_id`,`description`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['device_type_id'] , ':field3' => $post['description'] ) );


	
}


function getExecutions() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, m.device_type_id, t.name as devname,m.description,LEFT(M.description,64) shortDescription FROM executions as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}



function insertExecution($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO executions (`name`,`device_type_id`,`description`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['device_type_id'], ':field3' => $post['description'] ) );

	
}


function getTriggers() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, m.device_type_id, t.name as devname,m.description,LEFT(M.description,64) shortDescription FROM triggers as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertTrigger($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO triggers (`name`,`device_type_id`,`description`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['device_type_id'], ':field3' => $post['description'] ) );

	
}



function getOperatorSelect($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM operators  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="operator_id" id="operator" >';
	
	$html .= '<option value="">No Operand</option>';

		foreach ($results as $mfr) {
			
			if ($mfr['operand_type_index'] == $current) {
				$html .= '<option value="'.$mfr['operand_type_index'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['operand_type_index'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}
function getOperatorSelectE($current,$struct) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM operators  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if($struct == 0){
		$html = '<select class="form-control" name="operator_id" id="operator2" >';
	} else {
		$html = '<select class="form-control" disabled name="operator_id" id="operator2" >';
	}
	
	$html .= '<option value="">No Operand</option>';

		foreach ($results as $mfr) {
			
			if ($mfr['operand_type_index'] == $current) {
				$html .= '<option value="'.$mfr['operand_type_index'].'" selected="selected">'.$mfr['name'].'</option>';
			} else {
				$html .= '<option value="'.$mfr['operand_type_index'].'">'.$mfr['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}

function getStructures($current,$t) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT m.name,dm.configID FROM device_measurements dm, measurements m WHERE dm.tag_id = m.id  AND dm.data_type_id in (SELECT id from data_types where name = 'STRUCT')
						UNION ALL 
						SELECT t.name,dt.configID FROM device_triggers dt, triggers t WHERE dt.tag_id = t.id  AND dt.data_type_id in (SELECT id from data_types where name = 'STRUCT')
						UNION ALL
						SELECT e.name,de.configID FROM device_executions de, executions e WHERE de.tag_id = e.id  AND de.data_type_id in (SELECT id from data_types where name = 'STRUCT')
						UNION ALL
						SELECT n.name,dn.configID FROM device_notifications dn, notifications n WHERE dn.tag_id = n.id  AND dn.data_type_id in (SELECT id from data_types where name = 'STRUCT')
						UNION ALL
						SELECT f.name,df.configID FROM device_functions df, functions f WHERE df.tag_id = f.id  AND df.data_type_id in (SELECT id from data_types where name = 'STRUCT')
");
	
	
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;	
	
}

function getStructuresOnEdit($current,$t,$struct) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT m.name,dm.configID FROM device_measurements dm, measurements m WHERE dm.tag_id = m.id  AND dm.data_type_id in (SELECT id from data_types where name = 'STRUCT')
							UNION ALL 
							SELECT t.name,dt.configID FROM device_triggers dt, triggers t WHERE dt.tag_id = t.id  AND dt.data_type_id in (SELECT id from data_types where name = 'STRUCT')
							UNION ALL
							SELECT e.name,de.configID FROM device_executions de, executions e WHERE de.tag_id = e.id  AND de.data_type_id in (SELECT id from data_types where name = 'STRUCT')
							UNION ALL
							SELECT n.name,dn.configID FROM device_notifications dn, notifications n WHERE dn.tag_id = n.id  AND dn.data_type_id in (SELECT id from data_types where name = 'STRUCT')
							UNION ALL
							SELECT f.name,df.configID FROM device_functions df, functions f WHERE df.tag_id = f.id  AND df.data_type_id in (SELECT id from data_types where name = 'STRUCT')
	");
		
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if($struct == 0) {
		$html = '<select class="form-control" name="structure_id" id="structure2" >';
	} else {
		$html = '<select class="form-control" disabled name="structure_id" id="structure2" >';
	}
	$html .= '<option value="">No Structure</option>';

		foreach ($results as $struct) {
			
			if ($struct['configID'] == $current) {
				$html .= '<option value="'.$struct['configID'].'" selected="selected">'.$struct['name'].'</option>';
			} else {
				$html .= '<option value="'.$struct['configID'].'">'.$struct['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}

function getMembers($current) {
	
	$db = connect();
	
	$stmt = $db->prepare("SELECT configID,m.name FROM device_measurements dm, measurements m WHERE m.id = dm.tag_id AND structure_id =:id
							UNION ALL 
							SELECT configID,t.name FROM device_triggers dt, triggers t WHERE t.id = dt.tag_id AND structure_id =:id
							UNION ALL
							SELECT configID,e.name FROM device_executions de, executions e WHERE e.id = de.tag_id AND structure_id =:id
							UNION ALL
							SELECT configID,n.name FROM device_notifications dn, notifications n WHERE n.id = dn.tag_id AND structure_id =:id
							UNION ALL
							SELECT configID,f.name FROM device_functions df, functions f WHERE f.id = df.tag_id AND structure_id =:id
	");
	$stmt->bindValue(':id',$current,PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	if($struct == 0) {
		$html = '<select class="form-control" name="structure_id" id="structure2" >';
	} else {
		$html = '<select class="form-control" disabled name="structure_id" id="structure2" >';
	}
		foreach ($results as $struct) {
			
			if ($struct['configID'] == $current) {
				$html .= '<option value="'.$struct['configID'].'" selected="selected">'.$struct['name'].'</option>';
			} else {
				$html .= '<option value="'.$struct['configID'].'">'.$struct['name'].'</option>';
			}
		}

	$html .= '</select>';


	return $html;	
	
}


function getOperands() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM operators ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertOperand($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO operators (`name`,`operand_type_index`) VALUES(:field1,:field2)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['operand_type_index'] ) );


	
}



function getDataTypeSelect($current,$operation) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM data_types  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;	
	
}

function getDataTypeSelectE($current,$operation) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM data_types  ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	$html = '<select class="form-control" name="data_type_id" id="data_type_id_edit2" onChange="changeDropdown2();">';

	$html .= '<option value="">No Data Type</option>';
	
		foreach ($results as $mfr) {
			if($operation != 'function' AND $mfr['name'] == 'STRUCT') {
				
			} else {
				if ($mfr['id'] == $current) {
					$html .= '<option value="'.$mfr['id'].'" selected="selected">'.$mfr['name'].'</option>';
				} else {
					$html .= '<option value="'.$mfr['id'].'">'.$mfr['name'].'</option>';
				}
			}
		}

	$html .= '</select>';


	return $html;	
	
}



function getDataTypes() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM data_types ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}

function getDataType($current) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT name FROM data_types WHERE id=:id ");
	$stmt->bindValue(':id',$current, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0];
}

function insertDataType($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO data_types (`name`) VALUES(:field1)");
	
	$stmt->execute(array(':field1' => $post['name'] ) );


	
}



function getNotifications() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, m.device_type_id, t.name as devname, m.description,LEFT(M.description,64) shortDescription FROM notifications as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertNotification($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO notifications (`name`,`device_type_id`,`description`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['device_type_id'], ':field3' => $post['description'] ) );

	
}

function getFunctions() {
	
	$db = connect();

	$stmt = $db->prepare("SELECT m.id, m.name, m.device_type_id, t.name as devname,m.description,LEFT(M.description,64) shortDescription FROM functions as m LEFT JOIN device_types as t ON m.device_type_id=t.id ORDER BY m.name ");
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results;
}


function insertFunction($post) {
	
	$db = connect();

	$stmt = $db->prepare("INSERT INTO functions (`name`,`device_type_id`,`description`) VALUES(:field1,:field2,:field3)");
	
	$stmt->execute(array(':field1' => $post['name'], ':field2' => $post['device_type_id'], ':field3' => $post['description'] ) );

	
}

function insertAccount($post){
	
	$db = connect();
	
	$stmt=$db->prepare("INSERT INTO accounts (`name`,`account_num`,`super_account`) VALUES (?,?,?)");
	$stmt->execute(array($post['account_name'],$post['account_num'],$post['super_account']));
}

function getSuperAccountSelect(){

	$html = '<select class="form-control" name="super_account" >';
	
	$html .= '<option value="">--Super--</option>';

	$html .= '<option value="1">Yes</option>';
	$html .= '<option value="0">No</option>';

	$html .= '</select>';


	return $html;
}

function deleteRecord($table,$id){
	
	$db = connect();
	
	$stmt=$db->query("DELETE FROM $table WHERE id = $id");
}

function deleteInstance($table,$id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("UPDATE $table set trash_bin = 1 where id = $id");
	
	$stmt->execute();
}

function updateControllerType($post)  {
	
	$db = connect();
	
	if($post['controller_type'] == "Standard") {
		$post['num_config'] = 4;
	}
	
	$stmt = $db->prepare("UPDATE devices SET controller_type = '".$post['controller_type']."',  num_config = '".$post['num_config']."' WHERE id = ".$post['id']."  ");
	
	$stmt->execute();
}

function restoreRecord($table,$id)  {
	
	$db = connect();
	
	$stmt = $db->prepare("UPDATE $table set trash_bin = 0 where id = $id");
	
	$stmt->execute();
}

function restoreAllRecords()  {
	
	$db = connect();
	
	$stmt = $db->query("UPDATE sites set trash_bin = 0 where trash_bin = 1");
	
	$stmt = $db->query("UPDATE site_controllers set trash_bin = 0 where trash_bin = 1");
	
	$stmt = $db->query("UPDATE controller_objects set trash_bin = 0 where trash_bin = 1");
	
	$stmt = $db->query("UPDATE object_devices set trash_bin = 0 where trash_bin = 1");
	
}

function deleteAllRecords()  {
	
	$db = connect();
	
	$stmt = $db->query("DELETE FROM sites WHERE trash_bin = 1");
	
	$stmt = $db->query("DELETE FROM site_controllers  where trash_bin = 1");
	
	$stmt = $db->query("DELETE FROM controller_objects where trash_bin = 1");
	
	$stmt = $db->query("DELETE FROM object_devices where trash_bin = 1");
	
}


function deleteDevice($table,$id){
	
	$db = connect();
	
	$stmt=$db->query("DELETE FROM $table WHERE id = $id");
	$sql = 'CALL reorder()';
	$stmt=$db->query($sql);
}

function deleteObject($table,$id){
	
	$db = connect();
	
	$stmt=$db->query("UPDATE $table set trash_bin = 1 where id = $id");
	$stmt=$db->query("UPDATE object_devices set trash_bin = 1 WHERE object_id = $id");
}

function deleteController($table,$id){
	
	$db = connect();
	
	$stmt=$db->query("UPDATE $table set trash_bin = 1 where id = $id");
	$stmt=$db->query("UPDATE controller_objects set trash_bin = 1 WHERE controller_id = $id");
	$stmt=$db->query("UPDATE object_devices set trash_bin = 1 WHERE controller_id = $id");
}

function deleteSite($table,$id){
	
	$db = connect();
	
	$stmt=$db->query("UPDATE $table set trash_bin = 1 where id = $id");
	$stmt=$db->query("UPDATE site_controllers set trash_bin = 1 WHERE site_id = $id");
	$stmt=$db->query("UPDATE controller_objects set trash_bin = 1 WHERE controller_id in (SELECT id from site_controllers WHERE site_id = $id)");
	$stmt=$db->query("UPDATE object_devices set trash_bin = 1 WHERE site_id = $id");
}

function deleteDeviceSetting($table,$configID){

	$regNum = getRegNum($table,$configID);
 
	$db = connect();
	
	$stmt=$db->query("DELETE FROM $table WHERE configID = $configID");
	$stmt=$db->query("UPDATE device_measurements SET structure_id = 0 WHERE structure_id = $configID");
	$stmt=$db->query("UPDATE device_triggers SET structure_id = 0 WHERE structure_id = $configID");
	$stmt=$db->query("UPDATE device_executions SET structure_id = 0 WHERE structure_id = $configID");
	$stmt=$db->query("UPDATE device_notifications SET structure_id = 0 WHERE structure_id = $configID");
	$stmt=$db->query("UPDATE device_functions SET structure_id = 0 WHERE structure_id = $configID");
	
	decrementGreaterRegs($table,$regNum);
}

function getRegNum($table,$id){
	
	$db = connect();
	
	$stmt=$db->query("SELECT * FROM $table WHERE configID = $id");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results[0]['regnum'];
}

function decrementGreaterRegs($table,$num){
	
	$db = connect();
	
	$stmt=$db->query("SELECT * FROM $table WHERE regnum > $num ORDER BY regnum DESC");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	foreach($results as $row){
		$decrement = $row['regnum']-1;
		$id = $row['configID'];
		$stmt = $db->query("UPDATE $table SET `regnum` = $decrement WHERE configID = $id");
	}
}

function getSelectIPAddressType(){
	
	$html = '<select class="form-control" name="address_type" >';
	
	$html .= '<option value="">--Type--</option>';

	$html .= '<option value="IPv4">IPv4</option>';
	$html .= '<option value="IPv6">IPv6</option>';

	$html .= '</select>';


	return $html;	
}

function getIPAddressType(){
	
	$html = '<select class="form-control" name="address_type" >';

	$html .= '<option value="IPv4">IPv4</option>';
	$html .= '<option value="IPv6">IPv6</option>';

	$html .= '</select>';


	return $html;	
}

function getControllerIPAddressType(){
	
	$html = '<select class="form-control" name="address_type" id="address_type" >';

	$html .= '<option value="IPv4">IPv4</option>';
	$html .= '<option value="IPv6">IPv6</option>';
	$html .= '<option value="Modbus">ModbusRTU / RS485</option>';
	$html .= '<option value="Com">COM / RS232</option>';
	$html .= '<option value="URL">URL</option>';
	
	$html .= '</select>';


	return $html;	
}

function insertSite($post){
	
	$db = connect();
	
	$stmt=$db->prepare("INSERT INTO sites (`account_id`,`name`,`latitude`,`longitude`,`active`,`status`,`ip_address`,`address_type`) VALUES (?,?,?,?,?,?,?,?)");
	$stmt->execute(array($post['account_id'],$post['name'],$post['latitude'],$post['longitude'],$post['active'],$post['status'],$post['ip_address'],$post['address_type']));
}



function getTagData($table,$id) {
	
	$db = connect();

	$stmt = $db->prepare("SELECT * FROM $table WHERE configID=:id  ");
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$stmt->execute();
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);

	return $results[0];
}

function getTagTypeSelect($type,$did,$current){
	
	$dtype = getDeviceType($did);
 
	$ctob = getTagsByDeviceTypeAndParameterTypeTwo($type,$dtype);

	if ($ctob) {

		$html = '<select name="tag_id" class="form-control" id="tag">
		<option value="">--Choose Tag--</option>';

				foreach ($ctob as $ob) {
					if($ob['id'] == $current){
						$html .= '<option value="'.$ob['id'].'" selected="selected">'.$ob['name'].'</option>';
					}else{
						$html .= '<option value="'.$ob['id'].'">'.$ob['name'].'</option>';
					}
				}


		$html .= '</select>';


	} else {

		$html = 'error';

	}
	return $html;
}

function getDeviceType($id){
	
	$db = connect();
	
	$stmt=$db->query("SELECT * FROM devices WHERE id = $id");
	$results = $stmt->fetchAll(PDO::FETCH_ASSOC);
	
	return $results[0]['device_type_id'];
}
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Quest Project Status</title>

<!-- start: Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- end: Mobile Specific -->

<!-- start: CSS -->
<link href="/quest/css/bootstrap.min.css" rel="stylesheet">
<link href="/quest/css/bootstrap-responsive.min.css" rel="stylesheet">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->
	
</head>

<body>

<img src="/reporting/images/logo.jpg" class="img-responsive"  />

<h4 class="text-center">Which Project Are You Currently Working On?</h4>

<div class="alert alert-danger" role="alert" style="font-size: 5.0vw;">This will update the PM Portal with your current status...</div>

<a href="update.php"><button type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 200%;padding:3%;">Champion</button></a><br>
<a href="update.php"><button type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 200%;padding:3%;">Grenadier</button></a><br>
<a href="update.php"><button type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 200%;padding:3%;">Trinity</button></a>

</body>
</html>
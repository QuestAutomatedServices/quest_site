<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Quest Project Status</title>

<!-- start: Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- end: Mobile Specific -->

<!-- start: CSS -->
<link id="bootstrap-style" href="/quest/css/bootstrap.min.css" rel="stylesheet">
<link href="/quest/css/bootstrap-responsive.min.css" rel="stylesheet">

<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
<!-- end: CSS -->
	
</head>

<body>

<img src="/reporting/images/logo.jpg" class="img-responsive"  />

<h4 class="text-center">Are There Any Significant Issus Affecting the Implementation/Timeline?</h4>

<a href="complete.php"><button type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 200%;padding:3%;">No</button></a><br>
<a href="problem.php"><button type="button" class="btn btn-primary btn-lg btn-block" style="font-size: 200%;padding:3%;">Yes</button></a><br>

</body>
</html>